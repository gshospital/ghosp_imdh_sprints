using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InfNosocomiales
{
	partial class EGI2117F1
	{

		#region "Upgrade Support "
		private static EGI2117F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static EGI2117F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new EGI2117F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_Option2_0", "_Option2_1", "Frame3", "_Option1_1", "_Option1_0", "Frame2", "SdcFechaIni", "SdcFechaFin", "Line1", "Label2", "Label1", "Frame1", "CbImprimir", "CbCerrar", "CbPantalla", "Option1", "Option2", "ShapeContainer2"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadRadioButton _Option2_0;
		private Telerik.WinControls.UI.RadRadioButton _Option2_1;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		private Telerik.WinControls.UI.RadRadioButton _Option1_1;
		private Telerik.WinControls.UI.RadRadioButton _Option1_0;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaFin;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		public Telerik.WinControls.UI.RadRadioButton[] Option1 = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] Option2 = new Telerik.WinControls.UI.RadRadioButton[2];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EGI2117F1));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this._Option2_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._Option2_1 = new Telerik.WinControls.UI.RadRadioButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this._Option1_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._Option1_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
			this.SdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.CbImprimir = new Telerik.WinControls.UI.RadButton();
			this.CbCerrar = new Telerik.WinControls.UI.RadButton();
			this.CbPantalla = new Telerik.WinControls.UI.RadButton();
			this.Frame1.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer2
			// 
			this.ShapeContainer2.Location = new System.Drawing.Point(3, 16);
			this.ShapeContainer2.Size = new System.Drawing.Size(311, 163);
			this.ShapeContainer2.Shapes.Add(Line1);
			// 
			// Frame1
			// 
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this.Frame3);
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Controls.Add(this.SdcFechaIni);
			this.Frame1.Controls.Add(this.SdcFechaFin);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(4, 8);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(311, 163);
			this.Frame1.TabIndex = 8;
			this.Frame1.Text = "Seleccione intervalo de fechas";
			this.Frame1.Visible = true;
			// 
			// Frame3
			// 
			//this.Frame3.BackColor = System.Drawing.SystemColors.Control;
			this.Frame3.Controls.Add(this._Option2_0);
			this.Frame3.Controls.Add(this._Option2_1);
			this.Frame3.Enabled = true;
			//this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(148, 76);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(153, 69);
			this.Frame3.TabIndex = 13;
			this.Frame3.Text = "Ordenaci�n";
			this.Frame3.Visible = true;
			// 
			// _Option2_0
			// 
			//this._Option2_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._Option2_0.BackColor = System.Drawing.SystemColors.Control;
			this._Option2_0.CausesValidation = true;
			this._Option2_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option2_0.IsChecked = true;
			this._Option2_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option2_0.Enabled = true;
			//this._Option2_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option2_0.Location = new System.Drawing.Point(12, 20);
			this._Option2_0.Name = "_Option2_0";
			this._Option2_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option2_0.Size = new System.Drawing.Size(129, 13);
			this._Option2_0.TabIndex = 4;
			this._Option2_0.TabStop = true;
			this._Option2_0.Text = "Fecha de Registro";
			this._Option2_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option2_0.Visible = true;
			// 
			// _Option2_1
			// 
			//this._Option2_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._Option2_1.BackColor = System.Drawing.SystemColors.Control;
			this._Option2_1.CausesValidation = true;
			this._Option2_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option2_1.IsChecked = false;
			this._Option2_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option2_1.Enabled = true;
			//this._Option2_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option2_1.Location = new System.Drawing.Point(12, 44);
			this._Option2_1.Name = "_Option2_1";
			this._Option2_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option2_1.Size = new System.Drawing.Size(85, 13);
			this._Option2_1.TabIndex = 5;
			this._Option2_1.TabStop = true;
			this._Option2_1.Text = "Nombre";
			this._Option2_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option2_1.Visible = true;
			// 
			// Frame2
			// 
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this._Option1_1);
			this.Frame2.Controls.Add(this._Option1_0);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(12, 76);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(133, 69);
			this.Frame2.TabIndex = 12;
			this.Frame2.Text = "Selecci�n de Pacientes";
			this.Frame2.Visible = true;
			// 
			// _Option1_1
			// 
			//this._Option1_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._Option1_1.BackColor = System.Drawing.SystemColors.Control;
			this._Option1_1.CausesValidation = true;
			this._Option1_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_1.IsChecked = false;
			this._Option1_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_1.Enabled = true;
			//this._Option1_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_1.Location = new System.Drawing.Point(12, 44);
			this._Option1_1.Name = "_Option1_1";
			this._Option1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_1.Size = new System.Drawing.Size(53, 13);
			this._Option1_1.TabIndex = 3;
			this._Option1_1.TabStop = true;
			this._Option1_1.Text = "Todos";
			this._Option1_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_1.Visible = true;
			// 
			// _Option1_0
			// 
			//this._Option1_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._Option1_0.BackColor = System.Drawing.SystemColors.Control;
			this._Option1_0.CausesValidation = true;
			this._Option1_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_0.IsChecked = true;
			this._Option1_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_0.Enabled = true;
			//this._Option1_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_0.Location = new System.Drawing.Point(12, 20);
			this._Option1_0.Name = "_Option1_0";
			this._Option1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_0.Size = new System.Drawing.Size(81, 13);
			this._Option1_0.TabIndex = 2;
			this._Option1_0.TabStop = true;
			this._Option1_0.Text = "Ingresados";
			this._Option1_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_0.Visible = true;
			// 
			// SdcFechaIni
			// 
			//this.SdcFechaIni.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SdcFechaIni.CustomFormat = "DD/MM/YYYY";
			this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaIni.Location = new System.Drawing.Point(16, 42);
			this.SdcFechaIni.Name = "SdcFechaIni";
			this.SdcFechaIni.Size = new System.Drawing.Size(105, 17);
			this.SdcFechaIni.TabIndex = 0;
			// 
			// SdcFechaFin
			// 
			//this.SdcFechaFin.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SdcFechaFin.CustomFormat = "DD/MM/YYYY";
			this.SdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaFin.Location = new System.Drawing.Point(176, 42);
			this.SdcFechaFin.Name = "SdcFechaFin";
			this.SdcFechaFin.Size = new System.Drawing.Size(105, 17);
			this.SdcFechaFin.TabIndex = 1;
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.SystemColors.ScrollBar;
			this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line1.BorderWidth = 1;
			this.Line1.Enabled = false;
			this.Line1.Name = "Line1";
			this.Line1.Visible = true;
			this.Line1.X1 = (int) 0;
			this.Line1.X2 = (int) 348;
			this.Line1.Y1 = (int) 61;
			this.Line1.Y2 = (int) 61;
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(176, 24);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(73, 17);
			this.Label2.TabIndex = 11;
			this.Label2.Text = "Fecha fin:";
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(16, 24);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(73, 17);
			this.Label1.TabIndex = 10;
			this.Label1.Text = "Fecha inicio:";
			// 
			// CbImprimir
			// 
			//this.CbImprimir.BackColor = System.Drawing.SystemColors.Control;
			this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbImprimir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbImprimir.Location = new System.Drawing.Point(60, 180);
			this.CbImprimir.Name = "CbImprimir";
			this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbImprimir.Size = new System.Drawing.Size(81, 31);
			this.CbImprimir.TabIndex = 6;
			this.CbImprimir.Text = "&Impresora";
			this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbImprimir.UseVisualStyleBackColor = false;
			this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
			// 
			// CbCerrar
			// 
			//this.CbCerrar.BackColor = System.Drawing.SystemColors.Control;
			this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbCerrar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbCerrar.Location = new System.Drawing.Point(236, 180);
			this.CbCerrar.Name = "CbCerrar";
			this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCerrar.Size = new System.Drawing.Size(81, 31);
			this.CbCerrar.TabIndex = 9;
			this.CbCerrar.Text = "&Cerrar";
			this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbCerrar.UseVisualStyleBackColor = false;
			this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
			// 
			// CbPantalla
			// 
			//this.CbPantalla.BackColor = System.Drawing.SystemColors.Control;
			this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbPantalla.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbPantalla.Location = new System.Drawing.Point(148, 180);
			this.CbPantalla.Name = "CbPantalla";
			this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbPantalla.Size = new System.Drawing.Size(81, 31);
			this.CbPantalla.TabIndex = 7;
			this.CbPantalla.Text = "&Pantalla";
			this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbPantalla.UseVisualStyleBackColor = false;
			this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
			// 
			// EGI2117F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(321, 223);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.CbImprimir);
			this.Controls.Add(this.CbCerrar);
			this.Controls.Add(this.CbPantalla);
			this.Frame1.Controls.Add(ShapeContainer2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EGI2117F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Listado de Infecciones Nosocomiales - EGI2117F1";
			this.Closed += new System.EventHandler(this.EGI2117F1_Closed);
			this.Load += new System.EventHandler(this.EGI2117F1_Load);
			this.Frame1.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeOption2();
			InitializeOption1();
		}
		void InitializeOption2()
		{
			this.Option2 = new Telerik.WinControls.UI.RadRadioButton[2];
			this.Option2[0] = _Option2_0;
			this.Option2[1] = _Option2_1;
		}
		void InitializeOption1()
		{
			this.Option1 = new Telerik.WinControls.UI.RadRadioButton[2];
			this.Option1[1] = _Option1_1;
			this.Option1[0] = _Option1_0;
		}
		#endregion
	}
}