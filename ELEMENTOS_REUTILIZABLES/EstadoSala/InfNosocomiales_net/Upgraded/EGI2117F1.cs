using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using ElementosCompartidos;

namespace InfNosocomiales
{
	public partial class EGI2117F1
		: RadForm
	{

		public EGI2117F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}




		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			if (fechas())
			{
				IR_A_IMPRIMIR("I");
			}
		}

		private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
			if (fechas())
			{
				IR_A_IMPRIMIR("P");
			}
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void EGI2117F1_Load(Object eventSender, EventArgs eventArgs)
		{
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);

            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            //DGMORENOG_TODO_X_5 - PENDIENTE REVISAR
            //this.setHelpContextID(152117);
            SdcFechaIni.MaxDate = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
			SdcFechaFin.MaxDate = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
		}

		private void IR_A_IMPRIMIR(string ParaDestino)
		{
            //DGMORENOG_TODO_X_5 - PENDIENTE DEFINIR TRATAMIENTO DE BASE DATOS ACCES
            //object dbLangSpanish = null;
            //object DBEngine = null;
            //try
            //{
            //	string sql = String.Empty;
            //	DataSet RrSQL = null;
            //	string stPathTemp = String.Empty;
            //	bool bBase = false;
            //	//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //	//UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //	Database bs = null;
            //	//UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //	Recordset Rs = null;

            //	switch(ParaDestino)
            //	{
            //		case "I" : 
            //			//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.Destination = 1; 
            //			//UPGRADE_TODO: (1067) Member ShowPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.fPantalla.ShowPrinter(); 
            //			//UPGRADE_TODO: (1067) Member CopiesToPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			//UPGRADE_TODO: (1067) Member Copies is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.CopiesToPrinter = BNosocomiales.fPantalla.Copies; 
            //			break;
            //		case "P" : 
            //			//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.WindowShowPrintSetupBtn = true; 
            //			//UPGRADE_TODO: (1067) Member WindowShowPrintBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.WindowShowPrintBtn = true; 
            //			//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.Destination = 0; 
            //			//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.WindowState = 2; 
            //			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			BNosocomiales.Listado_general.WindowTitle = "Listado de Infecciones Nosocomiales"; 
            //			break;
            //	}


            //	//Se crea una base de datos temporal para realizar el informe
            //	stPathTemp = BNosocomiales.PathAplicacion + "\\" + BNosocomiales.NombreBaseAccess;
            //	bBase = false;
            //	if (FileSystem.Dir(stPathTemp, FileAttribute.Normal) != "")
            //	{
            //		bBase = true;
            //	}
            //	if (!bBase)
            //	{ // SI est� dado
            //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		bs = (Database) DBEngine.Workspaces(0).CreateDatabase(stPathTemp, dbLangSpanish);
            //	}
            //	else
            //	{
            //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		bs = (Database) DBEngine.Workspaces(0).OpenDatabase(stPathTemp);
            //		//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		foreach (TableDef TABLA in bs.TableDefs)
            //		{
            //			//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			if (Convert.ToString(TABLA.Name) == "tabla_infecciones")
            //			{
            //				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //				bs.Execute("DROP TABLE tabla_infecciones");
            //				break;
            //			}
            //		}
            //	}
            //	//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	bs.Execute("CREATE TABLE tabla_infecciones (Historia text, NombrePaciente text, " + 
            //	           " FechaApunte text, NombreMedico text, TipoInfeccion text, PacienteQuirurgico text, " + 
            //	           " CuracionAlta text, Fallecimiento text, Consecuencia text, Origen text)");

            //	switch(BNosocomiales.stTipoSer)
            //	{
            //		case "H" : 

            //			sql = "SELECT HDOSSIER.ghistoria, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, " + 
            //			      " ENOSOCOM.fapuntes, " + 
            //			      " DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers, " + 
            //			      " ENOSOCOM.oinfecci , ENOSOCOM.iquirurg, ENOSOCOM.icuracio, ENOSOCOM.ifalleci, " + 
            //			      " CONS.dconsinf, ENOSOCOM.iorigeni " + 
            //			      " FROM ENOSOCOM " + 
            //			      " inner join aepisadm on aepisadm.ganoadme = ENOSOCOM.ganoregi and aepisadm.gnumadme = ENOSOCOM.gnumregi and ENOSOCOM.itiposer='H' " + 
            //			      " INNER JOIN AMOVIMIE ON ENOSOCOM.ganoregi = AMOVIMIE.ganoregi AND " + 
            //			      " ENOSOCOM.gnumregi = AMOVIMIE.gnumregi AND " + 
            //			      " AMOVIMIE.itiposer = 'H' AND " + 
            //			      " AMOVIMIE.fmovimie <= ENOSOCOM.fapuntes And " + 
            //			      " (AMOVIMIE.ffinmovi >= ENOSOCOM.fapuntes or AMOVIMIE.ffinmovi is null) " + 
            //			      " inner join dpersona on dpersona.gpersona = gmedicor " + 
            //			      " inner join dpacient on dpacient.gidenpac = aepisadm.gidenpac " + 
            //			      " left join HDOSSIER on HDOSSIER.gidenpac = DPACIENT.gidenpac and HDOSSIER.icontenido='H' " + 
            //			      " LEFT JOIN ECONSINF CONS ON CONS.gconsinf = ENOSOCOM.gconsinf " + 
            //			      " WHERE " + 
            //			      "  AEPISADM.faltplan is null AND " + 
            //			      "(ENOSOCOM.fapuntes >= '" + SdcFechaIni.Value.ToString("MM/dd/yyyy") + " 00:00:00" + " ') AND " + 
            //			      "(ENOSOCOM.fapuntes <= '" + SdcFechaFin.Value.ToString("MM/dd/yyyy") + " 23:59:59" + " ') "; 
            //			if (Option1[0].IsChecked)
            //			{
            //				if (Option2[0].IsChecked)
            //				{
            //					sql = sql + " ORDER BY fapuntes DESC ";
            //				}
            //				else
            //				{
            //					sql = sql + " ORDER BY DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, fapuntes DESC";
            //				}
            //			}
            //			else
            //			{
            //				sql = sql + "UNION ALL " + 
            //				      " SELECT HDOSSIER.ghistoria, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, " + 
            //				      " ENOSOALT.fapuntes, " + 
            //				      " DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers, " + 
            //				      " ENOSOALT.oinfecci , ENOSOALT.iquirurg, ENOSOALT.icuracio, ENOSOALT.ifalleci, " + 
            //				      " CONS.dconsinf, ENOSOALT.iorigeni " + 
            //				      " FROM ENOSOALT " + 
            //				      " inner join aepisadm on aepisadm.ganoadme = ENOSOALT.ganoregi and aepisadm.gnumadme = ENOSOALT.gnumregi and ENOSOALT.itiposer='H' " + 
            //				      " INNER JOIN AMOVIMIE ON ENOSOALT.GANOREGI = AMOVIMIE.ganoregi AND " + 
            //				      "ENOSOALT.GNUMREGI = AMOVIMIE.gnumregi AND " + 
            //				      "AMOVIMIE.itiposer = 'H' AND " + 
            //				      "AMOVIMIE.ffinmovi >= ENOSOALT.FAPUNTES AND " + 
            //				      "AMOVIMIE.fmovimie <= ENOSOALT.FAPUNTES " + 
            //				      " inner join dpersona on dpersona.gpersona = gmedicor " + 
            //				      " inner join dpacient on dpacient.gidenpac = aepisadm.gidenpac " + 
            //				      " left join HDOSSIER on HDOSSIER.gidenpac = DPACIENT.gidenpac and HDOSSIER.icontenido='H' " + 
            //				      " LEFT JOIN ECONSINF CONS ON CONS.gconsinf = ENOSOALT.gconsinf " + 
            //				      " WHERE " + 
            //				      "  AEPISADM.faltplan is NOT null AND " + 
            //				      "(ENOSOALT.fapuntes >= '" + SdcFechaIni.Value.ToString("MM/dd/yyyy") + " 00:00:00" + " ') AND " + 
            //				      "(ENOSOALT.fapuntes <= '" + SdcFechaFin.Value.ToString("MM/dd/yyyy") + " 23:59:59" + " ') ";
            //				if (Option2[0].IsChecked)
            //				{
            //					sql = sql + " ORDER BY fapuntes DESC ";
            //				}
            //				else
            //				{
            //					sql = sql + " ORDER BY DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, fapuntes DESC";
            //				}
            //			} 
            //			break;
            //		case "U" : case "N" : 
            //			//No implementado 
            //			break;
            //	}

            //	SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BNosocomiales.cnConexion);
            //	RrSQL = new DataSet();
            //	tempAdapter.Fill(RrSQL);
            //	if (RrSQL.Tables[0].Rows.Count != 0)
            //	{
            //		sql = "select * from tabla_infecciones  where 1=2";
            //		//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		Rs = (Recordset) bs.OpenRecordset(sql);
            //		//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
            //		RrSQL.MoveFirst();
            //		foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
            //		{
            //			//UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Rs.AddNew();
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Historia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			Rs["Historia"] = (Recordset) (Convert.ToString(iteration_row["ghistoria"]) + "").Trim();
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(NombrePaciente). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			Rs["NombrePaciente"] = (Recordset) ((Convert.ToString(iteration_row["dape1pac"]) + "").Trim() + " " + (Convert.ToString(iteration_row["dape2pac"]) + "").Trim() + ", " + (Convert.ToString(iteration_row["dnombpac"]) + "").Trim());
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(FechaApunte). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			Rs["FechaApunte"] = (Recordset) Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm");
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(NombreMedico). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			Rs["NombreMedico"] = (Recordset) ((Convert.ToString(iteration_row["dap1pers"]) + "").Trim() + " " + (Convert.ToString(iteration_row["dap2pers"]) + "").Trim() + ", " + (Convert.ToString(iteration_row["dnompers"]) + "").Trim());
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(TipoInfeccion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			Rs["TipoInfeccion"] = (Recordset) (Convert.ToString(iteration_row["oinfecci"]) + "").Trim();
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["iquirurg"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(PacienteQuirurgico). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				Rs["PacienteQuirurgico"] = (Recordset) ((Convert.ToString(iteration_row["iquirurg"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["icuracio"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(CuracionAlta). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				Rs["CuracionAlta"] = (Recordset) ((Convert.ToString(iteration_row["icuracio"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["ifalleci"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Fallecimiento). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				Rs["Fallecimiento"] = (Recordset) ((Convert.ToString(iteration_row["ifalleci"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Consecuencia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			Rs["Consecuencia"] = (Recordset) (Convert.ToString(iteration_row["dconsinf"]) + "").Trim();
            //			switch(Convert.ToString(iteration_row["iorigeni"]))
            //			{
            //				case "I" : 
            //					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
            //					Rs["Origen"] = (Recordset) "Intrahospitalaria"; 
            //					break;
            //				case "E" : 
            //					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
            //					Rs["Origen"] = (Recordset) "Extrahospitalaria"; 
            //					break;
            //				default:
            //					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
            //					Rs["Origen"] = (Recordset) ""; 
            //					break;
            //			}
            //			//UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Rs.Update();
            //		}
            //		//UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		Rs.Close();
            //		//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		bs.Close();
            //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		DBEngine.Workspaces(0).Close();
            //	}
            //	else
            //	{
            //		MessageBox.Show("No se han encontrado datos para las condiciones especificadas", "Listado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //		return;
            //	}
            //	//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSQL.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //	RrSQL.Close();


            //	BNosocomiales.proCabCrystal();

            //	//UPGRADE_TODO: (1067) Member reportfilename is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.reportfilename = BNosocomiales.PathReport + "\\EGI2117R1.rpt";
            //	//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.SQLQuery = " SELECT * From tabla_infecciones";
            //	//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.DataFiles[0] = BNosocomiales.PathAplicacion + "\\" + BNosocomiales.NombreBaseAccess;
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["form1"] = "\"" + BNosocomiales.VstrCrystal.VstGrupoHo + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["form2"] = "\"" + BNosocomiales.VstrCrystal.VstNombreHo + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["form3"] = "\"" + BNosocomiales.VstrCrystal.VstDireccHo + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["FechaDesde"] = "\"" + SdcFechaIni.Value.Date + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["FechaHasta"] = "\"" + SdcFechaFin.Value.Date + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["Pacientes"] = "\"" + ((Option1[0].IsChecked) ? Option1[0].Text : Option1[1].Text) + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Formulas["Ordenacion"] = "\"" + ((Option2[0].IsChecked) ? Option2[0].Text : Option2[1].Text) + "\"";
            //	//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Action = 0;
            //	//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.PageCount();
            //	//UPGRADE_TODO: (1067) Member Copies is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.fPantalla.Copies = 1;
            //	//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.PrinterStopPage = BNosocomiales.Listado_general.PageCount;
            //	//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	BNosocomiales.Listado_general.Reset();
            //}
            //catch (Exception e)
            //{

            //	//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
            //	if (Information.Err().Number == 32755 || Information.Err().Number == 20545)
            //	{ // error producido por pulsar cancelar en el commondialog
            //		return;
            //	}
            //	else
            //	{
            //		Serrores.AnalizaError("Infecciones Nosocomiales", Path.GetDirectoryName(Application.ExecutablePath), "indra", "IR_A_IMPRIMIR:FORMULARIO", BNosocomiales.cnConexion);
            //	}
            //}

        }

        private bool fechas()
		{
			bool result = false;
			try
			{
				result = true;
				if (this.SdcFechaIni.Value > this.SdcFechaFin.Value)
				{
					result = false;
					short tempRefParam = 1020;
					string[] tempRefParam2 = new string[] { Label2.Text, "mayor", Label1.Text};
					BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BNosocomiales.cnConexion, tempRefParam2);
				}
				else
				{
					result = true;
				}

				return result;
			}
			catch
			{
				result = false;
				RadMessageBox.Show("Error de fechas.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
				return result;
			}
		}
		private void EGI2117F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}