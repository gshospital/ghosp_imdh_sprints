using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace InfNosocomiales
{
	internal static class BNosocomiales
	{


		public static SqlConnection cnConexion = null;

		public static mbAcceso.strControlOculto[] astrcontroles = null;
		public static mbAcceso.strEventoNulo[] astreventos = null;
		public static string VstUsuario_NT = String.Empty;

		public static int viAgruServicio = 0;
		public static int viSerUsuario = 0;
		public static string iotrprof = String.Empty; //para otros profesionales

		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static dynamic Listado_general = null;
		public static dynamic fPantalla = null;

		public static string gUsuario_Crystal = String.Empty;
		public static string gContrase�a_Crystal = String.Empty;

		public static string NOMBRE_DSN_SQLSERVER = String.Empty;
		public static string NOMBRE_DSN_ACCESS = String.Empty;

		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public string VstPoblaciHo;
			public string VstrTelefono;
			public string VstrFax;
			public string VstrNif;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstPoblaciHo = String.Empty;
					result.VstrTelefono = String.Empty;
					result.VstrFax = String.Empty;
					result.VstrNif = String.Empty;
					return result;
			}
		}

		public static BNosocomiales.CabCrystal VstrCrystal = BNosocomiales.CabCrystal.CreateInstance();

		public static string vgUsuario = String.Empty;
		public static string VstUniUsuario = String.Empty;

		public static string vPathDocumentos = String.Empty; //lleva el path de los documentos de Word
		public static string NombreBaseAccess = String.Empty; //nombre del .mdb
		public static string PathAplicacion = String.Empty;
		public static string PathReport = String.Empty;

		public static string vstSeparadorDecimal = String.Empty;
		public static string stDecimalBD = String.Empty;

		public static string lSep_Fecha = String.Empty; //SEPARADOR FECHA CONFIGURACION REGIONAL
		public static string lSep_Hora = String.Empty; //SEPARADOR HORA CONFIGURACION REGIONAL
		public static string lFor_fecha = String.Empty; // FORMATO CONFIGURACION REGIONAL FECHA SOLO
		public static string LFOR_HORA = String.Empty; //FORMATO DE CONFIGURACION DE HORA
		public static string lEsp_For_Siglo = String.Empty; // MOSTRAR SIGLO O NO
		public static int LNumero_Ceros = 0; // numero de ceros de las horas
		public static int iFormato_Fecha = 0; // entero que identifica al formato  de fecha -> vaspread.typedateformat
		public static string lSep_Decimal = String.Empty; // separador decimal (triage temperatura y peso)
		public static string Separador_Hora_SQL = String.Empty;
		public static string Formato_Fecha_SQL = String.Empty;
		public static string Separador_Fecha_SQL = String.Empty;

		public static string FechaHora_Sistema = String.Empty;
		public static string VstSeparadorFecha_BaseDatos = String.Empty;
		public static string VstSeparadorHora_BaseDatos = String.Empty;
		public static string VstFormato_BaseDatos = String.Empty;

		public const int LOCALE_SDATE = 0x1D; //
		public const int LOCALE_STIME = 0x1E;
		public const int LOCALE_SSHORTDATE = 0x1F;
		public const int LOCALE_STIMEFORMAT = 0x1003; //formato de hora
		public const int LOCALE_SYSTEM_DEFAULT = 0x800;
		public const int LOCALE_ICENTURY = 0x24; //cuatro cifras de a�o
		public const int LOCALE_ITLZERO = 0x25; // numero de ceros en la hora
		public const int LOCALE_IDATE = 0x21; // entero que devuelve el formato de fecha
		public const int LOCALE_ITIME = 0x23; // ENTERO QUE IDENTIFICA A LA HORA
		public const int LOCALE_SDECIMAL = 0xE; // devuelve el caracter decimal

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetPrivateProfileString([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpApplicationName, System.IntPtr lpKeyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDefault, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpReturnedString, int nSize, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFileName);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetCurrentDirectoryA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetCurrentDirectory(int nBufferLength, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData, int cchData);

		public static string stTipoSer = String.Empty;
		public static int intAnioRegi = 0;
		public static int lngNumRegi = 0;
		public static string stPaciente = String.Empty;
		public static string stHistoria = String.Empty;
		public static string stIngreso = String.Empty;
		public static string stIdenPac = String.Empty;
		public static bool bDesdeInfMasivos = false; //para cuando se accede a esta dll desde documento de la evolucion del paciente y se ha
		//accedido a �sta ultima desde imprimir informes masivos de la consulta de la historia clinica
		public static bool gGenerarPDFDesdeInformesMasivos = false;
		public static string gIdProcesoMasivo = String.Empty;
		public static string gRutaDirectorioPDF = String.Empty;
		public static string ErroresNet = String.Empty;


		internal static void ObtenerValoresTipoBD()
		{
			DataSet tRr = null;
			string tstQuery = String.Empty;
			string formato = String.Empty;
			int I = 0;
			try
			{
				tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, cnConexion);
				tRr = new DataSet();
				tempAdapter.Fill(tRr);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
				{
					case "SQL SERVER 6.5" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						Formato_Fecha_SQL = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
						for (I = 1; I <= formato.Length; I++)
						{
							if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && Separador_Fecha_SQL == "")
							{
								Separador_Fecha_SQL = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
							}
							if (Separador_Fecha_SQL != "")
							{
								break;
							}
						} 
						formato = formato.Substring(Strings.InStr(I + 1, formato, Separador_Fecha_SQL, CompareMethod.Binary)); 
						for (I = 1; I <= formato.Length; I++)
						{
							if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && Separador_Hora_SQL == "")
							{
								Separador_Hora_SQL = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
							}
							if (Separador_Hora_SQL != "")
							{
								break;
							}
						} 
						break;
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRr.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tRr.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("Infecciones Nosocomiales", Path.GetDirectoryName(Application.ExecutablePath), vgUsuario, "Form_Activate:InfNosocomiales.dll", ex);
			}
		}

		internal static void ObtenerLocale()
		{
			//'    Dim buffer As String * 100
			//'    Dim dl&
			//'
			//'    #If Win32 Then
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, buffer, 99)
			//'        lSep_Fecha = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, buffer, 99)
			//'        lSep_Hora = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, buffer, 99)
			//'        lFor_fecha = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT, buffer, 99)
			//'        LFOR_HORA = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICENTURY, buffer, 99)
			//'        lEsp_For_Siglo = LPSTRToVBString(buffer)
			//''        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ITLZERO, buffer, 99)
			//''        LNumero_Ceros = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_IDATE, buffer, 99)
			//'        iFormato_Fecha = LPSTRToVBString(buffer)
			//''        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ITIME, buffer, 99)
			//''        iFormato_Hora = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, buffer, 99)
			//'        lSep_Decimal = LPSTRToVBString(buffer)
			//'
			//'    #Else
			//'        Print " Not implemented under Win16"
			//'    #End If
			//' CURIAE - EQUIPO DE MIGRACION: NUEVA FORMA DE OBENER LA CONFIGURACION REGIONAL
		}

		//UPGRADE_NOTE: (7001) The following declaration (LPSTRToVBString) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string LPSTRToVBString(string s)
		//{
				//int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
				//if (nullpos > 0)
				//{
					//return s.Substring(0, Math.Min(nullpos - 1, s.Length));
				//}
				//else
				//{
					//return "";
				//}
		//}


		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************

			DataSet tRrCrystal = null;
			string tstCab = String.Empty;
			try
			{
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, cnConexion);
				tRrCrystal = new DataSet();
				tempAdapter.Fill(tRrCrystal);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, cnConexion);
				tRrCrystal = new DataSet();
				tempAdapter_2.Fill(tRrCrystal);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, cnConexion);
				tRrCrystal = new DataSet();
				tempAdapter_3.Fill(tRrCrystal);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tstCab, cnConexion);
				tRrCrystal = new DataSet();
				tempAdapter_4.Fill(tRrCrystal);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VstrCrystal.VstPoblaciHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrCrystal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tRrCrystal.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("Inf. Nonocomiales", Path.GetDirectoryName(Application.ExecutablePath), vgUsuario, "proCabCrystal:Nosocomiales.bas", ex);
			}

		}

		public static bool fImprimirInfeccionesNosocomiales(ref string stDesde, ref string stHasta)
		{
            //DGMORENOG_TODO_X_5 - PENDIENTE DEFINIR TRATAMIENTO DE BASE DATOS ACCES
            bool result = false;
            //object dbLangSpanish = null;
            //object DBEngine = null;
            //string stSql = String.Empty;
            //DataSet RR = null;
            ////UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //Database bsDatabase = null;
            ////UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //Recordset rsCursor = null;
            ////UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //string stPathTemp = String.Empty;
            //bool blnBase = false;
            //string stTabla = String.Empty;
            //string stNombrePac = String.Empty;
            //string loNombreReport = String.Empty;
            //PDFCreator.clsPDFCreator miObjPdf = null;
            //try
            //{


            //	if (!bDesdeInfMasivos)
            //	{
            //		Serrores.Obtener_Multilenguaje(cnConexion, stIdenPac);
            //	}
            //	else
            //	{
            //		Serrores.CampoIdioma = "IDIOMA0";
            //	}

            //	stPathTemp = PathAplicacion + "\\" + NombreBaseAccess;
            //	if (FileSystem.Dir(stPathTemp, FileAttribute.Normal) != "")
            //	{
            //		blnBase = true;
            //	}

            //	if (!blnBase)
            //	{ // SI est� dado
            //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		bsDatabase = (Database) DBEngine.Workspaces(0).CreateDatabase(stPathTemp, dbLangSpanish);
            //		blnBase = true;
            //	}
            //	else
            //	{
            //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		bsDatabase = (Database) DBEngine.Workspaces(0).OpenDatabase(stPathTemp);
            //		//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		foreach (TableDef tblTabla in bsDatabase.TableDefs)
            //		{
            //			//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			if (Convert.ToString(tblTabla.Name) == "INF_NOSOCOMIALE")
            //			{
            //				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //				bsDatabase.Execute("DROP TABLE INF_NOSOCOMIALE");
            //			}
            //		}
            //		blnBase = true;
            //	}

            //	//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	bsDatabase.Execute("CREATE TABLE INF_NOSOCOMIALE " + 
            //	                   "( Ingreso Text, Registro Text, Historia Text, NombrePac Text, " + 
            //	                   "PacQuirur Text(2), TipoInfec LONGTEXT, fCultivo Text, Germenes LONGTEXT, " + 
            //	                   "RealiAnti Text(2), TtoAntibio LONGTEXT, Curacion Text(2), Falleci Text(2), Consecuencia TEXT, Origen TEXT )");
            //	//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	bsDatabase.Close();

            //	switch(stTipoSer)
            //	{
            //		case "H" : 
            //			stSql = "SELECT faltplan, fllegada, gidenpac FROM AEPISADM where ganoadme = " + intAnioRegi.ToString() + " AND gnumadme = " + lngNumRegi.ToString(); 
            //			break;
            //		case "U" : 
            //			stSql = "SELECT faltaurg, fllegada, gidenpac FROM UEPISURG where ganourge = " + intAnioRegi.ToString() + " AND gnumurge = " + lngNumRegi.ToString(); 
            //			break;
            //		case "N" : 
            //			stSql = "SELECT faltnido, fregineo, gidenpac FROM NEPISNEO where ganourge = " + intAnioRegi.ToString() + " AND gnumurge = " + lngNumRegi.ToString(); 
            //			break;
            //	}
            //	SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
            //	RR = new DataSet();
            //	tempAdapter.Fill(RR);
            //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
            //	//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //	stTabla = (Convert.IsDBNull(RR.Tables[0].Rows[0][0])) ? "ENOSOCOM" : "ENOSOALT";
            //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
            //	stIngreso = Convert.ToDateTime(RR.Tables[0].Rows[0][1]).ToString("dd/MM/yyyy HH:mm");
            //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
            //	stIdenPac = Convert.ToString(RR.Tables[0].Rows[0][2]);
            //	//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //	RR.Close();

            //	stSql = "SELECT dape1pac, dape2pac, dnombpac, ghistoria " + 
            //	        " FROM DPACIENT " + 
            //	        " LEFT JOIN HDOSSIER ON HDOSSIER.gidenpac = DPACIENT.gidenpac AND HDOSSIER.icontenido='H'" + 
            //	        " WHERE DPACIENT.gidenpac ='" + stIdenPac + "'";
            //	SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, cnConexion);
            //	RR = new DataSet();
            //	tempAdapter_2.Fill(RR);
            //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
            //	stHistoria = Convert.ToString(RR.Tables[0].Rows[0]["ghistoria"]);
            //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
            //	stNombrePac = (Convert.ToString(RR.Tables[0].Rows[0]["dape1pac"]) + "").Trim() + " " + (Convert.ToString(RR.Tables[0].Rows[0]["dape2pac"]) + "").Trim() + ", " + (Convert.ToString(RR.Tables[0].Rows[0]["dnombpac"]) + "").Trim();
            //	//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //	RR.Close();

            //	//UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
            //	//UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
            //	object tempRefParam = stDesde;
            //	object tempRefParam2 = stHasta;
            //	stSql = "SELECT fapuntes, iquirurg, oinfecci, fcultivo, ogermene, " + 
            //	        "irealant, oantibio, icuracio, ifalleci, econsinf.dconsinf, iorigeni " + 
            //	        " From " + stTabla + 
            //	        " left join econsinf on " + stTabla + ".gconsinf = econsinf.gconsinf" + 
            //	        " WHERE itiposer = '" + stTipoSer + "' AND " + 
            //	        " ganoregi = " + intAnioRegi.ToString() + " AND " + 
            //	        " gnumregi = " + lngNumRegi.ToString() + " AND " + 
            //	        " fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam) + " AND " + 
            //	        " fapuntes <= " + Serrores.FormatFechaHMS(tempRefParam2) + 
            //	        " ORDER BY 1 DESC ";
            //	stHasta = Convert.ToString(tempRefParam2);
            //	stDesde = Convert.ToString(tempRefParam);
            //	SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, cnConexion);
            //	RR = new DataSet();
            //	tempAdapter_3.Fill(RR);
            //	if (RR.Tables[0].Rows.Count != 0)
            //	{
            //		result = true;
            //		foreach (DataRow iteration_row in RR.Tables[0].Rows)
            //		{

            //			//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			bsDatabase = (Database) DBEngine.Workspaces(0).OpenDatabase(stPathTemp);
            //			//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			bsDatabase.Execute("DELETE * FROM INF_NOSOCOMIALE");
            //			//Por cada registro de Infeccion Noscomial, se imprime la hoja de la infeccion

            //			//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			rsCursor = (Recordset) bsDatabase.OpenRecordset("SELECT * FROM INF_NOSOCOMIALE Where 1=2");
            //			//UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			rsCursor.AddNew();
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["Ingreso"] = (Recordset) stIngreso;
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Registro). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["Registro"] = (Recordset) Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:NN");
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["Historia"] = (Recordset) stHistoria;
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["NombrePac"] = (Recordset) stNombrePac;
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["iquirurg"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(PacQuirur). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				rsCursor["PacQuirur"] = (Recordset) ((Convert.ToString(iteration_row["iquirurg"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(TipoInfec). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["TipoInfec"] = (Recordset) (Convert.ToString(iteration_row["oinfecci"]) + "").Trim();
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(fCultivo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["fCultivo"] = (Recordset) Convert.ToDateTime(iteration_row["fcultivo"]).ToString("dd/MM/yyyy HH:NN");
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Germenes). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["Germenes"] = (Recordset) (Convert.ToString(iteration_row["ogermene"]) + "").Trim();
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["irealant"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(RealiAnti). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				rsCursor["RealiAnti"] = (Recordset) ((Convert.ToString(iteration_row["irealant"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(TtoAntibio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["TtoAntibio"] = (Recordset) (Convert.ToString(iteration_row["oantibio"]) + "").Trim();
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["icuracio"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Curacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				rsCursor["Curacion"] = (Recordset) ((Convert.ToString(iteration_row["icuracio"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //			if (!Convert.IsDBNull(iteration_row["ifalleci"]))
            //			{
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Falleci). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //				rsCursor["Falleci"] = (Recordset) ((Convert.ToString(iteration_row["ifalleci"]) == "S") ? "S�" : "No");
            //			}
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Consecuencia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
            //			rsCursor["Consecuencia"] = (Recordset) (Convert.ToString(iteration_row["dconsinf"]) + "").Trim();
            //			switch((Convert.ToString(iteration_row["iorigeni"]) + "").Trim().ToUpper())
            //			{
            //				case "I" : 
            //					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
            //					rsCursor["Origen"] = (Recordset) "Intrahospitalaria"; 
            //					break;
            //				case "E" : 
            //					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
            //					rsCursor["Origen"] = (Recordset) "Extrahospitalaria"; 
            //					break;
            //				default:
            //					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
            //					rsCursor["Origen"] = (Recordset) ""; 
            //					break;
            //			}
            //			//UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			rsCursor.Update();
            //			//UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			rsCursor.Close();
            //			//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			bsDatabase.Close();

            //			//UPGRADE_TODO: (1067) Member reportfilename is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.reportfilename = PathReport + "\\EGI2116R1.RPT";


            //			Serrores.LLenarFormulas(cnConexion, Listado_general, "EGI2116R1", "EGI2116R1");

            //			//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.SubreportToChange = "";

            //			proCabCrystal();

            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Formulas["Grupo"] = "\"" + VstrCrystal.VstGrupoHo + "\"";
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Formulas["DireccionH"] = "\"" + VstrCrystal.VstDireccHo + "\"";
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Formulas["NombreH"] = "\"" + VstrCrystal.VstNombreHo + "\"";
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Formulas["NomRPT"] = "\"" + "EGI2116R1" + "\"";

            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Formulas["LOGOIDC"] = "\"" + Serrores.ExisteLogo(cnConexion) + "\"";

            //			//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.DataFiles[0] = PathAplicacion + "\\" + NombreBaseAccess;

            //			//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.SubreportToChange = "LogotipoIDC";
            //			//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Connect = "DSN=" + NOMBRE_DSN_SQLSERVER + ";UID=" + gUsuario_Crystal + ";PWD=" + gContrase�a_Crystal;
            //			//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.SubreportToChange = "";

            //			//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Destination = 0;
            //			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.WindowTitle = "Registro de Infecciones Hospitalarias";
            //			//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.WindowState = (int) FormWindowState.Maximized;


            //			if (bDesdeInfMasivos && gGenerarPDFDesdeInformesMasivos && gRutaDirectorioPDF.Trim() != "")
            //			{
            //				loNombreReport = gIdProcesoMasivo + DateTime.Now.ToString("yyyyMMddHHmmss") + Serrores.fGET_HASH(5);
            //				//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //				Listado_general.Destination = 1;
            //				if (Serrores.fPropiedadesPDF(ref miObjPdf))
            //				{
            //					miObjPdf.set_cOption("AutosaveDirectory", gRutaDirectorioPDF); //carpeta a generar
            //					miObjPdf.set_cOption("AutosaveFilename", loNombreReport); //Guarda el nombre
            //					//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //					Listado_general.Action = 1;

            //					while(FileSystem.Dir(gRutaDirectorioPDF + "\\" + loNombreReport + ".pdf", FileAttribute.Normal) == "")
            //					{
            //						Application.DoEvents();
            //					};
            //					miObjPdf.cClose();
            //					miObjPdf = null;
            //				}
            //				else
            //				{
            //					ErroresNet = "No es posible generar el PDF en estos momentos.";
            //				}
            //			}
            //			else
            //			{
            //				//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //				Listado_general.Action = 1;
            //			}

            //			//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			Listado_general.Reset();


            //		}
            //	}
            //	//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //	RR.Close();
            //	//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	DBEngine.Workspaces(0).Close();
            //	return result;
            //}
            //catch (System.Exception excep)
            //{
            //	result = false;
            //	ErroresNet = "fImprimirInfeccionesNosocomiales - " + excep.Message;
            //return result;
            //}
            return result;
		}

		//UPGRADE_NOTE: (7001) The following declaration (fCargarDatosACCES) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool fCargarDatosACCES(DataSet RR)
		//{
				//bool result = false;
				//object DBEngine = null;
				//
				////UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				//
				//
				//result = true;
				//
				//
				//
				////UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//DBEngine.Workspaces(0).Close();
				//
				//return result;
		//}
	}
}