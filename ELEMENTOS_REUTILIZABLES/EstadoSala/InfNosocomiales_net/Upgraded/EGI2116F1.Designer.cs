using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InfNosocomiales
{
	partial class frmNosocomiales
	{

		#region "Upgrade Support "
		private static frmNosocomiales m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmNosocomiales DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmNosocomiales();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprNosocomiales", "cmdImpresion", "cbBuscar", "CheckTodos", "_OBApuntes_0", "_OBApuntes_1", "SdcFechaDesde", "_lblTextos_6", "_Frame1_1", "cbCerrar", "_optOrigen_0", "_optOrigen_1", "_Frame1_10", "cbbConsecuencia", "vaFarmacos", "_chkTodos_1", "_chkTodos_0", "cmbInsertar", "_txtDatos_2", "_Frame1_9", "_txtDatos_1", "_Frame1_8", "_txtDatos_0", "_Frame1_7", "_optFalle_1", "_optFalle_0", "_Frame1_4", "_optCuracion_0", "_optCuracion_1", "_Frame1_6", "_optRealizado_1", "_optRealizado_0", "_Frame1_5", "CbCancelar", "cbAceptar", "_ssdFechas_0", "_ssdFechas_1", "_optQuiru_1", "_optQuiru_0", "_Frame1_3", "_tbHora_0", "_tbHora_1", "_lblTextos_10", "_lblTextos_9", "_lblTextos_7", "_Frame1_2", "_lblTextos_5", "_lblTextos_4", "_lblTextos_3", "_lblTextos_2", "_lblTextos_1", "_lblTextos_0", "_Frame1_0", "lnUrgencias", "lbUrgencias", "_lblTextos_8", "_Line1_1", "_lblTextos_12", "_Line1_0", "Frame1", "Line1", "OBApuntes", "chkTodos", "lblTextos", "optCuracion", "optFalle", "optOrigen", "optQuiru", "optRealizado", "ssdFechas", "tbHora", "txtDatos", "ShapeContainer1", "commandButtonHelper1", "vaFarmacos_Sheet1", "sprNosocomiales_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprNosocomiales;
		public Telerik.WinControls.UI.RadButton cmdImpresion;
		public Telerik.WinControls.UI.RadButton cbBuscar;
		public Telerik.WinControls.UI.RadCheckBox CheckTodos;
		private Telerik.WinControls.UI.RadRadioButton _OBApuntes_0;
		private Telerik.WinControls.UI.RadRadioButton _OBApuntes_1;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaDesde;
		private Telerik.WinControls.UI.RadLabel _lblTextos_6;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_1;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		private Telerik.WinControls.UI.RadRadioButton _optOrigen_0;
		private Telerik.WinControls.UI.RadRadioButton _optOrigen_1;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_10;
		public UpgradeHelpers.MSForms.MSCombobox cbbConsecuencia;
		public UpgradeHelpers.Spread.FpSpread vaFarmacos;
		private Telerik.WinControls.UI.RadCheckBox _chkTodos_1;
		private Telerik.WinControls.UI.RadCheckBox _chkTodos_0;
		public Telerik.WinControls.UI.RadButton cmbInsertar;
		private Telerik.WinControls.UI.RadTextBoxControl _txtDatos_2;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_9;
		private Telerik.WinControls.UI.RadTextBoxControl _txtDatos_1;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_8;
		private Telerik.WinControls.UI.RadTextBoxControl _txtDatos_0;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_7;
		private Telerik.WinControls.UI.RadRadioButton _optFalle_1;
		private Telerik.WinControls.UI.RadRadioButton _optFalle_0;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_4;
		private Telerik.WinControls.UI.RadRadioButton _optCuracion_0;
		private Telerik.WinControls.UI.RadRadioButton _optCuracion_1;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_6;
		private Telerik.WinControls.UI.RadRadioButton _optRealizado_1;
		private Telerik.WinControls.UI.RadRadioButton _optRealizado_0;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_5;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		private Telerik.WinControls.UI.RadDateTimePicker _ssdFechas_0;
		private Telerik.WinControls.UI.RadDateTimePicker _ssdFechas_1;
		private Telerik.WinControls.UI.RadRadioButton _optQuiru_1;
		private Telerik.WinControls.UI.RadRadioButton _optQuiru_0;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_3;
		private Telerik.WinControls.UI.RadMaskedEditBox _tbHora_0;
		private Telerik.WinControls.UI.RadMaskedEditBox _tbHora_1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_10;
		private Telerik.WinControls.UI.RadLabel _lblTextos_9;
		private Telerik.WinControls.UI.RadLabel _lblTextos_7;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_2;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_5;
		private Telerik.WinControls.UI.RadLabel _lblTextos_4;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_3;
		private Telerik.WinControls.UI.RadLabel _lblTextos_2;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_0;
		private Telerik.WinControls.UI.RadGroupBox _Frame1_0;
		public Microsoft.VisualBasic.PowerPacks.LineShape lnUrgencias;
		public Telerik.WinControls.UI.RadLabel lbUrgencias;
		private Telerik.WinControls.UI.RadLabel _lblTextos_8;
		private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_12;
		private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_0;
		public Telerik.WinControls.UI.RadGroupBox[] Frame1 = new Telerik.WinControls.UI.RadGroupBox[11];
		public Microsoft.VisualBasic.PowerPacks.LineShape[] Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[2];
		public Telerik.WinControls.UI.RadRadioButton[] OBApuntes = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadCheckBox[] chkTodos = new Telerik.WinControls.UI.RadCheckBox[2];
		public Telerik.WinControls.UI.RadLabel[] lblTextos = new Telerik.WinControls.UI.RadLabel[13];
		public Telerik.WinControls.UI.RadRadioButton[] optCuracion = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] optFalle = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] optOrigen = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] optQuiru = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] optRealizado = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadDateTimePicker[] ssdFechas = new Telerik.WinControls.UI.RadDateTimePicker[2];
		public Telerik.WinControls.UI.RadMaskedEditBox[] tbHora = new Telerik.WinControls.UI.RadMaskedEditBox[2];
		public Telerik.WinControls.UI.RadTextBoxControl[] txtDatos = new Telerik.WinControls.UI.RadTextBoxControl[3];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNosocomiales));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.sprNosocomiales = new UpgradeHelpers.Spread.FpSpread();
			this.cmdImpresion = new Telerik.WinControls.UI.RadButton();
			this._Frame1_1 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbBuscar = new Telerik.WinControls.UI.RadButton();
			this.CheckTodos = new Telerik.WinControls.UI.RadCheckBox();
			this._OBApuntes_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._OBApuntes_1 = new Telerik.WinControls.UI.RadRadioButton();
			this.SdcFechaDesde = new Telerik.WinControls.UI.RadDateTimePicker();
			this._lblTextos_6 = new Telerik.WinControls.UI.RadLabel();
			this.cbCerrar = new Telerik.WinControls.UI.RadButton();
			this._Frame1_2 = new Telerik.WinControls.UI.RadGroupBox();
			this._Frame1_10 = new Telerik.WinControls.UI.RadGroupBox();
			this._optOrigen_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._optOrigen_1 = new Telerik.WinControls.UI.RadRadioButton();
			this.cbbConsecuencia = new UpgradeHelpers.MSForms.MSCombobox();
			this._Frame1_9 = new Telerik.WinControls.UI.RadGroupBox();
			this.vaFarmacos = new UpgradeHelpers.Spread.FpSpread();
			this._chkTodos_1 = new Telerik.WinControls.UI.RadCheckBox();
			this._chkTodos_0 = new Telerik.WinControls.UI.RadCheckBox();
			this.cmbInsertar = new Telerik.WinControls.UI.RadButton();
			this._txtDatos_2 = new Telerik.WinControls.UI.RadTextBoxControl();
			this._Frame1_8 = new Telerik.WinControls.UI.RadGroupBox();
			this._txtDatos_1 = new Telerik.WinControls.UI.RadTextBoxControl();
			this._Frame1_7 = new Telerik.WinControls.UI.RadGroupBox();
			this._txtDatos_0 = new Telerik.WinControls.UI.RadTextBoxControl();
			this._Frame1_4 = new Telerik.WinControls.UI.RadGroupBox();
			this._optFalle_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optFalle_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._Frame1_6 = new Telerik.WinControls.UI.RadGroupBox();
			this._optCuracion_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._optCuracion_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._Frame1_5 = new Telerik.WinControls.UI.RadGroupBox();
			this._optRealizado_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optRealizado_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this._ssdFechas_0 = new Telerik.WinControls.UI.RadDateTimePicker();
			this._ssdFechas_1 = new Telerik.WinControls.UI.RadDateTimePicker();
			this._Frame1_3 = new Telerik.WinControls.UI.RadGroupBox();
			this._optQuiru_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optQuiru_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._tbHora_0 = new Telerik.WinControls.UI.RadMaskedEditBox();
			this._tbHora_1 = new Telerik.WinControls.UI.RadMaskedEditBox();
			this._lblTextos_10 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_9 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_7 = new Telerik.WinControls.UI.RadLabel();
			this._Frame1_0 = new Telerik.WinControls.UI.RadGroupBox();
			this._lblTextos_5 = new Telerik.WinControls.UI.RadTextBox();
			this._lblTextos_4 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_3 = new Telerik.WinControls.UI.RadTextBox();
			this._lblTextos_2 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_1 = new Telerik.WinControls.UI.RadTextBox();
			this._lblTextos_0 = new Telerik.WinControls.UI.RadLabel();
			this.lnUrgencias = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.lbUrgencias = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_8 = new Telerik.WinControls.UI.RadLabel();
			this._Line1_1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this._lblTextos_12 = new Telerik.WinControls.UI.RadLabel();
			this._Line1_0 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this._Frame1_1.SuspendLayout();
			this._Frame1_2.SuspendLayout();
			this._Frame1_10.SuspendLayout();
			this._Frame1_9.SuspendLayout();
			this._Frame1_8.SuspendLayout();
			this._Frame1_7.SuspendLayout();
			this._Frame1_4.SuspendLayout();
			this._Frame1_6.SuspendLayout();
			this._Frame1_5.SuspendLayout();
			this._Frame1_3.SuspendLayout();
			this._Frame1_0.SuspendLayout();
			this.SuspendLayout();
			this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
			// 
			// ShapeContainer1
			// 
			this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
			this.ShapeContainer1.Size = new System.Drawing.Size(765, 581);
			this.ShapeContainer1.Shapes.Add(lnUrgencias);
			this.ShapeContainer1.Shapes.Add(_Line1_1);
			this.ShapeContainer1.Shapes.Add(_Line1_0);
			// 
			// sprNosocomiales
			// 
			this.sprNosocomiales.Location = new System.Drawing.Point(4, 80);
			this.sprNosocomiales.Name = "sprNosocomiales";
			this.sprNosocomiales.Size = new System.Drawing.Size(757, 110);
			this.sprNosocomiales.TabIndex = 5;
			this.sprNosocomiales.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprNosocomiales_CellClick);
			// 
			// cmdImpresion
			// 
			//this.cmdImpresion.BackColor = System.Drawing.SystemColors.Control;
			this.cmdImpresion.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmdImpresion.Enabled = false;
			//this.cmdImpresion.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdImpresion.Location = new System.Drawing.Point(590, 546);
			this.cmdImpresion.Name = "cmdImpresion";
			this.cmdImpresion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdImpresion.Size = new System.Drawing.Size(81, 29);
			this.cmdImpresion.TabIndex = 30;
			this.cmdImpresion.Text = "&Impresi�n";
			this.cmdImpresion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cmdImpresion.Click += new System.EventHandler(this.cmdImpresion_Click);
			// 
			// _Frame1_1
			// 
			//this._Frame1_1.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_1.Controls.Add(this.cbBuscar);
			this._Frame1_1.Controls.Add(this.CheckTodos);
			this._Frame1_1.Controls.Add(this._OBApuntes_0);
			this._Frame1_1.Controls.Add(this._OBApuntes_1);
			this._Frame1_1.Controls.Add(this.SdcFechaDesde);
			this._Frame1_1.Controls.Add(this._lblTextos_6);
			this._Frame1_1.Enabled = true;
			//this._Frame1_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_1.Location = new System.Drawing.Point(4, 42);
			this._Frame1_1.Name = "_Frame1_1";
			this._Frame1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_1.Size = new System.Drawing.Size(757, 35);
			this._Frame1_1.TabIndex = 38;
			this._Frame1_1.Text = "Apuntes";
			this._Frame1_1.Visible = true;
			// 
			// cbBuscar
			// 
			//this.cbBuscar.BackColor = System.Drawing.SystemColors.Control;
			this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbBuscar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbBuscar.Image = (System.Drawing.Image) resources.GetObject("cbBuscar.Image");
			this.cbBuscar.Location = new System.Drawing.Point(708, 8);
			this.cbBuscar.Name = "cbBuscar";
			this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBuscar.Size = new System.Drawing.Size(27, 24);
			this.cbBuscar.TabIndex = 4;
			this.cbBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbBuscar.UseVisualStyleBackColor = false;
			this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
			// 
			// CheckTodos
			// 
			//this.CheckTodos.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.CheckTodos.BackColor = System.Drawing.SystemColors.Control;
			this.CheckTodos.CausesValidation = true;
			this.CheckTodos.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.CheckTodos.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.CheckTodos.Cursor = System.Windows.Forms.Cursors.Default;
			this.CheckTodos.Enabled = true;
			//this.CheckTodos.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CheckTodos.Location = new System.Drawing.Point(644, 14);
			this.CheckTodos.Name = "CheckTodos";
			this.CheckTodos.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CheckTodos.Size = new System.Drawing.Size(53, 13);
			this.CheckTodos.TabIndex = 3;
			this.CheckTodos.TabStop = true;
			this.CheckTodos.Text = "Todos";
			this.CheckTodos.Visible = true;
			// 
			// _OBApuntes_0
			// 
			//this._OBApuntes_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._OBApuntes_0.BackColor = System.Drawing.SystemColors.Control;
			this._OBApuntes_0.CausesValidation = true;
			this._OBApuntes_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._OBApuntes_0.IsChecked = true;
			this._OBApuntes_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._OBApuntes_0.Enabled = true;
			//this._OBApuntes_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._OBApuntes_0.Location = new System.Drawing.Point(62, 14);
			this._OBApuntes_0.Name = "_OBApuntes_0";
			this._OBApuntes_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._OBApuntes_0.Size = new System.Drawing.Size(103, 13);
			this._OBApuntes_0.TabIndex = 0;
			this._OBApuntes_0.TabStop = true;
			this._OBApuntes_0.Text = "Activos";
			this._OBApuntes_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._OBApuntes_0.Visible = true;
			this._OBApuntes_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.OBApuntes_CheckedChanged);
			// 
			// _OBApuntes_1
			// 
			//this._OBApuntes_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._OBApuntes_1.BackColor = System.Drawing.SystemColors.Control;
			this._OBApuntes_1.CausesValidation = true;
			this._OBApuntes_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._OBApuntes_1.IsChecked = false;
			this._OBApuntes_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._OBApuntes_1.Enabled = true;
			//this._OBApuntes_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._OBApuntes_1.Location = new System.Drawing.Point(184, 14);
			this._OBApuntes_1.Name = "_OBApuntes_1";
			this._OBApuntes_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._OBApuntes_1.Size = new System.Drawing.Size(157, 13);
			this._OBApuntes_1.TabIndex = 1;
			this._OBApuntes_1.TabStop = true;
			this._OBApuntes_1.Text = "Activos e inactivos";
			this._OBApuntes_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._OBApuntes_1.Visible = true;
			this._OBApuntes_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.OBApuntes_CheckedChanged);
			// 
			// SdcFechaDesde
			// 
			//this.SdcFechaDesde.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SdcFechaDesde.CustomFormat = "DD/MM/YYYY";
			this.SdcFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaDesde.Location = new System.Drawing.Point(506, 11);
			this.SdcFechaDesde.Name = "SdcFechaDesde";
			this.SdcFechaDesde.Size = new System.Drawing.Size(119, 19);
			this.SdcFechaDesde.TabIndex = 2;
			// 
			// _lblTextos_6
			// 
			this._lblTextos_6.AutoSize = true;
			//this._lblTextos_6.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_6.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_6.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_6.Location = new System.Drawing.Point(462, 14);
			this._lblTextos_6.Name = "_lblTextos_6";
			this._lblTextos_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_6.Size = new System.Drawing.Size(44, 13);
			this._lblTextos_6.TabIndex = 45;
			this._lblTextos_6.Text = "Desde:";
			// 
			// cbCerrar
			// 
			//this.cbCerrar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCerrar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCerrar.Location = new System.Drawing.Point(678, 546);
			this.cbCerrar.Name = "cbCerrar";
			this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCerrar.Size = new System.Drawing.Size(81, 29);
			this.cbCerrar.TabIndex = 32;
			this.cbCerrar.Text = "&Cerrar";
			this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCerrar.UseVisualStyleBackColor = false;
			this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
			// 
			// _Frame1_2
			// 
			//this._Frame1_2.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_2.Controls.Add(this._Frame1_10);
			this._Frame1_2.Controls.Add(this.cbbConsecuencia);
			this._Frame1_2.Controls.Add(this._Frame1_9);
			this._Frame1_2.Controls.Add(this._Frame1_8);
			this._Frame1_2.Controls.Add(this._Frame1_7);
			this._Frame1_2.Controls.Add(this._Frame1_4);
			this._Frame1_2.Controls.Add(this._Frame1_6);
			this._Frame1_2.Controls.Add(this._Frame1_5);
			this._Frame1_2.Controls.Add(this.CbCancelar);
			this._Frame1_2.Controls.Add(this.cbAceptar);
			this._Frame1_2.Controls.Add(this._ssdFechas_0);
			this._Frame1_2.Controls.Add(this._ssdFechas_1);
			this._Frame1_2.Controls.Add(this._Frame1_3);
			this._Frame1_2.Controls.Add(this._tbHora_0);
			this._Frame1_2.Controls.Add(this._tbHora_1);
			this._Frame1_2.Controls.Add(this._lblTextos_10);
			this._Frame1_2.Controls.Add(this._lblTextos_9);
			this._Frame1_2.Controls.Add(this._lblTextos_7);
			this._Frame1_2.Enabled = true;
			//this._Frame1_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_2.Location = new System.Drawing.Point(0, 188);
			this._Frame1_2.Name = "_Frame1_2";
			this._Frame1_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_2.Size = new System.Drawing.Size(757, 343);
			this._Frame1_2.TabIndex = 37;
			this._Frame1_2.Visible = true;
			// 
			// _Frame1_10
			// 
			//this._Frame1_10.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_10.Controls.Add(this._optOrigen_0);
			this._Frame1_10.Controls.Add(this._optOrigen_1);
			this._Frame1_10.Enabled = true;
			//this._Frame1_10.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_10.Location = new System.Drawing.Point(520, 258);
			this._Frame1_10.Name = "_Frame1_10";
			this._Frame1_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_10.Size = new System.Drawing.Size(229, 39);
			this._Frame1_10.TabIndex = 55;
			this._Frame1_10.Text = "Origen";
			this._Frame1_10.Visible = true;
			// 
			// _optOrigen_0
			// 
			//this._optOrigen_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optOrigen_0.BackColor = System.Drawing.SystemColors.Control;
			this._optOrigen_0.CausesValidation = true;
			this._optOrigen_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrigen_0.IsChecked = false;
			this._optOrigen_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optOrigen_0.Enabled = true;
			//this._optOrigen_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optOrigen_0.Location = new System.Drawing.Point(12, 20);
			this._optOrigen_0.Name = "_optOrigen_0";
			this._optOrigen_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optOrigen_0.Size = new System.Drawing.Size(101, 13);
			this._optOrigen_0.TabIndex = 25;
			this._optOrigen_0.TabStop = true;
			this._optOrigen_0.Text = "Intrahospitalaria";
			this._optOrigen_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrigen_0.Visible = true;
			this._optOrigen_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optOrigen_CheckedChanged);
			// 
			// _optOrigen_1
			// 
			//this._optOrigen_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optOrigen_1.BackColor = System.Drawing.SystemColors.Control;
			this._optOrigen_1.CausesValidation = true;
			this._optOrigen_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrigen_1.IsChecked = false;
			this._optOrigen_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optOrigen_1.Enabled = true;
			//this._optOrigen_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optOrigen_1.Location = new System.Drawing.Point(116, 20);
			this._optOrigen_1.Name = "_optOrigen_1";
			this._optOrigen_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optOrigen_1.Size = new System.Drawing.Size(97, 13);
			this._optOrigen_1.TabIndex = 26;
			this._optOrigen_1.TabStop = true;
			this._optOrigen_1.Text = "Extrahospitalaria";
			this._optOrigen_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrigen_1.Visible = true;
			this._optOrigen_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optOrigen_CheckedChanged);
			// 
			// cbbConsecuencia
			// 
			this.cbbConsecuencia.BackColor = System.Drawing.SystemColors.Window;
			this.cbbConsecuencia.CausesValidation = true;
			this.cbbConsecuencia.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbbConsecuencia.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.cbbConsecuencia.Enabled = true;
			this.cbbConsecuencia.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.cbbConsecuencia.IntegralHeight = true;
			this.cbbConsecuencia.Location = new System.Drawing.Point(96, 308);
			this.cbbConsecuencia.Name = "cbbConsecuencia";
			this.cbbConsecuencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbbConsecuencia.Size = new System.Drawing.Size(284, 21);
			//this.cbbConsecuencia.Sorted = false;
			this.cbbConsecuencia.TabIndex = 27;
			this.cbbConsecuencia.TabStop = true;
			this.cbbConsecuencia.Visible = true;
			// 
			// _Frame1_9
			// 
			//this._Frame1_9.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_9.Controls.Add(this.vaFarmacos);
			this._Frame1_9.Controls.Add(this._chkTodos_1);
			this._Frame1_9.Controls.Add(this._chkTodos_0);
			this._Frame1_9.Controls.Add(this.cmbInsertar);
			this._Frame1_9.Controls.Add(this._txtDatos_2);
			this._Frame1_9.Enabled = true;
			//this._Frame1_9.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_9.Location = new System.Drawing.Point(12, 142);
			this._Frame1_9.Name = "_Frame1_9";
			this._Frame1_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_9.Size = new System.Drawing.Size(735, 107);
			this._Frame1_9.TabIndex = 51;
			this._Frame1_9.Text = "Tratamiento Antibi�tico";
			this._Frame1_9.Visible = true;
			// 
			// vaFarmacos
			// 
			this.vaFarmacos.Location = new System.Drawing.Point(402, 16);
			this.vaFarmacos.Name = "vaFarmacos";
			this.vaFarmacos.Size = new System.Drawing.Size(327, 85);
			this.vaFarmacos.TabIndex = 16;
			// 
			// _chkTodos_1
			// 
			//this._chkTodos_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._chkTodos_1.BackColor = System.Drawing.SystemColors.Control;
			this._chkTodos_1.CausesValidation = true;
			this._chkTodos_1.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this._chkTodos_1.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chkTodos_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._chkTodos_1.Enabled = true;
			//this._chkTodos_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chkTodos_1.Location = new System.Drawing.Point(344, 78);
			this._chkTodos_1.Name = "_chkTodos_1";
			this._chkTodos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chkTodos_1.Size = new System.Drawing.Size(51, 13);
			this._chkTodos_1.TabIndex = 15;
			this._chkTodos_1.TabStop = true;
			this._chkTodos_1.Text = "Todos";
			this._chkTodos_1.Visible = true;
			this._chkTodos_1.CheckStateChanged += new System.EventHandler(this.chkTodos_CheckStateChanged);
			// 
			// _chkTodos_0
			// 
			//this._chkTodos_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._chkTodos_0.BackColor = System.Drawing.SystemColors.Control;
			this._chkTodos_0.CausesValidation = true;
			this._chkTodos_0.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this._chkTodos_0.CheckState = System.Windows.Forms.CheckState.Checked;
			this._chkTodos_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._chkTodos_0.Enabled = true;
			//this._chkTodos_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chkTodos_0.Location = new System.Drawing.Point(334, 56);
			this._chkTodos_0.Name = "_chkTodos_0";
			this._chkTodos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chkTodos_0.Size = new System.Drawing.Size(61, 13);
			this._chkTodos_0.TabIndex = 14;
			this._chkTodos_0.TabStop = true;
			this._chkTodos_0.Text = "Ninguno";
			this._chkTodos_0.Visible = true;
			this._chkTodos_0.CheckStateChanged += new System.EventHandler(this.chkTodos_CheckStateChanged);
			// 
			// cmbInsertar
			// 
			//this.cmbInsertar.BackColor = System.Drawing.SystemColors.Control;
			this.cmbInsertar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmbInsertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			//this.cmbInsertar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmbInsertar.Location = new System.Drawing.Point(352, 18);
			this.cmbInsertar.Name = "cmbInsertar";
			this.cmbInsertar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmbInsertar.Size = new System.Drawing.Size(29, 23);
			this.cmbInsertar.TabIndex = 13;
			this.cmbInsertar.Text = "<";
			this.cmbInsertar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cmbInsertar.UseVisualStyleBackColor = false;
			this.cmbInsertar.Click += new System.EventHandler(this.cmbInsertar_Click);
			// 
			// _txtDatos_2
			// 
			this._txtDatos_2.AcceptsReturn = true;
			//this._txtDatos_2.BackColor = System.Drawing.SystemColors.Window;
			//this._txtDatos_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._txtDatos_2.Cursor = System.Windows.Forms.Cursors.IBeam;
			//this._txtDatos_2.ForeColor = System.Drawing.SystemColors.WindowText;
			this._txtDatos_2.Location = new System.Drawing.Point(6, 16);
			this._txtDatos_2.MaxLength = 1000;
			this._txtDatos_2.Multiline = true;
			this._txtDatos_2.Name = "_txtDatos_2";
			this._txtDatos_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._txtDatos_2.AutoScroll = true;
			this._txtDatos_2.Size = new System.Drawing.Size(327, 85);
			this._txtDatos_2.TabIndex = 12;
			this._txtDatos_2.TextChanged += new System.EventHandler(this.txtDatos_TextChanged);
			// 
			// _Frame1_8
			// 
			//this._Frame1_8.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_8.Controls.Add(this._txtDatos_1);
			this._Frame1_8.Enabled = true;
			//this._Frame1_8.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_8.Location = new System.Drawing.Point(381, 40);
			this._Frame1_8.Name = "_Frame1_8";
			this._Frame1_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_8.Size = new System.Drawing.Size(365, 101);
			this._Frame1_8.TabIndex = 50;
			this._Frame1_8.Text = "Germen(es) Aislados";
			this._Frame1_8.Visible = true;
			// 
			// _txtDatos_1
			// 
			this._txtDatos_1.AcceptsReturn = true;
			//this._txtDatos_1.BackColor = System.Drawing.SystemColors.Window;
			//this._txtDatos_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._txtDatos_1.Cursor = System.Windows.Forms.Cursors.IBeam;
			//this._txtDatos_1.ForeColor = System.Drawing.SystemColors.WindowText;
			this._txtDatos_1.Location = new System.Drawing.Point(6, 18);
			this._txtDatos_1.MaxLength = 255;
			this._txtDatos_1.Multiline = true;
			this._txtDatos_1.Name = "_txtDatos_1";
			this._txtDatos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._txtDatos_1.AutoScroll = true;
			this._txtDatos_1.Size = new System.Drawing.Size(351, 77);
			this._txtDatos_1.TabIndex = 11;
			this._txtDatos_1.TextChanged += new System.EventHandler(this.txtDatos_TextChanged);
			// 
			// _Frame1_7
			// 
			//this._Frame1_7.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_7.Controls.Add(this._txtDatos_0);
			this._Frame1_7.Enabled = true;
			//this._Frame1_7.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_7.Location = new System.Drawing.Point(12, 40);
			this._Frame1_7.Name = "_Frame1_7";
			this._Frame1_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_7.Size = new System.Drawing.Size(365, 101);
			this._Frame1_7.TabIndex = 49;
			this._Frame1_7.Text = "Tipo de Infecci�n";
			this._Frame1_7.Visible = true;
			// 
			// _txtDatos_0
			// 
			this._txtDatos_0.AcceptsReturn = true;
			//this._txtDatos_0.BackColor = System.Drawing.SystemColors.Window;
			//this._txtDatos_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._txtDatos_0.Cursor = System.Windows.Forms.Cursors.IBeam;
			//this._txtDatos_0.ForeColor = System.Drawing.SystemColors.WindowText;
			this._txtDatos_0.Location = new System.Drawing.Point(6, 18);
			this._txtDatos_0.MaxLength = 255;
			this._txtDatos_0.Multiline = true;
			this._txtDatos_0.Name = "_txtDatos_0";
			this._txtDatos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._txtDatos_0.AutoScroll = true;
			this._txtDatos_0.Size = new System.Drawing.Size(351, 77);
			this._txtDatos_0.TabIndex = 10;
			this._txtDatos_0.TextChanged += new System.EventHandler(this.txtDatos_TextChanged);
			// 
			// _Frame1_4
			// 
			//this._Frame1_4.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_4.Controls.Add(this._optFalle_1);
			this._Frame1_4.Controls.Add(this._optFalle_0);
			this._Frame1_4.Enabled = true;
			//this._Frame1_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_4.Location = new System.Drawing.Point(386, 258);
			this._Frame1_4.Name = "_Frame1_4";
			this._Frame1_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_4.Size = new System.Drawing.Size(129, 39);
			this._Frame1_4.TabIndex = 35;
			this._Frame1_4.Text = "Fallecimiento Infecci�n";
			this._Frame1_4.Visible = true;
			// 
			// _optFalle_1
			// 
			//this._optFalle_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optFalle_1.BackColor = System.Drawing.SystemColors.Control;
			this._optFalle_1.CausesValidation = true;
			this._optFalle_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optFalle_1.IsChecked = false;
			this._optFalle_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optFalle_1.Enabled = true;
			//this._optFalle_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optFalle_1.Location = new System.Drawing.Point(72, 20);
			this._optFalle_1.Name = "_optFalle_1";
			this._optFalle_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optFalle_1.Size = new System.Drawing.Size(41, 13);
			this._optFalle_1.TabIndex = 24;
			this._optFalle_1.TabStop = true;
			this._optFalle_1.Text = "No";
			this._optFalle_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optFalle_1.Visible = true;
			this._optFalle_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optFalle_CheckedChanged);
			// 
			// _optFalle_0
			// 
			//this._optFalle_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optFalle_0.BackColor = System.Drawing.SystemColors.Control;
			this._optFalle_0.CausesValidation = true;
			this._optFalle_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optFalle_0.IsChecked = false;
			this._optFalle_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optFalle_0.Enabled = true;
			//this._optFalle_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optFalle_0.Location = new System.Drawing.Point(20, 20);
			this._optFalle_0.Name = "_optFalle_0";
			this._optFalle_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optFalle_0.Size = new System.Drawing.Size(41, 13);
			this._optFalle_0.TabIndex = 23;
			this._optFalle_0.TabStop = true;
			this._optFalle_0.Text = "S�";
			this._optFalle_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optFalle_0.Visible = true;
			this._optFalle_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optFalle_CheckedChanged);
			// 
			// _Frame1_6
			// 
			//this._Frame1_6.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_6.Controls.Add(this._optCuracion_0);
			this._Frame1_6.Controls.Add(this._optCuracion_1);
			this._Frame1_6.Enabled = true;
			//this._Frame1_6.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_6.Location = new System.Drawing.Point(268, 258);
			this._Frame1_6.Name = "_Frame1_6";
			this._Frame1_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_6.Size = new System.Drawing.Size(113, 39);
			this._Frame1_6.TabIndex = 34;
			this._Frame1_6.Text = "Curaci�n al Alta";
			this._Frame1_6.Visible = true;
			// 
			// _optCuracion_0
			// 
			//this._optCuracion_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optCuracion_0.BackColor = System.Drawing.SystemColors.Control;
			this._optCuracion_0.CausesValidation = true;
			this._optCuracion_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optCuracion_0.IsChecked = false;
			this._optCuracion_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optCuracion_0.Enabled = true;
			//this._optCuracion_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optCuracion_0.Location = new System.Drawing.Point(8, 18);
			this._optCuracion_0.Name = "_optCuracion_0";
			this._optCuracion_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optCuracion_0.Size = new System.Drawing.Size(39, 15);
			this._optCuracion_0.TabIndex = 21;
			this._optCuracion_0.TabStop = true;
			this._optCuracion_0.Text = "S�";
			this._optCuracion_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optCuracion_0.Visible = true;
			this._optCuracion_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optCuracion_CheckedChanged);
			// 
			// _optCuracion_1
			// 
			//this._optCuracion_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optCuracion_1.BackColor = System.Drawing.SystemColors.Control;
			this._optCuracion_1.CausesValidation = true;
			this._optCuracion_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optCuracion_1.IsChecked = false;
			this._optCuracion_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optCuracion_1.Enabled = true;
			//this._optCuracion_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optCuracion_1.Location = new System.Drawing.Point(50, 18);
			this._optCuracion_1.Name = "_optCuracion_1";
			this._optCuracion_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optCuracion_1.Size = new System.Drawing.Size(43, 15);
			this._optCuracion_1.TabIndex = 22;
			this._optCuracion_1.TabStop = true;
			this._optCuracion_1.Text = "No";
			this._optCuracion_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optCuracion_1.Visible = true;
			this._optCuracion_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optCuracion_CheckedChanged);
			// 
			// _Frame1_5
			// 
			//this._Frame1_5.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_5.Controls.Add(this._optRealizado_1);
			this._Frame1_5.Controls.Add(this._optRealizado_0);
			this._Frame1_5.Enabled = true;
			//this._Frame1_5.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_5.Location = new System.Drawing.Point(134, 258);
			this._Frame1_5.Name = "_Frame1_5";
			this._Frame1_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_5.Size = new System.Drawing.Size(133, 39);
			this._Frame1_5.TabIndex = 33;
			this._Frame1_5.Text = "Realizado Antibiograma";
			this._Frame1_5.Visible = true;
			// 
			// _optRealizado_1
			// 
			//this._optRealizado_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optRealizado_1.BackColor = System.Drawing.SystemColors.Control;
			this._optRealizado_1.CausesValidation = true;
			this._optRealizado_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optRealizado_1.IsChecked = false;
			this._optRealizado_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optRealizado_1.Enabled = true;
			//this._optRealizado_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optRealizado_1.Location = new System.Drawing.Point(68, 18);
			this._optRealizado_1.Name = "_optRealizado_1";
			this._optRealizado_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optRealizado_1.Size = new System.Drawing.Size(43, 15);
			this._optRealizado_1.TabIndex = 20;
			this._optRealizado_1.TabStop = true;
			this._optRealizado_1.Text = "No";
			this._optRealizado_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optRealizado_1.Visible = true;
			this._optRealizado_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optRealizado_CheckedChanged);
			// 
			// _optRealizado_0
			// 
			//this._optRealizado_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optRealizado_0.BackColor = System.Drawing.SystemColors.Control;
			this._optRealizado_0.CausesValidation = true;
			this._optRealizado_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optRealizado_0.IsChecked = false;
			this._optRealizado_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optRealizado_0.Enabled = true;
			//this._optRealizado_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optRealizado_0.Location = new System.Drawing.Point(18, 18);
			this._optRealizado_0.Name = "_optRealizado_0";
			this._optRealizado_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optRealizado_0.Size = new System.Drawing.Size(43, 15);
			this._optRealizado_0.TabIndex = 19;
			this._optRealizado_0.TabStop = true;
			this._optRealizado_0.Text = "S�";
			this._optRealizado_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optRealizado_0.Visible = true;
			this._optRealizado_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optRealizado_CheckedChanged);
			// 
			// CbCancelar
			// 
			//this.CbCancelar.BackColor = System.Drawing.SystemColors.Control;
			this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbCancelar.Location = new System.Drawing.Point(666, 308);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 29);
			this.CbCancelar.TabIndex = 29;
			this.CbCancelar.Text = "&Cancelar";
			this.CbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbCancelar.UseVisualStyleBackColor = false;
			this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
			// 
			// cbAceptar
			// 
			//this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(580, 308);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 28;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbAceptar.UseVisualStyleBackColor = false;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// _ssdFechas_0
			// 
			//this._ssdFechas_0.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._ssdFechas_0.CustomFormat = "DD/MM/YYYY";
			this._ssdFechas_0.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this._ssdFechas_0.Location = new System.Drawing.Point(142, 16);
			this._ssdFechas_0.Name = "_ssdFechas_0";
			this._ssdFechas_0.Size = new System.Drawing.Size(119, 19);
			this._ssdFechas_0.TabIndex = 6;
			this._ssdFechas_0.Click += new System.EventHandler(this.ssdFechas_Click);
			// 
			// _ssdFechas_1
			// 
			//this._ssdFechas_1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._ssdFechas_1.CustomFormat = "DD/MM/YYYY";
			this._ssdFechas_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this._ssdFechas_1.Location = new System.Drawing.Point(578, 16);
			this._ssdFechas_1.Name = "_ssdFechas_1";
			this._ssdFechas_1.Size = new System.Drawing.Size(119, 19);
			this._ssdFechas_1.TabIndex = 8;
			this._ssdFechas_1.Click += new System.EventHandler(this.ssdFechas_Click);
			// 
			// _Frame1_3
			// 
			//this._Frame1_3.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_3.Controls.Add(this._optQuiru_1);
			this._Frame1_3.Controls.Add(this._optQuiru_0);
			this._Frame1_3.Enabled = true;
			//this._Frame1_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_3.Location = new System.Drawing.Point(10, 258);
			this._Frame1_3.Name = "_Frame1_3";
			this._Frame1_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_3.Size = new System.Drawing.Size(123, 39);
			this._Frame1_3.TabIndex = 31;
			this._Frame1_3.Text = "Paciente Quir�rgico";
			this._Frame1_3.Visible = true;
			// 
			// _optQuiru_1
			// 
			//this._optQuiru_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optQuiru_1.BackColor = System.Drawing.SystemColors.Control;
			this._optQuiru_1.CausesValidation = true;
			this._optQuiru_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optQuiru_1.IsChecked = false;
			this._optQuiru_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optQuiru_1.Enabled = true;
			//this._optQuiru_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optQuiru_1.Location = new System.Drawing.Point(66, 20);
			this._optQuiru_1.Name = "_optQuiru_1";
			this._optQuiru_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optQuiru_1.Size = new System.Drawing.Size(39, 13);
			this._optQuiru_1.TabIndex = 18;
			this._optQuiru_1.TabStop = true;
			this._optQuiru_1.Text = "No";
			this._optQuiru_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optQuiru_1.Visible = true;
			this._optQuiru_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optQuiru_CheckedChanged);
			// 
			// _optQuiru_0
			// 
			//this._optQuiru_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optQuiru_0.BackColor = System.Drawing.SystemColors.Control;
			this._optQuiru_0.CausesValidation = true;
			this._optQuiru_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optQuiru_0.IsChecked = false;
			this._optQuiru_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optQuiru_0.Enabled = true;
			//this._optQuiru_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optQuiru_0.Location = new System.Drawing.Point(16, 20);
			this._optQuiru_0.Name = "_optQuiru_0";
			this._optQuiru_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optQuiru_0.Size = new System.Drawing.Size(39, 13);
			this._optQuiru_0.TabIndex = 17;
			this._optQuiru_0.TabStop = true;
			this._optQuiru_0.Text = "S�";
			this._optQuiru_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optQuiru_0.Visible = true;
			this._optQuiru_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optQuiru_CheckedChanged);
			// 
			// _tbHora_0
			// 
			this._tbHora_0.AllowPromptAsInput = false;
			this._tbHora_0.Location = new System.Drawing.Point(268, 16);
			this._tbHora_0.Mask = "##:##";
			this._tbHora_0.Name = "_tbHora_0";
			this._tbHora_0.PromptChar = ' ';
			this._tbHora_0.Size = new System.Drawing.Size(42, 19);
			this._tbHora_0.TabIndex = 7;
			// 
			// _tbHora_1
			// 
			this._tbHora_1.AllowPromptAsInput = false;
			this._tbHora_1.Location = new System.Drawing.Point(704, 16);
			this._tbHora_1.Mask = "##:##";
			this._tbHora_1.Name = "_tbHora_1";
			this._tbHora_1.PromptChar = ' ';
			this._tbHora_1.Size = new System.Drawing.Size(42, 19);
			this._tbHora_1.TabIndex = 9;
			// 
			// _lblTextos_10
			// 
			//this._lblTextos_10.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_10.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_10.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_10.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_10.Location = new System.Drawing.Point(12, 312);
			this._lblTextos_10.Name = "_lblTextos_10";
			this._lblTextos_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_10.Size = new System.Drawing.Size(75, 17);
			this._lblTextos_10.TabIndex = 54;
			this._lblTextos_10.Text = "Consecuencia:";
			// 
			// _lblTextos_9
			// 
			this._lblTextos_9.AutoSize = true;
			//this._lblTextos_9.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_9.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_9.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_9.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_9.Location = new System.Drawing.Point(454, 19);
			this._lblTextos_9.Name = "_lblTextos_9";
			this._lblTextos_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_9.Size = new System.Drawing.Size(119, 13);
			this._lblTextos_9.TabIndex = 47;
			this._lblTextos_9.Text = "Fecha y Hora del Cultivo:";
			// 
			// _lblTextos_7
			// 
			this._lblTextos_7.AutoSize = true;
			//this._lblTextos_7.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_7.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_7.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_7.Location = new System.Drawing.Point(12, 18);
			this._lblTextos_7.Name = "_lblTextos_7";
			this._lblTextos_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_7.Size = new System.Drawing.Size(126, 13);
			this._lblTextos_7.TabIndex = 46;
			this._lblTextos_7.Text = "Fecha y Hora del Registro:";
			// 
			// _Frame1_0
			// 
			//this._Frame1_0.BackColor = System.Drawing.SystemColors.Control;
			this._Frame1_0.Controls.Add(this._lblTextos_5);
			this._Frame1_0.Controls.Add(this._lblTextos_4);
			this._Frame1_0.Controls.Add(this._lblTextos_3);
			this._Frame1_0.Controls.Add(this._lblTextos_2);
			this._Frame1_0.Controls.Add(this._lblTextos_1);
			this._Frame1_0.Controls.Add(this._lblTextos_0);
			this._Frame1_0.Enabled = true;
			//this._Frame1_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Frame1_0.Location = new System.Drawing.Point(4, -2);
			this._Frame1_0.Name = "_Frame1_0";
			this._Frame1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Frame1_0.Size = new System.Drawing.Size(757, 43);
			this._Frame1_0.TabIndex = 36;
			this._Frame1_0.Visible = true;
			// 
			// _lblTextos_5
			// 
			//this._lblTextos_5.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_5.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_5.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_5.Location = new System.Drawing.Point(630, 16);
			this._lblTextos_5.Name = "_lblTextos_5";
			this._lblTextos_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_5.Size = new System.Drawing.Size(113, 17);
			this._lblTextos_5.TabIndex = 44;
			this._lblTextos_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._lblTextos_5.Enabled = false;
            // 
            // _lblTextos_4
            // 
            this._lblTextos_4.AutoSize = true;
			//this._lblTextos_4.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_4.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_4.Location = new System.Drawing.Point(554, 16);
			this._lblTextos_4.Name = "_lblTextos_4";
			this._lblTextos_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_4.Size = new System.Drawing.Size(71, 17);
			this._lblTextos_4.TabIndex = 43;
			this._lblTextos_4.Text = "Fecha Ingreso:";
			// 
			// _lblTextos_3
			// 
			//this._lblTextos_3.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_3.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_3.Location = new System.Drawing.Point(448, 16);
			this._lblTextos_3.Name = "_lblTextos_3";
			this._lblTextos_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_3.Size = new System.Drawing.Size(89, 17);
			this._lblTextos_3.TabIndex = 42;
            this._lblTextos_3.Enabled = false;
            // 
            // _lblTextos_2
            // 
            this._lblTextos_2.AutoSize = true;
			//this._lblTextos_2.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_2.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_2.Location = new System.Drawing.Point(402, 16);
			this._lblTextos_2.Name = "_lblTextos_2";
			this._lblTextos_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_2.Size = new System.Drawing.Size(38, 17);
			this._lblTextos_2.TabIndex = 41;
			this._lblTextos_2.Text = "Historia:";
			// 
			// _lblTextos_1
			// 
			//this._lblTextos_1.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_1.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_1.Location = new System.Drawing.Point(62, 16);
			this._lblTextos_1.Name = "_lblTextos_1";
			this._lblTextos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_1.Size = new System.Drawing.Size(321, 17);
			this._lblTextos_1.TabIndex = 40;
            this._lblTextos_1.Enabled = false;
            // 
            // _lblTextos_0
            // 
            this._lblTextos_0.AutoSize = true;
			//this._lblTextos_0.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_0.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_0.Location = new System.Drawing.Point(12, 16);
			this._lblTextos_0.Name = "_lblTextos_0";
			this._lblTextos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_0.Size = new System.Drawing.Size(45, 17);
			this._lblTextos_0.TabIndex = 39;
			this._lblTextos_0.Text = "Paciente:";
			// 
			// lnUrgencias
			// 
			//this.lnUrgencias.BorderColor = System.Drawing.Color.Fuchsia;
			this.lnUrgencias.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.lnUrgencias.BorderWidth = 4;
			this.lnUrgencias.Enabled = false;
			this.lnUrgencias.Name = "lnUrgencias";
			this.lnUrgencias.Visible = true;
			this.lnUrgencias.X1 = (int) 12;
			this.lnUrgencias.X2 = (int) 44;
			this.lnUrgencias.Y1 = (int) 572;
			this.lnUrgencias.Y2 = (int) 572;
			// 
			// lbUrgencias
			// 
			//this.lbUrgencias.BackColor = System.Drawing.SystemColors.Control;
			//this.lbUrgencias.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbUrgencias.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbUrgencias.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbUrgencias.Location = new System.Drawing.Point(52, 564);
			this.lbUrgencias.Name = "lbUrgencias";
			this.lbUrgencias.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbUrgencias.Size = new System.Drawing.Size(129, 13);
			this.lbUrgencias.TabIndex = 53;
			this.lbUrgencias.Text = "Apuntes de H.d�a";
			// 
			// _lblTextos_8
			// 
			this._lblTextos_8.AutoSize = true;
			//this._lblTextos_8.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_8.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_8.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_8.ForeColor = System.Drawing.Color.Black;
			this._lblTextos_8.Location = new System.Drawing.Point(52, 534);
			this._lblTextos_8.Name = "_lblTextos_8";
			this._lblTextos_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_8.Size = new System.Drawing.Size(80, 13);
			this._lblTextos_8.TabIndex = 52;
			this._lblTextos_8.Text = "Apuntes Activos.";
			// 
			// _Line1_1
			// 
			this._Line1_1.BorderColor = System.Drawing.Color.Black;
			this._Line1_1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this._Line1_1.BorderWidth = 4;
			this._Line1_1.Enabled = false;
			this._Line1_1.Name = "_Line1_1";
			this._Line1_1.Visible = true;
			this._Line1_1.X1 = (int) 12;
			this._Line1_1.X2 = (int) 44;
			this._Line1_1.Y1 = (int) 540;
			this._Line1_1.Y2 = (int) 540;
			// 
			// _lblTextos_12
			// 
			this._lblTextos_12.AutoSize = true;
			//this._lblTextos_12.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_12.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_12.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_12.ForeColor = System.Drawing.Color.Black;
			this._lblTextos_12.Location = new System.Drawing.Point(52, 548);
			this._lblTextos_12.Name = "_lblTextos_12";
			this._lblTextos_12.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_12.Size = new System.Drawing.Size(88, 13);
			this._lblTextos_12.TabIndex = 48;
			this._lblTextos_12.Text = "Apuntes Inactivos.";
			// 
			// _Line1_0
			// 
			//this._Line1_0.BorderColor = System.Drawing.Color.Gray;
			this._Line1_0.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this._Line1_0.BorderWidth = 4;
			this._Line1_0.Enabled = false;
			this._Line1_0.Name = "_Line1_0";
			this._Line1_0.Visible = true;
			this._Line1_0.X1 = (int) 12;
			this._Line1_0.X2 = (int) 44;
			this._Line1_0.Y1 = (int) 556;
			this._Line1_0.Y2 = (int) 556;
			// 
			// frmNosocomiales
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(765, 581);
			this.Controls.Add(this.sprNosocomiales);
			this.Controls.Add(this.cmdImpresion);
			this.Controls.Add(this._Frame1_1);
			this.Controls.Add(this.cbCerrar);
			this.Controls.Add(this._Frame1_2);
			this.Controls.Add(this._Frame1_0);
			this.Controls.Add(this.lbUrgencias);
			this.Controls.Add(this._lblTextos_8);
			this.Controls.Add(this._lblTextos_12);
			this.Controls.Add(ShapeContainer1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(11, 64);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmNosocomiales";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Registro de infecciones nosocomiales.";
			//commandButtonHelper1.SetStyle(this.cbBuscar, 1);
			this.Closed += new System.EventHandler(this.frmNosocomiales_Closed);
			this.Load += new System.EventHandler(this.frmNosocomiales_Load);
			this._Frame1_1.ResumeLayout(false);
			this._Frame1_2.ResumeLayout(false);
			this._Frame1_10.ResumeLayout(false);
			this._Frame1_9.ResumeLayout(false);
			this._Frame1_8.ResumeLayout(false);
			this._Frame1_7.ResumeLayout(false);
			this._Frame1_4.ResumeLayout(false);
			this._Frame1_6.ResumeLayout(false);
			this._Frame1_5.ResumeLayout(false);
			this._Frame1_3.ResumeLayout(false);
			this._Frame1_0.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializetxtDatos();
			InitializetbHora();
			InitializessdFechas();
			InitializeoptRealizado();
			InitializeoptQuiru();
			InitializeoptOrigen();
			InitializeoptFalle();
			InitializeoptCuracion();
			InitializelblTextos();
			InitializechkTodos();
			InitializeOBApuntes();
			InitializeLine1();
			InitializeFrame1();
		}
		void InitializetxtDatos()
		{
			this.txtDatos = new Telerik.WinControls.UI.RadTextBoxControl[3];
			this.txtDatos[2] = _txtDatos_2;
			this.txtDatos[1] = _txtDatos_1;
			this.txtDatos[0] = _txtDatos_0;
		}
		void InitializetbHora()
		{
			this.tbHora = new Telerik.WinControls.UI.RadMaskedEditBox[2];
			this.tbHora[0] = _tbHora_0;
			this.tbHora[1] = _tbHora_1;
		}
		void InitializessdFechas()
		{
			this.ssdFechas = new Telerik.WinControls.UI.RadDateTimePicker[2];
			this.ssdFechas[0] = _ssdFechas_0;
			this.ssdFechas[1] = _ssdFechas_1;
		}
		void InitializeoptRealizado()
		{
			this.optRealizado = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optRealizado[1] = _optRealizado_1;
			this.optRealizado[0] = _optRealizado_0;
		}
		void InitializeoptQuiru()
		{
			this.optQuiru = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optQuiru[1] = _optQuiru_1;
			this.optQuiru[0] = _optQuiru_0;
		}
		void InitializeoptOrigen()
		{
			this.optOrigen = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optOrigen[0] = _optOrigen_0;
			this.optOrigen[1] = _optOrigen_1;
		}
		void InitializeoptFalle()
		{
			this.optFalle = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optFalle[1] = _optFalle_1;
			this.optFalle[0] = _optFalle_0;
		}
		void InitializeoptCuracion()
		{
			this.optCuracion = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optCuracion[0] = _optCuracion_0;
			this.optCuracion[1] = _optCuracion_1;
		}
		void InitializelblTextos()
		{
			this.lblTextos = new Telerik.WinControls.UI.RadLabel[13];
			this.lblTextos[6] = _lblTextos_6;
			this.lblTextos[10] = _lblTextos_10;
			this.lblTextos[9] = _lblTextos_9;
			this.lblTextos[7] = _lblTextos_7;
			//this.lblTextos[5] = _lblTextos_5;
			this.lblTextos[4] = _lblTextos_4;
			//this.lblTextos[3] = _lblTextos_3;
			this.lblTextos[2] = _lblTextos_2;
			//this.lblTextos[1] = _lblTextos_1;
			this.lblTextos[0] = _lblTextos_0;
			this.lblTextos[8] = _lblTextos_8;
			this.lblTextos[12] = _lblTextos_12;
		}
		void InitializechkTodos()
		{
			this.chkTodos = new Telerik.WinControls.UI.RadCheckBox[2];
			this.chkTodos[1] = _chkTodos_1;
			this.chkTodos[0] = _chkTodos_0;
		}
		void InitializeOBApuntes()
		{
			this.OBApuntes = new Telerik.WinControls.UI.RadRadioButton[2];
			this.OBApuntes[0] = _OBApuntes_0;
			this.OBApuntes[1] = _OBApuntes_1;
		}
		void InitializeLine1()
		{
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[2];
			this.Line1[1] = _Line1_1;
			this.Line1[0] = _Line1_0;
		}
		void InitializeFrame1()
		{
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox[11];
			this.Frame1[1] = _Frame1_1;
			this.Frame1[10] = _Frame1_10;
			this.Frame1[9] = _Frame1_9;
			this.Frame1[8] = _Frame1_8;
			this.Frame1[7] = _Frame1_7;
			this.Frame1[4] = _Frame1_4;
			this.Frame1[6] = _Frame1_6;
			this.Frame1[5] = _Frame1_5;
			this.Frame1[3] = _Frame1_3;
			this.Frame1[2] = _Frame1_2;
			this.Frame1[0] = _Frame1_0;
		}
		#endregion
	}
}