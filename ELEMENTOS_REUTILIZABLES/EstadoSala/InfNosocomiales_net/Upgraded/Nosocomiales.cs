using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace InfNosocomiales
{
	public class MCNosocomiales
	{


		public void Llamada(object nForm, MCNosocomiales nClase, SqlConnection nConnet, string pGidenPac, string pTipoSer, int pAnoReg, int pNumReg, string pstPaciente, string pstHistoria, string pstIngreso, string usuario, object ObjetoCristal, string stPathAplicacion, string stConexionSQL, string pNombreBaseAccess, string Usuario_C, string Contrase�a_C)
		{
			BNosocomiales.gUsuario_Crystal = Usuario_C;
			BNosocomiales.gContrase�a_Crystal = Contrase�a_C;
			BNosocomiales.PathAplicacion = stPathAplicacion;
			BNosocomiales.NombreBaseAccess = pNombreBaseAccess;
			BNosocomiales.NOMBRE_DSN_SQLSERVER = stConexionSQL;

			BNosocomiales.cnConexion = nConnet;
			BNosocomiales.Listado_general = ObjetoCristal;
			Serrores.AjustarRelojCliente(BNosocomiales.cnConexion);

			//gstConexionAccess = stConexionAccess
			//CrearCadenaDSN NOMBRE_DSN_SQLSERVER, NOMBRE_DSN_ACCESS


			BNosocomiales.stTipoSer = pTipoSer;
			BNosocomiales.intAnioRegi = pAnoReg;
			BNosocomiales.lngNumRegi = pNumReg;
			BNosocomiales.vgUsuario = usuario;
			BNosocomiales.stPaciente = pstPaciente;
			BNosocomiales.stHistoria = pstHistoria;
			BNosocomiales.stIngreso = pstIngreso;
			BNosocomiales.stIdenPac = pGidenPac;

			frmNosocomiales.DefInstance.ShowDialog();

		}

		public MCNosocomiales()
		{
			try
			{
				BNosocomiales.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{
				RadMessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}


		~MCNosocomiales()
		{
			BNosocomiales.clase_mensaje = null;
		}

		//Funcion: fImprimirInfecciones
		//Proposito: Imprimir una hoja por cada infeccion nosocomial del episodio recibido
		//retorno:   true--> Si se han imprimido datos
		//           false--> Si no se han imprimo datos u error
		public bool fImprimirInfecciones(SqlConnection nConnet, object ObjetoCristal, string pTipoSer, int pAnoReg, int pNumReg, ref string stDesde, ref string stHasta, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C, bool pDesdeInfMasivos, bool bGenerarPDFDesdeInformesMasivos, string IdProcesoMasivo, string stRutaTempPDF, ref string descripError)
		{

			bool result = false;
			BNosocomiales.cnConexion = nConnet;
			BNosocomiales.Listado_general = ObjetoCristal;
			BNosocomiales.ErroresNet = "";
			BNosocomiales.gUsuario_Crystal = Usuario_C;
			BNosocomiales.gContrase�a_Crystal = Contrase�a_C;
			BNosocomiales.PathAplicacion = stPathAplicacion;
			BNosocomiales.PathReport = Path.GetDirectoryName(Application.ExecutablePath) + "\\RPT";
			BNosocomiales.NombreBaseAccess = pNombreBaseAccess;
			BNosocomiales.NOMBRE_DSN_SQLSERVER = stConexionSQL;

			BNosocomiales.bDesdeInfMasivos = pDesdeInfMasivos;
			BNosocomiales.gGenerarPDFDesdeInformesMasivos = bGenerarPDFDesdeInformesMasivos;
			BNosocomiales.gIdProcesoMasivo = IdProcesoMasivo;
			BNosocomiales.gRutaDirectorioPDF = stRutaTempPDF;

			BNosocomiales.stTipoSer = pTipoSer;
			BNosocomiales.intAnioRegi = pAnoReg;
			BNosocomiales.lngNumRegi = pNumReg;

			result = BNosocomiales.fImprimirInfeccionesNosocomiales(ref stDesde, ref stHasta);
			descripError = BNosocomiales.ErroresNet;
			return result;
		}

		public bool fImprimirInfecciones(SqlConnection nConnet, object ObjetoCristal, string pTipoSer, int pAnoReg, int pNumReg, ref string stDesde, ref string stHasta, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C, bool pDesdeInfMasivos, bool bGenerarPDFDesdeInformesMasivos, string IdProcesoMasivo, string stRutaTempPDF)
		{
			string tempRefParam = "";
			return fImprimirInfecciones(nConnet, ObjetoCristal, pTipoSer, pAnoReg, pNumReg, ref stDesde, ref stHasta, stPathAplicacion, pNombreBaseAccess, stConexionSQL, Usuario_C, Contrase�a_C, pDesdeInfMasivos, bGenerarPDFDesdeInformesMasivos, IdProcesoMasivo, stRutaTempPDF, ref tempRefParam);
		}

		public bool fImprimirInfecciones(SqlConnection nConnet, object ObjetoCristal, string pTipoSer, int pAnoReg, int pNumReg, ref string stDesde, ref string stHasta, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C, bool pDesdeInfMasivos, bool bGenerarPDFDesdeInformesMasivos, string IdProcesoMasivo)
		{
			string tempRefParam2 = "";
			return fImprimirInfecciones(nConnet, ObjetoCristal, pTipoSer, pAnoReg, pNumReg, ref stDesde, ref stHasta, stPathAplicacion, pNombreBaseAccess, stConexionSQL, Usuario_C, Contrase�a_C, pDesdeInfMasivos, bGenerarPDFDesdeInformesMasivos, IdProcesoMasivo, "", ref tempRefParam2);
		}

		public bool fImprimirInfecciones(SqlConnection nConnet, object ObjetoCristal, string pTipoSer, int pAnoReg, int pNumReg, ref string stDesde, ref string stHasta, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C, bool pDesdeInfMasivos, bool bGenerarPDFDesdeInformesMasivos)
		{
			string tempRefParam3 = "";
			return fImprimirInfecciones(nConnet, ObjetoCristal, pTipoSer, pAnoReg, pNumReg, ref stDesde, ref stHasta, stPathAplicacion, pNombreBaseAccess, stConexionSQL, Usuario_C, Contrase�a_C, pDesdeInfMasivos, bGenerarPDFDesdeInformesMasivos, "", "", ref tempRefParam3);
		}

		public bool fImprimirInfecciones(SqlConnection nConnet, object ObjetoCristal, string pTipoSer, int pAnoReg, int pNumReg, ref string stDesde, ref string stHasta, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C, bool pDesdeInfMasivos)
		{
			string tempRefParam4 = "";
			return fImprimirInfecciones(nConnet, ObjetoCristal, pTipoSer, pAnoReg, pNumReg, ref stDesde, ref stHasta, stPathAplicacion, pNombreBaseAccess, stConexionSQL, Usuario_C, Contrase�a_C, pDesdeInfMasivos, false, "", "", ref tempRefParam4);
		}

		public bool fImprimirInfecciones(SqlConnection nConnet, object ObjetoCristal, string pTipoSer, int pAnoReg, int pNumReg, ref string stDesde, ref string stHasta, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C)
		{
			string tempRefParam5 = "";
			return fImprimirInfecciones(nConnet, ObjetoCristal, pTipoSer, pAnoReg, pNumReg, ref stDesde, ref stHasta, stPathAplicacion, pNombreBaseAccess, stConexionSQL, Usuario_C, Contrase�a_C, false, false, "", "", ref tempRefParam5);
		}

		//Funcion: proListadoInfecciones
		//Proposito: Listado de Infecciones Nosocomiales
		public void proListadoInfecciones(SqlConnection nConnet, string pTipoSer, object ObjetoCristal, object ObjetoCommonDialog, string stPathAplicacion, string pNombreBaseAccess, string stConexionSQL, string Usuario_C, string Contrase�a_C)
		{
			BNosocomiales.cnConexion = nConnet;
			BNosocomiales.Listado_general = ObjetoCristal;
			BNosocomiales.fPantalla = ObjetoCommonDialog;
			BNosocomiales.stTipoSer = pTipoSer;
			BNosocomiales.gUsuario_Crystal = Usuario_C;
			BNosocomiales.gContrase�a_Crystal = Contrase�a_C;
			BNosocomiales.NOMBRE_DSN_SQLSERVER = stConexionSQL;
			BNosocomiales.NombreBaseAccess = pNombreBaseAccess;
			BNosocomiales.PathReport = Path.GetDirectoryName(Application.ExecutablePath) + "\\RPT";
			BNosocomiales.PathAplicacion = stPathAplicacion;
			EGI2117F1.DefInstance.ShowDialog();
		}
	}
}