using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace InfNosocomiales
{
	public partial class frmNosocomiales
		: RadForm
	{

		// Declaro las variables de columnas del grid
		private const byte Col_FecRegig = 1;
		private const byte Col_TipInfec = 2;
		private const byte Col_FecCulti = 3;
		private const byte Col_Germenes = 4;
		private const byte Col_Farmacos = 5;
		private const byte Col_Quirurgi = 6;
		private const byte Col_RealAnti = 7;
		private const byte Col_CuraInfe = 8;
		private const byte Col_Fallecim = 9;
		private const byte Col_codConsecuencia = 10;
		private const byte Col_desConsecuencia = 11;
		private const byte Col_Origen = 12;
		private const byte col_DesOrigen = 13;
		private const byte Col_CodUsuar = 14;
		private const byte Col_DesUsuar = 15;
		private const byte Col_ConSegundos = 16;

		private const byte Col_MaxCol = 16;

		//Variables del grid de farmacos
		private const byte Far_Select = 1;
		private const byte Far_Farmaco = 2;
		private const byte Far_MaxCol = 2;


		bool blnInicializa = false;
		bool blnModifi = false;
		bool blnActiva = false;
		string stAntibiotico = String.Empty;
		bool blnChk = false;
		bool blnBuscar = false;
		string stSegundos = String.Empty;
		bool blnAusente = false;
		const int COLOR_APUNTES_CERRADOS = 0xC0C0C0;

		string stFechaLLegada = String.Empty;
		public frmNosocomiales()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}



		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			bool ErrAceptar = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{
				ErrAceptar = true;

				if (fValidaDatos())
				{
					object tempRefParam = ssdFechas[0].Text + " " + tbHora[0].Text + ":" + ((stSegundos.Trim() == "") ? "00" : stSegundos);
					stSql = "SELECT * FROM ENOSOCOM " + 
					        "WHERE itiposer = '" + BNosocomiales.stTipoSer + "' AND " + 
					        "ganoregi = " + BNosocomiales.intAnioRegi.ToString() + " AND " + 
					        "gnumregi = " + BNosocomiales.lngNumRegi.ToString() + "AND " + 
					        "fapuntes = " + Serrores.FormatFechaHMS(tempRefParam);
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BNosocomiales.cnConexion);
					rdoTemp = new DataSet();
					tempAdapter.Fill(rdoTemp);
					if (!blnModifi)
					{
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						rdoTemp.AddNew();
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["itiposer"] = BNosocomiales.stTipoSer;
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["ganoregi"] = BNosocomiales.intAnioRegi;
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["gnumregi"] = BNosocomiales.lngNumRegi;
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(ssdFechas[0].Text + " " + tbHora[0].Text + ":" + DateTime.Now.Second.ToString());
					}
					else
					{
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						rdoTemp.Edit();
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["iquirurg"] = (optQuiru[0].IsChecked) ? "S" : ((optQuiru[1].IsChecked) ? "N" : "");
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["oinfecci"] = txtDatos[0].Text.Trim();
					if (ssdFechas[1].Text != "")
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["fcultivo"] = ssdFechas[1].Text + " " + tbHora[1].Text;
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["ogermene"] = txtDatos[1].Text.Trim() + "";
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["irealant"] = (optRealizado[0].IsChecked) ? "S" : ((optRealizado[1].IsChecked) ? "N" : "");
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["oantibio"] = txtDatos[2].Text.Trim() + "";
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["icuracio"] = (optCuracion[0].IsChecked) ? "S" : ((optCuracion[1].IsChecked) ? "N" : "");
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["ifalleci"] = (optFalle[0].IsChecked) ? "S" : ((optFalle[1].IsChecked) ? "N" : "");
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["iorigeni"] = (optOrigen[0].IsChecked) ? "I" : ((optOrigen[1].IsChecked) ? "E" : "");
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["gUsuario"] = BNosocomiales.vgUsuario;
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["fmecaniz"] = DateTime.Now;
					if (cbbConsecuencia.SelectedIndex > 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["gconsinf"] = cbbConsecuencia.GetItemData(cbbConsecuencia.SelectedIndex);
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						rdoTemp.Tables[0].Rows[0]["gconsinf"] = DBNull.Value;
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					string tempQuery = rdoTemp.Tables[0].TableName;
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					tempAdapter_2.Update(rdoTemp, rdoTemp.Tables[0].TableName);
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					rdoTemp.Close();

					CargaGrid();
					InicializaControles();
					ActiDesaBoton(false);
					cmdImpresion.Enabled = false;
				}
				ErrAceptar = false;
			}
			catch (Exception excep)
			{
				if (!ErrAceptar)
				{
					throw excep;
				}

				if (ErrAceptar)
				{

					RadMessageBox.Show(excep.Message, this.Text + " - cbAceptar", MessageBoxButtons.OK, RadMessageIcon.Error);
				}
			}
		}

		private void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			blnBuscar = true;
			CargaGrid();
			InicializaControles();
			ActiDesaBoton(false);
			//blnBuscar = False
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			InicializaControles();
			txtDatos[0].Enabled = true;
			ssdFechas[0].Enabled = true;
			tbHora[0].Enabled = true;
			ssdFechas[1].Enabled = true;
			tbHora[1].Enabled = true;
			txtDatos[1].Enabled = true;
			txtDatos[2].Enabled = true;
			vaFarmacos.Enabled = true;
			optCuracion[0].Enabled = true;
			optFalle[0].Enabled = true;
			optQuiru[0].Enabled = true;
			optOrigen[0].Enabled = true;
			optRealizado[0].Enabled = true;
			optCuracion[1].Enabled = true;
			optFalle[1].Enabled = true;
			optQuiru[1].Enabled = true;
			optOrigen[1].Enabled = true;
			optRealizado[1].Enabled = true;
			chkTodos[0].Enabled = true;
			chkTodos[1].Enabled = true;
			cbbConsecuencia.Enabled = true;
			ActiDesaBoton(true);
			cmbInsertar.Enabled = true;
			cmdImpresion.Enabled = false;
			ssdFechas[0].Value = DateTime.Today;
			CargaGrid();
		}

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}


		private void cmbInsertar_Click(Object eventSender, EventArgs eventArgs)
		{
			int varExis = 0;

			string stMensaje = "";

			int tempForVar = vaFarmacos.MaxRows;
			for (int intContador = 1; intContador <= tempForVar; intContador++)
			{
				vaFarmacos.Row = intContador;
				vaFarmacos.Col = Far_Select;
				if (StringsHelper.ToDoubleSafe(Convert.ToString(vaFarmacos.Value)) == 1)
				{
					vaFarmacos.Col = Far_Farmaco;
					varExis = (txtDatos[2].Text.IndexOf(vaFarmacos.Text) + 1);
					if (varExis == 0)
					{
						txtDatos[2].Text = txtDatos[2].Text + vaFarmacos.Text + "; " + Environment.NewLine;
					}
					else
					{
						stMensaje = stMensaje + "- El f�rmaco " + vaFarmacos.Text.Trim() + " est� incorporado en el tratamiento antibi�tico, no ser� incorporado." + Environment.NewLine + Environment.NewLine;
					}
					vaFarmacos.Col = Far_Select;
					vaFarmacos.Value = 0;
				}
			}

			if (stMensaje.Trim() != "")
			{
				RadMessageBox.Show(stMensaje, "Errores al incorporar farmacos.", MessageBoxButtons.OK, RadMessageIcon.Info);
			}
		}

		private void cmdImpresion_Click(Object eventSender, EventArgs eventArgs)
		{

			if (fCargarDatosACCES())
			{

				//UPGRADE_TODO: (1067) Member reportfilename is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.reportfilename = BNosocomiales.PathAplicacion + "\\..\\elementoscomunes\\rpt\\EGI2116R1.RPT";

				Serrores.Obtener_Multilenguaje(BNosocomiales.cnConexion, BNosocomiales.stIdenPac);
				Serrores.LLenarFormulas(BNosocomiales.cnConexion, BNosocomiales.Listado_general, "EGI2116R1", "EGI2116R1");

				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.SubreportToChange = "";

				BNosocomiales.proCabCrystal();

				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//BNosocomiales.Listado_general.Formulas[0] = "Grupo = \"" + BNosocomiales.VstrCrystal.VstGrupoHo + "\"";
                BNosocomiales.Listado_general.Formulas["Grupo"] = "\"" + BNosocomiales.VstrCrystal.VstGrupoHo + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //BNosocomiales.Listado_general.Formulas[1] = "DireccionH = \"" + BNosocomiales.VstrCrystal.VstDireccHo + "\"";
                BNosocomiales.Listado_general.Formulas["DireccionH"] = "\"" + BNosocomiales.VstrCrystal.VstDireccHo + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //BNosocomiales.Listado_general.Formulas[2] = "NombreH= \"" + BNosocomiales.VstrCrystal.VstNombreHo + "\"";
                BNosocomiales.Listado_general.Formulas["NombreH"] = "\"" + BNosocomiales.VstrCrystal.VstNombreHo + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                BNosocomiales.Listado_general.Formulas["NomRPT"] = "\"" + "EGI2116R1" + "\"";



				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.Formulas["LOGOIDC"] = "\"" + Serrores.ExisteLogo(BNosocomiales.cnConexion) + "\"";

				//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.DataFiles[0] = BNosocomiales.PathAplicacion + "\\" + BNosocomiales.NombreBaseAccess;

				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.SubreportToChange = "LogotipoIDC";
				//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.Connect = "DSN=" + BNosocomiales.NOMBRE_DSN_SQLSERVER + ";UID=" + BNosocomiales.gUsuario_Crystal + ";PWD=" + BNosocomiales.gContrase�a_Crystal;
				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.SubreportToChange = "";

				//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.Destination = 0;
				//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.WindowState = (int) FormWindowState.Maximized;
				//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.Action = 1;
				//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				BNosocomiales.Listado_general.Reset();
			}


		}

		private void chkTodos_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			int index = Array.IndexOf(this.chkTodos, eventSender);
			if (!blnChk && chkTodos[index].CheckState == CheckState.Checked)
			{
				if (index == 0)
				{
					if (chkTodos[0].CheckState == CheckState.Checked)
					{
						blnChk = true;
						chkTodos[1].CheckState = CheckState.Unchecked;
						blnChk = false;
					}
				}
				else
				{
					if (chkTodos[1].CheckState == CheckState.Checked)
					{
						blnChk = true;
						chkTodos[0].CheckState = CheckState.Unchecked;
						blnChk = false;
					}
				}

				vaFarmacos.Col = Far_Select;
				int tempForVar = vaFarmacos.MaxRows;
				for (int intContador = 1; intContador <= tempForVar; intContador++)
				{
					vaFarmacos.Row = intContador;
					vaFarmacos.Value = index;
				}
			}
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void frmNosocomiales_Load(Object eventSender, EventArgs eventArgs)
		{
			bool ErrFromLoad = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;


			try
			{
				ErrFromLoad = true;

                //para la ayuda                
                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

                //DGMORENOG_TODO_X_5 - PENDIENTE REVISAR
                //this.setHelpContextID(154020);

                blnAusente = false;

				SdcFechaDesde.Text = "";
				
                //DGMORENOG_TODO_5_5 - VERIFICAR
				//SdcFechaDesde.setDateSeparator(BNosocomiales.lSep_Fecha);

                SdcFechaDesde.CustomFormat = BNosocomiales.lFor_fecha;

                //DGMORENOG_TODO_5_5 - VERIFICAR
                //SdcFechaDesde.setNullDateLabel("__" + BNosocomiales.lSep_Fecha + "__" + BNosocomiales.lSep_Fecha + "____");

                //DGMORENOG_TODO_5_5 - VERIFICAR
                //SdcFechaDesde.setShowCentury(true);

                SdcFechaDesde.MinDate = DateTime.Parse("01/01/1900");
				SdcFechaDesde.MaxDate = DateTime.Today;
				stSql = "SELECT ISNULL(NNUMERI1,0) FROM SCONSGLO WHERE GCONSGLO='NDIASENF'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BNosocomiales.cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					SdcFechaDesde.Value = DateTime.Today.AddDays(-1 * Convert.ToDouble(rdoTemp.Tables[0].Rows[0][0]));
				}
				else
				{
					SdcFechaDesde.Value = DateTime.Today;
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();


				sprNosocomiales.MaxRows = 0;

				lblTextos[1].Text = BNosocomiales.stPaciente;
				lblTextos[3].Text = BNosocomiales.stHistoria;

				blnModifi = false;
				this.Top = (int) 0;
				this.Left = (int) 0;

				BNosocomiales.ObtenerLocale();
				BNosocomiales.ObtenerValoresTipoBD();
				Serrores.Obtener_TipoBD_FormatoFechaBD(ref BNosocomiales.cnConexion);
				BNosocomiales.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(BNosocomiales.cnConexion);

				BNosocomiales.stDecimalBD = Serrores.ObtenerSeparadorDecimalBD(BNosocomiales.cnConexion);


				//Line1.BorderColor = COLOR_APUNTES_CERRADOS

				switch(BNosocomiales.stTipoSer)
				{
					case "H" : 
						stSql = "SELECT fllegada, gmotause " + 
						        "FROM AEPISADM " + 
						        "WHERE ganoadme = " + BNosocomiales.intAnioRegi.ToString() + " AND gnumadme = " + BNosocomiales.lngNumRegi.ToString(); 
						 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, BNosocomiales.cnConexion); 
						rdoTemp = new DataSet(); 
						tempAdapter_2.Fill(rdoTemp); 
						 
						if (rdoTemp.Tables[0].Rows.Count != 0)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							ssdFechas[0].MinDate = DateTime.Parse(Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy"));
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							ssdFechas[1].MinDate = DateTime.Parse(Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy"));
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							BNosocomiales.stIngreso = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:NN");
							lblTextos[5].Text = BNosocomiales.stIngreso;
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gmotause"]))
							{
								blnAusente = true;
							}
							//Oscar C Diciembre 2011
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							stFechaLLegada = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm:ss");
							//-----------------
						} 
						 
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
						rdoTemp.Close(); 
						 
						break;
					case "U" : 
						//Oscar C Diciembre 2011 
						stSql = "SELECT fllegada  " + 
						        "FROM UEPISURG " + 
						        "WHERE ganourge = " + BNosocomiales.intAnioRegi.ToString() + " AND gnumurge = " + BNosocomiales.lngNumRegi.ToString(); 
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, BNosocomiales.cnConexion); 
						rdoTemp = new DataSet(); 
						tempAdapter_3.Fill(rdoTemp); 
						if (rdoTemp.Tables[0].Rows.Count != 0)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							stFechaLLegada = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm:ss");
						} 
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
						rdoTemp.Close(); 
						//----------------- 
						break;
				}

				CabeceraGrid();

				ActiDesaBoton(false);

				CargaGrid();

				InicializaControles();

				CargaFarmacos();
				CargaConsecuencias();

				if (blnAusente)
				{
					Frame1[2].Enabled = false;
				}


				ErrFromLoad = false;
			}
			catch (Exception excep)
			{
				if (!ErrFromLoad)
				{
					throw excep;
				}

				if (ErrFromLoad)
				{

					RadMessageBox.Show(excep.Message, this.Text.Trim() + " / From_Load.", MessageBoxButtons.OK, RadMessageIcon.Error);

				}
			}
		}


		private void CabeceraGrid()
		{

			sprNosocomiales.MaxCols = Col_MaxCol;
			sprNosocomiales.MaxRows = 0;
			sprNosocomiales.Row = 0;

			sprNosocomiales.Col = Col_FecRegig;
			sprNosocomiales.Text = "Fecha" + "\r" + "Registro";
			sprNosocomiales.SetColWidth(Col_FecRegig, 1900);

			sprNosocomiales.Col = Col_TipInfec;
			sprNosocomiales.Text = "Tipo" + "\r" + "Infecci�n";
			sprNosocomiales.SetColWidth(Col_TipInfec, 4000);

			sprNosocomiales.Col = Col_FecCulti;
			sprNosocomiales.Text = "Fecha" + "\r" + "del Cultivo";
			sprNosocomiales.SetColWidth(Col_FecCulti, 1900);

			sprNosocomiales.Col = Col_Germenes;
			sprNosocomiales.Text = "Germen(es)" + "\r" + "Aislados";
			sprNosocomiales.SetColWidth(Col_Germenes, 3500);

			sprNosocomiales.Col = Col_Farmacos;
			sprNosocomiales.Text = "Tratamiento" + "\r" + "Antibi�tico";
			sprNosocomiales.SetColWidth(Col_Farmacos, 5000);

			sprNosocomiales.Col = Col_Quirurgi;
			sprNosocomiales.Text = "Paciente" + "\r" + "Quir�rgico";
			sprNosocomiales.SetColWidth(Col_Quirurgi, 1400);

			sprNosocomiales.Col = Col_RealAnti;
			sprNosocomiales.Text = "Realizado" + "\r" + "Antibiograma";
			sprNosocomiales.SetColWidth(Col_RealAnti, 1500);

			sprNosocomiales.Col = Col_CuraInfe;
			sprNosocomiales.Text = "Curaci�n de" + "\r" + "la Infecci�n";
			sprNosocomiales.SetColWidth(Col_CuraInfe, 1500);

			sprNosocomiales.Col = Col_Fallecim;
			sprNosocomiales.Text = "Fallecimiento" + "\r" + "por Infecci�n";
			sprNosocomiales.SetColWidth(Col_Fallecim, 1600);

			sprNosocomiales.Col = Col_CodUsuar;
			sprNosocomiales.Text = "C�digo Usuario";
			sprNosocomiales.SetColWidth(Col_CodUsuar, 1800);

			sprNosocomiales.Col = Col_DesUsuar;
			sprNosocomiales.Text = "Usuario";
			sprNosocomiales.SetColWidth(Col_DesUsuar, 5000);

			sprNosocomiales.Col = Col_ConSegundos;
			sprNosocomiales.Text = "Con Segundos";
			sprNosocomiales.SetColWidth(Col_ConSegundos, 3000);
			sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);
			// Farmacos

			sprNosocomiales.Col = Col_desConsecuencia;
			sprNosocomiales.Text = "Consecuencia";
			sprNosocomiales.SetColWidth(Col_desConsecuencia, 4000);

			sprNosocomiales.Col = Col_codConsecuencia;
			sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);

			sprNosocomiales.Col = Col_Origen;
			sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);

			sprNosocomiales.Col = col_DesOrigen;
			sprNosocomiales.Text = "Origen";
			sprNosocomiales.SetColWidth(col_DesOrigen, 3000);

			vaFarmacos.MaxCols = Far_MaxCol;
			vaFarmacos.MaxRows = 0;
			vaFarmacos.Row = 0;

			vaFarmacos.Col = Far_Select;
			vaFarmacos.Text = " ";
			vaFarmacos.SetColWidth(Far_Select, 450);

			vaFarmacos.Col = Far_Farmaco;
			vaFarmacos.Text = "F�rmaco";
			vaFarmacos.SetColWidth(Far_Farmaco, 4150);
		}

		private void frmNosocomiales_Closed(Object eventSender, EventArgs eventArgs)
		{
			BNosocomiales.fPantalla = null;
			BNosocomiales.clase_mensaje = null;
		}

		private void CargaGrid()
		{
			bool ErrCargaGrid = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;
			int lngContador = 0;
			Color lngColor = new Color();

			try
			{
				ErrCargaGrid = true;

				sprNosocomiales.MaxRows = 0;

				stSql = "SELECT ENOSOCOM.fapuntes, ENOSOCOM.iquirurg, ENOSOCOM.oinfecci, ENOSOCOM.fcultivo, ENOSOCOM.ogermene, " + 
				        "ENOSOCOM.irealant, ENOSOCOM.oantibio, ENOSOCOM.icuracio, ENOSOCOM.ifalleci, ENOSOCOM.gusuario, ENOSOCOM.iorigeni, " + 
				        "DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers, 'S' as activos, " + 
				        "ENOSOCOM.gconsinf, CONS.dconsinf " + 
				        "From ENOSOCOM " + 
				        "INNER JOIN SUSUARIO ON ENOSOCOM.gusuario = SUSUARIO.gusuario " + 
				        "INNER JOIN DPERSONA ON SUSUARIO.gpersona = DPERSONA.gpersona " + 
				        "LEFT JOIN ECONSINF CONS ON CONS.gconsinf = ENOSOCOM.gconsinf " + 
				        "WHERE ENOSOCOM.itiposer = '" + BNosocomiales.stTipoSer + "' AND " + 
				        "ENOSOCOM.ganoregi = " + BNosocomiales.intAnioRegi.ToString() + " AND " + 
				        "ENOSOCOM.gnumregi = " + BNosocomiales.lngNumRegi.ToString();

				if (CheckTodos.CheckState == CheckState.Unchecked)
				{
					stSql = stSql + " AND ENOSOCOM.fapuntes >= " + Serrores.FormatFecha(SdcFechaDesde);
				}

				if (OBApuntes[1].IsChecked)
				{
					stSql = stSql + "UNION ALL ";
					stSql = stSql + "SELECT ENOSOMOD.fapuntes, ENOSOMOD.iquirurg, ENOSOMOD.oinfecci, ENOSOMOD.fcultivo, ENOSOMOD.ogermene, " + 
					        "ENOSOMOD.irealant, ENOSOMOD.oantibio, ENOSOMOD.icuracio, ENOSOMOD.ifalleci, ENOSOMOD.gusuario, ENOSOMOD.iorigeni, " + 
					        "DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers,  'N' as activos, " + 
					        "ENOSOMOD.gconsinf, CONS.dconsinf " + 
					        "From ENOSOMOD " + 
					        "INNER JOIN SUSUARIO ON ENOSOMOD.gusuario = SUSUARIO.gusuario " + 
					        "INNER JOIN DPERSONA ON SUSUARIO.gpersona = DPERSONA.gpersona " + 
					        "LEFT JOIN ECONSINF CONS ON CONS.gconsinf = ENOSOMOD.gconsinf " + 
					        "WHERE ENOSOMOD.itiposer = '" + BNosocomiales.stTipoSer + "' AND " + 
					        "ENOSOMOD.ganoregi = " + BNosocomiales.intAnioRegi.ToString() + " AND " + 
					        "ENOSOMOD.gnumregi = " + BNosocomiales.lngNumRegi.ToString();

					if (CheckTodos.CheckState == CheckState.Unchecked)
					{
						stSql = stSql + " AND ENOSOMOD.fapuntes >= " + Serrores.FormatFecha(SdcFechaDesde);
					}
				}

				stSql = stSql + "ORDER BY 1 DESC ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BNosocomiales.cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);

				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					sprNosocomiales.Enabled = true;
					sprNosocomiales.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
					//sprNosocomiales.ScrollBars = 3
					lngContador = 1;

					StringBuilder stMedico = new StringBuilder();
					foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
					{
						sprNosocomiales.MaxRows = lngContador;
						sprNosocomiales.Row = lngContador;

						//If rdoTemp("activos") = "S" Then
						//    lngColor = vbBlack
						//Else
						//    lngColor = COLOR_APUNTES_CERRADOS
						//End If

						if (Convert.ToString(iteration_row["activos"]) == "N")
						{
							lngColor = ColorTranslator.FromOle(COLOR_APUNTES_CERRADOS);
						}
						else
						{
							if (stFechaLLegada != "")
							{
								if (DateTime.Parse(Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse(stFechaLLegada))
								{
									lngColor = lnUrgencias.BorderColor; // naranja
								}
								else
								{
									lngColor = Color.Black; // negro
								}
							}
							else
							{
								lngColor = Color.Black; // negro
							}
						}


						sprNosocomiales.Col = Col_FecRegig;
						sprNosocomiales.Text = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:NN");
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);
						sprNosocomiales.ForeColor = lngColor;


						sprNosocomiales.Col = Col_TipInfec;
                        //DGMORENOG_TODO_5_5 - VERIFICAR
						//sprNosocomiales.CellType = (FarPoint.Win.Spread.CellType.BaseCellType) 5;
						sprNosocomiales.Text = Convert.ToString(iteration_row["oinfecci"]);
						sprNosocomiales.TypeHAlign = 0;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_FecCulti;
						sprNosocomiales.Text = Convert.ToDateTime(iteration_row["fcultivo"]).ToString("dd/MM/yyyy HH:NN");
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_Germenes;
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.CellType = (FarPoint.Win.Spread.CellType.BaseCellType) 5;
						sprNosocomiales.Text = Convert.ToString(iteration_row["ogermene"]);
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 0;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_Farmacos;
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.CellType = (FarPoint.Win.Spread.CellType.BaseCellType) 5;
						sprNosocomiales.Text = Convert.ToString(iteration_row["oantibio"]).Trim();
						sprNosocomiales.TypeHAlign = 0;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_Quirurgi;
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["iquirurg"]))
						{
							sprNosocomiales.Text = (Convert.ToString(iteration_row["iquirurg"]) == "S") ? "S�" : "No";
						}
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_RealAnti;
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["irealant"]))
						{
							sprNosocomiales.Text = (Convert.ToString(iteration_row["irealant"]) == "S") ? "S�" : "No";
						}
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_CuraInfe;
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["icuracio"]))
						{
							sprNosocomiales.Text = (Convert.ToString(iteration_row["icuracio"]) == "S") ? "S�" : "No";
						}
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_Fallecim;
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["ifalleci"]))
						{
							sprNosocomiales.Text = (Convert.ToString(iteration_row["ifalleci"]) == "S") ? "S�" : "No";
						}
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_CodUsuar;
						sprNosocomiales.Text = Convert.ToString(iteration_row["gusuario"]);
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);

						sprNosocomiales.Col = Col_DesUsuar;
						stMedico = new StringBuilder(Convert.ToString(iteration_row["dap1pers"]).Trim());
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["dap2pers"]))
						{
							stMedico.Append(" " + Convert.ToString(iteration_row["dap2pers"]).Trim());
						}
						stMedico.Append(", " + Convert.ToString(iteration_row["dnompers"]).Trim());
						sprNosocomiales.Text = stMedico.ToString();
						sprNosocomiales.TypeHAlign = 0;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_ConSegundos;
						sprNosocomiales.Text = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:NN:SS");
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);
						sprNosocomiales.ForeColor = lngColor;

						sprNosocomiales.Col = Col_codConsecuencia;
						sprNosocomiales.Text = Convert.ToString(iteration_row["gconsinf"]) + "";
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);

						sprNosocomiales.Col = Col_desConsecuencia;
						sprNosocomiales.Text = Convert.ToString(iteration_row["dconsinf"]) + "";
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						sprNosocomiales.Col = Col_Origen;
						sprNosocomiales.Text = Convert.ToString(iteration_row["iorigeni"]) + "";
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, true);

						sprNosocomiales.Col = col_DesOrigen;
						switch(Convert.ToString(iteration_row["iorigeni"]) + "")
						{
							case "I" :  
								sprNosocomiales.Text = optOrigen[0].Text; 
								break;
							case "E" :  
								sprNosocomiales.Text = optOrigen[1].Text; 
								break;
							default: 
								sprNosocomiales.Text = ""; 
								break;
						}
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //sprNosocomiales.TypeHAlign = 2;
						sprNosocomiales.ForeColor = lngColor;
						sprNosocomiales.SetColHidden(sprNosocomiales.Col, false);

						lngContador++;

					}
					sprNosocomiales.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
					//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprNosocomiales.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					sprNosocomiales.setSelModeIndex(0); //Ninguna fila seleccionada

				}
				else
				{

					sprNosocomiales.Enabled = false;
					InicializaControles();
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();
				rdoTemp = null;
				ErrCargaGrid = false;
			}
			catch (Exception excep)
			{
				if (!ErrCargaGrid)
				{
					throw excep;
				}

				if (ErrCargaGrid)
				{

					RadMessageBox.Show(excep.Message, this.Text.Trim() + " / CargaGrid.", MessageBoxButtons.OK, RadMessageIcon.Error);

				}
			}
		}

		private void InicializaControles()
		{
			blnInicializa = true;
			ssdFechas[0].MaxDate = DateTime.Today;
			ssdFechas[1].MaxDate = DateTime.Today;
			ssdFechas[0].Value = DateTime.Today;
			ssdFechas[1].Text = "";
			tbHora[0].Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
			tbHora[1].Text = "  :  "; //Format(Now, "hh") & ":" & Format(Now, "nn")
			txtDatos[0].Text = "";
			txtDatos[1].Text = "";
			txtDatos[2].Text = "";
			optQuiru[0].IsChecked = false;
			optQuiru[1].IsChecked = false;
			optRealizado[0].IsChecked = false;
			optRealizado[1].IsChecked = false;
			optCuracion[0].IsChecked = false;
			optCuracion[1].IsChecked = false;
			optFalle[0].IsChecked = false;
			optFalle[1].IsChecked = false;
			optOrigen[0].IsChecked = false;
			optOrigen[1].IsChecked = false;
			chkTodos[0].CheckState = CheckState.Unchecked;
			chkTodos[1].CheckState = CheckState.Unchecked;
			cbbConsecuencia.SelectedIndex = -1;
			blnInicializa = false;
			blnModifi = false;
		}

		private void CargaFarmacos()
		{
			bool ErrCombo = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;
			int lngContador = 0;

			try
			{
				ErrCombo = true;


				stSql = "SELECT DCODFARMVT_LJOIN.dnomfarm " + 
				        "FROM EFARMACO " + 
				        "INNER JOIN DCODFARMVT_LJOIN ON EFARMACO.gfarmaco = DCODFARMVT_LJOIN.gfarmaco " + 
				        "WHERE EFARMACO.itiposer = '" + BNosocomiales.stTipoSer + "' AND " + 
				        "EFARMACO.ganoregi = " + BNosocomiales.intAnioRegi.ToString() + " AND " + 
				        "EFARMACO.gnumregi = " + BNosocomiales.lngNumRegi.ToString() + "  " + 
				        "group by DCODFARMVT_LJOIN.dnomfarm " + 
				        "ORDER BY DCODFARMVT_LJOIN.dnomfarm ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BNosocomiales.cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					lngContador = 1;
					vaFarmacos.MaxRows = lngContador;
					foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
					{
						vaFarmacos.MaxRows = lngContador;
						vaFarmacos.Row = lngContador;

						vaFarmacos.Col = Far_Select;
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //vaFarmacos.CellType = (FarPoint.Win.Spread.CellType.BaseCellType) 10;
						vaFarmacos.TypeCheckCenter = true;
						vaFarmacos.Text = "";
						vaFarmacos.SetColHidden(vaFarmacos.Col, false);

						vaFarmacos.Col = Far_Farmaco;
                        //DGMORENOG_TODO_5_5 - VERIFICAR
                        //vaFarmacos.CellType = (FarPoint.Win.Spread.CellType.BaseCellType) 5;
						vaFarmacos.TypeHAlign = 0;
						vaFarmacos.Text = Convert.ToString(iteration_row["dnomfarm"]).Trim();
						vaFarmacos.SetColHidden(vaFarmacos.Col, false);

						lngContador++;
					}

				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();
				rdoTemp = null;
				ErrCombo = false;
			}
			catch (Exception excep)
			{
				if (!ErrCombo)
				{
					throw excep;
				}

				if (ErrCombo)
				{

					RadMessageBox.Show(excep.Message, this.Text.Trim() + " / CargaFarmacos.", MessageBoxButtons.OK, RadMessageIcon.Error);

				}
			}
		}


		private bool isInitializingComponent;
		private void OBApuntes_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				InicializaControles();
				ActiDesaBoton(false);
				CargaGrid();
				cmdImpresion.Enabled = false;
			}
		}

		private void optCuracion_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!blnActiva)
				{
					ActiDesaBoton(true);
				}
			}
		}

		private void optFalle_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!blnActiva)
				{
					ActiDesaBoton(true);
				}
			}
		}

		private void optQuiru_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!blnActiva)
				{
					ActiDesaBoton(true);
				}
			}
		}

		private void optOrigen_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!blnActiva)
				{
					ActiDesaBoton(true);
				}
			}
		}


		private void optRealizado_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!blnActiva)
				{
					ActiDesaBoton(true);
				}
			}
		}

		private void sprNosocomiales_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;

            //DGMORENOG_TODO_5_5 - VERIFICAR
            //sprNosocomiales.Row = sprNosocomiales.getSelModeIndex(); //Fila activa

            //DGMORENOG_TODO_5_5 - VERIFICAR
            //if (sprNosocomiales.getSelModeIndex() > 0)
            if (sprNosocomiales.Row > 0)
            {
				blnModifi = true;

				sprNosocomiales.Col = Col_ConSegundos;
				ssdFechas[0].Value = DateTime.Parse(DateTime.Parse(sprNosocomiales.Text).ToString("dd/MM/yyyy"));
				tbHora[0].Text = DateTime.Parse(sprNosocomiales.Text).ToString("HH:NN");
				stSegundos = DateTime.Parse(sprNosocomiales.Text).ToString("SS");
				ssdFechas[0].Enabled = false;
				tbHora[0].Enabled = false;

				sprNosocomiales.Col = Col_TipInfec;
				txtDatos[0].Text = sprNosocomiales.Text;

				sprNosocomiales.Col = Col_FecCulti;
				if (sprNosocomiales.Text.Trim() != "")
				{
					ssdFechas[1].Value = DateTime.Parse(DateTime.Parse(sprNosocomiales.Text).ToString("dd/MM/yyyy"));
					tbHora[1].Text = DateTime.Parse(sprNosocomiales.Text).ToString("HH:NN");
				}
				else
				{
					ssdFechas[1].Text = "";
					tbHora[1].Text = "  :  ";
				}

				sprNosocomiales.Col = Col_Germenes;
				txtDatos[1].Text = sprNosocomiales.Text;

				sprNosocomiales.Col = Col_Farmacos;
				txtDatos[2].Text = sprNosocomiales.Text;

				optQuiru[0].IsChecked = false;
				optQuiru[1].IsChecked = false;
				sprNosocomiales.Col = Col_Quirurgi;
				if (sprNosocomiales.Text == "S�")
				{
					optQuiru[0].IsChecked = true;
				}
				else if (sprNosocomiales.Text == "No")
				{ 
					optQuiru[1].IsChecked = true;
				}

				optRealizado[0].IsChecked = false;
				optRealizado[1].IsChecked = false;
				sprNosocomiales.Col = Col_RealAnti;
				if (sprNosocomiales.Text == "S�")
				{
					optRealizado[0].IsChecked = true;
				}
				else if (sprNosocomiales.Text == "No")
				{ 
					optRealizado[1].IsChecked = true;
				}

				optCuracion[0].IsChecked = false;
				optCuracion[1].IsChecked = false;
				sprNosocomiales.Col = Col_CuraInfe;
				if (sprNosocomiales.Text == "S�")
				{
					optCuracion[0].IsChecked = true;
				}
				else if (sprNosocomiales.Text == "No")
				{ 
					optCuracion[1].IsChecked = true;
				}

				optFalle[0].IsChecked = false;
				optFalle[1].IsChecked = false;
				sprNosocomiales.Col = Col_Fallecim;
				if (sprNosocomiales.Text == "S�")
				{
					optFalle[0].IsChecked = true;
				}
				else if (sprNosocomiales.Text == "No")
				{ 
					optFalle[1].IsChecked = true;
				}

				optOrigen[0].IsChecked = false;
				optOrigen[1].IsChecked = false;
				sprNosocomiales.Col = Col_Origen;
				if (sprNosocomiales.Text == "I")
				{
					optOrigen[0].IsChecked = true;
				}
				else if (sprNosocomiales.Text == "E")
				{ 
					optOrigen[1].IsChecked = true;
				}

				sprNosocomiales.Col = Col_codConsecuencia;
				if (sprNosocomiales.Text.Trim() != "")
				{
					cbbConsecuencia.SelectedIndex = Serrores.fnBuscaListIndexID(cbbConsecuencia, sprNosocomiales.Text.Trim());
				}
				else
				{
					cbbConsecuencia.SelectedIndex = -1;
				}

				//txtDatos(0).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//ssdFechas(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//tbHora(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//txtDatos(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//txtDatos(2).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//vaFarmacos.Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optCuracion(0).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optFalle(0).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optQuiru(0).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optRealizado(0).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optCuracion(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optFalle(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optQuiru(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//optRealizado(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//
				//chkTodos(0).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//chkTodos(1).Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//ActiDesaBoton sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//
				//cmdImpresion.Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS
				//cmbInsertar.Enabled = sprNosocomiales.ForeColor <> COLOR_APUNTES_CERRADOS

				txtDatos[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				ssdFechas[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				tbHora[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				txtDatos[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				txtDatos[2].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				vaFarmacos.Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optCuracion[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optFalle[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optQuiru[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optOrigen[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optRealizado[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optCuracion[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optFalle[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optQuiru[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optOrigen[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				optRealizado[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);

				chkTodos[0].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				chkTodos[1].Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				cbbConsecuencia.Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				ActiDesaBoton(sprNosocomiales.ForeColor.Equals(Color.Black));

				cmdImpresion.Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
				cmbInsertar.Enabled = sprNosocomiales.ForeColor.Equals(Color.Black);
			}
			else
			{
				InicializaControles();
				blnModifi = false;
				cmdImpresion.Enabled = false;
				cmbInsertar.Enabled = true;
			}
		}

		private void ssdFechas_Click(Object eventSender, EventArgs eventArgs)
		{
			int index = Array.IndexOf(this.ssdFechas, eventSender);
			if (index == 1 && tbHora[1].Text == "  :  ")
			{
				tbHora[1].Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
			}
			if (!blnActiva)
			{
				ActiDesaBoton(true);
			}
		}

		private void ActiDesaBoton(bool blnOpcion)
		{
			if (!blnInicializa)
			{
				cbAceptar.Enabled = blnOpcion;
				//CbCancelar.Enabled = blnOpcion
				blnActiva = blnOpcion;
			}
		}

		//UPGRADE_WARNING: (2050) SSCalendarWidgets_A.SSDateCombo Event ssdFechas.Change was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2050.aspx
		private void ssdFechas_Change(int index)
		{
			ssdFechas_Click(ssdFechas[index], new EventArgs());
		}

		//UPGRADE_WARNING: (2050) MSMask.MaskEdBox Event tbHora.Change was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2050.aspx
		private void tbHora_Change(int index)
		{
			if (!blnActiva)
			{
				ActiDesaBoton(true);
			}
		}

		private void txtDatos_TextChanged(Object eventSender, EventArgs eventArgs)
		{

			if (!blnActiva)
			{
				ActiDesaBoton(true);
			}
		}

		private bool fValidaDatos()
		{
			bool result = false;

			result = true;


			if (!Information.IsDate(ssdFechas[0].Text))
			{
				short tempRefParam = 1140;
				string[] tempRefParam2 = new string[] { lblTextos[7].Text};
				BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BNosocomiales.cnConexion, tempRefParam2);
				ssdFechas[0].Focus();
				return false;
			}

			if (!Information.IsDate(tbHora[0].Text))
			{
				short tempRefParam3 = 1150;
                string[] tempRefParam4 = new string[] { lblTextos[7].Text};
				BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BNosocomiales.cnConexion, tempRefParam4);
				tbHora[0].Focus();
				return false;
			}

			if (DateTime.Parse(tbHora[0].Text) > DateTimeHelper.Time && DateTime.Parse(ssdFechas[0].Text) == DateTime.Today)
			{
				short tempRefParam5 = 1020;
                string[] tempRefParam6 = new string[] { "La hora de registro", "menor", "la hora del sistema"};
				BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, BNosocomiales.cnConexion, tempRefParam6);
				tbHora[1].Focus();
				return false;
			}

			if (ssdFechas[1].Text != "")
			{
				if (!Information.IsDate(ssdFechas[1].Text))
				{
					short tempRefParam7 = 1140;
                    string[] tempRefParam8 = new string[] { lblTextos[9].Text};
					BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, BNosocomiales.cnConexion, tempRefParam8);
					ssdFechas[1].Focus();
					return false;
				}

				if (DateTime.Parse(ssdFechas[1].Text) > DateTime.Parse(ssdFechas[0].Text))
				{
					short tempRefParam9 = 1020;
                    string[] tempRefParam10 = new string[] { "La fecha del cultivo", "menor o igual", "la fecha de registro"};
					BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, BNosocomiales.cnConexion, tempRefParam10);
					tbHora[1].Focus();
					return false;
				}



				if (tbHora[1].Text.Trim() != ":")
				{
					if (!Information.IsDate(tbHora[1].Text))
					{
						short tempRefParam11 = 1150;
                        string[] tempRefParam12 = new string[] { lblTextos[9].Text};
						BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, BNosocomiales.cnConexion, tempRefParam12);
						tbHora[1].Focus();
						return false;
					}
					else
					{
						if (DateTime.Parse(tbHora[1].Text) > DateTime.Parse(tbHora[0].Text) && DateTime.Parse(ssdFechas[1].Text) == DateTime.Parse(ssdFechas[0].Text))
						{
							short tempRefParam13 = 1020;
                            string[] tempRefParam14 = new string[] { "La hora del cultivo", "menor", "la hora de registro"};
							BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, BNosocomiales.cnConexion, tempRefParam14);
							tbHora[1].Focus();
							return false;
						}
						if (DateTime.Parse(tbHora[1].Text) > DateTimeHelper.Time && DateTime.Parse(ssdFechas[1].Text) == DateTime.Today)
						{
							short tempRefParam15 = 1020;
                            string[] tempRefParam16 = new string[] { "La hora del cultivo", "menor", "la hora del sistema"};
							BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, BNosocomiales.cnConexion, tempRefParam16);
							tbHora[1].Focus();
							return false;
						}
					}
				}
				else
				{
					short tempRefParam17 = 1140;
                    string[] tempRefParam18 = new string[] { lblTextos[9].Text};
					BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam17, BNosocomiales.cnConexion, tempRefParam18);
					ssdFechas[1].Focus();
					return false;
				}
			}
			else
			{
				if (tbHora[1].Text.Trim() != ":")
				{
					short tempRefParam19 = 1140;
                    string[] tempRefParam20 = new string[] { lblTextos[9].Text};
					BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam19, BNosocomiales.cnConexion, tempRefParam20);
					ssdFechas[1].Focus();
					return false;
				}
			}


			if (txtDatos[0].Text.Trim() == "")
			{
				short tempRefParam21 = 1040;
                string[] tempRefParam22 = new string[] { Frame1[7].Text};
				BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam21, BNosocomiales.cnConexion, tempRefParam22);
				txtDatos[0].Focus();
				return false;
			}

			if (txtDatos[2].Text.Trim() == "")
			{
				short tempRefParam23 = 1040;
                string[] tempRefParam24 = new string[] { Frame1[9].Text};
				BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam23, BNosocomiales.cnConexion, tempRefParam24);
				txtDatos[2].Focus();
				return false;
			}

			if (DateTime.Parse(ssdFechas[0].Text + " " + tbHora[0].Text) < DateTime.Parse(BNosocomiales.stIngreso))
			{
				System.DateTime TempDate = DateTime.FromOADate(0);
				short tempRefParam25 = 1020;
                string[] tempRefParam26 = new string[] { lblTextos[7].Text, "> o =", (DateTime.TryParse(BNosocomiales.stIngreso, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : BNosocomiales.stIngreso};
				BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam25, BNosocomiales.cnConexion, tempRefParam26);
				tbHora[0].Focus();
				return false;
			}
			if (ssdFechas[1].Text != "")
			{
				if (DateTime.Parse(ssdFechas[1].Text + " " + tbHora[1].Text) < DateTime.Parse(BNosocomiales.stIngreso))
				{
					System.DateTime TempDate2 = DateTime.FromOADate(0);
					short tempRefParam27 = 1020;
                    string[] tempRefParam28 = new string[] { lblTextos[9].Text, "> o =", (DateTime.TryParse(BNosocomiales.stIngreso, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : BNosocomiales.stIngreso};
					BNosocomiales.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam27, BNosocomiales.cnConexion, tempRefParam28);
					tbHora[1].Focus();
					return false;
				}
			}
			return result;
		}


		private bool fCargarDatosACCES()
		{
            //DGMORENOG_TODO_X_5 - PENDIENTE DEFINIR TRATAMIENTO DE BASE DATOS ACCES
			//bool Etiqueta = false;
			//bool result = false;
			//object dbLangSpanish = null;
			//object DBEngine = null;
			////UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			//Database bsDatabase = null;
			////UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			////UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			//Recordset rsCursor = null;
			//string stPathTemp = String.Empty;
			//bool blnBase = false;

			//try
			//{
			//	Etiqueta = true;

			//	result = true;

			//	stPathTemp = BNosocomiales.PathAplicacion + "\\" + BNosocomiales.NombreBaseAccess;
			//	if (FileSystem.Dir(stPathTemp, FileAttribute.Normal) != "")
			//	{
			//		blnBase = true;
			//	}

			//	if (!blnBase)
			//	{ // SI est� dado
			//		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//		bsDatabase = (Database) DBEngine.Workspaces(0).CreateDatabase(stPathTemp, dbLangSpanish);
			//		blnBase = true;
			//	}
			//	else
			//	{
			//		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//		bsDatabase = (Database) DBEngine.Workspaces(0).OpenDatabase(stPathTemp);
			//		//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//		foreach (TableDef tblTabla in bsDatabase.TableDefs)
			//		{
			//			//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//			if (Convert.ToString(tblTabla.Name) == "INF_NOSOCOMIALE")
			//			{
			//				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//				bsDatabase.Execute("DROP TABLE INF_NOSOCOMIALE");
			//			}
			//		}
			//	}

			//	//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//	bsDatabase.Execute("CREATE TABLE INF_NOSOCOMIALE " + 
			//	                   "( Ingreso Text, Registro Text, Historia Text, NombrePac Text, " + 
			//	                   "PacQuirur Text(2), TipoInfec LONGTEXT, fCultivo Text, Germenes LONGTEXT, " + 
			//	                   "RealiAnti Text(2), TtoAntibio LONGTEXT, Curacion Text(2), Falleci Text(2), Consecuencia TEXT, Origen TEXT )");


			//	//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//	rsCursor = (Recordset) bsDatabase.OpenRecordset("SELECT * FROM INF_NOSOCOMIALE Where 1=2");

			//	//UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//	rsCursor.AddNew();
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Ingreso). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["Ingreso"] = (Recordset) lblTextos[5].Text;
			//	sprNosocomiales.Col = Col_FecRegig;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Registro). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["Registro"] = (Recordset) sprNosocomiales.Text.Trim();
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Historia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["Historia"] = (Recordset) lblTextos[3].Text;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(NombrePac). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["NombrePac"] = (Recordset) lblTextos[1].Text;
			//	sprNosocomiales.Col = Col_Quirurgi;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(PacQuirur). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["PacQuirur"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_TipInfec;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(TipoInfec). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["TipoInfec"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_FecCulti;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(fCultivo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["fCultivo"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_Germenes;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Germenes). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["Germenes"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_RealAnti;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(RealiAnti). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["RealiAnti"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_Farmacos;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(TtoAntibio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["TtoAntibio"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_CuraInfe;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Curacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["Curacion"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_Fallecim;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(Falleci). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["Falleci"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = Col_desConsecuencia;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(consecuencia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["consecuencia"] = (Recordset) sprNosocomiales.Text.Trim();
			//	sprNosocomiales.Col = col_DesOrigen;
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(origen). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object rsCursor(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
			//	rsCursor["origen"] = (Recordset) sprNosocomiales.Text.Trim();
			//	//UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//	rsCursor.Update();

			//	//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//	bsDatabase.Close();
			//	//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//	DBEngine.Workspaces(0).Close();

			//	Etiqueta = false;
			//	return result;
			//}
			//catch (Exception excep)
			//{
			//	if (!Etiqueta)
			//	{
			//		throw excep;
			//	}

			//	if (Etiqueta)
			//	{

			//		bsDatabase = null;
			//		Serrores.AnalizaError("Inf. Nosocomiales.", Path.GetDirectoryName(Application.ExecutablePath), BNosocomiales.vgUsuario, "fCargarDatosACCES", BNosocomiales.cnConexion);
			//		result = false;

			//		return result;
			//	}
			//}
			return false;
		}

		private void CargaConsecuencias()
		{
			cbbConsecuencia.Items.Clear();
			string stSql = "SELECT gconsinf, dconsinf " + 
			               "FROM ECONSINF " + 
			               "WHERE ECONSINF.fborrado is null order by dconsinf ";
			cbbConsecuencia.AddItem("","");
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BNosocomiales.cnConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
				{
					cbbConsecuencia.AddItem((Convert.ToString(iteration_row["dconsinf"]) + "").Trim(),"");
					cbbConsecuencia.SetItemData(cbbConsecuencia.GetNewIndex(), Convert.ToInt32(iteration_row["gconsinf"]));
				}

			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rdoTemp.Close();
		}
	}
}