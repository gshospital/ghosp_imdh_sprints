using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PautasCorrectoras
{
	partial class frmPlanPauCorr
	{

		#region "Upgrade Support "
		private static frmPlanPauCorr m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmPlanPauCorr DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmPlanPauCorr();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprPlantillasPAU", "TxtDesc", "cbCancelar", "cbAceptar", "sprPlantillasPAU_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread sprPlantillasPAU;
        public Telerik.WinControls.UI.RadTextBoxControl TxtDesc;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.sprPlantillasPAU = new UpgradeHelpers.Spread.FpSpread();
            this.TxtDesc = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.sprPlantillasPAU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprPlantillasPAU.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // sprPlantillasPAU
            // 
            this.sprPlantillasPAU.Location = new System.Drawing.Point(4, 4);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "Descripción";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 600;
            this.sprPlantillasPAU.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1});
            this.sprPlantillasPAU.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprPlantillasPAU.Name = "sprPlantillasPAU";
            this.sprPlantillasPAU.ReadOnly = true;
            this.sprPlantillasPAU.Size = new System.Drawing.Size(668, 246);
            this.sprPlantillasPAU.TabIndex = 0;
            this.sprPlantillasPAU.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprPlantillasPAU_CellClick);
            // 
            // TxtDesc
            // 
            this.TxtDesc.AcceptsReturn = true;
            this.TxtDesc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtDesc.Location = new System.Drawing.Point(4, 252);
            this.TxtDesc.MaxLength = 0;
            this.TxtDesc.Multiline = true;
            this.TxtDesc.Name = "TxtDesc";
            this.TxtDesc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDesc.Size = new System.Drawing.Size(501, 133);
            this.TxtDesc.TabIndex = 3;
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(598, 356);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(75, 30);
            this.cbCancelar.TabIndex = 2;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(512, 356);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(75, 30);
            this.cbAceptar.TabIndex = 1;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // frmPlanPauCorr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 394);
            this.Controls.Add(this.sprPlantillasPAU);
            this.Controls.Add(this.TxtDesc);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPlanPauCorr";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Selección de plantilla de Pauta Correctora";
            this.Closed += new System.EventHandler(this.frmPlanPauCorr_Closed);
            this.Load += new System.EventHandler(this.frmPlanPauCorr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sprPlantillasPAU.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprPlantillasPAU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}