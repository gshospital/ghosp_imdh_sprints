using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PautasCorrectoras
{
	public class MCPautasCorrectoras
	{


		frmPauCorr frmPautas = null;
		frmPlanPauCorr frmPlantillas = null;


		public void Llamada(object nForm, MCPautasCorrectoras nClase, SqlConnection nConnet, string sParam1, string sParam2, string sParam3, string sParm4, string strcama)
		{

			Serrores.AjustarRelojCliente(nConnet);
			frmPautas = new frmPauCorr();
			frmPautas.REFERENCIAS(nForm, nClase, nConnet, sParam1, sParam2, sParam3, sParm4, strcama);
			frmPautas.ShowDialog();


		}

		public MCPautasCorrectoras()
		{

			try
			{
				BGPautas.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				MessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}


		public void LlamadaPlantillas(dynamic nForm, SqlConnection nConexion)
		{
			BGPautas.RcEnfermeria = nConexion;
			Serrores.AjustarRelojCliente(BGPautas.RcEnfermeria);
			frmPlantillas = new frmPlanPauCorr();
			frmPlantillas.ShowDialog();
			//UPGRADE_TODO: (1067) Member tbPautaCorrectora is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			nForm.tbPautaCorrectora.Text = BGPautas.stPautaCorrectora;
			frmPlantillas = null;
		}


		~MCPautasCorrectoras()
		{
			BGPautas.clase_mensaje = null;
			frmPauCorr.DefInstance = null;
			frmPautas = null;
		}
	}
}