using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PautasCorrectoras
{
	partial class frmPauCorr
	{

		#region "Upgrade Support "
		private static frmPauCorr m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmPauCorr DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmPauCorr();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "vasPautas", "_cmdBotones_4", "_cmdBotones_3", "cbBuscar", "chkTodos", "_optVer_1", "_optVer_0", "_sdcFechas_2", "_lblTextos_6", "Frame4", "_cmdBotones_2", "_cmdBotones_1", "_cmdBotones_0", "txtDatos", "_sdcFechas_1", "_sdcFechas_0", "_tbHora_0", "_tbHora_1", "_lblTextos_5", "_lblTextos_4", "_lblTextos_3", "_lblTextos_2", "Frame3", "lnUrgencias", "lbUrgencias", "_Line1_0", "_lblTextos_8", "_lblTextos_7", "_Line1_1", "Frame2", "_lblTextos_10", "_lblTextos_9", "_lblTextos_1", "_lblTextos_0", "Frame1", "Line1", "cmdBotones", "lblTextos", "optVer", "sdcFechas", "tbHora", "ShapeContainer1", "commandButtonHelper1", "vasPautas_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread vasPautas;

        private Telerik.WinControls.UI.RadButton _cmdBotones_4;
		private Telerik.WinControls.UI.RadButton _cmdBotones_3;
		public Telerik.WinControls.UI.RadButton cbBuscar;
		public Telerik.WinControls.UI.RadCheckBox chkTodos;
		private Telerik.WinControls.UI.RadRadioButton _optVer_1;
		private Telerik.WinControls.UI.RadRadioButton _optVer_0;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcFechas_2;
		private Telerik.WinControls.UI.RadLabel _lblTextos_6;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		private Telerik.WinControls.UI.RadButton _cmdBotones_2;
		private Telerik.WinControls.UI.RadButton _cmdBotones_1;
		private Telerik.WinControls.UI.RadButton _cmdBotones_0;
		public Telerik.WinControls.UI.RadTextBoxControl txtDatos;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcFechas_1;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcFechas_0;
		private Telerik.WinControls.UI.RadMaskedEditBox _tbHora_0;
		private Telerik.WinControls.UI.RadMaskedEditBox _tbHora_1;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_5;
		private Telerik.WinControls.UI.RadLabel _lblTextos_4;
		private Telerik.WinControls.UI.RadLabel _lblTextos_3;
		private Telerik.WinControls.UI.RadLabel _lblTextos_2;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Microsoft.VisualBasic.PowerPacks.LineShape lnUrgencias;
		public Telerik.WinControls.UI.RadLabel lbUrgencias;
		private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_0;
		private Telerik.WinControls.UI.RadLabel _lblTextos_8;
		private Telerik.WinControls.UI.RadLabel _lblTextos_7;
		private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_1;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_10;
		private Telerik.WinControls.UI.RadLabel _lblTextos_9;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_0;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Microsoft.VisualBasic.PowerPacks.LineShape[] Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[2];
		public Telerik.WinControls.UI.RadButton[] cmdBotones = new Telerik.WinControls.UI.RadButton[5];
		public Telerik.WinControls.UI.RadLabel[] lblTextos = new Telerik.WinControls.UI.RadLabel[11];
		public Telerik.WinControls.UI.RadRadioButton[] optVer = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadDateTimePicker[] sdcFechas = new Telerik.WinControls.UI.RadDateTimePicker[3];
		public Telerik.WinControls.UI.RadMaskedEditBox[] tbHora = new Telerik.WinControls.UI.RadMaskedEditBox[2];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPauCorr));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lnUrgencias = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this._Line1_0 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this._Line1_1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.vasPautas = new UpgradeHelpers.Spread.FpSpread();
            this._cmdBotones_4 = new Telerik.WinControls.UI.RadButton();
            this._cmdBotones_3 = new Telerik.WinControls.UI.RadButton();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbBuscar = new Telerik.WinControls.UI.RadButton();
            this.chkTodos = new Telerik.WinControls.UI.RadCheckBox();
            this._optVer_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._optVer_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._sdcFechas_2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._lblTextos_6 = new Telerik.WinControls.UI.RadLabel();
            this._cmdBotones_2 = new Telerik.WinControls.UI.RadButton();
            this._cmdBotones_1 = new Telerik.WinControls.UI.RadButton();
            this._cmdBotones_0 = new Telerik.WinControls.UI.RadButton();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.txtDatos = new Telerik.WinControls.UI.RadTextBoxControl();
            this._sdcFechas_1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._sdcFechas_0 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._tbHora_0 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this._tbHora_1 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this._lblTextos_5 = new Telerik.WinControls.UI.RadTextBox();
            this._lblTextos_4 = new Telerik.WinControls.UI.RadLabel();
            this._lblTextos_3 = new Telerik.WinControls.UI.RadLabel();
            this._lblTextos_2 = new Telerik.WinControls.UI.RadLabel();
            this.lbUrgencias = new Telerik.WinControls.UI.RadLabel();
            this._lblTextos_8 = new Telerik.WinControls.UI.RadLabel();
            this._lblTextos_7 = new Telerik.WinControls.UI.RadLabel();
            this._lblTextos_10 = new Telerik.WinControls.UI.RadTextBox();
            this._lblTextos_9 = new Telerik.WinControls.UI.RadLabel();
            this._lblTextos_1 = new Telerik.WinControls.UI.RadTextBox();
            this._lblTextos_0 = new Telerik.WinControls.UI.RadLabel();
            this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vasPautas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vasPautas.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTodos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._optVer_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._optVer_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcFechas_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcFechas_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcFechas_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbHora_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbHora_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUrgencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(2, 18);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lnUrgencias,
            this._Line1_0,
            this._Line1_1});
            this.ShapeContainer1.Size = new System.Drawing.Size(639, 389);
            this.ShapeContainer1.TabIndex = 32;
            this.ShapeContainer1.TabStop = false;
            // 
            // lnUrgencias
            // 
            this.lnUrgencias.BorderColor = System.Drawing.Color.Fuchsia;
            this.lnUrgencias.BorderWidth = 4;
            this.lnUrgencias.Enabled = false;
            this.lnUrgencias.Name = "lnUrgencias";
            this.lnUrgencias.Visible = false;
            this.lnUrgencias.X1 = 14;
            this.lnUrgencias.X2 = 36;
            this.lnUrgencias.Y1 = 379;
            this.lnUrgencias.Y2 = 379;
            // 
            // _Line1_0
            // 
            this._Line1_0.BorderColor = System.Drawing.Color.Black;
            this._Line1_0.BorderWidth = 4;
            this._Line1_0.Enabled = false;
            this._Line1_0.Name = "_Line1_0";
            this._Line1_0.X1 = 14;
            this._Line1_0.X2 = 36;
            this._Line1_0.Y1 = 347;
            this._Line1_0.Y2 = 347;
            // 
            // _Line1_1
            // 
            this._Line1_1.BorderColor = System.Drawing.Color.Gray;
            this._Line1_1.BorderWidth = 4;
            this._Line1_1.Enabled = false;
            this._Line1_1.Name = "_Line1_1";
            this._Line1_1.X1 = 14;
            this._Line1_1.X2 = 36;
            this._Line1_1.Y1 = 363;
            this._Line1_1.Y2 = 363;
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this._lblTextos_10);
            this.Frame1.Controls.Add(this._lblTextos_9);
            this.Frame1.Controls.Add(this._lblTextos_1);
            this.Frame1.Controls.Add(this._lblTextos_0);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(6, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(655, 463);
            this.Frame1.TabIndex = 0;
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.vasPautas);
            this.Frame2.Controls.Add(this._cmdBotones_4);
            this.Frame2.Controls.Add(this._cmdBotones_3);
            this.Frame2.Controls.Add(this.Frame4);
            this.Frame2.Controls.Add(this._cmdBotones_2);
            this.Frame2.Controls.Add(this._cmdBotones_1);
            this.Frame2.Controls.Add(this._cmdBotones_0);
            this.Frame2.Controls.Add(this.Frame3);
            this.Frame2.Controls.Add(this.lbUrgencias);
            this.Frame2.Controls.Add(this._lblTextos_8);
            this.Frame2.Controls.Add(this._lblTextos_7);
            this.Frame2.Controls.Add(this.ShapeContainer1);
            this.Frame2.HeaderText = "";
            this.Frame2.Location = new System.Drawing.Point(6, 48);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(643, 409);
            this.Frame2.TabIndex = 3;
            // 
            // vasPautas
            // 
            this.vasPautas.Location = new System.Drawing.Point(8, 62);
            // 
            // 
            // 
            gridViewTextBoxColumn10.HeaderText = "Inicio Periodo";
            gridViewTextBoxColumn10.Name = "column1";
            gridViewTextBoxColumn10.Width = 100;
            gridViewTextBoxColumn11.HeaderText = "Fin Periodo";
            gridViewTextBoxColumn11.Name = "column2";
            gridViewTextBoxColumn11.Width = 100;
            gridViewTextBoxColumn12.HeaderText = "Pauta Correctora";
            gridViewTextBoxColumn12.Name = "column3";
            gridViewTextBoxColumn12.Width = 250;
            gridViewTextBoxColumn13.HeaderText = "Usuario / M�dico";
            gridViewTextBoxColumn13.Name = "column4";
            gridViewTextBoxColumn13.Width = 250;
            gridViewTextBoxColumn14.HeaderText = "Usuario";
            gridViewTextBoxColumn14.Name = "column5";
            gridViewTextBoxColumn14.Width = 80;
            gridViewTextBoxColumn15.HeaderText = "F. Sistema";
            gridViewTextBoxColumn15.Name = "column6";
            gridViewTextBoxColumn15.Width = 80;
            gridViewTextBoxColumn16.HeaderText = "Usuario Modificaci�n";
            gridViewTextBoxColumn16.Name = "column7";
            gridViewTextBoxColumn16.Width = 150;
            gridViewTextBoxColumn17.HeaderText = "Fecha Modificaci�n";
            gridViewTextBoxColumn17.Name = "column8";
            gridViewTextBoxColumn17.Width = 150;
            gridViewTextBoxColumn18.HeaderText = "Activar";
            gridViewTextBoxColumn18.Name = "column9";
            this.vasPautas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.vasPautas.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.vasPautas.Name = "vasPautas";
            this.vasPautas.ReadOnly = true;
            this.vasPautas.Size = new System.Drawing.Size(627, 121);
            this.vasPautas.TabIndex = 7;
            this.vasPautas.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.vasPautas_CellClick);
            // 
            // _cmdBotones_4
            // 
            this._cmdBotones_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._cmdBotones_4.Location = new System.Drawing.Point(204, 356);
            this._cmdBotones_4.Name = "_cmdBotones_4";
            this._cmdBotones_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cmdBotones_4.Size = new System.Drawing.Size(81, 35);
            this._cmdBotones_4.TabIndex = 26;
            this._cmdBotones_4.Text = "&Plantillas";
            this._cmdBotones_4.Click += new System.EventHandler(this.cmdBotones_Click);
            // 
            // _cmdBotones_3
            // 
            this._cmdBotones_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._cmdBotones_3.Enabled = false;
            this._cmdBotones_3.Location = new System.Drawing.Point(291, 356);
            this._cmdBotones_3.Name = "_cmdBotones_3";
            this._cmdBotones_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cmdBotones_3.Size = new System.Drawing.Size(81, 35);
            this._cmdBotones_3.TabIndex = 27;
            this._cmdBotones_3.Text = "Ac&tivaci�n";
            this._cmdBotones_3.Click += new System.EventHandler(this.cmdBotones_Click);
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.cbBuscar);
            this.Frame4.Controls.Add(this.chkTodos);
            this.Frame4.Controls.Add(this._optVer_1);
            this.Frame4.Controls.Add(this._optVer_0);
            this.Frame4.Controls.Add(this._sdcFechas_2);
            this.Frame4.Controls.Add(this._lblTextos_6);
            this.Frame4.HeaderText = "Pautas";
            this.Frame4.Location = new System.Drawing.Point(8, 10);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(627, 45);
            this.Frame4.TabIndex = 10;
            this.Frame4.Text = "Pautas";
            // 
            // cbBuscar
            // 
            this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscar.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscar.Image")));
            this.cbBuscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscar.Location = new System.Drawing.Point(590, 14);
            this.cbBuscar.Name = "cbBuscar";
            this.cbBuscar.Size = new System.Drawing.Size(27, 25);
            this.cbBuscar.TabIndex = 16;
            this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
            // 
            // chkTodos
            // 
            this.chkTodos.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkTodos.Location = new System.Drawing.Point(522, 18);
            this.chkTodos.Name = "chkTodos";
            this.chkTodos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTodos.Size = new System.Drawing.Size(51, 18);
            this.chkTodos.TabIndex = 15;
            this.chkTodos.Text = "Todos";
            this.chkTodos.CheckStateChanged += new System.EventHandler(this.chkTodos_CheckStateChanged);
            // 
            // _optVer_1
            // 
            this._optVer_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._optVer_1.Location = new System.Drawing.Point(140, 18);
            this._optVer_1.Name = "_optVer_1";
            this._optVer_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optVer_1.Size = new System.Drawing.Size(155, 18);
            this._optVer_1.TabIndex = 12;
            this._optVer_1.Text = "Apuntes activos e inactivos";
            this._optVer_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optVer_CheckedChanged);
            // 
            // _optVer_0
            // 
            this._optVer_0.CheckState = System.Windows.Forms.CheckState.Checked;
            this._optVer_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._optVer_0.Location = new System.Drawing.Point(26, 18);
            this._optVer_0.Name = "_optVer_0";
            this._optVer_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optVer_0.Size = new System.Drawing.Size(99, 18);
            this._optVer_0.TabIndex = 11;
            this._optVer_0.Text = "Apuntes activos";
            this._optVer_0.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this._optVer_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optVer_CheckedChanged);
            // 
            // _sdcFechas_2
            // 
            this._sdcFechas_2.CustomFormat = "dd/MM/yyyy";
            this._sdcFechas_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._sdcFechas_2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcFechas_2.Location = new System.Drawing.Point(380, 17);
            this._sdcFechas_2.Name = "_sdcFechas_2";
            this._sdcFechas_2.Size = new System.Drawing.Size(119, 18);
            this._sdcFechas_2.TabIndex = 13;
            this._sdcFechas_2.TabStop = false;
            this._sdcFechas_2.Click += new System.EventHandler(this.sdcFechas_Click);
            // 
            // _lblTextos_6
            // 
            this._lblTextos_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_6.Location = new System.Drawing.Point(334, 18);
            this._lblTextos_6.Name = "_lblTextos_6";
            this._lblTextos_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_6.Size = new System.Drawing.Size(40, 18);
            this._lblTextos_6.TabIndex = 14;
            this._lblTextos_6.Text = "Desde:";
            // 
            // _cmdBotones_2
            // 
            this._cmdBotones_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._cmdBotones_2.Location = new System.Drawing.Point(554, 356);
            this._cmdBotones_2.Name = "_cmdBotones_2";
            this._cmdBotones_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cmdBotones_2.Size = new System.Drawing.Size(81, 35);
            this._cmdBotones_2.TabIndex = 30;
            this._cmdBotones_2.Text = "&Salir";
            this._cmdBotones_2.Click += new System.EventHandler(this.cmdBotones_Click);
            // 
            // _cmdBotones_1
            // 
            this._cmdBotones_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._cmdBotones_1.Location = new System.Drawing.Point(466, 356);
            this._cmdBotones_1.Name = "_cmdBotones_1";
            this._cmdBotones_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cmdBotones_1.Size = new System.Drawing.Size(81, 35);
            this._cmdBotones_1.TabIndex = 29;
            this._cmdBotones_1.Text = "&Cancelar";
            this._cmdBotones_1.Click += new System.EventHandler(this.cmdBotones_Click);
            // 
            // _cmdBotones_0
            // 
            this._cmdBotones_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._cmdBotones_0.Enabled = false;
            this._cmdBotones_0.Location = new System.Drawing.Point(379, 356);
            this._cmdBotones_0.Name = "_cmdBotones_0";
            this._cmdBotones_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cmdBotones_0.Size = new System.Drawing.Size(81, 35);
            this._cmdBotones_0.TabIndex = 28;
            this._cmdBotones_0.Text = "&Aceptar";
            this._cmdBotones_0.Click += new System.EventHandler(this.cmdBotones_Click);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.txtDatos);
            this.Frame3.Controls.Add(this._sdcFechas_1);
            this.Frame3.Controls.Add(this._sdcFechas_0);
            this.Frame3.Controls.Add(this._tbHora_0);
            this.Frame3.Controls.Add(this._tbHora_1);
            this.Frame3.Controls.Add(this._lblTextos_5);
            this.Frame3.Controls.Add(this._lblTextos_4);
            this.Frame3.Controls.Add(this._lblTextos_3);
            this.Frame3.Controls.Add(this._lblTextos_2);
            this.Frame3.HeaderText = "";
            this.Frame3.Location = new System.Drawing.Point(8, 186);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(627, 163);
            this.Frame3.TabIndex = 4;
            // 
            // txtDatos
            // 
            this.txtDatos.AcceptsReturn = true;
            this.txtDatos.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDatos.Enabled = false;
            this.txtDatos.Location = new System.Drawing.Point(16, 62);
            this.txtDatos.MaxLength = 900;
            this.txtDatos.Multiline = true;
            this.txtDatos.Name = "txtDatos";
            this.txtDatos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDatos.Size = new System.Drawing.Size(597, 91);
            this.txtDatos.TabIndex = 25;
            this.txtDatos.TextChanged += new System.EventHandler(this.txtDatos_TextChanged);
            this.txtDatos.Enter += new System.EventHandler(this.txtDatos_Enter);
            // 
            // _sdcFechas_1
            // 
            this._sdcFechas_1.CustomFormat = "dd/MM/yyyy";
            this._sdcFechas_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._sdcFechas_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcFechas_1.Location = new System.Drawing.Point(88, 37);
            this._sdcFechas_1.Name = "_sdcFechas_1";
            this._sdcFechas_1.Size = new System.Drawing.Size(119, 18);
            this._sdcFechas_1.TabIndex = 23;
            this._sdcFechas_1.TabStop = false;
            this._sdcFechas_1.Click += new System.EventHandler(this.sdcFechas_Click);
            // 
            // _sdcFechas_0
            // 
            this._sdcFechas_0.CustomFormat = "dd/MM/yyyy";
            this._sdcFechas_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._sdcFechas_0.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcFechas_0.Location = new System.Drawing.Point(88, 12);
            this._sdcFechas_0.Name = "_sdcFechas_0";
            this._sdcFechas_0.Size = new System.Drawing.Size(119, 18);
            this._sdcFechas_0.TabIndex = 21;
            this._sdcFechas_0.TabStop = false;
            this._sdcFechas_0.Click += new System.EventHandler(this.sdcFechas_Click);
            // 
            // _tbHora_0
            // 
            this._tbHora_0.Location = new System.Drawing.Point(214, 12);
            this._tbHora_0.Mask = "##:##";
            this._tbHora_0.Name = "_tbHora_0";
            this._tbHora_0.PromptChar = ' ';
            this._tbHora_0.Size = new System.Drawing.Size(42, 20);
            this._tbHora_0.TabIndex = 22;
            this._tbHora_0.TabStop = false;
            this._tbHora_0.Enter += new System.EventHandler(this.tbHora_Enter);
            this._tbHora_0.Leave += new System.EventHandler(this.tbHora_Leave);
            // 
            // _tbHora_1
            // 
            this._tbHora_1.Location = new System.Drawing.Point(214, 36);
            this._tbHora_1.Mask = "##:##";
            this._tbHora_1.Name = "_tbHora_1";
            this._tbHora_1.PromptChar = ' ';
            this._tbHora_1.Size = new System.Drawing.Size(42, 20);
            this._tbHora_1.TabIndex = 24;
            this._tbHora_1.TabStop = false;
            this._tbHora_1.Enter += new System.EventHandler(this.tbHora_Enter);
            this._tbHora_1.Leave += new System.EventHandler(this.tbHora_Leave);
            // 
            // _lblTextos_5
            // 
            this._lblTextos_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_5.Enabled = false;
            this._lblTextos_5.Location = new System.Drawing.Point(324, 38);
            this._lblTextos_5.Name = "_lblTextos_5";
            this._lblTextos_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_5.Size = new System.Drawing.Size(289, 20);
            this._lblTextos_5.TabIndex = 9;
            // 
            // _lblTextos_4
            // 
            this._lblTextos_4.AutoSize = false;
            this._lblTextos_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_4.Location = new System.Drawing.Point(270, 28);
            this._lblTextos_4.Name = "_lblTextos_4";
            this._lblTextos_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_4.Size = new System.Drawing.Size(58, 36);
            this._lblTextos_4.TabIndex = 8;
            this._lblTextos_4.Text = "Usuario / M�dico:";
            // 
            // _lblTextos_3
            // 
            this._lblTextos_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_3.Location = new System.Drawing.Point(16, 38);
            this._lblTextos_3.Name = "_lblTextos_3";
            this._lblTextos_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_3.Size = new System.Drawing.Size(55, 18);
            this._lblTextos_3.TabIndex = 6;
            this._lblTextos_3.Text = "Fin Pauta:";
            // 
            // _lblTextos_2
            // 
            this._lblTextos_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_2.Location = new System.Drawing.Point(16, 14);
            this._lblTextos_2.Name = "_lblTextos_2";
            this._lblTextos_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_2.Size = new System.Drawing.Size(67, 18);
            this._lblTextos_2.TabIndex = 5;
            this._lblTextos_2.Text = "Inicio Pauta:";
            // 
            // lbUrgencias
            // 
            this.lbUrgencias.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUrgencias.Location = new System.Drawing.Point(53, 388);
            this.lbUrgencias.Name = "lbUrgencias";
            this.lbUrgencias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUrgencias.Size = new System.Drawing.Size(153, 18);
            this.lbUrgencias.TabIndex = 31;
            this.lbUrgencias.Text = "Apuntes de urgencias o H.d�a";
            this.lbUrgencias.Visible = false;
            // 
            // _lblTextos_8
            // 
            this._lblTextos_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_8.Location = new System.Drawing.Point(52, 372);
            this._lblTextos_8.Name = "_lblTextos_8";
            this._lblTextos_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_8.Size = new System.Drawing.Size(97, 18);
            this._lblTextos_8.TabIndex = 18;
            this._lblTextos_8.Text = "Apuntes inactivos.";
            // 
            // _lblTextos_7
            // 
            this._lblTextos_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_7.Location = new System.Drawing.Point(52, 356);
            this._lblTextos_7.Name = "_lblTextos_7";
            this._lblTextos_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_7.Size = new System.Drawing.Size(88, 18);
            this._lblTextos_7.TabIndex = 17;
            this._lblTextos_7.Text = "Apuntes activos.";
            // 
            // _lblTextos_10
            // 
            this._lblTextos_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_10.Enabled = false;
            this._lblTextos_10.Location = new System.Drawing.Point(566, 24);
            this._lblTextos_10.Name = "_lblTextos_10";
            this._lblTextos_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_10.Size = new System.Drawing.Size(71, 20);
            this._lblTextos_10.TabIndex = 20;
            // 
            // _lblTextos_9
            // 
            this._lblTextos_9.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_9.Location = new System.Drawing.Point(526, 24);
            this._lblTextos_9.Name = "_lblTextos_9";
            this._lblTextos_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_9.Size = new System.Drawing.Size(37, 18);
            this._lblTextos_9.TabIndex = 19;
            this._lblTextos_9.Text = "Cama:";
            // 
            // _lblTextos_1
            // 
            this._lblTextos_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_1.Enabled = false;
            this._lblTextos_1.Location = new System.Drawing.Point(72, 24);
            this._lblTextos_1.Name = "_lblTextos_1";
            this._lblTextos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_1.Size = new System.Drawing.Size(439, 20);
            this._lblTextos_1.TabIndex = 2;
            // 
            // _lblTextos_0
            // 
            this._lblTextos_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblTextos_0.Location = new System.Drawing.Point(16, 24);
            this._lblTextos_0.Name = "_lblTextos_0";
            this._lblTextos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblTextos_0.Size = new System.Drawing.Size(51, 18);
            this._lblTextos_0.TabIndex = 1;
            this._lblTextos_0.Text = "Paciente:";
            // 
            // frmPauCorr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 476);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPauCorr";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Pauta Correctora Glucemia";
            this.Activated += new System.EventHandler(this.frmPauCorr_Activated);
            this.Closed += new System.EventHandler(this.frmPauCorr_Closed);
            this.Load += new System.EventHandler(this.frmPauCorr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vasPautas.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vasPautas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTodos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._optVer_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._optVer_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcFechas_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cmdBotones_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcFechas_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcFechas_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbHora_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tbHora_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUrgencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblTextos_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializetbHora();
			InitializesdcFechas();
			InitializeoptVer();
			InitializelblTextos();
			InitializecmdBotones();
			InitializeLine1();
		}
		void InitializetbHora()
		{
			this.tbHora = new Telerik.WinControls.UI.RadMaskedEditBox[2];
			this.tbHora[0] = _tbHora_0;
			this.tbHora[1] = _tbHora_1;
		}
		void InitializesdcFechas()
		{
			this.sdcFechas = new Telerik.WinControls.UI.RadDateTimePicker[3];
			this.sdcFechas[2] = _sdcFechas_2;
			this.sdcFechas[1] = _sdcFechas_1;
			this.sdcFechas[0] = _sdcFechas_0;
		}
		void InitializeoptVer()
		{
			this.optVer = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optVer[1] = _optVer_1;
			this.optVer[0] = _optVer_0;
		}
		void InitializelblTextos()
		{
			this.lblTextos = new Telerik.WinControls.UI.RadLabel[11];
			this.lblTextos[6] = _lblTextos_6;
			//this.lblTextos[5] = _lblTextos_5;
			this.lblTextos[4] = _lblTextos_4;
			this.lblTextos[3] = _lblTextos_3;
			this.lblTextos[2] = _lblTextos_2;
			this.lblTextos[8] = _lblTextos_8;
			this.lblTextos[7] = _lblTextos_7;
			//this.lblTextos[10] = _lblTextos_10;
			this.lblTextos[9] = _lblTextos_9;
			//this.lblTextos[1] = _lblTextos_1;
			this.lblTextos[0] = _lblTextos_0;
		}
		void InitializecmdBotones()
		{
			this.cmdBotones = new Telerik.WinControls.UI.RadButton[5];
			this.cmdBotones[4] = _cmdBotones_4;
			this.cmdBotones[3] = _cmdBotones_3;
			this.cmdBotones[2] = _cmdBotones_2;
			this.cmdBotones[1] = _cmdBotones_1;
			this.cmdBotones[0] = _cmdBotones_0;
		}
		void InitializeLine1()
		{
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[2];
			this.Line1[0] = _Line1_0;
			this.Line1[1] = _Line1_1;
		}
		#endregion
	}
}