using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PautasCorrectoras
{
	public partial class frmPlanPauCorr
		: RadForm
	{

		public frmPlanPauCorr()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void frmPlanPauCorr_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		private const int COLUMNA_DPLANPAUC = 1;

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{

			//Dim iContador As Integer
			BGPautas.stPautaCorrectora = "";
			BGPautas.stPautaCorrectora = TxtDesc.Text.Trim();
			this.Close(); //una vez que se guardan los datos, desaparece el formulario
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void frmPlanPauCorr_Load(Object eventSender, EventArgs eventArgs)
		{

			RellenarGridPlantillasPAUC();
			BGPautas.stPautaCorrectora = "";
			//para la ayuda
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
		}



		//en el grid se cargan los registros de la tabla DPLANPAUC (las plantillas de Pautas Correctoras)
		private void RellenarGridPlantillasPAUC()
		{

			string stSql = String.Empty;
			DataSet RrSQL = null;

			try
			{

				this.Cursor = Cursors.WaitCursor;
				sprPlantillasPAU.MaxRows = 0;
				stSql = "SELECT gplanpauc, dplanpauc FROM DPLANPAUC ORDER BY dplanpauc";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BGPautas.RcEnfermeria);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);

				while (RrSQL.Tables[0].Rows.Count != 0)
				{
					sprPlantillasPAU.MaxRows++;
					sprPlantillasPAU.Row = sprPlantillasPAU.MaxRows;

					sprPlantillasPAU.Col = COLUMNA_DPLANPAUC;
					//sprPlantillasPAU.CellType = (FarPoint.Win.Spread.CellType.BaseCellType) 1;
					sprPlantillasPAU.setTypeEditLen(900);
					sprPlantillasPAU.Text = Convert.ToString(RrSQL.Tables[0].Rows[0]["dplanpauc"]).Trim();
					sprPlantillasPAU.Lock = true;

					RrSQL.MoveNext();
				}

				RrSQL.Close();

				//Si no hay ninguna plantilla definida, no se permite hacer nada en el formulario
				if (sprPlantillasPAU.MaxRows == 0)
				{
					cbAceptar.Enabled = false;
				}
				else
				{
                    var currentRow = sprPlantillasPAU.Rows[sprPlantillasPAU.Row].Cells[sprPlantillasPAU.Col].RowInfo;
                    var currentCol = sprPlantillasPAU.Rows[sprPlantillasPAU.Row].Cells[sprPlantillasPAU.Col].ColumnInfo;
                    sprPlantillasPAU_CellClick(sprPlantillasPAU, new GridViewCellEventArgs(currentRow, currentCol, null));
				}

				this.Cursor = Cursors.Default;
			}
			catch(Exception ex)
			{
				this.Cursor = Cursors.Default;
				Serrores.AnalizaError("Pautas Correctoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "RellenarGridPlantillasPAUC:PautasCorrectoras.dll", ex);
			}

		}


		private void sprPlantillasPAU_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;
			sprPlantillasPAU.Col = Col;
			sprPlantillasPAU.Row = Row;
			TxtDesc.Text = sprPlantillasPAU.Text;
		}
		private void frmPlanPauCorr_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}