using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace PautasCorrectoras
{
	public partial class frmPauCorr
		: RadForm
	{



		//Variables para referenciar la clase y la pantalla
		object objPantalla = null;
		MCPautasCorrectoras mcClase = null;

		//Variables pasadas a la Dll
		string strPaciente = String.Empty; // Paciente
		string strServicio = String.Empty; // Tipo servicio
		int intAnio = 0; // A�o del registro
		int lngNumero = 0; // Numero del registro
		string strgidenpac = String.Empty;


		string Formato_Fecha_SQL = String.Empty;
		string Separador_Fecha_SQL = String.Empty;
		string Separador_Hora_SQL = String.Empty;
		string lSep_Decimal = String.Empty;
		string lSep_Mil = String.Empty;



		//variables para la columna del grid.
		int Col_IniPeriodo = 0;
		int Col_FinPeriodo = 0;
		int Col_Pauta = 0;
		int Col_Medico = 0;
		int Col_Usuario = 0;
		int Col_FecSistema = 0;
		int Col_FecModi = 0;
		int Col_UsuModi = 0;
		int Col_Activar = 0;

		//Variables de aplicacion
		DataSet rdoDatos = null;
		bool blnCambios = false;
		bool blnNuevo = false;
		bool blnActivos = false;
		bool blnTodos = false;

		string stFechaLLegada = String.Empty;
		public frmPauCorr()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}



		public void REFERENCIAS(object ifPAntalla, MCPautasCorrectoras iClase, SqlConnection iConnet, string stParam1, string stParam2, string stParam3, string stParam4, string strcama)
		{
			string sql = String.Empty;
			DataSet RR = null;

			objPantalla = ifPAntalla;
			mcClase = iClase;
			BGPautas.RcEnfermeria = iConnet;


			strPaciente = stParam1;
			strServicio = stParam2.Substring(0, Math.Min(1, stParam2.Length));
			intAnio = Convert.ToInt32(Double.Parse(stParam2.Substring(1, Math.Min(4, stParam2.Length - 1))));
			lngNumero = Convert.ToInt32(Double.Parse(stParam2.Substring(5, Math.Min(6, stParam2.Length - 5))));


			//Oscar C Diciembre 2011
			switch(strServicio)
			{
				case "H" : case "U" : 
					if (strServicio == "H")
					{
						sql = "SELECT fllegada FROM AEPISADM WHERE ganoadme = " + intAnio.ToString() + " AND gnumadme = " + lngNumero.ToString();
					}
					else
					{
						sql = "SELECT fllegada FROM UEPISURG WHERE ganourge = " + intAnio.ToString() + " AND gnumurge = " + lngNumero.ToString();
					} 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, iConnet); 
					RR = new DataSet(); 
					tempAdapter.Fill(RR); 
					stFechaLLegada = Convert.ToDateTime(RR.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm:ss"); 
					RR.Close(); 
					break;
			}
			//-------------

			strgidenpac = stParam1;

			//stParam4 = Ususaruo
			BGPautas.gUsuario = stParam4;


            //stParam3 = Nombre Paciente
            _lblTextos_1.Text = stParam3;

            _lblTextos_10.Text = strcama;

			lnUrgencias.Visible = (strServicio == "H");
			lbUrgencias.Visible = (strServicio == "H");

		}


		private void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{

			RecuperaDatos((optVer[0].IsChecked) ? 1 : 2);

		}

		private void cmdBotones_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cmdBotones, eventSender);

			string SqlSt = String.Empty;
			DataSet RrSqlSt = null;

			cmdBotones[4].Enabled = true;
			if (Index == 0)
			{ //Aceptar

				//'' Comprobar si el paciente ya ha sido dado de alta en cualquier modulo  por otro usuario

				switch(strServicio)
				{
					case "U" : 
						SqlSt = "Select faltaurg from uepisurg where ganourge = " + intAnio.ToString() + " and gnumurge = " + lngNumero.ToString(); 
						SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlSt, BGPautas.RcEnfermeria); 
						RrSqlSt = new DataSet(); 
						tempAdapter.Fill(RrSqlSt); 
						if (RrSqlSt.Tables[0].Rows.Count != 0)
						{
							if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltaurg"]))
							{
								RadMessageBox.Show("Paciente de alta en Urgencias", Application.ProductName);
								RrSqlSt.Close();
								return;
							}
						} 
						RrSqlSt.Close(); 
						break;
					case "H" : 
						SqlSt = "Select faltplan from aepisadm where ganoadme = " + intAnio.ToString() + " and gnumadme = " + lngNumero.ToString(); 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SqlSt, BGPautas.RcEnfermeria); 
						RrSqlSt = new DataSet(); 
						tempAdapter_2.Fill(RrSqlSt); 
						if (RrSqlSt.Tables[0].Rows.Count != 0)
						{
							if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltplan"]))
							{
								RadMessageBox.Show("Paciente de alta en Hospitalizaci�n", Application.ProductName);
								RrSqlSt.Close();
								return;
							}
						} 
						RrSqlSt.Close(); 
						break;
				}


				if (fValida())
				{
					if (fActualizaDAtos())
					{

						if (optVer[0].IsChecked)
						{
							RecuperaDatos(1);
						}
						else if (optVer[1].IsChecked)
						{ 
							RecuperaDatos(2);
						}
						LimpiaControles();
						ModoControles(true);
						cmdBotones[0].Enabled = false;
						//cmdBotones(1).Enabled = False
						blnNuevo = true;
					}
				}

			}
			else if (Index == 1)
			{  //Cancelar

				if (optVer[0].IsChecked)
				{
					RecuperaDatos(1);
				}
				else if (optVer[1].IsChecked)
				{ 
					RecuperaDatos(2);
				}
				LimpiaControles();
				ModoControles(true);
				cmdBotones[0].Enabled = false;
				//cmdBotones(1).Enabled = False
				cmdBotones[3].Enabled = false;
				blnNuevo = true;

			}
			else if (Index == 2)
			{  //Salir

				this.Close();
				blnNuevo = true;


			}
			else if (Index == 3)
			{  //Activaci�n

				//(maplaza)(26/09/2006)
				//'' Comprobar si el paciente ya ha sido dado de alta en cualquier modulo  por otro usuario
				switch(strServicio)
				{
					case "U" : 
						SqlSt = "Select faltaurg from uepisurg where ganourge = " + intAnio.ToString() + " and gnumurge = " + lngNumero.ToString(); 
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SqlSt, BGPautas.RcEnfermeria); 
						RrSqlSt = new DataSet(); 
						tempAdapter_3.Fill(RrSqlSt); 
						if (RrSqlSt.Tables[0].Rows.Count != 0)
						{
							if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltaurg"]))
							{
								RadMessageBox.Show("Paciente de alta en Urgencias", Application.ProductName);
								RrSqlSt.Close();
								return;
							}
						} 
						RrSqlSt.Close(); 
						break;
					case "H" : 
						SqlSt = "Select faltplan from aepisadm where ganoadme = " + intAnio.ToString() + " and gnumadme = " + lngNumero.ToString(); 
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SqlSt, BGPautas.RcEnfermeria); 
						RrSqlSt = new DataSet(); 
						tempAdapter_4.Fill(RrSqlSt); 
						if (RrSqlSt.Tables[0].Rows.Count != 0)
						{
							if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltplan"]))
							{
								RadMessageBox.Show("Paciente de alta en Hospitalizaci�n", Application.ProductName);
								RrSqlSt.Close();
								return;
							}
						} 
						RrSqlSt.Close(); 
						break;
				}
				//-----

				if (RadMessageBox.Show("Desea realmente activar pauta correctora.", "Activaci�n", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
				{

					if (fActualizaDAtos(true))
					{

						if (optVer[0].IsChecked)
						{
							RecuperaDatos(1);
						}
						else if (optVer[1].IsChecked)
						{ 
							RecuperaDatos(2);
						}
						LimpiaControles();
						ModoControles(true);
						cmdBotones[0].Enabled = false;
						//cmdBotones(1).Enabled = False
						blnNuevo = true;

					}

				}

				cmdBotones[3].Enabled = false;

			}
			else if (Index == 4)
			{ 

				//Plantillas de Pautas correctoras
				frmPlanPauCorr.DefInstance.ShowDialog();
				txtDatos.Text = BGPautas.stPautaCorrectora;

			}

		}

		private void chkTodos_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{

			blnTodos = chkTodos.CheckState != CheckState.Unchecked;

			RecuperaDatos((optVer[0].IsChecked) ? 1 : 2);

		}

		private void frmPauCorr_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				try
				{
					//Aplicar el control de acceso a la pantalla
					if (!mbAcceso.AplicarControlAcceso(BGPautas.gUsuario, strServicio, this, "frmPauCorr", ref BGPautas.astrcontroles, ref BGPautas.astreventos, BGPautas.RcEnfermeria))
					{
						return;
					}


					return;
				}
				catch(Exception ex)
				{
					Serrores.AnalizaError("Pautas Correctoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "Form_Activate:PautasCorrectoras.dll", ex);
					this.Cursor = Cursors.Default;
				}
			}
		}

		private void frmPauCorr_Load(Object eventSender, EventArgs eventArgs)
		{
			bool error = false;
			DataSet RdoFecha = null;
			string SqlFecha = String.Empty;

			try
			{
				error = true;


				ObtenerValoresTipoBD();
				ObtenerLocale();

				BGPautas.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(BGPautas.RcEnfermeria);

				BGPautas.stDecimalBD = Serrores.ObtenerSeparadorDecimalBD(BGPautas.RcEnfermeria);

				for (int intContador = 0; intContador <= 2; intContador++)
				{
					//sdcFechas[intContador].setDateSeparator(BGPautas.lSep_Fecha);
					sdcFechas[intContador].CustomFormat = BGPautas.lFor_Fecha;
					sdcFechas[intContador].NullText = ("__" + BGPautas.lSep_Fecha + "__" + BGPautas.lSep_Fecha + "____");
					//sdcFechas[intContador].setShowCentury(true);
				}
				Serrores.Obtener_TipoBD_FormatoFechaBD(ref BGPautas.RcEnfermeria);

				Col_IniPeriodo = 1;
				Col_FinPeriodo = 2;
				Col_Pauta = 3;
				Col_Medico = 4;
				Col_Usuario = 5;
				Col_FecSistema = 6;
				Col_UsuModi = 7;
				Col_FecModi = 8;
				Col_Activar = 9;

				blnCambios = false;
				blnNuevo = true;

				optVer[0].IsChecked = true;
				optVer[1].IsChecked = false;
				chkTodos.CheckState = CheckState.Unchecked;
				sdcFechas[2].Text = "";

				sdcFechas[2].Text = "";
				//sdcFechas[2].setShowCentury(true);
				sdcFechas[2].MinDate = DateTime.Parse("01/01/999");
				sdcFechas[2].MaxDate = DateTime.Today;
				SqlFecha = "SELECT ISNULL(NNUMERI1,0) FROM SCONSGLO WHERE GCONSGLO='NDIASENF'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlFecha, BGPautas.RcEnfermeria);
				RdoFecha = new DataSet();
				tempAdapter.Fill(RdoFecha);
				if (RdoFecha.Tables[0].Rows.Count == 0)
				{
					sdcFechas[2].Value = DateTime.Today;
				}
				else
				{
					sdcFechas[2].Value = DateTime.Today.AddDays(-1 * Convert.ToDouble(RdoFecha.Tables[0].Rows[0][0]));
				}
				RdoFecha.Close();

				RdoFecha = null;

				if (optVer[0].IsChecked)
				{
					RecuperaDatos(1);
				}
				else
				{
					RecuperaDatos(2);
				}

                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}


				if (error)
				{

					Serrores.AnalizaError("Pautas Correctoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "Form_Load:PautasCorrectoras.dll", excep);

				}
			}



		}


		private bool isInitializingComponent;
		private void optVer_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}

				blnActivos = optVer[0].IsChecked;

				RecuperaDatos((optVer[0].IsChecked) ? 1 : 2);

			}
		}

		private void sdcFechas_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.sdcFechas, eventSender);

			if (Index < 2)
			{
				blnCambios = true;
				cmdBotones[0].Enabled = true;
				//cmdBotones(1).Enabled = True
			}

		}

		private void sdcFechas_Change(int Index)
		{

			if (Index < 2)
			{
				blnCambios = true;
				cmdBotones[0].Enabled = true;
				//cmdBotones(1).Enabled = True
			}

		}

		private void tbHora_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbHora, eventSender);
			tbHora[Index].SelectionStart = 0;
			tbHora[Index].SelectionLength = Strings.Len(tbHora[Index].Text);

		}

		private void tbHora_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbHora, eventSender);
			int iHora = 0;
			int iMinutos = 0;
			int irespuesta = 0;

			if (tbHora[Index].Text.Trim() != "" && tbHora[Index].Text.Trim() != ":")
			{
				iHora = Convert.ToInt32(Conversion.Val(tbHora[Index].Text.Substring(0, Math.Min(tbHora[Index].Text.IndexOf(':'), tbHora[Index].Text.Length))));
				iMinutos = Convert.ToInt32(Conversion.Val(tbHora[Index].Text.Substring(tbHora[Index].Text.IndexOf(':') + 1, Math.Min(2, tbHora[Index].Text.Length - (tbHora[Index].Text.IndexOf(':') + 1)))));

				if (iHora < 0 || iHora > 23)
				{
					short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[]{"Hora"};
					BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BGPautas.RcEnfermeria, tempRefParam2);
					//MsgBox "la hora tecleada no es correcta", vbOKOnly & vbCritical, Me.Caption
					tbHora[Index].Focus();
					return;
				}
				if (iMinutos < 0 || iMinutos > 59)
				{
					short tempRefParam3 = 1150;
					string[] tempRefParam4 = new string[]{"Hora"};
					BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BGPautas.RcEnfermeria, tempRefParam4);
					//MsgBox "los minutos tecleados no son correctos", vbOKOnly & vbCritical, Me.Caption
					tbHora[Index].Focus();
					return;
				}
			}
		}

		private void txtDatos_TextChanged(Object eventSender, EventArgs eventArgs)
		{

			blnCambios = true;
			cmdBotones[0].Enabled = true;
			//cmdBotones(1).Enabled = True

		}

		private void txtDatos_Enter(Object eventSender, EventArgs eventArgs)
		{
			txtDatos.SelectionStart = 0;
			txtDatos.SelectionLength = Strings.Len(txtDatos.Text);
		}

		private void vasPautas_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;

			blnNuevo = false;

			if (Row > 0)
			{

				cmdBotones[3].Enabled = false;

				vasPautas.Row = Row;


				vasPautas.Col = Col_IniPeriodo;
				//sdcFechas(0).Text = vasPautas.Text
				sdcFechas[0].Text = vasPautas.Text.Substring(0, Math.Min(10, vasPautas.Text.Length));
				tbHora[0].Text = vasPautas.Text.Substring(11, Math.Min(5, vasPautas.Text.Length - 11));
				if (vasPautas.Text.Trim() != "")
				{
					sdcFechas[1].MinDate = DateTime.Parse(vasPautas.Text);
				}

				vasPautas.Col = Col_FinPeriodo;
				//sdcFechas(1).Text = vasPautas.Text
				if (vasPautas.Text.Trim() == "")
				{
					sdcFechas[1].Text = "";
					tbHora[1].Text = "  :  ";
				}
				else
				{
					sdcFechas[1].Text = vasPautas.Text.Substring(0, Math.Min(10, vasPautas.Text.Length));
					tbHora[1].Text = vasPautas.Text.Substring(11, Math.Min(5, vasPautas.Text.Length - 11));
				}

				vasPautas.Col = Col_Medico;
                _lblTextos_5.Text = vasPautas.Text;

				vasPautas.Col = Col_Pauta;
				txtDatos.Text = vasPautas.Text;

				txtDatos.Enabled = sdcFechas[1].Text.Trim() == "";

				if (sdcFechas[1].Text.Trim() == "" && ColorTranslator.ToOle(vasPautas.ForeColor) == 0x0)
				{
					ModoControles(true);
					cmdBotones[0].Enabled = true;
					//cmdBotones(1).Enabled = True
					cmdBotones[4].Enabled = true;
				}
				else
				{
					ModoControles(false);
					cmdBotones[0].Enabled = false;
					//cmdBotones(1).Enabled = True
					cmdBotones[4].Enabled = false;

					vasPautas.Col = Col_Activar;
					if (vasPautas.Text == "1" && ColorTranslator.ToOle(vasPautas.ForeColor) == 0x0)
					{
						cmdBotones[3].Enabled = true;
					}
					vasPautas.Col = Col_Pauta;
				}

				//Oscar C Diciembre 2011
				if (ColorTranslator.ToOle(vasPautas.ForeColor) == ColorTranslator.ToOle(lnUrgencias.BorderColor))
				{
					ModoControles(false);
					cmdBotones[0].Enabled = false;
					cmdBotones[1].Enabled = false;
					cmdBotones[3].Enabled = false;
					cmdBotones[4].Enabled = false;
				}
				//---------------

			}
		}


		private bool fActualizaDAtos(bool blnActiva = false)
		{
			bool ErrActuali = false;

			bool result = false;
			string strsql = String.Empty;

			try
			{
				ErrActuali = true;

				result = true;

				strsql = "SELECT * " + 
				         "FROM EPAUCORR " + 
				         "WHERE itiposer = '" + strServicio + "' And " + 
				         "ganoregi = " + intAnio.ToString() + " And " + 
				         "gnumregi = " + lngNumero.ToString() + " " + 
				         "order by fapuntes desc ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(strsql, BGPautas.RcEnfermeria);
				rdoDatos = new DataSet();
				tempAdapter.Fill(rdoDatos);


				if (!blnActiva)
				{
					if (blnNuevo)
					{
						if (rdoDatos.Tables[0].Rows.Count != 0)
						{
							if (Convert.IsDBNull(rdoDatos.Tables[0].Rows[0]["fecfinal"]))
							{
								rdoDatos.Edit();
								//(maplaza)(13/09/2006)Se produc�a un error representado por el siguiente caso (por ejemplo):
								//1-) Se registra una pauta correctora de hoy (12/09) en adelante sin fecha fin.
								//2-) Se registra otra de fechas entre el 15/09 y el 25/09. Al registrar esta �ltima me cierra
								//la primera pero con fechas entre el 12/09 y el 25/09 (est� ultima deber�a ser el 14/09, es decir,
								//la fecha de inicio de la nueva menos un d�a)
								//rdoDatos("fecfinal") = IIf(Trim(sdcFechas(1).Text) = "", Format(CDate(sdcFechas(0).Text) - 1, "dd/mm/yyyy HH:NN"), Format(sdcFechas(1).Text, "dd/mm/yyyy HH:NN"))
								//rdoDatos("fecfinal") = Format(CDate(sdcFechas(0).Text) - 1, "dd/mm/yyyy HH:NN")
								//como tengo los minutos, segundos ..
								rdoDatos.Tables[0].Rows[0]["fecfinal"] = DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text + ":00").AddMinutes(-1);
								//---
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                                tempAdapter.Update(rdoDatos, rdoDatos.Tables[0].TableName);
							}
						}
						rdoDatos.AddNew();
						rdoDatos.Tables[0].Rows[0]["itiposer"] = strServicio;
						rdoDatos.Tables[0].Rows[0]["ganoregi"] = intAnio;
						rdoDatos.Tables[0].Rows[0]["gnumregi"] = lngNumero;
						//rdoDatos("fapuntes") = Format(sdcFechas(0).Text, "dd/mm/yyyy HH:NN")
						rdoDatos.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text + ":00");
						if (sdcFechas[1].Text.Trim() == "")
						{
							rdoDatos.Tables[0].Rows[0]["fecfinal"] = DBNull.Value;
						}
						else
						{
							rdoDatos.Tables[0].Rows[0]["fecfinal"] = DateTime.Parse(sdcFechas[1].Text + " " + tbHora[1].Text + ":00");
						}
						//rdoDatos("fecfinal") = IIf(Trim(sdcFechas(1).Text) = "", Null, Format(sdcFechas(1).Text, "dd/mm/yyyy HH:NN"))
						rdoDatos.Tables[0].Rows[0]["opautaco"] = txtDatos.Text;
						rdoDatos.Tables[0].Rows[0]["gUsuario"] = BGPautas.gUsuario;
						rdoDatos.Tables[0].Rows[0]["fsistema"] = DateTime.Now;
						SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
						tempAdapter.Update(rdoDatos, rdoDatos.Tables[0].TableName);
					}
					else
					{
						if (rdoDatos.Tables[0].Rows.Count != 0)
						{
							rdoDatos.Edit();
							rdoDatos.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text + ":00");
							if (sdcFechas[1].Text.Trim() == "")
							{
								rdoDatos.Tables[0].Rows[0]["fecfinal"] = DBNull.Value;
							}
							else
							{
								rdoDatos.Tables[0].Rows[0]["fecfinal"] = DateTime.Parse(sdcFechas[1].Text + " " + tbHora[1].Text + ":00");
							}
							//rdoDatos("fapuntes") = Format(sdcFechas(0).Text, "dd/mm/yyyy HH:NN")
							//rdoDatos("fecfinal") = IIf(Trim(sdcFechas(1).Text) = "", Null, Format(sdcFechas(1).Text, "dd/mm/yyyy HH:NN"))
							rdoDatos.Tables[0].Rows[0]["opautaco"] = txtDatos.Text;
							rdoDatos.Tables[0].Rows[0]["gUsuario"] = BGPautas.gUsuario;
							rdoDatos.Tables[0].Rows[0]["fsistema"] = DateTime.Now;
							SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter);
							tempAdapter.Update(rdoDatos, rdoDatos.Tables[0].TableName);
						}
					}
				}
				else
				{
					if (rdoDatos.Tables[0].Rows.Count != 0)
					{
						rdoDatos.Edit();
						rdoDatos.Tables[0].Rows[0]["fecfinal"] = DBNull.Value;
						rdoDatos.Tables[0].Rows[0]["gUsuario"] = BGPautas.gUsuario;
						rdoDatos.Tables[0].Rows[0]["fsistema"] = DateTime.Now;
						SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter);
						tempAdapter.Update(rdoDatos, rdoDatos.Tables[0].TableName);
					}
				}
				rdoDatos.Close();

				rdoDatos = null;

				ErrActuali = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!ErrActuali)
				{
					throw excep;
				}

				if (ErrActuali)
				{

					result = false;
					Serrores.AnalizaError("Pautas Correctoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "Form_Load:PautasCorrectoras.dll", excep);


					return result;
				}
			}
			return false;
		}

		private bool fValida()
		{

			bool result = false;
			result = true;

			int intFinal = (sdcFechas[1].Text.Trim() == "") ? 0 : 1;

			for (int intContador = 0; intContador <= intFinal; intContador++)
			{
				if (!Information.IsDate(sdcFechas[intContador].Text))
				{
					result = false;
				}
			}

			if (!result)
			{
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{"Inicio Pauta"};
				BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BGPautas.RcEnfermeria, tempRefParam2);
				return result;
			}

			//se debe validar que se ha introducido una hora correcta
			//debe introducir una hora
			if (tbHora[0].Text.Trim() == ":")
			{
				short tempRefParam3 = 1040;
				string[] tempRefParam4 = new string[]{"Hora inicio pauta"};
				BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BGPautas.RcEnfermeria, tempRefParam4);
				result = false;
				tbHora[0].Focus();
				return result;
			}
			else
			{
				if (!Information.IsDate(tbHora[0].Text))
				{
					short tempRefParam5 = 1150;
					string[] tempRefParam6 = new string[]{"Hora inicio pauta"};
					BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, BGPautas.RcEnfermeria, tempRefParam6);
					result = false;
					tbHora[0].Focus();
					return result;
				}
			}
			if (sdcFechas[1].Text.Trim() != "")
			{
				//hay que validar que la hora sea correcta
				if (tbHora[1].Text.Trim() == ":")
				{
					short tempRefParam7 = 1040;
					string[] tempRefParam8 = new string[]{"Hora fin pauta"};
					BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, BGPautas.RcEnfermeria, tempRefParam8);
					result = false;
					tbHora[1].Focus();
					return result;
				}
				else
				{
					if (!Information.IsDate(tbHora[1].Text))
					{
						short tempRefParam9 = 1150;
						string[] tempRefParam10 = new string[]{"Hora fin pauta"};
						BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, BGPautas.RcEnfermeria, tempRefParam10);
						result = false;
						tbHora[1].Focus();
						return result;
					}
				}
			}
			else
			{
				//si la fecha esta vacia, la hora no debe estar rellena
				if (tbHora[1].Text.Trim() != ":")
				{
					short tempRefParam11 = 1040;
					string[] tempRefParam12 = new string[]{"fecha fin pauta"};
					BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, BGPautas.RcEnfermeria, tempRefParam12);
					result = false;
					sdcFechas[1].Focus();
					return result;
				}
			}

			//***************

			if (!blnNuevo)
			{
				//si tengo algun registro seleccionado y voy a modificar
				//se puede modificar la fecha de inicio, la pauta o introducir una fecha de fin
				//si se modifica la fecha de inicio esta no puede ser inferior a la maxima fecha de fin existente
				if (!fValidaTramo("fecfinal"))
				{
					result = false;
					tbHora[0].Focus();
					return result;
				}
				//*********
				//si se introduce la fecha de fin
				if (sdcFechas[1].Text.Trim() != "")
				{
					if (DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text) > DateTime.Parse(sdcFechas[1].Text + " " + tbHora[1].Text))
					{
						result = false;
						short tempRefParam13 = 1020;
						string[] tempRefParam14 = new string[]{"Fin de Pauta", ">=", "Inicio de Pauta"};
						BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, BGPautas.RcEnfermeria, tempRefParam14);
						return result;
					}

				}

			}
			else
			{
				if (!fValidaTramo("fecfinal"))
				{
					return false;
				}
				else
				{

					if (fValidaTramo("fapuntes"))
					{

						if (DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text) < DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:NN")))
						{
							result = false;
							short tempRefParam15 = 1020;
							string[] tempRefParam16 = new string[]{"Inicio Pauta", ">=", DateTime.Now.ToString("dd/MM/yyyy HH:NN")};
							BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, BGPautas.RcEnfermeria, tempRefParam16);
							return result;
						}
						if (sdcFechas[1].Text.Trim() != "")
						{
							if (DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text) > DateTime.Parse(sdcFechas[1].Text + " " + tbHora[1].Text))
							{
								result = false;
								short tempRefParam17 = 1020;
								string[] tempRefParam18 = new string[]{"Fin de Pauta", ">=", "Inicio de Pauta"};
								BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam17, BGPautas.RcEnfermeria, tempRefParam18);
								return result;
							}
						}
					}
					else
					{
						return false;

					}
				}
			}


			if (txtDatos.Text.Trim() == "")
			{
				result = false;
				short tempRefParam19 = 1040;
				string[] tempRefParam20 = new string[]{"Pauta correctora"};
				BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam19, BGPautas.RcEnfermeria, tempRefParam20);
				txtDatos.Focus();
				return result;
			}


			return result;
		}


		private bool fValidaTramo(string campo)
		{
			bool DesError = false;
			bool result = false;
			string strsql = String.Empty;

			result = true;

			try
			{
				DesError = true;


				//If Trim(sdcFechas(1)) = "" Then
				strsql = "SELECT max(" + campo + ") as Fecha " + 
				         "FROM EPAUCORR " + 
				         "Where itiposer = '" + strServicio + "' And " + 
				         "ganoregi = " + intAnio.ToString() + " And " + 
				         "gnumregi = " + lngNumero.ToString();


				SqlDataAdapter tempAdapter = new SqlDataAdapter(strsql, BGPautas.RcEnfermeria);
				rdoDatos = new DataSet();
				tempAdapter.Fill(rdoDatos);

				if (rdoDatos.Tables[0].Rows.Count != 0 && !Convert.IsDBNull(rdoDatos.Tables[0].Rows[0]["Fecha"]))
				{
					if (DateTime.Parse(sdcFechas[0].Text + " " + tbHora[0].Text) <= DateTime.Parse(Convert.ToDateTime(rdoDatos.Tables[0].Rows[0]["Fecha"]).ToString("dd/MM/yyyy HH:NN")))
					{
						short tempRefParam = 3502;
						string[] tempRefParam2 = new string[]{" el per�odo anterior"};
						BGPautas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BGPautas.RcEnfermeria, tempRefParam2);
						result = false;
					}
				}
				rdoDatos.Close();
				rdoDatos = null;
				//End If

				DesError = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!DesError)
				{
					throw excep;
				}

				if (DesError)
				{

					result = false;
					Serrores.AnalizaError("PautasCorrectoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "Aceptar_click:PautasCorrectoras.dll", excep);
					return result;
				}
			}
			return false;
		}




		private string fCargarMedico()
		{
			bool ErrMedico = false;
			string result = String.Empty;
			string strsql = String.Empty;
			DataSet rdoMedico = null;

			try
			{
				ErrMedico = true;

				result = "";

				strsql = "SELECT SUSUARIO.gusuario, SUSUARIO.dusuario, DPERSONA.imedico, DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers  " + 
				         "From SUSUARIO " + 
				         "INNER JOIN DPERSONA ON SUSUARIO.gpersona = DPERSONA.gpersona " + 
				         "WHERE SUSUARIO.gusuario = '" + BGPautas.gUsuario + "' ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(strsql, BGPautas.RcEnfermeria);
				rdoMedico = new DataSet();
				tempAdapter.Fill(rdoMedico);
				if (rdoMedico.Tables[0].Rows.Count != 0)
				{
					if (Convert.ToString(rdoMedico.Tables[0].Rows[0]["Imedico"]).ToUpper() == "S")
					{
						//fCargarMedico = rdoMedico("dusuario")
						result = Convert.ToString(rdoMedico.Tables[0].Rows[0]["dnompers"]).Trim() + " " + Convert.ToString(rdoMedico.Tables[0].Rows[0]["dap1pers"]).Trim() + " ";
						if (!Convert.IsDBNull(rdoMedico.Tables[0].Rows[0]["dap2pers"]))
						{
							result = result + Convert.ToString(rdoMedico.Tables[0].Rows[0]["dap2pers"]).Trim();
						}
					}
					else
					{
						rdoMedico.Close();
						switch(strServicio)
						{
							case "U" : 
								strsql = "SELECT DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers " + 
								         "FROM DPERSONA " + 
								         "INNER JOIN UEPISURG ON DPERSONA.gpersona = UEPISURG.gpersona " + 
								         "WHERE UEPISURG.ganourge = " + intAnio.ToString() + " and UEPISURG.gnumurge = " + lngNumero.ToString(); 
								break;
							case "H" : 
								strsql = "SELECT DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers " + 
								         "FROM DPERSONA " + 
								         "INNER JOIN AEPISADM ON DPERSONA.gpersona = AEPISADM.gperulti " + 
								         "WHERE AEPISADM.ganoadme = " + intAnio.ToString() + " and AEPISADM.gnumadme = " + lngNumero.ToString(); 
								break;
						}

						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strsql, BGPautas.RcEnfermeria);
						rdoMedico = new DataSet();
						tempAdapter_2.Fill(rdoMedico);
						if (rdoMedico.Tables[0].Rows.Count != 0)
						{
							result = Convert.ToString(rdoMedico.Tables[0].Rows[0]["dnompers"]).Trim() + " " + Convert.ToString(rdoMedico.Tables[0].Rows[0]["dap1pers"]).Trim() + " ";
							if (!Convert.IsDBNull(rdoMedico.Tables[0].Rows[0]["dap2pers"]))
							{
								result = result + Convert.ToString(rdoMedico.Tables[0].Rows[0]["dap2pers"]).Trim();
							}
						}
					}
				}
				rdoMedico.Close();
				rdoMedico = null;

				if (result.Trim() == "")
				{
					result = "No se pudo cargar el medico.";
				}


				ErrMedico = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!ErrMedico)
				{
					throw excep;
				}

				if (ErrMedico)
				{

					Serrores.AnalizaError("Pautas Correctoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "Form_Load:PautasCorrectoras.dll", excep);

					return result;
				}
			}
			return System.String.Empty;
		}


		private void RecuperaDatos(int intOpcion)
		{
			bool ErrRecupera = false;

			string strsql = String.Empty;

			try
			{
				ErrRecupera = true;

				LimpiaControles();
				ModoControles(true);

                sdcFechas[0].SetToNullValue();
				sdcFechas[1].SetToNullValue();
                tbHora[0].Text = "  :  ";
				tbHora[1].Text = "  :  ";
				sdcFechas[0].MinDate = DateTime.Now;
				sdcFechas[1].MinDate = DateTime.Now;
				blnNuevo = true;
				cmdBotones[4].Enabled = true;

				switch(intOpcion)
				{
					case 1 :  // Activos 
						strsql = " SELECT rTrim( DPERSONA.dap1pers) + ' ' + ISNULL(ltrim(rtrim(DPERSONA.dap2pers)) + ' ','') + ',' + rTrim(DPERSONA.dnompers) as Profesional," + 
						         " NULL as fmodific, EPAUCORR.* " + 
						         " FROM EPAUCORR " + 
						         " INNER JOIN SUSUARIO on SUSUARIO.gusuario = EPAUCORR.gusuario " + 
						         " INNER JOIN DPERSONA on DPERSONA.gpersona = SUSUARIO.gpersona " + 
						         " WHERE EPAUCORR.itiposer = '" + strServicio + "' And " + 
						         "EPAUCORR.ganoregi = " + intAnio.ToString() + " And " + 
						         "EPAUCORR.gnumregi = " + lngNumero.ToString() + " "; 
						 
						if (!blnTodos)
						{
							object tempRefParam = sdcFechas[2].Text;
							strsql = strsql + " AND EPAUCORR.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam);
						} 
						 
						break;
					case 2 :  // Activos e inactivos 
						 
						strsql = " SELECT rTrim( DPERSONA.dap1pers) + ' ' + ISNULL(ltrim(rtrim(DPERSONA.dap2pers)) + ' ','') + ',' + rTrim(DPERSONA.dnompers) as Profesional," + 
						         " NULL as fmodific, EPAUCORR.* " + 
						         " FROM EPAUCORR " + 
						         " INNER JOIN SUSUARIO on SUSUARIO.gusuario = EPAUCORR.gusuario " + 
						         " INNER JOIN DPERSONA on DPERSONA.gpersona = SUSUARIO.gpersona " + 
						         " WHERE EPAUCORR.itiposer = '" + strServicio + "' And " + 
						         "EPAUCORR.ganoregi = " + intAnio.ToString() + " And " + 
						         "EPAUCORR.gnumregi = " + lngNumero.ToString() + " "; 
						if (!blnTodos)
						{
							object tempRefParam2 = sdcFechas[2].Text;
							strsql = strsql + " AND EPAUCORR.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam2);
						} 
						 
						strsql = strsql + "UNION "; 
						 
						strsql = strsql + 
						         " SELECT rTrim( DPERSONA.dap1pers) + ' ' + ISNULL(ltrim(rtrim(DPERSONA.dap2pers)) + ' ','') + ',' + rTrim(DPERSONA.dnompers) as Profesional," + 
						         " EPAUCMOD.* " + 
						         " FROM EPAUCMOD " + 
						         " INNER JOIN SUSUARIO on SUSUARIO.gusuario = EPAUCMOD.gusuario " + 
						         " INNER JOIN DPERSONA on DPERSONA.gpersona = SUSUARIO.gpersona " + 
						         " WHERE EPAUCMOD.itiposer = '" + strServicio + "' And " + 
						         "EPAUCMOD.ganoregi = " + intAnio.ToString() + " And " + 
						         "EPAUCMOD.gnumregi = " + lngNumero.ToString() + " "; 
						 
						if (!blnTodos)
						{
							object tempRefParam3 = sdcFechas[2].Text;
							strsql = strsql + " AND EPAUCMOD.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam3);
						} 
						break;
				}


				strsql = strsql + "ORDER BY itiposer, fsistema desc";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(strsql, BGPautas.RcEnfermeria);
				rdoDatos = new DataSet();
				tempAdapter.Fill(rdoDatos);

				if (rdoDatos.Tables[0].Rows.Count == 0)
				{                    
                    sdcFechas[0].NullableValue = null;
                    sdcFechas[0].SetToNullValue();

                    tbHora[0].Text = "  :  ";

					vasPautas.Enabled = false;
					vasPautas.MaxRows = 0;
                    _lblTextos_5.Text = fCargarMedico();

					blnCambios = false;
					cmdBotones[0].Enabled = false;

				}
				else
				{

					vasPautas.Enabled = true;
					CargaGrid();
				}

				rdoDatos.Close();
				rdoDatos = null;

				cmdBotones[0].Enabled = false;

				ErrRecupera = false;
			}
			catch (Exception excep)
			{
				if (!ErrRecupera)
				{
					throw excep;
				}

				if (ErrRecupera)
				{

					Serrores.AnalizaError("Pautas Correctoras", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "Form_Load:PautasCorrectoras.dll", excep);

				}
			}
		}


		private void CargaGrid()
		{
			Color varColor = new Color();

			int intFilas = 0;
			vasPautas.MaxRows = 0;

			int intActiva = 0;
			foreach (DataRow iteration_row in rdoDatos.Tables[0].Rows)
			{

				if (!Convert.IsDBNull(iteration_row["fmodific"]))
				{
					varColor = Color.Gray;
				}
				else
				{
					if (stFechaLLegada != "")
					{
						if (DateTime.Parse(Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse(stFechaLLegada))
						{
							varColor = lnUrgencias.BorderColor; // naranja
						}
						else
						{
							varColor = Color.Black; // negro
						}
					}
					else
					{
						varColor = Color.Black; // negro
					}
				}

				intFilas++;
				vasPautas.MaxRows = intFilas;
				vasPautas.Row = intFilas;

				intActiva = (Convert.IsDBNull(iteration_row["fmodific"])) ? intActiva + 1 : intActiva;


				vasPautas.Col = Col_IniPeriodo;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:NN");
				vasPautas.Lock = true;

				vasPautas.Col = Col_FinPeriodo;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = (Convert.IsDBNull(iteration_row["fecfinal"])) ? "" : Convert.ToDateTime(iteration_row["fecfinal"]).ToString("dd/MM/yyyy HH:NN");
				vasPautas.Lock = true;

				vasPautas.Col = Col_Pauta;
				vasPautas.ForeColor = varColor;
				vasPautas.setTypeEditLen(900);
				vasPautas.Text = Convert.ToString(iteration_row["opautaco"]);
				vasPautas.Lock = true;

				vasPautas.Col = Col_Medico;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = Convert.ToString(iteration_row["profesional"]).Trim(); //fCargarMedico
				vasPautas.Lock = true;

				vasPautas.Col = Col_Usuario;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = Convert.ToString(iteration_row["gUsuario"]).Trim();
				vasPautas.Lock = true;
				vasPautas.SetColHidden(vasPautas.Col, true);


				vasPautas.Col = Col_FecSistema;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = Convert.ToString(iteration_row["fsistema"]);
				vasPautas.Lock = true;
				vasPautas.SetColHidden(vasPautas.Col, true);


				vasPautas.Col = Col_UsuModi;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = Convert.ToString(iteration_row["gUsuario"]).Trim();
				vasPautas.Lock = true;
				vasPautas.SetColHidden(vasPautas.Col, true);

				vasPautas.Col = Col_FecModi;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = (Convert.IsDBNull(iteration_row["fmodific"])) ? "" : Convert.ToString(iteration_row["fmodific"]);
				vasPautas.Lock = true;
				vasPautas.SetColHidden(vasPautas.Col, false);


				vasPautas.Col = Col_Activar;
				vasPautas.ForeColor = varColor;
				vasPautas.Text = intActiva.ToString();
				vasPautas.Lock = true;
				vasPautas.SetColHidden(vasPautas.Col, true);



			}

			vasPautas.Row = 0;
			vasPautas.Row2 = vasPautas.MaxRows;
			vasPautas.setSelModeIndex(0);

		}

		private void LimpiaControles()
		{
			sdcFechas[0].Text = "";
			sdcFechas[1].Text = "";
			tbHora[0].Text = "  :  ";
			tbHora[1].Text = "  :  ";

			//lblTextos(5).Caption = ""
			txtDatos.Text = "";

            _lblTextos_5.Text = fCargarMedico();


		}

		private void ModoControles(bool blnActiva)
		{

			sdcFechas[0].Enabled = blnActiva;
			sdcFechas[1].Enabled = blnActiva;

			tbHora[0].Enabled = blnActiva;
			tbHora[1].Enabled = blnActiva;

			txtDatos.Enabled = blnActiva;


		}

		public void ObtenerValoresTipoBD()
		{
			bool error = false;
			DataSet tRr = null;
			string tstQuery = String.Empty;
			string formato = String.Empty;
			int I = 0;

			try
			{
				error = true;
				tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, BGPautas.RcEnfermeria);
				tRr = new DataSet();
				tempAdapter.Fill(tRr);
				switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
				{
					case "SQL SERVER 6.5" : 
						Formato_Fecha_SQL = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
						formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
						for (I = 1; I <= formato.Length; I++)
						{
							if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && Separador_Fecha_SQL == "")
							{
								Separador_Fecha_SQL = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
							}
							if (Separador_Fecha_SQL != "")
							{
								break;
							}
						} 
						formato = formato.Substring(Strings.InStr(I + 1, formato, Separador_Fecha_SQL, CompareMethod.Binary)); 
						for (I = 1; I <= formato.Length; I++)
						{
							if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && Separador_Hora_SQL == "")
							{
								Separador_Hora_SQL = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
							}
							if (Separador_Hora_SQL != "")
							{
								break;
							}
						} 
						break;
				}
				tRr.Close();

				error = false;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}
				if (error)
				{

					Serrores.AnalizaError("Medidas", Path.GetDirectoryName(Application.ExecutablePath), BGPautas.gUsuario, "ObtenerValoresTipoBD:Medidas.dll", excep);
					tRr = null;
				}
			}
		}



		public void ObtenerLocale()
		{
			//'    Dim buffer As String * 100
			//'    Dim dl&
			//'
			//'    #If Win32 Then
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, buffer, 99)
			//'        lSep_Fecha = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, buffer, 99)
			//'        lSep_Hora = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, buffer, 99)
			//'        lFor_Fecha = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT, buffer, 99)
			//'        lFor_Hora = LPSTRToVBString(buffer)
			//'        'le a�ado esto para que carge el local decimal
			//'         dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, buffer, 99)
			//'        lSep_Decimal = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, buffer, 99)
			//'        lSep_Mil = LPSTRToVBString(buffer)
			//'    #Else
			//'        Print " Not implemented under Win16"
			//'    #End If
			//' CURIAE - EQUIPO DE MIGRACION - NUEVA FORMA DE OBTENER LA CONFIGURACION REGIONAL
		}

		//private string LPSTRToVBString(string s)
		//{
				//int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
				//if (nullpos > 0)
				//{
					//return s.Substring(0, Math.Min(nullpos - 1, s.Length));
				//}
				//else
				//{
					//return "";
				//}
		//}
		private void frmPauCorr_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}