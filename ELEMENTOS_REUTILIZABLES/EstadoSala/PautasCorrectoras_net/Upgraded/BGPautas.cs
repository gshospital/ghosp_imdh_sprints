using System;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PautasCorrectoras
{
	internal static class BGPautas
	{

		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static SqlConnection RcEnfermeria = null;
		public static string gUsuario = String.Empty;
		public static string gContrase�a = String.Empty;

		public const int LOCALE_SYSTEM_DEFAULT = 0x800;
		public const int LOCALE_SDECIMAL = 0xE; //  separador de decimales
		public const int LOCALE_STHOUSAND = 0xF; //  separador de miles

		public static string VstUsuario_NT = String.Empty; //Usuario de NT

		public static mbAcceso.strEventoNulo[] astreventos = null; //Array para el control de acceso en los eventos
		public static mbAcceso.strControlOculto[] astrcontroles = null; //Array para el control de acceso de los controles
        		
		public static string vstSeparadorDecimal = String.Empty;
		public static string stDecimalBD = String.Empty;


		public const int LOCALE_SDATE = 0x1D; //
		public const int LOCALE_STIME = 0x1E;
		public const int LOCALE_SSHORTDATE = 0x1F;
		public const int LOCALE_STIMEFORMAT = 0x1003; //formato de hora

		public static string lSep_Fecha = String.Empty;
		public static string lSep_Hora = String.Empty;
		public static string lFor_Fecha = String.Empty;
		public static string lFor_Hora = String.Empty;


		public static string stPautaCorrectora = String.Empty;
	}
}