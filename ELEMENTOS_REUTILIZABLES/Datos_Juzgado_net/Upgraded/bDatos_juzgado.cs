using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLDatosJuzgado
{
	internal static class bDatos_juzgado
	{

		public static SqlConnection cnConexion = null;

		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					return result;
			}
		}
		public static bDatos_juzgado.CabCrystal VstrCrystal = bDatos_juzgado.CabCrystal.CreateInstance();

		public static string Formato_Fecha_SQL = String.Empty;
		public static string Separador_Fecha_SQL = String.Empty;
		public static string Separador_Hora_SQL = String.Empty;

		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************

			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, cnConexion);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, cnConexion);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, cnConexion);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}

		internal static void ObtenerValoresTipoBD()
		{
			string formato = String.Empty;
			int i = 0;
			string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, cnConexion);
			DataSet tRr = new DataSet();
			tempAdapter.Fill(tRr);
			switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
			{
				case "SQL SERVER 6.5" : 
					Formato_Fecha_SQL = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
					formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Fecha_SQL == "")
						{
							Separador_Fecha_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Fecha_SQL != "")
						{
							break;
						}
					} 
					formato = formato.Substring(Strings.InStr(i + 1, formato, Separador_Fecha_SQL, CompareMethod.Binary)); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Hora_SQL == "")
						{
							Separador_Hora_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Hora_SQL != "")
						{
							break;
						}
					} 
					break;
			}
			tRr.Close();
		}

		internal static string probuscar_provincia(string codigo)
		{
			string result = String.Empty;
			string provincia = String.Empty;
			string sql = "select dnomprov from dprovinc where gprovinc = '" + codigo + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, cnConexion);
			DataSet Rrsql = new DataSet();
			tempAdapter.Fill(Rrsql);
			if (Rrsql.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(Rrsql.Tables[0].Rows[0]["dnomprov"]))
				{
					provincia = Convert.ToString(Rrsql.Tables[0].Rows[0]["dnomprov"]).Trim();
				}
				else
				{
					provincia = "";
				}
			}
			else
			{
				provincia = "";
			}
			result = provincia;
			Rrsql.Close();
			return result;
		}
	}
}