using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLDatosJuzgado
{
	partial class resol_juzgado
	{

		#region "Upgrade Support "
		private static resol_juzgado m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static resol_juzgado DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new resol_juzgado();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbExpediente2", "sdcFechaResol2", "cbbJuzgado2", "Label7", "Label6", "Label5", "Label2", "frmDuplicado", "ChbEnfMental", "tbExpediente", "sdcFechaResol1", "cbbJuzgado", "Label4", "Label1", "Label3", "frmPrimerExp", "LbNumsegu", "LbFinancia", "LbHistoria", "Label10", "LbPaciente", "Label11", "Label12", "Label28", "Frame2", "CbCancelar", "CbAceptar"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadTextBoxControl tbExpediente2;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaResol2;
		public UpgradeHelpers.MSForms.MSCombobox cbbJuzgado2;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox frmDuplicado;
		public Telerik.WinControls.UI.RadCheckBox ChbEnfMental;
		public Telerik.WinControls.UI.RadTextBoxControl tbExpediente;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaResol1;
		public UpgradeHelpers.MSForms.MSCombobox cbbJuzgado;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadGroupBox frmPrimerExp;
		public Telerik.WinControls.UI.RadTextBox LbNumsegu;
		public Telerik.WinControls.UI.RadTextBox LbFinancia;
		public Telerik.WinControls.UI.RadTextBox LbHistoria;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.frmDuplicado = new Telerik.WinControls.UI.RadGroupBox();
            this.tbExpediente2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.sdcFechaResol2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.cbbJuzgado2 = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.frmPrimerExp = new Telerik.WinControls.UI.RadGroupBox();
            this.ChbEnfMental = new Telerik.WinControls.UI.RadCheckBox();
            this.tbExpediente = new Telerik.WinControls.UI.RadTextBoxControl();
            this.sdcFechaResol1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.cbbJuzgado = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.LbNumsegu = new Telerik.WinControls.UI.RadTextBox();
            this.LbFinancia = new Telerik.WinControls.UI.RadTextBox();
            this.LbHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label11 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.Label28 = new Telerik.WinControls.UI.RadLabel();
            this.CbCancelar = new Telerik.WinControls.UI.RadButton();
            this.CbAceptar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.frmDuplicado)).BeginInit();
            this.frmDuplicado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbExpediente2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaResol2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbJuzgado2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrimerExp)).BeginInit();
            this.frmPrimerExp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbEnfMental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbExpediente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaResol1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbJuzgado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumsegu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFinancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // frmDuplicado
            // 
            this.frmDuplicado.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmDuplicado.Controls.Add(this.tbExpediente2);
            this.frmDuplicado.Controls.Add(this.sdcFechaResol2);
            this.frmDuplicado.Controls.Add(this.cbbJuzgado2);
            this.frmDuplicado.Controls.Add(this.Label7);
            this.frmDuplicado.Controls.Add(this.Label6);
            this.frmDuplicado.Controls.Add(this.Label5);
            this.frmDuplicado.Controls.Add(this.Label2);
            this.frmDuplicado.Enabled = false;
            this.frmDuplicado.HeaderText = "Expediente duplicado";
            this.frmDuplicado.Location = new System.Drawing.Point(2, 183);
            this.frmDuplicado.Name = "frmDuplicado";
            this.frmDuplicado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmDuplicado.Size = new System.Drawing.Size(557, 104);
            this.frmDuplicado.TabIndex = 18;
            this.frmDuplicado.Text = "Expediente duplicado";
            // 
            // tbExpediente2
            // 
            this.tbExpediente2.AcceptsReturn = true;
            this.tbExpediente2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbExpediente2.Location = new System.Drawing.Point(116, 52);
            this.tbExpediente2.MaxLength = 20;
            this.tbExpediente2.Name = "tbExpediente2";
            this.tbExpediente2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbExpediente2.Size = new System.Drawing.Size(145, 19);
            this.tbExpediente2.TabIndex = 19;
            this.tbExpediente2.TextChanged += new System.EventHandler(this.tbExpediente2_TextChanged);
            this.tbExpediente2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbExpediente2_KeyPress);
            // 
            // sdcFechaResol2
            // 
            this.sdcFechaResol2.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaResol2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdcFechaResol2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaResol2.Location = new System.Drawing.Point(399, 54);
            this.sdcFechaResol2.Name = "sdcFechaResol2";
            this.sdcFechaResol2.Size = new System.Drawing.Size(113, 18);
            this.sdcFechaResol2.TabIndex = 20;
            this.sdcFechaResol2.TabStop = false;
            this.sdcFechaResol2.Value = new System.DateTime(((long)(0)));
            this.sdcFechaResol2.Leave += new System.EventHandler(this.sdcFechaResol2_Leave);
            // 
            // cbbJuzgado2
            // 
            this.cbbJuzgado2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbbJuzgado2.ColumnWidths = "";
            this.cbbJuzgado2.Location = new System.Drawing.Point(116, 26);
            this.cbbJuzgado2.Name = "cbbJuzgado2";
            this.cbbJuzgado2.Size = new System.Drawing.Size(395, 20);
            this.cbbJuzgado2.TabIndex = 25;
            this.cbbJuzgado2.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbJuzgado2_Change);
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Location = new System.Drawing.Point(8, 2);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(114, 18);
            this.Label7.TabIndex = 24;
            this.Label7.Text = "Expediente duplicado";
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(293, 54);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(108, 18);
            this.Label6.TabIndex = 23;
            this.Label6.Text = "Fecha de resoluci�n:";
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(48, 54);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(64, 18);
            this.Label5.TabIndex = 22;
            this.Label5.Text = "Expediente:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(47, 26);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(50, 18);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "Juzgado:";
            // 
            // frmPrimerExp
            // 
            this.frmPrimerExp.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmPrimerExp.Controls.Add(this.ChbEnfMental);
            this.frmPrimerExp.Controls.Add(this.tbExpediente);
            this.frmPrimerExp.Controls.Add(this.sdcFechaResol1);
            this.frmPrimerExp.Controls.Add(this.cbbJuzgado);
            this.frmPrimerExp.Controls.Add(this.Label4);
            this.frmPrimerExp.Controls.Add(this.Label1);
            this.frmPrimerExp.Controls.Add(this.Label3);
            this.frmPrimerExp.HeaderText = "Primer expediente";
            this.frmPrimerExp.Location = new System.Drawing.Point(2, 80);
            this.frmPrimerExp.Name = "frmPrimerExp";
            this.frmPrimerExp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPrimerExp.Size = new System.Drawing.Size(558, 99);
            this.frmPrimerExp.TabIndex = 11;
            this.frmPrimerExp.Text = "Primer expediente";
            // 
            // ChbEnfMental
            // 
            this.ChbEnfMental.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.ChbEnfMental.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbEnfMental.Location = new System.Drawing.Point(48, 20);
            this.ChbEnfMental.Name = "ChbEnfMental";
            this.ChbEnfMental.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbEnfMental.Size = new System.Drawing.Size(86, 18);
            this.ChbEnfMental.TabIndex = 26;
            this.ChbEnfMental.Text = "Incapacitado:";
            this.ChbEnfMental.CheckStateChanged += new System.EventHandler(this.ChbEnfMental_CheckStateChanged);
            // 
            // tbExpediente
            // 
            this.tbExpediente.AcceptsReturn = true;
            this.tbExpediente.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbExpediente.Location = new System.Drawing.Point(118, 68);
            this.tbExpediente.MaxLength = 20;
            this.tbExpediente.Name = "tbExpediente";
            this.tbExpediente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbExpediente.Size = new System.Drawing.Size(145, 19);
            this.tbExpediente.TabIndex = 12;
            this.tbExpediente.TextChanged += new System.EventHandler(this.tbExpediente_TextChanged);
            this.tbExpediente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbExpediente_KeyPress);
            // 
            // sdcFechaResol1
            // 
            this.sdcFechaResol1.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaResol1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdcFechaResol1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaResol1.Location = new System.Drawing.Point(398, 70);
            this.sdcFechaResol1.Name = "sdcFechaResol1";
            this.sdcFechaResol1.Size = new System.Drawing.Size(113, 18);
            this.sdcFechaResol1.TabIndex = 13;
            this.sdcFechaResol1.TabStop = false;
            this.sdcFechaResol1.Value = new System.DateTime(((long)(0)));
            this.sdcFechaResol1.Click += new System.EventHandler(this.sdcFechaResol1_Click);
            this.sdcFechaResol1.Leave += new System.EventHandler(this.sdcFechaResol1_Leave);
            // 
            // cbbJuzgado
            // 
            this.cbbJuzgado.ColumnWidths = "";
            this.cbbJuzgado.Location = new System.Drawing.Point(118, 42);
            this.cbbJuzgado.Name = "cbbJuzgado";
            this.cbbJuzgado.Size = new System.Drawing.Size(393, 20);
            this.cbbJuzgado.TabIndex = 17;
            this.cbbJuzgado.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbJuzgado_Change);
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(52, 44);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(50, 18);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "Juzgado:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(50, 68);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(64, 18);
            this.Label1.TabIndex = 15;
            this.Label1.Text = "Expediente:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(294, 70);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(108, 18);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "Fecha de resoluci�n:";
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.LbNumsegu);
            this.Frame2.Controls.Add(this.LbFinancia);
            this.Frame2.Controls.Add(this.LbHistoria);
            this.Frame2.Controls.Add(this.Label10);
            this.Frame2.Controls.Add(this.LbPaciente);
            this.Frame2.Controls.Add(this.Label11);
            this.Frame2.Controls.Add(this.Label12);
            this.Frame2.Controls.Add(this.Label28);
            this.Frame2.HeaderText = "Datos del Paciente";
            this.Frame2.Location = new System.Drawing.Point(2, 4);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(557, 74);
            this.Frame2.TabIndex = 2;
            this.Frame2.Text = "Datos del Paciente";
            // 
            // LbNumsegu
            // 
            this.LbNumsegu.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNumsegu.Enabled = false;
            this.LbNumsegu.Location = new System.Drawing.Point(422, 40);
            this.LbNumsegu.Name = "LbNumsegu";
            this.LbNumsegu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNumsegu.Size = new System.Drawing.Size(131, 20);
            this.LbNumsegu.TabIndex = 10;
            // 
            // LbFinancia
            // 
            this.LbFinancia.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFinancia.Enabled = false;
            this.LbFinancia.Location = new System.Drawing.Point(104, 40);
            this.LbFinancia.Name = "LbFinancia";
            this.LbFinancia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFinancia.Size = new System.Drawing.Size(229, 20);
            this.LbFinancia.TabIndex = 9;
            // 
            // LbHistoria
            // 
            this.LbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbHistoria.Enabled = false;
            this.LbHistoria.Location = new System.Drawing.Point(456, 16);
            this.LbHistoria.Name = "LbHistoria";
            this.LbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbHistoria.Size = new System.Drawing.Size(97, 20);
            this.LbHistoria.TabIndex = 8;
            // 
            // Label10
            // 
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Location = new System.Drawing.Point(376, 16);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(81, 18);
            this.Label10.TabIndex = 7;
            this.Label10.Text = "Historia cl�nica:";
            // 
            // LbPaciente
            // 
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPaciente.Enabled = false;
            this.LbPaciente.Location = new System.Drawing.Point(72, 16);
            this.LbPaciente.Name = "LbPaciente";
            this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPaciente.Size = new System.Drawing.Size(261, 20);
            this.LbPaciente.TabIndex = 6;
            // 
            // Label11
            // 
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label11.Location = new System.Drawing.Point(344, 40);
            this.Label11.Name = "Label11";
            this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label11.Size = new System.Drawing.Size(78, 18);
            this.Label11.TabIndex = 5;
            this.Label11.Text = "N� asegurado:";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(8, 40);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(94, 18);
            this.Label12.TabIndex = 4;
            this.Label12.Text = "Ent. Financiadora:";
            // 
            // Label28
            // 
            this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label28.Location = new System.Drawing.Point(8, 16);
            this.Label28.Name = "Label28";
            this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label28.Size = new System.Drawing.Size(51, 18);
            this.Label28.TabIndex = 3;
            this.Label28.Text = "Paciente:";
            // 
            // CbCancelar
            // 
            this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CbCancelar.Location = new System.Drawing.Point(478, 290);
            this.CbCancelar.Name = "CbCancelar";
            this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCancelar.Size = new System.Drawing.Size(81, 29);
            this.CbCancelar.TabIndex = 1;
            this.CbCancelar.Text = "&Cancelar";
            this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
            // 
            // CbAceptar
            // 
            this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbAceptar.Enabled = false;
            this.CbAceptar.Location = new System.Drawing.Point(390, 290);
            this.CbAceptar.Name = "CbAceptar";
            this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbAceptar.Size = new System.Drawing.Size(81, 29);
            this.CbAceptar.TabIndex = 0;
            this.CbAceptar.Text = "&Aceptar";
            this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
            // 
            // resol_juzgado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CbCancelar;
            this.ClientSize = new System.Drawing.Size(563, 322);
            this.Controls.Add(this.frmDuplicado);
            this.Controls.Add(this.frmPrimerExp);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.CbCancelar);
            this.Controls.Add(this.CbAceptar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "resol_juzgado";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "REGISTRO DE LA RESOLUCION DEL JUZGADO";
            this.Activated += new System.EventHandler(this.resol_juzgado_Activated);
            this.Closed += new System.EventHandler(this.resol_juzgado_Closed);
            this.Load += new System.EventHandler(this.resol_juzgado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.frmDuplicado)).EndInit();
            this.frmDuplicado.ResumeLayout(false);
            this.frmDuplicado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbExpediente2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaResol2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbJuzgado2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrimerExp)).EndInit();
            this.frmPrimerExp.ResumeLayout(false);
            this.frmPrimerExp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbEnfMental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbExpediente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaResol1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbJuzgado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumsegu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFinancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}