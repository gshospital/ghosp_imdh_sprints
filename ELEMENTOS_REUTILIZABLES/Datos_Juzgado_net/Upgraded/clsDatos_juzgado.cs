using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using ElementosCompartidos;
using CrystalWrapper;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;


namespace DLLDatosJuzgado
{
	public class clsDatos_juzgado
	{


		public void proLoad(SqlConnection ParaConexion, int Panoadme, int Pnumadme, string Pidenpac, string pUsuario)
		{

			bDatos_juzgado.cnConexion = ParaConexion;
			Serrores.AjustarRelojCliente(ParaConexion);
			resol_juzgado tempLoadForm = resol_juzgado.DefInstance;
			resol_juzgado.DefInstance.mganoregi = Panoadme;
			resol_juzgado.DefInstance.mgnumregi = Pnumadme;
			resol_juzgado.DefInstance.mgidenpac = Pidenpac;
			resol_juzgado.DefInstance.mVstCodUsua = pUsuario;
			resol_juzgado.DefInstance.ShowDialog();

		}

		public clsDatos_juzgado()
		{
			Serrores.oClass = new Mensajes.ClassMensajes();
		}

		~clsDatos_juzgado()
		{
			Serrores.oClass = null;
		}

		public void Imprimir(Crystal Paralistado, SqlConnection ParaConexion, string ParaUsuarioCrystal, string ParaContraseñaCrystal, string ParaPathAplicacion, string ParaDsnCrystal, string ParaInforme, bool ParaSoloIngresados = true)
		{

			string sql = String.Empty;
			Crystal listado_local = Paralistado;
			string sTitulo = String.Empty;
			DataSet RrSql = null;
			string stNombre_Director = String.Empty;
			string stNHospit = String.Empty;
			string stDDireccion = String.Empty;
			string stPoblacion = String.Empty;
			string stFechaHoy = String.Empty;
			string stFechaBusqueda = String.Empty;
			string stFechaFormateada = String.Empty;
			string Sql_Datos = String.Empty;
			DataSet RrSql_Datos = null;
			string stJuzgado = String.Empty;
			string stDireccion = String.Empty;
			string stNJuzgado = String.Empty;
			StringBuilder stProvincia = new StringBuilder();
			StringBuilder stPaciente = new StringBuilder();
			DialogResult intRespuesta = (DialogResult) 0;
            string stGidenpac = String.Empty;



            bDatos_juzgado.cnConexion = ParaConexion;
			Cursor.Current = Cursors.WaitCursor;
			string LitPersona = Serrores.ObtenerLiteralPersona(bDatos_juzgado.cnConexion);
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(bDatos_juzgado.cnConexion);

			bool bInicioTransaccion = false;
			try
			{


                switch (ParaInforme)
                {
                    case "AIC4120R1":
                        //Imprimir_partes_emitidos_juzgado 
                        sql = " SELECT " +
                              " AEPISADM.nnumexp1, AEPISADM.fresolu1, AEPISADM.fultnotif, " +
                              " DJUZGADO.dnomjuzg, DJUZGADO.ddirjuzg, DJUZGADO.gcodipos, " +
                              " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " +
                              " DPACIENT.ndninifp , DPROVINC.dnomprov, DPOBLACI.DPOBLACI " +
                              " From " +
                              " (((AEPISADM INNER JOIN DPACIENT ON " +
                              " AEPISADM.gidenpac = DPACIENT.gidenpac) " +
                              " INNER JOIN DJUZGADO ON " +
                              " AEPISADM.gjuzgad1 = DJUZGADO.gjuzgado) " +
                              " LEFT OUTER JOIN DPOBLACI ON " +
                              " DJUZGADO.gpoblaci = DPOBLACI.gpoblaci) " +
                              " LEFT OUTER JOIN DPROVINC ON " +
                              " DJUZGADO.gprovinc = DPROVINC.gprovinc " +
                              " Where ";
                        if (ParaSoloIngresados)
                        {
                            sql = sql + " aepisadm.faltplan is null AND ";
                        }
                        sql = sql + " aepisadm.gjuzgad1 is not null ";
                        sql = sql +
                              " Order By DJUZGADO.dnomjuzg Asc, DPACIENT.dape1pac ASC, " +
                              " DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";

                        if (!bDatos_juzgado.VstrCrystal.VfCabecera)
                        {
                            bDatos_juzgado.proCabCrystal();
                        }

                        //hay que cambiar el destino del informe y la ubicacion 
                        //listado_local.ReportFileName = ParaPathAplicacion + "\\..\\Elementoscomunes\\rpt\\AIC4120R1.rpt";
                        listado_local.ReportFileName = ParaPathAplicacion + "\\Elementoscomunes\\rpt\\AIC4120R1_CR11.rpt";
                        //listado_local.Formulas[0] = "FORM1=\"" + bDatos_juzgado.VstrCrystal.VstGrupoHo + "\"";
                        listado_local.Formulas["FORM1"] = "\"" + bDatos_juzgado.VstrCrystal.VstGrupoHo + "\"";
                        //listado_local.Formulas[1] = "FORM2=\"" + bDatos_juzgado.VstrCrystal.VstNombreHo + "\"";
                        listado_local.Formulas["FORM2"] = "\"" + bDatos_juzgado.VstrCrystal.VstNombreHo + "\"";
                        //listado_local.Formulas[2] = "FORM3=\"" + bDatos_juzgado.VstrCrystal.VstDireccHo + "\"";
                        listado_local.Formulas["FORM3"] = "\"" + bDatos_juzgado.VstrCrystal.VstDireccHo + "\"";

                        //DV 
                        //listado_local.Formulas[3] = "LitPers=\"" + LitPersona + "\"";
                        listado_local.Formulas["LitPers"] = "\"" + LitPersona + "\"";


                        sTitulo = "RELACIÓN DE PARTES EMITIDOS AL JUZGADO DE " + LitPersona.ToUpper() + "S";
                        if (ParaSoloIngresados)
                        {
                            sTitulo = sTitulo + " INGRESADOS";
                        }

                        //listado_local.Formulas[4] = "TituloLitPers=\"" + sTitulo + "\"";
                        listado_local.Formulas["TituloLitPers"] = "\"" + sTitulo + "\"";

                        //RELACIÓN DE PARTES EMITIDOS AL JUZGADO 
                        listado_local.Destination = 0;
                        //listado_local.WindowState = 2;
                        listado_local.WindowState = Crystal.WindowStateConstants.crptMaximized;

                        listado_local.WindowTitle = "Relación de partes emitidos a juzgado";

                        //listado_local.Connect = "DSN=" + ParaDsnCrystal + ";UID=" + ParaUsuarioCrystal + ";PWD=" + ParaContraseñaCrystal + "";
                        listado_local.Connect = ParaDsnCrystal + ";" + ParaUsuarioCrystal + "; " + ParaContraseñaCrystal + ";";
                        listado_local.SQLQuery = sql;

                        listado_local.Action = 1;
                        listado_local.Reset();

                        break;
                    case "AIC4121R1":
                        //Imprimir_notificaciones_pendientes_expediente 
                        sql = " SELECT " +
                              " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.ndninifp," +
                              " AEPISADM.fultnotif From  aepisadm INNER JOIN dpacient ON " +
                              " AEPISADM.gidenpac = DPACIENT.gidenpac" +
                              " Where " +
                              " AEPISADM.NNUMEXP1 IS NULL AND " +
                              " (aepisadm.ienfment = 'S' or aepisadm.fnotifincap is not null)and " +
                              " AEPISADM.FALTAADM IS NULL ";
                        if (!bDatos_juzgado.VstrCrystal.VfCabecera)
                        {
                            bDatos_juzgado.proCabCrystal();
                        }
                        //hay que cambiar el destino del informe y la ubicacion 
                        //listado_local.ReportFileName = ParaPathAplicacion + "\\..\\Elementoscomunes\\rpt\\AIC4121R1.rpt";
                        listado_local.ReportFileName = ParaPathAplicacion + "\\ELEMENTOSCOMUNES\\rpt\\AIC4121R1_CR11.rpt";
                        //listado_local.Formulas[0] = "FORM1=\"" + bDatos_juzgado.VstrCrystal.VstGrupoHo + "\"";
                        listado_local.Formulas["FORM1"] = "\"" + bDatos_juzgado.VstrCrystal.VstGrupoHo + "\"";
                        //listado_local.Formulas[1] = "FORM2=\"" + bDatos_juzgado.VstrCrystal.VstNombreHo + "\"";
                        listado_local.Formulas["FORM2"] = "\"" + bDatos_juzgado.VstrCrystal.VstNombreHo + "\"";
                        //listado_local.Formulas[2] = "FORM3=\"" + bDatos_juzgado.VstrCrystal.VstDireccHo + "\"";
                        listado_local.Formulas["FORM3"] = "\"" + bDatos_juzgado.VstrCrystal.VstDireccHo + "\"";
                        //listado_local.Formulas[3] = "LitPers=\"" + LitPersona + "\"";
                        listado_local.Formulas["LitPers"] = "\"" + LitPersona + "\"";

                        listado_local.Destination = 0;
                        //listado_local.WindowState = 2;
                        listado_local.WindowState = Crystal.WindowStateConstants.crptMaximized;
                        listado_local.WindowTitle = "Relación de notificaciones al juzgado pendientes de expediente"; 
						//listado_local.Connect = "DSN=" + ParaDsnCrystal + ";UID=" + ParaUsuarioCrystal + ";PWD=" + ParaContraseñaCrystal + "";
                        listado_local.Connect = ParaDsnCrystal + ";" + ParaUsuarioCrystal + "; " + ParaContraseñaCrystal + ";";
                        listado_local.SQLQuery = sql; 
						 
						// la sql la tiene guardada con el report 
						listado_local.Action = 1; 
						listado_local.Reset(); 
						 
						break;
					case "AGI174R1" : 
						//Imprimir_Notificacion_permanencia_centro 
						 
						sql = "select valfanu1  from sconsglo where gconsglo = 'ndnidire'";  //Busca el nombre del director 
						SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion); 
						RrSql = new DataSet(); 
						tempAdapter.Fill(RrSql); 
						stNombre_Director = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper(); 
						 
						sql = "select valfanu1  from sconsglo where gconsglo = 'nombreho'";  //Busca el nombre del hospital 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion); 
						RrSql = new DataSet(); 
						tempAdapter_2.Fill(RrSql); 
						stNHospit = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim(); 
						 
						sql = "select valfanu1,valfanu2   from sconsglo where gconsglo = 'direccho'";  //busca la dirección del hospital 
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion); 
						RrSql = new DataSet(); 
						tempAdapter_3.Fill(RrSql); 
						stDDireccion = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim() + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim(); 

						 
						//************************* O.Frias (SQL-2005) ************************* 
						//'''        sql = "select dnomprov From sconsglo , dprovinc Where sconsglo.gconsglo = 'CODPROVI' and  substring(valfanu1,1,2)=gprovinc" 
						 
						sql = "SELECT DPROVINC.dnomprov FROM SCONSGLO " + 
						      "INNER JOIN DPROVINC ON SUBSTRING(ISNULL(SCONSGLO.valfanu1, ''), 1, 2) = DPROVINC.gprovinc " + 
						      "WHERE (SCONSGLO.gconsglo = 'CODPROVI') "; 
						 
						//************************* O.Frias (SQL-2005) ************************* 
						 
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion); 
						RrSql = new DataSet(); 
						tempAdapter_4.Fill(RrSql); 
						stPoblacion = Convert.ToString(RrSql.Tables[0].Rows[0]["dnomprov"]).Trim(); 
						 
						//antes de hacer la select hay que cargar fecha del dia menos 10 dias para que lo haga 10 dias antes 
						//tenemos que cargar la fecha del dia más 10 dias para que avise 10 dias antes de que suceda el evento 
						stFechaHoy = DateTimeHelper.ToString(DateTime.Now); 
						stFechaBusqueda = Convert.ToDateTime(DateTimeHelper.ToString(DateTime.FromOADate(10).AddDays(DateTime.Now.ToOADate()))).ToShortDateString(); 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						stFechaBusqueda = stFechaBusqueda + " " + ((DateTime.TryParse(stFechaHoy, out TempDate)) ? TempDate.ToString("HH:mm:ss") : stFechaHoy); 
						 
						bDatos_juzgado.ObtenerValoresTipoBD(); 
						 
						System.DateTime tempRefParam = DateTime.Parse(stFechaBusqueda); 
						stFechaFormateada = Serrores.FormatFecha(tempRefParam); 
						stFechaFormateada = (DateTimeHelper.ToString(DateTime.Parse(stFechaBusqueda))); 
						if (Strings.InStr((stFechaFormateada.IndexOf(bDatos_juzgado.Separador_Hora_SQL) + 1) + 1, stFechaFormateada, bDatos_juzgado.Separador_Hora_SQL, CompareMethod.Binary) == 0)
						{
							stFechaFormateada = stFechaFormateada.Substring(0, Math.Min(10, stFechaFormateada.Length));
							System.DateTime TempDate2 = DateTime.FromOADate(0);
							stFechaFormateada = stFechaFormateada + " " + ((DateTime.TryParse(stFechaHoy, out TempDate2)) ? TempDate2.ToString("HH:mm:ss") : stFechaHoy);
						}
						else
						{
							System.DateTime TempDate3 = DateTime.FromOADate(0);
							stFechaFormateada = stFechaFormateada.Substring(0, Math.Min(Strings.InStr((stFechaFormateada.IndexOf(bDatos_juzgado.Separador_Hora_SQL) + 1) + 1, stFechaFormateada, bDatos_juzgado.Separador_Hora_SQL, CompareMethod.Binary) - 1, stFechaFormateada.Length)) + bDatos_juzgado.Separador_Hora_SQL + ((DateTime.TryParse(stFechaHoy, out TempDate3)) ? TempDate3.ToString("ss") : stFechaHoy);
						} 
						 
						object tempRefParam2 = stFechaFormateada; 
						object tempRefParam3 = stFechaFormateada; 
						sql = "select " + 
						      " dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, " + 
						      " dpacient.ndninifp , aepisadm.nnumexp1, aepisadm.gjuzgad1, " + 
						      " aepisadm.ganoadme, aepisadm.gnumadme, aepisadm.gidenpac, " + 
						      " aepisadm.fultnotif " + 
						      " From aepisadm , dpacient Where " + 
						      " aepisadm.faltplan is null and aepisadm.gjuzgad1 is not null and " + 
						      " aepisadm.gidenpac = dpacient.gidenpac and " + 
						      " ((aepisadm.fultnotif is null and aepisadm.fresolu1 is not null and " + 
						      " " + Serrores.FormatFechaHMS(tempRefParam2) + " >= dateadd(mm, 6,aepisadm.fresolu1)) " + 
						      " or " + 
						      " (aepisadm.fultnotif is not null and " + 
						      " " + Serrores.FormatFechaHMS(tempRefParam3) + " >= dateadd(mm, 6, aepisadm.fultnotif)))"; 
						stFechaFormateada = Convert.ToString(tempRefParam3); 
						stFechaFormateada = Convert.ToString(tempRefParam2);

                       

                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion); 
						RrSql = new DataSet(); 
						tempAdapter_5.Fill(RrSql); 
						if (RrSql.Tables[0].Rows.Count != 0)
						{

							//intRespuesta = MessageBox.Show("Se van a imprimir " + StringsHelper.Format(RrSql.Tables[0].Rows.Count, "##,##0") + " notificaciones. ¿Desea previsualizarlas? ", "Gestión Asistencial y Departamental", MessageBoxButtons.YesNoCancel);
                            //intRespuesta = MessageBox.Show("Se van a imprimir " + StringsHelper.Format(RrSql.Tables[0].Rows.Count, "##,##0") + " notificaciones. ¿Desea previsualizarlas? ", "Gestión Asistencial y Departamental", MessageBoxButtons.YesNoCancel);
                            intRespuesta = RadMessageBox.Show("Se van a imprimir " + StringsHelper.Format(RrSql.Tables[0].Rows.Count, "##,##0") + " notificaciones. ¿Desea previsualizarlas? ", "Gestión Asistencial y Departamental", MessageBoxButtons.YesNoCancel);
                            if (intRespuesta != System.Windows.Forms.DialogResult.Cancel)
							{

                                bDatos_juzgado.cnConexion.BeginTransScope();
								bInicioTransaccion = true;
                               

                                foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
                                {
                                    listado_local.ReportFileName = ParaPathAplicacion + "\\..\\Elementoscomunes\\rpt\\AGI174R1_CR11.rpt";

                                    Sql_Datos = "select * from djuzgado where gjuzgado = '" + Convert.ToString(iteration_row["gjuzgad1"]) + "'";
                                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Sql_Datos, bDatos_juzgado.cnConexion);
                                    RrSql_Datos = new DataSet();
                                    tempAdapter_6.Fill(RrSql_Datos);
                                    if (RrSql_Datos.Tables[0].Rows.Count != 0)
                                    {
                                        stJuzgado = Convert.ToString(RrSql_Datos.Tables[0].Rows[0]["dnomjuzg"]).Trim() + "";
                                        stNJuzgado = Convert.ToString(RrSql_Datos.Tables[0].Rows[0]["dnomnum"]).Trim() + "";
                                        stDireccion = Convert.ToString(RrSql_Datos.Tables[0].Rows[0]["ddirjuzg"]).Trim() + "";
                                        stProvincia = new StringBuilder(Convert.ToString(RrSql_Datos.Tables[0].Rows[0]["gcodipos"]).Trim() + "");
                                        stGidenpac = Convert.ToString(RrSql.Tables[0].Rows[0]["gidenpac"]).Trim() + "";
                                        if (!Convert.IsDBNull(RrSql_Datos.Tables[0].Rows[0]["gprovinc"]))
                                        {
                                            stProvincia.Append(" " + bDatos_juzgado.probuscar_provincia(Convert.ToString(RrSql_Datos.Tables[0].Rows[0]["gprovinc"])));
                                        }
                                        else
                                        {
                                            stProvincia.Append(" ");
                                        }
                                    }
                                    RrSql_Datos.Close();

                                    //listado_local.Formulas[0] = "Juzgado= \"" + stJuzgado + "\"";
                                    listado_local.Formulas["Juzgado"] = "\"" + stJuzgado + "\"";
                                    //listado_local.Formulas[1] = "NJuzgado= \"" + stNJuzgado + "\"";
                                    listado_local.Formulas["stNJuzgado"] = "\"" + stNJuzgado + "\"";
                                    //listado_local.Formulas[2] = "Direccion= \"" + stDireccion + "\"";
                                    listado_local.Formulas["Direccion"] = "\"" + stDireccion + "\"";
                                    //listado_local.Formulas[3] = "Provincia= \"" + stProvincia.ToString() + "\"";
                                    listado_local.Formulas["Provincia"] = "\"" + stProvincia.ToString() + "\"";
                                    //listado_local.Formulas[4] = "Nombre_Director= \"" + stNombre_Director + "\"";
                                    listado_local.Formulas["Nombre_Director"] = "\"" + stNombre_Director + "\"";
                                    //listado_local.Formulas[5] = "NHospit= \"" + stNHospit + "\"";
                                    listado_local.Formulas["NHospit"] = "\"" + stNHospit + "\"";
                                    //listado_local.Formulas[6] = "DDireccion= \"" + stDDireccion + "\"";
                                    listado_local.Formulas["DDireccion"] = "\"" + stDDireccion + "\"";

                                    stPaciente = new StringBuilder("");
                                    if (!Convert.IsDBNull(iteration_row["dnombpac"]))
                                    {
                                        stPaciente = new StringBuilder(Convert.ToString(iteration_row["dnombpac"]).Trim());
                                    }
                                    if (!Convert.IsDBNull(iteration_row["dape1pac"]))
                                    {
                                        stPaciente.Append(" " + Convert.ToString(iteration_row["dape1pac"]).Trim());
                                    }
                                    if (!Convert.IsDBNull(iteration_row["dape2pac"]))
                                    {
                                        stPaciente.Append(" " + Convert.ToString(iteration_row["dape2pac"]).Trim());
                                    }
                                    //listado_local.Formulas[7] = "Paciente= \"" + stPaciente.ToString() + "\"";
                                    listado_local.Formulas["Paciente"] = "\"" + stPaciente.ToString() + "\"";

                                    if (!Convert.IsDBNull(iteration_row["ndninifp"]))
                                    {
                                        //listado_local.Formulas[8] = "DNINIF= \"" + Convert.ToString(iteration_row["ndninifp"]) + "\"";
                                        listado_local.Formulas["DNINIF"] = "\"" + Convert.ToString(iteration_row["ndninifp"]) + "\"";
                                    }
                                    //listado_local.Formulas[9] = "Nexpediente= \"" + Convert.ToString(iteration_row["nnumexp1"]) + "\"";
                                    listado_local.Formulas["Nexpediente"] = "\"" + Convert.ToString(iteration_row["nnumexp1"]) + "\"";
                                    //listado_local.Formulas[10] = "Poblacion= \"" + stPoblacion + "\"";
                                    listado_local.Formulas["Poblacion"] = "\"" + stPoblacion + "\"";

                                    //listado_local.ReportFileName = ParaPathAplicacion + "\\..\\Elementoscomunes\\rpt\\AGI174R1.rpt";
                                    

                                    //28/03/07
                                    //Se condiciona segun la opción selecionada Si --> Pantalla, No -- Impresora.
                                    //O.Frias
                                    if (intRespuesta == System.Windows.Forms.DialogResult.Yes)
                                    { //' Pantalla
                                        listado_local.Destination = 0;
                                    }
                                    else
                                    {
                                        listado_local.Destination = Crystal.DestinationConstants.crptToPrinter;
                                    }



                                    //listado_local.WindowState = 2;
                                    listado_local.WindowState = Crystal.WindowStateConstants.crptMaximized;
                                    listado_local.WindowTitle = "Notificación de permanencia en el centro";

                                    //Aqui se realiza la modificacion de cada registro
                                    //en caso de que falle algo ni se actualiza el registro
                                    //ni lanza el informe.
                                    //stAño = RrSQL("ganoadme")
                                    //stNumero = RrSQL("gnumadme")
                                    //Fecha_ultimo_movimiento = Format(RrSQL("fultnotif"), "dd/mm/yyyy hh:nn")
                                    

                                    RrSql.Edit();
                                    iteration_row["fultnotif"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_5);

                                    //INDRA SDSALAZAR 17-06-2016
                                    //SE CREA UPDATE PARA QUE NO SE GENERE EL ERROR DE UPDATE DE DOS TABLAS
                                    string sqlUpdate;
                                    sqlUpdate = "update AEPISADM SET fultnotif = '" +  DateTime.Now.ToString("yyyy/MM/dd HH:mm") + "'" +
                                    " Where " +
                                    " aepisadm.faltplan is null and aepisadm.gjuzgad1 is not null and " +
                                    " aepisadm.gidenpac = '" + stGidenpac + "' and " +
                                    " ((aepisadm.fultnotif is null and aepisadm.fresolu1 is not null and " +
                                    " " + Serrores.FormatFechaHMS(tempRefParam2) + " >= dateadd(mm, 6,aepisadm.fresolu1)) " +
                                    " or " +
                                    " (aepisadm.fultnotif is not null and " +
                                    " " + Serrores.FormatFechaHMS(tempRefParam3) + " >= dateadd(mm, 6, aepisadm.fultnotif)))";

                                    SqlCommand command = new SqlCommand(sqlUpdate, bDatos_juzgado.cnConexion);
                                    command.ExecuteNonQuery();
                                    // tempAdapter_5.Update(RrSql, RrSql.Tables[0].TableName);

									listado_local.CopiesToPrinter = 2;
									listado_local.Action = 1;
									listado_local.Reset();

								}

                                bDatos_juzgado.cnConexion.CommitTransScope();
							}

						}
						else
						{
							short tempRefParam4 = 1520;
							string[] tempRefParam5 = new string[]{LitPersona, "notificación de permanencia en el centro"};
							Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, bDatos_juzgado.cnConexion, tempRefParam5);
						} 
						RrSql.Close(); 
						 
						break;
				}

				listado_local = null;
				Cursor.Current = Cursors.Default;
			}
			catch (Exception excep)
			{

				if (bInicioTransaccion)
				{
                    bDatos_juzgado.cnConexion.RollbackTransScope();
				}
				listado_local.Reset();
				if (excep.Source.ToUpper() == "CRYSTALREPORT")
				{
					Serrores.GestionErrorCrystal(Information.Err().Number, bDatos_juzgado.cnConexion);
				}
				else
				{
                    Serrores.AnalizaError("DLLDatosJuzgado", Path.GetDirectoryName(Application.ExecutablePath), "indra", "clsDatos_juzgado:imprimir", excep);
				}
			}
		}
	}
}