using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace DLLDatosJuzgado
{
	public partial class resol_juzgado
		: RadForm
	{

		//HEMOS CAMBIADO A DROPDOWLIST LA PROPIEDAD DE LOS COMBOS
		//DE JUZGADOS PARA QUE NO PUEDAN ESCRIBIR EN ELLOS LO QUE LES DE LA GANA


		DataSet rrpaciente = null;
		public int mganoregi = 0;
		public int mgnumregi = 0;
		public string mgidenpac = String.Empty;
		public string mVstCodUsua = String.Empty;
		public resol_juzgado()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string stMensaje1 = String.Empty;
			string stMensaje2 = String.Empty;
			string stMensaje3 = String.Empty;
			if (Convert.ToDouble(cbbJuzgado.get_ListIndex()) != -1 && Convert.ToDouble(cbbJuzgado2.get_ListIndex()) != -1)
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbJuzgado.get_ListIndex();
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbJuzgado2.get_ListIndex();
				if (cbbJuzgado.get_Column(tempRefParam, tempRefParam2).Equals(cbbJuzgado2.get_Column(tempRefParam3, tempRefParam4)))
				{
					stMensaje1 = "El juzgado del " + frmPrimerExp.Text;
					stMensaje2 = "distinto";
					stMensaje3 = "el juzgado del " + Label7.Text;
					short tempRefParam5 = 1020;
					string[] tempRefParam6 = new string[]{stMensaje1, stMensaje2, stMensaje3};
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, bDatos_juzgado.cnConexion, tempRefParam6));
				}
				else
				{
					grabar_registro();
				}
			}
			else
			{
				if (Convert.ToDouble(cbbJuzgado.get_ListIndex()) != -1)
				{
					grabar_registro();
				}
			}
			CbCancelar_Click(CbCancelar, new EventArgs());
		}


		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void ChbEnfMental_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Aceptar();
		}

		private void resol_juzgado_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				cbbJuzgado2.set_ListIndex(-1);
				cbbJuzgado2.set_ListIndex(-1);

				//************************* O.Frias (SQL-2005) *************************
				//'''sql = "SELECT " _
				//''''        & " DPACIENT.DAPE1PAC,DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC,dpacient.gidenpac, " _
				//''''        & " Dpacient.GSOCIEDA,DPACIENT.IFILPROV,DPACIENT.NAFILIAC, " _
				//''''        & " AEPISADM.gnumadme,AEPISADM.ganoadme, " _
				//''''        & " AEPISADM.gjuzgad1, AEPISADM.nnumexp1, AEPISADM.fresolu1, " _
				//''''        & " AEPISADM.gjuzgad2, AEPISADM.nnumexp2, AEPISADM.fresolu2 " _
				//''''        & " FROM " _
				//''''        & " DPACIENT, AEPISADM, DENTIPAC" _
				//''''        & " WHERE " _
				//''''        & " DPACIENT.GIDENPAC= '" & mgidenpac & "' and" _
				//''''        & " DPACIENT.GIDENPAC = AEPISADM.GIDENPAC AND " _
				//''''        & " AEPISADM.GIDENPAC *= DENTIPAC.GIDENPAC AND " _
				//''''        & " AEPISADM.FALTPLAN IS NULL and AEPISADM.FALtaadm is null AND " _
				//''''        & " ganoadme= " & mganoregi & " AND gnumadme=" & mgnumregi


				string sql = "SELECT DPACIENT.dape1pac, ISNULL(DPACIENT.dape2pac,'') DAPE2PAC, DPACIENT.dnombpac, DPACIENT.gidenpac, " + 
				             "DPACIENT.gsocieda, DPACIENT.ifilprov, DPACIENT.nafiliac, AEPISADM.gnumadme, " + 
				             "AEPISADM.ganoadme, AEPISADM.gjuzgad1, AEPISADM.nnumexp1, AEPISADM.fresolu1, " + 
				             "AEPISADM.gjuzgad2, AEPISADM.nnumexp2, AEPISADM.fresolu2, AEPISADM.IENFMENT " + 
				             "FROM DPACIENT " + 
				             "INNER JOIN AEPISADM ON DPACIENT.gidenpac = AEPISADM.gidenpac " + 
				             "LEFT OUTER JOIN DENTIPAC ON AEPISADM.gidenpac = DENTIPAC.gidenpac " + 
				             "WHERE (DPACIENT.gidenpac = '" + mgidenpac + "' ) AND " + 
				             "(AEPISADM.faltplan IS NULL) AND " + 
				             "(AEPISADM.faltaadm IS NULL) AND " + 
				             "(AEPISADM.ganoadme = " + mganoregi.ToString() + " ) AND " + 
				             "(AEPISADM.gnumadme = " + mgnumregi.ToString() + " ) ";

				//************************* O.Frias (SQL-2005) *************************
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion);
				rrpaciente = new DataSet();
				tempAdapter.Fill(rrpaciente);
				if (rrpaciente.Tables[0].Rows.Count != 0)
				{

					proNumero_Historia();
					proSociedad();

					if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE1PAC"]) != "")
					{
						LbPaciente.Text = Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE1PAC"]).Trim();
					}
					if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE2PAC"]) != "")
					{
						LbPaciente.Text = LbPaciente.Text + " " + Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
					}
					if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["DNOMBPAC"]) != "")
					{
						LbPaciente.Text = LbPaciente.Text + " " + Convert.ToString(rrpaciente.Tables[0].Rows[0]["DNOMBPAC"]).Trim();
					}
					if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["NAFILIAC"]) != "")
					{
						LbNumsegu.Text = Convert.ToString(rrpaciente.Tables[0].Rows[0]["NAFILIAC"]);
					}

					if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["IENFMENT"]) == "S")
					{
						ChbEnfMental.CheckState = CheckState.Checked;
					}
					else if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["IENFMENT"]) == "N")
					{ 
						ChbEnfMental.CheckState = CheckState.Unchecked;
					}

					cargar_combos();
					cargar_datos();
					proActivar_Duplicado();

				}

				sdcFechaResol1.MaxDate = DateTime.Now;
				sdcFechaResol2.MaxDate = DateTime.Now;
				cbbJuzgado.Focus();
				cbbJuzgado.SelectionStart = 0;
				cbbJuzgado.SelectionLength = Strings.Len(cbbJuzgado.Text);

			}
		}
		public void proSociedad()
		{
			if (Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["gsocieda"]))
			{
				return;
			}


			string SQSoci = "SELECT Fmovimie,gsocieda " + " FROM  AMOVIFIN " + " WHERE " + "  AMOVIFIN.GNUMREGI=" + Convert.ToString(rrpaciente.Tables[0].Rows[0]["GNUMADME"]) + " AND " + "  AMOVIFIN.GANOREGI= " + Convert.ToString(rrpaciente.Tables[0].Rows[0]["GANOADME"]) + " order by fmovimie";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQSoci, bDatos_juzgado.cnConexion);
			DataSet tRramovifin = new DataSet();
			tempAdapter.Fill(tRramovifin);
			tRramovifin.MoveLast(null);
			string tstNuevo = Convert.ToString(tRramovifin.Tables[0].Rows[0]["gsocieda"]);
			tRramovifin.Close();

			SQSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQSoci, bDatos_juzgado.cnConexion);
			DataSet RrSoci = new DataSet();
			tempAdapter_2.Fill(RrSoci);
			if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == StringsHelper.ToDoubleSafe(tstNuevo))
			{ //Seguridad Social
				LbFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
				return;
			}
			RrSoci.Close();


			SQSoci = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQSoci, bDatos_juzgado.cnConexion);
			RrSoci = new DataSet();
			tempAdapter_3.Fill(RrSoci);
			if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == StringsHelper.ToDoubleSafe(tstNuevo))
			{ // Privado
				LbFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
				return;
			}
			RrSoci.Close();

			SQSoci = "SELECT dsocieda,IREGALOJ FROM DSOCIEDA WHERE gSOCIEDA = " + tstNuevo;
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQSoci, bDatos_juzgado.cnConexion);
			RrSoci = new DataSet();
			tempAdapter_4.Fill(RrSoci);
			LbFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["DSOCIEDA"]).Trim();
			RrSoci.Close();

		}


		public void proNumero_Historia()
		{
			string StSqHistoria = "SELECT GHISTORIA,GSERARCH FROM HSERARCH, HDOSSIER " + " WHERE GIDENPAC = '" + Convert.ToString(rrpaciente.Tables[0].Rows[0]["gidenpac"]) + "' AND " + " GSERPROPIET = GSERARCH AND " + " ICENTRAL='S' and icontenido='H'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqHistoria, bDatos_juzgado.cnConexion);
			DataSet rrhistoria = new DataSet();
			tempAdapter.Fill(rrhistoria);
			if (rrhistoria.Tables[0].Rows.Count == 1)
			{
				LbHistoria.Text = Conversion.Str(rrhistoria.Tables[0].Rows[0]["GHISTORIA"]);
			}
			else
			{
				LbHistoria.Text = "";
			}
			rrhistoria.Close();
			return;
		}

		private void resol_juzgado_Load(Object eventSender, EventArgs eventArgs)
		{

            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_PRINCIPAL);

			string LitPersona = Serrores.ObtenerLiteralPersona(bDatos_juzgado.cnConexion);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label28.Text = LitPersona;
			Frame2.Text = "Datos del " + LitPersona;
		}

		private void resol_juzgado_Closed(Object eventSender, EventArgs eventArgs)
		{
			rrpaciente.Close();
			MemoryHelper.ReleaseMemory();
		}


		private void cargar_combos()
		{
			string sql = String.Empty;
			DataSet RrSQL = null;
			int i = 0;
			try
			{
				cbbJuzgado.Clear();
				cbbJuzgado2.Clear();
				sql = "select * from djuzgado where fborrado is null order by dnomjuzg";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);
				//cbbJuzgado.ColumnCount = 2;
				//cbbJuzgado2.ColumnCount = 2;
				cbbJuzgado.ColumnWidths = "40; 0";
				cbbJuzgado2.ColumnWidths = "40; 0";
				foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
				{
					object tempRefParam = Convert.ToString(iteration_row["dnomjuzg"]).Trim();
					object tempRefParam2 = Type.Missing;
					cbbJuzgado.AddItem(tempRefParam, tempRefParam2);
					object tempRefParam3 = Convert.ToString(iteration_row["dnomjuzg"]).Trim();
					object tempRefParam4 = Type.Missing;
					cbbJuzgado2.AddItem(tempRefParam3, tempRefParam4);
					cbbJuzgado.set_Column(1, i, Convert.ToString(iteration_row["gjuzgado"]).Trim());
					cbbJuzgado2.set_Column(1, i, Convert.ToString(iteration_row["gjuzgado"]).Trim());
					i++;
				}
				RrSQL.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), mVstCodUsua, "resol_juzgado:carga_combos", ex);
			}
		}


		private void cargar_datos()
		{
			//busca el juzgado del primer expediente
			try
			{
                tbExpediente.Text = "";
                sdcFechaResol1.NullableValue = null;

                if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["gjuzgad1"]))
				{
					for (int i = 0; i <= cbbJuzgado.ListCount - 1; i++)
					{
						object tempRefParam = 1;
						object tempRefParam2 = i;
						if (Convert.ToString(cbbJuzgado.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper() == Convert.ToString(rrpaciente.Tables[0].Rows[0]["gjuzgad1"]).Trim().ToUpper())
						{
							i = Convert.ToInt32(tempRefParam2);
							cbbJuzgado.set_ListIndex(i);
							break;
						}
						else
						{
							i = Convert.ToInt32(tempRefParam2);
						}
					}
				}
				//asigna el n�mero del primer expediente
				if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["nnumexp1"]))
				{
					tbExpediente.Text = Convert.ToString(rrpaciente.Tables[0].Rows[0]["nnumexp1"]).Trim();
				}
				//asigna la fecha del primer expediente
				if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["fresolu1"]))
				{
					sdcFechaResol1.Value = Convert.ToDateTime(rrpaciente.Tables[0].Rows[0]["fresolu1"]);
					sdcFechaResol1.Text = Convert.ToDateTime(rrpaciente.Tables[0].Rows[0]["fresolu1"]).ToString("dd/MM/yyyy");
					proActivar_Aceptar();
				}

				//busca el juzgado del segundo expediente
				if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["gjuzgad2"]))
				{
					for (int i = 0; i <= cbbJuzgado2.ListCount - 1; i++)
					{
						object tempRefParam3 = 1;
						object tempRefParam4 = i;
						if (Convert.ToString(cbbJuzgado.get_Column(tempRefParam3, tempRefParam4)).Trim().ToUpper() == Convert.ToString(rrpaciente.Tables[0].Rows[0]["gjuzgad2"]).Trim().ToUpper())
						{
							i = Convert.ToInt32(tempRefParam4);
							cbbJuzgado2.set_ListIndex(i);
							break;
						}
						else
						{
							i = Convert.ToInt32(tempRefParam4);
						}
					}
				}
				//asigna el n�mero del segundo expediente
				if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["nnumexp2"]))
				{
					tbExpediente2.Text = Convert.ToString(rrpaciente.Tables[0].Rows[0]["nnumexp2"]).Trim();
				}

				//asigna la fecha del segundo expediente
				if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["fresolu2"]))
				{
					sdcFechaResol2.Value = Convert.ToDateTime(rrpaciente.Tables[0].Rows[0]["fresolu2"]);
					sdcFechaResol2.Text = Convert.ToDateTime(rrpaciente.Tables[0].Rows[0]["fresolu2"]).ToString("dd/MM/yyyy");
					proActivar_Aceptar();
				}
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), mVstCodUsua, "resol_juzgado:carga_datos", ex);
			}
		}


		private void proActivar_Duplicado()
		{
			if (Convert.ToDouble(cbbJuzgado.get_ListIndex()) != -1 && tbExpediente.Text.Trim() != "" && sdcFechaResol1.NullableValue != null)
			{
				frmDuplicado.Enabled = true;
			}
			else
			{
				frmDuplicado.Enabled = false;
				cbbJuzgado2.set_ListIndex(-1);
				tbExpediente2.Text = "";
				sdcFechaResol2.NullableValue = null;
			}
		}


		private void cbbJuzgado_Change(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}

		private void cbbJuzgado2_Change(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}
		private void sdcFechaResol1_Click(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}

		private void sdcFechaResol1_Leave(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}

		private void sdcFechaResol2_Leave(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}

		private void tbExpediente_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}

		private void tbExpediente_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbExpediente2_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Duplicado();
			proActivar_Aceptar();
		}

		private void proActivar_Aceptar()
		{

            if (ChbEnfMental.CheckState == CheckState.Checked && (Convert.ToDouble(cbbJuzgado.get_ListIndex()) == -1 || tbExpediente.Text == "" || sdcFechaResol1.NullableValue == null))
			{
				CbAceptar.Enabled = false;
			}
			else
			{
				if ((Convert.ToDouble(cbbJuzgado.get_ListIndex()) > -1 && cbbJuzgado.Text != "") && (tbExpediente.Text == "" || !Information.IsDate(sdcFechaResol1.Value.Date.ToString())))
				{
					CbAceptar.Enabled = false;
				}
				else if (tbExpediente.Text != "" && ((Convert.ToDouble(cbbJuzgado.get_ListIndex()) == -1 || cbbJuzgado.Text == "") || !Information.IsDate(sdcFechaResol1.Value.Date.ToString())))
				{ 
					CbAceptar.Enabled = false;
				}
				else if (Information.IsDate(sdcFechaResol1.Value.Date.ToString()) && ((Convert.ToDouble(cbbJuzgado.get_ListIndex()) == -1 || cbbJuzgado.Text == "") || tbExpediente.Text == ""))
				{ 
					CbAceptar.Enabled = false;
				}
			}

			//si no esta completo el primer expediente
			try
			{
                if (Convert.ToDouble(cbbJuzgado.get_ListIndex()) == -1 || tbExpediente.Text.Trim() == "" || sdcFechaResol1.NullableValue == null)
				{
					CbAceptar.Enabled = false;
				}
				else
				{
					//comprueba si el segundo no tiene nada
					if (Convert.ToDouble(cbbJuzgado2.get_ListIndex()) == -1 && tbExpediente2.Text.Trim() == "" && sdcFechaResol2.NullableValue == null)
					{
						CbAceptar.Enabled = true;
					}
					else
					{
						//si tiene algo comprueba que este todo
						CbAceptar.Enabled = Convert.ToDouble(cbbJuzgado2.get_ListIndex()) != -1 && tbExpediente2.Text.Trim() != "" && sdcFechaResol2.NullableValue != null;
					}
				}
				string stMensaje1 = String.Empty;
				string stMensaje2 = String.Empty;
				string stMensaje3 = String.Empty;
				if (Convert.ToDouble(cbbJuzgado.get_ListIndex()) != -1 && Convert.ToDouble(cbbJuzgado2.get_ListIndex()) != -1)
				{
					object tempRefParam = 1;
					object tempRefParam2 = cbbJuzgado.get_ListIndex();
					object tempRefParam3 = 1;
					object tempRefParam4 = cbbJuzgado2.get_ListIndex();
					if (cbbJuzgado.get_Column(tempRefParam, tempRefParam2).Equals(cbbJuzgado2.get_Column(tempRefParam3, tempRefParam4)))
					{
						stMensaje1 = "El juzgado del " + frmPrimerExp.Text;
						stMensaje2 = "distinto";
						stMensaje3 = "el juzgado del " + Label7.Text;
						short tempRefParam5 = 1020;
						string[] tempRefParam6 = new string[]{stMensaje1, stMensaje2, stMensaje3};
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, bDatos_juzgado.cnConexion, tempRefParam6));
                        cbbJuzgado2.Text = null;
					}
				}
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), mVstCodUsua, "resol_juzgado:proactivar_aceptar", null);
			}
		}

		private void grabar_registro()
		{
			try
			{
				this.Cursor = Cursors.WaitCursor;
				string sql = String.Empty;
				DataSet RrSQL = null;
				sql = "select * from aepisadm where " + 
				      " gidenpac = '" + mgidenpac + "' and " + 
				      " ganoadme = " + mganoregi.ToString() + " and " + 
				      " gnumadme = " + mgnumregi.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bDatos_juzgado.cnConexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);
				if (RrSQL.Tables[0].Rows.Count != 0)
				{
                    bDatos_juzgado.cnConexion.BeginTransScope();
					RrSQL.Edit();
					object tempRefParam = 1;
					object tempRefParam2 = cbbJuzgado.get_ListIndex();
					RrSQL.Tables[0].Rows[0]["gjuzgad1"] = cbbJuzgado.get_Column(tempRefParam, tempRefParam2);
					RrSQL.Tables[0].Rows[0]["nnumexp1"] = tbExpediente.Text;
					RrSQL.Tables[0].Rows[0]["fresolu1"] = sdcFechaResol1.Value.Date;
					if (ChbEnfMental.CheckState == CheckState.Checked)
					{ //Enfermedad mental
						RrSQL.Tables[0].Rows[0]["IENFMENT"] = "S";
					}
					else
					{
						RrSQL.Tables[0].Rows[0]["IENFMENT"] = "N";
					}
					if (Convert.ToDouble(cbbJuzgado2.get_ListIndex()) != -1)
					{
						object tempRefParam3 = 1;
						object tempRefParam4 = cbbJuzgado2.get_ListIndex();
						RrSQL.Tables[0].Rows[0]["gjuzgad2"] = cbbJuzgado2.get_Column(tempRefParam3, tempRefParam4);
						RrSQL.Tables[0].Rows[0]["nnumexp2"] = tbExpediente2.Text;
						RrSQL.Tables[0].Rows[0]["fresolu2"] = sdcFechaResol2.Value.Date;
					}
					else
					{
						RrSQL.Tables[0].Rows[0]["gjuzgad2"] = DBNull.Value;
						RrSQL.Tables[0].Rows[0]["nnumexp2"] = DBNull.Value;
						RrSQL.Tables[0].Rows[0]["fresolu2"] = DBNull.Value;
					}

					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrSQL, RrSQL.Tables[0].TableName);
                    bDatos_juzgado.cnConexion.CommitTransScope();
				}
				RrSQL.Close();
				this.Cursor = Cursors.Default;
			}
			catch (Exception ex)
			{

                bDatos_juzgado.cnConexion.RollbackTransScope();
				this.Cursor = Cursors.Default;
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), mVstCodUsua, "resol_juzgado:grabar_registro", ex);
			}

		}


		private void tbExpediente2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
    }
}