using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PrimerHueco
{
	public class clsPrimerHueco
	{


		// DIEGO 18/12/2006 - Optional stTipoHueco As String = "" - Si vienen vac�o funciona como hasta ahora.
		// Le pasar� cualqueir texto, para que distinga entre el RecogerHueco al que tiene que llamar
		// RecogerHueco2 har� lo mimso que el RecogerHueco, pero no modificar� nada


		public void Primer_Hueco(dynamic Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion,  int DuracionPrestacion, string stAgenda, string stTipoHueco, string stUsuario, string stTurno, string stSala)
		{
			string rbTarde = String.Empty;
			string stDESPrestacion = String.Empty;
			SqlCommand Rqd = new SqlCommand();
			SqlCommand rqh = new SqlCommand();
			DataSet rsh = null;
			DataSet rsCitas = null;
			DataSet RsHuecos = null;
			DataSet rsSesiones = null;
            DataSet RsHoras = null;
			System.DateTime FechaAnt = DateTime.FromOADate(0);
			string AgendaAnt = String.Empty;
			int BloqueAnt = 0;
			bool MaximoCitas = false;
			int TiempoResultados = 0;
			string stDESSociedad = String.Empty;

			//OSCAR C
			//Se obtiene la hora a la que cambia el turno entre ma�ana y tarde
			FixedLengthString CambioTurno = new FixedLengthString(5);
			CambioTurno.Value = Serrores.ObternerValor_CTEGLOBAL(Conexion, "HOTURNOM", "valfanu2");
			//------

			Mensajes.ClassMensajes ClaseMensaje = new Mensajes.ClassMensajes();
			Rqd.Connection = Conexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD( ref Conexion);
			Serrores.AjustarRelojCliente(Conexion);
			//Buscando en la tabla de constantes globales el nombre de la aplicaci�n
			string StSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMAPLIC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet rsd = new DataSet();
			tempAdapter.Fill(rsd);
			Serrores.vNomAplicacion = Convert.ToString(rsd.Tables[0].Rows[0]["valfanu1"]).Trim();
			rsd.Close();
			StSql = "SELECT nnumeri1 FROM SCONSGLO WHERE gconsglo = 'segursoc'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
			rsd = new DataSet();
			tempAdapter_2.Fill(rsd);
			int codss = Convert.ToInt32(Double.Parse(Convert.ToString(rsd.Tables[0].Rows[0]["nnumeri1"]).Trim()));
			rsd.Close();

			Serrores.AjustarRelojCliente(Conexion);
			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();

			StSql = "SELECT fborrado FROM dservici WHERE gservici = " + Servicio.ToString();
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, Conexion);
			rsd = new DataSet();
			tempAdapter_3.Fill(rsd);
			if (rsd.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(rsd.Tables[0].Rows[0]["fborrado"]))
				{
					short tempRefParam = 1520;
					string[] tempRefParam2 = new string[] {"fechas disponibles", "la cita solicitada (servicio borrado)"};
					ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam,  Conexion,  tempRefParam2);
					ClaseMensaje = null;
					rsd.Close();
					//18-12-2006 - Diego - controlo el tipo de hueco que quiero buscar, el normal, o para la fecha propuesta
					if (stTipoHueco.Trim() == "")
					{
						Formulario.RecogerHueco(null, null, null, null, null, null, null, null);
					}
					else
					{
						Formulario.RecogerHueco_FechaPrescripcion(null, null, null, null, null, null, null);
					}
					return;
				}
			}
			else
			{
				short tempParam3 = 1520;
				string[] tempRefParam4 = new string[]{"fechas disponibles", "la cita solicitada (servicio inexistente)"};
				ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam3,  Conexion,  tempRefParam4);
				ClaseMensaje = null;
				rsd.Close();
				//18-12-2006 - Diego - controlo el tipo de hueco que quiero buscar, el normal, o para la fecha propuesta
				if (stTipoHueco.Trim() == "")
				{
					Formulario.RecogerHueco(null, null, null, null, null, null, null, null);
				}
				else
				{
					Formulario.RecogerHueco_FechaPrescripcion(null, null, null, null, null, null, null);
				}
				return;
			}


			//Se debe tener en cuenta el perfil de citacion del usuario
			int PerFilCitacionUsuario = Serrores.fPerfilCitacionUsuario(Conexion, stUsuario);
			//----------------


			//Se obtiene la primera fecha con huecos disponibles para la prestacion
			Rqd.CommandText = "SELECT Distinct  cdietari.fdietari ,hinicons,cbloques.gbloques  " + 
			                  "FROM cagendas, cbloques, cprespro, cdietari  ";
			//Ademas se debe tener en cuenta el perfil de citacion del usuario
			Rqd.CommandText = Rqd.CommandText + ", CPECITPR ";
			//------------


			Rqd.CommandText = Rqd.CommandText + " WHERE ";

			//Ademas se debe tener en cuenta los perfiles de citacion del usuario
			Rqd.CommandText = Rqd.CommandText + " CPRESPRO.gagendas = CPECITPR.gagendas and " + 
			                  " CPRESPRO.gbloques = CPECITPR.gbloques and " + 
			                  " CPRESPRO.gprestac = CPECITPR.gprestac and " + 
			                  " ( CPECITPR.gpercita =" + PerFilCitacionUsuario.ToString() + " OR " + 
			                  "   CPECITPR.gpercita IN (SELECT gpercita FROM CUSUPCIT where gusuario='" + stUsuario + "') " + 
			                  " ) ";

			Rqd.CommandText = Rqd.CommandText + " AND cagendas.gagendas = cbloques.gagendas And ";
			if (Servicio != 0)
			{
				Rqd.CommandText = Rqd.CommandText + 
				                  "cagendas.gservici = " + Servicio.ToString() + " AND ";
			}
			if (Responsable != 0)
			{
				Rqd.CommandText = Rqd.CommandText + 
				                  "cagendas.gpersona = " + Responsable.ToString() + " AND ";
			}
			if (SalaConsulta != "")
			{
				Rqd.CommandText = Rqd.CommandText + 
				                  "cagendas.gsalacon = '" + SalaConsulta + "' AND ";
			}
			if (stAgenda != "")
			{
				Rqd.CommandText = Rqd.CommandText + 
				                  "cagendas.gagendas = '" + stAgenda + "' AND ";
			}

			Rqd.CommandText = Rqd.CommandText + 
			                  "(cbloques.icitprop = 'N' OR " + 
			                  " (cbloques.icitprop = 'S' AND cagendas.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
			                  "cbloques.gagendas = cprespro.gagendas AND " + 
			                  "cbloques.gbloques = cprespro.gbloques AND " + 
			                  "cprespro.gprestac = '" + Prestacion + "' AND ";
			if (Sociedad != 0)
			{
				if (Sociedad != codss)
				{
					Rqd.CommandText = Rqd.CommandText + 
					                  " not exists (select ' ' from cagesoci where " + 
					                  " cprespro.gagendas=cagesoci.gagendas and " + 
					                  " cprespro.gbloques=cagesoci.gbloques and " + 
					                  " cprespro.gprestac=cagesoci.gprestac and cagesoci.gsocieda=" + Sociedad.ToString() + ") and ";
				}
				else if (Inspeccion.Trim() != "")
				{ 
					Rqd.CommandText = Rqd.CommandText + 
					                  " not exists (select ' ' from cagesoci where " + 
					                  " cprespro.gagendas=cagesoci.gagendas and " + 
					                  " cprespro.gbloques=cagesoci.gbloques and " + 
					                  " cprespro.gprestac=cagesoci.gprestac and cagesoci.gsocieda = " + Sociedad.ToString() + " and cagesoci.ginspecc='" + Inspeccion + "') and ";
				}
			}
			object tempRefParam5 = FechaBusqueda;
			object tempRefParam6 = DateTime.Now;
			Rqd.CommandText = Rqd.CommandText + 
			                  "cbloques.gagendas = cdietari.gagendas AND " + 
			                  "cbloques.gbloques = cdietari.gbloques AND " + 
			                  "cdietari.fdietari >= " + Serrores.FormatFecha( tempRefParam5) + " AND " + 
			                  "cdietari.fdietari >= " + Serrores.FormatFecha( tempRefParam6) + " AND " + 
			                  "cdietari.ibloquea = '0' AND ";
			FechaBusqueda = Convert.ToDateTime(tempRefParam5);
			if (TipoPaciente == "N")
			{
				Rqd.CommandText = Rqd.CommandText + 
				                  "((cdietari.npacnorm > 0) OR " + 
				                  "(cdietari.nminnorm >= cprespro.ndurpres)) ";
			}
			else
			{
				Rqd.CommandText = Rqd.CommandText + 
				                  "((cdietari.npacnorm + cdietari.npacpref > 0) OR " + 
				                  "(cdietari.nminnorm + cdietari.nminpref >= cprespro.ndurpres)) ";
			}
			Rqd.CommandText = Rqd.CommandText + 
			                  " and ((cdietari.nminnorm > = " + DuracionPrestacion.ToString() + " or cdietari.nminpref > = " + DuracionPrestacion.ToString() + " and imodoges = 'D') or imodoges = 'C' )";

			//OSCAR C 03/06/2009
			//Se a�ade turno a la condicion de busqueda (si viene)
			if (stTurno != "")
			{
				object tempRefParam7 = DateTime.Now;
				Rqd.CommandText = Rqd.CommandText + 
				                  " AND EXISTS ( SELECT '' FROM CHORASPU WHERE " + 
				                  "     CDIETARI.gagendas = CHORASPU.gagendas AND " + 
				                  "     CDIETARI.gbloques = CHORASPU.gbloques AND " + 
				                  "     CONVERT(Char(10), CDIETARI.fdietari, 101) = CONVERT(Char(10), CHORASPU.fdietari, 101) AND " + 
				                  "     CHORASPU.ibloquea ='0' AND " + 
				                  "     CHORASPU.fdietari > " + Serrores.FormatFechaHM( tempRefParam7);
				if (stTurno == "M")
				{
					Rqd.CommandText = Rqd.CommandText + " AND CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "'  ";
				}
				if (stTurno == "T")
				{
					Rqd.CommandText = Rqd.CommandText + " AND CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "'  ";
				}
				Rqd.CommandText = Rqd.CommandText + " ) ";
			}
			//--------------------------------------------------

			Rqd.CommandText = Rqd.CommandText + 
			                  " ORDER BY cdietari.fdietari ,hinicons ";

			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Rqd);
			rsd = new DataSet();
			tempAdapter_4.Fill(rsd);

			// Si se ha encontrado una fecha, se busca la primera hora con hueco para la prestacion
			bool Encontrado = false;
			bool NoPasaSociedad = false;
			bool bPerimitirExceso = false;
			DataSet rAux = null;
			if (rsd.Tables[0].Rows.Count > 0)
			{
				rsd.MoveFirst();

				while(!(rsd.Tables[0].Rows.Count == 0 || Encontrado))
				{

					rqh = null;
					rqh.Connection = Conexion;

					rqh.CommandText = "SELECT distinct cbloques.imodoges, cprespro.gprestac, cprespro.ndurpres, cprespro.ntobtres, cprespro.nmaxcita, choraspu.*, " + 
					                  "Amodoges, Anexcepac, Anexcemin, gsalacon " + 
					                  "FROM cagendas, cbloques, cprespro, cdietari, choraspu, CPECITPR  " + 
					                  "WHERE ";
					//Ademas se debe tener en cuenta los perfiles de citacion del usuario
					rqh.CommandText = rqh.CommandText + " CPRESPRO.gagendas = CPECITPR.gagendas and " + 
					                  " CPRESPRO.gbloques = CPECITPR.gbloques and " + 
					                  " CPRESPRO.gprestac = CPECITPR.gprestac and " + 
					                  " ( CPECITPR.gpercita =" + PerFilCitacionUsuario.ToString() + " OR " + 
					                  "   CPECITPR.gpercita IN (SELECT gpercita FROM CUSUPCIT where gusuario='" + stUsuario + "') " + 
					                  " ) ";
					//------------
					rqh.CommandText = rqh.CommandText + " and cagendas.gagendas = cbloques.gagendas And ";
					if (Servicio != 0)
					{
						rqh.CommandText = rqh.CommandText + 
						                  "cagendas.gservici = " + Servicio.ToString() + " AND ";
					}
					if (Responsable != 0)
					{
						rqh.CommandText = rqh.CommandText + 
						                  "cagendas.gpersona = " + Responsable.ToString() + " AND ";
					}
					if (SalaConsulta != "")
					{
						rqh.CommandText = rqh.CommandText + 
						                  "cagendas.gsalacon = '" + SalaConsulta + "' AND ";
					}
					if (stAgenda != "")
					{
						rqh.CommandText = rqh.CommandText + 
						                  "cagendas.gagendas = '" + stAgenda + "' AND ";
					}
					rqh.CommandText = rqh.CommandText + 
					                  "(cbloques.icitprop = 'N' OR " + 
					                  " (cbloques.icitprop = 'S' AND cagendas.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
					                  "cbloques.gagendas = cprespro.gagendas AND " + 
					                  "cbloques.gbloques = cprespro.gbloques AND " + 
					                  "cprespro.gprestac = '" + Prestacion + "' AND ";
					if (Sociedad != 0)
					{
						if (Sociedad != codss)
						{
							rqh.CommandText = rqh.CommandText + 
							                  " not exists  (select ' ' from cagesoci where " + 
							                  " cprespro.gagendas=cagesoci.gagendas and " + 
							                  " cprespro.gbloques=cagesoci.gbloques and " + 
							                  " cprespro.gprestac=cagesoci.gprestac and cagesoci.gsocieda=" + Sociedad.ToString() + ") and ";
						}
						else if (Inspeccion.Trim() != "")
						{ 
							rqh.CommandText = rqh.CommandText + 
							                  " not exists (select ' ' from cagesoci where " + 
							                  " cprespro.gagendas=cagesoci.gagendas and " + 
							                  " cprespro.gbloques=cagesoci.gbloques and " + 
							                  " cprespro.gprestac=cagesoci.gprestac and cagesoci.gsocieda = " + Sociedad.ToString() + " and cagesoci.ginspecc='" + Inspeccion + "') and ";
						}
					}
					object tempRefParam8 = rsd.Tables[0].Rows[0]["fdietari"];
					rqh.CommandText = rqh.CommandText + 
					                  "cbloques.gagendas = cdietari.gagendas AND " + 
					                  "cbloques.gbloques = cdietari.gbloques AND " + 
					                  "cdietari.fdietari = " + Serrores.FormatFecha( tempRefParam8) + " AND " + 
					                  "cdietari.ibloquea = '0' AND ";
					if (TipoPaciente == "N")
					{
						rqh.CommandText = rqh.CommandText + 
						                  "((cdietari.npacnorm > 0) OR " + 
						                  "(cdietari.nminnorm >= cprespro.ndurpres)) AND ";
					}
					else
					{
						rqh.CommandText = rqh.CommandText + 
						                  "((cdietari.npacnorm + cdietari.npacpref > 0) OR " + 
						                  "(cdietari.nminnorm + cdietari.nminpref >= cprespro.ndurpres)) AND ";
					}
					object tempRefParam9 = DateTime.Now;
					rqh.CommandText = rqh.CommandText + 
					                  "cdietari.gagendas = choraspu.gagendas AND " + 
					                  "cdietari.gbloques = choraspu.gbloques AND " + 
					                  "convert(char(10), cdietari.fdietari, 101) = convert(char(10), choraspu.fdietari, 101) AND " + 
					                  "choraspu.ibloquea = '0' AND " + 
					                  "choraspu.fdietari > " + Serrores.FormatFechaHM( tempRefParam9) + " AND ";
					if (TipoPaciente == "N")
					{
						rqh.CommandText = rqh.CommandText + 
						                  "((choraspu.npacnorm > 0) OR " + 
						                  "(choraspu.nminnorm > 0)) ";
					}
					else
					{
						rqh.CommandText = rqh.CommandText + 
						                  "((choraspu.npacnorm + choraspu.npacpref > 0) OR " + 
						                  "(choraspu.nminnorm + choraspu.nminpref > 0)) ";
					}
					rqh.CommandText = rqh.CommandText + 
					                  " and cbloques.gbloques = " + Convert.ToString(rsd.Tables[0].Rows[0]["gbloques"]) + " ";
					//aunq encuentre fecha, que se centre en el bloque que hay huecos no en todos los bloques de ese tenga ese dia


					//rsdelacalle . Si existe un bloqueo parcial, en cdietari no se refleja, por eso hay que restar en lo anterior los minutos o los pacientes de los huecos bloqueados.
					rqh.CommandText = rqh.CommandText + 
					                  " AND" + 
					                  " (" + 
					                  "    (cbloques.imodoges='D' and ( cbloques.nexcemin is null OR ( cbloques.nexcemin is not null and  (((cdietari.nminnorm + cdietari.nminpref -(Select isnull(sum(nminpref) + sum(nminnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1))+ cprespro.ndurpres ) <= cbloques.nexcemin)) )" + 
					                  "     OR" + 
					                  "    (cbloques.imodoges='C' and ( cbloques.nexcepac is null OR ( cbloques.nexcepac is not null and  (cdietari.npacnorm + cdietari.npacpref -(Select isnull(sum(npacpref) + sum(npacnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1) < cbloques.nexcepac)) )" + 
					                  " ) ";


					//OSCAR C 03/06/2009
					//Se a�ade turno a la condicion de busqueda (si viene)
					if (stTurno != "")
					{
						if (stTurno == "M")
						{
							rqh.CommandText = rqh.CommandText + " AND CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "'  ";
						}
						if (rbTarde == "T")
						{
							rqh.CommandText = rqh.CommandText + " AND CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "'  ";
						}
					}
					//--------------------------------------------------

					rqh.CommandText = rqh.CommandText + 
					                  " ORDER BY choraspu.fdietari, choraspu.gagendas, choraspu.gbloques";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(rqh);
					rsh = new DataSet();
					tempAdapter_5.Fill(rsh);

					FechaAnt = DateTime.FromOADate(0);
					AgendaAnt = String.Empty;
					BloqueAnt = 0;

					// Si se ha encontrado alguna hora
					if (rsh.Tables[0].Rows.Count > 0)
					{

						rsh.MoveFirst();


						while(!(rsh.Tables[0].Rows.Count == 0 || Encontrado))
						{

							// Si el tipo de paciente es normal y existe un n�mero maximo de
							// citas para la prestacion en el bloque, se comprueba que no
							// se haya mas citas programadas que el maximo
							if (Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("MM/dd/yyyy") != FechaAnt.ToString("MM/dd/yyyy") || Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]) != AgendaAnt || Convert.ToDouble(rsh.Tables[0].Rows[0]["gbloques"]) != BloqueAnt)
							{
								MaximoCitas = false;
								FechaAnt = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]);
								AgendaAnt = Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]);
								BloqueAnt = Convert.ToInt32(rsh.Tables[0].Rows[0]["gbloques"]);
								if (TipoPaciente == "N" && !Convert.IsDBNull(rsh.Tables[0].Rows[0]["nmaxcita"]) && Convert.ToDouble(rsh.Tables[0].Rows[0]["nmaxcita"]) > 0)
								{
									object tempRefParam10 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 00:00";
									object tempRefParam11 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 23:59";
									StSql = "SELECT COUNT(*)as NumeroCitas " + 
									        "FROM ccitprog " + 
									        "WHERE gagendas = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]) + "' AND " + 
									        "gbloques = " + Convert.ToString(rsh.Tables[0].Rows[0]["gbloques"]) + " AND " + 
									        "ffeccita >= " + Serrores.FormatFechaHM( tempRefParam10) + " AND " + 
									        "ffeccita <= " + Serrores.FormatFechaHM( tempRefParam11) + " AND " + 
									        "gprestac = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gprestac"]) + "'";
									SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(StSql, Conexion);
									rsCitas = new DataSet();
									tempAdapter_6.Fill(rsCitas);
									object tempRefParam12 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 00:00";
									object tempRefParam13 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 23:59";
									StSql = "SELECT count(distinct gnumregi) as NumeroCitas " + 
									        "FROM chuecpro " + 
									        "WHERE gagendas = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]) + "' AND " + 
									        "gbloques = " + Convert.ToString(rsh.Tables[0].Rows[0]["gbloques"]) + " AND " + 
									        "fhueccit >= " + Serrores.FormatFechaHM( tempRefParam12) + " AND " + 
									        "fhueccit <= " + Serrores.FormatFechaHM( tempRefParam13) + " AND " + 
									        "gprestac = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gprestac"]) + "'";
									SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(StSql, Conexion);
									RsHuecos = new DataSet();
									tempAdapter_7.Fill(RsHuecos);
									object tempRefParam14 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 00:00";
									object tempRefParam15 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 23:59";
									StSql = "SELECT COUNT(*)as NumeroCitas " + 
									        "FROM csesione, csolcita " + 
									        "WHERE gagendas = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]) + "' AND " + 
									        "gbloques = " + Convert.ToString(rsh.Tables[0].Rows[0]["gbloques"]) + " AND " + 
									        "ffeccita >= " + Serrores.FormatFechaHM( tempRefParam14) + " AND " + 
									        "ffeccita <= " + Serrores.FormatFechaHM( tempRefParam15) + " AND " + 
									        "csesione.ganoregi = csolcita.ganoregi AND " + 
									        "csesione.gnumregi = csolcita.gnumregi AND " + 
									        "gprestac = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gprestac"]) + "'";
									SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(StSql, Conexion);
									rsSesiones = new DataSet();
									tempAdapter_8.Fill(rsSesiones);

									if (Convert.ToDouble(Convert.ToDouble(rsCitas.Tables[0].Rows[0]["NumeroCitas"])) + Convert.ToDouble(RsHuecos.Tables[0].Rows[0]["NumeroCitas"]) + Convert.ToDouble(rsSesiones.Tables[0].Rows[0]["NumeroCitas"]) >= Convert.ToDouble(rsh.Tables[0].Rows[0]["nmaxcita"]))
									{
										MaximoCitas = true;
									}
								}
							}

							if (!MaximoCitas)
							{

								if (Convert.ToString(rsh.Tables[0].Rows[0]["imodoges"]) == "D")
								{
									if ((TipoPaciente == "N" && Convert.ToDouble(rsh.Tables[0].Rows[0]["nminnorm"]) < Convert.ToDouble(rsh.Tables[0].Rows[0]["Ndurpres"])) || (TipoPaciente != "N" && (Convert.ToDouble(rsh.Tables[0].Rows[0]["nminnorm"]) + Convert.ToDouble(rsh.Tables[0].Rows[0]["nminpref"])) < Convert.ToDouble(rsh.Tables[0].Rows[0]["Ndurpres"])) || (TipoPaciente != "N" && (Convert.ToDouble(Convert.ToDouble(rsh.Tables[0].Rows[0]["nminnorm"]) + Convert.ToDouble(rsh.Tables[0].Rows[0]["nminpref"]))) < DuracionPrestacion))
									{

										object tempRefParam16 = rsh.Tables[0].Rows[0]["fdietari"];
										object tempRefParam17 = Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("dd/MM/yyyy") + " 23:59";
										StSql = "SELECT SUM(nminnorm)as MinutosNormales, SUM(nminpref)as MinutosPreferentes " + 
										        "FROM choraspu " + 
										        "WHERE gagendas = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]) + "' AND " + 
										        "gbloques = " + Convert.ToString(rsh.Tables[0].Rows[0]["gbloques"]) + " AND " + 
										        "fdietari >= " + Serrores.FormatFechaHM( tempRefParam16) + " AND " + 
										        "fdietari <= " + Serrores.FormatFechaHM( tempRefParam17) + " AND " + 
										        "ibloquea = '0'";

										SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(StSql, Conexion);
										RsHoras = new DataSet();
										tempAdapter_9.Fill(RsHoras);

										if ((TipoPaciente == "N" && Convert.ToDouble(RsHoras.Tables[0].Rows[0]["MinutosNormales"]) >= Convert.ToDouble(rsh.Tables[0].Rows[0]["Ndurpres"])) || (TipoPaciente == "P" && (Convert.ToDouble(RsHoras.Tables[0].Rows[0]["MinutosNormales"]) + Convert.ToDouble(RsHoras.Tables[0].Rows[0]["MinutosPreferentes"])) >= Convert.ToDouble(rsh.Tables[0].Rows[0]["Ndurpres"])))
										{
											Encontrado = true;
											if ((Convert.ToDouble(Convert.ToDouble(RsHoras.Tables[0].Rows[0]["MinutosNormales"]) + Convert.ToDouble(RsHoras.Tables[0].Rows[0]["MinutosPreferentes"]))) < DuracionPrestacion)
											{
												Encontrado = false;
											}
										}
									}
									else
									{
										Encontrado = true;
									}
								}
								else
								{
									Encontrado = true;
								}

								if (Encontrado)
								{
									//OSCAR C 17/04/2007
									//COMPROBAMOS EL EXCESO POR AGENDA
									bPerimitirExceso = true;
									if (!Convert.IsDBNull(rsh.Tables[0].Rows[0]["Amodoges"]))
									{
										StSql = "SELECT SUM(nminnorm)+SUM(nminpref) duracion, SUM(npacnorm)+ SUM(npacpref) paciente" + 
										        " FROM CHORASPU " + 
										        " WHERE gagendas = '" + Convert.ToString(rsh.Tables[0].Rows[0]["gagendas"]) + "' AND ibloquea = '0' AND " + 
										        " fdietari >= '" + Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("MM/dd/yyyy") + "' AND " + 
										        " fdietari <= '" + Convert.ToDateTime(rsh.Tables[0].Rows[0]["fdietari"]).ToString("MM/dd/yyyy") + " 23:59' ";
										SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(StSql, Conexion);
										rAux = new DataSet();
										tempAdapter_10.Fill(rAux);

										if (Convert.ToString(rsh.Tables[0].Rows[0]["Amodoges"]) == "D")
										{
											if (!Convert.IsDBNull(rsh.Tables[0].Rows[0]["anexcemin"]))
											{
												if ((Convert.ToDouble(rAux.Tables[0].Rows[0]["Duracion"]) * (-1)) + Convert.ToDouble(rsh.Tables[0].Rows[0]["Ndurpres"]) > Convert.ToDouble(rsh.Tables[0].Rows[0]["anexcemin"]))
												{
													bPerimitirExceso = false;
												}
											}
										}
										else
										{
											if (!Convert.IsDBNull(rsh.Tables[0].Rows[0]["anexcepac"]))
											{
												if (Convert.ToDouble(rAux.Tables[0].Rows[0]["Paciente"]) * (-1) >= Convert.ToDouble(rsh.Tables[0].Rows[0]["anexcepac"]))
												{
													bPerimitirExceso = false;
												}
											}
										}
										rAux.Close();
										rAux = null;
									}
									Encontrado = bPerimitirExceso;
									//------
								}

								if (Encontrado)
								{
									if (Convert.IsDBNull(rsh.Tables[0].Rows[0]["ntobtres"]))
									{
										TiempoResultados = 0;
									}
									else
									{
										TiempoResultados = Convert.ToInt32(rsh.Tables[0].Rows[0]["ntobtres"]);
									}
									if (DuracionPrestacion == 0)
									{
										DuracionPrestacion = Convert.ToInt32(rsh.Tables[0].Rows[0]["ndurpres"]);
									}
									//18-12-2006 - Diego - controlo el tipo de hueco que quiero buscar, el normal, o para la fecha propuesta
									if (stTipoHueco.Trim() == "")
									{
										Formulario.RecogerHueco(rsh.Tables[0].Rows[0]["gagendas"], rsh.Tables[0].Rows[0]["gbloques"], rsh.Tables[0].Rows[0]["fdietari"], Convert.ToDouble(rsh.Tables[0].Rows[0]["nordcita"]) + 1, "N", DuracionPrestacion, TiempoResultados, null, null, null, rsh.Tables[0].Rows[0]["gsalacon"]);
									}
									else
									{
										Formulario.RecogerHueco_FechaPrescripcion(rsh.Tables[0].Rows[0]["gagendas"], rsh.Tables[0].Rows[0]["gbloques"], rsh.Tables[0].Rows[0]["fdietari"], Convert.ToDouble(rsh.Tables[0].Rows[0]["nordcita"]) + 1, "N", DuracionPrestacion, TiempoResultados);
									}

								}

							}

							rsh.MoveNext();

						};

						rsh.Close();

					}

					rsd.MoveNext();

				};

			}
			else
			{


			}

			rsd.Close();



			//Oscar C Septiembre 2011. Requerimiento Argentina 3.2.12.
			//Explicaci�n al no encontrar hueco libre en Citaci�n.
			//Los operadores que utilizan la aplicaci�n necesitan conocer el motivo por el cual el sistema de citaci�n no proporciona hueco, con el fin de informar al paciente de forma telef�nica o presencial.
			//Se solicita que cuando el sistema de citaci�n no proporcione hueco para una determinada cita se especifique el motivo por pantalla. Los motivos que deber�an ser identificados son los siguientes:
			//1) Hacemos la busqueda como hasta ahora. Si hay huecos, guay. Si no, pasamos al punto 2.
			//2) Hacemos la select inicial, pero quitando la restriccion del perfil y usando count(*) en lugar de campos. Si encontramos huecos, el problema eran los permisos (Mensaje: �Hay huecos pero el usuario no tiene permiso para asignarlos por perfil de citaci�n� . Si no, al punto 3.
			//3) Hacemos la select inicial, sin la restricion de entidad y con el count(*). Si encontramos algo, era por la entidad (Mensaje: La entidad econ�mica seleccionada no cubre la prestaci�n. Si no, es que no hay huecos (Mensaje: No existen huecos para los criterios introducidos) y punto.

			bool m = false;

			if (!Encontrado)
			{

				//2) Hacemos la select inicial, pero quitando la restriccion del perfil y usando count(*) en lugar de campos.
				//   Si encontramos huecos, el problema eran los permisos (Mensaje: �Hay huecos pero el usuario no tiene permiso para asignarlos por perfil de citaci�n� . Si no, al punto 3.
				if (!m)
				{

					StSql = "SELECT count(cdietari.fdietari)  " + 
					        "FROM cagendas, cbloques, cprespro, cdietari " + 
					        " WHERE " + 
					        " cagendas.gagendas = cbloques.gagendas And ";
					if (Servicio != 0)
					{
						StSql = StSql + "cagendas.gservici = " + Servicio.ToString() + " AND ";
					}
					if (Responsable != 0)
					{
						StSql = StSql + "cagendas.gpersona = " + Responsable.ToString() + " AND ";
					}
					if (SalaConsulta != "")
					{
						StSql = StSql + "cagendas.gsalacon = '" + SalaConsulta + "' AND ";
					}
					if (stAgenda != "")
					{
						StSql = StSql + "cagendas.gagendas = '" + stAgenda + "' AND ";
					}
					StSql = StSql + 
					        " (cbloques.icitprop = 'N' OR " + 
					        " (cbloques.icitprop = 'S' AND cagendas.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
					        " cbloques.gagendas = cprespro.gagendas AND " + 
					        " cbloques.gbloques = cprespro.gbloques AND " + 
					        " cprespro.gprestac = '" + Prestacion + "' AND ";
					if (Sociedad != 0)
					{
						if (Sociedad != codss)
						{
							StSql = StSql + 
							        " not exists (select ' ' from cagesoci where " + 
							        " cprespro.gagendas=cagesoci.gagendas and " + 
							        " cprespro.gbloques=cagesoci.gbloques and " + 
							        " cprespro.gprestac=cagesoci.gprestac and cagesoci.gsocieda=" + Sociedad.ToString() + ") and ";
						}
						else if (Inspeccion.Trim() != "")
						{ 
							StSql = StSql + 
							        " not exists (select ' ' from cagesoci where " + 
							        " cprespro.gagendas=cagesoci.gagendas and " + 
							        " cprespro.gbloques=cagesoci.gbloques and " + 
							        " cprespro.gprestac=cagesoci.gprestac and cagesoci.gsocieda = " + Sociedad.ToString() + " and cagesoci.ginspecc='" + Inspeccion + "') and ";
						}
					}

					object tempRefParam18 = FechaBusqueda;
					object tempRefParam19 = DateTime.Now;
					StSql = StSql + 
					        "cbloques.gagendas = cdietari.gagendas AND " + 
					        "cbloques.gbloques = cdietari.gbloques AND " + 
					        "cdietari.fdietari >= " + Serrores.FormatFecha( tempRefParam18) + " AND " + 
					        "cdietari.fdietari >= " + Serrores.FormatFecha( tempRefParam19) + " AND " + 
					        "cdietari.ibloquea = '0' AND ";
					FechaBusqueda = Convert.ToDateTime(tempRefParam18);

					if (TipoPaciente == "N")
					{
						StSql = StSql + 
						        "((cdietari.npacnorm > 0) OR " + 
						        "(cdietari.nminnorm >= cprespro.ndurpres)) ";
					}
					else
					{
						StSql = StSql + 
						        "((cdietari.npacnorm + cdietari.npacpref > 0) OR " + 
						        "(cdietari.nminnorm + cdietari.nminpref >= cprespro.ndurpres)) ";
					}

					StSql = StSql + 
					        " and ((cdietari.nminnorm > = " + DuracionPrestacion.ToString() + " or cdietari.nminpref > = " + DuracionPrestacion.ToString() + " and imodoges = 'D') or imodoges = 'C' )";

					if (stTurno != "")
					{
						object tempRefParam20 = DateTime.Now;
						StSql = StSql + 
						        " AND EXISTS ( SELECT '' FROM CHORASPU WHERE " + 
						        "     CDIETARI.gagendas = CHORASPU.gagendas AND " + 
						        "     CDIETARI.gbloques = CHORASPU.gbloques AND " + 
						        "     CONVERT(Char(10), CDIETARI.fdietari, 101) = CONVERT(Char(10), CHORASPU.fdietari, 101) AND " + 
						        "     CHORASPU.ibloquea ='0' AND " + 
						        "     CHORASPU.fdietari > " + Serrores.FormatFechaHM( tempRefParam20);
						if (stTurno == "M")
						{
							StSql = StSql + " AND CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "'  ";
						}
						if (stTurno == "T")
						{
							StSql = StSql + " AND CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "'  ";
						}
						StSql = StSql + " ) ";
					}

					SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(StSql, Conexion);
					RsHuecos = new DataSet();
					tempAdapter_11.Fill(RsHuecos);
					if (Convert.ToDouble(RsHuecos.Tables[0].Rows[0][0]) > 0)
					{
						short tempRefParam21 = 4036;
						string[] tempRefParam22 = new string[]{" asignar huecos a ", " por perfil de citaci�n"};
						ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam21,  Conexion,  tempRefParam22);
						m = true;
					}
					RsHuecos.Close();
				}


				//3) Hacemos la select inicial, sin la restricion de entidad y con el count(*). Si encontramos algo, era por la entidad (Mensaje: La entidad econ�mica seleccionada no cubre la prestaci�n. Si no, es que no hay huecos (Mensaje: No existen huecos para los criterios introducidos) y punto.
				if (!m)
				{
                    //If Not IsMissing(Sociedad) And Sociedad <> 0 Then
                    if ((Object)Sociedad != Type.Missing && Sociedad != 0)
					{
						if (Sociedad != codss)
						{
							StSql = "Select dsocieda from dsocieda where gsocieda = " + Sociedad.ToString();
						}
						else if (Inspeccion.Trim() != "")
						{ 
							StSql = "Select dinspecc from dinspecc where ginspecc ='" + Inspeccion + "'";
						}

						SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(StSql, Conexion);
						RsHuecos = new DataSet();
						tempAdapter_12.Fill(RsHuecos);
						if (RsHuecos.Tables[0].Rows.Count == 0)
						{
							stDESSociedad = "";
						}
						else
						{
							stDESSociedad = Convert.ToString(RsHuecos.Tables[0].Rows[0][0]).Trim();
						}
						RsHuecos.Close();
					}

					StSql = "Select dprestac from dcodpres where gprestac = '" + Prestacion + "'";
					SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(StSql, Conexion);
					RsHuecos = new DataSet();
					tempAdapter_13.Fill(RsHuecos);
					if (RsHuecos.Tables[0].Rows.Count == 0)
					{
						stDESPrestacion = "";
					}
					else
					{
						stDESPrestacion = Convert.ToString(RsHuecos.Tables[0].Rows[0][0]).Trim();
					}
					RsHuecos.Close();


					StSql = "SELECT count(cdietari.fdietari) " + 
					        " FROM cagendas, cbloques, cprespro, cdietari, CPECITPR   " + 
					        " WHERE " + 
					        " cagendas.gagendas = cbloques.gagendas And ";

					if (Servicio != 0)
					{
						StSql = StSql + "cagendas.gservici = " + Servicio.ToString() + " AND ";
					}
					if (Responsable != 0)
					{
						StSql = StSql + "cagendas.gpersona = " + Responsable.ToString() + " AND ";
					}
					if (SalaConsulta != "")
					{
						StSql = StSql + "cagendas.gsalacon = '" + SalaConsulta + "' AND ";
					}
					if (stAgenda != "")
					{
						StSql = StSql + "cagendas.gagendas = '" + stAgenda + "' AND ";
					}
					StSql = StSql + 
					        " (cbloques.icitprop = 'N' OR " + 
					        " (cbloques.icitprop = 'S' AND cagendas.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
					        " cbloques.gagendas = cprespro.gagendas AND " + 
					        " cbloques.gbloques = cprespro.gbloques AND " + 
					        " cprespro.gprestac = '" + Prestacion + "' AND ";

					object tempRefParam23 = FechaBusqueda;
					object tempRefParam24 = DateTime.Now;
					StSql = StSql + 
					        "cbloques.gagendas = cdietari.gagendas AND " + 
					        "cbloques.gbloques = cdietari.gbloques AND " + 
					        "cdietari.fdietari >= " + Serrores.FormatFecha( tempRefParam23) + " AND " + 
					        "cdietari.fdietari >= " + Serrores.FormatFecha( tempRefParam24) + " AND " + 
					        "cdietari.ibloquea = '0' AND ";
					FechaBusqueda = Convert.ToDateTime(tempRefParam23);

					StSql = StSql + " CPRESPRO.gagendas = CPECITPR.gagendas and " + 
					        " CPRESPRO.gbloques = CPECITPR.gbloques and " + 
					        " CPRESPRO.gprestac = CPECITPR.gprestac and " + 
					        " ( CPECITPR.gpercita =" + PerFilCitacionUsuario.ToString() + " OR " + 
					        "   CPECITPR.gpercita IN (SELECT gpercita FROM CUSUPCIT where gusuario='" + stUsuario + "') " + 
					        " ) AND ";

					if (TipoPaciente == "N")
					{
						StSql = StSql + 
						        "((cdietari.npacnorm > 0) OR " + 
						        "(cdietari.nminnorm >= cprespro.ndurpres)) ";
					}
					else
					{
						StSql = StSql + 
						        "((cdietari.npacnorm + cdietari.npacpref > 0) OR " + 
						        "(cdietari.nminnorm + cdietari.nminpref >= cprespro.ndurpres)) ";
					}

					StSql = StSql + 
					        " and ((cdietari.nminnorm > = " + DuracionPrestacion.ToString() + " or cdietari.nminpref > = " + DuracionPrestacion.ToString() + " and imodoges = 'D') or imodoges = 'C' )";

					if (stTurno != "")
					{
						object tempRefParam25 = DateTime.Now;
						StSql = StSql + 
						        " AND EXISTS ( SELECT '' FROM CHORASPU WHERE " + 
						        "     CDIETARI.gagendas = CHORASPU.gagendas AND " + 
						        "     CDIETARI.gbloques = CHORASPU.gbloques AND " + 
						        "     CONVERT(Char(10), CDIETARI.fdietari, 101) = CONVERT(Char(10), CHORASPU.fdietari, 101) AND " + 
						        "     CHORASPU.ibloquea ='0' AND " + 
						        "     CHORASPU.fdietari > " + Serrores.FormatFechaHM( tempRefParam25);
						if (stTurno == "M")
						{
							StSql = StSql + " AND CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "'  ";
						}
						if (stTurno == "T")
						{
							StSql = StSql + " AND CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "'  ";
						}
						StSql = StSql + " ) ";
					}

					SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(StSql, Conexion);
					RsHuecos = new DataSet();
					tempAdapter_14.Fill(RsHuecos);
					if (Convert.ToDouble(RsHuecos.Tables[0].Rows[0][0]) > 0)
					{
						if (Inspeccion.Trim() != "")
						{
							short tempRefParam26 = 3810;
							string[] tempRefParam27 = new string[]{stDESPrestacion, " inspecci�n " + stDESSociedad};
							ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam26,  Conexion,  tempRefParam27);
						}
						else if (Sociedad > 0)
						{ 
							short tempRefParam28 = 3810;
							string[] tempRefParam29 = new string[]{stDESPrestacion, " sociedad " + stDESSociedad};
							ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam28,  Conexion,  tempRefParam29);
						}
						else
						{
							short tempRefParam30 = 3810;
                            string[] tempRefParam31 = new string[] { stDESPrestacion, " sociedad/inspecci�n del paciente" };
							ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam30,  Conexion,  tempRefParam31);
						}
						m = true;
					}
					RsHuecos.Close();

				}


				//3. No existen huecos para los criterios introducidos.
				if (!m)
				{
					short tempRefParam32 = 1520;
                    string[] tempRefParam33 = new string[] { "fechas disponibles", "la cita solicitada" };
					ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam32,  Conexion,  tempRefParam33);
					m = true;
				}

			}
			//-----------------------------


			if (!Encontrado && !NoPasaSociedad && !m)
			{
				short tempRefParam34 = 1520;
                string[] tempRefParam35 = new string[] { "fechas disponibles", "la cita solicitada" };
				ClaseMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam34,  Conexion,  tempRefParam35);
			}


			if (!Encontrado)
			{
				//18-12-2006 - Diego - controlo el tipo de hueco que quiero buscar, el normal, o para la fecha propuesta
				if (stTipoHueco.Trim() == "")
				{
					Formulario.RecogerHueco(null, null, null, null, null, null, null, null);
				}
				else
				{
					Formulario.RecogerHueco_FechaPrescripcion(null, null, null, null, null, null, null);
				}
			}
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion,  int DuracionPrestacion, string stAgenda, string stTipoHueco, string stUsuario, string stTurno)
		{
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, Inspeccion,  DuracionPrestacion, stAgenda, stTipoHueco, stUsuario, stTurno, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion,  int DuracionPrestacion, string stAgenda, string stTipoHueco, string stUsuario)
		{
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, Inspeccion,  DuracionPrestacion, stAgenda, stTipoHueco, stUsuario, String.Empty, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion,  int DuracionPrestacion, string stAgenda, string stTipoHueco)
		{
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, Inspeccion,  DuracionPrestacion, stAgenda, stTipoHueco, String.Empty, String.Empty, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion,  int DuracionPrestacion, string stAgenda)
		{
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, Inspeccion,  DuracionPrestacion, stAgenda, "", String.Empty, String.Empty, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion,  int DuracionPrestacion)
		{
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, Inspeccion,  DuracionPrestacion, "", "", String.Empty, String.Empty, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad, string Inspeccion)
		{
			int tempRefParam36 = 0;
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, Inspeccion,  tempRefParam36, "", "", String.Empty, String.Empty, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta, int Sociedad)
		{
			int tempRefParam37 = 0;
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, Sociedad, String.Empty,  tempRefParam37, "", "", String.Empty, String.Empty, String.Empty);
		}

		public void Primer_Hueco(object Formulario,  SqlConnection Conexion, int ServicioUsuario, string Prestacion,  System.DateTime FechaBusqueda, string TipoPaciente, int Servicio, int Responsable, string SalaConsulta)
		{
			int tempRefParam38 = 0;
			Primer_Hueco(Formulario,  Conexion, ServicioUsuario, Prestacion,  FechaBusqueda, TipoPaciente, Servicio, Responsable, SalaConsulta, 0, String.Empty,  tempRefParam38, "", "", String.Empty, String.Empty, String.Empty);
		}
	}
}