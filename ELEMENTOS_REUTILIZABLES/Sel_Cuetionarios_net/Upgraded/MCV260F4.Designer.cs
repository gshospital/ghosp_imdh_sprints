using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Cuetionariosdll
{
	partial class Sel_Cuetionarios
	{

		#region "Upgrade Support "
		private static Sel_Cuetionarios m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Sel_Cuetionarios DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Sel_Cuetionarios();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCancelar", "cbImprimir", "_chbCuestionario_11", "_chbCuestionario_10", "_chbCuestionario_9", "_chbCuestionario_8", "_chbCuestionario_7", "_chbCuestionario_6", "_chbCuestionario_5", "_chbCuestionario_4", "_chbCuestionario_3", "_chbCuestionario_2", "_chbCuestionario_1", "_chbCuestionario_0", "frmCuestionarios", "chbCuestionario"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_11;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_10;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_9;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_8;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_7;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_6;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_5;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_4;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_3;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_2;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_1;
		private Telerik.WinControls.UI.RadCheckBox _chbCuestionario_0;
		public Telerik.WinControls.UI.RadGroupBox frmCuestionarios;
		public Telerik.WinControls.UI.RadCheckBox[] chbCuestionario = new Telerik.WinControls.UI.RadCheckBox[12];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sel_Cuetionarios));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbImprimir = new Telerik.WinControls.UI.RadButton();
			this.frmCuestionarios = new Telerik.WinControls.UI.RadGroupBox();
			this._chbCuestionario_11 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_10 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_9 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_8 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_7 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_6 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_5 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_4 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_3 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_2 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_1 = new Telerik.WinControls.UI.RadCheckBox();
			this._chbCuestionario_0 = new Telerik.WinControls.UI.RadCheckBox();
			//this.frmCuestionarios.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbCancelar
			// 			
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;		
			this.cbCancelar.Location = new System.Drawing.Point(264, 272);
			this.cbCancelar.Name = "cbCancelar";		
			this.cbCancelar.Size = new System.Drawing.Size(81, 32);
			this.cbCancelar.TabIndex = 14;
			this.cbCancelar.Text = "&Cancelar";			
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbImprimir
			// 			
			this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbImprimir.Enabled = false;
			//this.cbImprimir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbImprimir.Location = new System.Drawing.Point(172, 272);
			this.cbImprimir.Name = "cbImprimir";
			this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbImprimir.Size = new System.Drawing.Size(81, 32);
			this.cbImprimir.TabIndex = 13;
			this.cbImprimir.Text = "&Imprimir";			
			this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
			// 
			// frmCuestionarios
			// 			
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_11);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_10);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_9);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_8);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_7);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_6);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_5);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_4);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_3);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_2);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_1);
			this.frmCuestionarios.Controls.Add(this._chbCuestionario_0);
			this.frmCuestionarios.Enabled = true;
			this.frmCuestionarios.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frmCuestionarios.Location = new System.Drawing.Point(6, 8);
			this.frmCuestionarios.Name = "frmCuestionarios";
			this.frmCuestionarios.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmCuestionarios.Size = new System.Drawing.Size(339, 257);
			this.frmCuestionarios.TabIndex = 0;
			this.frmCuestionarios.Text = "Cuestionarios";
			this.frmCuestionarios.Visible = true;
			// 
			// _chbCuestionario_11
			// 			
			this._chbCuestionario_11.CausesValidation = true;			
			this._chbCuestionario_11.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_11.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_11.Enabled = true;
			this._chbCuestionario_11.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_11.Location = new System.Drawing.Point(10, 234);
			this._chbCuestionario_11.Name = "_chbCuestionario_11";
			this._chbCuestionario_11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_11.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_11.TabIndex = 12;
			this._chbCuestionario_11.TabStop = true;
			this._chbCuestionario_11.Text = "Indice de Katz";
			this._chbCuestionario_11.Visible = true;
			this._chbCuestionario_11.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_10
			// 			
			this._chbCuestionario_10.CausesValidation = true;			
			this._chbCuestionario_10.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_10.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_10.Enabled = true;
			this._chbCuestionario_10.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_10.Location = new System.Drawing.Point(10, 215);
			this._chbCuestionario_10.Name = "_chbCuestionario_10";
			this._chbCuestionario_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_10.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_10.TabIndex = 11;
			this._chbCuestionario_10.TabStop = true;
			this._chbCuestionario_10.Text = "Escala de puntuaci�n para depresi�n de Montgomery Asber";
			this._chbCuestionario_10.Visible = true;
			this._chbCuestionario_10.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_9
			// 			
			this._chbCuestionario_9.CausesValidation = true;			
			this._chbCuestionario_9.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_9.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_9.Enabled = true;
			this._chbCuestionario_9.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_9.Location = new System.Drawing.Point(10, 196);
			this._chbCuestionario_9.Name = "_chbCuestionario_9";
			this._chbCuestionario_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_9.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_9.TabIndex = 10;
			this._chbCuestionario_9.TabStop = true;
			this._chbCuestionario_9.Text = "Escala de Yahr (Enf. de Parkinson)";
			this._chbCuestionario_9.Visible = true;
			this._chbCuestionario_9.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_8
			// 			
			this._chbCuestionario_8.CausesValidation = true;			
			this._chbCuestionario_8.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_8.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_8.Enabled = true;			
			this._chbCuestionario_8.Location = new System.Drawing.Point(10, 177);
			this._chbCuestionario_8.Name = "_chbCuestionario_8";
			this._chbCuestionario_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_8.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_8.TabIndex = 9;
			this._chbCuestionario_8.TabStop = true;
			this._chbCuestionario_8.Text = "Escala de Hachinski";
			this._chbCuestionario_8.Visible = true;
			this._chbCuestionario_8.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_7
			//			
			this._chbCuestionario_7.CausesValidation = true;			
			this._chbCuestionario_7.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_7.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_7.Enabled = true;
			this._chbCuestionario_7.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_7.Location = new System.Drawing.Point(10, 157);
			this._chbCuestionario_7.Name = "_chbCuestionario_7";
			this._chbCuestionario_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_7.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_7.TabIndex = 8;
			this._chbCuestionario_7.TabStop = true;
			this._chbCuestionario_7.Text = "Escala de depresi�n geri�trica de Yesavage (Ver. reducida)";
			this._chbCuestionario_7.Visible = true;
			this._chbCuestionario_7.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_6
			//			
			this._chbCuestionario_6.CausesValidation = true;			
			this._chbCuestionario_6.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_6.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_6.Enabled = true;
			//this._chbCuestionario_6.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_6.Location = new System.Drawing.Point(10, 138);
			this._chbCuestionario_6.Name = "_chbCuestionario_6";
			this._chbCuestionario_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_6.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_6.TabIndex = 7;
			this._chbCuestionario_6.TabStop = true;
			this._chbCuestionario_6.Text = "Escala de depresi�n geri�trica";
			this._chbCuestionario_6.Visible = true;
			this._chbCuestionario_6.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_5
			// 			
			this._chbCuestionario_5.CausesValidation = true;			
			this._chbCuestionario_5.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_5.Enabled = true;
			this._chbCuestionario_5.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_5.Location = new System.Drawing.Point(10, 119);
			this._chbCuestionario_5.Name = "_chbCuestionario_5";
			this._chbCuestionario_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_5.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_5.TabIndex = 6;
			this._chbCuestionario_5.TabStop = true;
			this._chbCuestionario_5.Text = "Escala de demencia de Blessed";
			this._chbCuestionario_5.Visible = true;
			this._chbCuestionario_5.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_4
			//			
		
			this._chbCuestionario_4.CausesValidation = true;			
			this._chbCuestionario_4.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_4.Enabled = true;
			this._chbCuestionario_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_4.Location = new System.Drawing.Point(10, 100);
			this._chbCuestionario_4.Name = "_chbCuestionario_4";
			this._chbCuestionario_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_4.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_4.TabIndex = 5;
			this._chbCuestionario_4.TabStop = true;
			this._chbCuestionario_4.Text = "Miniexamen cognoscitivo de Lobo";
			this._chbCuestionario_4.Visible = true;
			this._chbCuestionario_4.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_3
			// 	
			this._chbCuestionario_3.CausesValidation = true;			
			this._chbCuestionario_3.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_3.Enabled = true;
			this._chbCuestionario_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_3.Location = new System.Drawing.Point(10, 80);
			this._chbCuestionario_3.Name = "_chbCuestionario_3";
			this._chbCuestionario_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_3.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_3.TabIndex = 4;
			this._chbCuestionario_3.TabStop = true;
			this._chbCuestionario_3.Text = "�ndice de Barthel";
			this._chbCuestionario_3.Visible = true;
			this._chbCuestionario_3.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_2
			// 			
			this._chbCuestionario_2.CausesValidation = true;			
			this._chbCuestionario_2.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_2.Enabled = true;
			this._chbCuestionario_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_2.Location = new System.Drawing.Point(10, 61);
			this._chbCuestionario_2.Name = "_chbCuestionario_2";
			this._chbCuestionario_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_2.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_2.TabIndex = 3;
			this._chbCuestionario_2.TabStop = true;
			this._chbCuestionario_2.Text = "Escala de Pfeiffer";
			this._chbCuestionario_2.Visible = true;
			this._chbCuestionario_2.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_1
			//				
			this._chbCuestionario_1.CausesValidation = true;			
			this._chbCuestionario_1.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_1.Enabled = true;			
			this._chbCuestionario_1.Location = new System.Drawing.Point(10, 42);
			this._chbCuestionario_1.Name = "_chbCuestionario_1";
			this._chbCuestionario_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_1.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_1.TabIndex = 2;
			this._chbCuestionario_1.TabStop = true;
			this._chbCuestionario_1.Text = "Escalas de incapacidad f�sica y mental de la Cruz Roja";
			this._chbCuestionario_1.Visible = true;
			this._chbCuestionario_1.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// _chbCuestionario_0
			// 				
			this._chbCuestionario_0.CausesValidation = true;			
			this._chbCuestionario_0.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbCuestionario_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbCuestionario_0.Enabled = true;
			this._chbCuestionario_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbCuestionario_0.Location = new System.Drawing.Point(10, 22);
			this._chbCuestionario_0.Name = "_chbCuestionario_0";
			this._chbCuestionario_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbCuestionario_0.Size = new System.Drawing.Size(325, 19);
			this._chbCuestionario_0.TabIndex = 1;
			this._chbCuestionario_0.TabStop = true;
			this._chbCuestionario_0.Text = "Evaluaci�n estado nutricional";
			this._chbCuestionario_0.Visible = true;
			this._chbCuestionario_0.CheckStateChanged += new System.EventHandler(this.chbCuestionario_CheckStateChanged);
			// 
			// Sel_Cuetionarios
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;			
			this.ClientSize = new System.Drawing.Size(352, 311);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbImprimir);
			this.Controls.Add(this.frmCuestionarios);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 23);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Sel_Cuetionarios";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Cuestionarios Valoraciones M�dicas - MCV260F4";
			this.Closed += new System.EventHandler(this.Sel_Cuetionarios_Closed);
			this.Load += new System.EventHandler(this.Sel_Cuetionarios_Load);
			this.frmCuestionarios.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializechbCuestionario();
		}
		void InitializechbCuestionario()
		{
			this.chbCuestionario = new Telerik.WinControls.UI.RadCheckBox[12];
			this.chbCuestionario[11] = _chbCuestionario_11;
			this.chbCuestionario[10] = _chbCuestionario_10;
			this.chbCuestionario[9] = _chbCuestionario_9;
			this.chbCuestionario[8] = _chbCuestionario_8;
			this.chbCuestionario[7] = _chbCuestionario_7;
			this.chbCuestionario[6] = _chbCuestionario_6;
			this.chbCuestionario[5] = _chbCuestionario_5;
			this.chbCuestionario[4] = _chbCuestionario_4;
			this.chbCuestionario[3] = _chbCuestionario_3;
			this.chbCuestionario[2] = _chbCuestionario_2;
			this.chbCuestionario[1] = _chbCuestionario_1;
			this.chbCuestionario[0] = _chbCuestionario_0;
		}
		#endregion
	}
}