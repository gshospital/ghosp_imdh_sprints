using Microsoft.VisualBasic;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.Data;


namespace Cuetionariosdll
{
	public partial class Sel_Cuetionarios
		: Telerik.WinControls.UI.RadForm
	{

		public Sel_Cuetionarios()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			int I = 0;
			try
			{
				this.Cursor = Cursors.WaitCursor;
				for (; I <= 11; I++)
				{
					if (chbCuestionario[I].CheckState == CheckState.Checked)
					{
                        //Camaya todo_x_5: Crystal report	
                        //MSel_Pacientes.ObjListado.windowtitle = chbCuestionario[I].Text;
                        switch (I)
						{
							case 0 :	
                                //Camaya todo_x_5: Crystal report							
								//MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R3.rpt"; 
								proImprimir_Informe(); 
								break;
							case 1 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R4.rpt"; 
                                proImprimir_Informe(); 
								break;
							case 2 :
                                //Camaya todo_x_5: Crystal report                                
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R5.rpt"; 
								proImprimir_Informe(); 
								break;
							case 3 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R6.rpt"; 
								proImprimir_Informe(); 
								break;
							case 4 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R7.rpt"; 
								proImprimir_Informe(); 
								break;
							case 5 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R8.rpt"; 
								proImprimir_Informe(); 
								break;
							case 6 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R9.rpt"; 
								proImprimir_Informe(); 
								break;
							case 7 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R10.rpt"; 
								proImprimir_Informe(); 
								break;
							case 8 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R11.rpt"; 
								proImprimir_Informe(); 
								break;
							case 9 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R12.rpt"; 
								proImprimir_Informe(); 
								break;
							case 10 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R13.rpt"; 
								proImprimir_Informe(); 
								break;
							case 11 :
                                //Camaya todo_x_5: Crystal report
                                //MSel_Pacientes.ObjListado.ReportFileName = MSel_Pacientes.PathInformes + "\\DCV260R14.rpt"; 
								proImprimir_Informe(); 
								break;
						}
					}
				}
				for (; I <= 11; I++)
				{
					chbCuestionario[I].CheckState = CheckState.Unchecked;
				}
				
				this.Cursor = Cursors.Default;
			}
			catch (Exception e)
			{
				//***************************************************
				this.Cursor = Cursors.Default;
				//cancela la impresi�n
				
				if (Information.Err().Number == 32755)
				{
					for (I=0; I <= 11; I++)
					{
						chbCuestionario[I].CheckState = CheckState.Unchecked;
					}
				}
				else
				{
					Serrores.AnalizaError("EConsValMedica", MSel_Pacientes.PathAplicacion, MSel_Pacientes.GUsuario, "Cuestionarios Valoraciones M�dicas:cbImprimir_Click", e);
				}
			}
		}

		private void chbCuestionario_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar_aceptar();
		}

		private void proActivar_aceptar()
		{
			cbImprimir.Enabled = false;
			for (int I = 0; I <= 11; I++)
			{
				if (this.chbCuestionario[I].CheckState == CheckState.Checked)
				{
					cbImprimir.Enabled = true;
				}
			}
		}
		private void proImprimir_Informe()
		{
            //camaya todo_x_5
            //MSel_Pacientes.ObjListado.Destination = 0; //1 'crptToPrinter			
            //MSel_Pacientes.ObjListado.WindowState = (int) FormWindowState.Maximized;			
            //MSel_Pacientes.ObjListado.Connect = "DSN=" + MSel_Pacientes.GstDSN_GHOSP + ";UID=" + MSel_Pacientes.GstUsuario_Crystal + ";PWD=" + MSel_Pacientes.GstContrase�a_Crystal + "";
            //MSel_Pacientes.ObjListado.SQLQuery = "select * from destaciv where 1 = 2";			
            //MSel_Pacientes.ObjListado.Action = 1;			
            //MSel_Pacientes.ObjListado.PageCount();				
            //MSel_Pacientes.ObjListado.PrinterStopPage = MSel_Pacientes.ObjListado.PageCount;			
            //MSel_Pacientes.ObjListado.Reset();
        }

		
		private void Sel_Cuetionarios_Load(Object eventSender, EventArgs eventArgs)
		{
           CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
        }
		private void Sel_Cuetionarios_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}