using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BloqueoSistema
{
	partial class frmTimer
	{

		#region "Upgrade Support "
		private static frmTimer m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmTimer DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmTimer();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "timBloqueo"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain; // System.Windows.Forms.ToolTip
		public System.Windows.Forms.Timer timBloqueo;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTimer));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.timBloqueo = new System.Windows.Forms.Timer(components);
			this.SuspendLayout();
			// 
			// timBloqueo
			// 
			this.timBloqueo.Enabled = false;
			this.timBloqueo.Interval = 1;
			this.timBloqueo.Tick += new System.EventHandler(this.timBloqueo_Tick);
			// 
			// frmTimer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(33, 34);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Location = new System.Drawing.Point(0, 0);
			this.MaximizeBox = true;
			this.MinimizeBox = true;
			this.Name = "frmTimer";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ShowInTaskbar = false;
			this.Text = "Form1";
			this.Closed += new System.EventHandler(this.frmTimer_Closed);
			this.ResumeLayout(false);
		}
		#endregion
	}
}