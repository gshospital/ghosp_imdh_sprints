using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BloqueoSistema
{
	partial class frmBloqueo
	{

		#region "Upgrade Support "
		private static frmBloqueo m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmBloqueo DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmBloqueo();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbSalir", "Mensaje", "lbUsuario"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbSalir;
		public Telerik.WinControls.UI.RadLabel Mensaje;
		public Telerik.WinControls.UI.RadLabel lbUsuario;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbSalir = new Telerik.WinControls.UI.RadButton();
            this.Mensaje = new Telerik.WinControls.UI.RadLabel();
            this.lbUsuario = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mensaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbSalir
            // 
            this.cbSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbSalir.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbSalir.Location = new System.Drawing.Point(256, 160);
            this.cbSalir.Name = "cbSalir";
            this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbSalir.Size = new System.Drawing.Size(83, 35);
            this.cbSalir.TabIndex = 0;
            this.cbSalir.Text = "Desbloquear";
            this.cbSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
            // 
            // Mensaje
            // 
            this.Mensaje.AutoSize = false;
            this.Mensaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.Mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mensaje.Location = new System.Drawing.Point(8, 8);
            this.Mensaje.Name = "Mensaje";
            this.Mensaje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Mensaje.Size = new System.Drawing.Size(585, 105);
            this.Mensaje.TabIndex = 2;
            this.Mensaje.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbUsuario
            // 
            this.lbUsuario.AutoSize = false;
            this.lbUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsuario.Location = new System.Drawing.Point(272, 128);
            this.lbUsuario.Name = "lbUsuario";
            this.lbUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUsuario.Size = new System.Drawing.Size(47, 19);
            this.lbUsuario.TabIndex = 1;
            // 
            // frmBloqueo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 213);
            this.Controls.Add(this.cbSalir);
            this.Controls.Add(this.Mensaje);
            this.Controls.Add(this.lbUsuario);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBloqueo";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closed += new System.EventHandler(this.frmBloqueo_Closed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBloqueo_FormClosing);
            this.Load += new System.EventHandler(this.frmBloqueo_Load);
            this.Leave += new System.EventHandler(this.frmBloqueo_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.cbSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mensaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}