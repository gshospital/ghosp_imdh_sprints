using System;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BloqueoSistema
{
	public partial class frmTimer
        : Telerik.WinControls.UI.RadForm
	{

		public frmTimer()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void timBloqueo_Tick(Object eventSender, EventArgs eventArgs)
		{
			int state = 0;
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI tmpPos = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
            
            int ret = 0;
			//the counter uses by the For loop

			int p = 0;

			try
			{

				basBloqueo.IdleTimeValue++;

				for (int i = 2; i <= 256; i++)
				{
					//call the API

                    p = UpgradeSupportHelper.PInvoke.SafeNative.user32.GetAsyncKeyState(i);

					if (p != 0 && p != -32768)
					{
						basBloqueo.IdleTimeValue = 0;
					}

					Application.DoEvents();
				}

				//get the position of the mouse cursor
                ret = UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref tmpPos);

				//if the coordinates of the mouse are different than
				//last time or when the idle started, then the system
				//is not idling:
				if (tmpPos.X != basBloqueo.MousePos.X || tmpPos.Y != basBloqueo.MousePos.Y)
				{
					//store the current coordinates so that we
					//can compare next time round
					basBloqueo.MousePos.X = tmpPos.X;
					basBloqueo.MousePos.Y = tmpPos.Y;

					//Reset Idle Time Value
					basBloqueo.IdleTimeValue = 0;
				}

				if (basBloqueo.IdleTimeValue == 0)
				{
					//Set Last Idle Date
                    basBloqueo.LastIdleTime = DateTime.Parse(StringsHelper.Format(DateTime.Now, "dd/MM/yyyy HH:mm:ss"));
				}

				Application.DoEvents();

				if (basBloqueo.IdleTimeValue >= basBloqueo.TiempoBloqueo)
				{
					frmBloqueo.DefInstance.ShowDialog();
				}
			}
			catch
			{

				basBloqueo.IdleTimeValue = 0;
                basBloqueo.LastIdleTime = DateTime.Parse(StringsHelper.Format(DateTime.Now, "dd/MM/yyyy HH:mm:ss"));
			}

		}
		private void frmTimer_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}