using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;
using System.Dynamic;
namespace BloqueoSistema
{
	public partial class frmBloqueo
        : Telerik.WinControls.UI.RadForm, inf_Bloqueo
	{

		ConfirmacionTomas.clsConfirmacionTomas objValidacion = null;
         
		public frmBloqueo()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


        private void frmBloqueo_Load(Object eventSender, EventArgs eventArgs)
		{
            
			bool fError = false;
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
			this.Height = (int) Screen.PrimaryScreen.Bounds.Width;

            basBloqueo.HookKeyB(VB6.GetHInstance().ToInt32());
            
            cbSalir.Top = (int) (this.Height / 2);
            cbSalir.Left = (int) (this.Width / 2 - cbSalir.Width / 2);

            lbUsuario.BackColor = this.BackColor;
            lbUsuario.Text = "Equipo bloqueado por " + basBloqueo.NombrePaciente;
            lbUsuario.Width = (int) (Strings.Len(lbUsuario.Text) * 150 / 15);
            lbUsuario.Left = (int) (this.Width / 2 - lbUsuario.Width / 2);
            lbUsuario.Top = (int) (this.Height / 2 - 33);

            Mensaje.Width = (int) (this.Width - 7);
            Mensaje.Height = (int) ((this.Height / 2) - 20);
            Mensaje.Left = (int) (this.Width / 2 - Mensaje.Width / 2);
            Mensaje.Top = 7;

            UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), basBloqueo.HWND_TOPMOST, 0, 0, 0, 0, basBloqueo.wFlags);

            basBloqueo.PonerClave = false;

            Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
            string tempRefParam = "P";
            string tempRefParam2 = "";
            string tempRefParam3 = "";
            string tempRefParam4 = "";
            string tempRefParam5 = "0";
            string tempRefParam6 = "0";
            string tempRefParam7 = "PRINCIPAL";
            instancia.Conexion(ref basBloqueo.cnConexion, tempRefParam, tempRefParam2, ref fError, ref tempRefParam3, ref tempRefParam4, basBloqueo.BstServerActual, basBloqueo.BstDriver, basBloqueo.BstBasedatosActual, basBloqueo.BstUsuarioBD, basBloqueo.BstPasswordBD, tempRefParam5, tempRefParam6, tempRefParam7, basBloqueo.BstUsuarioApli);
            instancia = null;

            MostrarMensaje();
            basBloqueo.cnConexion.Close();
            //frmOrigen.WindowState = 1

        }
        
		private void frmBloqueo_Leave(Object eventSender, EventArgs eventArgs)
		{
			this.Activate();
            UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), basBloqueo.HWND_TOPMOST, 0, 0, 0, 0, basBloqueo.wFlags);
		}

		private void frmBloqueo_FormClosing(Object eventSender, FormClosingEventArgs eventArgs)
		{
			int Cancel = (eventArgs.Cancel) ? 1 : 0;
			int UnloadMode = (int) eventArgs.CloseReason;

			basBloqueo.UnHookKeyB();
            UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(VB6.GetHInstance().ToInt32(), basBloqueo.HWND_NOTOPMOST, 0, 0, 0, 0, basBloqueo.wFlags);
			//frmOrigen.WindowState = 2

			eventArgs.Cancel = Cancel != 0;
            this.Dispose();
		}

		public void ObtenerAcceso(string valor)
		{
			basBloqueo.UsuarioConfirmacion = valor.Trim();
		}
       
		private void proCerrarTodo()
		{

            // Registramos el cierre de la sesi�n

            proRegistraCierre(basBloqueo.stUsuario, basBloqueo.UsuarioConfirmacion);

            // Cerramos todos los m�dulos activos

            // Cerramos los m�dulos que se abrieron desde el m�dulo principal

            basBloqueo.proCerrarModulosAbiertos();

            // Cerramos los m�dulos indicados en la tabla SSIAEXES

            mbProcesos.proCerrarModulos(basBloqueo.cnConexion);

            // Esperamos un poquito para que se terminen de cerrar los procesos

            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(1000);

            // Reiniciamos la conexi�n con la validaci�n del otro usuario

            basBloqueo.frmOrigen.proReiniciaSesion(basBloqueo.UsuarioConfirmacion, fOBtenerUsuarioLargo(basBloqueo.UsuarioConfirmacion));
        
        }
       
		private string fOBtenerUsuarioLargo(string usuario)
		{
			string result = String.Empty;
			string sql = "SELECT gusularg from SUSUARIO where gusuario='" + usuario + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, basBloqueo.cnConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			result = (Convert.ToString(RR.Tables[0].Rows[0]["gusularg"]) + "").Trim();
			RR.Close();
			return result;
		}

		private void proRegistraCierre(string usuarioAntiguo, string usuarioNuevo)
		{


			string stSql = "insert into SUSUKILL (gusuario, gusudead, facceso) values (" + 
			               "'" + usuarioNuevo.Trim() + "', " + 
			               "'" + usuarioAntiguo.Trim() + "', " + 
			               "getDate())";

			SqlCommand tempCommand = new SqlCommand(stSql, basBloqueo.cnConexion);
			tempCommand.ExecuteNonQuery();

		}



		private void MostrarMensaje()
		{


			string stSql = " select dmensaje from smensaje where " + 
			               " gmensaje>=(select nnumeri1 from sconsglo where gconsglo='TEXTBLOQ') and " + 
			               " gmensaje<=(select nnumeri2 from sconsglo where gconsglo='TEXTBLOQ') ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basBloqueo.cnConexion);
			DataSet Rrmensaje = new DataSet();
			tempAdapter.Fill(Rrmensaje);


			Mensaje.Text = "";
			
            foreach (DataRow iteration_row in Rrmensaje.Tables[0].Rows)
			{
				Mensaje.Text = Mensaje.Text + Environment.NewLine + Convert.ToString(iteration_row["dmensaje"]);
			}

		}
		private void frmBloqueo_Closed(Object eventSender, EventArgs eventArgs)
		{
		}

        private void cbSalir_Click(object sender, EventArgs e)
        {
            bool fError = false;
            // Ahora ya no tiene que estar en el frente

            UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), basBloqueo.HWND_NOTOPMOST, 0, 0, 0, 0, basBloqueo.wFlags);

            // Llamamos a la validaci�n de usuario

            basBloqueo.PonerClave = true;

            basBloqueo.UsuarioConfirmacion = "";
            Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
            string tempRefParam = "P";
            string tempRefParam2 = "";
            string tempRefParam3 = "";
            string tempRefParam4 = "";
            string tempRefParam5 = "0";
            string tempRefParam6 = "0";
            string tempRefParam7 = "PRINCIPAL";
            
            instancia.Conexion(ref basBloqueo.cnConexion, tempRefParam, tempRefParam2, ref fError, ref tempRefParam3, ref tempRefParam4, basBloqueo.BstServerActual, basBloqueo.BstDriver, basBloqueo.BstBasedatosActual, basBloqueo.BstUsuarioBD, basBloqueo.BstPasswordBD, tempRefParam5, tempRefParam6, tempRefParam7, basBloqueo.BstUsuarioApli);
            instancia = null;
            
            objValidacion = new ConfirmacionTomas.clsConfirmacionTomas();

            dynamic tempRefParam8 = this;
            string tempRefParam9 = Path.GetDirectoryName(Application.ExecutablePath);
            string tempRefParam10 = "VALIDACION";
            string tempRefParam11 = "0";
            string tempRefParam12 = "0";
            string tempRefParam13 = "0";
            string tempRefParam14 = "0";
            string tempRefParam15 = "0";
            string tempRefParam16 = "0";
            bool tempRefParam17 = false;
            bool tempRefParam18 = true;
            objValidacion.Llamada(tempRefParam8, basBloqueo.cnConexion, basBloqueo.stUsuario, tempRefParam9, tempRefParam10, tempRefParam11, tempRefParam12, tempRefParam13, tempRefParam14, tempRefParam15, tempRefParam16, tempRefParam17, tempRefParam18);

            objValidacion = null;

            basBloqueo.cnConexion.Close();
            basBloqueo.PonerClave = false;

            // Si no hay ning�n usuario, es que no se valid�. Volvemos

            if (basBloqueo.UsuarioConfirmacion == "")
            {
                return;
            }

            object iBoton = null;
            if (basBloqueo.UsuarioConfirmacion.Trim().ToUpper() == basBloqueo.stUsuario.Trim().ToUpper())
            {

                // Si es el mismo usuario, salimos

                this.Close();

            }
            else
            {

                // Si es un usuario distinto, mostramos un mensaje avisando de que se cerrar�n
                // todas las tareas.


                basBloqueo.PonerClave = true;
                //        UnHookKeyB

                // Se cambia el mensaje a petici�n de la FJD porque se considera demasiado alarmante...
                //        iBoton = clase_mensaje.RespuestaMensaje("BloqueoSistema", "", 1125, cnConexion, "Si se identifica como un usuario distinto al actual, se perder�n todos los cambios realizados", "continuar")
                instancia = new Conexion.Obtener_conexion();
                string tempRefParam19 = "P";
                string tempRefParam20 = "";
                string tempRefParam21 = "";
                string tempRefParam22 = "";
                string tempRefParam23 = "0";
                string tempRefParam24 = "0";
                string tempRefParam25 = "PRINCIPAL";
                instancia.Conexion(ref basBloqueo.cnConexion, tempRefParam19, tempRefParam20, ref fError, ref tempRefParam21, ref tempRefParam22, basBloqueo.BstServerActual, basBloqueo.BstDriver, basBloqueo.BstBasedatosActual, basBloqueo.BstUsuarioBD, basBloqueo.BstPasswordBD, tempRefParam23, tempRefParam24, tempRefParam25, basBloqueo.BstUsuarioApli);
                instancia = null;
                string tempRefParam26 = "BloqueoSistema";
                string tempRefParam27 = "";
                short tempRefParam28 = 1125;
                string[] tempRefParam29 = { "Si se identifica como un usuario distinto al actual, se perder�n todos los cambios no guardados", "continuar" };

                iBoton = basBloqueo.clase_mensaje.RespuestaMensaje(tempRefParam26, tempRefParam27, tempRefParam28, basBloqueo.cnConexion, tempRefParam29);

                basBloqueo.PonerClave = false;

                //        HookKeyB App.hInstance

                if (((DialogResult)Convert.ToInt32(iBoton)) == System.Windows.Forms.DialogResult.Yes)
                {
                    this.Close();
                    proCerrarTodo();

                    //''         daba un error cuando se bloqueaba y cambia el usuario conectado
                    //''            cnConexion.Close
                }
                else
                {
                    basBloqueo.cnConexion.Close();
                    UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), basBloqueo.HWND_TOPMOST, 0, 0, 0, 0, basBloqueo.wFlags);
                    return;
                }

            }

        }
	}
}