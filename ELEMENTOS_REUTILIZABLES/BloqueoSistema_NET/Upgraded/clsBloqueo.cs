using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BloqueoSistema
{
	public class clsBloqueo
	{


		public System.DateTime GetLastTime()
		{

			return basBloqueo.LastIdleTime;

		}

		public int GetIdleTime()
		{

			return Convert.ToInt32(basBloqueo.IdleTimeValue);

		}

		~clsBloqueo()
		{

			frmTimer.DefInstance.timBloqueo.Enabled = false;

			ResetValues();

			frmTimer.DefInstance = null;

			// Eliminamos la clase mensaje

			basBloqueo.clase_mensaje = null;
			//  cnConexion.Close
			basBloqueo.cnConexion = null;
		}

		public clsBloqueo()
		{

			ResetValues();

			frmTimer.DefInstance.timBloqueo.Enabled = true;

			// Creamos la clase de los mensajes

			basBloqueo.clase_mensaje = new Mensajes.ClassMensajes();

		}

		private void ResetValues()
		{


			frmTimer.DefInstance.timBloqueo.Interval = 1000;
			frmTimer.DefInstance.timBloqueo.Enabled = true;
            //basBloqueo.LastIdleTime = DateTime.Parse(StringsHelper.Format(DateTime.Now, "dd/mm/yyyy hh:mm:ss AMPM"));
            basBloqueo.LastIdleTime = DateTime.Parse(StringsHelper.Format(DateTime.Now, "dd/MM/yyyy HH:mm:ss"));
			basBloqueo.IdleTimeValue = 0;

		}

		public void CambioUsuario(string usuario, dynamic objFrm)
		{

			bool fError = false;


			Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
			string tempRefParam = "P";
			string tempRefParam2 = "";
			string tempRefParam3 = "";
			string tempRefParam4 = "";
			string tempRefParam5 = "0";
			string tempRefParam6 = "0";
			string tempRefParam7 = "PRINCIPAL";
			instancia.Conexion(ref basBloqueo.cnConexion, tempRefParam, tempRefParam2, ref fError, ref tempRefParam3, ref tempRefParam4, basBloqueo.BstServerActual, basBloqueo.BstDriver, basBloqueo.BstBasedatosActual, basBloqueo.BstUsuarioBD, basBloqueo.BstPasswordBD, tempRefParam5, tempRefParam6, tempRefParam7, basBloqueo.BstUsuarioApli);
			instancia = null;


			if (usuario.Trim() == basBloqueo.stUsuario.Trim())
			{
				return;
			}
            
            
			basBloqueo.stUsuario = usuario;
			basBloqueo.frmOrigen = objFrm;
        
			// Obtenemos el nombre del usuario que bloquea la aplicaci�n

			TomarNombre();

            basBloqueo.cnConexion.Close();

		}

		//Public Sub Iniciar(objConexion As rdoConnection, objOrigen As Object, usuarioOrigen As String)
		public void Iniciar(dynamic objOrigen, ref string usuarioOrigen, ref string stServer, ref string stDriver, ref string stBasedatos, ref string stUsuarioBD, ref string stPasswordBD)
		{




			//Set cnConexion = objConexion


			bool fError = false;

			basBloqueo.BstDriver = stDriver;
			basBloqueo.BstBasedatosActual = stBasedatos;
			basBloqueo.BstServerActual = stServer;
			basBloqueo.BstUsuarioBD = stUsuarioBD;
			basBloqueo.BstPasswordBD = stPasswordBD;
			basBloqueo.BstUsuarioApli = usuarioOrigen;
			Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
			string tempRefParam = "P";
			string tempRefParam2 = "";
			string tempRefParam3 = "";
			string tempRefParam4 = "";
			string tempRefParam5 = "0";
			string tempRefParam6 = "0";
			string tempRefParam7 = "PRINCIPAL";
			instancia.Conexion(ref basBloqueo.cnConexion, tempRefParam, tempRefParam2, ref fError, ref tempRefParam3, ref tempRefParam4, stServer, stDriver, stBasedatos, stUsuarioBD, stPasswordBD, tempRefParam5, tempRefParam6, tempRefParam7, usuarioOrigen);
			instancia = null;
           
			basBloqueo.frmOrigen = objOrigen;
			basBloqueo.stUsuario = usuarioOrigen;
            
			// Obtenemos el nombre del usuario que bloquea la aplicaci�n

			TomarNombre();

			// Tomamos el tiempo m�ximo

			TomarTiempoMaximo();

            basBloqueo.cnConexion.Close();

		}

		private string TomarNombre()
		{


			string stSql = "select rTrim(isNull(dap1pers, '')) + ' ' + rTrim(isNull(dap2pers, '')) + " + 
			               "', ' + rTrim(isNull(dnompers, '')) as Nombre from SUSUARIO , DPERSONA where " + 
			               "SUSUARIO.gusuario = '" + basBloqueo.stUsuario.Trim() + "' and " + 
			               "DPERSONA.gpersona = SUSUARIO.gpersona";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basBloqueo.cnConexion);
			DataSet RrNombre = new DataSet();
			tempAdapter.Fill(RrNombre);

			if (RrNombre.Tables[0].Rows.Count == 0)
			{
				basBloqueo.NombrePaciente = "";
			}
			else
			{
				basBloqueo.NombrePaciente = Convert.ToString(RrNombre.Tables[0].Rows[0]["Nombre"]);
			}

			RrNombre.Close();


			return String.Empty;
		}

		private void TomarTiempoMaximo()
		{


			string stSql = "select isNull(nnumeri1,120) tiempo from SCONSGLO where " + 
			               "gconsglo = 'IBLOQUEO'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basBloqueo.cnConexion);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count == 0)
			{
				basBloqueo.TiempoBloqueo = 120;
			}
            else
            {

            }
			{
				basBloqueo.TiempoBloqueo = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["tiempo"]);
				if (basBloqueo.TiempoBloqueo < 3)
				{
					basBloqueo.TiempoBloqueo = 120;
				}
			}

            RrDatos.Close();
		}
	}
}