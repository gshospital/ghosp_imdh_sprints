using System;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using UpgradeHelpers.Helpers;


namespace BloqueoSistema
{
	internal static class basBloqueo
	{


		// Variables globales

		public static SqlConnection cnConexion = null;
		public static string stUsuario = String.Empty;
		public static string NombrePaciente = String.Empty;
        public static dynamic frmOrigen = null;
		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static string UsuarioConfirmacion = String.Empty;

		public static bool PonerClave = false;

		public static int TiempoBloqueo = 0;

		// Tipos para las API

        private struct tagKBDLLHOOKSTRUCT
		{
			public int vkCode;
			public int scanCode;
			public int flags;
			public int time;
			public int dwExtraInfo;
		}

        public static UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI MousePos = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI(); // Posici�n del rat�n
		public static double IdleTimeValue = 0; // N�mero de segundos sin hacer nada
		public static System.DateTime LastIdleTime = DateTime.FromOADate(0); // Fecha del �ltimo momento para no hacer nada

		private static int mHook = 0; // Gancho creado con SetWindowsHookEx

		private const int WH_KEYBOARD_LL = 13; // Tipo de gancho queremos instalar (teclado)

		//Private Const WH_MOUSE_LL       As Long = 14&     ' Tipo de gancho queremos instalar (rat�n)

		private const int VK_0 = 0x30; // Tecla 0 (cero)
		private const int VK_9 = 0x39; // Tecla 9
		private const int VK_NUMPAD0 = 0x60; // Tecla 0 (Teclado num�rico)
		private const int VK_NUMPAD9 = 0x69; // Tecla 9 (Teclado num�rico)
		private const int VK_A = 0x41; // Tecla A
		private const int VK_Z = 0x5A; // Tecla Z
		private const int VK_TAB = 0x9; // Tecla Tabulador
		private const int VK_CONTROL = 0x11; // Tecla Ctrl
		private const int VK_MENU = 0x12; // Tecla Alt
		private const int VK_ESCAPE = 0x1B; // Tecla Esc
		private const int VK_DELETE = 0x2E; // Tecla Supr
		private const int VK_SPACE = 0x20; // Tecla Space
		private const int VK_BACK_SPACE = 0x8; // Tecla backSpace
		private const int VK_ENTER = 0xD; // Tecla Enter
		private const int VK_DOT = 0xBE; // Tecla Punto
		private const int VK_NUMPADDOT = 0x6E; // Tecla Punto (Teclado num�rico)
		private const int VK_E�E = 0xC0; // Tecla E�e
		private const int VK_GUION = 0xBD; // Tecla Gui�n
		private const int VK_SHIFT1 = 161; // Techa Shift derecho
		private const int VK_SHIFT2 = 160; // Techa Shift izquierdo
		private const int VK_CAPSLOCK = 20; // Tecla Caps Lock

		private const int LLKHF_ALTDOWN = 0x20; // Alt pulsado

		// c�digos para los ganchos (la acci�n a tomar en el gancho del teclado)

		private const int HC_ACTION = 0;

		private const int SWP_NOSIZE = 0x1;
		private const int SWP_NOMOVE = 0x2;
		private const int SWP_NOACTIVATE = 0x10;

		public static readonly int wFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE;

		//   Valores de hwndInsertAfter

		public const int HWND_TOPMOST = -1;
		public const int HWND_NOTOPMOST = -2;

		public static string BstDriver = String.Empty;
		public static string BstBasedatosActual = String.Empty;
		public static string BstServerActual = String.Empty;
		public static string BstUsuarioBD = String.Empty;
		public static string BstPasswordBD = String.Empty;
		public static string BstUsuarioApli = String.Empty;


		// La funci�n a usar para el gancho del teclado
       
		internal static int LLKeyBoardProc(int nCode, int wParam, int lparam)
		{

			basBloqueo.tagKBDLLHOOKSTRUCT pkbhs = new basBloqueo.tagKBDLLHOOKSTRUCT();

			int ret = 0;

			// copiar el par�metro en la estructura

			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<basBloqueo.tagKBDLLHOOKSTRUCT, int>(ref pkbhs, lparam, Marshal.SizeOf(pkbhs));
			//
			if (nCode == HC_ACTION)
			{

				if (PonerClave)
				{
					if ((pkbhs.vkCode >= VK_0 && pkbhs.vkCode <= VK_9) || (pkbhs.vkCode >= VK_NUMPAD0 && pkbhs.vkCode <= VK_NUMPAD9) || (pkbhs.vkCode >= VK_A && pkbhs.vkCode <= VK_Z) || pkbhs.vkCode == VK_DELETE || pkbhs.vkCode == VK_SPACE || pkbhs.vkCode == VK_BACK_SPACE || pkbhs.vkCode == VK_ENTER || pkbhs.vkCode == VK_DOT || pkbhs.vkCode == VK_NUMPADDOT || pkbhs.vkCode == VK_E�E || pkbhs.vkCode == VK_GUION || pkbhs.vkCode == VK_SHIFT1 || pkbhs.vkCode == VK_SHIFT2 || pkbhs.vkCode == VK_CAPSLOCK)
					{
						ret = 0;
					}
					else if (pkbhs.vkCode == VK_TAB)
					{ 
						if ((pkbhs.flags & LLKHF_ALTDOWN) != 0)
						{
							ret = 1;
						}
						else
						{
							ret = 0;
						}
					}
					else
					{
						//                If pkbhs.vkCode <> 160 Then MsgBox pkbhs.vkCode
						ret = 1;
					}

					//            ' Si se pulsa Ctrl+Esc
					//
					//            If pkbhs.vkCode = VK_ESCAPE Then
					//                If (GetAsyncKeyState(VK_CONTROL) And &H8000) Then
					//                    ret = 1
					//                End If
					//            End If
					//
					//            ' Si se pulsa Alt+Tab
					//
					//            If pkbhs.vkCode = VK_TAB Then
					//                If (pkbhs.flags And LLKHF_ALTDOWN) <> 0 Then
					//                    ret = 1
					//                End If
					//            End If
					//
					//            ' Si se pulsa Alt+Esc
					//
					//            If pkbhs.vkCode = VK_ESCAPE Then
					//                If (pkbhs.flags And LLKHF_ALTDOWN) <> 0 Then
					//                    ret = 1
					//                End If
					//            End If

				}
				else
				{

					// No dejamos pulsar ninguna tecla

					ret = 1;

				}

			}

			if (ret == 0)
			{
                ret = UpgradeSupportHelper.PInvoke.SafeNative.user32.CallNextHookEx(mHook, nCode, wParam, lparam);
			}

			return ret;

		}
        
		internal static void HookKeyB(int hMod)
		{

			// Instalar el gancho para el teclado.
			// hMod ser� el valor de App.hInstance de la aplicaci�n

			//UPGRADE_WARNING: (1048) Add a delegate for AddressOf LLKeyBoardProc More Information: http://www.vbtonet.com/ewis/ewi1048.aspx
            //SDSALAZAR_NOTODO_X_1
            
            //mHook = UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowsHookEx(WH_KEYBOARD_LL, LLKeyBoardProc(), hMod, 0);

		}
       
		internal static void UnHookKeyB()
		{

			// Desinstalar el gancho para el teclado
			// Es importante hacerlo antes de finalizar la aplicaci�n,
			// normalmente en el evento Unload o QueryUnload

			if (mHook != 0)
			{
                UpgradeSupportHelper.PInvoke.SafeNative.user32.UnhookWindowsHookEx(mHook);
			}

		}
       
                public static void proCerrarModulosAbiertos()
                {
                    
                    int[] lProcesos = (int[])ArraysHelper.DeepCopy(frmOrigen.proProcesosActivos());
                    int l = 0;
                    if (lProcesos.GetUpperBound(0) > 0)
                    {

                        // Hay procesos abiertos. Los cerramos


                        foreach (int lProcesos_item in lProcesos)
                        {
                            l = lProcesos_item;
                            if (l > 0)
                            {
                                ElementosCompartidos.mbProcesos.proCerrarModuloPorPID(l);
                            }
                        }

                    }
                  
                }
    }
 
}