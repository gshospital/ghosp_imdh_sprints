using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class libro_regi
	{

		#region "Upgrade Support "
		private static libro_regi m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static libro_regi DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new libro_regi();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "optSalida", "optEntrada", "Frame2", "CbPantalla", "CbCerrar", "CbImprimir", "SdcFechaIni", "SdcFechaFin", "Label2", "Label1", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadRadioButton optSalida;
		public Telerik.WinControls.UI.RadRadioButton optEntrada;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaFin;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.optSalida = new Telerik.WinControls.UI.RadRadioButton();
            this.optEntrada = new Telerik.WinControls.UI.RadRadioButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optEntrada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.optSalida);
            this.Frame2.Controls.Add(this.optEntrada);
            this.Frame2.HeaderText = "Seleccione el criterio de ordenaci�n";
            this.Frame2.Location = new System.Drawing.Point(8, 100);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(280, 50);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Seleccione el criterio de ordenaci�n";
            // 
            // optSalida
            // 
            this.optSalida.Cursor = System.Windows.Forms.Cursors.Default;
            this.optSalida.Location = new System.Drawing.Point(160, 24);
            this.optSalida.Name = "optSalida";
            this.optSalida.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optSalida.Size = new System.Drawing.Size(81, 18);
            this.optSalida.TabIndex = 10;
            this.optSalida.Text = "N� de salida";
            // 
            // optEntrada
            // 
            this.optEntrada.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optEntrada.Cursor = System.Windows.Forms.Cursors.Default;
            this.optEntrada.ForeColor = System.Drawing.SystemColors.ControlText;
            this.optEntrada.Location = new System.Drawing.Point(12, 24);
            this.optEntrada.Name = "optEntrada";
            this.optEntrada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optEntrada.Size = new System.Drawing.Size(91, 18);
            this.optEntrada.TabIndex = 9;
            this.optEntrada.Text = "N� de entrada";
            this.optEntrada.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbPantalla.Location = new System.Drawing.Point(119, 160);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(81, 29);
            this.CbPantalla.TabIndex = 5;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbCerrar.Location = new System.Drawing.Point(207, 160);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 4;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbImprimir.Location = new System.Drawing.Point(31, 160);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 3;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.SdcFechaIni);
            this.Frame1.Controls.Add(this.SdcFechaFin);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.HeaderText = "Seleccione intervalo de fechas de ingreso";
            this.Frame1.Location = new System.Drawing.Point(8, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(280, 80);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Seleccione intervalo de fechas de ingreso";
            // 
            // SdcFechaIni
            // 
            this.SdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaIni.Location = new System.Drawing.Point(8, 48);
            this.SdcFechaIni.Name = "SdcFechaIni";
            this.SdcFechaIni.Size = new System.Drawing.Size(105, 20);
            this.SdcFechaIni.TabIndex = 6;
            this.SdcFechaIni.TabStop = false;
            this.SdcFechaIni.Text = "25/04/2016";
            this.SdcFechaIni.Value = new System.DateTime(2016, 4, 25, 16, 17, 58, 917);
            // 
            // SdcFechaFin
            // 
            this.SdcFechaFin.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaFin.Location = new System.Drawing.Point(160, 48);
            this.SdcFechaFin.Name = "SdcFechaFin";
            this.SdcFechaFin.Size = new System.Drawing.Size(105, 20);
            this.SdcFechaFin.TabIndex = 7;
            this.SdcFechaFin.TabStop = false;
            this.SdcFechaFin.Text = "25/04/2016";
            this.SdcFechaFin.Value = new System.DateTime(2016, 4, 25, 16, 17, 58, 927);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(160, 24);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(53, 18);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Fecha fin:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(67, 18);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Fecha inicio:";
            // 
            // libro_regi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(292, 196);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.CbPantalla);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(153, 178);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "libro_regi";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Libro de registro - AGI410F1";
            this.Closed += new System.EventHandler(this.libro_regi_Closed);
            this.Load += new System.EventHandler(this.libro_regi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optEntrada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}