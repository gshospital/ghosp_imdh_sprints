using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class EPISODIOS_PACIENTE
	{

		#region "Upgrade Support "
		private static EPISODIOS_PACIENTE m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static EPISODIOS_PACIENTE DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new EPISODIOS_PACIENTE();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprEntidades", "FrSociedades", "cbSalir", "cbCancelar", "cbAceptar", "CbCambio", "Label8", "Label7", "tbInspeccDe", "tbInspeccA", "tbNafiliaDe", "tbNafiliaA", "tbEntidadA", "tbEntidadde", "Label5", "Label6", "Frame6", "SprEpisodios", "Frame1", "LbPaciente", "Label28", "SprEpisodios_Sheet1", "SprEntidades_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprEntidades;
		public Telerik.WinControls.UI.RadGroupBox FrSociedades;
		public Telerik.WinControls.UI.RadButton cbSalir;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton CbCambio;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadTextBox tbInspeccDe;
		public Telerik.WinControls.UI.RadTextBox tbInspeccA;
		public Telerik.WinControls.UI.RadTextBox tbNafiliaDe;
		public Telerik.WinControls.UI.RadTextBox tbNafiliaA;
		public Telerik.WinControls.UI.RadTextBox tbEntidadA;
		public Telerik.WinControls.UI.RadTextBox tbEntidadde;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadGroupBox Frame6;
		public UpgradeHelpers.Spread.FpSpread SprEpisodios;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadLabel Label28;

		//private FarPoint.Win.Spread.SheetView SprEpisodios_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprEntidades_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EPISODIOS_PACIENTE));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.FrSociedades = new Telerik.WinControls.UI.RadGroupBox();
			this.SprEntidades = new UpgradeHelpers.Spread.FpSpread();
			this.cbSalir = new Telerik.WinControls.UI.RadButton();
			this.Frame6 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.CbCambio = new Telerik.WinControls.UI.RadButton();
			this.Label8 = new Telerik.WinControls.UI.RadLabel();
			this.Label7 = new Telerik.WinControls.UI.RadLabel();
			this.tbInspeccDe = new Telerik.WinControls.UI.RadTextBox();
			this.tbInspeccA = new Telerik.WinControls.UI.RadTextBox();
			this.tbNafiliaDe = new Telerik.WinControls.UI.RadTextBox();
			this.tbNafiliaA = new Telerik.WinControls.UI.RadTextBox();
			this.tbEntidadA = new Telerik.WinControls.UI.RadTextBox();
			this.tbEntidadde = new Telerik.WinControls.UI.RadTextBox();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.SprEpisodios = new UpgradeHelpers.Spread.FpSpread();
			this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.FrSociedades.SuspendLayout();
			this.Frame6.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// FrSociedades
			// 
			this.FrSociedades.Controls.Add(this.SprEntidades);
			this.FrSociedades.Enabled = true;
			this.FrSociedades.Location = new System.Drawing.Point(6, 163);
			this.FrSociedades.Name = "FrSociedades";
			this.FrSociedades.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrSociedades.Size = new System.Drawing.Size(870, 141);
			this.FrSociedades.TabIndex = 15;
			this.FrSociedades.Visible = true;
			// 
			// SprEntidades
			// 
			this.SprEntidades.Location = new System.Drawing.Point(8, 17);
			this.SprEntidades.Name = "SprEntidades";
			this.SprEntidades.Size = new System.Drawing.Size(853, 115);
			this.SprEntidades.TabIndex = 16;
			this.SprEntidades.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEntidades_CellClick);
			// 
			// cbSalir
			// 
			this.cbSalir.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbSalir.Location = new System.Drawing.Point(780, 478);
			this.cbSalir.Name = "cbSalir";
			this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbSalir.Size = new System.Drawing.Size(81, 29);
			this.cbSalir.TabIndex = 11;
			this.cbSalir.Text = "&Salir";
			this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
			// 
			// Frame6
			// 
			this.Frame6.Controls.Add(this.cbCancelar);
			this.Frame6.Controls.Add(this.cbAceptar);
			this.Frame6.Controls.Add(this.CbCambio);
			this.Frame6.Controls.Add(this.Label8);
			this.Frame6.Controls.Add(this.Label7);
			this.Frame6.Controls.Add(this.tbInspeccDe);
			this.Frame6.Controls.Add(this.tbInspeccA);
			this.Frame6.Controls.Add(this.tbNafiliaDe);
			this.Frame6.Controls.Add(this.tbNafiliaA);
			this.Frame6.Controls.Add(this.tbEntidadA);
			this.Frame6.Controls.Add(this.tbEntidadde);
			this.Frame6.Controls.Add(this.Label5);
			this.Frame6.Controls.Add(this.Label6);
			this.Frame6.Enabled = true;
			this.Frame6.Location = new System.Drawing.Point(6, 308);
			this.Frame6.Name = "Frame6";
			this.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame6.Size = new System.Drawing.Size(870, 164);
			this.Frame6.TabIndex = 4;
			this.Frame6.Text = "Cambio de Entidad / Inspección";
			this.Frame6.Visible = true;
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Enabled = false;
			this.cbCancelar.Location = new System.Drawing.Point(768, 124);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(79, 29);
			this.cbCancelar.TabIndex = 14;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			this.cbAceptar.Location = new System.Drawing.Point(684, 124);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(79, 29);
			this.cbAceptar.TabIndex = 13;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// CbCambio
			// 
			this.CbCambio.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCambio.Enabled = false;
			this.CbCambio.Location = new System.Drawing.Point(600, 124);
			this.CbCambio.Name = "CbCambio";
			this.CbCambio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCambio.Size = new System.Drawing.Size(79, 29);
			this.CbCambio.TabIndex = 5;
			this.CbCambio.Text = "Ca&mbio";
			this.CbCambio.Click += new System.EventHandler(this.CbCambio_Click);
			// 
			// Label8
			// 
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label8.Location = new System.Drawing.Point(16, 96);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(77, 17);
			this.Label8.TabIndex = 20;
			this.Label8.Text = "Inspección:";
			// 
			// Label7
			// 
			this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label7.Location = new System.Drawing.Point(16, 44);
			this.Label7.Name = "Label7";
			this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label7.Size = new System.Drawing.Size(77, 17);
			this.Label7.TabIndex = 19;
			this.Label7.Text = "Inspección:";
			// 
			// tbInspeccDe
			// 
			this.tbInspeccDe.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbInspeccDe.Location = new System.Drawing.Point(96, 41);
			this.tbInspeccDe.Name = "tbInspeccDe";
			this.tbInspeccDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbInspeccDe.Size = new System.Drawing.Size(550, 17);
			this.tbInspeccDe.TabIndex = 18;
            this.tbInspeccDe.Enabled = false;
            // 
            // tbInspeccA
            // 
			this.tbInspeccA.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbInspeccA.Location = new System.Drawing.Point(96, 95);
			this.tbInspeccA.Name = "tbInspeccA";
			this.tbInspeccA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbInspeccA.Size = new System.Drawing.Size(550, 17);
			this.tbInspeccA.TabIndex = 17;
            this.tbInspeccA.Enabled = false;
            // 
            // tbNafiliaDe
            // 
			this.tbNafiliaDe.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbNafiliaDe.Location = new System.Drawing.Point(652, 18);
			this.tbNafiliaDe.Name = "tbNafiliaDe";
			this.tbNafiliaDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbNafiliaDe.Size = new System.Drawing.Size(196, 17);
			this.tbNafiliaDe.TabIndex = 12;
            this.tbNafiliaDe.Enabled = false;
            // 
            // tbNafiliaA
            // 
            this.tbNafiliaA.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbNafiliaA.Location = new System.Drawing.Point(652, 73);
			this.tbNafiliaA.Name = "tbNafiliaA";
			this.tbNafiliaA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbNafiliaA.Size = new System.Drawing.Size(196, 17);
			this.tbNafiliaA.TabIndex = 10;
            this.tbNafiliaA.Enabled = false;
            // 
            // tbEntidadA
            // 
			this.tbEntidadA.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbEntidadA.Location = new System.Drawing.Point(96, 73);
			this.tbEntidadA.Name = "tbEntidadA";
			this.tbEntidadA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbEntidadA.Size = new System.Drawing.Size(550, 17);
			this.tbEntidadA.TabIndex = 9;
            this.tbEntidadA.Enabled = false;
            // 
            // tbEntidadde
            // 
			this.tbEntidadde.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbEntidadde.Location = new System.Drawing.Point(96, 18);
			this.tbEntidadde.Name = "tbEntidadde";
			this.tbEntidadde.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbEntidadde.Size = new System.Drawing.Size(550, 17);
			this.tbEntidadde.TabIndex = 8;
            this.tbEntidadde.Enabled = false;
            // 
            // Label5
            // 
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.Location = new System.Drawing.Point(16, 22);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(59, 17);
			this.Label5.TabIndex = 7;
			this.Label5.Text = "De Entidad:";
			// 
			// Label6
			// 
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.Location = new System.Drawing.Point(16, 74);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(59, 17);
			this.Label6.TabIndex = 6;
			this.Label6.Text = "A Entidad:";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.SprEpisodios);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(6, 22);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(870, 141);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Episodios";
			this.Frame1.Visible = true;
			// 
			// SprEpisodios
			// 
			this.SprEpisodios.Location = new System.Drawing.Point(8, 17);
			this.SprEpisodios.Name = "SprEpisodios";
			this.SprEpisodios.Size = new System.Drawing.Size(853, 115);
			this.SprEpisodios.TabIndex = 3;
            this.SprEpisodios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEpisodios_CellClick);
			// 
			// LbPaciente
			// 
			this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbPaciente.Location = new System.Drawing.Point(77, 5);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(510, 17);
			this.LbPaciente.TabIndex = 1;
            this.LbPaciente.Enabled = false;
            // 
            // Label28
            // 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(8, 4);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(55, 17);
			this.Label28.TabIndex = 0;
			this.Label28.Text = "Paciente:";
			// 
			// EPISODIOS_PACIENTE
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(887, 513);
			this.Controls.Add(this.FrSociedades);
			this.Controls.Add(this.cbSalir);
			this.Controls.Add(this.Frame6);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.LbPaciente);
			this.Controls.Add(this.Label28);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EPISODIOS_PACIENTE";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Cambio de entidad de pacientes dados de alta";
			this.Activated += new System.EventHandler(this.EPISODIOS_PACIENTE_Activated);
			this.Closed += new System.EventHandler(this.EPISODIOS_PACIENTE_Closed);
			this.Load += new System.EventHandler(this.EPISODIOS_PACIENTE_Load);
			this.FrSociedades.ResumeLayout(false);
			this.Frame6.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}