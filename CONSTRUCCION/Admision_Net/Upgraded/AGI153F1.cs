using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using System.Transactions;

namespace ADMISION
{
	public partial class Cambio_producto
        : Telerik.WinControls.UI.RadForm
	{


		string VstGidenprod = String.Empty;
		int IGanoprod = 0;
		int IGnumprod = 0;
		bool BUltimo = false;
		int IProducto = 0;
		bool VstSeleccionado = false;
		int IUltimoSeleccionado = 0;
		public Cambio_producto()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public void proActivaAceptar()
		{
			if (CbbServicioA.SelectedIndex != -1)
			{ //And CbbServicioA.Text <> "" Then
				CbAceptar.Enabled = true;
			}
			else
			{
				CbAceptar.Enabled = false;
			}
			if (!BUltimo)
			{
				CbAceptar.Enabled = false;
			}
		}

		public void proActivaEliminar()
		{
			CbEliminar.Enabled = BUltimo && SprProductos.MaxRows > 1 && VstSeleccionado;
			if (!BUltimo)
			{
				SdcFecha.Enabled = false;
				tbHoraCambio.Enabled = false;
				CbbServicioA.Enabled = false;
			}
			else
			{
				if (SprProductos.MaxRows == 1 && VstSeleccionado)
				{
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					CbbServicioA.Enabled = true;
				}
				else
				{
					SdcFecha.Enabled = true;
					tbHoraCambio.Enabled = true;
					CbbServicioA.Enabled = true;
				}
			}
		}

		public void proActualizaFila()
		{
			int iColumna = 0;
			int iFila = 0;
			try
			{
				iColumna = SprProductos.Col;
				iFila = SprProductos.Row;
				SprProductos.Col = 3;
				IProducto = Convert.ToInt32(Double.Parse(SprProductos.Text));
				SprProductos.Col = 2;
				TbServicioDe.Text = SprProductos.Text.Trim();
				if (BUltimo && !VstSeleccionado)
				{
					proRellenaFecha(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
				}
				else
				{
					SprProductos.Col = 4;
					proRellenaFecha(DateTime.Parse(SprProductos.Text).ToString("dd/MM/yyyy HH:mm:ss"));
				}
				SprProductos.Col = iColumna;
				SprProductos.Row = iFila;
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_producto:proActualizafila", ex);
			}
		}

		public void proCabecerasGrid()
		{
			SprProductos.MaxCols = 4;
			SprProductos.MaxRows = 0;
			SprProductos.Row = 0;
			int iColumna = 0;
			iColumna++;
			SprProductos.Col = iColumna;
			SprProductos.Text = "Fecha";
			SprProductos.SetColWidth(iColumna, 1700);
			iColumna++;
			SprProductos.Col = iColumna;
			SprProductos.Text = "Producto";
			SprProductos.SetColWidth(iColumna, 5905);
			//oculta el codigo del producto
			iColumna++;
			SprProductos.Col = iColumna;
			SprProductos.SetColHidden(SprProductos.Col, true);
			//oculta la fecha del movimiento completa
			iColumna++;
			SprProductos.Col = iColumna;
			SprProductos.SetColHidden(SprProductos.Col, true);
		}

		public void proRellenacombo()
		{
			string sqltemp = String.Empty;
			DataSet rrtemp = null;
			try
			{

				sqltemp = "select * from dproductva order by dproducto";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
				rrtemp = new DataSet();
				tempAdapter.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
				{
                    CbbServicioA.Items.Clear();
                    foreach (DataRow iteration_row in rrtemp.Tables[0].Rows)
					{  
                       CbbServicioA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dproducto"]), Convert.ToInt32(iteration_row["gproducto"])));
					}
				}
				rrtemp.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_producto:proRellenacombo", ex);
			}
		}

		public void proRellenaFecha(string ParaFecha)
		{
            System.DateTime TempDate = DateTime.FromOADate(0);
            SdcFecha.Text = (DateTime.TryParse(ParaFecha, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : ParaFecha;
            System.DateTime TempDate3 = DateTime.FromOADate(0);
            System.DateTime TempDate2 = DateTime.FromOADate(0);
            tbHoraCambio.Text = ((DateTime.TryParse(ParaFecha, out TempDate2)) ? TempDate2.ToString("HH") : ParaFecha) + ":" + ((DateTime.TryParse(ParaFecha, out TempDate3)) ? TempDate3.ToString("mm") : ParaFecha);
		}

		public void proRellenaGrid()
		{
			string sqltemp = String.Empty;
			DataSet rrtemp = null;
			int iColumna = 0;
			int iFila = 0;
			try
			{
				sqltemp = "select * from amovprod,dproduct where itiposer='H'" + " and ganoregi=" + IGanoprod.ToString() + " and gnumregi=" + IGnumprod.ToString() + " and gidenpac='" + VstGidenprod + "' and " + " amovprod.gproducto=dproduct.gproducto order by fmovimie";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
				rrtemp = new DataSet();
				tempAdapter.Fill(rrtemp);
				if (rrtemp.Tables[0].Rows.Count != 0)
				{
					SprProductos.MaxRows = rrtemp.Tables[0].Rows.Count;
					iFila = 0;
					foreach (DataRow iteration_row in rrtemp.Tables[0].Rows)
					{
						iFila++;
						iColumna = 0;
						SprProductos.Row = iFila;
						iColumna++;
						SprProductos.Col = iColumna;
						SprProductos.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm");
						SprProductos.Lock = true;
						iColumna++;
						SprProductos.Col = iColumna;
						SprProductos.Text = Convert.ToString(iteration_row["dproducto"]);
						SprProductos.Lock = true;
						iColumna++;
						SprProductos.Col = iColumna;
						SprProductos.Text = Convert.ToString(iteration_row["gproducto"]);
						iColumna++;
						SprProductos.Col = iColumna;
						SprProductos.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
					}
				}
				else
				{
					SprProductos.MaxRows = 0;
				}
				rrtemp.Close();
				SprProductos.Row = SprProductos.MaxRows;
				SprProductos.Col = 1;
				SprProductos.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
				if (SprProductos.MaxRows != 0)
				{
					BUltimo = true;
					proActualizaFila();
				}
				else
				{
					BUltimo = false;
				}
				//proActivaEliminar
				//proVaciaDatosa
				//proActivaAceptar
				VstSeleccionado = false;
				IUltimoSeleccionado = SprProductos.Row;

                SprProductos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly; 
				proActivaEliminar();
				proVaciaDatosa();
				proActivaAceptar();
				//rellenar fecha con la hora del sistema
				proRellenaFecha(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_producto:proRellenaGrid", ex);
			}
		}

		public void proVaciaDatosa()
		{
			//CbbServicioA.Text = ""
			CbbServicioA.SelectedIndex = -1;
		}

        private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
        {
            string sqltemp = String.Empty;
            DataSet rrtemp = null;
            string vstfecha = String.Empty;
            string vstSegundos = String.Empty;
            int iColumna = 0;
            int iFila = 0;
            string vstFechaTabla = String.Empty;
            string vstFechaAnterior = String.Empty;
            string vstFechaPosterior = String.Empty;
            int iErrorFecha = 0;
            string mensaje1 = String.Empty;
            string mensaje2 = String.Empty;
            string mensaje3 = String.Empty;
            bool factError = false;
            DataSet trrAnterior = null;

            try
            {
                factError = false;

                iErrorFecha = 0;
                iColumna = SprProductos.Col;
                iFila = SprProductos.Row;
                vstSegundos = DateTime.Now.ToString("ss");
                SprProductos.Col = 4;

                //fecha del cambio en pantalla
                System.DateTime TempDate = DateTime.FromOADate(0);
                vstfecha = (DateTime.TryParse(SdcFecha.Text + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : SdcFecha.Text + " " + tbHoraCambio.Text + ":" + vstSegundos;
                if (!Information.IsDate(vstfecha))
                {
                    short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[] { "Hora" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    tbHoraCambio.Focus();
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                {
                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                        Conexion_Base_datos.RcAdmision.Close();
                    Conexion_Base_datos.RcAdmision.Open();

                    //fecha del movimiento de la tabla
                    vstFechaTabla = DateTime.Parse(SprProductos.Text).ToString("dd/MM/yyyy HH:mm:ss");
                    //si no es el ultimo movimiento actualizar fechas para validar
                    if (SprProductos.Row - 1 == 0)
                    {
                        //si se modifica el primer elemento de la tabla la
                        //fecha anterior es la del ingreso es decir la del propio movimiento
                        vstFechaAnterior = vstFechaTabla;
                    }
                    else
                    {
                        if (VstSeleccionado)
                        {
                            SprProductos.Row--;
                        }
                        vstFechaAnterior = DateTime.Parse(SprProductos.Text).ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    //verificar que se ha cambiado algo
                    System.DateTime TempDate3 = DateTime.FromOADate(0);
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    if (IProducto != Convert.ToInt32(CbbServicioA.SelectedValue) || DateTime.Parse((DateTime.TryParse(vstFechaTabla, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : vstFechaTabla) != DateTime.Parse((DateTime.TryParse(vstfecha, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm") : vstfecha))
                    {
                        System.DateTime TempDate5 = DateTime.FromOADate(0);
                        System.DateTime TempDate4 = DateTime.FromOADate(0);
                        if (DateTime.Parse((DateTime.TryParse(vstfecha, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:mm") : vstfecha) <= DateTime.Parse((DateTime.TryParse(vstFechaAnterior, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy HH:mm") : vstFechaAnterior))
                        {
                            System.DateTime TempDate7 = DateTime.FromOADate(0);
                            System.DateTime TempDate6 = DateTime.FromOADate(0);
                            if (VstSeleccionado && DateTime.Parse((DateTime.TryParse(vstFechaTabla, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy HH:mm") : vstFechaTabla) == DateTime.Parse((DateTime.TryParse(vstfecha, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy HH:mm") : vstfecha))
                            {
                                //nada
                            }
                            else
                            {
                                iErrorFecha = 1020;
                                mensaje1 = "La fecha del cambio";
                                mensaje2 = "mayor";
                                mensaje3 = "la fecha del anterior movimiento " + vstFechaAnterior;
                            }
                        }
                        else
                        {
                            System.DateTime TempDate8 = DateTime.FromOADate(0);
                            if (DateTime.Parse((DateTime.TryParse(vstfecha, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy HH:mm") : vstfecha) > DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm")))
                            {
                                iErrorFecha = 1020;
                                mensaje1 = "La fecha del cambio";
                                mensaje2 = "menor o igual";
                                mensaje3 = "la fecha del sistema " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                            }
                        }

                        if (iErrorFecha == 0)
                        {
                            if (!VstSeleccionado)
                            {
                                //si ha cambiado de producto
                                if (IProducto != Convert.ToInt32(CbbServicioA.SelectedValue))
                                {
                                    //cerrar el movimiento anterior y crear uno nuevo
                                    sqltemp = "select * from amovprod where " +
                                              " ganoregi=" + IGanoprod.ToString() +
                                              " and gnumregi=" + IGnumprod.ToString() +
                                              " and itiposer='H' and " +
                                              " ffinmovi is null";
                                    SqlCommand command = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                                    SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                                    rrtemp = new DataSet();
                                    tempAdapter.Fill(rrtemp);

                                    rrtemp.Tables[0].Rows[0]["ffinmovi"] = vstfecha;
                                    rrtemp.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                    rrtemp.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(rrtemp, rrtemp.Tables[0].TableName);

                                    rrtemp.AddNew();
                                    rrtemp.Tables[0].Rows[0]["itiposer"] = "H";
                                    rrtemp.Tables[0].Rows[0]["ganoregi"] = IGanoprod;
                                    rrtemp.Tables[0].Rows[0]["gnumregi"] = IGnumprod;
                                    rrtemp.Tables[0].Rows[0]["fmovimie"] = vstfecha;
                                    rrtemp.Tables[0].Rows[0]["gidenpac"] = VstGidenprod;
                                    rrtemp.Tables[0].Rows[0]["gproducto"] = CbbServicioA.SelectedValue;
                                    rrtemp.Tables[0].Rows[0]["iestado"] = "P";
                                    rrtemp.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                    rrtemp.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(rrtemp, rrtemp.Tables[0].TableName);

                                    //actualizar aepisadm
                                    sqltemp = "select * from aepisadm where ganoadme=" + IGanoprod.ToString() + " and gnumadme=" + IGnumprod.ToString();
                                    SqlCommand command1 = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command1);
                                    rrtemp = new DataSet();
                                    tempAdapter_4.Fill(rrtemp);
                                    rrtemp.Tables[0].Rows[0]["gproducto"] = CbbServicioA.SelectedValue;
                                    SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_4);
                                    tempAdapter_4.Update(rrtemp, rrtemp.Tables[0].TableName);
                                    rrtemp.Close();
                                }
                                else
                                {
                                    //debe cambiar al menos de producto
                                    short tempRefParam3 = 1500;
                                    string[] tempRefParam4 = new string[] { "cambiar", "un dato" };
                                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                                }

                            }
                            else
                            {
                                //modificar los datos del movimiento con los nuevos datos
                                sqltemp = "select * from amovprod where " +
                                          " ganoregi=" + IGanoprod.ToString() +
                                          " and gnumregi=" + IGnumprod.ToString() +
                                          " and itiposer='H' " +
                                          " and fmovimie=" + Serrores.FormatFechaHMS(vstFechaTabla);
                                SqlCommand command = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                                SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(command);
                                rrtemp = new DataSet();
                                tempAdapter_6.Fill(rrtemp);
                                if (!fnFacturacion(rrtemp))
                                {
                                    this.Cursor = Cursors.Default;
                                    rrtemp.Close();
                                    
                                    short tempRefParam6 = 1200;
                                    string[] tempRefParam7 = new string[] { "Este apunte ", " facturado,imposible modificar" };
                                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, Conexion_Base_datos.RcAdmision, tempRefParam7));
                                    SprProductos.Col = iColumna;
                                    SprProductos.Row = iFila;
                                    return;
                                }
                                else
                                {
                                    if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["fpasfact"]))
                                    {
                                        //cambiamos la fecha de fin de movimiento del registro anterior
                                        object tempRefParam8 = rrtemp.Tables[0].Rows[0]["fmovimie"];
                                        sqltemp = "SELECT ffinmovi, itiposer, ganoregi, gnumregi, fmovimie " + " FROM AMOVPROD WHERE " + "      GNUMREGI = " + Convert.ToString(rrtemp.Tables[0].Rows[0]["gnumregi"]) + "  and GANOREGI = " + Convert.ToString(rrtemp.Tables[0].Rows[0]["ganoregi"]) + "  and ITIPOSER = 'H'" + "  and FFINMOVI =" + Serrores.FormatFechaHMS(tempRefParam8) + "";
                                        SqlCommand command1 = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                                        SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(command1);
                                        trrAnterior = new DataSet();
                                        tempAdapter_7.Fill(trrAnterior);
                                        if (trrAnterior.Tables[0].Rows.Count != 0)
                                        {
                                            trrAnterior.Tables[0].Rows[0]["ffinmovi"] = vstfecha;
                                            SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_7);
                                            tempAdapter_7.Update(trrAnterior, trrAnterior.Tables[0].TableName);
                                        }
                                        trrAnterior.Close();

                                        rrtemp.Tables[0].Rows[0]["fmovimie"] = vstfecha;
                                        rrtemp.Tables[0].Rows[0]["gproducto"] = CbbServicioA.SelectedValue;
                                        rrtemp.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                        rrtemp.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                        SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_6);
                                        tempAdapter_6.Update(rrtemp, rrtemp.Tables[0].TableName);

                                        sqltemp = "select * from aepisadm where ganoadme=" + IGanoprod.ToString() + " and gnumadme=" + IGnumprod.ToString();
                                        SqlCommand command2 = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                                        SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(command2);
                                        rrtemp = new DataSet();
                                        tempAdapter_10.Fill(rrtemp);

                                        rrtemp.Tables[0].Rows[0]["gproducto"] = CbbServicioA.SelectedValue;
                                        SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapter_10);
                                        tempAdapter_10.Update(rrtemp, rrtemp.Tables[0].TableName);
                                    }
                                    else
                                    {
                                        factError = true;
                                    }
                                    rrtemp.Close();
                                }
                            }
                        }

                        SprProductos.Col = iColumna;
                        SprProductos.Row = iFila;
                        this.Cursor = Cursors.Default;
                        if (iErrorFecha != 0)
                        {
                            short tempRefParam9 = (short)iErrorFecha;
                            string[] tempRefParam10 = new string[] { mensaje1, mensaje2, mensaje3 };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                            iErrorFecha = tempRefParam9;
                            if (SdcFecha.Enabled)
                            {
                                SdcFecha.Focus();
                            }

                        }
                        else
                        {
                            if (!factError)
                            {
                                scope.Complete();
                                proRellenaGrid();
                                SprProductos.CurrentRow = null;
                            }
                            else
                            {
                                short tempRefParam11 = 1160;
                                string[] tempRefParam12 = new string[] { "modificar ", "este apunte", "apunte pasado a facturaci�n" };
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12));
                            }
                        }
                    }
                    else
                    {
                        SprProductos.Col = iColumna;
                        SprProductos.Row = iFila;
                        this.Cursor = Cursors.Default;
                        //no ha efectuado cambios
                        short tempRefParam13 = 1500;
                        string[] tempRefParam14 = new string[] { "cambiar", "un dato" };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, Conexion_Base_datos.RcAdmision, tempRefParam14));
                    }
                }
            }
            catch (Exception ex)
            {
                //error de modificando movimientos de productos
                this.Cursor = Cursors.Default;
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_producto:CbAceptar_click", ex);
            }
        }

        private void CbbServicioA_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			//validar boton de aceptar
			proActivaAceptar();
		}


		private void CbbServicioA_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			proActivaAceptar();
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void CbEliminar_Click(Object eventSender, EventArgs eventArgs)
		{
           SqlCommand command; 

			string sqltemp = String.Empty;
			DataSet rrtemp = null;
			bool bactualiza = true;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //eliminar de amovprod
                SprProductos.Row = SprProductos.ActiveRowIndex;
                SprProductos.Col = 4;
                object tempRefParam = SprProductos.Text;
                sqltemp = "select * from amovprod where " +
                          " ganoregi=" + IGanoprod.ToString() + " and " +
                          " gnumregi=" + IGnumprod.ToString() + " and " +
                          " fmovimie=" + Serrores.FormatFechaHMS(tempRefParam) + " and " +
                          " itiposer ='H'";
                using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                {
                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                        Conexion_Base_datos.RcAdmision.Close();
                    Conexion_Base_datos.RcAdmision.Open();
                    command = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                    rrtemp = new DataSet();
                    tempAdapter.Fill(rrtemp);
                    if (!fnFacturacion(rrtemp))
                    {
                        this.Cursor = Cursors.Default;
                        rrtemp.Close();

                        short tempRefParam2 = 1200;
                        string[] tempRefParam3 = new string[] { "Este apunte ", " facturado,imposible eliminar" };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam2, Conexion_Base_datos.RcAdmision, tempRefParam3));
                    }
                    else
                    {
                        if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["fpasfact"]))
                        {
                            rrtemp.Delete(tempAdapter);
                            rrtemp.Close();
                            //vaciar la fecha del ffinmovi del ultimo movimiento
                            sqltemp = "select * from amovprod where " +
                                      " ganoregi=" + IGanoprod.ToString() + " and " +
                                      " gnumregi=" + IGnumprod.ToString() + " and itiposer='H' and " +
                                      " fmovimie in (select max(fmovimie) from amovprod where " +
                                      " ganoregi=" + IGanoprod.ToString() +
                                      " and gnumregi=" + IGnumprod.ToString() +
                                      " and itiposer='H')";

                            command = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command);
                            rrtemp = new DataSet();
                            tempAdapter_2.Fill(rrtemp);

                            rrtemp.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                            tempAdapter_2.Update(rrtemp, rrtemp.Tables[0].TableName);
                            rrtemp.Close();

                            //Si es la �ltima fila actualizar datos en aepisadm
                            if (BUltimo)
                            {
                                sqltemp = "select * from aepisadm where ganoadme=" + IGanoprod.ToString() + " and " + " gnumadme=" + IGnumprod.ToString();
                                command = new SqlCommand(sqltemp, Conexion_Base_datos.RcAdmision);
                                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command);
                                rrtemp = new DataSet();
                                tempAdapter_4.Fill(rrtemp);
                                SprProductos.Row = SprProductos.MaxRows - 1;
                                proActualizaFila();

                                rrtemp.Tables[0].Rows[0]["gproducto"] = IProducto;
                                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                                tempAdapter_4.Update(rrtemp, rrtemp.Tables[0].TableName);
                                rrtemp.Close();
                            }
                            scope.Complete();
                        }
                        else
                        {
                            short tempRefParam4 = 1160;
                            string[] tempRefParam5 = new string[] { "eliminar ", "este apunte", "apunte pasado a facturaci�n" };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, Conexion_Base_datos.RcAdmision, tempRefParam5));
                        }

                        this.Cursor = Cursors.Default;
                        proRellenaGrid();
                        proVaciaDatosa();

                    }
                }
            }
            catch (Exception ex)
            {
                //Error eliminando movimiento en amovprod
                this.Cursor = Cursors.Default;
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_producto:Cbeliminar_click", ex);
            }
		}

		private void Cambio_producto_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				//Busca la sociedad principal
				//rellenar cabecera
				proCabecerasGrid();
				//rellenar grid
				proRellenaGrid();
				//rellenar combo a:
				proRellenacombo();
				//rellenar fecha con la hora del sistema
				proRellenaFecha(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
				proActivaAceptar();
				VstSeleccionado = false;
                SprProductos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
			}
		}

		private void Cambio_producto_Load(Object eventSender, EventArgs eventArgs)
		{

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label28.Text = LitPersona;
		}

        private void SprProductos_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;
			//actualizar datos de:
			//vaciar datos a:
			//validar el boton eliminar
			if (Row != 0)
			{
				BUltimo = Row == SprProductos.MaxRows;
				if (VstSeleccionado && IUltimoSeleccionado == Row)
				{
					VstSeleccionado = false;
                    SprProductos.CurrentRow = null;
				}
				else
				{
                    VstSeleccionado = true;
				}
				IUltimoSeleccionado = Row;

				SprProductos.Row = Row;
				proVaciaDatosa();
				proActualizaFila();
			}
			proActivaEliminar();
			proActivaAceptar();

            if (VstSeleccionado)
            {
                SdcFecha.Enabled = false;
                tbHoraCambio.Enabled = false;
            }
            else
            {
                SdcFecha.Enabled = true;
                tbHoraCambio.Enabled = true;
            }
		}

		public void proRecibePaciente(string Paragidenpac, int ParaAno, int ParaNum, string ParaNomPaciente)
		{
			VstGidenprod = Paragidenpac;
			IGanoprod = ParaAno;
			IGnumprod = ParaNum;
			LbPaciente.Text = ParaNomPaciente;
			Cambio_producto_Activated(this, new EventArgs());
		}
        
		public bool fnFacturacion(DataSet RrCursor)
		{
			bool result = false;
			result = true;

			object tempRefParam = RrCursor.Tables[0].Rows[0]["fmovimie"];
			string stSqlFact = "select ffactura,fsistema from dmovfact where" + " gidenpac='" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gidenpac"]) + "' and " + " ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and " + " gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and " + " gcodeven='ES' and ffactura is not null and " + " ffechfin >" + Serrores.FormatFechaHMS(tempRefParam);

			if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
			{
				string tempRefParam2 = "DMOVFACT";
				stSqlFact = stSqlFact + " UNION ALL " + Serrores.proReplace(ref stSqlFact, tempRefParam2, "DMOVFACH");
			}

            SqlCommand command = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
			DataSet rrFact = new DataSet();
			tempAdapter.Fill(rrFact);
			if (rrFact.Tables[0].Rows.Count != 0)
			{
				result = false;
			}

			return result;
		}

		private void tbHoraCambio_Leave(Object eventSender, EventArgs eventArgs)
		{
            if (tbHoraCambio.Text != "  :  ")
            {
                tbHoraCambio.Text = DateTime.Parse(tbHoraCambio.Text).ToString("HH:mm");
            }
		}
		private void Cambio_producto_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
		}
	}
}