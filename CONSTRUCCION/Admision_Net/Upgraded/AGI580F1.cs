using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using CrystalWrapper;

namespace ADMISION
{
	public partial class Listado_ambulancias
		: RadForm
	{

		public Listado_ambulancias()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		private void Listado_ambulancias_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

        Crystal cryInformes = null;

		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }

        private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }

        public void proImprimir(Crystal.DestinationConstants Parasalida)
		{
			string sqlAmbulancias = String.Empty;
			string LitPersona = String.Empty;

			try
			{
				if (sdcFechDesde.Value.Date > sdcFechHasta.Value.Date)
				{
					// Control del mensaje enviado
					stMensaje1 = "menor o igual";
					//UPGRADE_WARNING: (6021) Casting 'Variant' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
					//UPGRADE_WARNING: (1068) oClass.RespuestaMensaje() of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
					//UPGRADE_WARNING: (1068) oClass.RespuestaMensaje() of type Variant is being forced to VBA.VbMsgBoxResult. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
					short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { LbFechDesde.Text, stMensaje1, LbFechHasta.Text };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					// ***************************
					sdcFechDesde.Focus();
					return;
				}
				if (sdcFechHasta.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
				{
					// Control del mensaje enviado
					stMensaje1 = "menor o igual";
					stMensaje2 = "Fecha del Sistema";
					//UPGRADE_WARNING: (6021) Casting 'Variant' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
					//UPGRADE_WARNING: (1068) oClass.RespuestaMensaje() of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
					//UPGRADE_WARNING: (1068) oClass.RespuestaMensaje() of type Variant is being forced to VBA.VbMsgBoxResult. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
					short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { LbFechHasta.Text, stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
					// ***************************
					sdcFechHasta.Focus();
					return;
				}

				sqlAmbulancias = "select aepisadm.faltplan, dpacient.dnombpac, dpacient.dape1pac, ";
				sqlAmbulancias = sqlAmbulancias + "dpacient.dape2pac, aepisadm.dplacamb, ";
				sqlAmbulancias = sqlAmbulancias + "aepisadm.dnomcond, dempream.dempream, dcausamb.dcauambu  ";
				sqlAmbulancias = sqlAmbulancias + "From aepisadm ";
				sqlAmbulancias = sqlAmbulancias + "inner join dpacient on aepisadm.gidenpac = dpacient.gidenpac ";
				sqlAmbulancias = sqlAmbulancias + "left join dempream on aepisadm.gempream = dempream.gempream ";
				sqlAmbulancias = sqlAmbulancias + "left join dcausamb on aepisadm.gcauambu = dcausamb.gcauambu ";
				sqlAmbulancias = sqlAmbulancias + "Where ";
				sqlAmbulancias = sqlAmbulancias + "aepisadm.iambulan = 'S' and ";
				object tempRefParam5 = sdcFechDesde.Text + " 00:00:00";
				sqlAmbulancias = sqlAmbulancias + "aepisadm.faltplan>= " + Serrores.FormatFechaHMS(tempRefParam5) + " and ";
				object tempRefParam6 = sdcFechHasta.Text + " 23:59:59";
				sqlAmbulancias = sqlAmbulancias + "aepisadm.faltplan<= " + Serrores.FormatFechaHMS(tempRefParam6) + " ";
				sqlAmbulancias = sqlAmbulancias + "\r" + "\n" + "order by faltplan ";

				if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
				{
					Conexion_Base_datos.proCabCrystal();
				}
                cryInformes.ReportFileName = Conexion_Base_datos.PathString + "\\RPT\\AGI580R2.rpt";
                cryInformes.Formulas["FORM1"]= "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
				cryInformes.Formulas["FORM2"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
				cryInformes.Formulas["FORM3"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
				cryInformes.Formulas["Fdesde"] = "\"" + sdcFechDesde.Value.Date + "\"";
				cryInformes.Formulas["Fhasta"] = "\"" + sdcFechHasta.Value.Date + "\"";

				LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
				LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
				cryInformes.Formulas["LitPers"] = "\"" + LitPersona + "\"";

				cryInformes.SQLQuery = sqlAmbulancias;
				
				cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
				cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + "";
				cryInformes.Destination = Parasalida;
				cryInformes.WindowShowPrintSetupBtn = true;
				if (cryInformes.Destination == Crystal.DestinationConstants.crptToPrinter)
				{
					Conexion_Base_datos.proDialogo_impre(cryInformes);
				}
				cryInformes.Action = 1;
				cryInformes.PrinterStopPage = cryInformes.PageCount;
				cryInformes.Reset();
				this.Cursor = Cursors.Default;
			}
			catch (Exception e)
			{
				//UPGRADE_ISSUE: (2036) Form property Listado_ambulancias.MousePointer does not support custom mousepointers. More Information: http://www.vbtonet.com/ewis/ewi2036.aspx
				this.Cursor = Cursors.Default;
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 32755)
				{
					//si pulsa cancelar en la pantalla de impresi�n
				}
			}
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void Listado_ambulancias_Load(Object eventSender, EventArgs eventArgs)
		{
            cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;
		}
		private void Listado_ambulancias_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}