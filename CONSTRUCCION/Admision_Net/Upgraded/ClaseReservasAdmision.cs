using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ADMISION
{
	internal class ClaseReservasAdmision
	{


		//PROPIEDADES DEL OBJECTO "reservas en Admision"

		string Identificador = String.Empty; // obligatorio
		string FechaPrevistaIng = String.Empty; // obligatorio
		string FechaInValDiagnos = String.Empty;
		string CodigoPlanta = String.Empty; // obligatorio
		string CodigoHabitacion = String.Empty; // obligatorio
		string CodigoCama = String.Empty; // obligatorio
		string Cliente = String.Empty; // obligatorio
		string SociedadPri = String.Empty; // obligatorio
		string SociedadFar = String.Empty;
		string SociedadMedica = String.Empty;
		string Producto = String.Empty; // obligatorio
		//Oscar C 23/11/2004
		string FInterven = String.Empty; //Fecha de Intervencion
		string DPrevistosEst = String.Empty; //Dias previstos de estancias
		string stObserva = String.Empty; //Observaciones
		//-------------------
		string IndRegimenAlojamiento = String.Empty;
		string ViaLlegada = String.Empty;
		string CantFianza = String.Empty;
		string Estado = String.Empty; // obligatorio
		string IndEnfermoMental = String.Empty; // obligatorio
		string Juzgado1 = String.Empty;
		string Juzgado2 = String.Empty;
		string Expediente1 = String.Empty;
		string Expediente2 = String.Empty;
		string FechaResolucion = String.Empty;
		string FechaRes2 = String.Empty;
		string vstusuario = String.Empty;
		string FechaAtencion = String.Empty; // Fecha reserva
		string A�o = String.Empty;
		string Numero = String.Empty;
		string ServicioRes = String.Empty;
		string MedicoRes = String.Empty;
		string UnidadRes = String.Empty;
		string CodDiagRes = String.Empty;
		string TexDiagRes = String.Empty;
		int A�oProg = 0;
		int NumeroProg = 0;

		//oscar C ene/2005
		string msocieda = String.Empty;
		string minspecc = String.Empty;
		string midReserva = String.Empty;
		string mOperacionreserva = String.Empty;
		//-------
		//Fernando Silvestre 05/12/2005
		string IndPrioridad = String.Empty;
		//-------

		//O.Frias - 30/06/2009
		//Incorporamos los nuevos valores.
		string TIPOINGRESO = String.Empty;
		string tipoAmbito = String.Empty;
		//''Dim hospiOrigen As Integer
		string hospiOrigen = String.Empty;
		string servSoli = String.Empty;
		string perSoli = String.Empty;
		string medServSoli = String.Empty;
		string anoUrge = String.Empty;
		string numUrge = String.Empty;

		//LISTA DE FUNCIONES ASOCIADAS A LAS PROPIEDADES
		//-------
		//***************************************************************************************
		//******************************PROPIEDADES A ESTABLECER*********************************
		//***************************************************************************************


		public string TexDiagReserva
		{
			get
			{
				return TexDiagRes;
			}
			set
			{
				TexDiagRes = value;
			}
		}



		public string CodDiagReserva
		{
			get
			{
				return CodDiagRes;
			}
			set
			{
				CodDiagRes = value;
			}
		}

		public string UnidadReserva
		{
			get
			{
				return UnidadRes;
			}
			set
			{
				UnidadRes = value;
			}
		}


		public string MedicoReserva
		{
			get
			{
				return MedicoRes;
			}
			set
			{
				MedicoRes = value;
			}
		}



		public string ServicioReserva
		{
			get
			{
				return ServicioRes;
			}
			set
			{
				ServicioRes = value;
			}
		}



		public string IdentificadorPaciente
		{
			get
			{
				return Identificador;
			}
			set
			{
				Identificador = value;
			}
		}




		public string FechaInValDiag
		{
			get
			{
				return FechaInValDiagnos;
			}
			set
			{
				FechaInValDiagnos = value;
			}
		}



		public string FechaPrevistaIngreso
		{
			get
			{
				return FechaPrevistaIng;
			}
			set
			{
				FechaPrevistaIng = value;
			}
		}


		public string CodigoCompletoCama
		{
			get
			{
				return StringsHelper.Format(CodigoPlanta, "00") + StringsHelper.Format(CodigoHabitacion, "00") + CodigoCama;
			}
			set
			{
				CodigoPlanta = Convert.ToInt32(Double.Parse(value.Substring(0, Math.Min(2, value.Length)))).ToString();
				CodigoHabitacion = Convert.ToInt32(Double.Parse(value.Substring(2, Math.Min(2, value.Length - 2)))).ToString();
				CodigoCama = value.Substring(4).Trim();
			}
		}



		public string TipoCliente
		{
			get
			{
				return Cliente;
			}
			set
			{
				Cliente = value;
			}
		}

		public string CodigoSociedadPri
		{
			get
			{
				return SociedadPri;
			}
			set
			{
				SociedadPri = value;
			}
		}

		public string CodigoSociedadFar
		{
			get
			{
				return SociedadFar;
			}
			set
			{
				SociedadFar = value;
			}
		}

		public string CodigoSociedadMedica
		{
			get
			{
				return SociedadMedica;
			}
			set
			{
				SociedadMedica = value;
			}
		}

		public string IndicadorRegimenAlojamiento
		{
			get
			{
				return IndRegimenAlojamiento;
			}
			set
			{
				IndRegimenAlojamiento = value;
			}
		}

		public string CodigoViaLlegada
		{
			get
			{
				return ViaLlegada;
			}
			set
			{
				ViaLlegada = value;
			}
		}

		public string fianza
		{
			get
			{
				return CantFianza;
			}
			set
			{
				CantFianza = value;
			}
		}

		public string IndEstado
		{
			get
			{
				return Estado;
			}
			set
			{
				Estado = value;
			}
		}

		public string IndicadorEnfermoMental
		{
			get
			{
				return IndEnfermoMental;
			}
			set
			{
				IndEnfermoMental = value;
			}
		}

		public string CodigoJuzgado1
		{
			get
			{
				return Juzgado1;
			}
			set
			{
				Juzgado1 = value;
			}
		}



		public string NumeroExpediente1
		{
			get
			{
				return Expediente1;
			}
			set
			{
				Expediente1 = value;
			}
		}



		public string FechaResolucion1
		{
			get
			{
				return FechaResolucion;
			}
			set
			{
				FechaResolucion = value;
			}
		}



		public string usuario
		{
			get
			{
				return vstusuario;
			}
			set
			{
				vstusuario = value;
			}
		}



		public string FechaReservaAtencion
		{
			get
			{
				return FechaAtencion;
			}
			set
			{
				FechaAtencion = value;
			}
		}



		public string A�oRegistro
		{
			get
			{
				return A�o;
			}
			set
			{
				A�o = value;
			}
		}

		public string NumeroRegistro
		{
			get
			{
				return Numero;
			}
			set
			{
				Numero = value;
			}
		}

		public int A�oProgramacion
		{
			get
			{
				return A�oProg;
			}
			set
			{
				A�oProg = value;
			}
		}

		public int NumeroProgramacion
		{
			get
			{
				return NumeroProg;
			}
			set
			{
				NumeroProg = value;
			}
		}


		//oscar C ene/2005
		//oscar C ene/2005
		public string gsocieda
		{
			get
			{
				return msocieda;
			}
			set
			{
				msocieda = value;
			}
		}

		public string ginspecc
		{
			get
			{
				return minspecc;
			}
			set
			{
				minspecc = value;
			}
		}

		public string idReserva
		{
			get
			{
				return midReserva;
			}
			set
			{
				midReserva = value;
			}
		}

		public string OperacionReserva
		{
			get
			{
				return mOperacionreserva;
			}
			set
			{
				mOperacionreserva = value;
			}
		}




		public string CodigoProducto
		{
			get
			{
				return Producto;
			}
			set
			{
				Producto = value;
			}
		}


		//Oscar C 23/11/2004
		//Fecha de Intervencion
		public string FechaIntervencion
		{
			get
			{
				return FInterven;
			}
			set
			{
				FInterven = value;
			}
		}

		//Dias previstos de estancias

		public string DiasPrevistosEst
		{
			get
			{
				return DPrevistosEst;
			}
			set
			{
				DPrevistosEst = value;
			}
		}

		//Observaciones

		public string Observaciones
		{
			get
			{
				return stObserva;
			}
			set
			{
				stObserva = value;
			}
		}

		//-------
		//Fernando Silvestre 05/12/2005
		public string IndicadorPrioridad
		{
			get
			{
				return IndPrioridad;
			}
			set
			{
				IndPrioridad = value;
			}
		}

		//-------
		//O.Frias- 30/06/2009
		//Incorporamos los nuevos valores
		public string tpIngreso
		{
			get
			{
				return TIPOINGRESO;
			}
			set
			{
				TIPOINGRESO = value;
			}
		}


		public string tpAmbito
		{
			get
			{
				return tipoAmbito;
			}
			set
			{
				tipoAmbito = value;
			}
		}


		public string hosOrigen
		{
			get
			{
				return hospiOrigen;
			}
			set
			{
				hospiOrigen = value;
			}
		}


		public string serviSoli
		{
			get
			{
				return servSoli;
			}
			set
			{
				servSoli = value;
			}
		}


		public string persoSoli
		{
			get
			{
				return perSoli;
			}
			set
			{
				perSoli = value;
			}
		}


		public string medSerSoli
		{
			get
			{
				return medServSoli;
			}
			set
			{
				medServSoli = value;
			}
		}


		public string AnoRegUrge
		{
			get
			{
				return anoUrge;
			}
			set
			{
				anoUrge = value;
			}
		}


		public string NumRegUrge
		{
			get
			{
				return numUrge;
			}
			set
			{
				numUrge = value;
			}
		}


		public string CodigoJuzgado2
		{
			get
			{
				return Juzgado2;
			}
			set
			{
				Juzgado2 = value;
			}
		}

		public string NumeroExpediente2
		{
			get
			{
				return Expediente2;
			}
			set
			{
				Expediente2 = value;
			}
		}

		public string FechaResolucion2
		{
			get
			{
				return FechaRes2;
			}
			set
			{
				FechaRes2 = value;
			}
		}


		//-------------------


		//Public Function Actualizar(CodigoAccion As Long, RcReserva As rdoConnection)
		//    RealizarAccion CodigoAccion
		//End Function
		//
		// pone a todas a EMPTY
		public object Reset()
		{
			Identificador = "";
			FechaPrevistaIng = "";
			CodigoPlanta = "";
			CodigoHabitacion = "";
			CodigoCama = "";
			Cliente = "";
			SociedadPri = "";
			SociedadFar = "";
			SociedadMedica = "";
			Producto = "";
			//Oscar C 23/11/2004
			FInterven = "";
			DPrevistosEst = "";
			stObserva = "";
			//------------------
			IndRegimenAlojamiento = "";
			ViaLlegada = "";
			CantFianza = "";
			Estado = "";
			IndEnfermoMental = "";
			Juzgado1 = "";
			Expediente1 = "";
			FechaResolucion = "";
			vstusuario = "";
			FechaAtencion = "";
			A�o = "";
			Numero = "";
			A�oProg = 0;
			NumeroProg = 0;
			//oscar C Ene 2005
			msocieda = "";
			minspecc = "";
			midReserva = "";
			//-----
			IndPrioridad = "";

			//O.Frias - 30/06/2009
			//Incorporamos los nuevos valores.
			TIPOINGRESO = "";
			tipoAmbito = "";
			hospiOrigen = "0";
			servSoli = "";
			perSoli = "0";
			medServSoli = "";
			anoUrge = "";
			numUrge = "";


			return null;
		}

		// SE EDITA O A�ADE CON LOS DATOS QUE SE OBTIENEn de claseReserva
		// Cuando se edita necesito los datos anteriores para poder recuperar el registro

		public object EditarReserva(SqlConnection RcReserva, ClaseReservasAdmision claseReserva)
		{
			string sqlEditar = String.Empty;


			//Oscar C Julio 2012
			if (StringsHelper.ToDoubleSafe(claseReserva.hosOrigen) > 0)
			{
				sqlEditar = "UPDATE DPACIENT SET ghospscs=" + claseReserva.hosOrigen + 
				            "WHERE gidenpac='" + claseReserva.IdentificadorPaciente + "'";
				SqlCommand tempCommand = new SqlCommand(sqlEditar, RcReserva);
				tempCommand.ExecuteNonQuery();
			}
			//-----------

			//sqlEditar = "select * from aresingrvp where gidenpac='" & claseReserva.IdentificadorPaciente & "'"
			//OSCAR C ENE 2005
			//'sqlEditar = "select * from aresingrva where gidenpac='" & claseReserva.IdentificadorPaciente & "' and " & _
			//''            "ganoadme is null and gnumadme is null "
			sqlEditar = "select * from aresingrva where " + 
			            "     ganoregi = " + claseReserva.idReserva.Substring(0, Math.Min(claseReserva.idReserva.IndexOf('/'), claseReserva.idReserva.Length)).Trim() + 
			            " AND gnumregi = " + claseReserva.idReserva.Substring(claseReserva.idReserva.IndexOf('/') + 1).Trim() + 
			            " AND ganoadme is null and gnumadme is null ";
			//----------

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEditar, RcReserva);
			DataSet RrEditar = new DataSet();
			tempAdapter.Fill(RrEditar);
			if (RrEditar.Tables[0].Rows.Count == 1)
			{
				RrEditar.Edit();
				RrEditar.Tables[0].Rows[0]["fpreving"] = DateTime.Parse(claseReserva.FechaPrevistaIngreso);

				if (Conversion.Val(claseReserva.CodigoCompletoCama.Trim()) == 0)
				{
					RrEditar.Tables[0].Rows[0]["gplantas"] = DBNull.Value;
					RrEditar.Tables[0].Rows[0]["ghabitac"] = DBNull.Value;
					RrEditar.Tables[0].Rows[0]["gcamasbo"] = DBNull.Value;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["gplantas"] = Convert.ToInt32(Double.Parse(claseReserva.CodigoCompletoCama.Substring(0, Math.Min(2, claseReserva.CodigoCompletoCama.Length))));
					RrEditar.Tables[0].Rows[0]["ghabitac"] = Convert.ToInt32(Double.Parse(claseReserva.CodigoCompletoCama.Substring(2, Math.Min(2, claseReserva.CodigoCompletoCama.Length - 2))));
					RrEditar.Tables[0].Rows[0]["gcamasbo"] = claseReserva.CodigoCompletoCama.Substring(4).Trim();
				}

                if (claseReserva.UnidadReserva.Trim() == "")
                    RrEditar.Tables[0].Rows[0]["gunienfe"] = DBNull.Value;
                else RrEditar.Tables[0].Rows[0]["gunienfe"] = claseReserva.UnidadReserva.Trim();

				if (claseReserva.IndicadorRegimenAlojamiento != "")
				{
					RrEditar.Tables[0].Rows[0]["iregaloj"] = claseReserva.IndicadorRegimenAlojamiento;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["iregaloj"] = DBNull.Value;
				}

                if (Conversion.Val(claseReserva.ServicioReserva.Trim()) == 0)
                    RrEditar.Tables[0].Rows[0]["gservici"] = DBNull.Value;
                else RrEditar.Tables[0].Rows[0]["gservici"] = Conversion.Val(claseReserva.ServicioReserva.Trim());
                if (Conversion.Val(claseReserva.MedicoReserva.Trim()) == 0)
                    RrEditar.Tables[0].Rows[0]["gpersona"] = DBNull.Value;
                else RrEditar.Tables[0].Rows[0]["gpersona"] = Conversion.Val(claseReserva.MedicoReserva.Trim());

                //oscar 30/03/2004
                if (Conversion.Val(claseReserva.CodigoProducto.Trim()) == 0)
                    RrEditar.Tables[0].Rows[0]["gproducto"] = DBNull.Value;
                else RrEditar.Tables[0].Rows[0]["gproducto"] = Conversion.Val(claseReserva.CodigoProducto.Trim());
                //----

				if (claseReserva.CodDiagReserva.Trim() != "")
				{
					RrEditar.Tables[0].Rows[0]["gdiaging"] = claseReserva.CodDiagReserva.Trim();
				}
				else if (claseReserva.TexDiagReserva.Trim() != "")
				{ 
					RrEditar.Tables[0].Rows[0]["odiaging"] = claseReserva.TexDiagReserva.Trim();
				}
				//si no hay codigo nulos
				if (claseReserva.CodDiagReserva.Trim() == "")
				{
					RrEditar.Tables[0].Rows[0]["gdiaging"] = DBNull.Value;
					RrEditar.Tables[0].Rows[0]["finvaldi"] = DBNull.Value;
				}
				if (claseReserva.TexDiagReserva.Trim() == "")
				{
					RrEditar.Tables[0].Rows[0]["odiaging"] = DBNull.Value;
				}

				if (claseReserva.FechaInValDiag.Trim() != "")
				{
					RrEditar.Tables[0].Rows[0]["finvaldi"] = DateTime.Parse(claseReserva.FechaInValDiag);
				}
				else if (claseReserva.FechaInValDiag.Trim() == "")
				{ 
					RrEditar.Tables[0].Rows[0]["finvaldi"] = DBNull.Value;
				}

				if (claseReserva.IndicadorRegimenAlojamiento != "")
				{
					RrEditar.Tables[0].Rows[0]["iregaloj"] = claseReserva.IndicadorRegimenAlojamiento;
				}
				RrEditar.Tables[0].Rows[0]["gusuario"] = claseReserva.usuario;
				RrEditar.Tables[0].Rows[0]["ganoadme"] = DBNull.Value;
				RrEditar.Tables[0].Rows[0]["gnumadme"] = DBNull.Value;
				if (claseReserva.A�oProgramacion != 0)
				{
					RrEditar.Tables[0].Rows[0]["ganoprog"] = claseReserva.A�oProgramacion;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["ganoprog"] = DBNull.Value;
				}
				if (claseReserva.NumeroProgramacion != 0)
				{
					RrEditar.Tables[0].Rows[0]["gnumprog"] = claseReserva.NumeroProgramacion;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["gnumprog"] = DBNull.Value;
				}

				//oscar 10/03/2004
				if (claseReserva.fianza != "")
				{
					string tempRefParam = claseReserva.fianza.Trim();
					string tempRefParam2 = Serrores.ObtenerSeparadorDecimalBD(RcReserva);
					RrEditar.Tables[0].Rows[0]["cfianza"] = Serrores.ConvertirDecimales(ref tempRefParam, ref tempRefParam2, 2);
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["cfianza"] = DBNull.Value;
				}


				if (claseReserva.FechaIntervencion != "")
				{
					//oscar C 23/11/2004
					//Si cambia la fecha de prevision de intervencion, se desasocia de la programacion
					if (!Convert.IsDBNull(RrEditar.Tables[0].Rows[0]["finterve"]))
					{
						if (Convert.ToDateTime(RrEditar.Tables[0].Rows[0]["finterve"]) != DateTime.Parse(claseReserva.FechaIntervencion))
						{
							RrEditar.Tables[0].Rows[0]["ganoprog"] = DBNull.Value;
							RrEditar.Tables[0].Rows[0]["gnumprog"] = DBNull.Value;
						}
					}
					RrEditar.Tables[0].Rows[0]["finterve"] = DateTime.Parse(claseReserva.FechaIntervencion);
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["finterve"] = DBNull.Value;
					RrEditar.Tables[0].Rows[0]["ganoprog"] = DBNull.Value;
					RrEditar.Tables[0].Rows[0]["gnumprog"] = DBNull.Value;
				}

				if (claseReserva.DiasPrevistosEst != "")
				{
					RrEditar.Tables[0].Rows[0]["ndiasest"] = Convert.ToInt32(Double.Parse(claseReserva.DiasPrevistosEst));
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["ndiasest"] = DBNull.Value;
				}
				if (claseReserva.Observaciones != "")
				{
					RrEditar.Tables[0].Rows[0]["oobserva"] = claseReserva.Observaciones;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
				}
				//OSCAR C ENE 2005
				if (claseReserva.gsocieda != "")
				{
					RrEditar.Tables[0].Rows[0]["gsocieda"] = claseReserva.gsocieda;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["gsocieda"] = DBNull.Value;
				}
				if (claseReserva.ginspecc != "")
				{
					RrEditar.Tables[0].Rows[0]["ginspecc"] = claseReserva.ginspecc;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
				}
				//-----
				//Fernando Silvestre 05/12/2005
				if (claseReserva.IndicadorPrioridad != "")
				{
					RrEditar.Tables[0].Rows[0]["ipriorid"] = claseReserva.IndicadorPrioridad;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["ipriorid"] = DBNull.Value;
				}
				//-----

				//O.Frias - 01/07/2009
				//Se actualizan los nuevos campos.
				if (claseReserva.tpIngreso != "")
				{
					RrEditar.Tables[0].Rows[0]["itipingr"] = claseReserva.tpIngreso;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["itipingr"] = DBNull.Value;
				}

				if (claseReserva.tpAmbito != "")
				{
					RrEditar.Tables[0].Rows[0]["itipambi"] = claseReserva.tpAmbito;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["itipambi"] = DBNull.Value;
				}

				if (StringsHelper.ToDoubleSafe(claseReserva.hosOrigen) != 0)
				{
					RrEditar.Tables[0].Rows[0]["ghospori"] = claseReserva.hosOrigen;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["ghospori"] = DBNull.Value;
				}

				if (claseReserva.serviSoli.Trim() != "" && claseReserva.serviSoli.Trim() != "0")
				{
					RrEditar.Tables[0].Rows[0]["gsersoli"] = Conversion.Val(claseReserva.serviSoli);
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["gsersoli"] = DBNull.Value;
				}

				if (claseReserva.persoSoli.Trim() != "" && claseReserva.persoSoli.Trim() != "0")
				{
					RrEditar.Tables[0].Rows[0]["gpersoli"] = Conversion.Val(claseReserva.persoSoli);
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["gpersoli"] = DBNull.Value;
				}

				if (claseReserva.medSerSoli != "")
				{
					RrEditar.Tables[0].Rows[0]["dmediori"] = claseReserva.medSerSoli;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["dmediori"] = DBNull.Value;
				}

				if (claseReserva.AnoRegUrge != "")
				{
					RrEditar.Tables[0].Rows[0]["ganourge"] = claseReserva.AnoRegUrge;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["ganourge"] = DBNull.Value;
				}
				if (claseReserva.NumRegUrge != "")
				{
					RrEditar.Tables[0].Rows[0]["gnumurge"] = claseReserva.NumRegUrge;
				}
				else
				{
					RrEditar.Tables[0].Rows[0]["gnumurge"] = DBNull.Value;
				}

				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrEditar, RrEditar.Tables[0].TableName);
			}
			RrEditar.Close();
			return null;
		}


		public object A�adirReserva(SqlConnection RcReserva, ClaseReservasAdmision claseReserva, int anoLEQ, int numLEQ)
		{
			string sqlA�adir = String.Empty;
			string sqlLE = String.Empty;
			DataSet RrLE = null;

			//Oscar C Julio 2012
			if (StringsHelper.ToDoubleSafe(claseReserva.hosOrigen) > 0)
			{
				sqlA�adir = "UPDATE DPACIENT SET ghospscs=" + claseReserva.hosOrigen + 
				            "WHERE gidenpac='" + claseReserva.IdentificadorPaciente + "'";
				SqlCommand tempCommand = new SqlCommand(sqlA�adir, RcReserva);
				tempCommand.ExecuteNonQuery();
			}
			//-----------

			sqlA�adir = "select * from aresingrva where 1=2";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlA�adir, RcReserva);
			DataSet RrA�adir = new DataSet();
			tempAdapter.Fill(RrA�adir);
			RrA�adir.AddNew();
			RrA�adir.Tables[0].Rows[0]["gidenpac"] = claseReserva.IdentificadorPaciente;
			RrA�adir.Tables[0].Rows[0]["fpreving"] = DateTime.Parse(claseReserva.FechaPrevistaIngreso);
			//RrA�adir("gplantas") = CLng(Mid(claseReserva.CodigoCompletoCama, 1, 2))
			if (Conversion.Val(claseReserva.CodigoCompletoCama.Trim()) == 0)
			{
				RrA�adir.Tables[0].Rows[0]["gplantas"] = DBNull.Value;
				RrA�adir.Tables[0].Rows[0]["ghabitac"] = DBNull.Value;
				RrA�adir.Tables[0].Rows[0]["gcamasbo"] = DBNull.Value;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["gplantas"] = Convert.ToInt32(Double.Parse(claseReserva.CodigoCompletoCama.Substring(0, Math.Min(2, claseReserva.CodigoCompletoCama.Length))));
				RrA�adir.Tables[0].Rows[0]["ghabitac"] = Convert.ToInt32(Double.Parse(claseReserva.CodigoCompletoCama.Substring(2, Math.Min(2, claseReserva.CodigoCompletoCama.Length - 2))));
				RrA�adir.Tables[0].Rows[0]["gcamasbo"] = claseReserva.CodigoCompletoCama.Substring(4).Trim();
			}

            if (claseReserva.UnidadReserva.Trim() == "")
                RrA�adir.Tables[0].Rows[0]["gunienfe"] = DBNull.Value;
            else RrA�adir.Tables[0].Rows[0]["gunienfe"] = claseReserva.UnidadReserva.Trim();
            if (Conversion.Val(claseReserva.ServicioReserva.Trim()) == 0)
                RrA�adir.Tables[0].Rows[0]["gservici"] = DBNull.Value;
            else RrA�adir.Tables[0].Rows[0]["gservici"] = Conversion.Val(claseReserva.ServicioReserva.Trim());

            //oscar 30/03/2004
            if (Conversion.Val(claseReserva.CodigoProducto.Trim()) == 0)
                RrA�adir.Tables[0].Rows[0]["gproducto"] = DBNull.Value;
            else RrA�adir.Tables[0].Rows[0]["gproducto"] = Conversion.Val(claseReserva.CodigoProducto.Trim());
            //----
            if (Conversion.Val(claseReserva.MedicoReserva.Trim()) == 0)
                RrA�adir.Tables[0].Rows[0]["gpersona"] = DBNull.Value;
            else RrA�adir.Tables[0].Rows[0]["gpersona"] = Conversion.Val(claseReserva.MedicoReserva.Trim());
			if (claseReserva.CodDiagReserva.Trim() != "")
			{
				RrA�adir.Tables[0].Rows[0]["gdiaging"] = claseReserva.CodDiagReserva.Trim();
			}
			else if (claseReserva.TexDiagReserva.Trim() != "")
			{ 
				RrA�adir.Tables[0].Rows[0]["odiaging"] = claseReserva.TexDiagReserva.Trim();
			}
			if (claseReserva.CodDiagReserva.Trim() == "")
			{
				RrA�adir.Tables[0].Rows[0]["gdiaging"] = DBNull.Value;
				RrA�adir.Tables[0].Rows[0]["finvaldi"] = DBNull.Value;
			}
			if (claseReserva.TexDiagReserva.Trim() == "")
			{
				RrA�adir.Tables[0].Rows[0]["odiaging"] = DBNull.Value;
			}

			if (claseReserva.FechaInValDiag.Trim() != "")
			{
				RrA�adir.Tables[0].Rows[0]["finvaldi"] = DateTime.Parse(claseReserva.FechaInValDiag);
			}
			else if (claseReserva.FechaInValDiag.Trim() == "")
			{ 
				RrA�adir.Tables[0].Rows[0]["finvaldi"] = DBNull.Value;
			}
			if (claseReserva.IndicadorRegimenAlojamiento != "")
			{
				RrA�adir.Tables[0].Rows[0]["iregaloj"] = claseReserva.IndicadorRegimenAlojamiento;
			}
			RrA�adir.Tables[0].Rows[0]["gusuario"] = claseReserva.usuario;
			RrA�adir.Tables[0].Rows[0]["fatencion"] = claseReserva.FechaReservaAtencion;
			RrA�adir.Tables[0].Rows[0]["ganoadme"] = DBNull.Value;
			RrA�adir.Tables[0].Rows[0]["gnumadme"] = DBNull.Value;
			//    If Ia�opro <> 0 Then
			//        RrA�adir("ganoprog") = Ia�opro
			//    Else
			//        RrA�adir("ganoprog") = Null
			//    End If
			//    If lnumpro <> 0 Then
			//        RrA�adir("gnumprog") = lnumpro
			//    Else
			//        RrA�adir("gnumprog") = Null
			//    End If

			if (claseReserva.A�oProgramacion != 0)
			{
				RrA�adir.Tables[0].Rows[0]["ganoprog"] = claseReserva.A�oProgramacion;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["ganoprog"] = DBNull.Value;
			}
			if (claseReserva.NumeroProgramacion != 0)
			{
				RrA�adir.Tables[0].Rows[0]["gnumprog"] = claseReserva.NumeroProgramacion;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["gnumprog"] = DBNull.Value;
			}


			//oscar 10/03/2004
			if (claseReserva.fianza != "")
			{
				string tempRefParam = claseReserva.fianza.Trim();
				string tempRefParam2 = Serrores.ObtenerSeparadorDecimalBD(RcReserva);
				RrA�adir.Tables[0].Rows[0]["cfianza"] = Serrores.ConvertirDecimales(ref tempRefParam, ref tempRefParam2, 2);
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["cfianza"] = DBNull.Value;
			}
			//oscar C 23/11/2004
			if (claseReserva.FechaIntervencion != "")
			{
				RrA�adir.Tables[0].Rows[0]["finterve"] = DateTime.Parse(claseReserva.FechaIntervencion);
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["finterve"] = DBNull.Value;
			}
			if (claseReserva.DiasPrevistosEst != "")
			{
				RrA�adir.Tables[0].Rows[0]["ndiasest"] = Convert.ToInt32(Double.Parse(claseReserva.DiasPrevistosEst));
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["ndiasest"] = DBNull.Value;
			}
			if (claseReserva.Observaciones != "")
			{
				RrA�adir.Tables[0].Rows[0]["oobserva"] = claseReserva.Observaciones;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
			}

			//oscar 01/07/04

			int lano = DateTime.Today.Year;
            Numeracion.SIGUIENTE ob = new Numeracion.SIGUIENTE();
            int lnumero = Convert.ToInt32(ob.SIG_NUMERO("NRORESIN", RcReserva));
			ob = null;

			RrA�adir.Tables[0].Rows[0]["ganoregi"] = lano;
			RrA�adir.Tables[0].Rows[0]["gnumregi"] = lnumero;
			//-------------

			//OSCAR C ENE 2005
			if (claseReserva.gsocieda != "")
			{
				RrA�adir.Tables[0].Rows[0]["gsocieda"] = claseReserva.gsocieda;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["gsocieda"] = DBNull.Value;
			}
			if (claseReserva.ginspecc != "")
			{
				RrA�adir.Tables[0].Rows[0]["ginspecc"] = claseReserva.ginspecc;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
			}

			claseReserva.idReserva = lano.ToString() + "/" + lnumero.ToString();
			//-----
			//Fernando Silvestre 05/12/2005
			if (claseReserva.IndicadorPrioridad != "")
			{
				RrA�adir.Tables[0].Rows[0]["ipriorid"] = claseReserva.IndicadorPrioridad;
			}

			//-----

			//O.Frias - 01/07/2009
			//Se actualizan los nuevos campos.
			if (claseReserva.tpIngreso != "")
			{
				RrA�adir.Tables[0].Rows[0]["itipingr"] = claseReserva.tpIngreso;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["itipingr"] = DBNull.Value;
			}

			if (claseReserva.tpAmbito != "")
			{
				RrA�adir.Tables[0].Rows[0]["itipambi"] = claseReserva.tpAmbito;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["itipambi"] = DBNull.Value;
			}

			if (StringsHelper.ToDoubleSafe(claseReserva.hosOrigen) != 0)
			{
				RrA�adir.Tables[0].Rows[0]["ghospori"] = claseReserva.hosOrigen;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["ghospori"] = DBNull.Value;
			}

			if (claseReserva.serviSoli.Trim() != "" && claseReserva.serviSoli.Trim() != "0")
			{
				RrA�adir.Tables[0].Rows[0]["gsersoli"] = Conversion.Val(claseReserva.serviSoli);
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["gsersoli"] = DBNull.Value;
			}

			if (claseReserva.persoSoli.Trim() != "" && claseReserva.persoSoli.Trim() != "0")
			{
				RrA�adir.Tables[0].Rows[0]["gpersoli"] = Conversion.Val(claseReserva.persoSoli);
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["gpersoli"] = DBNull.Value;
			}

			if (claseReserva.medSerSoli != "")
			{
				RrA�adir.Tables[0].Rows[0]["dmediori"] = claseReserva.medSerSoli;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["dmediori"] = DBNull.Value;
			}

			if (claseReserva.AnoRegUrge != "")
			{
				RrA�adir.Tables[0].Rows[0]["ganourge"] = claseReserva.AnoRegUrge;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["ganourge"] = DBNull.Value;
			}
			if (claseReserva.NumRegUrge != "")
			{
				RrA�adir.Tables[0].Rows[0]["gnumurge"] = claseReserva.NumRegUrge;
			}
			else
			{
				RrA�adir.Tables[0].Rows[0]["gnumurge"] = DBNull.Value;
			}

			//O.Frias - 25/01/2013
			//Se agrega la actualizacion de nuevos campoes relacion de reserva de ingreso con LE.
			if (anoLEQ != 0)
			{
				RrA�adir.Tables[0].Rows[0]["ganolesp"] = anoLEQ;
				RrA�adir.Tables[0].Rows[0]["gnumlesp"] = numLEQ;
				if (Convert.IsDBNull(RrA�adir.Tables[0].Rows[0]["ganoprog"]))
				{
					sqlLE = "Select ganoprog, gnumprog from AEPISLEQ where ganoregi = " + anoLEQ.ToString() + " and gnumregi = " + numLEQ.ToString();
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlLE, RcReserva);
					RrLE = new DataSet();
					tempAdapter_2.Fill(RrLE);
					if (RrLE.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RrLE.Tables[0].Rows[0]["ganoprog"]))
						{
							RrA�adir.Tables[0].Rows[0]["ganoprog"] = RrLE.Tables[0].Rows[0]["ganoprog"];
							RrA�adir.Tables[0].Rows[0]["gnumprog"] = RrLE.Tables[0].Rows[0]["gnumprog"];
						}
					}
					RrLE.Close();
				}
			}

			SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
            tempAdapter.Update(RrA�adir, RrA�adir.Tables[0].TableName);
			RrA�adir.Close();


			return null;
		}
	}
}