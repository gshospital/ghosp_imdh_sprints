using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using CrystalWrapper;

namespace ADMISION
{
	public partial class Movimientos_SS
		: Telerik.WinControls.UI.RadForm
    {

		string sqlLista = String.Empty;
		string stFecha = String.Empty;
		bool fDiag1 = false;
		bool fDiag2 = false;
		DataSet RrDINSPECC = null;
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		string stFechaini = String.Empty;
		string stFechafin = String.Empty;
		string stIntervalo = String.Empty;
		string stTipo = String.Empty;
		string stSeleccion = String.Empty;
		string stInspeccion = String.Empty;
		string stSql = String.Empty;
		string stInforme = String.Empty;
		string vstSqServ = String.Empty;
		DataSet vRrServicio = null;

		int viform = 0;

        
        //Workspace Wk = null;
		DataSet DbInforme = new DataSet();
        
		bool ttemp = false;
        
        Crystal CrystalReport1 = new Crystal();

		string vvSTTemporal = String.Empty;

		string stBaseTemp2 = String.Empty; // debido a que se lanzan los dos report seguidos, el primero deja bloqueada la
		public Movimientos_SS()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

        // BBDD y el segundo no puede acceder a el, con lo cual utilizo otra BBDD

        
        public void proImprimir(Crystal.DestinationConstants Parasalida)
		{
			string LitPersona = String.Empty;
			try
			{
				this.Cursor = Cursors.WaitCursor;
				// Realizamos las validaciones de las fechas introducidas *******************
				if (SdcFechaIni.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
				{
					stMensaje1 = "menor o igual";
					stMensaje2 = "Fecha del Sistema";
					
					short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { Label1.Text, stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					SdcFechaIni.Focus();
					return;
				}
				else if (SdcFechaIni.Value.Date > sdcFechaFin.Value.Date)
				{ 
					stMensaje1 = "menor o igual";
					stMensaje2 = Label2.Text;
					
					short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { Label1.Text, stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
					SdcFechaIni.Focus();
					return;
				}
				else if (sdcFechaFin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
				{ 
					stMensaje1 = "menor o igual";
					stMensaje2 = "Fecha del Sistema";
					
					short tempRefParam5 = 1020;
                    string[] tempRefParam6 = new string[] { Label2.Text, stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
					sdcFechaFin.Focus();
					return;
				}
				// **************************************************************************
				// FORMATEO DE LA FECHA, para mandar en la consulta SQL un rango de fecha, en
				// nuestro caso el dia entero
				stFechaini = SdcFechaIni.Text;
				stFechaini = stFechaini + " 00:00:00";
				stFechafin = sdcFechaFin.Text;
				stFechafin = stFechafin + " 23:59:59";

				// **************************************************************************
				stIntervalo = "Fecha inicio: " + SdcFechaIni.Text + "  Fecha fin: " + sdcFechaFin.Text;
				//    PathString = App.Path   ' Cojo la ruta de acceso para buscar el informe
				//'    If stFORMFILI = "V" Then
				//'        ProvinEstado = "ESTADO"
				//'    Else
				Conexion_Base_datos.ProvinEstado = "PROVINCIA";
				//'    End If

				LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
				LitPersona = LitPersona.ToUpper();

				if (chbEntradas.CheckState != CheckState.Unchecked)
				{
					CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;

					stSeleccion = "Tipo de Movimiento: Entradas";

                    //ProObtenerDatos("fmovimie", "TEMPORAL"); //"Temporal"
                    ProObtenerDatos("fmovimie", "CR11_agi4110r1"); //"Temporal"

                    // CrystalReport1.Connect = "dsn=GHOSP_ACCESS"
                    //CrystalReport1.Connect = "dsn=" + Conexion_Base_datos.stNombreDsnACCESS;
                    CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";

                    CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi4110r1_CR11.rpt";
					CrystalReport1.WindowTitle = "Listado de movimientos para la Seguridad Social";

                    //CrystalReport1.set_DataFiles(0, Conexion_Base_datos.StBaseTemporal);
                    CrystalReport1.DataFiles[0] = DbInforme.Tables["CR11_agi4110r1"];

                    CrystalReport1.SQLQuery = " SELECT * FROM Temporal" + "\r" + "\n" + " ORDER BY Fecha";
					//*********cabeceras
					if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
					{
						Conexion_Base_datos.proCabCrystal();
					}
					//*******************
					CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
					CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
					CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
					CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
					CrystalReport1.Formulas["{@MOVIMIENTO}"] = "\"" + stSeleccion + "\"";
					CrystalReport1.Formulas["{@INSPECCION}"] = "\"" + ((stInspeccion.Length > 120) ? stInspeccion.Substring(0, Math.Min(120, stInspeccion.Length)) + "..." : stInspeccion) + "\"";
					CrystalReport1.Formulas["{@TexProvincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";
					CrystalReport1.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";
					CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
					CrystalReport1.Destination = Parasalida;
					CrystalReport1.WindowShowPrintSetupBtn = true;
					if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
					{
						Conexion_Base_datos.proDialogo_impre(CrystalReport1);
					}
					CrystalReport1.Action = 1;
					CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
					CrystalReport1.Reset();

					CrystalReport1 = null;

				}

				if (chbSalidas.CheckState != CheckState.Unchecked)
				{

					CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;

					stSeleccion = "Tipo de Movimiento: Salidas";

                    //ProObtenerDatos("ffinmovi", "TEMPORAL"); //"Temporal"
                    ProObtenerDatos("ffinmovi", "CR11_agi4110r2"); //"Temporal"

                    //CrystalReport1.Connect = "DSN=GHOSP_ACCESS"
                    //CrystalReport1.Connect = "DSN=" + Conexion_Base_datos.stNombreDsnACCESS;
                    CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";

                    CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi4110r2_CR11.rpt";
					CrystalReport1.WindowTitle = "Listado de movimientos para la Seguridad Social";
                    //CrystalReport1.set_DataFiles(0, Conexion_Base_datos.StBaseTemporal);
                    CrystalReport1.DataFiles[0] = DbInforme.Tables["CR11_agi4110r2"];

                    CrystalReport1.SQLQuery = " SELECT * FROM Temporal" + "\r" + "\n" + " ORDER BY Fecha";

					//*********cabeceras
					if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
					{
						Conexion_Base_datos.proCabCrystal();
					}
					//*******************
					CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
					CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
					CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
					CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
					CrystalReport1.Formulas["{@MOVIMIENTO}"] = "\"" + stSeleccion + "\"";
					CrystalReport1.Formulas["{@INSPECCION}"] = "\"" + ((stInspeccion.Length > 120) ? stInspeccion.Substring(0, Math.Min(120, stInspeccion.Length)) + "..." : stInspeccion) + "\"";
					CrystalReport1.Formulas["{@TexProvincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";
					CrystalReport1.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";
					CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
					CrystalReport1.Destination = Parasalida;
					CrystalReport1.WindowShowPrintSetupBtn = true;

					if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter && !(chbEntradas.CheckState != CheckState.Unchecked))
					{
						Conexion_Base_datos.proDialogo_impre(CrystalReport1);
					}
					//            If chbEntradas Then
					//                For iEspera = 0 To 11000000
					//                Next
					//            End If
					CrystalReport1.Action = 1;
					CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
					CrystalReport1.Reset();
					CrystalReport1 = null;
				}

				CbPantalla.Enabled = false;
				CbImprimir.Enabled = false;
				chbEntradas.CheckState = CheckState.Unchecked;
				chbSalidas.CheckState = CheckState.Unchecked;
				SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
				sdcFechaFin.Text = DateTime.Today.ToString("dd/MM/yyyy");
				LsbLista2.Items.Clear();
				LsbLista1.Items.Clear();
				proCarga_Lista1();
				CbInsTodo.Enabled = true;
				CbInsUno.Enabled = true;
				CbDelTodo.Enabled = false;
				CbDelUno.Enabled = false;
				tbCodiDiagini.Text = "";
				TbCodDiagfin.Text = "";
				tbDiagnosini.Text = "";
				TbDiagnosfin.Text = "";
				if (!FrmHospital.Visible)
				{
					ChBHosDia.CheckState = CheckState.Checked;
					ChBHosGeneral.CheckState = CheckState.Checked;
				}
				else
				{
					ChBHosDia.CheckState = CheckState.Unchecked;
					ChBHosGeneral.CheckState = CheckState.Unchecked;
				}

				this.Cursor = Cursors.Default;
			}
			catch (Exception e)
			{
				this.Cursor = Cursors.Default;
				if (Information.Err().Number == 32755)
				{
					CrystalReport1.Reset();
					CbPantalla.Enabled = false;
					CbImprimir.Enabled = false;
					chbEntradas.CheckState = CheckState.Unchecked;
					chbSalidas.CheckState = CheckState.Unchecked;
					SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
					LsbLista2.Items.Clear();
					LsbLista1.Items.Clear();
					proCarga_Lista1();
					CbInsTodo.Enabled = true;
					CbInsUno.Enabled = true;
					CbDelTodo.Enabled = false;
					CbDelUno.Enabled = false;
					tbCodiDiagini.Text = "";
					TbCodDiagfin.Text = "";
					tbDiagnosini.Text = "";
					TbDiagnosfin.Text = "";
					if (!FrmHospital.Visible)
					{
						ChBHosDia.CheckState = CheckState.Checked;
						ChBHosGeneral.CheckState = CheckState.Checked;
					}
					else
					{
						ChBHosDia.CheckState = CheckState.Unchecked;
						ChBHosGeneral.CheckState = CheckState.Unchecked;
					}

					CrystalReport1 = null;
				}
				else
				{
					if (Information.Err().Number == 20527)
					{
					}
					else
					{
						MessageBox.Show("Error: " + Information.Err().Number.ToString() + " " + e.Message, Application.ProductName);
					}
				}
			}
		}
        

		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			//If ttemp Then
			//    DbInforme.Close
			//    wk.Close
			//End If
			this.Close();
			//menu_admision.prokill (stInforme)
		}

		private void CbDelTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			LsbLista2.Items.Clear();
			LsbLista1.Items.Clear();
			proCarga_Lista1();
			CbDelTodo.Enabled = false;
			CbDelUno.Enabled = false;
			CbInsTodo.Enabled = true;
			CbInsUno.Enabled = true;
			CbImprimir.Enabled = false;
			CbPantalla.Enabled = false;

		}

		private void CbDelUno_Click(Object eventSender, EventArgs eventArgs)
		{
            if (LsbLista2.SelectedIndex == -1)
            {
				//MsgBox ("Debe haber seleccionado alguna entidad")
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1270, Conexion_Base_datos.RcAdmision, "una Inspeccion"));
				return;
			}
            LsbLista1.Items.Add(LsbLista2.SelectedItem);
            LsbLista2.Refresh();
            LsbLista2.SelectedIndex = -1;
            if (LsbLista2.Items.Count == 0)
			{
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
			}

		}

		private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{            
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
		}

		private void CbInsTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			LsbLista1.Items.Clear();
			LsbLista2.Items.Clear();
			proCarga_Lista2();
			CbInsTodo.Enabled = false;
			CbInsUno.Enabled = false;
			CbDelTodo.Enabled = true;
			CbDelUno.Enabled = true;
			if (((((int) chbEntradas.CheckState) | ((int) chbSalidas.CheckState)) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState))) != 0)
			{
				CbImprimir.Enabled = true;
				CbPantalla.Enabled = true;
			}
		}

		private void CbInsUno_Click(Object eventSender, EventArgs eventArgs)
		{
            if (LsbLista1.SelectedIndex == -1)
            {
				//MsgBox ("Debe haber seleccionado alguna entidad")
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1270, Conexion_Base_datos.RcAdmision, "una Inspecci�n"));
				return;
			}
            LsbLista2.Items.Add(LsbLista1.SelectedItem);
            LsbLista1.SelectedIndex = -1;
            LsbLista1.Refresh();
			CbDelUno.Enabled = true;
			CbDelTodo.Enabled = true;
			if (((((int) chbEntradas.CheckState) | ((int) chbSalidas.CheckState)) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState))) != 0)
			{
				CbImprimir.Enabled = true;
				CbPantalla.Enabled = true;
			}
		}

		private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{            
            proImprimir(Crystal.DestinationConstants.crptToWindow);
		}

		private void chbEntradas_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}
        
		private void ChBHosDia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}

		private void ChBHosGeneral_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}

		private void chbSalidas_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}

		private void Movimientos_SS_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				chbEntradas.TabIndex = 1;
				chbSalidas.TabIndex = 2;
				SdcFechaIni.TabIndex = 3;
				tbCodiDiagini.TabIndex = 4;
				tbCodiDiagini.TabIndex = 5;
				TbCodDiagfin.TabIndex = 6;
				_Tbdiagini_Button1.TabIndex = 7;
				_TbDiagfin_Button1.TabIndex = 8;
				LsbLista1.TabIndex = 9;
				CbInsTodo.TabIndex = 10;
				CbInsUno.TabIndex = 11;
				CbDelTodo.TabIndex = 12;
				CbDelUno.TabIndex = 13;
				LsbLista1.TabIndex = 14;
				CbImprimir.TabIndex = 15;
				CbPantalla.TabIndex = 16;
				CbCerrar.TabIndex = 17;
				chbEntradas.Focus();
			}
		}

		private void Movimientos_SS_Load(Object eventSender, EventArgs eventArgs)
		{
			stInforme = Conexion_Base_datos.StBaseTemporal;
			ttemp = FileSystem.Dir(stInforme, FileAttribute.Normal) != "";


			bool MostrarHospitales = false;

			string stsqltemp = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='VERCAMAS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
				{
					MostrarHospitales = true;
				}
			}
			rrtemp.Close();

			if (!MostrarHospitales)
			{
				/*this.Height = 394;
				this.Width = 624;
				FrInspecciones.Top = 154;
				FrmHospital.Visible = false;
				FrPantalla.Height = 333;
				CbImprimir.Top = 337;
				CbPantalla.Top = 337;
				this.CbCerrar.Top = 337;*/
                FrmHospital.Visible = false;
                this.ChBHosDia.CheckState = CheckState.Checked;
				this.ChBHosGeneral.CheckState = CheckState.Checked;
			}

			//cambiar los literales en el tipo de ingreso
			stsqltemp = "SELECT VALFANU1,VALFANU2 FROM SCONSGLO WHERE GCONSGLO='DETIPING'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
			rrtemp = new DataSet();
			tempAdapter_2.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{
				ChBHosGeneral.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();
				ChBHosDia.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2"]).Trim();
			}
			rrtemp.Close();

			CbPantalla.Enabled = false;
			CbImprimir.Enabled = false;
			chbEntradas.CheckState = CheckState.Unchecked;
			chbSalidas.CheckState = CheckState.Unchecked;
			CbDelUno.Enabled = false;
			CbDelTodo.Enabled = false;
			proCarga_Lista1();

		}

		public void proCarga_Lista1()
		{
			try
			{
				//cargamos la lista con las INSPECCIONES
				sqlLista = "SELECT * " + " FROM DINSPECC ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				RrDINSPECC = new DataSet();
				tempAdapter.Fill(RrDINSPECC);
                foreach (DataRow iteration_row in RrDINSPECC.Tables[0].Rows)
				{
					if (!Convert.IsDBNull(iteration_row["dinspecc"]))
					{
                        LsbLista1.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dinspecc"]).Trim(), Convert.ToInt32(iteration_row["ginspecc"])));
                    }
                }
				RrDINSPECC.Close();
			}
			catch (SqlException ex)
			{
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		public void proCarga_Lista2()
		{
			try
			{
				//cargamos la lista con las INSPECCIONES
				sqlLista = "SELECT * " + " FROM DINSPECC ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				RrDINSPECC = new DataSet();
				tempAdapter.Fill(RrDINSPECC);

				foreach (DataRow iteration_row in RrDINSPECC.Tables[0].Rows)
				{
					if (!Convert.IsDBNull(iteration_row["dinspecc"]))
					{
                        LsbLista2.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dinspecc"]).Trim(), Convert.ToInt32(iteration_row["ginspecc"])));
                    }
				}
				RrDINSPECC.Close();
			}
			catch (SqlException ex)
			{
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void Movimientos_SS_Closed(Object eventSender, EventArgs eventArgs)
		{            
            CrystalReport1 = null;
		}

		private void LsbLista1_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsUno_Click(CbInsUno, new EventArgs());
			if (LsbLista1.Items.Count == 0)
			{
				CbInsUno.Enabled = false;
				CbInsTodo.Enabled = false;
			}
		}


		private void LsbLista2_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelUno_Click(CbDelUno, new EventArgs());
			if (LsbLista2.Items.Count == 0)
			{
				CbDelUno.Enabled = false;
				CbDelTodo.Enabled = false;
				CbInsUno.Enabled = true;
				CbInsTodo.Enabled = true;
			}
		}

		private void TbCodDiagfin_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			TbDiagnosfin.Text = "";
			proAceptar();
		}

		private void TbCodDiagfin_Enter(Object eventSender, EventArgs eventArgs)
		{
			TbCodDiagfin.SelectionStart = 1;
			TbCodDiagfin.SelectionLength = Strings.Len(TbCodDiagfin.Text);
		}

		private void TbCodDiagfin_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			fDiag2 = true;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void TbCodDiagfin_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (fDiag2)
			{
				vstSqServ = "select dnombdia from ddiagnos where GCODIDIA='" + TbCodDiagfin.Text + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(vstSqServ, Conexion_Base_datos.RcAdmision);
				vRrServicio = new DataSet();
				tempAdapter.Fill(vRrServicio);
				if (vRrServicio.Tables[0].Rows.Count == 1)
				{
					TbDiagnosfin.Text = Convert.ToString(vRrServicio.Tables[0].Rows[0]["dnombdia"]);
				}
				vRrServicio.Close();
				fDiag2 = false;
			}
		}

		private void tbCodiDiagini_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			tbDiagnosini.Text = "";
			proAceptar();
		}

		private void tbCodiDiagini_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbCodiDiagini.SelectionStart = 0;
			tbCodiDiagini.SelectionLength = Strings.Len(TbCodDiagfin.Text);
		}


		private void tbCodiDiagini_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			fDiag1 = true;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbCodiDiagini_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (fDiag1)
			{
				vstSqServ = "select dnombdia from ddiagnos where GCODIDIA='" + tbCodiDiagini.Text + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(vstSqServ, Conexion_Base_datos.RcAdmision);
				vRrServicio = new DataSet();
				tempAdapter.Fill(vRrServicio);
				if (vRrServicio.Tables[0].Rows.Count == 1)
				{
					tbDiagnosini.Text = Convert.ToString(vRrServicio.Tables[0].Rows[0]["dnombdia"]);
				}
				vRrServicio.Close();
				fDiag1 = false;
			}
			proAceptar();
		}

		private void TbDiagfin_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			stFecha = "FIN";

            Diagnosticos.DiagClase mcdiag = new Diagnosticos.DiagClase();			
			mcdiag.proLoad(this, mcdiag,ref Conexion_Base_datos.RcAdmision, "D",Type.Missing,Type.Missing);
			mcdiag = null;
           
            this.Show();
			proAceptar();

		}

		private void Tbdiagini_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			stFecha = "INI";

            Diagnosticos.DiagClase mcdiag = new Diagnosticos.DiagClase();			
			mcdiag.proLoad(this, mcdiag,ref Conexion_Base_datos.RcAdmision, "D",Type.Missing,Type.Missing);
            mcdiag = null;
			this.Show();
           
            proAceptar();
		}

		public void proRecoger_Diag(string Codidiag, string nomdiag)
		{
			switch(stFecha)
			{
				case "INI" : 
					tbCodiDiagini.Text = Codidiag; 
					tbDiagnosini.Text = nomdiag; 
					break;
				case "FIN" : 
					TbCodDiagfin.Text = Codidiag; 
					TbDiagnosfin.Text = nomdiag; 
					break;
			}

		}

		public void proAceptar()
		{
			if (TbCodDiagfin.Text != "" && tbCodiDiagini.Text == "")
			{
				CbPantalla.Enabled = false;
				CbImprimir.Enabled = false;
			}
			else
			{
				if (TbCodDiagfin.Text == "" && tbCodiDiagini.Text != "")
				{
					CbPantalla.Enabled = false;
					CbImprimir.Enabled = false;
				}
				else
				{
					//If (RbEntradas.Value = True Or RbSalidas.Value = True) And LsbLista2.ListCount <> 0 Then
					if ((((((int) chbEntradas.CheckState) | ((int) chbSalidas.CheckState)) & ((LsbLista2.Items.Count != 0) ? -1 : 0)) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState))) != 0)
					{
						CbPantalla.Enabled = true;
						CbImprimir.Enabled = true;
					}
					else
					{
						CbPantalla.Enabled = false;
						CbImprimir.Enabled = false;
					}
				}
			}
		}

		private void TbDiagnosfin_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void tbDiagnosini_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void ProObtenerDatos(string CampoFechaIncluir, string VstNomtabla)
		{
            
			object dbLangSpanish = null;
			object DBEngine = null;
			StringBuilder vstPaciente_D = new StringBuilder();
			DataSet RrAEPISADM = null;
			DataSet RrDPACIENT = null;
			DataSet RrAMOVIMIE = null;
			DataSet RrDPERSONA = null;
			string stAepisadm = String.Empty;
			string stDpacient = String.Empty;
			StringBuilder stAmovimie = new StringBuilder();
			StringBuilder stDpersona = new StringBuilder();
			object[] MatrizSql = null;
			DLLTextosSql.TextosSql oTextoSql = null;
			//stBaseTemp2 = stInforme
			//If UCase(CampoFechaIncluir) = "FFINMOVI" Then   ' SALIDAS -> UTILIZO OTRA ACABADA EN DOS
			//    stBaseTemp2 = Mid(stInforme, 1, InStr(1, stInforme, ".") - 1) & "2.mdb"
			//Else
			stBaseTemp2 = stInforme;
			//End If
			bool bExiste_base = false;
            /*Wk = (Workspace) DBEngine.Workspaces(0);
			if (FileSystem.Dir(stBaseTemp2, FileAttribute.Normal) == "")
			{
				DbInforme = (Database) Wk.CreateDatabase(stBaseTemp2, dbLangSpanish);
			}
			else
			{
				//SI EXISTE LA TABLA BORRAR
				DbInforme = (Database) Wk.OpenDatabase(stBaseTemp2);
				foreach (TableDef dTabla in DbInforme.TableDefs)
				{
					if (Convert.ToString(dTabla.Name).ToUpper() == VstNomtabla.ToUpper())
					{
						bExiste_base = true;
						break;
					}
				}
				//si existe borrarla
				if (bExiste_base)
				{
					DbInforme.Execute("drop table " + VstNomtabla);
				}
			}*/

            DbInforme.CreateTable(VstNomtabla, Conexion_Base_datos.PathString + "\\XSD\\" + VstNomtabla + ".xsd");
			//DbInforme.Execute("CREATE TABLE " + VstNomtabla.Trim() + "(FECHA DATETIME, SS TEXT, HISTORIA TEXT, CAMA TEXT, " + " PACIENTE TEXT (100), NACIMIENTO DATETIME, SEXO TEXT, INSPECCION TEXT, " + " DIRECCION TEXT (100), POBLACION TEXT (100), " + " CODIGO TEXT, PROVINCIA TEXT (100), TELEFONO TEXT, " + " MOTIVO TEXT, DIAGNOSTICO TEXT (120), " + " MEDICO TEXT)");

			//DbInforme.Execute(" create index tempoindex " + "on " + VstNomtabla.Trim() + " (FECHA)");

			ttemp = true;

			// REGISTRAMOS EL DSN PARA EL CRYSTAL
			string stConexion = "Description= BASE TEMPORAL EN ACCESS" + "\r" + "Oemtoansi=no" + "\r" + "server = (local)" + "\r" + "dbq=" + stBaseTemp2 + "";
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, stConexion);
            // GRABAMOS EN LA TABLA QUE HEMOS CREADO LOS CAMPOS QUE NOS VAN
            // HACER FALTA EN EL LISTADO
            //Recordset RsINFORME = DbInforme.OpenRecordset("Select * from " + VstNomtabla.Trim() + " where 2=1");
            DataRow RsINFORME = null;

            object tempRefParam = stFechaini;
			object tempRefParam2 = stFechafin;
			StringBuilder stAmovifin = new StringBuilder();
			stAmovifin.Append("SELECT * " + " FROM AMOVIFIN, SCONSGLO, DINSPECC " + " WHERE " + " AMOVIFIN.itiposer = 'H' AND " + " AMOVIFIN." + CampoFechaIncluir + ">= " + Serrores.FormatFechaHMS(tempRefParam) + " AND " + " AMOVIFIN." + CampoFechaIncluir + " <= " + Serrores.FormatFechaHMS(tempRefParam2) + " AND " + " SCONSGLO.gconsglo = 'SEGURSOC' AND " + " AMOVIFIN.gsocieda = SCONSGLO.nnumeri1 AND " + " AMOVIFIN.ginspecc = DINSPECC.ginspecc AND ( ");
			stFechafin = Convert.ToString(tempRefParam2);
			stFechaini = Convert.ToString(tempRefParam);

			bool tIndicador = true;
			stInspeccion = "Inspecci�n: ";
			for (int iIn = 0; iIn <= LsbLista2.Items.Count - 1; iIn++)
			{ // Bucle por la lista
				if (iIn < LsbLista2.Items.Count - 1)
				{
					stAmovifin.Append(" DINSPECC.dinspecc='" + LsbLista2.Items[iIn].Text + "' or ");
					if (tIndicador)
					{
						stInspeccion = stInspeccion + " " + LsbLista2.Items[iIn].Text;
						tIndicador = false;
					}
					else
					{
						stInspeccion = stInspeccion + ", " + LsbLista2.Items[iIn].Text;
					}
				}
				else if (iIn == LsbLista2.Items.Count - 1)
				{ 
					stAmovifin.Append(" DINSPECC.dinspecc='" + LsbLista2.Items[iIn].Text + "' )");
					//stInspeccion = stInspeccion & ", " & LsbLista2.List(iIn)
					if (tIndicador)
					{
						stInspeccion = stInspeccion + " " + LsbLista2.Items[iIn].Text;
						tIndicador = false;
					}
					else
					{
						stInspeccion = stInspeccion + ", " + LsbLista2.Items[iIn].Text;
					}
				}
			}

			if (this.ChBHosDia.CheckState == CheckState.Checked && this.ChBHosGeneral.CheckState == CheckState.Unchecked)
			{
				stAmovifin.Append(" and AMOVIFIN.ganoregi < 1900");
			}
			if (this.ChBHosDia.CheckState == CheckState.Unchecked && this.ChBHosGeneral.CheckState == CheckState.Checked)
			{
				stAmovifin.Append(" and AMOVIFIN.ganoregi > 1900");
			}

			stAmovifin.Append(" ORDER BY ganoregi asc, gnumregi asc ");
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stAmovifin.ToString(), Conexion_Base_datos.RcAdmision);
			DataSet RrAMOVIFIN = new DataSet();
			tempAdapter.Fill(RrAMOVIFIN);
            int i = 0;
			//        RrAMOVIFIN.MoveFirst
			foreach (DataRow iteration_row in RrAMOVIFIN.Tables[0].Rows)
			{
                i++;
                // Recorremos la tabla de amovifin con los que cumplen las condiciones
				// Abrimos un resulset para consultar AEPISADM que dejo
				// abierto para poder sacar datos de AEPISADM y saco la HISTORIA
				//    stAepisadm = " SELECT * FROM AEPISADM, HDOSSIER, HSERARCH " _
				//'        & " WHERE AEPISADM.ganoadme = " & RrAMOVIFIN("ganoregi") & " AND " _
				//'        & " AEPISADM.gnumadme = " & RrAMOVIFIN("gnumregi") & " AND " _
				//'        & " AEPISADM.gidenpac *= HDOSSIER.gidenpac AND " _
				//'        & " HDOSSIER.gserpropiet =* HSERARCH.gserarch AND " _
				//'        & " HSERARCH.icentral = 'S' "
				//       If tbCodiDiagini.Text <> "" And TbCodDiagfin.Text <> "" Then
				//            If UCase(CampoFechaIncluir) = "FMOVIMIE" Then   'ENTRADAS
				//                    stAepisadm = stAepisadm _
				//'                                 & " AND AEPISADM.gdiaging>='" & Trim(tbCodiDiagini.Text) _
				//'                                 & "'  AND AEPISADM.gdiaging<='" & Trim(TbCodDiagfin.Text) & "'"
				//            Else   'SALIDAS
				//                    stAepisadm = stAepisadm _
				//'                                 & " AND AEPISADM.gdiagalt>='" & Trim(tbCodiDiagini.Text) _
				//'                                 & "'  AND AEPISADM.gdiagalt<='" & Trim(TbCodDiagfin.Text) & "'"
				//            End If
				//        End If
				MatrizSql = new object[3];
				MatrizSql[1] = iteration_row["ganoregi"];
				MatrizSql[2] = iteration_row["gnumregi"];
				oTextoSql = new DLLTextosSql.TextosSql();
				string tempRefParam3 = "ADMISION";
				string tempRefParam4 = "AGI4110F1";
				string tempRefParam5 = "ProObtenerDatos";
				short tempRefParam6 = 1;
				stAepisadm = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, MatrizSql, Conexion_Base_datos.RcAdmision));
				oTextoSql = null;
				if (tbCodiDiagini.Text != "" && TbCodDiagfin.Text != "")
				{
					if (CampoFechaIncluir.ToUpper() == "FMOVIMIE")
					{ //ENTRADAS
						MatrizSql = new object[5];
						MatrizSql[1] = iteration_row["ganoregi"];
						MatrizSql[2] = iteration_row["gnumregi"];
						MatrizSql[3] = tbCodiDiagini.Text.Trim();
						MatrizSql[4] = TbCodDiagfin.Text.Trim();
						oTextoSql = new DLLTextosSql.TextosSql();
						string tempRefParam8 = "ADMISION";
						string tempRefParam9 = "AGI4110F1";
						string tempRefParam10 = "ProObtenerDatos";
						short tempRefParam11 = 2;
						stAepisadm = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam8, tempRefParam9, tempRefParam10, tempRefParam11, MatrizSql, Conexion_Base_datos.RcAdmision));
						oTextoSql = null;
					}
					else
					{
						//SALIDAS
						MatrizSql = new object[5];
						MatrizSql[1] = iteration_row["ganoregi"];
						MatrizSql[2] = iteration_row["gnumregi"];
						MatrizSql[3] = tbCodiDiagini.Text.Trim();
						MatrizSql[4] = TbCodDiagfin.Text.Trim();
						oTextoSql = new DLLTextosSql.TextosSql();
						string tempRefParam13 = "ADMISION";
						string tempRefParam14 = "AGI4110F1";
						string tempRefParam15 = "ProObtenerDatos";
						short tempRefParam16 = 3;
						stAepisadm = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam13, tempRefParam14, tempRefParam15, tempRefParam16, MatrizSql, Conexion_Base_datos.RcAdmision));
						oTextoSql = null;
					}
				}

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stAepisadm, Conexion_Base_datos.RcAdmision);
				RrAEPISADM = new DataSet();
				tempAdapter_2.Fill(RrAEPISADM);
				if (RrAEPISADM.Tables[0].Rows.Count > 0)
				{
                    // Creamos UNA FILA en la TABLA TEMPORAL
                    //RsINFORME.AddNew();
                    RsINFORME = DbInforme.Tables[VstNomtabla].NewRow();
                    // Grabo la INSPECCION en la tabla temporal
                    RsINFORME["inspeccion"] = Convert.ToString(iteration_row["dinspecc"]).Trim();
					// Grabo la FECHA en la tabla temporal
					if (CampoFechaIncluir.ToUpper() == "FMOVIMIE")
					{ //ENTRADAS
						RsINFORME["fecha"] = RrAEPISADM.Tables[0].Rows[0]["fllegada"];
					}
					else
					{
						RsINFORME["fecha"] = RrAEPISADM.Tables[0].Rows[0]["faltplan"];
					}

					// Grabo la HISTORIA
					RsINFORME["historia"] = RrAEPISADM.Tables[0].Rows[0]["ghistoria"];
					// Abrimos un resulset para DPACIENT
					//        stDpacient = "SELECT * " _
					//'            & " FROM DPACIENT, DPOBLACI ,DPROVINC " _
					//'            & " WHERE DPACIENT.gidenpac = '" & RrAEPISADM("gidenpac") & "' AND " _
					//'            & " DPACIENT.gpoblaci *= DPOBLACI.gpoblaci AND " _
					//'            & " DPACIENT.gprovinc *= DPROVINC.gprovinc "

					MatrizSql = new object[2];
					MatrizSql[1] = RrAEPISADM.Tables[0].Rows[0]["gidenpac"];
					oTextoSql = new DLLTextosSql.TextosSql();
					string tempRefParam18 = "ADMISION";
					string tempRefParam19 = "AGI4110F1";
					string tempRefParam20 = "ProObtenerDatos";
					short tempRefParam21 = 4;
					stDpacient = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam18, tempRefParam19, tempRefParam20, tempRefParam21, MatrizSql, Conexion_Base_datos.RcAdmision));
					oTextoSql = null;

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stDpacient, Conexion_Base_datos.RcAdmision);
					RrDPACIENT = new DataSet();
					tempAdapter_3.Fill(RrDPACIENT);
					// Grabo NUMERO DE SS, PACIENTE, NACIMIENTO, SEXO, DIRECCION,
					// POBLACION, CODIGO POSTAL, PROVINCIA
					RsINFORME["SS"] = RrDPACIENT.Tables[0].Rows[0]["nafiliac"];
					vstPaciente_D = new StringBuilder(" ");

					if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dnombpac"]))
					{
						vstPaciente_D.Append(Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dnombpac"]).Trim());
					}
					if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dape1pac"]))
					{
						vstPaciente_D.Append(" " + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dape1pac"]).Trim());
					}
					if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dape2pac"]))
					{
						vstPaciente_D.Append(" " + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dape2pac"]).Trim());
					}
					RsINFORME["paciente"] = vstPaciente_D.ToString();
					RsINFORME["nacimiento"] = RrDPACIENT.Tables[0].Rows[0]["fnacipac"];
					RsINFORME["sexo"] = RrDPACIENT.Tables[0].Rows[0]["itipsexo"];
					RsINFORME["direccion"] = Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["ddirepac"]).Trim();
					if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dpoblaci"]))
					{
						RsINFORME["poblacion"] = Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dpoblaci"]).Trim();
					}
					else
					{
						if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["gpoblaci"]))
						{
							RsINFORME["poblacion"] = Conexion_Base_datos.FnPoblacion(Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["gpoblaci"]), Conexion_Base_datos.RcAdmision);
						}
					}
					if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["gpaisres"]))
					{
						if (!Convert.IsDBNull(RsINFORME["poblacion"]))
						{
							RsINFORME["poblacion"] = (((string) RsINFORME["poblacion"]) + " - " + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dpaisres"]));
						}
						else
						{
							RsINFORME["poblacion"] = (((string) RsINFORME["poblacion"]) + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dpaisres"]));
						}
					}
					RsINFORME["codigo"] = RrDPACIENT.Tables[0].Rows[0]["gcodipos"];
					RsINFORME["provincia"] = Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dnomprov"]).Trim();
					RsINFORME["telefono"] = RrDPACIENT.Tables[0].Rows[0]["ntelefo1"];
					RrDPACIENT.Close();

					if (CampoFechaIncluir.ToUpper() == "FMOVIMIE")
					{ //ENTRADAS
						stAmovimie = new StringBuilder(" SELECT * " + " FROM AMOVIMIE " + " WHERE AMOVIMIE.ganoregi =" + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["ganoadme"]) + " AND " + " AMOVIMIE.gnumregi =" + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gnumadme"]) + " AND ");

						object tempRefParam23 = RrAEPISADM.Tables[0].Rows[0]["fllegada"];
						stAmovimie.Append(" AMOVIMIE." + CampoFechaIncluir + "=" + Serrores.FormatFechaHMS(tempRefParam23) + " ");

					}
					else
					{
						// salidas

						// Abrimos una consulta en AMOVIMIE para obtener LA CAMA ORIGEN
						stAmovimie = new StringBuilder(" SELECT max(fmovimie),gplantor,ghabitor,gcamaori " + " FROM AMOVIMIE " + " WHERE AMOVIMIE.ganoregi =" + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["ganoadme"]) + " AND " + " AMOVIMIE.gnumregi =" + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gnumadme"]) + " AND ");

						object tempRefParam24 = iteration_row["fmovimie"];
						stAmovimie.Append(" AMOVIMIE.fmovimie <=" + Serrores.FormatFechaHMS(tempRefParam24) + "");
						stAmovimie.Append(" group by gplantor,ghabitor,gcamaori ORDER BY gplantor,ghabitor,gcamaori");

					}


					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stAmovimie.ToString(), Conexion_Base_datos.RcAdmision);
					RrAMOVIMIE = new DataSet();
					tempAdapter_4.Fill(RrAMOVIMIE);

					if (RrAMOVIMIE.Tables[0].Rows.Count > 0)
					{

						// Grabo la CAMA ORIGEN DEL PACIENTE
						//If UCase(CampoFechaIncluir) = "FMOVIMIE" Then   'ENTRADAS
						RsINFORME["cama"] = (StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantor"], "00") + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitor"], "00") + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamaori"], "00"));
						//Else   ' salidas
						//            RsINFORME["cama") = Format(RrAMOVIMIE("gplantde"), "00") + _
						//Format(RrAMOVIMIE("ghabitde"), "00") + _
						//Format(RrAMOVIMIE("gcamades"), "00")
						//End If
					}
					RrAMOVIMIE.Close();
					// Abrimos una consulta para grabar las descripciones de los
					// codigos de AEPISADM
					if (CampoFechaIncluir.ToUpper() == "FMOVIMIE")
					{ //ENTRADAS
						if (Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["gdiaging"]))
						{
							stDpersona = new StringBuilder(" SELECT * " + " FROM DPERSONA, DMOTINGR " + " WHERE gpersona = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gpersona"]) + " AND " + " gmotingr = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gmotingr"]) + "");
						}
						else
						{
							stDpersona = new StringBuilder(" SELECT * " + " FROM DPERSONA, DDIAGNOS, DMOTINGR " + " WHERE gpersona = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gpersona"]) + " AND " + " gmotingr = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gmotingr"]) + " AND " + " gcodidia = '" + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gdiaging"]) + "'");
						}
					}
					else
					{
						// SALIDAS
						if (Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["gdiagalt"]))
						{
							stDpersona = new StringBuilder(" SELECT * " + " FROM DPERSONA");

							if (!Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["gmotalta"]))
							{
								stDpersona.Append(",DMOTALTA " + " where gpersona = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gperulti"]) + " and gmotalta = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gmotalta"]));
							}
							else
							{

								stDpersona.Append(" WHERE gpersona = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gperulti"]));
							}
						}
						else
						{
							stDpersona = new StringBuilder(" SELECT * " + " FROM DPERSONA, DDIAGNOS, DMOTALTA " + " WHERE gpersona = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gperulti"]) + " AND " + " gmotalta = " + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gmotalta"]) + " AND " + " gcodidia = '" + Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gdiagalt"]) + "'");
						}
					}
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stDpersona.ToString(), Conexion_Base_datos.RcAdmision);
					RrDPERSONA = new DataSet();
					tempAdapter_5.Fill(RrDPERSONA);
					// Grabo MEDICO, MOTIVO INGRESO Y DIAGNOSTICO AL INGRESO
					if (CampoFechaIncluir.ToUpper() == "FMOVIMIE")
					{ //ENTRADAS
						RsINFORME["motivo"] = Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dmotingr"]).Trim();
					}
					else
					{
						if (!Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["gmotalta"]))
						{
							RsINFORME["motivo"] = Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dmotalta"]).Trim();
						}
					}
					if (CampoFechaIncluir.ToUpper() == "FMOVIMIE")
					{ //ENTRADAS
						if (Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["gdiaging"]))
						{
							if (tbCodiDiagini.Text != "" && TbCodDiagfin.Text != "")
							{
								RsINFORME["diagnostico}"] = "";
							}
							else
							{
								RsINFORME["diagnostico"] = RrAEPISADM.Tables[0].Rows[0]["odiaging"];
							}
						}
						else
						{
							RsINFORME["diagnostico"] = Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dnombdia"]).Trim();
						}
					}
					else
					{
						// salidas
						if (Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["gdiagalt"]))
						{
							if (tbCodiDiagini.Text != "" && TbCodDiagfin.Text != "")
							{
								RsINFORME["diagnostico}"] = "";
							}
							else
							{
								RsINFORME["diagnostico"] = RrAEPISADM.Tables[0].Rows[0]["odiagalt"];
							}
						}
						else
						{
							RsINFORME["diagnostico"] = Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dnombdia"]).Trim();
						}
					}

					if (!Convert.IsDBNull(RrDPERSONA.Tables[0].Rows[0]["dnompers"]))
					{
                        //RsINFORME["medico"] = (((string) RsINFORME["medico"]) + Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dnompers"]).Trim());
                        RsINFORME["medico"] = (!Convert.IsDBNull(RsINFORME["medico"]) ? RsINFORME["medico"].ToString() : "") + Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dnompers"]).Trim();
                    }
					if (!Convert.IsDBNull(RrDPERSONA.Tables[0].Rows[0]["dap1pers"]))
					{
						RsINFORME["medico"] = (((string) RsINFORME["medico"]) + " " + Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dap1pers"]).Trim());
					}
					if (!Convert.IsDBNull(RrDPERSONA.Tables[0].Rows[0]["dap2pers"]))
					{
						RsINFORME["medico"] = (((string) RsINFORME["medico"]) + " " + Convert.ToString(RrDPERSONA.Tables[0].Rows[0]["dap2pers"]).Trim());
					}

					//''        RsINFORME["medico") = Trim(RrDPERSONA("dnompers")) + " " + _
					//Trim(RrDPERSONA("dap1pers")) + " " + _
					//Trim(RrDPERSONA("dap2pers"))
					RrDPERSONA.Close();
					RrAEPISADM.Close();
                    //RsINFORME.Update();
                    DbInforme.Tables[VstNomtabla].Rows.Add(RsINFORME);
				}
			}
			RrAMOVIFIN.Close();
            //RsINFORME.Close();
            RsINFORME = null;
            //DbInforme.Close();
			//Wk.Close();            
        }

        private void LsbLista1_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (LsbLista1.Items.Count != 0)
            {
                CbInsUno.Enabled = true;
                CbInsTodo.Enabled = true;
            }
        }
    }
}