using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Cambio_entidad
	{

		#region "Upgrade Support "
		private static Cambio_entidad m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Cambio_entidad DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Cambio_entidad();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprEntidades", "tbHoraCambio", "sdcFecha", "Label19", "Label21", "Frame3", "Label28", "LbPaciente", "Frame2", "cbEliminar", "CbNuevo", "CbCambio", "tbNafilia", "tbEntidadA", "tbEntidadde", "Label5", "Label6", "Label7", "Frame6", "cbAceptar", "cbCancelar", "SprEntidades_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprEntidades;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraCambio;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFecha;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cbEliminar;
		public Telerik.WinControls.UI.RadButton CbNuevo;
		public Telerik.WinControls.UI.RadButton CbCambio;
		public Telerik.WinControls.UI.RadTextBox tbNafilia;
		public Telerik.WinControls.UI.RadTextBox tbEntidadA;
		public Telerik.WinControls.UI.RadTextBox tbEntidadde;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadGroupBox Frame6;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
        
        //	private FarPoint.Win.Spread.SheetView SprEntidades_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cambio_entidad));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.SprEntidades = new UpgradeHelpers.Spread.FpSpread();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.tbHoraCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.sdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.Label21 = new Telerik.WinControls.UI.RadLabel();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Frame6 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbEliminar = new Telerik.WinControls.UI.RadButton();
			this.CbNuevo = new Telerik.WinControls.UI.RadButton();
			this.CbCambio = new Telerik.WinControls.UI.RadButton();
			this.tbNafilia = new Telerik.WinControls.UI.RadTextBox();
			this.tbEntidadA = new Telerik.WinControls.UI.RadTextBox();
			this.tbEntidadde = new Telerik.WinControls.UI.RadTextBox();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Label7 = new Telerik.WinControls.UI.RadLabel();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Frame2.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame6.SuspendLayout();
			this.SuspendLayout();
            // 
            // SprEntidades
            // 
            this.SprEntidades.ReadOnly = true;
			this.SprEntidades.Location = new System.Drawing.Point(6, 72);
			this.SprEntidades.Name = "SprEntidades";
			this.SprEntidades.Size = new System.Drawing.Size(539, 220);
			this.SprEntidades.TabIndex = 20;
            this.SprEntidades.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEntidades_CellClick);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.Frame3);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Controls.Add(this.LbPaciente);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(6, 0);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(538, 67);
			this.Frame2.TabIndex = 11;
			this.Frame2.Visible = true;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.tbHoraCambio);
			this.Frame3.Controls.Add(this.sdcFecha);
			this.Frame3.Controls.Add(this.Label19);
			this.Frame3.Controls.Add(this.Label21);
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(72, 28);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(462, 35);
			this.Frame3.TabIndex = 12;
			this.Frame3.Visible = true;
            // 
            // tbHoraCambio
            // 
            this.tbHoraCambio.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraCambio.AllowPromptAsInput = false;
			this.tbHoraCambio.Location = new System.Drawing.Point(357, 10);
			this.tbHoraCambio.Mask = "99:99";
			this.tbHoraCambio.Name = "tbHoraCambio";
			this.tbHoraCambio.PromptChar = ' ';
			this.tbHoraCambio.Size = new System.Drawing.Size(41, 21);
			this.tbHoraCambio.TabIndex = 2;
			this.tbHoraCambio.Enter += new System.EventHandler(this.tbHoraCambio_Enter);
			this.tbHoraCambio.Leave += new System.EventHandler(this.tbHoraCambio_Leave);
			this.tbHoraCambio.TextChanged += new System.EventHandler(this.tbHoraCambio_TextChanged);
			// 
			// sdcFecha
			// 
			this.sdcFecha.CustomFormat = "dd/MM/yyyy";
            this.sdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcFecha.Location = new System.Drawing.Point(115, 10);
			this.sdcFecha.Name = "sdcFecha";
			this.sdcFecha.Size = new System.Drawing.Size(129, 17);
			this.sdcFecha.TabIndex = 1;
			this.sdcFecha.Leave += new System.EventHandler(this.sdcFecha_Leave);
			// 
			// Label19
			// 
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label19.Location = new System.Drawing.Point(304, 13);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(33, 14);
			this.Label19.TabIndex = 14;
			this.Label19.Text = "Hora:";
			// 
			// Label21
			// 
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label21.Location = new System.Drawing.Point(31, 13);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(75, 17);
			this.Label21.TabIndex = 13;
			this.Label21.Text = "Fecha cambio:";
			// 
			// Label28
			// 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(8, 10);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(55, 17);
			this.Label28.TabIndex = 16;
			this.Label28.Text = "Paciente:";
            // 
            // LbPaciente
            // 
            this.LbPaciente.Enabled = false;
			this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbPaciente.Location = new System.Drawing.Point(72, 10);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(461, 17);
			this.LbPaciente.TabIndex = 15;
			// 
			// Frame6
			// 
			this.Frame6.Controls.Add(this.cbEliminar);
			this.Frame6.Controls.Add(this.CbNuevo);
			this.Frame6.Controls.Add(this.CbCambio);
			this.Frame6.Controls.Add(this.tbNafilia);
			this.Frame6.Controls.Add(this.tbEntidadA);
			this.Frame6.Controls.Add(this.tbEntidadde);
			this.Frame6.Controls.Add(this.Label5);
			this.Frame6.Controls.Add(this.Label6);
			this.Frame6.Controls.Add(this.Label7);
			this.Frame6.Enabled = true;
			this.Frame6.Location = new System.Drawing.Point(0, 297);
			this.Frame6.Name = "Frame6";
			this.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame6.Size = new System.Drawing.Size(545, 80);
			this.Frame6.TabIndex = 7;
			this.Frame6.Text = "Cambio de Entidad";
			this.Frame6.Visible = true;
			// 
			// cbEliminar
			// 
			this.cbEliminar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbEliminar.Location = new System.Drawing.Point(182, 44);
			this.cbEliminar.Name = "cbEliminar";
			this.cbEliminar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbEliminar.Size = new System.Drawing.Size(79, 29);
			this.cbEliminar.TabIndex = 5;
			this.cbEliminar.Text = "&Eliminar";
			this.cbEliminar.Click += new System.EventHandler(this.cbEliminar_Click);
			// 
			// CbNuevo
			// 
			this.CbNuevo.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbNuevo.Location = new System.Drawing.Point(10, 44);
			this.CbNuevo.Name = "CbNuevo";
			this.CbNuevo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbNuevo.Size = new System.Drawing.Size(79, 29);
			this.CbNuevo.TabIndex = 3;
			this.CbNuevo.Text = "&Nuevo";
			this.CbNuevo.Click += new System.EventHandler(this.CbNuevo_Click);
			// 
			// CbCambio
			// 
			this.CbCambio.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCambio.Location = new System.Drawing.Point(96, 44);
			this.CbCambio.Name = "CbCambio";
			this.CbCambio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCambio.Size = new System.Drawing.Size(79, 29);
			this.CbCambio.TabIndex = 4;
			this.CbCambio.Text = "Ca&mbio";
			this.CbCambio.Click += new System.EventHandler(this.CbCambio_Click);
            // 
            // tbNafilia
            // 
            //this.tbNafilia.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbNafilia.Enabled = false;
            this.tbNafilia.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbNafilia.Location = new System.Drawing.Point(379, 52);
			this.tbNafilia.Name = "tbNafilia";
			this.tbNafilia.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbNafilia.Size = new System.Drawing.Size(156, 17);
			this.tbNafilia.TabIndex = 19;
            // 
            // tbEntidadA
            // 
            //this.tbEntidadA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbEntidadA.Enabled = false;
            this.tbEntidadA.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbEntidadA.Location = new System.Drawing.Point(313, 20);
			this.tbEntidadA.Name = "tbEntidadA";
			this.tbEntidadA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbEntidadA.Size = new System.Drawing.Size(222, 17);
			this.tbEntidadA.TabIndex = 18;
            // 
            // tbEntidadde
            // 
            //this.tbEntidadde.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbEntidadde.Enabled = false;
            this.tbEntidadde.Cursor = System.Windows.Forms.Cursors.Default;
			this.tbEntidadde.Location = new System.Drawing.Point(32, 20);
			this.tbEntidadde.Name = "tbEntidadde";
			this.tbEntidadde.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbEntidadde.Size = new System.Drawing.Size(230, 16);
			this.tbEntidadde.TabIndex = 17;
			// 
			// Label5
			// 
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.Location = new System.Drawing.Point(8, 20);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(17, 17);
			this.Label5.TabIndex = 10;
			this.Label5.Text = "De:";
			// 
			// Label6
			// 
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.Location = new System.Drawing.Point(293, 20);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(18, 17);
			this.Label6.TabIndex = 9;
			this.Label6.Text = "A:";
			// 
			// Label7
			// 
			this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label7.Location = new System.Drawing.Point(290, 52);
			this.Label7.Name = "Label7";
			this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label7.Size = new System.Drawing.Size(82, 17);
			this.Label7.TabIndex = 8;
			this.Label7.Text = "N� de afiliaci�n:";
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			this.cbAceptar.Location = new System.Drawing.Point(375, 383);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 6;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(464, 383);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 0;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// Cambio_entidad
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(552, 417);
			this.Controls.Add(this.SprEntidades);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.Frame6);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Cambio_entidad";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Cambio de entidad - AGI140F1";
			this.Activated += new System.EventHandler(this.Cambio_entidad_Activated);
			this.Closed += new System.EventHandler(this.Cambio_entidad_Closed);
			this.Load += new System.EventHandler(this.Cambio_entidad_Load);
			this.Frame2.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame6.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			//this.Show();
		}
		#endregion
	}
}