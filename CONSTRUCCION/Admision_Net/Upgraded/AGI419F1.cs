using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;


namespace ADMISION
{
	public partial class MOVI_PACI_ENTI
		: RadForm
	{
		int IiAnoadme = 0;
		int IiGnumadme = 0;
		string stCodpac = String.Empty;
		public MOVI_PACI_ENTI()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		public void rellena_cabecera2()
		{
			int lcol = 1;
			int lfil = 0;
			Sprcambios.MaxRows = 0;
			Sprcambios.MaxCols = 3;
			Sprcambios.Row = lfil;
			Sprcambios.Col = lcol;
			Sprcambios.Text = "Fecha";
			Sprcambios.SetColWidth(lcol, 1200);
			lcol++;
			Sprcambios.Col = lcol;
			Sprcambios.Text = "Entidad ";
			Sprcambios.SetColWidth(lcol, 3225);
			lcol++;
			Sprcambios.Col = lcol;
			Sprcambios.Text = "N� filiaci�n ";
			Sprcambios.SetColWidth(lcol, 2000);
		}

		public void rellena_datos2(int iAnoadme, int iGnumadme)
		{
			int lcol = 0;
			int lfil = 0;
			string stsqlCambio = "select fmovimie,amovifin.gsocieda,amovifin.nafiliac,dsocieda from " + " amovifin,dsocieda where" + " ganoregi=" + iAnoadme.ToString() + " and gnumregi=" + iGnumadme.ToString() + " and amovifin.gsocieda=dsocieda.gsocieda order by fmovimie";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqlCambio, Conexion_Base_datos.RcAdmision);
			DataSet rRcambio = new DataSet();
			tempAdapter.Fill(rRcambio);
			rellena_cabecera2();
			if (rRcambio.Tables[0].Rows.Count != 0)
			{				
				rRcambio.MoveFirst();
				Sprcambios.MaxRows = rRcambio.Tables[0].Rows.Count;
				foreach (DataRow iteration_row in rRcambio.Tables[0].Rows)
				{
					lfil++;
					Sprcambios.Row = lfil;
					lcol++;
					Sprcambios.Col = lcol;
					Sprcambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy");
					lcol++;
					Sprcambios.Col = lcol;
					Sprcambios.Text = Convert.ToString(iteration_row["dsocieda"]);
					lcol++;
					Sprcambios.Col = lcol;
					Sprcambios.Text = Convert.ToString(iteration_row["nafiliac"]);
					lcol = 0;
				}
			}
            Sprcambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
            Sprcambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
            rRcambio.Close();
		}

		public void rellena_datos1(string stGidenpac)
		{
			int lcol = 0;
			int lfil = 0;
			string stsqlEpisodios = "select aepisadm.fllegada,dservici.dnomserv,ganoadme,gnumadme from " + " aepisadm,dservici where" + " gidenpac='" + stGidenpac + "' and aepisadm.gservici=" + " dservici.gservici order by fllegada";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqlEpisodios, Conexion_Base_datos.RcAdmision);
			DataSet rREpisodios = new DataSet();
			tempAdapter.Fill(rREpisodios);
			rellena_cabecera1();
			if (rREpisodios.Tables[0].Rows.Count != 0)
			{				
				rREpisodios.MoveFirst();
				SprEpisodios.MaxRows = rREpisodios.Tables[0].Rows.Count;
				SprEpisodios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
                SprEpisodios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                foreach (DataRow iteration_row in rREpisodios.Tables[0].Rows)
				{
					lfil++;
					SprEpisodios.Row = lfil;
					lcol++;
					SprEpisodios.Col = lcol;
					SprEpisodios.Text = Convert.ToDateTime(iteration_row["fllegada"]).ToString("dd/MM/yyyy");
					lcol++;
					SprEpisodios.Col = lcol;
					SprEpisodios.Text = Convert.ToString(iteration_row["dnomserv"]);
					lcol++;
					SprEpisodios.Col = lcol;
					SprEpisodios.Text = Conversion.Str(iteration_row["ganoadme"]);
					if (lfil == 1)
					{
						IiAnoadme = Convert.ToInt32(Double.Parse(SprEpisodios.Text));
					}
					lcol++;
					SprEpisodios.Col = lcol;
					SprEpisodios.Text = Convert.ToString(iteration_row["gnumadme"]);
					if (lfil == 1)
					{
						IiGnumadme = Convert.ToInt32(Double.Parse(SprEpisodios.Text));
					}
					lcol = 0;
				}
			}
			
			rREpisodios.Close();
		}

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void MOVI_PACI_ENTI_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				int iencuentra = (Convert.ToString(this.Tag).IndexOf('*') + 1);
				string stcadena = Convert.ToString(this.Tag).Substring(0, Math.Min(iencuentra - 1, Convert.ToString(this.Tag).Length));
				rellena_datos1(stcadena);
				stcadena = Convert.ToString(this.Tag).Substring(iencuentra);
				LbNompac.Text = stcadena;
				if (SprEpisodios.MaxRows != 0)
				{
					rellena_datos2(IiAnoadme, IiGnumadme);
				}
			}
		}
		
		private void MOVI_PACI_ENTI_Load(Object eventSender, EventArgs eventArgs)
		{
			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			LbPaciente.Text = LitPersona;

			rellena_cabecera1();
			rellena_cabecera2();
		}

		public void rellena_cabecera1()
		{
			int lcol = 1;
			int lfil = 0;
			SprEpisodios.MaxRows = 0;
			SprEpisodios.MaxCols = 4;
			SprEpisodios.Row = lfil;
			SprEpisodios.Col = lcol;
			SprEpisodios.Text = "Fecha Ingreso";
			SprEpisodios.SetColWidth(lcol, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 17);
			lcol++;
			SprEpisodios.Col = lcol;
			SprEpisodios.Text = "Servicio de Ingreso";
			SprEpisodios.SetColWidth(lcol, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + 800);
			lcol++;
			SprEpisodios.Col = lcol;
			SprEpisodios.Text = "GANOADME";
			SprEpisodios.SetColHidden(SprEpisodios.Col, true);
			lcol++;
			SprEpisodios.Col = lcol;
			SprEpisodios.Text = "GANOREGI";
			SprEpisodios.SetColHidden(SprEpisodios.Col, true);
		}

		private void SprEpisodios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
			int Col = eventArgs.ColumnIndex+1;
			int Row = eventArgs.RowIndex+1;
			int stAno = 0;
			int stReg = 0;
			SprEpisodios.Col = Col;
			SprEpisodios.Row = Row;
			if (SprEpisodios.Row != 0)
			{
				SprEpisodios.Col = 3;
				stAno = Convert.ToInt32(Double.Parse(SprEpisodios.Text));
				SprEpisodios.Col = 4;
				stReg = Convert.ToInt32(Double.Parse(SprEpisodios.Text));
				rellena_datos2(stAno, stReg);
			}
		}

		private void MOVI_PACI_ENTI_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}