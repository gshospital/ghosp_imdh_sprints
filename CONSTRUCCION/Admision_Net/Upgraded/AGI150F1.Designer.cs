using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Documentos_ingreso
	{

		#region "Upgrade Support "
		private static Documentos_ingreso m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Documentos_ingreso DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Documentos_ingreso();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "chbContratoIngreso", "ChbAutoEmision", "chkAportaPension", "ChbInterJuzga", "chbDuplicidad", "chbAutoDatos", "chbAutoEstudio", "chbPa�ales", "chbContrato", "chbFactSomb", "chbDeposito", "ChbOficio", "chbHojaClinEsta", "chbIntervencion", "ChbJusAcompa", "ChbHaceconstar", "Chbtrabajador", "Chbreligioso", "ChbAusencia", "chbEtiqueta", "chbDatos", "ChbJustificante", "chbIngreso", "chbVolante", "Label1", "lbPaciente", "Frame3", "_Option1_5", "_Option1_4", "_Option1_3", "_Option1_2", "_Option1_1", "_Option1_0", "Frame2", "Frame1", "cbCancelar", "cbAceptar", "Image2", "Image1", "Option1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadCheckBox chbContratoIngreso;
		public Telerik.WinControls.UI.RadCheckBox ChbAutoEmision;
		public Telerik.WinControls.UI.RadCheckBox chkAportaPension;
		public Telerik.WinControls.UI.RadCheckBox ChbInterJuzga;
		public Telerik.WinControls.UI.RadCheckBox chbDuplicidad;
		public Telerik.WinControls.UI.RadCheckBox chbAutoDatos;
		public Telerik.WinControls.UI.RadCheckBox chbAutoEstudio;
		public Telerik.WinControls.UI.RadCheckBox chbPa�ales;
		public Telerik.WinControls.UI.RadCheckBox chbContrato;
		public Telerik.WinControls.UI.RadCheckBox chbFactSomb;
		public Telerik.WinControls.UI.RadCheckBox chbDeposito;
		public Telerik.WinControls.UI.RadCheckBox ChbOficio;
		public Telerik.WinControls.UI.RadCheckBox chbHojaClinEsta;
		public Telerik.WinControls.UI.RadCheckBox chbIntervencion;
		public Telerik.WinControls.UI.RadCheckBox ChbJusAcompa;
		public Telerik.WinControls.UI.RadCheckBox ChbHaceconstar;
		public Telerik.WinControls.UI.RadCheckBox Chbtrabajador;
		public Telerik.WinControls.UI.RadCheckBox Chbreligioso;
		public Telerik.WinControls.UI.RadCheckBox ChbAusencia;
		public Telerik.WinControls.UI.RadCheckBox chbEtiqueta;
		public Telerik.WinControls.UI.RadCheckBox chbDatos;
		public Telerik.WinControls.UI.RadCheckBox ChbJustificante;
		public Telerik.WinControls.UI.RadCheckBox chbIngreso;
		public Telerik.WinControls.UI.RadCheckBox chbVolante;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		private Telerik.WinControls.UI.RadRadioButton _Option1_5;
		private Telerik.WinControls.UI.RadRadioButton _Option1_4;
		private Telerik.WinControls.UI.RadRadioButton _Option1_3;
		private Telerik.WinControls.UI.RadRadioButton _Option1_2;
		private Telerik.WinControls.UI.RadRadioButton _Option1_1;
		private Telerik.WinControls.UI.RadRadioButton _Option1_0;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public System.Windows.Forms.PictureBox Image2;
		public System.Windows.Forms.PictureBox Image1;
		public Telerik.WinControls.UI.RadRadioButton[] Option1 = new Telerik.WinControls.UI.RadRadioButton[6];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.chbContratoIngreso = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbAutoEmision = new Telerik.WinControls.UI.RadCheckBox();
            this.chkAportaPension = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbInterJuzga = new Telerik.WinControls.UI.RadCheckBox();
            this.chbDuplicidad = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoDatos = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoEstudio = new Telerik.WinControls.UI.RadCheckBox();
            this.chbPa�ales = new Telerik.WinControls.UI.RadCheckBox();
            this.chbContrato = new Telerik.WinControls.UI.RadCheckBox();
            this.chbFactSomb = new Telerik.WinControls.UI.RadCheckBox();
            this.chbDeposito = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbOficio = new Telerik.WinControls.UI.RadCheckBox();
            this.chbHojaClinEsta = new Telerik.WinControls.UI.RadCheckBox();
            this.chbIntervencion = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbJusAcompa = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbHaceconstar = new Telerik.WinControls.UI.RadCheckBox();
            this.Chbtrabajador = new Telerik.WinControls.UI.RadCheckBox();
            this.Chbreligioso = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbAusencia = new Telerik.WinControls.UI.RadCheckBox();
            this.chbEtiqueta = new Telerik.WinControls.UI.RadCheckBox();
            this.chbDatos = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbJustificante = new Telerik.WinControls.UI.RadCheckBox();
            this.chbIngreso = new Telerik.WinControls.UI.RadCheckBox();
            this.chbVolante = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this._Option1_5 = new Telerik.WinControls.UI.RadRadioButton();
            this._Option1_4 = new Telerik.WinControls.UI.RadRadioButton();
            this._Option1_3 = new Telerik.WinControls.UI.RadRadioButton();
            this._Option1_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._Option1_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._Option1_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Image2 = new System.Windows.Forms.PictureBox();
            this.Image1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbContratoIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAutoEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAportaPension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbInterJuzga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDuplicidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoEstudio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPa�ales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbContrato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFactSomb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDeposito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbOficio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbHojaClinEsta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIntervencion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJusAcompa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbHaceconstar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chbtrabajador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chbreligioso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAusencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEtiqueta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJustificante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbVolante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.chbContratoIngreso);
            this.Frame1.Controls.Add(this.ChbAutoEmision);
            this.Frame1.Controls.Add(this.chkAportaPension);
            this.Frame1.Controls.Add(this.ChbInterJuzga);
            this.Frame1.Controls.Add(this.chbDuplicidad);
            this.Frame1.Controls.Add(this.chbAutoDatos);
            this.Frame1.Controls.Add(this.chbAutoEstudio);
            this.Frame1.Controls.Add(this.chbPa�ales);
            this.Frame1.Controls.Add(this.chbContrato);
            this.Frame1.Controls.Add(this.chbFactSomb);
            this.Frame1.Controls.Add(this.chbDeposito);
            this.Frame1.Controls.Add(this.ChbOficio);
            this.Frame1.Controls.Add(this.chbHojaClinEsta);
            this.Frame1.Controls.Add(this.chbIntervencion);
            this.Frame1.Controls.Add(this.ChbJusAcompa);
            this.Frame1.Controls.Add(this.ChbHaceconstar);
            this.Frame1.Controls.Add(this.Chbtrabajador);
            this.Frame1.Controls.Add(this.Chbreligioso);
            this.Frame1.Controls.Add(this.ChbAusencia);
            this.Frame1.Controls.Add(this.chbEtiqueta);
            this.Frame1.Controls.Add(this.chbDatos);
            this.Frame1.Controls.Add(this.ChbJustificante);
            this.Frame1.Controls.Add(this.chbIngreso);
            this.Frame1.Controls.Add(this.chbVolante);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(2, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(367, 532);
            this.Frame1.TabIndex = 32;
            // 
            // chbContratoIngreso
            // 
            this.chbContratoIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbContratoIngreso.Location = new System.Drawing.Point(16, 504);
            this.chbContratoIngreso.Name = "chbContratoIngreso";
            this.chbContratoIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbContratoIngreso.Size = new System.Drawing.Size(120, 18);
            this.chbContratoIngreso.TabIndex = 29;
            this.chbContratoIngreso.Text = "Contrato de Ingreso";
            this.chbContratoIngreso.Visible = false;
            this.chbContratoIngreso.CheckStateChanged += new System.EventHandler(this.chbContratoIngreso_CheckStateChanged);
            // 
            // ChbAutoEmision
            // 
            this.ChbAutoEmision.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbAutoEmision.Location = new System.Drawing.Point(16, 272);
            this.ChbAutoEmision.Name = "ChbAutoEmision";
            this.ChbAutoEmision.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbAutoEmision.Size = new System.Drawing.Size(215, 18);
            this.ChbAutoEmision.TabIndex = 17;
            this.ChbAutoEmision.Text = "Autorizaci�n de Emisi�n de Justificante";
            this.ChbAutoEmision.CheckStateChanged += new System.EventHandler(this.ChbAutoEmision_CheckStateChanged);
            // 
            // chkAportaPension
            // 
            this.chkAportaPension.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkAportaPension.Location = new System.Drawing.Point(16, 484);
            this.chkAportaPension.Name = "chkAportaPension";
            this.chkAportaPension.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkAportaPension.Size = new System.Drawing.Size(118, 18);
            this.chkAportaPension.TabIndex = 28;
            this.chkAportaPension.Text = "Aportaci�n Pensi�n";
            this.chkAportaPension.Visible = false;
            this.chkAportaPension.CheckStateChanged += new System.EventHandler(this.chkAportaPension_CheckStateChanged);
            // 
            // ChbInterJuzga
            // 
            this.ChbInterJuzga.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbInterJuzga.Location = new System.Drawing.Point(16, 446);
            this.ChbInterJuzga.Name = "ChbInterJuzga";
            this.ChbInterJuzga.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbInterJuzga.Size = new System.Drawing.Size(269, 18);
            this.ChbInterJuzga.TabIndex = 26;
            this.ChbInterJuzga.Text = "Notificaci�n de ingreso con internamiento judicial";
            this.ChbInterJuzga.Visible = false;
            this.ChbInterJuzga.CheckStateChanged += new System.EventHandler(this.ChbInterJuzga_CheckStateChanged);
            // 
            // chbDuplicidad
            // 
            this.chbDuplicidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbDuplicidad.Location = new System.Drawing.Point(16, 465);
            this.chbDuplicidad.Name = "chbDuplicidad";
            this.chbDuplicidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbDuplicidad.Size = new System.Drawing.Size(225, 18);
            this.chbDuplicidad.TabIndex = 27;
            this.chbDuplicidad.Text = "Notificaci�n de duplicidad de expediente";
            this.chbDuplicidad.Visible = false;
            this.chbDuplicidad.CheckStateChanged += new System.EventHandler(this.chbDuplicidad_CheckStateChanged);
            // 
            // chbAutoDatos
            // 
            this.chbAutoDatos.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbAutoDatos.Location = new System.Drawing.Point(16, 388);
            this.chbAutoDatos.Name = "chbAutoDatos";
            this.chbAutoDatos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbAutoDatos.Size = new System.Drawing.Size(335, 18);
            this.chbAutoDatos.TabIndex = 23;
            this.chbAutoDatos.Text = "Documento de Aceptaci�n de tratamiento de Datos Personales";
            this.chbAutoDatos.Visible = false;
            this.chbAutoDatos.CheckStateChanged += new System.EventHandler(this.chbAutoDatos_CheckStateChanged);
            // 
            // chbAutoEstudio
            // 
            this.chbAutoEstudio.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbAutoEstudio.Location = new System.Drawing.Point(16, 407);
            this.chbAutoEstudio.Name = "chbAutoEstudio";
            this.chbAutoEstudio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbAutoEstudio.Size = new System.Drawing.Size(173, 18);
            this.chbAutoEstudio.TabIndex = 24;
            this.chbAutoEstudio.Text = "Autorizaci�n de Estudio cl�nico";
            this.chbAutoEstudio.Visible = false;
            this.chbAutoEstudio.CheckStateChanged += new System.EventHandler(this.chbAutoEstudio_CheckStateChanged);
            // 
            // chbPa�ales
            // 
            this.chbPa�ales.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbPa�ales.Location = new System.Drawing.Point(16, 427);
            this.chbPa�ales.Name = "chbPa�ales";
            this.chbPa�ales.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbPa�ales.Size = new System.Drawing.Size(230, 18);
            this.chbPa�ales.TabIndex = 25;
            this.chbPa�ales.Text = "Documento de informaci�n sobre pa�ales";
            this.chbPa�ales.Visible = false;
            this.chbPa�ales.CheckStateChanged += new System.EventHandler(this.chbPa�ales_CheckStateChanged);
            // 
            // chbContrato
            // 
            this.chbContrato.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbContrato.Location = new System.Drawing.Point(16, 369);
            this.chbContrato.Name = "chbContrato";
            this.chbContrato.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbContrato.Size = new System.Drawing.Size(129, 18);
            this.chbContrato.TabIndex = 22;
            this.chbContrato.Text = "Contrato de admisi�n";
            this.chbContrato.Visible = false;
            this.chbContrato.CheckStateChanged += new System.EventHandler(this.chbContrato_CheckStateChanged);
            // 
            // chbFactSomb
            // 
            this.chbFactSomb.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFactSomb.Location = new System.Drawing.Point(16, 349);
            this.chbFactSomb.Name = "chbFactSomb";
            this.chbFactSomb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFactSomb.Size = new System.Drawing.Size(99, 18);
            this.chbFactSomb.TabIndex = 21;
            this.chbFactSomb.Text = "Factura Sombra";
            this.chbFactSomb.Visible = false;
            this.chbFactSomb.CheckStateChanged += new System.EventHandler(this.chbFactSomb_CheckStateChanged);
            // 
            // chbDeposito
            // 
            this.chbDeposito.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbDeposito.Location = new System.Drawing.Point(16, 330);
            this.chbDeposito.Name = "chbDeposito";
            this.chbDeposito.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbDeposito.Size = new System.Drawing.Size(52, 18);
            this.chbDeposito.TabIndex = 20;
            this.chbDeposito.Text = "Fianza";
            this.chbDeposito.Visible = false;
            this.chbDeposito.CheckStateChanged += new System.EventHandler(this.chbDeposito_CheckStateChanged);
            // 
            // ChbOficio
            // 
            this.ChbOficio.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbOficio.Location = new System.Drawing.Point(16, 292);
            this.ChbOficio.Name = "ChbOficio";
            this.ChbOficio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbOficio.Size = new System.Drawing.Size(50, 18);
            this.ChbOficio.TabIndex = 18;
            this.ChbOficio.Text = "Oficio";
            this.ChbOficio.Visible = false;
            this.ChbOficio.CheckStateChanged += new System.EventHandler(this.ChbOficio_CheckStateChanged);
            // 
            // chbHojaClinEsta
            // 
            this.chbHojaClinEsta.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbHojaClinEsta.Location = new System.Drawing.Point(16, 311);
            this.chbHojaClinEsta.Name = "chbHojaClinEsta";
            this.chbHojaClinEsta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbHojaClinEsta.Size = new System.Drawing.Size(136, 18);
            this.chbHojaClinEsta.TabIndex = 19;
            this.chbHojaClinEsta.Text = "Hoja Cl�nico-Estad�stica";
            this.chbHojaClinEsta.Visible = false;
            this.chbHojaClinEsta.CheckStateChanged += new System.EventHandler(this.chbHojaClinEsta_CheckStateChanged);
            // 
            // chbIntervencion
            // 
            this.chbIntervencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbIntervencion.Location = new System.Drawing.Point(16, 99);
            this.chbIntervencion.Name = "chbIntervencion";
            this.chbIntervencion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbIntervencion.Size = new System.Drawing.Size(173, 18);
            this.chbIntervencion.TabIndex = 8;
            this.chbIntervencion.Text = "Justificante de Consentimiento";
            this.chbIntervencion.CheckStateChanged += new System.EventHandler(this.chbIntervencion_CheckStateChanged);
            // 
            // ChbJusAcompa
            // 
            this.ChbJusAcompa.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbJusAcompa.Location = new System.Drawing.Point(16, 253);
            this.ChbJusAcompa.Name = "ChbJusAcompa";
            this.ChbJusAcompa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbJusAcompa.Size = new System.Drawing.Size(167, 18);
            this.ChbJusAcompa.TabIndex = 16;
            this.ChbJusAcompa.Text = "Justificante del Acompa�ante";
            this.ChbJusAcompa.CheckStateChanged += new System.EventHandler(this.ChbJusAcompa_CheckStateChanged);
            // 
            // ChbHaceconstar
            // 
            this.ChbHaceconstar.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbHaceconstar.Location = new System.Drawing.Point(16, 195);
            this.ChbHaceconstar.Name = "ChbHaceconstar";
            this.ChbHaceconstar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbHaceconstar.Size = new System.Drawing.Size(129, 18);
            this.ChbHaceconstar.TabIndex = 13;
            this.ChbHaceconstar.Text = "Compromiso de Pago";
            this.ChbHaceconstar.CheckStateChanged += new System.EventHandler(this.ChbHaceconstar_CheckStateChanged);
            // 
            // Chbtrabajador
            // 
            this.Chbtrabajador.Cursor = System.Windows.Forms.Cursors.Default;
            this.Chbtrabajador.Location = new System.Drawing.Point(16, 176);
            this.Chbtrabajador.Name = "Chbtrabajador";
            this.Chbtrabajador.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Chbtrabajador.Size = new System.Drawing.Size(170, 18);
            this.Chbtrabajador.TabIndex = 12;
            this.Chbtrabajador.Text = "Hoja para el Trabajador Social";
            this.Chbtrabajador.CheckStateChanged += new System.EventHandler(this.Chbtrabajador_CheckStateChanged);
            // 
            // Chbreligioso
            // 
            this.Chbreligioso.Cursor = System.Windows.Forms.Cursors.Default;
            this.Chbreligioso.Location = new System.Drawing.Point(16, 157);
            this.Chbreligioso.Name = "Chbreligioso";
            this.Chbreligioso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Chbreligioso.Size = new System.Drawing.Size(171, 18);
            this.Chbreligioso.TabIndex = 11;
            this.Chbreligioso.Text = "Hoja para el Servicio Religioso";
            this.Chbreligioso.CheckStateChanged += new System.EventHandler(this.Chbreligioso_CheckStateChanged);
            // 
            // ChbAusencia
            // 
            this.ChbAusencia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbAusencia.Location = new System.Drawing.Point(16, 215);
            this.ChbAusencia.Name = "ChbAusencia";
            this.ChbAusencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbAusencia.Size = new System.Drawing.Size(177, 18);
            this.ChbAusencia.TabIndex = 14;
            this.ChbAusencia.Text = "Solicitud de Ausencia Temporal";
            this.ChbAusencia.CheckStateChanged += new System.EventHandler(this.ChbAusencia_CheckStateChanged);
            // 
            // chbEtiqueta
            // 
            this.chbEtiqueta.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbEtiqueta.Location = new System.Drawing.Point(16, 60);
            this.chbEtiqueta.Name = "chbEtiqueta";
            this.chbEtiqueta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbEtiqueta.Size = new System.Drawing.Size(66, 18);
            this.chbEtiqueta.TabIndex = 0;
            this.chbEtiqueta.Text = "Etiquetas";
            this.chbEtiqueta.CheckStateChanged += new System.EventHandler(this.chbEtiqueta_CheckStateChanged);
            // 
            // chbDatos
            // 
            this.chbDatos.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbDatos.Location = new System.Drawing.Point(16, 118);
            this.chbDatos.Name = "chbDatos";
            this.chbDatos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbDatos.Size = new System.Drawing.Size(91, 18);
            this.chbDatos.TabIndex = 9;
            this.chbDatos.Text = "Hoja de Datos";
            this.chbDatos.CheckStateChanged += new System.EventHandler(this.chbDatos_CheckStateChanged);
            // 
            // ChbJustificante
            // 
            this.ChbJustificante.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbJustificante.Location = new System.Drawing.Point(16, 234);
            this.ChbJustificante.Name = "ChbJustificante";
            this.ChbJustificante.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbJustificante.Size = new System.Drawing.Size(132, 18);
            this.ChbJustificante.TabIndex = 15;
            this.ChbJustificante.Text = "Justificante de Ingreso";
            this.ChbJustificante.CheckStateChanged += new System.EventHandler(this.ChbJustificante_CheckStateChanged);
            // 
            // chbIngreso
            // 
            this.chbIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbIngreso.Location = new System.Drawing.Point(16, 80);
            this.chbIngreso.Name = "chbIngreso";
            this.chbIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbIngreso.Size = new System.Drawing.Size(100, 18);
            this.chbIngreso.TabIndex = 7;
            this.chbIngreso.Text = "Hoja de Ingreso";
            this.chbIngreso.CheckStateChanged += new System.EventHandler(this.chbIngreso_CheckStateChanged);
            // 
            // chbVolante
            // 
            this.chbVolante.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbVolante.Location = new System.Drawing.Point(16, 138);
            this.chbVolante.Name = "chbVolante";
            this.chbVolante.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbVolante.Size = new System.Drawing.Size(107, 18);
            this.chbVolante.TabIndex = 10;
            this.chbVolante.Text = "Volante Sociedad";
            this.chbVolante.CheckStateChanged += new System.EventHandler(this.chbVolante_CheckStateChanged);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.lbPaciente);
            this.Frame3.HeaderText = "Paciente";
            this.Frame3.Location = new System.Drawing.Point(7, 8);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(345, 45);
            this.Frame3.TabIndex = 34;
            this.Frame3.Text = "Paciente";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(9, 19);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(48, 18);
            this.Label1.TabIndex = 36;
            this.Label1.Text = "Nombre";
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(60, 16);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(278, 19);
            this.lbPaciente.TabIndex = 35;
            this.lbPaciente.Enabled = false;
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this._Option1_5);
            this.Frame2.Controls.Add(this._Option1_4);
            this.Frame2.Controls.Add(this._Option1_3);
            this.Frame2.Controls.Add(this._Option1_2);
            this.Frame2.Controls.Add(this._Option1_1);
            this.Frame2.Controls.Add(this._Option1_0);
            this.Frame2.HeaderText = "Etiquetas";
            this.Frame2.Location = new System.Drawing.Point(256, 55);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(99, 162);
            this.Frame2.TabIndex = 33;
            this.Frame2.Text = "Etiquetas";
            // 
            // _Option1_5
            // 
            this._Option1_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._Option1_5.Location = new System.Drawing.Point(5, 138);
            this._Option1_5.Name = "_Option1_5";
            this._Option1_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Option1_5.Size = new System.Drawing.Size(66, 18);
            this._Option1_5.TabIndex = 6;
            this._Option1_5.Text = "Brazalete";            
            this._Option1_5.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // _Option1_4
            // 
            this._Option1_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._Option1_4.Location = new System.Drawing.Point(5, 115);
            this._Option1_4.Name = "_Option1_4";
            this._Option1_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Option1_4.Size = new System.Drawing.Size(87, 18);
            this._Option1_4.TabIndex = 5;
            this._Option1_4.Text = "Identificaci�n";
            this._Option1_4.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // _Option1_3
            // 
            this._Option1_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._Option1_3.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Option1_3.Location = new System.Drawing.Point(5, 92);
            this._Option1_3.Name = "_Option1_3";
            this._Option1_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Option1_3.Size = new System.Drawing.Size(63, 18);
            this._Option1_3.TabIndex = 4;
            this._Option1_3.Text = "Mas Etq.";
            this._Option1_3.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // _Option1_2
            // 
            this._Option1_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._Option1_2.Location = new System.Drawing.Point(5, 69);
            this._Option1_2.Name = "_Option1_2";
            this._Option1_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Option1_2.Size = new System.Drawing.Size(54, 18);
            this._Option1_2.TabIndex = 3;
            this._Option1_2.Text = "Correo";
            this._Option1_2.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // _Option1_1
            // 
            this._Option1_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._Option1_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Option1_1.Location = new System.Drawing.Point(5, 46);
            this._Option1_1.Name = "_Option1_1";
            this._Option1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Option1_1.Size = new System.Drawing.Size(69, 18);
            this._Option1_1.TabIndex = 2;
            this._Option1_1.Text = "Completo";
            this._Option1_1.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // _Option1_0
            // 
            this._Option1_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._Option1_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Option1_0.Location = new System.Drawing.Point(5, 23);
            this._Option1_0.Name = "_Option1_0";
            this._Option1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Option1_0.Size = new System.Drawing.Size(54, 18);
            this._Option1_0.TabIndex = 1;
            this._Option1_0.Text = "Simple";
            this._Option1_0.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(280, 546);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 31;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(184, 547);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 30;
            this.cbAceptar.Text = "&Imprimir";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // Image2
            // 
            this.Image2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image2.Location = new System.Drawing.Point(64, 551);
            this.Image2.Name = "Image2";
            this.Image2.Size = new System.Drawing.Size(54, 20);
            this.Image2.TabIndex = 33;
            this.Image2.TabStop = false;
            this.Image2.Visible = false;
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Location = new System.Drawing.Point(18, 551);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(32, 18);
            this.Image1.TabIndex = 34;
            this.Image1.TabStop = false;
            this.Image1.Visible = false;
            // 
            // Documentos_ingreso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(381, 587);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.Image2);
            this.Controls.Add(this.Image1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Documentos_ingreso";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Documentos del ingreso - AGI150F1";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.Documentos_ingreso_Activated);
            this.Closed += new System.EventHandler(this.Documentos_ingreso_Closed);
            this.Load += new System.EventHandler(this.Documentos_ingreso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbContratoIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAutoEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAportaPension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbInterJuzga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDuplicidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoEstudio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPa�ales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbContrato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFactSomb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDeposito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbOficio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbHojaClinEsta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIntervencion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJusAcompa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbHaceconstar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chbtrabajador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chbreligioso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAusencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEtiqueta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJustificante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbVolante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Option1_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeOption1();
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			//this.Show();
		}
		void InitializeOption1()
		{
			this.Option1 = new Telerik.WinControls.UI.RadRadioButton[6];
			this.Option1[5] = _Option1_5;
			this.Option1[4] = _Option1_4;
			this.Option1[3] = _Option1_3;
			this.Option1[2] = _Option1_2;
			this.Option1[1] = _Option1_1;
			this.Option1[0] = _Option1_0;
		}
		#endregion
	}
}