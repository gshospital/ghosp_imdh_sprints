using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using CrystalWrapper;

namespace ADMISION
{
    public partial class prevision_alta
         : Telerik.WinControls.UI.RadForm
    {

        public prevision_alta()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
            ReLoadForm(false);
        }


        private void prevision_alta_Activated(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
            {
                UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form)eventSender;
            }
        }
        string vstSqServ = String.Empty;
        string stMensaje1 = String.Empty;
        string stMensaje2 = String.Empty;
        string stMensaje3 = String.Empty;
        string tstSqlReg = String.Empty;
        string tSql1 = String.Empty;
        string tNombreHo = String.Empty, tGrupoHo = String.Empty, tDireccHo = String.Empty;
        string stIntervalo = String.Empty;
        string stSeleccion = String.Empty;
        int viIn = 0;
        string stFechaini = String.Empty;
        string stFechafin = String.Empty;


        int viform = 0;
        int iNumero_Servicios = 0;

        DataSet tRrReg = null;
        DataSet vRrServicio = null;
        DataSet tRr1 = null;

        Crystal CrystalReport1 = new Crystal();

        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {
            DLLTextosSql.TextosSql oTextoSql = null;

            int[] MatrizAux = null;
            try
            {
                // Realizamos las validaciones de las fechas introducidas *******************
                if (SdcFechaIni.Value.Date < DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje2 = "mayor o igual";
                    stMensaje3 = "Fecha del Sistema";

                    short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { Label2.Text, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    SdcFechaIni.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date < DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje2 = "mayor o igual";
                    stMensaje3 = "Fecha del Sistema";

                    short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { Label1.Text, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                    SdcFechaFin.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date < SdcFechaIni.Value.Date)
                {
                    stMensaje2 = "menor o igual";

                    short tempRefParam5 = 1020;
                    string[] tempRefParam6 = new string[] { Label2.Text, stMensaje2, Label1.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                    SdcFechaIni.Focus();
                    return;
                }
                // **************************************************************************
                stIntervalo = "Desde: " + SdcFechaIni.Text + "  Hasta: " + SdcFechaFin.Text;
                if (LsbLista2.Items.Count == -1)
                {
                    stMensaje1 = "un Servicio";

                    short tempRefParam7 = 1270;
                    string[] tempRefParam8 = new string[] { stMensaje1 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
                    return;
                }

                this.Cursor = Cursors.WaitCursor;
                // proFormulas viForm   'limpio las formulas

                //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //Se mueve esta linea aqu� porque se debe definir antes de la asignacion de formulas
                CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi413r1_CR11.rpt";
                //*******************
                CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                
                // Damos formato a las fechas para hacer la consulta *************************
                stFechaini = Convert.ToDateTime(SdcFechaIni.Text).ToString("yyyy-MM-dd") + " " + "00:00:00";
                stFechafin = Convert.ToDateTime(SdcFechaFin.Text).ToString("yyyy-MM-dd") + " " + "23:59:59";
                // ***************************************************************************
                //sql que mandare a Crystal con los servicios elegidos en la lista 2
                //seg�n los elegidos por su itemdata

                // Luis 17/08/2000 cambio sql para acceso a todo tipo de bd
                //    tstSqlReg = " SELECT   * " _
                //'        & " FROM DCAMASBO, AEPISADM,DSERVICI, DTIPOCAM, DUNIENFE, HDOSSIER, HSERARCH " _
                //'        & " WHERE dcamasbo.fprevalt is not null  AND " _
                //'        & " DCAMASBO.fprevalt >=" & FormatFechaHMS(stFechaini) & " and " _
                //'        & " DCAMASBO.fprevalt <=" & FormatFechaHMS(stFechafin) & " and " _
                //'        & " dcamasbo.iestcama = 'O' and " _
                //'        & " AEPISADM.FALTPLAN IS NULL AND " _
                //'        & " DCAMASBO.gidenpac = aepisadm.gidenpac AND " _
                //'        & " DCAMASBO.gtipocam = DTIPOCAM.gtipocam   AND " _
                //'        & " DCAMASBO.gcounien = DUNIENFE.gunidenf and " _
                //'        & " AEPISADM.gserulti = DSERVICI.gservici and " _
                //'        & " DCAMASBO.gidenpac *= HDOSSIER.gidenpac AND " _
                //'        & " HDOSSIER.gserpropiet =* HSERARCH.gserarch and " _
                //'        & " HSERARCH.icentral='S' " _
                //'        & "and ("

                //ListBoxHelper.SetSelectedIndex(LsbLista2, 0);
                LsbLista2.SelectedIndex = 0;
                MatrizAux = new int[1];
                stSeleccion = "Servicio/s seleccionado/s: ";
                MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { LsbLista2.Items.Count });
                for (viIn = 0; viIn <= LsbLista2.Items.Count - 1; viIn++)
                { // Bucle por la lista
                    if (viIn < LsbLista2.Items.Count - 1)
                    {
                        //               tstSqlReg = tstSqlReg & " AEPISADM.gserulti=" & LsbLista2.ItemData(viIn) & " or "
                        //stSeleccion = stSeleccion + LsbLista2.GetListItem(viIn) + "] = ";
                        stSeleccion = stSeleccion + LsbLista2.Items[viIn].Text + "] = ";                        
                        MatrizAux[viIn] = Convert.ToInt32(LsbLista2.Items[viIn].Value);
                    }
                    else if (viIn == LsbLista2.Items.Count - 1)
                    {
                        //            tstSqlReg = tstSqlReg & " AEPISADM.gserulti=" & LsbLista2.ItemData(viIn) & " )"
                        //stSeleccion = stSeleccion + LsbLista2.GetListItem(viIn);
                        stSeleccion = stSeleccion + LsbLista2.Items[viIn].Text + "] = ";                        
                        MatrizAux[viIn] = Convert.ToInt32(LsbLista2.Items[viIn].Value);
                    }
                }


                if (LsbLista2.Items.Count == iNumero_Servicios)
                {
                    stSeleccion = "Servicio/s seleccionado/s: Todos";
                }
                else if (LsbLista2.Items.Count == 1)
                {
                    stSeleccion = stSeleccion;
                }
                else
                {
                    //stSeleccion = Left(Trim$(stSeleccion), Len(stSeleccion) - 2)
                }

                object[] MatrizSql = new object[4];

                object tempRefParam9 = stFechaini;
                MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam9);
                stFechaini = Convert.ToString(tempRefParam9);

                object tempRefParam10 = stFechafin;
                MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam10);
                stFechafin = Convert.ToString(tempRefParam10);

                MatrizSql[3] = MatrizAux;
                oTextoSql = new DLLTextosSql.TextosSql();

                string tempRefParam11 = "ADMISION";
                string tempRefParam12 = "AGI413F1";
                string tempRefParam13 = "proImprimir";
                short tempRefParam14 = 1;
                tstSqlReg = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam11, tempRefParam12, tempRefParam13, tempRefParam14, MatrizSql, Conexion_Base_datos.RcAdmision));
                oTextoSql = null;


                if (this.ChBHosDia.CheckState == CheckState.Checked && this.ChBHosGeneral.CheckState == CheckState.Unchecked)
                {
                    tstSqlReg = tstSqlReg + " and AEPISADM.ganoadme < 1900";
                }
                if (this.ChBHosDia.CheckState == CheckState.Unchecked && this.ChBHosGeneral.CheckState == CheckState.Checked)
                {
                    tstSqlReg = tstSqlReg + " and AEPISADM.ganoadme > 1900";
                }
                tstSqlReg = tstSqlReg + "\r" + "\n";

                //DIEGO 05/06/2006

                if (optCriterioOrdenacion[0].IsChecked)
                {
                    //ORDENADO POR SERVICIO
                    tstSqlReg = tstSqlReg + " order by DSERVICI.dnomserv,AEPISADM.fprevalt    ,DPACIENT.dape1pac , DPACIENT.dape2pac, DPACIENT.dnombpac";
                }
                else
                {
                    //si est� seleccionado el otro...
                    //ORDENADO POR UNIDAD DE ENFEMERIA - CAMA
                    tstSqlReg = tstSqlReg + " order by DUNIENFE.dunidenf, DCAMASBO.gplantas, DCAMASBO.ghabitac, DCAMASBO.gcamasbo,DPACIENT.dape1pac , DPACIENT.dape2pac, DPACIENT.dnombpac";
                }                

                CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
                CrystalReport1.Formulas["{@SELECCION}"] = "\"" + stSeleccion + "\"";
                if (Serrores.ExisteAseguradora(Conexion_Base_datos.RcAdmision))
                {
                    CrystalReport1.Formulas["{@IASEGURA}"] = "'S'";
                }
                else
                {
                    CrystalReport1.Formulas["{@IASEGURA}"] = "'N'";
                }
                ////Se mueve esta linea para arriba porque se debe definir antes de la asignacion de formulas
                //CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi413r1_CR11.rpt";


                //CrystalReport1.ReportFileName = App.Path & "\..\ELEMENTOSCOMUNES\rpt\agi413r1.rpt"
                CrystalReport1.Destination = Crystal.DestinationConstants.crptToWindow;
                CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                CrystalReport1.WindowTitle = "Informe de Previsi�n de Altas";
                CrystalReport1.SQLQuery = tstSqlReg;
                //CrystalReport1.Connect = "DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "" + "";
                CrystalReport1.Destination = Parasalida;
                CrystalReport1.WindowShowPrintSetupBtn = true;
                if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                }
                CrystalReport1.Action = 1;
                //CrystalReport1.PrinterStopPage = CrystalReport1.PageCount();
                this.Cursor = Cursors.Default;
                viform = 5;
                Conexion_Base_datos.vacia_formulas(CrystalReport1, viform);
                CrystalReport1.Reset();
                LsbLista2.Items.Clear();
                proCarga_Servicio();
                CbPantalla.Enabled = false;
                CbImprimir.Enabled = false;
                CbDelTodo.Enabled = false;
                CbDelUno.Enabled = false;
                CbInsTodo.Enabled = true;
                CbInsUno.Enabled = true;
                SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
                SdcFechaFin.Text = SdcFechaIni.Text;

                if (!FrmHospital.Visible)
                {
                    ChBHosDia.CheckState = CheckState.Checked;
                    ChBHosGeneral.CheckState = CheckState.Checked;
                }
                else
                {
                    ChBHosDia.CheckState = CheckState.Unchecked;
                    ChBHosGeneral.CheckState = CheckState.Unchecked;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;                
                if (Information.Err().Number== 32755)
                {
                    //CANCELADA IMRPESION
                }
                else
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "prevision_alta:proimprimir", ex);
                }
            }


        }

        private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
        {

            this.Close();
        }

        private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }

        private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }

        private void ChBHosDia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            if ((((LsbLista2.Items.Count > 0) ? -1 : 0) & (((int)ChBHosDia.CheckState) | ((int)ChBHosGeneral.CheckState))) != 0)
            {
                //habilito los botones
                CbImprimir.Enabled = true;
                CbPantalla.Enabled = true;
            }
            else
            {
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
            }

        }

        private void ChBHosGeneral_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            if ((((LsbLista2.Items.Count > 0) ? -1 : 0) & (((int)ChBHosDia.CheckState) | ((int)ChBHosGeneral.CheckState))) != 0)
            {
                //habilito los botones
                CbImprimir.Enabled = true;
                CbPantalla.Enabled = true;
            }
            else
            {
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
            }

        }

        private void prevision_alta_Load(Object eventSender, EventArgs eventArgs)
        {



            this.Height = 430; //5535
            this.Width = 540;

            bool MostrarHospitales = false;

            string stsqltemp = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='VERCAMAS'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
                {
                    MostrarHospitales = true;
                }
            }

            rrtemp.Close();

            if (!MostrarHospitales)
            {
                this.Height = 322;
                this.Width = 540;
                FrmHospital.Visible = false;
                this.CbImprimir.Top = 264;
                CbPantalla.Top = 264;
                this.CbCerrar.Top = 264;
                this.ChBHosDia.CheckState = CheckState.Checked;
                this.ChBHosGeneral.CheckState = CheckState.Checked;
            }

            //cambiar los literales en el tipo de ingreso
            stsqltemp = "SELECT VALFANU1,VALFANU2 FROM SCONSGLO WHERE GCONSGLO='DETIPING'";
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
            rrtemp = new DataSet();
            tempAdapter_2.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                ChBHosGeneral.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();
                ChBHosDia.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2"]).Trim();
            }

            rrtemp.Close();


            CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;

            //deshabilito los botones de imprimir y aceptar
            CbImprimir.Enabled = false;
            CbPantalla.Enabled = false;

            CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            //CrystalReport1.Connect = Conexion_Base_datos.RcAdmision.ConnectionString;

            //cargamos la lista con los servicios
            proCarga_Servicio();
            CbDelUno.Enabled = false;
            CbDelTodo.Enabled = false;
            // Obtengo el n�mero de servicios que hay
            iNumero_Servicios = LsbLista1.Items.Count;

        }
        private void LsbLista1_DoubleClick(Object eventSender, EventArgs eventArgs)
        {

            CbInsUno_Click(CbInsUno, new EventArgs());
        }
        private void LsbLista2_DoubleClick(Object eventSender, EventArgs eventArgs)
        {

            CbDelUno_Click(CbDelUno, new EventArgs());
        }

        private void CbInsUno_Click(Object eventSender, EventArgs eventArgs)
        {
            //pongo el seleccionado en la segunda lista y le quito de la primera

            if (LsbLista1.SelectedItems.Count != 0)
            {
                LsbLista2.Items.Add(LsbLista1.Items[LsbLista1.SelectedIndex]);
                LsbLista1.Items.Remove(LsbLista1.SelectedItem);
                CbDelUno.Enabled = true;
                CbDelTodo.Enabled = true;
            }
            if ((((LsbLista2.Items.Count > 0) ? -1 : 0) & (((int)ChBHosDia.CheckState) | ((int)ChBHosGeneral.CheckState))) != 0)
            {
                //habilito los botones
                CbImprimir.Enabled = true;
                CbPantalla.Enabled = true;
            }
            else
            {
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
            }
        }

        private void CbInsTodo_Click(Object eventSender, EventArgs eventArgs)
        {
            //quito todos de 1 y los pongo en 2
            //indice de las listas
            if (LsbLista1.Items.Count > 0)
            {
                if (LsbLista1.Items.Count > 0)
                {
                    for (int i = 0; i < LsbLista1.Items.Count; i++)
                    {
                        LsbLista2.Items.Add(LsbLista1.Items[i]);
                    }
                    LsbLista1.Items.Clear();
                }

                LsbLista1.Items.Clear();
                //habilito los botones
                //''    CbImprimir.Enabled = True
                //''    CbPantalla.Enabled = True
                CbInsTodo.Enabled = false;
                CbInsUno.Enabled = false;
                if ((((LsbLista2.Items.Count > 0) ? -1 : 0) & (((int)ChBHosDia.CheckState) | ((int)ChBHosGeneral.CheckState))) != 0)
                {
                    //habilito los botones
                    CbImprimir.Enabled = true;
                    CbPantalla.Enabled = true;
                }
                else
                {
                    CbImprimir.Enabled = false;
                    CbPantalla.Enabled = false;
                }

            }
            CbDelTodo.Enabled = true;
            CbDelUno.Enabled = true;
        }

        private void CbDelTodo_Click(Object eventSender, EventArgs eventArgs)
        {
            //quito todos de 2 y los pongo en 1
            if (LsbLista2.Items.Count > 0)
            {
                for (int i = 0; i < LsbLista2.Items.Count; i++)
                {
                    //LsbLista1.Items.Add(LsbLista2.Items[i].Text);
                    LsbLista1.Items.Add(LsbLista2.Items[i]);
                }
                LsbLista2.Items.Clear();
            }
            LsbLista2.Items.Clear();
            //deshabilito los botones
            CbImprimir.Enabled = false;
            CbPantalla.Enabled = false;
            CbInsTodo.Enabled = true;
            CbInsUno.Enabled = true;
            CbDelTodo.Enabled = false;
            CbDelUno.Enabled = false;

        }

        private void CbDelUno_Click(Object eventSender, EventArgs eventArgs)
        {
            //pongo el deseleccionado de la 2 en la 1 y le quito de 2
            if (LsbLista2.SelectedItems.Count != 0)
            {
                LsbLista1.Items.Add(LsbLista2.Items[LsbLista2.SelectedIndex]);
                LsbLista2.Items.Remove(LsbLista2.SelectedItem);
            }
            if (LsbLista2.Items.Count == 0)
            {
                //deshabilito los botones
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
            }
        }
        public void proFormulas(int idForm)
        {
            int tiForm = 0;
            foreach (var key in CrystalReport1.Formulas.Keys)
            {
                if (tiForm <= idForm)
                    CrystalReport1.Formulas[key] = "\"" + "" + "\"";
                ++tiForm;
            }
        }


        public void proCarga_Servicio()
        {
            SqlCommand tRqServ = new SqlCommand();

            try
            {
                // Procedimiento para cargar la lista de servicios
                LsbLista1.Items.Clear();
                vstSqServ = "select distinct dnomserv,dserviciVA.gservici from dserviciVA,aepisadm where " + " dserviciVA.gservici=aepisadm.gserULTI order by dnomserv";
                tRqServ.Connection = Conexion_Base_datos.RcAdmision;
                tRqServ.CommandText = vstSqServ;
                SqlDataAdapter tempAdapter = new SqlDataAdapter(tRqServ);
                vRrServicio = new DataSet();
                tempAdapter.Fill(vRrServicio);
                foreach (DataRow iteration_row in vRrServicio.Tables[0].Rows)
                {
                    if (!Convert.IsDBNull(iteration_row["dnomserv"]))
                    {
                        LsbLista1.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dnomserv"]).Trim(), Convert.ToInt32(iteration_row["gservici"])));
                    }
                }

                vRrServicio.Close();
                tRqServ = null;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "prevision_alta:proCarga_servicio", ex);
            }
        }
        private void prevision_alta_Closed(Object eventSender, EventArgs eventArgs)
        {
            this.Dispose();
        }
    }
}