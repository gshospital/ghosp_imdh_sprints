using Telerik.WinControls.UI;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Seleccion_episodios
	{

		#region "Upgrade Support "
		private static Seleccion_episodios m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Seleccion_episodios DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Seleccion_episodios();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprEpisodios", "cbCancelar", "cbAceptar", "Label1", "lb_paciente", "Label3", "LB_Hist", "Frame1", "SprEpisodios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread SprEpisodios;
        public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox lb_paciente;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox LB_Hist;
		public Telerik.WinControls.UI.RadGroupBox Frame1;

        //	private FarPoint.Win.Spread.SheetView SprEpisodios_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.SprEpisodios = new UpgradeHelpers.Spread.FpSpread();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lb_paciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.LB_Hist = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.SprEpisodios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprEpisodios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb_paciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Hist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // SprEpisodios
            // 
            this.SprEpisodios.AutoScroll = true;
            this.SprEpisodios.AutoSizeRows = true;
            this.SprEpisodios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.SprEpisodios.Cursor = System.Windows.Forms.Cursors.Default;
            this.SprEpisodios.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.SprEpisodios.ForeColor = System.Drawing.Color.Black;
            this.SprEpisodios.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SprEpisodios.Location = new System.Drawing.Point(0, 50);
            // 
            // 
            // 
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.HeaderText = "Servicio de ingreso";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 171;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.HeaderText = "Fecha de ingreso";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 79;
            gridViewTextBoxColumn2.WrapText = true;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.HeaderText = "Hora de ingreso";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 56;
            gridViewTextBoxColumn3.WrapText = true;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.HeaderText = "Servicio de alta";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 154;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.HeaderText = "Fecha de alta";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 79;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.HeaderText = "Hora de alta";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 52;
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn7.EnableExpressionEditor = false;
            gridViewTextBoxColumn7.HeaderText = "G";
            gridViewTextBoxColumn7.Name = "column11";
            gridViewTextBoxColumn8.EnableExpressionEditor = false;
            gridViewTextBoxColumn8.HeaderText = "H";
            gridViewTextBoxColumn8.Name = "column12";
            gridViewTextBoxColumn9.EnableExpressionEditor = false;
            gridViewTextBoxColumn9.HeaderText = "I";
            gridViewTextBoxColumn9.Name = "column13";
            gridViewTextBoxColumn10.EnableExpressionEditor = false;
            gridViewTextBoxColumn10.HeaderText = "J";
            gridViewTextBoxColumn10.Name = "column14";
            this.SprEpisodios.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.SprEpisodios.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.SprEpisodios.Name = "SprEpisodios";
            this.SprEpisodios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SprEpisodios.Size = new System.Drawing.Size(575, 179);
            this.SprEpisodios.TabIndex = 2;
            this.SprEpisodios.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprEpisodios_KeyDown);
            this.SprEpisodios.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprEpisodios_MouseUp);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(494, 234);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 29);
            this.cbCancelar.TabIndex = 6;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(404, 234);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 5;
            this.cbAceptar.Text = "Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.lb_paciente);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.LB_Hist);
            this.Frame1.HeaderText = "Datos del paciente";
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(573, 47);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Datos del paciente";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(11, 19);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(51, 18);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Paciente:";
            // 
            // lb_paciente
            // 
            this.lb_paciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lb_paciente.Enabled = false;
            this.lb_paciente.Location = new System.Drawing.Point(72, 17);
            this.lb_paciente.Name = "lb_paciente";
            this.lb_paciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb_paciente.Size = new System.Drawing.Size(296, 20);
            this.lb_paciente.TabIndex = 3;
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(390, 19);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(64, 18);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "Hist. cl�nica:";
            // 
            // LB_Hist
            // 
            this.LB_Hist.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Hist.Enabled = false;
            this.LB_Hist.Location = new System.Drawing.Point(463, 17);
            this.LB_Hist.Name = "LB_Hist";
            this.LB_Hist.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Hist.Size = new System.Drawing.Size(84, 20);
            this.LB_Hist.TabIndex = 1;
            // 
            // Seleccion_episodios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 265);
            this.Controls.Add(this.SprEpisodios);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.Name = "Seleccion_episodios";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Seleccion de episodios - AGI585F1";
            this.Closed += new System.EventHandler(this.Seleccion_episodios_Closed);
            this.Load += new System.EventHandler(this.Seleccion_episodios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SprEpisodios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprEpisodios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb_paciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Hist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}