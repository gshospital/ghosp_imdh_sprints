using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ADMISION
{
	public partial class Justificante_asistencia_acompa�ante
		: Telerik.WinControls.UI.RadForm
	{
		public Justificante_asistencia_acompa�ante()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		private void Justificante_asistencia_acompa�ante_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		public Form VentanaDocumento = null;
		//I.S.M. 27-Mar-2000 A�adido para modificar los datos del acompa�ante
		private void cmdAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string sDNI = String.Empty;

			this.Tag = "pepo";

			string stsqltemp = String.Empty;
			DataSet rrtemp = null;
			string dnicedula1 = String.Empty;
			if (!ChbCalificacion.Visible)
			{
				if (txtNombre.Text.Trim().Length == 0)
				{
					short tempRefParam = 1040;
                    string[] tempRefParam2 = new string[] { lblNombre.Text};
					Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2);
					txtNombre.Focus();
					return;
				}
				else if (txtParentesco.Text.Trim().Length == 0)
				{ 
					short tempRefParam3 = 1040;
                    string[] tempRefParam4 = new string[] { lblParentesco.Text };
					Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4);
					txtParentesco.Focus();
					return;
				}
				else if (!Information.IsDate(mskHora.Text))
				{ 
					short tempRefParam5 = 1150;
                    string[] tempRefParam6 = new string[] { lblHora.Text };
					Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6);
					mskHora.Focus();
					return;
				}

                //camaya todo_x_4
				//UPGRADE_WARNING: (2065) Form method VentanaDocumento.ZOrder has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
				//VentanaDocumento.BringToFront();

				stsqltemp = "select valfanu1,valfanu1i1,valfanu1i2 from sconsglo where gconsglo= 'idenpers' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
				rrtemp = new DataSet();
				tempAdapter.Fill(rrtemp);
				if (rrtemp.Tables[0].Rows.Count != 0)
				{
					//oscar
					//DNICEDULA = Trim(Rs("valfanu1"))
					switch(Serrores.CampoIdioma)
					{
						case "IDIOMA0" : 							
							dnicedula1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim() + ""; 
							break;
						case "IDIOMA1" :							
							dnicedula1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]).Trim() + ""; 
							break;
						case "IDIOMA2" :  							
							dnicedula1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i2"]).Trim() + ""; 
							break;
					}
					//--------
				}				
				rrtemp.Close();

				//    VstNomacompa = txtNombre & IIf(Len(txtDNI), " con " & DNICEDULA & ": " & txtDNI, "")
				Conexion_Base_datos.VstNomacompa = txtNombre.Text + ((Strings.Len(txtDNI.Text) != 0) ? " " + Serrores.VarRPT[2] + " " + dnicedula1 + ": " + txtDNI.Text : "");
				Conexion_Base_datos.VstParentescoAcompa = txtParentesco.Text;
				Conexion_Base_datos.VstHoraAcompa = mskHora.Text;
				Conexion_Base_datos.VstFechaAcompa = sdcFechaIni.Value.Date.ToString();
			}
			else
			{
				if (txtNombre.Text.Trim().Length == 0 && txtNombre.Visible)
				{
					short tempRefParam7 = 1040;
                    string[] tempRefParam8 = new string[] { lblNombre.Text};
					Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8);
					txtNombre.Focus();
					return;
				}
				for (int icont = 0; icont <= lstSinConsen.Items.Count - 1; icont++)
				{
					if (lstSinConsen.Items[icont].ToString().Trim().ToUpper() == txtNombre.Text.Trim().ToUpper())
					{						
						short tempRefParam9 = 1125;
                        string[] tempRefParam10 = new string[] { "Los datos del familiar coinciden con alguien a " + "\n" + "\r" + "quien no se ha autorizado la impresi�n del justificante", "con la impresi�n" };
						if (Convert.ToDouble(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10)) != 6)
						{
							this.Close();
							Conexion_Base_datos.BtCancelar = true;
						}
					}
				}				
				VentanaDocumento.BringToFront();
				Conexion_Base_datos.VstNomacompa = txtNombre.Text + ((Strings.Len(txtDNI.Text) == 0) ? "" : "con DNI/NIE: " + txtDNI.Text + "");
				Conexion_Base_datos.VstParentescoAcompa = "";
				Conexion_Base_datos.VstHoraAcompa = "";
				Conexion_Base_datos.VstFechaAcompa = ((int) ChbCalificacion.CheckState).ToString();

			}
			this.Close();
		}
		private void cmdCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
			Conexion_Base_datos.BtCancelar = true;
		}		
		private void Justificante_asistencia_acompa�ante_Load(Object eventSender, EventArgs eventArgs)
		{

			//'If stFORMFILI <> "E" Then
			//'    lblDNI.Caption = "C�dula:"
			//'    If stFORMFILI = "V" Then
			//'        txtDNI.MaxLength = 9  ' en Venezuela tiene 9
			//'    Else
			//'        txtDNI.MaxLength = 15
			//'    End If
			//'Else
			lblDNI.Text = "D.N.I:";
            lblNombre.Text = "Nombre y Apellidos:";
            lblParentesco.Text = "Parentesco:";
            lblHora.Text = "Hora:";
            lblFecha.Text = "Fecha Asistencia:";

            txtDNI.MaxLength = 9;
			//'End If
			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			lblPaciente.Text = LitPersona;

		}
		private void txtDNI_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				//UPGRADE_ISSUE: (1058) Assignment not supported: KeyAscii to a non-positive constant More Information: http://www.vbtonet.com/ewis/ewi1058.aspx
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void txtNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}        
		private void txtParentesco_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void Justificante_asistencia_acompa�ante_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}