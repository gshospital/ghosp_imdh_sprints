using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;
using UpgradeHelpers.Helpers;

namespace ADMISION
{
	public partial class libro_regi
         : Telerik.WinControls.UI.RadForm
    {

		string stIntervalo = String.Empty;
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		string stFechaini = String.Empty;
		string stFechafin = String.Empty;
		int viform = 0;
		string vSqlc = String.Empty; //SQL PARA CRYSTAL

        DataSet Dbt1 = new DataSet();
        Crystal cryInformes = new Crystal();
        
        string vvSTTemporal = String.Empty;
		public libro_regi()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

        public void proImprimir(Crystal.DestinationConstants destino)
        {
            DLLTextosSql.TextosSql oTextoSql = null;
            try
            {
                if (Convert.ToDateTime(SdcFechaIni.Text) > Convert.ToDateTime(SdcFechaFin.Text))
                {
                    // Control del mensaje enviado
                    stMensaje1 = "menor o igual";

                    short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { Label1.Text, stMensaje1, Label2.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    // ***************************
                    SdcFechaIni.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    // Control del mensaje enviado
                    stMensaje1 = "menor o igual";
                    stMensaje2 = "Fecha del Sistema";

                    short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { Label2.Text, stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                    // ***************************
                    SdcFechaFin.Focus();
                    return;
                }
                stIntervalo = "Desde: " + SdcFechaIni.Text + "  Hasta: " + SdcFechaFin.Text;
                //**************Comprueba si tiene datos sobre las cabeceras******************
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //Se mueve esta linea aqu� porque se debe definir antes de la asignacion de formulas
                cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi410r1_CR11.rpt";

                //****************************************************************************
                proFormulas(viform); //limpio las formulas
                stFechaini = SdcFechaIni.Text + " " + "00:00:00";
                stFechafin = SdcFechaFin.Text + " " + "23:59:59";
                //    vSqlc = " SELECT AEPISADM.GANOADME,AEPISADM.GNUMADME,AEPISADM.GDIAGING," _
                //'        & " AEPISADM.ODIAGING,AEPISADM.FLLEGADA,AEPISADM.ITIPINGR,AEPISADM.FALTPLAN," _
                //'        & " AEPISADM.NANOADMS,AEPISADM.NNUMADMS, " _
                //'        & " DPACIENT.NDNINIFP,DPACIENT.ITIPSEXO,DPACIENT.FNACIPAC, " _
                //'        & " DPACIENT.DAPE1PAC,DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC, " _
                //'        & " DPACIENT.DPOBLACI AS POBLACION,DPACIENT.DDIREPAC,DPACIENT.NAFILIAC, " _
                //'        & " DPERSONA.DAP1PERS,DPERSONA.DAP2PERS,DPERSONA.DNOMPERS,DSERVICI.DNOMSERV, " _
                //'        & " DSOCIEDA.DSOCIEDA,DESTACIV.DESTACIV,DPOBLACI.DPOBLACI,DPROVINC.DNOMPROV, " _
                //'        & " DMOTINGR.DMOTINGR,ASITALTA.DSITALTA,DMOTALTA.DMOTALTA,HDOSSIER.GHISTORIA FROM " _
                //'        & " AEPISADM inner join DPACIENT on AEPISADM.gidenpac = DPACIENT.gidenpac " _
                //'        & " left join DMOTINGR on AEPISADM.gmotingr = DMOTINGR.gmotingr " _
                //'        & " left join ASITALTA on AEPISADM.gsitalta = ASITALTA.gsitalta " _
                //'        & " left join DMOTALTA on AEPISADM.gmotalta = DMOTALTA.gmotalta " _
                //'        & " left join DPERSONA on AEPISADM.gperULTI = DPERSONA.gpersona " _
                //'        & " left join DSERVICI on AEPISADM.gserULTI = DSERVICI.gservici " _
                //'        & " left join DSOCIEDA on DPACIENT.gsocieda = DSOCIEDA.gsocieda " _
                //'        & " left join HDOSSIER on AEPISADM.gidenpac = HDOSSIER.gidenpac " _
                //'        & " left join HSERARCH on HSERARCH.gserarch = HDOSSIER.gserpropiet " _
                //'        & " left join DESTACIV on DPACIENT.gestaciv = DESTACIV.gestaciv " _
                //'        & " left join DPROVINC on DPACIENT.gPROVINC = DPROVINC.gPROVINC " _
                //'        & " left join DPOBLACI on DPACIENT.gpoblaci = DPOBLACI.gpoblaci Where " _
                //'        & " AEPISADM.fllegada >=" & FormatFechaHMS(stFechaini) & " and AEPISADM.fllegada <=" & FormatFechaHMS(stFechafin) & " and " _
                //'        & " HSERARCH.icentral = 'S' AND HDOSSIER.icontenido='H'"
                string[] MatrizSql = new string[] { String.Empty, String.Empty, String.Empty };
                object tempRefParam5 = stFechaini;
                MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam5);
                stFechaini = Convert.ToString(tempRefParam5);
                object tempRefParam6 = stFechafin;
                MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam6);
                stFechafin = Convert.ToString(tempRefParam6);
                oTextoSql = new DLLTextosSql.TextosSql();
                string tempRefParam7 = "ADMISION";
                string tempRefParam8 = "AGI410F1";
                string tempRefParam9 = "proImprimir";
                short tempRefParam10 = 1;
                vSqlc = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam7, tempRefParam8, tempRefParam9, tempRefParam10, MatrizSql, Conexion_Base_datos.RcAdmision));
                oTextoSql = null;
                //CREA TEMPORAL CON LOS DIAGNOSTICOS
                this.Cursor = Cursors.WaitCursor;

                vSqlc = vSqlc + " and AEPISADM.ganoadme > 1900"; //para q no salga hospital de dia
                

                proTempo_diagnosticos(vSqlc);

                //'    If stFORMFILI = "V" Then
                //'        ProvinEstado = "ESTADO"
                //'    Else
                Conexion_Base_datos.ProvinEstado = "PROVINCIA";
                //'    End If

                //''    If stFORMFILI <> "E" Then
                //''        DNICEDULA = "C�DULA"  'en may�sculas
                //''    End If
                                
                cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                cryInformes.Formulas["{@RANGO_FECHAS}"] = "\"" + stIntervalo + "\"";
                //DGMORENOG_PENDIENTE CONFIRMACION
                cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + Conexion_Base_datos.DNICEDULA + "\"";
                cryInformes.Formulas["{@TexProvincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";
                //DGMORENOG_PENDIENTE CONFIRMACION

                cryInformes.SQLQuery = "select * from REGISTRO";

                //cryInformes.DataFiles[0] = Conexion_Base_datos.StBaseTemporal;
                cryInformes.DataFiles[0] = Dbt1.Tables[0]; 
                //I.S.M. 27-Nov-2000. Modificado para elegir el criterio de ordenaci�n
                //cryInformes.SortFields(0) = "+{REGISTRO.REGISTRO}"
                if (optEntrada.IsChecked)
                {
                    cryInformes.Formulas["{@ORDEN}"] = "\"Ordenado por N�mero de Entrada\"";
                    //DGMORENOG-SE ESTA ROMPIENDO TRAE SORTFIELDS.COUNT = 0
                    //cryInformes.SortFields[0] = "+{REGISTRO.REGISTRO}";
                }
                else if (optSalida.IsChecked)
                {
                    cryInformes.Formulas["{@ORDEN}"] = "\"Ordenado por N�mero de Salida\"";
                    //DGMORENOG-SE ESTA ROMPIENDO TRAE SORTFIELDS.COUNT = 0
                    //cryInformes.SortFields[0] = "+{REGISTRO.NSALIDA}";                    
                }
                //    PathString = App.Path

                //Se mueve esta linea para arriba porque se debe definir antes de la asignacion de formulas
                //cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\Agi410r1.rpt";

                cryInformes.WindowTitle = "Libro de Registro";
                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                //cryInformes.Connect = "dsn=GHOSP_access"
                //cryInformes.Connect = "dsn=" + Conexion_Base_datos.stNombreDsnACCESS;
                cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                cryInformes.Destination = destino;
                cryInformes.WindowShowPrintSetupBtn = true;
                if (cryInformes.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(cryInformes);
                }
                cryInformes.Action = 1;
                cryInformes.PrinterStopPage = cryInformes.PageCount;
                viform = 5;
                Conexion_Base_datos.vacia_formulas(cryInformes, viform);
                SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
                SdcFechaFin.Text = DateTime.Today.ToString("dd/MM/yyyy");
                this.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                this.Cursor = Cursors.Default;
                if (Information.Err().Number == 32755)
                {
                    //IMPRESION CANCELADA
                }
                else
                {
                    MessageBox.Show(e.Message, Application.ProductName);
                }
            }
        }
        
        public void proTempo_diagnosticos(string Consulta)
        {                        
            object dbLangSpanish = null;
            object DBEngine = null;

            DataSet RrTemp2 = null;

            DataTable Table = new DataTable();
            DataRow rrTabla = null;

            string sqlMov = String.Empty;
            DataSet RrMov = null;
            string stTemp = String.Empty;
            StringBuilder Paciente = new StringBuilder();
            string TIPOINGRESO = String.Empty;
            StringBuilder MEDICO = new StringBuilder();
            string Sexo = String.Empty;

            object FAlta = null; //Fecha de alta
            object FFin = null; //Fecha de fin del periodo

            string tstConexion = "Description=" + "BASE TEMPORAL PARA CUADRO MANDO EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + "";
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion);

            string VstNomtabla = "REGISTRO";
            //SI NO EXISTE LA BASE DE DATOS CREARLA
            bool bExiste_base = false;

            /*Workspace Wk1 = (Workspace)DBEngine.Workspaces(0);
            if (FileSystem.Dir(Conexion_Base_datos.StBaseTemporal, FileAttribute.Normal) == "")
            {
                Dbt1 = (Database)Wk1.CreateDatabase(Conexion_Base_datos.StBaseTemporal, dbLangSpanish);
            }
            else
            {
                //SI EXISTE LA TABLA BORRAR
                Dbt1 = (Database)Wk1.OpenDatabase(Conexion_Base_datos.StBaseTemporal);
                foreach (TableDef dTabla in Dbt1.TableDefs)
                {
                    if (Convert.ToString(dTabla.Name) == VstNomtabla)
                    {
                        bExiste_base = true;
                        break;
                    }
                }
                //si existe borrarla
                if (bExiste_base)
                {
                    Dbt1.Execute("drop table " + VstNomtabla);
                }
            }*/
            //crear tabla
            VstNomtabla = "CR11_AGI410R1";
            //Dbt1.Execute("CREATE TABLE " + VstNomtabla + " (REGISTRO STRING,NHISTORIA TEXT,DNI TEXT," + "FINGRESO TEXT,PACIENTE  TEXT,FNACIMIEN  TEXT,SEXO  TEXT," + "DOMICILIO TEXT,LOCALIDAD TEXT,PROVINCIA TEXT,TIPOINGRE TEXT,MOTIVOINGRE TEXT," + "DIAGNOSTICO TEXT,FALTA TEXT,SITALTA TEXT,MOTALTA TEXT,NSALIDA TEXT,MEDICO TEXT," + "SERVICIO TEXT,NAFILIA TEXT,ENTIDAD TEXT,ESTCIVIL TEXT);");
            Dbt1.CreateTable(VstNomtabla, Conexion_Base_datos.PathString + "\\XSD\\CR11_AGI410R1.xsd");
            SqlDataAdapter tempAdapter = new SqlDataAdapter(Consulta, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                //RELLENAR LA TABLA CON LOS DIAGNSOTICOS                
                //rrTabla = Dbt1.OpenRecordset("SELECT * FROM " + VstNomtabla, 2, 3);                                
                rrtemp.MoveFirst();
                    
                foreach (DataRow iteration_row in rrtemp.Tables[0].Rows)
                {
                    //rrTabla.AddNew();
                    rrTabla = Dbt1.Tables[VstNomtabla].NewRow();
                    rrTabla["REGISTRO"] = (Convert.ToString(iteration_row["GANOADME"]).Trim() + new string('0', 6 - Convert.ToString(iteration_row["GNUMADME"]).Trim().Length) + Convert.ToString(iteration_row["GNUMADME"]).Trim());
                    if (!Convert.IsDBNull(iteration_row["GHISTORIA"]))
                    {
                        rrTabla["NHISTORIA"] = iteration_row["GHISTORIA"];
                    }
                    if (!Convert.IsDBNull(iteration_row["NDNINIFP"]))
                    {
                        rrTabla["DNI"] = iteration_row["NDNINIFP"];
                    }
                    if (!Convert.IsDBNull(iteration_row["FLLEGADA"]))
                    {
                        rrTabla["FINGRESO"] = Convert.ToDateTime(iteration_row["FLLEGADA"]).ToString("dd/MM/yyyy");
                    }
                    Paciente = new StringBuilder(" ");
                    if (!Convert.IsDBNull(iteration_row["DAPE1PAC"]))
                    {
                        Paciente = new StringBuilder(Convert.ToString(iteration_row["DAPE1PAC"]).Trim());
                    }
                    if (!Convert.IsDBNull(iteration_row["DAPE2PAC"]))
                    {
                        Paciente.Append(" " + Convert.ToString(iteration_row["DAPE2PAC"]).Trim());
                    }
                    if (!Convert.IsDBNull(iteration_row["DNOMBPAC"]))
                    {
                        Paciente.Append("," + Convert.ToString(iteration_row["DNOMBPAC"]));
                    }
                    rrTabla["PACIENTE"] = Paciente.ToString();
                    if (!Convert.IsDBNull(iteration_row["FNACIPAC"]))
                    {
                        rrTabla["FNACIMIEN"] = iteration_row["FNACIPAC"];
                    }
                    if (!Convert.IsDBNull(iteration_row["ITIPSEXO"]))
                    {
                        switch (Convert.ToString(iteration_row["ITIPSEXO"]))
                        {
                            case "M":
                                Sexo = "Mujer";
                                break;
                            case "H":
                                Sexo = "Hombre";
                                break;
                            default:
                                Sexo = "Indefinido";
                                break;
                        }
                        rrTabla["SEXO"] = Sexo;
                    }
                    if (!Convert.IsDBNull(iteration_row["DDIREPAC"]))
                    {
                        rrTabla["DOMICILIO"] = iteration_row["DDIREPAC"];
                    }
                    if (!Convert.IsDBNull(iteration_row["POBLACION"]))
                    { //DPACIENT
                        rrTabla["LOCALIDAD"] = iteration_row["POBLACION"];
                    }
                    else
                    {
                        if (!Convert.IsDBNull(iteration_row["DPOBLACI"]))
                        {
                            rrTabla["LOCALIDAD"] = iteration_row["DPOBLACI"];
                        }
                    }
                    if (!Convert.IsDBNull(iteration_row["DPAISRES"]))
                    {
                        if (!Convert.IsDBNull(rrTabla["LOCALIDAD"]))
                        {
                            rrTabla["LOCALIDAD"] = (((string)rrTabla["LOCALIDAD"]) + " - " + (Convert.ToString(iteration_row["DPAISRES"])));
                        }
                        else
                        {
                            rrTabla["LOCALIDAD"] = (((string)rrTabla["LOCALIDAD"]) + (Convert.ToString(iteration_row["DPAISRES"])));
                        }
                    }
                    if (!Convert.IsDBNull(iteration_row["DNOMPROV"]))
                    {
                        rrTabla["PROVINCIA"] = iteration_row["DNOMPROV"];
                    }
                    TIPOINGRESO = " ";
                    if (!Convert.IsDBNull(iteration_row["itipingr"]))
                    {
                        switch (Convert.ToString(iteration_row["itipingr"]))
                        {
                            case "I":
                                TIPOINGRESO = "Inmediato";
                                break;
                            case "P":
                                TIPOINGRESO = "Programado";
                                break;
                            case "U":
                                TIPOINGRESO = "Urgente";
                                break;
                        }
                        rrTabla["TIPOINGRE"] = TIPOINGRESO;
                    }
                    if (!Convert.IsDBNull(iteration_row["dmotingr"]))
                    {
                        rrTabla["MOTIVOINGRE"] = iteration_row["dmotingr"];
                    }

                    //'''''''''''''''''
                    //    Si la fecha de alta es mayor que la de fin del periodo no debe mostrar los siguientes
                    //   datos:

                    System.DateTime TempDate = DateTime.FromOADate(0);
                    FFin = (DateTime.TryParse(stFechafin, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : stFechafin;
                    if (!Convert.IsDBNull(iteration_row["faltplan"]))
                    {
                        FAlta = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy");
                        if (((int)DateAndTime.DateDiff("d", Convert.ToDateTime(FAlta), Convert.ToDateTime(FFin), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) >= 0)
                        {
                            if (!Convert.IsDBNull(iteration_row["faltplan"]))
                            {
                                rrTabla["FALTA"] = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy");
                            }
                            if (!Convert.IsDBNull(iteration_row["dsitalta"]))
                            {
                                rrTabla["SITALTA"] = iteration_row["dsitalta"];
                            }
                            if (!Convert.IsDBNull(iteration_row["dmotalta"]))
                            {
                                rrTabla["MOTALTA"] = iteration_row["dmotalta"];
                            }
                            if (!Convert.IsDBNull(iteration_row["NANOADMS"]) && !Convert.IsDBNull(iteration_row["NNUMADMS"]))
                            {
                                rrTabla["NSALIDA"] = (Convert.ToString(iteration_row["NANOADMS"]).Trim() + new string('0', 6 - Convert.ToString(iteration_row["NNUMADMS"]).Trim().Length) + Convert.ToString(iteration_row["NNUMADMS"]).Trim());
                            }
                        }
                    }
                    //''''''''''''''''

                    MEDICO = new StringBuilder(" ");
                    if (!Convert.IsDBNull(iteration_row["DAP1PERS"]))
                    {
                        MEDICO = new StringBuilder(Convert.ToString(iteration_row["DAP1PERS"]).Trim());
                    }
                    if (!Convert.IsDBNull(iteration_row["DAP2PERS"]))
                    {
                        MEDICO.Append(" " + Convert.ToString(iteration_row["DAP2PERS"]).Trim());
                    }
                    if (!Convert.IsDBNull(iteration_row["DNOMPERS"]))
                    {
                        MEDICO.Append("," + Convert.ToString(iteration_row["DNOMPERS"]));
                    }
                    rrTabla["MEDICO"] = MEDICO.ToString();
                    if (!Convert.IsDBNull(iteration_row["DNOMSERV"]))
                    {
                        rrTabla["SERVICIO"] = iteration_row["DNOMSERV"];
                    }
                    //sacar la sociedad y el n� afiliacion de amovifin
                    object tempRefParam = iteration_row["FLLEGADA"];
                    sqlMov = "select * from amovifin,dsocieda where ganoregi=" + Convert.ToString(iteration_row["GANOADME"]) + " and gnumregi=" + Convert.ToString(iteration_row["GNUMADME"]) + " and itiposer='H' and " + " fmovimie=" + Serrores.FormatFechaHMS(tempRefParam) + " and amovifin.gsocieda=dsocieda.gsocieda";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlMov, Conexion_Base_datos.RcAdmision);
                    RrMov = new DataSet();
                    tempAdapter_2.Fill(RrMov);
                    if (RrMov.Tables[0].Rows.Count > 0)
                    {
                        if (!Convert.IsDBNull(RrMov.Tables[0].Rows[0]["NAFILIAC"]))
                        {
                            rrTabla["NAFILIA"] = RrMov.Tables[0].Rows[0]["NAFILIAC"];
                        }
                        if (!Convert.IsDBNull(RrMov.Tables[0].Rows[0]["DSOCIEDA"]))
                        {
                            rrTabla["ENTIDAD"] = RrMov.Tables[0].Rows[0]["DSOCIEDA"];
                        }
                    }
                    else
                    {
                        rrTabla["NAFILIA}"] = " ";
                        rrTabla["ENTIDAD}"] = " ";
                    }

                    RrMov.Close();
                    if (!Convert.IsDBNull(iteration_row["DESTACIV"]))
                    {
                        rrTabla["ESTCIVIL"] = iteration_row["DESTACIV"];
                    }
                    //If Not IsNull(rrtemp("GDIAGING")) Then
                    if (!Convert.IsDBNull(iteration_row["GDIAGINI"]))
                    {
                        object tempRefParam2 = iteration_row["FLLEGADA"];
                        object tempRefParam3 = iteration_row["FLLEGADA"];
                        stTemp = "SELECT * FROM DDIAGNOS WHERE GCODIDIA='" + Convert.ToString(iteration_row["GDIAGINI"]) + "' AND" + " FINVALDI<=" + Serrores.FormatFechaHMS(tempRefParam2) + " AND (FFIVALDI>=" + Serrores.FormatFechaHMS(tempRefParam3) + " OR FFIVALDI IS NULL)";
                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stTemp, Conexion_Base_datos.RcAdmision);
                        RrTemp2 = new DataSet();
                        tempAdapter_3.Fill(RrTemp2);
                        if (RrTemp2.Tables[0].Rows.Count != 0)
                        {
                            rrTabla["DIAGNOSTICO"] = RrTemp2.Tables[0].Rows[0]["DNOMBDIA"];
                        }

                        RrTemp2.Close();
                    }
                    else
                    {
                        rrTabla["DIAGNOSTICO"] = iteration_row["ODIAGINI"];
                    }
                    //rrTabla.Update();
                    Dbt1.Tables[VstNomtabla].Rows.Add(rrTabla);                    
                }
                //rrTabla.Close();
                rrTabla = null;
            }
            rrtemp.Close();

            //Oscar: 15/07/2002
            //A�adir los episodios anulados
            stTemp = "SELECT VALFANU1,VALFANU2 FROM SCONSGLO WHERE GCONSGLO='IAPROANU'";
            SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stTemp, Conexion_Base_datos.RcAdmision);
            rrtemp = new DataSet();
            tempAdapter_4.Fill(rrtemp);
            bool MostrarAnulados = false;
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "N" && Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU2"]).Trim().ToUpper() == "S")
                {
                    MostrarAnulados = true;
                }
            }
            rrtemp.Close();

            if (MostrarAnulados)
            {

                object tempRefParam4 = SdcFechaIni.Text + " 00:00:00";
                object tempRefParam5 = SdcFechaFin.Text + " 23:59:59";
                stTemp = "select * from depianul " +
                            " Where itiposer='H' and " +
                            " fanulaci >=" + Serrores.FormatFechaHMS(tempRefParam4) + " and " +
                            " fanulaci <=" + Serrores.FormatFechaHMS(tempRefParam5);

                SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stTemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter_5.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    //RELLENAR LA TABLA CON LOS EPISODIOS ANULADOS
                    //rrTabla = Dbt1.OpenRecordset("SELECT * FROM " + VstNomtabla, 2, 3);
                    foreach (DataRow iteration_row_2 in rrtemp.Tables[0].Rows)
                    {
                        //rrTabla.AddNew();
                        rrTabla = Dbt1.Tables[VstNomtabla].NewRow();

                        switch (Convert.ToString(iteration_row_2["IALTAING"]))
                        {
                            case "I":
                                rrTabla["PACIENTE}"] = "EPISODIO INGRESO CANCELADO";
                                rrTabla["REGISTRO"] = (Convert.ToString(iteration_row_2["GANOREGI"]).Trim() + new string('0', 6 - Convert.ToString(iteration_row_2["GNUMREGI"]).Trim().Length) + Convert.ToString(iteration_row_2["GNUMREGI"]).Trim());
                                rrTabla["NSALIDA}"] = "";
                                break;
                            case "A":
                                rrTabla["PACIENTE}"] = "EPISODIO DE ALTA CANCELADO";
                                rrTabla["REGISTRO}"] = "";
                                rrTabla["NSALIDA"] = (Convert.ToString(iteration_row_2["GANOREGI"]).Trim() + new string('0', 6 - Convert.ToString(iteration_row_2["GNUMREGI"]).Trim().Length) + Convert.ToString(iteration_row_2["GNUMREGI"]).Trim());
                                break;
                        }
                        //rrTabla.Update();
                        Dbt1.Tables[VstNomtabla].Rows.Add(rrTabla);
                    }
                    //rrTabla.Close();                    
                    rrTabla = null;
                }
                rrtemp.Close();
            }
            //------------------------------------
            Dbt1.Close();            
            //Wk1.Close();
        }

        private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{                    
            proImprimir(Crystal.DestinationConstants.crptToPrinter);            
        }

        private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{                        
            proImprimir(Crystal.DestinationConstants.crptToWindow);            
        }

        public void proFormulas(int idForm)
		{
            int tiForm = 0;
            string[] MatrizAux = null;
            MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { cryInformes.Formulas.Count });
            if (cryInformes.Formulas.Count > 0)
            {
                foreach (var key in cryInformes.Formulas.Keys)
                {
                    MatrizAux[tiForm] = key;
                    ++tiForm;
                }

                foreach (var form in MatrizAux)
                {
                    cryInformes.Formulas[form] = "";
                }
            }
            /*int tiForm = 0;
            foreach (var key in cryInformes.Formulas.Keys)
            {
                if (tiForm <= idForm)
                    cryInformes.Formulas[key] = "\"" + "" + "\"";

                ++tiForm;
            }*/
        }

        private void libro_regi_Load(Object eventSender, EventArgs eventArgs)
		{            
            cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;            
            this.Height = 220;
			this.Width = 300;
		}

		private void libro_regi_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}