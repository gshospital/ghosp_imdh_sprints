using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class ReservasAnuladas
	{

		#region "Upgrade Support "
		private static ReservasAnuladas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static ReservasAnuladas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new ReservasAnuladas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_chbFecha_1", "sdcDesdeAnulacion", "sdcHastaAnulacion", "lbDesde2", "lbHasta2", "Frame2", "cbImpresora", "cbsalir", "cbPantalla", "_chbFecha_0", "sdcDesde", "sdcHasta", "lbHasta", "lbDesde", "Frame1", "chbFecha"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadCheckBox _chbFecha_1;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDesdeAnulacion;
		public Telerik.WinControls.UI.RadDateTimePicker sdcHastaAnulacion;
		public Telerik.WinControls.UI.RadLabel lbDesde2;
		public Telerik.WinControls.UI.RadLabel lbHasta2;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cbImpresora;
		public Telerik.WinControls.UI.RadButton cbsalir;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		private Telerik.WinControls.UI.RadCheckBox _chbFecha_0;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDesde;
		public Telerik.WinControls.UI.RadDateTimePicker sdcHasta;
		public Telerik.WinControls.UI.RadLabel lbHasta;
		public Telerik.WinControls.UI.RadLabel lbDesde;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadCheckBox[] chbFecha = new Telerik.WinControls.UI.RadCheckBox[2];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReservasAnuladas));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this._chbFecha_1 = new Telerik.WinControls.UI.RadCheckBox();
			this.sdcDesdeAnulacion = new Telerik.WinControls.UI.RadDateTimePicker();
			this.sdcHastaAnulacion = new Telerik.WinControls.UI.RadDateTimePicker();
			this.lbDesde2 = new Telerik.WinControls.UI.RadLabel();
			this.lbHasta2 = new Telerik.WinControls.UI.RadLabel();
			this.cbImpresora = new Telerik.WinControls.UI.RadButton();
			this.cbsalir = new Telerik.WinControls.UI.RadButton();
			this.cbPantalla = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this._chbFecha_0 = new Telerik.WinControls.UI.RadCheckBox();
			this.sdcDesde = new Telerik.WinControls.UI.RadDateTimePicker();
			this.sdcHasta = new Telerik.WinControls.UI.RadDateTimePicker();
			this.lbHasta = new Telerik.WinControls.UI.RadLabel();
			this.lbDesde = new Telerik.WinControls.UI.RadLabel();
			this.Frame2.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame2
			// 
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this._chbFecha_1);
			this.Frame2.Controls.Add(this.sdcDesdeAnulacion);
			this.Frame2.Controls.Add(this.sdcHastaAnulacion);
			this.Frame2.Controls.Add(this.lbDesde2);
			this.Frame2.Controls.Add(this.lbHasta2);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(14, 74);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(392, 55);
			this.Frame2.TabIndex = 12;
			this.Frame2.Text = "       Fecha Anulación";
			this.Frame2.Visible = true;
			// 
			// _chbFecha_1
			// 
			//this._chbFecha_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._chbFecha_1.BackColor = System.Drawing.SystemColors.Control;
			this._chbFecha_1.CausesValidation = true;
			this._chbFecha_1.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._chbFecha_1.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbFecha_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbFecha_1.Enabled = true;
			this._chbFecha_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbFecha_1.Location = new System.Drawing.Point(8, 0);
			this._chbFecha_1.Name = "_chbFecha_1";
			this._chbFecha_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbFecha_1.Size = new System.Drawing.Size(17, 17);
			this._chbFecha_1.TabIndex = 4;
			this._chbFecha_1.TabStop = true;			
			this._chbFecha_1.Visible = true;
			// 
			// sdcDesdeAnulacion
			// 
			//this.sdcDesdeAnulacion.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcDesdeAnulacion.CustomFormat = "dd/MM/yyyy";
			this.sdcDesdeAnulacion.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcDesdeAnulacion.Location = new System.Drawing.Point(72, 22);
			this.sdcDesdeAnulacion.Name = "sdcDesdeAnulacion";
			this.sdcDesdeAnulacion.Size = new System.Drawing.Size(103, 19);
			this.sdcDesdeAnulacion.TabIndex = 5;
            // 
            // sdcHastaAnulacion
            // 
            //this.sdcHastaAnulacion.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.sdcHastaAnulacion.CustomFormat = "dd/MM/yyyy";
			this.sdcHastaAnulacion.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcHastaAnulacion.Location = new System.Drawing.Point(231, 22);
			this.sdcHastaAnulacion.Name = "sdcHastaAnulacion";
			this.sdcHastaAnulacion.Size = new System.Drawing.Size(115, 19);
			this.sdcHastaAnulacion.TabIndex = 6;
            // 
            // lbDesde2
            // 
            //this.lbDesde2.BackColor = System.Drawing.SystemColors.Control;
            //this.lbDesde2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbDesde2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDesde2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDesde2.Location = new System.Drawing.Point(15, 22);
			this.lbDesde2.Name = "lbDesde2";
			this.lbDesde2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDesde2.Size = new System.Drawing.Size(52, 16);
			this.lbDesde2.TabIndex = 14;
			this.lbDesde2.Text = "Desde :";
			// 
			// lbHasta2
			// 
			//this.lbHasta2.BackColor = System.Drawing.SystemColors.Control;
			//this.lbHasta2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbHasta2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbHasta2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbHasta2.Location = new System.Drawing.Point(186, 22);
			this.lbHasta2.Name = "lbHasta2";
			this.lbHasta2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHasta2.Size = new System.Drawing.Size(46, 16);
			this.lbHasta2.TabIndex = 13;
			this.lbHasta2.Text = "Hasta :";
			// 
			// cbImpresora
			// 
			//this.cbImpresora.BackColor = System.Drawing.SystemColors.Control;
			this.cbImpresora.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbImpresora.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbImpresora.Location = new System.Drawing.Point(144, 146);
			this.cbImpresora.Name = "cbImpresora";
			this.cbImpresora.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbImpresora.Size = new System.Drawing.Size(83, 30);
			this.cbImpresora.TabIndex = 7;
			this.cbImpresora.Text = "Imprimir";
            this.cbImpresora.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			this.cbImpresora.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbImpresora.UseVisualStyleBackColor = false;
			this.cbImpresora.Click += new System.EventHandler(this.cbImpresora_Click);
			// 
			// cbsalir
			// 
			//this.cbsalir.BackColor = System.Drawing.SystemColors.Control;
			this.cbsalir.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbsalir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbsalir.Location = new System.Drawing.Point(324, 146);
			this.cbsalir.Name = "cbsalir";
			this.cbsalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbsalir.Size = new System.Drawing.Size(82, 30);
			this.cbsalir.TabIndex = 9;
			this.cbsalir.Text = "Salir";
            this.cbsalir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbsalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbsalir.UseVisualStyleBackColor = false;
			this.cbsalir.Click += new System.EventHandler(this.cbsalir_Click);
			// 
			// cbPantalla
			// 
			//this.cbPantalla.BackColor = System.Drawing.SystemColors.Control;
			this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbPantalla.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbPantalla.Location = new System.Drawing.Point(234, 146);
			this.cbPantalla.Name = "cbPantalla";
			//this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbPantalla.Size = new System.Drawing.Size(83, 30);
			this.cbPantalla.TabIndex = 8;
			this.cbPantalla.Text = "Pantalla";
			this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
			// 
			// Frame1
			// 
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this._chbFecha_0);
			this.Frame1.Controls.Add(this.sdcDesde);
			this.Frame1.Controls.Add(this.sdcHasta);
			this.Frame1.Controls.Add(this.lbHasta);
			this.Frame1.Controls.Add(this.lbDesde);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(14, 12);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(392, 55);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "      Fecha Prevista de ingreso";
			this.Frame1.Visible = true;
			// 
			// _chbFecha_0
			// 
			//this._chbFecha_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._chbFecha_0.BackColor = System.Drawing.SystemColors.Control;
			this._chbFecha_0.CausesValidation = true;
			this._chbFecha_0.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._chbFecha_0.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chbFecha_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._chbFecha_0.Enabled = true;
			//this._chbFecha_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._chbFecha_0.Location = new System.Drawing.Point(10, 0);
			this._chbFecha_0.Name = "_chbFecha_0";
			this._chbFecha_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chbFecha_0.Size = new System.Drawing.Size(17, 17);
			this._chbFecha_0.TabIndex = 1;
			this._chbFecha_0.TabStop = true;			
			this._chbFecha_0.Visible = true;
			// 
			// sdcDesde
			// 
			//this.sdcDesde.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcDesde.CustomFormat = "dd/MM/yyyy";
			this.sdcDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcDesde.Location = new System.Drawing.Point(72, 22);
			this.sdcDesde.Name = "sdcDesde";
			this.sdcDesde.Size = new System.Drawing.Size(103, 19);
			this.sdcDesde.TabIndex = 2;
			// 
			// sdcHasta
			// 
			//this.sdcHasta.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcHasta.CustomFormat = "dd/MM/yyyy";
			this.sdcHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcHasta.Location = new System.Drawing.Point(231, 22);
			this.sdcHasta.Name = "sdcHasta";
			this.sdcHasta.Size = new System.Drawing.Size(115, 19);
			this.sdcHasta.TabIndex = 3;
            // 
            // lbHasta
            // 
            //this.lbHasta.BackColor = System.Drawing.SystemColors.Control;
            //this.lbHasta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbHasta.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbHasta.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbHasta.Location = new System.Drawing.Point(186, 22);
			this.lbHasta.Name = "lbHasta";
			this.lbHasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHasta.Size = new System.Drawing.Size(46, 16);
			this.lbHasta.TabIndex = 11;
			this.lbHasta.Text = "Hasta :";
			// 
			// lbDesde
			// 
			//this.lbDesde.BackColor = System.Drawing.SystemColors.Control;
			//this.lbDesde.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbDesde.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDesde.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDesde.Location = new System.Drawing.Point(15, 22);
			this.lbDesde.Name = "lbDesde";
			this.lbDesde.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDesde.Size = new System.Drawing.Size(52, 16);
			this.lbDesde.TabIndex = 10;
			this.lbDesde.Text = "Desde :";
			// 
			// ReservasAnuladas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(420, 186);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.cbImpresora);
			this.Controls.Add(this.cbsalir);
			this.Controls.Add(this.cbPantalla);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ReservasAnuladas";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Reservas Anuladas- AGR030F1";
			this.Closed += new System.EventHandler(this.ReservasAnuladas_Closed);
			this.Load += new System.EventHandler(this.ReservasAnuladas_Load);
			this.Frame2.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializechbFecha();
		}
		void InitializechbFecha()
		{
			this.chbFecha = new Telerik.WinControls.UI.RadCheckBox[2];
			this.chbFecha[1] = _chbFecha_1;
			this.chbFecha[0] = _chbFecha_0;
		}
		#endregion
	}
}