using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ADMISION
{
	public partial class FiMailing
		 : Telerik.WinControls.UI.RadForm
	{

		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		string stFechaini = String.Empty;
		string stFechafin = String.Empty;
		string vSqlc = String.Empty;
		int Total = 0;
		DataSet Rr1 = null;

		[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
		private struct codiFMailing
		{
			[System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 62)]
			public string stNombre;
			[System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 100)]
			public string stDireccion;
			[System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 40)]
			public string stPoblacion;
			[System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 6)]
			public string stCodigo_Postal;
			[System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 40)]
			public string stProvincia;
			public static codiFMailing CreateInstance()
			{
					codiFMailing result = new codiFMailing();
					result.stNombre =  new string('\0', 62); 
                    result.stDireccion = new string('\0', 100); 
                    result.stPoblacion = new string('\0', 40); 
					result.stCodigo_Postal = new string('\0', 6); 
                    result.stProvincia = new string('\0', 40);
					return result;
			}
		}
		FiMailing.codiFMailing[] AFicheroMailing = null;
		public FiMailing()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}



		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			DLLTextosSql.TextosSql oTextoSql = null;
			try
			{
				this.Cursor = Cursors.WaitCursor;
				if (SdcFechaIni.Value.Date > SdcFechaFin.Value.Date)
				{
					// Control del mensaje enviado
					stMensaje1 = "menor o igual";
					
					short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { Label1.Text, stMensaje1, Label2.Text };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					// ***************************
					SdcFechaIni.Focus();
					this.Cursor = Cursors.Default;
					return;
				}
				if (SdcFechaFin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
				{
					// Control del mensaje enviado
					stMensaje1 = "menor o igual";
					stMensaje2 = "Fecha del Sistema";
					
					short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { Label2.Text, stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
					// ***************************
					SdcFechaFin.Focus();
					this.Cursor = Cursors.Default;
					return;
				}
              
				stFechaini = SdcFechaIni.Value.ToString("dd-MM-yyyy") + " " + "00:00:00";
				stFechafin = SdcFechaFin.Value.ToString("dd-MM-yyyy") + " " + "23:59:59";
				// Luis 17/08/2000 cambio sql para acceso a todo tipo de bd
				//vSqlc = " select distinct dpacient.gidenpac," & _
				//'    " dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, " & _
				//'    " dpacient.ddirepac, dpacient.dpoblaci, " & _
				//'    " dpoblaci.dpoblaci as poblacion, dpacient.gcodipos, " & _
				//'    " dpacient.gcodipos , dprovinc.dnomprov, dpaisres.dpaisres " & _
				//'    " From aepisadm , dpacient, dpoblaci, dprovinc, dpaisres " & _
				//'    " Where " & _
				//'    " dpacient.iaututil='S' and " & _
				//'    " aepisadm.faltplan >= " & FormatFechaHMS(stFechaini) & " and " & _
				//'    " aepisadm.faltplan <= " & FormatFechaHMS(stFechafin) & " and " & _
				//'    " aepisadm.gidenpac = dpacient.gidenpac and " & _
				//'    " dpacient.gpoblaci *= dpoblaci.gpoblaci and " & _
				//'    " dpacient.gprovinc *= dprovinc.gprovinc and " & _
				//'    " dpacient.gpaisres *= dpaisres.gpaisres " & _
				//'    " Order By " & _
				//'    " dpacient.dape1pac , dpacient.dape2pac, dpacient.dnombpac"


				string[] MatrizSql = new string[]{String.Empty, String.Empty, String.Empty};
				
				object tempRefParam5 = stFechaini;
				MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam5);
				stFechaini = Convert.ToString(tempRefParam5);
				
				object tempRefParam6 = stFechafin;
				MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam6);
				stFechafin = Convert.ToString(tempRefParam6);
				oTextoSql = new DLLTextosSql.TextosSql();
				
				string tempRefParam7 = "ADMISION";
				string tempRefParam8 = "AGI440F1";
				string tempRefParam9 = "cbAceptar_Click";
				short tempRefParam10 = 1;
				vSqlc = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam7, tempRefParam8, tempRefParam9, tempRefParam10, MatrizSql, Conexion_Base_datos.RcAdmision));
				oTextoSql = null;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(vSqlc, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				StringBuilder stNombre_completo = new StringBuilder();
				int i = 0;
				if (Rr1.Tables[0].Rows.Count != 0)
				{
					AFicheroMailing = new FiMailing.codiFMailing[Rr1.Tables[0].Rows.Count];
					i = 0;

					while(i < Rr1.Tables[0].Rows.Count) 
					{
						//se mete el nombre completo en una variable
						stNombre_completo = new StringBuilder("");
					
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["dnombpac"]))
						{
							
							stNombre_completo = new StringBuilder(Convert.ToString(Rr1.Tables[0].Rows[i]["dnombpac"]).Trim());
						}
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["dape1pac"]))
						{
						
							stNombre_completo.Append(" " + Convert.ToString(Rr1.Tables[0].Rows[i]["dape1pac"]).Trim());
						}
					
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["dape2pac"]))
						{
							
							stNombre_completo.Append(" " + Convert.ToString(Rr1.Tables[0].Rows[i]["dape2pac"]).Trim());
						}
						// se itroduce el nombre
						if (stNombre_completo.ToString().Trim() != "")
						{
							AFicheroMailing[i].stNombre = stNombre_completo.ToString().Trim();
						}
						// se introduce la direcci�n
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["ddirepac"]))
						{
							
							AFicheroMailing[i].stDireccion = Convert.ToString(Rr1.Tables[0].Rows[i]["ddirepac"]).Trim();
						}
						// se introduce la poblacion si es nacional y si tiene
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["poblacion"]))
						{
							
							AFicheroMailing[i].stPoblacion = Convert.ToString(Rr1.Tables[0].Rows[i]["poblacion"]).Trim();
						}
						// se introduce la poblacion si no es nacional y si tiene
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["dpoblaci"]))
						{
						
							AFicheroMailing[i].stPoblacion = Convert.ToString(Rr1.Tables[0].Rows[i]["dpoblaci"]).Trim();
						}
						// se introduce la provincia si es nacional y si tiene
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["dnomprov"]))
						{
						
							AFicheroMailing[i].stProvincia = Convert.ToString(Rr1.Tables[0].Rows[i]["dnomprov"]).Trim();
						}
						// se introduce el pais si no tiene provincia y si tiene pais
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["dpaisres"]))
						{
							
							AFicheroMailing[i].stProvincia = Convert.ToString(Rr1.Tables[0].Rows[i]["dpaisres"]).Trim();
						}
						// se introduce el codigo postal si tiene
						
						if (!Convert.IsDBNull(Rr1.Tables[0].Rows[i]["gcodipos"]))
						{
						
							AFicheroMailing[i].stCodigo_Postal = Convert.ToString(Rr1.Tables[0].Rows[i]["gcodipos"]).Trim();
						}
					
							i++;
						
					};

					if (Rr1.Tables[0].Rows.Count != 0)
					{
						Total = Rr1.Tables[0].Rows.Count;
					}
					else
					{
						Total = 0;
					}
					grabar_fichero();
				}
				else
				{
					short tempRefParam12 = 1520;
                    string[] tempRefParam13 = new string[] { "filiados", "generar el fichero del mailing" };
					Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam12, Conexion_Base_datos.RcAdmision, tempRefParam13);
				}
				
				Rr1.Close();
				this.Cursor = Cursors.Default;
				//cbCancelar_Click
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "FiMailing:CbAceptar_click", ex);
				this.Cursor = Cursors.Default;
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
            this.Close();
		}

		
		private void FiMailing_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Height = 177;
			this.Width = 273;

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + "s";
			Frame1.Text = LitPersona + " dados de alta";

		}
		private void grabar_fichero()
		{
			
			try
			{
				CommonDialog1Save.DefaultExt = ".txt";
				
				CommonDialog1Save.InitialDirectory = "C:\\";
				
				CommonDialog1Save.Filter = "Archivos de texto (*.txt)|*.txt";
				CommonDialog1Save.FilterIndex = 2;
				CommonDialog1Save.FileName = "Mailing";
				CommonDialog1Save.ShowDialog();
				string StFichero = String.Empty;
				if (CommonDialog1Save.FileName != "")
				{
					FileSystem.FileOpen(1, CommonDialog1Save.FileName, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
					if (Total != 0)
					{
						for (int i = 0; i <= Total - 1; i++)
						{
							StFichero = AFicheroMailing[i].stNombre + 
							            AFicheroMailing[i].stDireccion + 
							            AFicheroMailing[i].stPoblacion + 
							            AFicheroMailing[i].stCodigo_Postal + 
							            AFicheroMailing[i].stProvincia;
							FileSystem.PrintLine(1, StFichero);
						}
					}
					FileSystem.FileClose(1);
				}
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "FiMailing:grabar_fichero", ex);
			}
		}
		private void FiMailing_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}