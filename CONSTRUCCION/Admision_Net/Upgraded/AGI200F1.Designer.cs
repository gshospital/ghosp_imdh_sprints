using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class menu_alta
	{

		#region "Upgrade Support "
		private static menu_alta m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static menu_alta DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new menu_alta();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprPacientes", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "_Toolbar1_Button4", "_Toolbar1_Button5", "_Toolbar1_Button6", "_Toolbar1_Button7", "_Toolbar1_Button8", "_Toolbar1_Button9", "_Toolbar1_Button10", "_Toolbar1_Button11", "_Toolbar1_Button12", "_Toolbar1_Button13", "Toolbar1", "LiNegra", "LbAltaAdmision", "LiRoja", "LbAltaHospi", "Frame3", "cbRefrescar", "Command1", "Label2", "ImageList1", "ShapeContainer1", "SprPacientes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;//System.Windows.Forms.ToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread SprPacientes;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button2;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button4;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button5;
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button6;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button7;
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button8;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button9;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button10;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button11;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button12;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button13;
       
        public Microsoft.VisualBasic.PowerPacks.LineShape LiNegra;
		public Telerik.WinControls.UI.RadLabel LbAltaAdmision;
		public Microsoft.VisualBasic.PowerPacks.LineShape LiRoja;
		public Telerik.WinControls.UI.RadLabel LbAltaHospi;
		public System.Windows.Forms.Panel Frame3;
		public Telerik.WinControls.UI.RadButton cbRefrescar;
		public Telerik.WinControls.UI.RadButton Command1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public System.Windows.Forms.ImageList ImageList1;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;

        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        public Telerik.WinControls.UI.RadCommandBar Toolbar1;
        //private FarPoint.Win.Spread.SheetView SprPacientes_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu_alta));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LiNegra = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LiRoja = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SprPacientes = new UpgradeHelpers.Spread.FpSpread();
            this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button2 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button5 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button7 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button9 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button10 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button11 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button12 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button13 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button4 = new System.Windows.Forms.ToolStripSeparator();
            this._Toolbar1_Button6 = new System.Windows.Forms.ToolStripSeparator();
            this._Toolbar1_Button8 = new System.Windows.Forms.ToolStripSeparator();
            this.Frame3 = new System.Windows.Forms.Panel();
            this.LbAltaAdmision = new Telerik.WinControls.UI.RadLabel();
            this.LbAltaHospi = new Telerik.WinControls.UI.RadLabel();
            this.cbRefrescar = new Telerik.WinControls.UI.RadButton();
            this.Command1 = new Telerik.WinControls.UI.RadButton();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.SprPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprPacientes.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbAltaAdmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbAltaHospi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbRefrescar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LiNegra,
            this.LiRoja});
            this.ShapeContainer1.Size = new System.Drawing.Size(269, 33);
            this.ShapeContainer1.TabIndex = 8;
            this.ShapeContainer1.TabStop = false;
            // 
            // LiNegra
            // 
            this.LiNegra.BorderColor = System.Drawing.SystemColors.WindowText;
            this.LiNegra.BorderWidth = 5;
            this.LiNegra.Enabled = false;
            this.LiNegra.Name = "LiNegra";
            this.LiNegra.X1 = 0;
            this.LiNegra.X2 = 31;
            this.LiNegra.Y1 = 6;
            this.LiNegra.Y2 = 6;
            // 
            // LiRoja
            // 
            this.LiRoja.BorderColor = System.Drawing.Color.Red;
            this.LiRoja.BorderWidth = 5;
            this.LiRoja.Enabled = false;
            this.LiRoja.Name = "LiRoja";
            this.LiRoja.X1 = 0;
            this.LiRoja.X2 = 31;
            this.LiRoja.Y1 = 23;
            this.LiRoja.Y2 = 23;
            // 
            // SprPacientes
            // 
            this.SprPacientes.AutoScroll = true;
            this.SprPacientes.AutoSizeRows = true;
            this.SprPacientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.SprPacientes.Cursor = System.Windows.Forms.Cursors.Default;
            this.SprPacientes.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.SprPacientes.ForeColor = System.Drawing.Color.Black;
            this.SprPacientes.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SprPacientes.Location = new System.Drawing.Point(10, 56);
            // 
            // 
            // 
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.HeaderText = "Cama";
            gridViewTextBoxColumn1.Name = "Col_Paciente";
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.HeaderText = "Primer Apellido, Segundo Apellido, Nombre";
            gridViewTextBoxColumn2.Name = "Col_FecRealiza";
            gridViewTextBoxColumn2.Width = 300;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.HeaderText = "Fecha de alta en planta";
            gridViewTextBoxColumn3.Name = "Col_Cama";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.HeaderText = "N� de episodio";
            gridViewTextBoxColumn4.Name = "Col_IdPaciente";
            gridViewTextBoxColumn4.Width = 100;
            this.SprPacientes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.SprPacientes.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.SprPacientes.Name = "SprPacientes";
            this.SprPacientes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SprPacientes.Size = new System.Drawing.Size(630, 388);
            this.SprPacientes.TabIndex = 2;
            this.SprPacientes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellClick);
            this.SprPacientes.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellDoubleClick);
            this.SprPacientes.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SprPacientes_KeyUp);
            this.SprPacientes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprPacientes_MouseUp);
            // 
            // Toolbar1
            // 
            this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Toolbar1.ImageList = this.ImageList1;
            this.Toolbar1.Location = new System.Drawing.Point(0, 0);
            this.Toolbar1.Name = "Toolbar1";
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.radCommandBarLineElement1});
            this.Toolbar1.Size = new System.Drawing.Size(658, 30);
            this.Toolbar1.TabIndex = 1;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "");
            this.ImageList1.Images.SetKeyName(5, "");
            this.ImageList1.Images.SetKeyName(6, "");
            this.ImageList1.Images.SetKeyName(7, "");
            this.ImageList1.Images.SetKeyName(8, "");
            this.ImageList1.Images.SetKeyName(9, "");
            this.ImageList1.Images.SetKeyName(10, "");
            this.ImageList1.Images.SetKeyName(11, "");
            this.ImageList1.Images.SetKeyName(12, "");
            this.ImageList1.Images.SetKeyName(13, "");
            this.ImageList1.Images.SetKeyName(14, "");
            this.ImageList1.Images.SetKeyName(15, "");
            this.ImageList1.Images.SetKeyName(16, "");
            this.ImageList1.Images.SetKeyName(17, "");
            this.ImageList1.Images.SetKeyName(18, "");
            // 
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this._Toolbar1_Button1,
            this._Toolbar1_Button2,
            this._Toolbar1_Button3,
            this._Toolbar1_Button5,
            this._Toolbar1_Button7,
            this._Toolbar1_Button9,
            this._Toolbar1_Button10,
            this._Toolbar1_Button11,
            this._Toolbar1_Button12,
            this._Toolbar1_Button13});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
            // 
            // 
            // 
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarStripElement1.StretchHorizontally = true;
            this.radCommandBarStripElement1.StretchVertically = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.radCommandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // _Toolbar1_Button1
            // 
            this._Toolbar1_Button1.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button1.Image")));
            this._Toolbar1_Button1.ImageIndex = 0;
            this._Toolbar1_Button1.Name = "_Toolbar1_Button1";
            this._Toolbar1_Button1.Tag = "";
            this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button1.ToolTipText = "Alta del paciente";
            this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button2
            // 
            this._Toolbar1_Button2.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button2.Image")));
            this._Toolbar1_Button2.ImageIndex = 1;
            this._Toolbar1_Button2.Name = "_Toolbar1_Button2";
            this._Toolbar1_Button2.Tag = "";
            this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button2.ToolTipText = "Documentos del Alta";
            this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button3
            // 
            this._Toolbar1_Button3.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button3.Image")));
            this._Toolbar1_Button3.ImageIndex = 2;
            this._Toolbar1_Button3.Name = "_Toolbar1_Button3";
            this._Toolbar1_Button3.Tag = "";
            this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button3.ToolTipText = "Anulaci�n del Alta";
            this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button5
            // 
            this._Toolbar1_Button5.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button5.Image")));
            this._Toolbar1_Button5.ImageIndex = 4;
            this._Toolbar1_Button5.Name = "_Toolbar1_Button5";
            this._Toolbar1_Button5.Tag = "";
            this._Toolbar1_Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button5.ToolTipText = "Consumos Por Paciente";
            this._Toolbar1_Button5.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button7
            // 
            this._Toolbar1_Button7.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button7.Image")));
            this._Toolbar1_Button7.ImageIndex = 6;
            this._Toolbar1_Button7.Name = "_Toolbar1_Button7";
            this._Toolbar1_Button7.Tag = "";
            this._Toolbar1_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button7.ToolTipText = "Anular Alta Admin.";
            this._Toolbar1_Button7.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button9
            // 
            this._Toolbar1_Button9.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button9.Image")));
            this._Toolbar1_Button9.ImageIndex = 15;
            this._Toolbar1_Button9.Name = "_Toolbar1_Button9";
            this._Toolbar1_Button9.Tag = "";
            this._Toolbar1_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button9.ToolTipText = "Cambio de Entidad";
            this._Toolbar1_Button9.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button10
            // 
            this._Toolbar1_Button10.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button10.Image")));
            this._Toolbar1_Button10.ImageIndex = 16;
            this._Toolbar1_Button10.Name = "_Toolbar1_Button10";
            this._Toolbar1_Button10.Tag = "";
            this._Toolbar1_Button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button10.ToolTipText = "Cambio de Servicio";
            this._Toolbar1_Button10.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button11
            // 
            this._Toolbar1_Button11.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button11.Image")));
            this._Toolbar1_Button11.ImageIndex = 14;
            this._Toolbar1_Button11.Name = "_Toolbar1_Button11";
            this._Toolbar1_Button11.Tag = "";
            this._Toolbar1_Button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button11.ToolTipText = "Mantenimiento datos Filiaci�n";
            this._Toolbar1_Button11.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button12
            // 
            this._Toolbar1_Button12.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button12.Image")));
            this._Toolbar1_Button12.ImageIndex = 10;
            this._Toolbar1_Button12.Name = "_Toolbar1_Button12";
            this._Toolbar1_Button12.Tag = "";
            this._Toolbar1_Button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button12.ToolTipText = "Listados de Paciente";
            this._Toolbar1_Button12.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button13
            // 
            this._Toolbar1_Button13.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button13.Image")));
            this._Toolbar1_Button13.ImageIndex = 18;
            this._Toolbar1_Button13.Name = "_Toolbar1_Button13";
            this._Toolbar1_Button13.Tag = "";
            this._Toolbar1_Button13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button13.ToolTipText = "IMDH-Open";
            this._Toolbar1_Button13.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button4
            // 
            this._Toolbar1_Button4.Name = "_Toolbar1_Button4";
            this._Toolbar1_Button4.Size = new System.Drawing.Size(6, 22);
            this._Toolbar1_Button4.Tag = "";
            this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button6
            // 
            this._Toolbar1_Button6.Name = "_Toolbar1_Button6";
            this._Toolbar1_Button6.Size = new System.Drawing.Size(6, 22);
            this._Toolbar1_Button6.Tag = "";
            this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button8
            // 
            this._Toolbar1_Button8.Name = "_Toolbar1_Button8";
            this._Toolbar1_Button8.Size = new System.Drawing.Size(6, 22);
            this._Toolbar1_Button8.Tag = "";
            this._Toolbar1_Button8.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.LbAltaAdmision);
            this.Frame3.Controls.Add(this.LbAltaHospi);
            this.Frame3.Controls.Add(this.ShapeContainer1);
            this.Frame3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Frame3.Location = new System.Drawing.Point(12, 452);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(269, 33);
            this.Frame3.TabIndex = 5;
            // 
            // LbAltaAdmision
            // 
            this.LbAltaAdmision.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbAltaAdmision.Location = new System.Drawing.Point(45, 0);
            this.LbAltaAdmision.Name = "LbAltaAdmision";
            this.LbAltaAdmision.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbAltaAdmision.Size = new System.Drawing.Size(213, 18);
            this.LbAltaAdmision.TabIndex = 7;
            this.LbAltaAdmision.Text = "Pacientes pendientes de alta en admisi�n";
            // 
            // LbAltaHospi
            // 
            this.LbAltaHospi.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbAltaHospi.Location = new System.Drawing.Point(45, 16);
            this.LbAltaHospi.Name = "LbAltaHospi";
            this.LbAltaHospi.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbAltaHospi.Size = new System.Drawing.Size(169, 18);
            this.LbAltaHospi.TabIndex = 6;
            this.LbAltaHospi.Text = "Pacientes dados de alta en el d�a";
            // 
            // cbRefrescar
            // 
            this.cbRefrescar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbRefrescar.Location = new System.Drawing.Point(469, 453);
            this.cbRefrescar.Name = "cbRefrescar";
            this.cbRefrescar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbRefrescar.Size = new System.Drawing.Size(81, 29);
            this.cbRefrescar.TabIndex = 4;
            this.cbRefrescar.Text = "&Refrescar";
            this.cbRefrescar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbRefrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbRefrescar.Click += new System.EventHandler(this.cbRefrescar_Click);
            // 
            // Command1
            // 
            this.Command1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Command1.Location = new System.Drawing.Point(559, 452);
            this.Command1.Name = "Command1";
            this.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Command1.Size = new System.Drawing.Size(81, 29);
            this.Command1.TabIndex = 0;
            this.Command1.Text = "&Cerrar";
            this.Command1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Command1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = false;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Label2.Location = new System.Drawing.Point(10, 35);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(620, 22);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "PACIENTES PENDIENTES DE CONFIRMAR EL ALTA";
            this.Label2.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menu_alta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(658, 487);
            this.Controls.Add(this.SprPacientes);
            this.Controls.Add(this.Toolbar1);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.cbRefrescar);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.Label2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(1637, 172);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "menu_alta";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "GESTI�N DE ALTAS - AGI200F1";
            this.Activated += new System.EventHandler(this.menu_alta_Activated);
            this.Closed += new System.EventHandler(this.menu_alta_Closed);
            this.Load += new System.EventHandler(this.menu_alta_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Menu_alta_PreviewKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.SprPacientes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbAltaAdmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbAltaHospi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbRefrescar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}