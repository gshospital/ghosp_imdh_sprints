using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using System.Reflection;
using CrystalWrapper;

namespace ADMISION
{
    public partial class menu_reservas
        : Telerik.WinControls.UI.RadForm
    {
        int NUMEROMAXIMOFILAS = 0;
        const int NUMEROMAXIMOFILASENPANTALLA = 7;
        static readonly UpgradeHelpers.Spread.SortByConstants SS_SORT_BY_ROW = UpgradeHelpers.Spread.SortByConstants.SortByRow;
        const int SS_SORT_BY_COL = 1;
        const int SS_SORT_ORDER_ASCENDING = 1;
        static readonly UpgradeHelpers.Spread.SortKeyOrderConstants SS_SORT_ORDER_DESCENDING = UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending;

        bool flagDbClick = false;

        const int Col_Paciente = 1;
        const int Col_FecRealiza = 2;
        const int Col_Cama = 3;
        const int Col_IdPaciente = 4;
        const int Col_FecIngreso = 5;
        const int Col_Edad = 6;
        const int Col_Financia = 7;
        const int Col_ServiIngre = 8; //13
        const int Col_UnidadEnf = 9; //8
        const int Col_Fianza = 10; //9
        const int Col_FecInterven = 11; //10
        const int Col_DiasEstanci = 12; //11
        const int Col_Prioridad = 13; //12
                                      //Const Col_ServiIngre = '13
        const int Col_TipoIngre = 14;
        const int Col_Ambito = 15;
        const int Col_CentOrigen = 16;
        const int Col_SerSolici = 17;
        const int Col_MedSolici = 18;
        const int Col_Usuario = 19;
        const int Col_Observa = 20;
        const int Col_IDReserva = 21;

        const int Col_OrdenFecRealiza = 22;
        const int Col_OrdenFecIngreso = 23;
        const int Col_OrdenFecInterven = 24;
        const int Col_HospitalDia = 25;
        const int Col_gservici = 26;
        const int Col_gpersona = 27;
        const int Col_gsersoli = 28;
        const int Col_gpersoli = 29;
        const int Col_ganourge = 30;
        const int Col_gnumurge = 31;

        UpgradeHelpers.Spread.SortKeyOrderConstants ORDEN_PACIENTE = (UpgradeHelpers.Spread.SortKeyOrderConstants)0; // ORDENACION ASCENDENTE O DESCENDENTE  DE LA COLUMNA PACIENTE =1 o 2
        UpgradeHelpers.Spread.SortKeyOrderConstants ORDEN_CAMA = (UpgradeHelpers.Spread.SortKeyOrderConstants)0; // ORDENACION ASCENDENTE O DESCENDENTE  DE LA COLUMNA CAMA =1 o 2

        private SqlCommand _RqReservas = null;

        public menu_reservas()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            isInitializingComponent = true;
            InitializeComponent();
            isInitializingComponent = false;
            ReLoadForm(false);            
        }
        private void Menu_alta_PreviewKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
            int hotKey = IMDHOPEN.TeclaFuncionOpen(Conexion_Base_datos.RcAdmision);
            if (e.KeyCode == (Keys)Enum.Parse(typeof(Keys), hotKey.ToString()))
            {
                if (SprPacientes.ActiveRowIndex > 0)
                {
                    SprPacientes.Row = SprPacientes.ActiveRowIndex;
                    string Episodio = sValorColumna(SprPacientes, Col_IDReserva);
                    string Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                    string ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                    Episodio = "R" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                    string Paciente = sValorColumna(SprPacientes, Col_IdPaciente);

                    string tempRefParam = this.Name;
                    string tempRefParam2 = "sprPacientes";
                    bool tempRefParam3 = true;
                    IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, tempRefParam, tempRefParam2, Paciente, Episodio, tempRefParam3, 0, 0);
                    IMDHOPEN = null;
                }
            }
        }
        SqlCommand RqReservas
        {
            get
            {
                if (_RqReservas == null)
                {
                    _RqReservas = new SqlCommand();
                }
                return _RqReservas;
            }
            set
            {
                _RqReservas = value;
            }
        }
        // establecer la query inicial para cargar todos
        DataSet RrCargarGrid = null;
        string sqlCargarGrid = String.Empty;
        bool bCargarMas = false;
        int iTope = 0;
        int UltFilaSeleccionada = 0;
        bool bRegistroSeleccionado = false;

        //Dim stUltimoRegistro As String  'guardo el �ltimo registro recuperado en la requery
        string stUltimoRegistroNom = String.Empty;
        string stUltimoRegistroAP1 = String.Empty;
        string stUltimoRegistroAP2 = String.Empty;
        internal ClaseReservasAdmision _ObjReservaAdmision = null;

        internal ClaseReservasAdmision ObjReservaAdmision
        {
            get
            {
                if (_ObjReservaAdmision == null)
                {
                    _ObjReservaAdmision = new ClaseReservasAdmision();
                }
                return _ObjReservaAdmision;
            }
            set
            {
                _ObjReservaAdmision = value;
            }
        }
        //  OBJECTO REFERENTE A LA RESERVA ACTUAL

        filiacionDLL.Filiacion Clase_Filiacion = null;
        string stCodPacE = String.Empty; //gidenpac del paciente devuelto por filiacion

        bool hacer_click = false;
        bool hacer_change = false;

        public string VstOperacionReserva = String.Empty;
        public string VstNombreFiliacion = String.Empty;

        bool bSort2 = false, bSort1 = false, bSort3 = false; //variables para ordenaci�n grid

        //oscar 10/03/2004
        string vstSeparadorDecimal = String.Empty;
        public string gUltimoPaciente = String.Empty;
        //-------------

        Crystal Crystal2 = null;
        Mensajes.ClassMensajes ClassErrores = null;

        public int TECLAFUNCION = 0;
        
        /*void InfReservas.Recoger_datos(string Nombre, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Identificador, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen, string Inspec)
        {
            Recoger_datos(Nombre, Apellido1, Apellido2, fechaNac, Sexo, Domicilio, NIF, Identificador, CodPostal, Asegurado, codPob, Historia, CodSOC, Filprov, IndActPen, Inspec);
        }*/
        public void Recoger_datos(string Nombre, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Identificador, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen = "", string Inspec = "")
        {
            // recogida de datos de filiacion
            //Public Sub Recoger_datos(nombre As String, Apellido1 As String, Apellido2 As String, FechaNacto As String, Sexo As String,        Historia As String,        Identificador As String)

            if (Identificador.Trim() != "")
            {
                ObjReservaAdmision.IdentificadorPaciente = Identificador.Trim();
                VstNombreFiliacion = (Apellido1.Trim() + " " + Apellido2.Trim() + ", " + Nombre.Trim()).ToUpper();
                //OSCAR C ENE 2005
                stCodPacE = Identificador.Trim();
                ObjReservaAdmision.gsocieda = CodSOC;
                ObjReservaAdmision.ginspecc = Inspec;
                //---------------
            }
            else
            {
                ObjReservaAdmision.IdentificadorPaciente = "";
                VstNombreFiliacion = "";
                //OSCAR C ENE 2005
                stCodPacE = "";
                ObjReservaAdmision.gsocieda = "";
                ObjReservaAdmision.ginspecc = "";
                //---------------
            }
        }

        private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void CbEntidad_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
        {
            proEstablecerConsultaInicial();
            SprPacientes.MaxRows = 0;
            proCargarGrid();
            SprPacientes.Update();
        }

        private void cmbIngreso_ClickEvent(Object eventSender, EventArgs eventArgs)
        {
            string stServicio = String.Empty;
            string stMedico = String.Empty;
            string stgAnoUrge = String.Empty;
            string stgNumUrge = String.Empty;
            DataSet rdoTemp = null;
            string stSql = String.Empty;

            SprPacientes.Col = Col_IdPaciente;
            string stPaciente = SprPacientes.Text;

            if (stPaciente.Trim() != "")
            {
                //compruebo si el paciente esta en urgencias
                stSql = "Select count(*) from uepisurg where gidenpac = '" + stPaciente.Trim() + "' and faltaurg is null";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);

                if (Convert.ToDouble(rdoTemp.Tables[0].Rows[0][0]) > 0)
                {
                    RadMessageBox.Show("Paciente ingresado en urgencias", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }

                rdoTemp.Close();
                //compruebo si el paciente esta en admision
                stSql = "Select count(*) from aepisadm where gidenpac = '" + stPaciente.Trim() + "' and faltplan is null";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rdoTemp = new DataSet();
                tempAdapter_2.Fill(rdoTemp);

                if (Convert.ToDouble(rdoTemp.Tables[0].Rows[0][0]) > 0)
                {
                    RadMessageBox.Show("Paciente ingresado en planta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }

                rdoTemp.Close();
            }
            //llamo a la ingreso.dll

            //DGMORENOG TODO_7_3
            //object mcIngreso = new IngresoDLL.Ingreso_clase();

            //SprPacientes.Col = Col_TipoIngre
            SprPacientes.Col = Col_gnumurge;
            //si no es una reserva asociada a la urgencia
            if (SprPacientes.Text != "")
            {
                SprPacientes.Col = Col_ganourge;
                stgAnoUrge = SprPacientes.Text;
                SprPacientes.Col = Col_gnumurge;
                stgNumUrge = SprPacientes.Text;
                SprPacientes.Col = Col_gservici;
                stServicio = SprPacientes.Text;
                SprPacientes.Col = Col_gpersona;
                stMedico = SprPacientes.Text;

                stSql = "select iactpens,ginspecc,gidenpac, gdiagnos, odiagnos from uepisurg where ganourge = " + stgAnoUrge +
                        " and gnumurge = " + stgNumUrge;
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rdoTemp = new DataSet();
                tempAdapter_3.Fill(rdoTemp);
                if (rdoTemp.Tables[0].Rows.Count == 1)
                {
                    // Llamada a la DLL de ingreso en Admision
                    // modificacion al 19/11/1998 -> pasar el a�o y el numero de entrada en urgencias para
                    // tener una relaci�n con todo lo que se le ha hecho en urgencias desde planta					
                    if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["IACTPENS"]))
                    {
                        if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["Ginspecc"]))
                        {
                            //DGMORENOG TODO_7_3
                            //mcIngreso.proCargaIngreso(this, mcIngreso, rdoTemp.Tables[0].Rows[0]["gidenpac"], "NA", rdoTemp.Tables[0].Rows[0]["IACTPENS"], rdoTemp.Tables[0].Rows[0]["Ginspecc"], "U", (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gdiagnos"]) + "").Trim(), (Convert.ToString(rdoTemp.Tables[0].Rows[0]["odiagnos"]) + "").Trim(), Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, stgAnoUrge, stgNumUrge, stServicio, stMedico, true, gUltimoPaciente);
                        }
                        else
                        {
                            //DGMORENOG TODO_7_3					
                            //mcIngreso.proCargaIngreso(this, mcIngreso, rdoTemp.Tables[0].Rows[0]["gidenpac"], "NA", rdoTemp.Tables[0].Rows[0]["IACTPENS"], "", "U", (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gdiagnos"]) + "").Trim(), (Convert.ToString(rdoTemp.Tables[0].Rows[0]["odiagnos"]) + "").Trim(), Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, stgAnoUrge, stgNumUrge, stServicio, stMedico, true, gUltimoPaciente);
                        }
                    }
                    else
                    {
                        if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["Ginspecc"]))
                        {
                            //DGMORENOG TODO_7_3
                            //mcIngreso.proCargaIngreso(this, mcIngreso, rdoTemp.Tables[0].Rows[0]["gidenpac"], "NA", "", rdoTemp.Tables[0].Rows[0]["Ginspecc"], "U", (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gdiagnos"]) + "").Trim(), (Convert.ToString(rdoTemp.Tables[0].Rows[0]["odiagnos"]) + "").Trim(), Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, stgAnoUrge, stgNumUrge, stServicio, stMedico, true, gUltimoPaciente);
                        }
                        else
                        {
                            //DGMORENOG TODO_7_3
                            //mcIngreso.proCargaIngreso(this, mcIngreso, rdoTemp.Tables[0].Rows[0]["gidenpac"], "NA", "", "", "U", (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gdiagnos"]) + "").Trim(), (Convert.ToString(rdoTemp.Tables[0].Rows[0]["odiagnos"]) + "").Trim(), Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, stgAnoUrge, stgNumUrge, stServicio, stMedico, true, gUltimoPaciente);
                        }
                    }
                    this.Close();
                }
                rdoTemp.Close();
            }
            else
            {
                //reserva no asociada a un episodio de urgencias
                SprPacientes.Col = Col_gservici;
                stServicio = SprPacientes.Text.Trim();

                SprPacientes.Col = Col_gpersona;
                stMedico = SprPacientes.Text.Trim();
                if (stServicio != "")
                {
                    //DGMORENOG TODO_7_3
                    //mcIngreso.proCargaIngreso(this, mcIngreso, stPaciente, "NA", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, null, null, stServicio, null, null, gUltimoPaciente);
                }
                else
                {
                    //DGMORENOG TODO_7_3			
                    //mcIngreso.proCargaIngreso(this, mcIngreso, stPaciente, "NA", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, null, null, null, null, null, gUltimoPaciente);
                }

            }
            //DGMORENOG TODO_7_3
            //mcIngreso = null;
            this.Show();

        }

        public void proNuevoPac(string stGidenpac, int Ganoadme, int Gnumadme)
        {
            proEstablecerConsultaInicial();
            SprPacientes.MaxRows = 0;
            proCargarGrid();
            SprPacientes.Refresh();
        }

        private void cmbIngresoHD_ClickEvent(Object eventSender, EventArgs eventArgs)
        {
            string stMedico = String.Empty;
            DataSet rdoTemp = null;
            string stSql = String.Empty;

            SprPacientes.Col = Col_IdPaciente;
            string stPaciente = SprPacientes.Text;

            if (stPaciente != "")
            {
                stSql = "Select count(*) from uepisurg where gidenpac = '" + stPaciente.Trim() + "' and faltaurg is null";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);

                if (Convert.ToDouble(rdoTemp.Tables[0].Rows[0][0]) > 0)
                {
                    RadMessageBox.Show("Paciente ingresado en urgencias", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }

                rdoTemp.Close();
                stSql = "Select count(*) from aepisadm where gidenpac = '" + stPaciente.Trim() + "' and faltplan is null";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rdoTemp = new DataSet();
                tempAdapter_2.Fill(rdoTemp);

                if (Convert.ToDouble(rdoTemp.Tables[0].Rows[0][0]) > 0)
                {
                    RadMessageBox.Show("Paciente ingresado en planta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }

                rdoTemp.Close();
            }

            //DGMORENOG TODO_7_3
            //object mcIngreso = new IngresoHD.Ingreso_clase();

            //datos de la reserva seleccionada
            SprPacientes.Col = Col_gservici;
            string stServicio = SprPacientes.Text.Trim();

            //DGMORENOG TODO_7_3
            /*if (stServicio != "")
            {                
                mcIngreso.proCargaIngreso(this, mcIngreso, stPaciente, "NA", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, stServicio, null, gUltimoPaciente);
            }
            else
            {                
                mcIngreso.proCargaIngreso(this, mcIngreso, stPaciente, "NA", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua, null, null, gUltimoPaciente);
            }
            mcIngreso = null;*/
        }

        private void ChEntidad_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            int i = 0;
            string sqlEntidad = String.Empty;
            DataSet rrEntidad = null;
            if (ChEntidad.CheckState == CheckState.Checked)
            {
                CbEntidad.Enabled = true;
                sqlEntidad = "select gsocieda, dsocieda from dsocieda order by dsocieda ";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEntidad, Conexion_Base_datos.RcAdmision);
                rrEntidad = new DataSet();
                tempAdapter.Fill(rrEntidad);

                CbEntidad.DataSource = rrEntidad.Tables[0];
                CbEntidad.DisplayMember = "dsocieda";
                CbEntidad.ValueMember = "gsocieda";
                CbEntidad.SelectedIndex = -1;
            }
            else
            {
                //CbEntidad.Items.Clear();
                CbEntidad.DataSource = null;
                CbEntidad.Refresh();
                CbEntidad.Enabled = true;
                proEstablecerConsultaInicial();
                SprPacientes.MaxRows = 0;
                proCargarGrid();
            }
        }

        private void chkIngreso_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {            
            proEstablecerConsultaInicial();
            SprPacientes.MaxRows = 0;
            proCargarGrid();                        
        }

        private void menu_reservas_Activated(Object eventSender, EventArgs eventArgs)
        {            
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                int n = 0;
                try
                {
                    int i = 0;
                    bool EncontradoPaciente = false;
                    Application.DoEvents();
                    n = Application.OpenForms.Count;
                    this.Show();
                    SprPacientes.MaxRows = 0;
                    proEstablecerConsultaInicial();
                    proCargarGrid();
                    bRegistroSeleccionado = false;
                    UltFilaSeleccionada = 0;

                    SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;

                    SprPacientes.setSelModeIndex(0);
                    if (tbBusqueda.Enabled)
                    {
                        hacer_change = false;
                        tbBusqueda.Text = "";
                    }
                    HabilitarBotones();
                    // COMPRUEBA QUE SI ALGUNO DE LOS BOTONES SE HA QUEDADO PINCHADO Y DISABLED PONERLO BIEN

                    //int tempForVar = Toolbar1.Rows[0].Strips.First.Children[1].LayoutableChildrenCount;
                    //for (int ibotones = 0; ibotones < tempForVar; ibotones++)
                    //{
                    //    if (Toolbar1.Rows[0].Strips.First.Children[1].Children[ibotones].Enabled)
                    //    {
                    //        Toolbar1.Rows[0].Strips.First.Children[1].Children[ibotones].Enabled = false;
                    //    }
                    //}

                    //OSCAR C: 25/11/2004: Nos posicionamos en el ultimo gidenpac con el que
                    //hubieramos trabajado (nuevo, editar, documentos, filiacion)
                    EncontradoPaciente = false;
                    if (gUltimoPaciente.Trim() != "")
                    {
                        int tempForVar2 = SprPacientes.MaxRows;
                        for (i = 1; i <= tempForVar2; i++)
                        {
                            SprPacientes.Col = Col_IDReserva;
                            SprPacientes.Row = i;
                            if (SprPacientes.Text.Trim() == gUltimoPaciente.Trim())
                            {
                                EncontradoPaciente = true;
                                break;
                            }
                        }
                        if (EncontradoPaciente)
                        {
                            SprPacientes.Row = i;
                            SprPacientes.Col = 1;
                            SprPacientes_CellClick(SprPacientes, null);

                            SprPacientes.setSelModeIndex(i);
                        }
                    }
                    this.WindowState = FormWindowState.Maximized;
                    //--------------

                    ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
                    IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
                    string tempRefParam = this.Name;
                    IMDHOPEN.EstableceUltimoForm(tempRefParam, Conexion_Base_datos.VstCodUsua);

                    _Toolbar1_Button10.VisibleInStrip = IMDHOPEN.TeclaFuncionBotonVisible(Conexion_Base_datos.RcAdmision);
                    IMDHOPEN = null;
                    //Toolbar1.Buttons(10).Enabled = True
                    
                    return;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:Form_Activate", ex);
                }
            }
        }

        private void menu_reservas_Load(Object eventSender, EventArgs eventArgs)
        {
            this.Top = 0;
            this.Left = 0;

            ClassErrores = new Mensajes.ClassMensajes();

            proEstablecerConstantes();
            menu_admision.DefInstance.mnu_adm001[0].Enabled = false;
            menu_admision.DefInstance.mnu_adm001[1].Enabled = true;

            string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
            LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
            rbPaciente.Text = LitPersona;

            SprPacientes.Col = Col_Paciente;
            SprPacientes.Row = 0;
            SprPacientes.Text = LitPersona;
            //proEstablecerConsultaInicial  ' establecer la query para luego ir haciendo el REQUERY

            //oscar 10/03/2004
            vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(Conexion_Base_datos.RcAdmision);
            //---------
            menu_admision.DefInstance.WindowState = FormWindowState.Maximized;
            this.WindowState = FormWindowState.Maximized;

            ////
            //INDRA_DGMORENOG_10/05/2016 - INICIO
            //Proposito: Necesario para activar el evento DoubleClick sobre el contro RadioButton 
            //Sprint 4
            ////
            MethodInfo m = typeof(RadioButton).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                m.Invoke(rbPaciente, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(rbCama, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });                                
            }
            rbPaciente.MouseDoubleClick += rbPaciente_DblClick;
            rbCama.MouseDoubleClick += rbCama_DblClick;
            ////
            //INDRA_DGMORENOG_10/05/2016 - FIN
            //Proposito: Necesario para activar el evento DoubleClick sobre el contro RadioButton 
            //Sprint 4
            ////
        }

        private void proCargarGrid()
        {
            SprPacientes.BeginUpdate();
            //INICIO - DGMORENOG - PODER VISUALIZAR EL HEADER CON DOBLE LINEA
            this.SprPacientes.TableElement.TableHeaderHeight = 50;
            this.SprPacientes.Columns[1].HeaderText = "Fecha realizaci�n" + Environment.NewLine + "reserva";
            this.SprPacientes.Columns[4].HeaderText = "Fecha prevista" + Environment.NewLine + "ingreso";
            this.SprPacientes.Columns[10].HeaderText = "Fecha de" + Environment.NewLine + "intervenci�n";
            this.SprPacientes.Columns[11].HeaderText = "Previsi�n de d�as de" + Environment.NewLine + "estancia";
            //FIN

            int iFilas = 0;
            StringBuilder stUsuar = new StringBuilder();
            Color varColor = new Color();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (RrCargarGrid.Tables[0].Rows.Count != 0)
                {
                    if (SprPacientes.MaxRows == 0)
                    {
                        iFilas = 1;
                        SprPacientes.MaxRows = RrCargarGrid.Tables[0].Rows.Count + SprPacientes.MaxRows;
                    }
                    else
                    {
                        SprPacientes.MaxRows = RrCargarGrid.Tables[0].Rows.Count + SprPacientes.MaxRows - 1;
                        iFilas = SprPacientes.MaxRows - RrCargarGrid.Tables[0].Rows.Count + 2;
                    }
                    string vPrioridad = String.Empty;
                    foreach (DataRow iteration_row in RrCargarGrid.Tables[0].Rows)
                    {
                        switch ((Convert.ToString(iteration_row["itipingr"]) + "").Trim())
                        {
                            case "P":
                            case "":
                                varColor = SystemColors.WindowText;
                                break;
                            case "U":
                                varColor = Color.Red;
                                break;
                            case "I":
                                varColor = Color.Blue;
                                break;
                            default:
                                varColor = SystemColors.WindowText;
                                break;
                        }

                        SprPacientes.Row = iFilas;

                        SprPacientes.Col = Col_Paciente;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["dape1pac"])) ? "" : Convert.ToString(iteration_row["dape1pac"]).Trim();

                        SprPacientes.Text = SprPacientes.Text + ((Convert.IsDBNull(iteration_row["dape2pac"])) ? "" : " " + Convert.ToString(iteration_row["dape2pac"]).Trim());

                        SprPacientes.Text = SprPacientes.Text + ((Convert.IsDBNull(iteration_row["dnombpac"])) ? "" : ", " + Convert.ToString(iteration_row["dnombpac"]).Trim());
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_FecRealiza;
                        SprPacientes.Text = Convert.ToDateTime(iteration_row["fatencion"]).ToString("dd/MM/yyyy") + Environment.NewLine +
                                            Convert.ToDateTime(iteration_row["fatencion"]).ToString("HH:mm");
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_OrdenFecRealiza;
                        SprPacientes.Text = Convert.ToDateTime(iteration_row["fatencion"]).ToString("yyyyMMdd") + Convert.ToDateTime(iteration_row["fatencion"]).ToString("HHmm");
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_Cama;

                        if(iteration_row["gplantas"].ToString() != "" )
                       //SprPacientes.Text = StringsHelper.Format(iteration_row["gplantas"], "00") + StringsHelper.Format(iteration_row["ghabitac"], "00") +
                                        //  Convert.ToString(iteration_row["gcamasbo"]);

                       // SprPacientes.Text = Convert.ToString(iteration_row["gplantas"])));
                        SprPacientes.Text = Convert.ToString(int.Parse(iteration_row["Gplantas"].ToString()).ToString("D2")) + Convert.ToString(int.Parse(iteration_row["GHABITAC"].ToString()).ToString("D2")) + Convert.ToString(iteration_row["GCAMASBO"]).ToUpper();

                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_IdPaciente;
                        SprPacientes.Text = Convert.ToString(iteration_row["gidenpac"]);
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_FecIngreso;
                        SprPacientes.Text = Convert.ToDateTime(iteration_row["fpreving"]).ToString("dd/MM/yyyy") + Environment.NewLine +
                                            Convert.ToDateTime(iteration_row["fpreving"]).ToString("HH:mm");
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_OrdenFecIngreso;
                        SprPacientes.Text = Convert.ToDateTime(iteration_row["fpreving"]).ToString("yyyyMMdd") + Convert.ToDateTime(iteration_row["fpreving"]).ToString("HHmm");
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_Edad;
                        SprPacientes.Text = Serrores.CalcularEdad(Convert.ToDateTime(iteration_row["fnacipac"]), DateTime.Today, Conexion_Base_datos.RcAdmision);
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_Financia;
                        SprPacientes.TypeEditMultiLine = true;
                        SprPacientes.setTypeEditLen(300);
                        //OSCAR C ENE 2005

                        if (!Convert.IsDBNull(iteration_row["ginspecc"]))
                        {
                            SprPacientes.Text = ObtenerFinaciadora(Convert.ToInt32(iteration_row["gsocieda"])) + " " + ObtenerInspeccion(Convert.ToString(iteration_row["ginspecc"]));
                        }
                        else
                        {
                            SprPacientes.Text = ObtenerFinaciadora(Convert.ToInt32(iteration_row["gsocieda"]));
                        }
                        //------
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_UnidadEnf;

                        if (!Convert.IsDBNull(iteration_row["gunienfe"]))
                        {
                            SprPacientes.Text = Obtenerunidad(Convert.ToString(iteration_row["gunienfe"]));
                        }
                        SprPacientes.foreColor = varColor;

                        //oscar C 10/03/2004
                        SprPacientes.Col = Col_Fianza;

                        if (!Convert.IsDBNull(iteration_row["cfianza"]))
                        {
                            string tempRefParam = Convert.ToString(iteration_row["cfianza"]);
                            SprPacientes.Text = Serrores.ConvertirDecimales(ref tempRefParam, ref vstSeparadorDecimal, 2);
                        }
                        //oscar C 23/11/2004
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_FecInterven;
                        SprPacientes.TypeEditMultiLine = true;

                        if (!Convert.IsDBNull(iteration_row["finterve"]))
                        {
                            SprPacientes.Text = Convert.ToDateTime(iteration_row["finterve"]).ToString("dd/MM/yyyy") + Environment.NewLine +
                                                Convert.ToDateTime(iteration_row["finterve"]).ToString("HH:mm");
                            SprPacientes.foreColor = varColor;

                            SprPacientes.Col = Col_OrdenFecInterven;
                            SprPacientes.Text = Convert.ToDateTime(iteration_row["finterve"]).ToString("yyyyMMdd") + Convert.ToDateTime(iteration_row["finterve"]).ToString("HHmm");
                            SprPacientes.SetColHidden(SprPacientes.Col, true);
                        }

                        SprPacientes.Col = Col_DiasEstanci;
                        SprPacientes.Text = Convert.ToString(iteration_row["ndiasest"]) + "";
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_Prioridad;
                        vPrioridad = Convert.ToString(iteration_row["ipriorid"]);
                        switch (vPrioridad)
                        {
                            case "N":
                                SprPacientes.Text = "Normal";
                                break;
                            case "S":
                                SprPacientes.Text = "Preferente";
                                break;
                            default:
                                SprPacientes.Text = "";
                                break;
                        }
                        //---------
                        SprPacientes.foreColor = varColor;

                        //O.Frias - 29/06/2009
                        //Se agregan columnas con los nuevos campos.

                        SprPacientes.Col = Col_ServiIngre;
                        SprPacientes.Text = (Convert.ToString(iteration_row["serIngreso"]) + "").Trim();
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_TipoIngre;
                        switch ((Convert.ToString(iteration_row["itipingr"]) + "").Trim())
                        {
                            case "I":
                                SprPacientes.Text = "Inmediato";
                                break;
                            case "U":
                                SprPacientes.Text = "Urgente";
                                break;
                            case "P":
                                SprPacientes.Text = "Programada";
                                break;
                        }
                        SprPacientes.foreColor = varColor;

                        //'''''        Col_TipoIngre

                        SprPacientes.Col = Col_Ambito;
                        switch ((Convert.ToString(iteration_row["itipambi"]) + "").Trim())
                        {
                            case "A":
                                SprPacientes.Text = "Ambulante";
                                break;
                            case "U":
                                SprPacientes.Text = "Urgencias";
                                break;
                        }

                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_CentOrigen;
                        SprPacientes.Text = (Convert.ToString(iteration_row["dnomhosp"]) + "").Trim();
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_SerSolici;
                        SprPacientes.Text = (Convert.ToString(iteration_row["dnomserv"]) + "").Trim();
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_MedSolici;
                        stUsuar = new StringBuilder((Convert.ToString(iteration_row["dap1pers"]) + "").Trim() + " " + (Convert.ToString(iteration_row["dap2pers"]) + "").Trim());
                        stUsuar.Append(((stUsuar.ToString() + "").Trim() == "") ? "" : ", " + (Convert.ToString(iteration_row["dnompers"]) + "").Trim());
                        SprPacientes.Text = stUsuar.ToString();
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_Usuario;
                        //SprPacientes.Text = Trim(RrCargarGrid("dusuario") & "")

                        if (!Convert.IsDBNull(iteration_row["gperusu"]))
                        {
                            SprPacientes.Text = fPersona(Convert.ToInt32(iteration_row["gperusu"]));
                        }
                        SprPacientes.foreColor = varColor;

                        SprPacientes.Col = Col_Observa; //observaciones

                        SprPacientes.setTypeEditLen(255);
                        SprPacientes.Text = Convert.ToString(iteration_row["oobserva"]);
                        SprPacientes.foreColor = varColor;

                        //OSCAR C ENE 2005
                        SprPacientes.Col = Col_IDReserva;
                        SprPacientes.Text = Convert.ToString(iteration_row["ganoregi"]) + "/" + Convert.ToString(iteration_row["gnumregi"]);
                        SprPacientes.foreColor = varColor;
                        //---------

                        //O.Frias - 02/07/2009
                        //Incorporo nuevas columnas para el envio a ingreso
                        SprPacientes.Col = Col_HospitalDia;
                        if ((Convert.ToString(iteration_row["isillon"]) + "").Trim() == "S" || (Convert.ToString(iteration_row["icentdia"]) + "").Trim() == "S")
                        {
                            SprPacientes.Text = "S";
                        }
                        else
                        {
                            SprPacientes.Text = "";
                        }
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_gservici;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["gservici"])) ? "" : Convert.ToString(iteration_row["gservici"]);
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_gpersona;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["gpersona"])) ? "" : Convert.ToString(iteration_row["gpersona"]);
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_gsersoli;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["gsersoli"])) ? "" : Convert.ToString(iteration_row["gsersoli"]);
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_gpersoli;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["gpersoli"])) ? "" : Convert.ToString(iteration_row["gpersoli"]);
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_ganourge;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["ganourge"])) ? "" : Convert.ToString(iteration_row["ganourge"]);
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        SprPacientes.Col = Col_gnumurge;

                        SprPacientes.Text = (Convert.IsDBNull(iteration_row["gnumurge"])) ? "": Convert.ToString(iteration_row["gnumurge"]);
                        SprPacientes.foreColor = varColor;
                        SprPacientes.SetColHidden(SprPacientes.Col, true);

                        iFilas++;
                    }
                }
                else
                {
                    SprPacientes.MaxRows = 0;
                }

                SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;

                this.Cursor = Cursors.Default;
                SprPacientes.EndUpdate();
            }
            catch (SqlException ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:procargarGrid", ex);
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

        private void proEstablecerConstantes()
        {
            string sqlConstantes = String.Empty;
            DataSet RrConstantes = null;
            try
            {
                sqlConstantes = "select nnumeri1 from sconsglo where gconsglo='MAXFILAS'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlConstantes, Conexion_Base_datos.RcAdmision);
                RrConstantes = new DataSet();
                tempAdapter.Fill(RrConstantes);
                if (RrConstantes.Tables[0].Rows.Count == 1)
                {
                    NUMEROMAXIMOFILAS = Convert.ToInt32(RrConstantes.Tables[0].Rows[0][0]); // numero m�ximo de filas a cargar en el grid
                }
                // CREACION DEL OBJECT0 RESERVA
                //Set ObjReservaAdmision = CreateObject("ClaseReserva.ClaseReservasAdmision")

                RrConstantes.Close();
                bRegistroSeleccionado = false;
                UltFilaSeleccionada = 0;
                SprPacientes.MaxRows = 0;

                bSort1 = true;
                bSort2 = true;
                bSort3 = true;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:ProestablecerConstantes", ex);
            }
        }

        private void proEstablecerConsultaInicial()
        {
            string FechaMaxima = String.Empty;
            bool blnComa = false;
            try
            {
                sqlCargarGrid = "select max(fpreving) as maximafecha from aresingrva";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCargarGrid, Conexion_Base_datos.RcAdmision);
                RrCargarGrid = new DataSet();
                tempAdapter.Fill(RrCargarGrid);
                if (!Convert.IsDBNull(RrCargarGrid.Tables[0].Rows[0][0]))
                {
                    FechaMaxima = Convert.ToString(RrCargarGrid.Tables[0].Rows[0][0]);
                }
                else
                {
                    //En curia val�a con ""  pero para Panam� es necesario que al par�metro se le mande valor.
                    FechaMaxima = "01/01/1900";
                }

                RrCargarGrid.Close();

                //sqlCargarGrid = "select aresingrva.*,(RTRIM(dpacient.dape1pac) + ' ' + RTRIM(dpacient.dape2pac)"
                //sqlCargarGrid = sqlCargarGrid & " + ', ' + RTRIM(dpacient.dnombpac)) as nombrepaciente, "

                //OSCAR C ENE 2005
                //'sqlCargarGrid = "select aresingrva.*,dpacient.dape1pac,dpacient.dape2pac,dpacient.dnombpac,"
                //'sqlCargarGrid = sqlCargarGrid & " fnacipac, dpacient.gsocieda "
                //'sqlCargarGrid = sqlCargarGrid & "from dpacient,aresingrva "
                //'If CbEntidad.Text <> "" Then
                //'   sqlCargarGrid = sqlCargarGrid & ",dsocieda "
                //'End If
                //'sqlCargarGrid = sqlCargarGrid & "where dpacient.gidenpac=aresingrva.gidenpac "
                //'If CbEntidad.Text <> "" Then
                //'   sqlCargarGrid = sqlCargarGrid & " and  dpacient.gsocieda = dsocieda.gsocieda "
                //'   sqlCargarGrid = sqlCargarGrid & " and dsocieda.dsocieda =  '" & CbEntidad.Text & "' "
                //'End If

                // O.Frias - 29/06/2009
                // Incorporo los nuevos datos en la SELECT

                //sqlCargarGrid = "select aresingrva.*," & _
                //'                " dpacient.dape1pac,dpacient.dape2pac,dpacient.dnombpac,dpacient.fnacipac " & _
                //'                " from dpacient,aresingrva "
                //sqlCargarGrid = sqlCargarGrid & "where dpacient.gidenpac=aresingrva.gidenpac "

                sqlCargarGrid = "SELECT ARESINGRVA.*, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, " +
                                "DPACIENT.fnacipac, DHOSPITAVA.dnomhosp, DSERVICIVA.dnomserv, A.dnomserv as SerIngreso, " +
                                "DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers, SUSUARIO.dusuario, DTIPOCAM.isillon, a.icentdia,SUSUARIO.gpersona as gperusu " +
                                "FROM ARESINGRVA " +
                                "INNER JOIN DPACIENT ON ARESINGRVA.gidenpac = DPACIENT.gidenpac " +
                                "LEFT OUTER JOIN DPERSONA ON ARESINGRVA.gpersoli = DPERSONA.gpersona " +
                                "LEFT OUTER JOIN DSERVICIVA ON ARESINGRVA.gsersoli = DSERVICIVA.gservici " +
                                "LEFT OUTER JOIN DSERVICIVA as A ON ARESINGRVA.gservici = A.gservici " +
                                "LEFT OUTER JOIN DHOSPITAVA ON ARESINGRVA.ghospori = DHOSPITAVA.ghospita " +
                                "INNER JOIN SUSUARIO ON ARESINGRVA.gusuario = SUSUARIO.gusuario " +
                                "LEFT OUTER JOIN DCAMASBO ON ARESINGRVA.gplantas = DCAMASBO.gplantas AND " +
                                "ARESINGRVA.ghabitac = DCAMASBO.ghabitac AND " +
                                "ARESINGRVA.gcamasbo = DCAMASBO.gcamasbo " +
                                "LEFT OUTER JOIN DTIPOCAM ON DCAMASBO.gtipocam = DTIPOCAM.gtipocam ";

                sqlCargarGrid = sqlCargarGrid + " WHERE (dpacient.dape1pac > @dape1pac_1 or " +
                                "(dpacient.dape1pac = @dape1pac_2 and dpacient.dape2pac > @dape2pac_1) or" +
                                "(dpacient.dape1pac = @dape1pac_3 and dpacient.dape2pac = @dape2pac_2 and dpacient.dnombpac > @dnombpac) )";

                if (CbEntidad.Text != "" && CbEntidad.Text != "System.Data.DataRowView" && CbEntidad.SelectedIndex > 0)
                {
                    //sqlCargarGrid = sqlCargarGrid + " AND  aresingrva.gsocieda =" + CbEntidad.GetItemData(CbEntidad.SelectedIndex).ToString();
                    sqlCargarGrid = sqlCargarGrid + " AND  aresingrva.gsocieda =" + CbEntidad.Items[CbEntidad.SelectedIndex].Value.ToString();
                }
                //----------------

                //O.Frias - 30/06/2009
                //Incorporo las condiciones de TipoIngreso
                if (chkIngreso[0].CheckState == CheckState.Checked || chkIngreso[1].CheckState == CheckState.Checked || chkIngreso[2].CheckState == CheckState.Checked)
                {
                    blnComa = false;
                    sqlCargarGrid = sqlCargarGrid + " AND  aresingrva.itipingr IN (";
                    if (chkIngreso[0].CheckState == CheckState.Checked)
                    {
                        sqlCargarGrid = sqlCargarGrid + "'I'";
                        blnComa = true;
                    }
                    if (chkIngreso[1].CheckState == CheckState.Checked)
                    {
                        if (blnComa)
                        {
                            sqlCargarGrid = sqlCargarGrid + ", 'U' ";
                        }
                        else
                        {
                            sqlCargarGrid = sqlCargarGrid + "'U'";
                        }
                        blnComa = true;
                    }
                    if (chkIngreso[2].CheckState == CheckState.Checked)
                    {
                        if (blnComa)
                        {
                            sqlCargarGrid = sqlCargarGrid + ", 'P'";
                        }
                        else
                        {
                            sqlCargarGrid = sqlCargarGrid + "'P'";
                        }
                    }
                    sqlCargarGrid = sqlCargarGrid + " ) ";
                }

                //*****carlos 05/04/2002 cambio la condici�n del par�metro, no se puede pedir una
                //cosa como par�metro y ordenar por otra

                //sqlCargarGrid = sqlCargarGrid & " and fpreving <= ? order by nombrepaciente"

                sqlCargarGrid = sqlCargarGrid + " order by DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac";

                RqReservas.Connection = Conexion_Base_datos.RcAdmision;
                RqReservas.CommandText = sqlCargarGrid;

                //If Trim(FechaMaxima) <> "" Then
                //    RqReservas.rdoParameters(0) = CDate(FechaMaxima)
                //End If

                RqReservas.Parameters.Add("@dape1pac_1", SqlDbType.VarChar).Value = "";
                RqReservas.Parameters.Add("@dape1pac_2", SqlDbType.VarChar).Value = "";
                RqReservas.Parameters.Add("@dape1pac_3", SqlDbType.VarChar).Value = "";
                RqReservas.Parameters.Add("@dape2pac_1", SqlDbType.VarChar).Value = "";
                RqReservas.Parameters.Add("@dape2pac_2", SqlDbType.VarChar).Value = "";
                RqReservas.Parameters.Add("@dnombpac", SqlDbType.VarChar).Value = "";

                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(RqReservas);
                RrCargarGrid = new DataSet();
                tempAdapter_2.Fill(RrCargarGrid);
                if (RrCargarGrid.Tables[0].Rows.Count == NUMEROMAXIMOFILAS)
                {
                    stUltimoRegistroNom = Convert.ToString(RrCargarGrid.Tables[0].Rows[0]["dape1pac"]);

                    stUltimoRegistroAP1 = Convert.ToString(RrCargarGrid.Tables[0].Rows[0]["dape2pac"]);

                    stUltimoRegistroAP2 = Convert.ToString(RrCargarGrid.Tables[0].Rows[0]["dnombpac"]);

                    iTope = RrCargarGrid.Tables[0].Rows.Count - NUMEROMAXIMOFILASENPANTALLA; // cuatro es el maximo de filas que caben en la pantalla
                    bCargarMas = true;
                }
                else
                {
                    if (RrCargarGrid.Tables[0].Rows.Count < NUMEROMAXIMOFILAS)
                    {
                        bCargarMas = false;
                    }
                }
                RqReservas.Parameters.Clear();
                RqReservas.Dispose();
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:ProestablecerConsultaInicial", null);
            }
        }

        private bool isInitializingComponent;

        private void menu_reservas_Resize(Object eventSender, EventArgs eventArgs)
        {
            bool ErrResize = false;
            if (isInitializingComponent)
            {
                return;
            }
            try
            {
                ErrResize = true;
                Frame1.Width = (int)(this.Width - 52);//52
                Frame1.Height = (int)(this.Height - 267);//267
                

                SprPacientes.Width = (int)(this.ClientRectangle.Width - 63);
                SprPacientes.Height = (int)(this.ClientRectangle.Height - 260);

                cbCerrar.Top = (int)(this.Height - cbCerrar.Height - 80);
                cbCerrar.Left = (int)(this.Width - cbCerrar.Width - 60);

                cmbIngreso.Top = (int)(this.Height - cmbIngreso.Height - 80);
                cmbIngreso.Left = (int)(this.Width - cmbIngreso.Width - 150);
                cmbIngresoHD.Top = (int)(this.Height - cmbIngresoHD.Height - 80);
                cmbIngresoHD.Left = (int)(this.Width - cmbIngresoHD.Width - 150);

                pxLineas.Top = (int)(this.Height - 115);
                
                Line1[0].Y1 = (int)(this.Height -93);//93
                Line1[0].Y2 = (int)Line1[0].Y1;
                Line1[1].Y1 = (int)(this.Height - 107);//107
                Line1[1].Y2 = (int)Line1[1].Y1;
                Line1[2].Y1 = (int)(this.Height - 80);//80
                Line1[2].Y2 = (int)Line1[2].Y1;

                Label1[0].Top = (int)(Line1[0].Y1 - 8);//-8
                Label1[1].Top = (int)(Line1[1].Y1 - 8);//-8
                Label1[2].Top = (int)(Line1[2].Y1 - 8);//-8

                ErrResize = false;
                
            }
            catch (Exception excep)
            {
                if (!ErrResize)
                {
                    throw excep;
                }

                if (ErrResize)
                {
                    menu_admision.DefInstance.WindowState = FormWindowState.Maximized;
                    this.WindowState = FormWindowState.Maximized;
                }
            }
        }

        private void menu_reservas_Closed(Object eventSender, CancelEventArgs eventArgs)
        {
            //comprueba que no existe otro formulario de relacionado con las reservas
            foreach (Form Form in Application.OpenForms)
            {
                if (Form.Name.Trim().ToUpper() == "MANT_RESERVA" || Form.Name.Trim().ToUpper() == "DOCU_RESERVAS")
                { // si est� cargado la ventana de reservas deshabilitar seg�n
                  // el tipo de operaci�n.
                    RadMessageBox.Show("Imposible cerrar. Existe un formulario de reservas abierto", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                    eventArgs.Cancel = (!true) ? false : true;
                    return;
                }
            }

            menu_admision.DefInstance.mnu_adm001[0].Enabled = true; // MENU RESERVAS
            menu_admision.DefInstance.mnu_adm001[1].Enabled = false; // NUEVA RESERVA
            menu_admision.DefInstance.mnu_adm001[2].Enabled = false; // MANTENIMIENTO DE RESERVAS
            menu_admision.DefInstance.mnu_adm001[3].Enabled = false; //DOCUMENTOS
            menu_admision.DefInstance.mnu_adm001[4].Enabled = false; // ANULACION
            menu_admision.DefInstance.mnu_adm001[5].Enabled = false; // FILIACION

            ClassErrores = null;
            MemoryHelper.ReleaseMemory();
        }

        private void rbCama_CheckedChanged(Object eventSender, Telerik.WinControls.UI.StateChangedEventArgs eventArgs)
        {
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                if (flagDbClick)
                {
                    rbCama.IsChecked = false;
                    flagDbClick = false;
                }
                else
                {
                    tbBusqueda.Enabled = true;
                    hacer_change = false;
                    tbBusqueda.Text = "";
                    hacer_change = true;
                    tbBusqueda.Focus();
                    bRegistroSeleccionado = false;

                    SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                    UltFilaSeleccionada = 0;
                }
            }
        }

        private void rbCama_DblClick(object sender, System.EventArgs e)
        {
            flagDbClick = true;
            if (rbCama.IsChecked)
            {
                FrmEntidad.Enabled = true;
                hacer_change = false;
                tbBusqueda.Text = "";
                tbBusqueda.Enabled = false;
                rbCama.IsChecked = false;

                SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                bRegistroSeleccionado = false;
                UltFilaSeleccionada = 0;
            }
        }

        private void rbPaciente_CheckedChanged(Object eventSender, Telerik.WinControls.UI.StateChangedEventArgs eventArgs)
        {
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                if (flagDbClick)
                {
                    rbPaciente.IsChecked = false;
                    flagDbClick = false;
                }
                else
                {
                    tbBusqueda.Enabled = true;
                    hacer_change = false;
                    tbBusqueda.Text = "";
                    hacer_change = true;
                    bRegistroSeleccionado = false;

                    SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                    UltFilaSeleccionada = 0;
                    tbBusqueda.Focus();
                }
            }
        }

        private void rbPaciente_DblClick(object sender, System.EventArgs e)
        {
            flagDbClick = true;
            if (rbPaciente.IsChecked)
            {
                hacer_change = false;
                tbBusqueda.Text = "";
                tbBusqueda.Enabled = false;
                rbPaciente.IsChecked = false;

                SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                bRegistroSeleccionado = false;
                UltFilaSeleccionada = 0;
            }
        }

        private void SprPacientes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = 0;
            int Row = 0;

            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = SprPacientes.Col;
                Row = SprPacientes.Row;
            }
            try
            {
                //If Not hacer_click Then Exit Sub
                if (Row == 0)
                {
                    //   If Col = 1 Or Col = 3 Then
                    hacer_change = false;
                    tbBusqueda.Text = "";
                                        
                    bRegistroSeleccionado = false;
                    ObjReservaAdmision.Reset();

                    //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                    cmbIngresoHD.Visible = false;
                    cmbIngreso.Visible = false;
                    //    End If
                    return;
                }

                hacer_change = false;
                if (bRegistroSeleccionado)
                { // si hay uno seleccionado
                    if (UltFilaSeleccionada == Row)
                    {
                        bRegistroSeleccionado = false;

                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                        ObjReservaAdmision.Reset();

                        if (this.ActiveControl.Name == SprPacientes.Name && tbBusqueda.Enabled)
                        {
                            tbBusqueda.Text = "";
                        }
                    }
                    else
                    {
                        bRegistroSeleccionado = true;

                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;

                        ObjReservaAdmision.Reset();
                        proIntroducirIdentificador(Row);

                        if (this.ActiveControl.Name == SprPacientes.Name && tbBusqueda.Enabled)
                        {
                            SprPacientes.Col = Col_Paciente;
                            tbBusqueda.Text = SprPacientes.Text;
                        }
                    }
                }
                else
                {
                    //SI NO HABIA REGISTRO SELECCIONADO
                    bRegistroSeleccionado = true;

                    ObjReservaAdmision.Reset();
                    proIntroducirIdentificador(Row);

                    if (this.ActiveControl.Name == SprPacientes.Name && tbBusqueda.Enabled)
                    {
                        SprPacientes.Col = Col_Paciente;
                        tbBusqueda.Text = SprPacientes.Text;
                        tbBusqueda.SelectionLength = Strings.Len(tbBusqueda.Text);
                    }
                }
                HabilitarBotones();
                UltFilaSeleccionada = Row; //GUARDO LA ULTIMA FILA SELECCIONADA
                hacer_change = true;

                if (bRegistroSeleccionado)
                {
                    SprPacientes.Col = Col_HospitalDia;
                    if (SprPacientes.Text == "S")
                    {
                        cmbIngresoHD.Visible = true;
                        cmbIngreso.Visible = false;
                    }
                    else
                    {
                        cmbIngreso.Visible = true;
                        cmbIngresoHD.Visible = false;
                    }
                    SprPacientes.Col = Col_IDReserva;
                    gUltimoPaciente = SprPacientes.Text;
                }
                else
                {
                    gUltimoPaciente = "";
                    cmbIngreso.Visible = false;
                    cmbIngresoHD.Visible = false;
                    this.Refresh();
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message);
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:SprPacientes_click", null);
            }
        }

        //''Private Sub SprPacientes_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
        //''MsgBox Str(SprPacientes.ColWidth(Col2))
        //''End Sub

        public void SprPacientes_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = 0;
            int Row = 0;

            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = SprPacientes.Col;
                Row = SprPacientes.Row;
            }


            if (Col == 0 || Row == 0)
            {
                return;
            }
            //comprueba que no existe otro formulario de reservas y que se est� modificando tambien
            foreach (dynamic Form in Application.OpenForms)
            {
                if (Form.Name.Trim().ToUpper() == "MANT_RESERVA")
                {
                    // si est� cargado la ventana de reservas deshabilitar seg�n
                    // el tipo de operaci�n
                    if (Convert.ToString(Form.VstOperacion) == "MODIFICAR_RESERVA") //Hace Parte del formulario Reserva.Mant_reserva
                    {
                        _Toolbar1_Button1.Enabled = false;
                        RadMessageBox.Show("Ya existe una modificaci�n en curso", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                }
            }
            //SprPacientes.OperationMode = OperationModeRead + OperationModeRow
            //'menu_admision.mnu_adm001(1).Enabled = False
            //'menu_admision.mnu_adm001(2).Enabled = False
            //'menu_admision.mnu_adm001(3).Enabled = False
            //'menu_admision.mnu_adm001(4).Enabled = False
            //bRegistroSeleccionado = False
            //UltFilaSeleccionada = 0
            //SprPacientes.SelModeIndex = -1

            HabilitarBotones();

            ObjReservaAdmision.Reset();
            proIntroducirIdentificador(Row);

            //OSCAR C ENE 2005
            stCodPacE = "";
            
            Clase_Filiacion = new filiacionDLL.Filiacion();            
            Clase_Filiacion.Load(Clase_Filiacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, ObjReservaAdmision.IdentificadorPaciente);
            if (stCodPacE == "")
            {
                this.Show();
                return;
            }
            //----------------

            proMemorizarReserva();
            VstOperacionReserva = "MODIFICAR_RESERVA";

            SprPacientes.Col = Col_IDReserva;
            gUltimoPaciente = SprPacientes.Text;

            //OSCAR C ENE 2005
            //LLAMADA A LA DLL DE MANTENIMIENTO DE RESERVAS - PANTALLA AGR010F1
            //Load Mant_reserva
            //Mant_reserva.Show
            ObjReservaAdmision.OperacionReserva = VstOperacionReserva;
            ObjReservaAdmision.usuario = Conexion_Base_datos.VstCodUsua;

            //DGMORENOG TODO_7_3
            /*object obManteReservas = new reserva.MCRESERVA();
            //UPGRADE_TODO: (1067) Member Mante_Reserva is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            obManteReservas.Mante_Reserva(this, Conexion_Base_datos.RcAdmision, ObjReservaAdmision);
            obManteReservas = null;
            //UPGRADE_WARNING: (2065) Form event menu_reservas.Activated has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
            menu_reservas_Activated(this, new EventArgs());*/
        }

        private void SprPacientes_MouseUp(Object eventSender, MouseEventArgs eventArgs)
        {
            int Button = (int)eventArgs.Button;
            int Shift = (int)(Control.ModifierKeys & Keys.Shift);
            float x = (float)(eventArgs.X * 15);
            float y = (float)(eventArgs.Y * 15);

            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
            int i = 0;
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            string Paciente = String.Empty;
            string Numero = String.Empty;
            string ano = String.Empty;
            string Episodio = String.Empty;
            if (eventArgs.Button == MouseButtons.Right)
            {
                if (SprPacientes.ActiveRowIndex > 0)
                {
                    if (_Toolbar1_Button10.Enabled)
                    {
                        UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
                        SprPacientes.Row = SprPacientes.ActiveRowIndex;
                        Episodio = sValorColumna(SprPacientes, Col_IDReserva);
                        Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                        ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                        Episodio = "R" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                        Paciente = sValorColumna(SprPacientes, Col_IdPaciente);

                        UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
                        IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
                        string tempRefParam = this.Name;
                        string tempRefParam2 = "sprPacientes";
                        bool tempRefParam3 = false;

                        IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, tempRefParam, tempRefParam2, Paciente, Episodio, tempRefParam3, Mouse.X, Mouse.Y);
                        IMDHOPEN = null;
                    }
                }
            }
        }

        private void SprPacientes_TopLeftChange(object eventSender, UpgradeHelpers.Spread.TopLeftChangeEventArgs eventArgs)
        {
            string stfecha = String.Empty;
            string stsegundos = String.Empty;
            if (bCargarMas)
            {
                if (eventArgs.NewTop >= iTope)
                {
                    this.Cursor = Cursors.WaitCursor;
                    SprPacientes.Row = SprPacientes.MaxRows;
                    //        SprPacientes.Col = 2  ' FECHA PREVISTA INGRESO
                    //        stfecha = SprPacientes.Text
                    //        SprPacientes.Col = 12   ' segundos
                    //        RqReservas.rdoParameters(0) = stfecha

                    //        RqReservas.rdoParameters(0) = stUltimoRegistro

                    RqReservas.Parameters.Add("@dape1pac_1", SqlDbType.VarChar);
                    RqReservas.Parameters.Add("@dape1pac_2", SqlDbType.VarChar);
                    RqReservas.Parameters.Add("@dape1pac_3", SqlDbType.VarChar);
                    RqReservas.Parameters.Add("@dape2pac_1", SqlDbType.VarChar);
                    RqReservas.Parameters.Add("@dape2pac_2", SqlDbType.VarChar);
                    RqReservas.Parameters.Add("@dnombpac", SqlDbType.VarChar);

                    RqReservas.Parameters[0].Value = RrCargarGrid.Tables[0].Rows[0]["dape1pac"];

                    RqReservas.Parameters[1].Value = RrCargarGrid.Tables[0].Rows[0]["dape1pac"];

                    RqReservas.Parameters[2].Value = RrCargarGrid.Tables[0].Rows[0]["dape2pac"];

                    RqReservas.Parameters[3].Value = RrCargarGrid.Tables[0].Rows[0]["dape1pac"];

                    RqReservas.Parameters[4].Value = RrCargarGrid.Tables[0].Rows[0]["dape2pac"];

                    RqReservas.Parameters[5].Value = RrCargarGrid.Tables[0].Rows[0]["dnombpac"];

                    string tempQuery = RrCargarGrid.Tables[0].TableName;
                    RrCargarGrid.Reset();
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(RqReservas);
                    tempAdapter.Fill(RrCargarGrid, tempQuery);
                    if (RrCargarGrid.Tables[0].Rows.Count == NUMEROMAXIMOFILAS)
                    {

                        //            stUltimoRegistro = RrCargarGrid("nombrepaciente")

                        stUltimoRegistroNom = Convert.ToString(RrCargarGrid.Tables[0].Rows[0]["dape1pac"]);

                        stUltimoRegistroAP1 = Convert.ToString(RrCargarGrid.Tables[0].Rows[0]["dape2pac"]);

                        stUltimoRegistroAP2 = Convert.ToString(RrCargarGrid.Tables[0].Rows[0]["dnombpac"]);

                        iTope = RrCargarGrid.Tables[0].Rows.Count + SprPacientes.MaxRows - NUMEROMAXIMOFILASENPANTALLA;
                        bCargarMas = true;
                    }
                    else
                    {
                        if (RrCargarGrid.Tables[0].Rows.Count < NUMEROMAXIMOFILAS)
                        {
                            bCargarMas = false;
                        }
                    }
                    if (RrCargarGrid.Tables[0].Rows.Count > 0)
                    {
                        proCargarGrid();
                    }

                    this.Cursor = Cursors.Default;
                    //reordenar
                    ProOrdenarFilas(SprPacientes.GetSortKey(1));
                }
            }
        }

        public void HabilitarBotones()
        {
            bool fNuevaReserva = false; // SI ENCUENTRA UN FORMULARIO QUE SE A�ADA UNA RESERVA -> TRUE
            bool fModificacionReserva = false; // SI ENCUENTRA UN FORMULARIO QUE SE MODIFIQUE UNA RESERVA -> TRUE
            bool fDocumentoReservas = false;
            // comprobar las ventanas que estan cargadas
            foreach (dynamic Form in Application.OpenForms)
            {
                if (Form.Name.Trim().ToUpper() == "MANT_RESERVA")
                {
                    // si est� cargado la ventana de reservas deshabilitar seg�n
                    // el tipo de operaci�n

                    if (Convert.ToString(Form.VstOperacion) == "A�ADIR_RESERVA")
                    {
                        fNuevaReserva = true;
                    }
                    if (Convert.ToString(Form.VstOperacion) == "MODIFICAR_RESERVA")//Hace Parte del formulario Reserva.Mant_reserva
                    {
                        fModificacionReserva = true;
                    }

                    //                Toolbar1.Buttons(1).Enabled = False
                    //                menu_admision.mnu_adm001(1).Enabled = False
                    //                fNuevaReserva = True
                    //                'Exit For
                }
                if (Form.Name.Trim().ToUpper() == "DOCU_RESERVAS")
                {
                    fDocumentoReservas = true;
                }
                //                fModificacionReserva = True
                //                menu_admision.mnu_adm001(2).Enabled = False
            }
            if (bRegistroSeleccionado)
            {
                _Toolbar1_Button3.Enabled = true; // anulacion de la reserva
                menu_admision.DefInstance.mnu_adm001[3].Enabled = true;

                _Toolbar1_Button4.Enabled = true; // MANTENIMIENTO DE DATOS DE FILIACION
                menu_admision.DefInstance.mnu_adm001[5].Enabled = true;
                if (fNuevaReserva)
                { // si se esta haciendo una nueva reserva                    
                    _Toolbar1_Button1.Enabled = false;
                    menu_admision.DefInstance.mnu_adm001[1].Enabled = false;
                }
                else
                {
                    _Toolbar1_Button1.Enabled = true;
                    menu_admision.DefInstance.mnu_adm001[1].Enabled = true;
                }
                menu_admision.DefInstance.mnu_adm001[2].Enabled = !fModificacionReserva;
                if (fDocumentoReservas)
                {
                    menu_admision.DefInstance.mnu_adm001[4].Enabled = false;
                    _Toolbar1_Button2.Enabled = false;
                }
                else
                {
                    menu_admision.DefInstance.mnu_adm001[4].Enabled = true;
                    _Toolbar1_Button2.Enabled = true;
                }
                _Toolbar1_Button10.Enabled = true;
            }
            else
            {
                if (fNuevaReserva)
                { // si se esta haciendo una nueva reserva                    
                    _Toolbar1_Button1.Enabled = false;
                    menu_admision.DefInstance.mnu_adm001[1].Enabled = false;
                }
                else
                {
                    _Toolbar1_Button1.Enabled = true;
                    menu_admision.DefInstance.mnu_adm001[1].Enabled = true;
                }

                _Toolbar1_Button2.Enabled = false;
                menu_admision.DefInstance.mnu_adm001[2].Enabled = false;

                _Toolbar1_Button3.Enabled = false;
                menu_admision.DefInstance.mnu_adm001[3].Enabled = false;
                menu_admision.DefInstance.mnu_adm001[4].Enabled = false;

                _Toolbar1_Button4.Enabled = false; // MANTENIMIENTO DE DATOS DE FILIACION
                menu_admision.DefInstance.mnu_adm001[5].Enabled = false;

                _Toolbar1_Button10.Enabled = false;
            }

            //If Not fNuevaReserva Then  ' hay una nueva reserva
            //    Toolbar1.Buttons(1).Enabled = True
            //    menu_admision.mnu_adm001(1).Enabled = True
            //End If
            //If Not fModificacionReserva Then
            //    menu_admision.mnu_adm001(2).Enabled = True
            //End If
        }

        private void tbBusqueda_TextChanged(Object eventSender, EventArgs eventArgs)
        {
            int pos = 0;
            //'''''''
            int posE = 0;
            try
            {
                if (hacer_change && SprPacientes.GetLastNonEmptyRow(null) > 0)
                {
                    if (rbPaciente.IsChecked)
                    {
                        if (ORDEN_PACIENTE == UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending)
                        {
                            ProOrdenarFilas(1);
                        }
                        pos = posicionarpacienteCama(tbBusqueda.Text.Trim());
                    }
                    if (rbCama.IsChecked)
                    {
                        if (ORDEN_CAMA == UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending)
                        {
                            ProOrdenarFilas(1);
                        }
                        pos = posicionarpacienteCama(tbBusqueda.Text.Trim());
                    }
                    hacer_click = false;
                    SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
                    //SPRPACIENTES.OperationMode = OperationModeSingle
                    if (pos == -1)
                    {
                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                        hacer_click = false;
                        //      SprPacientes_Click 0, 0
                        bRegistroSeleccionado = false;
                        UltFilaSeleccionada = 0;
                        HabilitarBotones();
                    }
                    else
                    {
                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;

                        hacer_click = false;
                        bRegistroSeleccionado = false;
                        SprPacientes.Row2 = pos;
                        SprPacientes.Col2 = 1;
                        //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
                    }
                    SprPacientes.setSelModeIndex(pos);
                }
                else
                {
                    hacer_change = true;
                }
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:tbBusqueda_Change", ex);
            }
        }

        private void tbBusqueda_Enter(Object eventSender, EventArgs eventArgs)
        {
            hacer_change = true;
        }

        private void tbBusqueda_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            int KeyAscii = Strings.Asc(eventArgs.KeyChar);
            if (KeyAscii == 39)
            {
                KeyAscii = Serrores.SustituirComilla();
            }
            if (KeyAscii == 0)
            {
                eventArgs.Handled = true;
            }
            eventArgs.KeyChar = Convert.ToChar(KeyAscii);
        }

        private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
        {
            Telerik.WinControls.UI.CommandBarButton Button = (Telerik.WinControls.UI.CommandBarButton)eventSender;

            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();

            int i = 0;
            string Paciente = String.Empty;
            string Numero = String.Empty;
            string ano = String.Empty;
            string Episodio = String.Empty;
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            switch (radCommandBarStripElement1.Items.IndexOf(Button) + 1)
            {
                case 1:
                    // LLAMA A LA DLL DE FILIACION PARA BUSCAR A UN PACIENTE Y REALIZAR SU RESERVA 
                    //nueva reserva                     
                    menu_admision.DefInstance.mnu_adm001[1].PerformClick();
                    break;
                case 2:
                    menu_admision.DefInstance.mnu_adm001[4].PerformClick();
                    break;
                case 3:
                    menu_admision.DefInstance.mnu_adm001[3].PerformClick();
                    break;
                case 4:
                    menu_admision.DefInstance.mnu_adm001[4].PerformClick();
                    menu_reservas_Activated(this, new EventArgs());
                    //OSCAR C ENE 2005 
                    break;
                case 5:
                    proPeticionHistoria();
                    //------- 

                    break;
                case 6:  //6 
                         //listado reservase pendientes de ingreso 
                         //        Load Reservas_Pend_Ingreso 
                         //        Reservas_Pend_Ingreso.Show vbModal 
                         //llamar a la dll 
                    ReservasPendIngreso.MCReservas Reservas = new ReservasPendIngreso.MCReservas();
                    Reservas.LoadClase(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.Cuadro_Diag_imprePrint, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.StBaseTemporal);
                    Reservas = null;
                    break;
                case 7:  //8 
                    ReservasAnuladas tempLoadForm = ReservasAnuladas.DefInstance;
                    ReservasAnuladas.DefInstance.ShowDialog();
                    break;
                case 8:
                    Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
                    if (SprPacientes.ActiveRowIndex > 0)
                    {
                        SprPacientes.Row = SprPacientes.ActiveRowIndex;
                        Episodio = sValorColumna(SprPacientes, Col_IDReserva);
                        Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                        ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                        Episodio = "R" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                        Paciente = sValorColumna(SprPacientes, Col_IdPaciente);

                        UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
                        IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();

                        IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, this.Name, "OpenImdh", Paciente, Episodio, false, Mouse.X, Mouse.Y);
                        IMDHOPEN = null;
                    }
                    break;
            }
        }

        public void proMemorizarReserva()
        {
            string sqlReserva = String.Empty;
            DataSet RrReserva = null;
            try
            {
                //OSCAR C ENE 2005
                //' con el GIDENPAC SACO TODOS LOS DATOS DE LA RESERVA DE LA TABLA
                //sqlReserva = "select * from aresingrva where gidenpac='"
                //sqlReserva = sqlReserva & ObjReservaAdmision.IdentificadorPaciente & "'"
                //sqlReserva = sqlReserva & " and ganoadme is null and gnumadme is null"
                sqlReserva = "select * from aresingrva  " + " WHERE ganoregi=" + ObjReservaAdmision.idReserva.Substring(0, Math.Min(ObjReservaAdmision.idReserva.IndexOf('/'), ObjReservaAdmision.idReserva.Length)) + " AND   gnumregi =" + ObjReservaAdmision.idReserva.Substring(ObjReservaAdmision.idReserva.IndexOf('/') + 1) + " and ganoadme is null and gnumadme is null";
                //---------------

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlReserva, Conexion_Base_datos.RcAdmision);
                RrReserva = new DataSet();
                tempAdapter.Fill(RrReserva);
                ClaseReservasAdmision withVar = null;
                if (RrReserva.Tables[0].Rows.Count == 1)
                {
                    withVar = ObjReservaAdmision;
                    //        .TipoCliente = IIf(Not IsNull(RrReserva("gtipocli")), RrReserva("gtipocli"), "")

                    if (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gplantas"]))
                    {
                        withVar.CodigoCompletoCama = Convert.ToString(Double.Parse(StringsHelper.Format(RrReserva.Tables[0].Rows[0]["gplantas"], "00") + StringsHelper.Format(RrReserva.Tables[0].Rows[0]["ghabitac"], "00")) + Convert.ToDouble(RrReserva.Tables[0].Rows[0]["gcamasbo"]));
                    }

                    withVar.IndicadorRegimenAlojamiento = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["iregaloj"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["iregaloj"]) : "";

                    withVar.A�oRegistro = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["ganoadme"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["ganoadme"]) : "";

                    withVar.NumeroRegistro = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gnumadme"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gnumadme"]) : "";

                    withVar.UnidadReserva = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gunienfe"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gunienfe"]) : "";

                    withVar.ServicioReserva = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gservici"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gservici"]) : "";

                    withVar.MedicoReserva = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gpersona"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gpersona"]) : "";

                    withVar.CodDiagReserva = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gdiaging"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gdiaging"]) : "";

                    withVar.FechaInValDiag = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["finvaldi"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["finvaldi"]) : "";

                    withVar.TexDiagReserva = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["odiaging"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["odiaging"]) : "";

                    //oscar 10/03/2004

                    if (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["cfianza"]))
                    {
                        string tempRefParam = Convert.ToString(RrReserva.Tables[0].Rows[0]["cfianza"]);
                        withVar.fianza = Serrores.ConvertirDecimales(ref tempRefParam, ref vstSeparadorDecimal, 2);
                    }
                    else
                    {
                        withVar.fianza = "";
                    }
                    //oscar 30/03/2004

                    withVar.CodigoProducto = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gproducto"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gproducto"]) : "";
                    //oscar C 23/11/2004

                    withVar.FechaIntervencion = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["finterve"])) ? Convert.ToDateTime(RrReserva.Tables[0].Rows[0]["finterve"]).ToString("dd/MM/yyyy HH:mm") : "";

                    withVar.DiasPrevistosEst = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["ndiasest"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["ndiasest"]) : "";

                    withVar.Observaciones = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["oobserva"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["oobserva"]) : "";

                    withVar.A�oProgramacion = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["ganoprog"])) ? Convert.ToInt32(RrReserva.Tables[0].Rows[0]["ganoprog"]) : 0;

                    withVar.NumeroProgramacion = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gnumprog"])) ? Convert.ToInt32(RrReserva.Tables[0].Rows[0]["gnumprog"]) : 0;
                    //-------
                    //Fernando Silvestre 05/12/2005					
                    withVar.IndicadorPrioridad = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["ipriorid"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["ipriorid"]) : "";
                    //-------
                    //'.usuario  se establece en el aceptar del formulario

                    //O.Frias - 30/06/2009

                    withVar.tpAmbito = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["itipambi"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["itipambi"]) : "";

                    withVar.tpIngreso = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["itipingr"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["itipingr"]) : "";

                    withVar.hosOrigen = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["ghospori"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["ghospori"]) : "0";

                    withVar.serviSoli = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gsersoli"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gsersoli"]) : "";

                    withVar.persoSoli = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gpersoli"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gpersoli"]) : "0";

                    withVar.medSerSoli = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["dmediori"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["dmediori"]) : "";

                    withVar.AnoRegUrge = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["ganourge"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["ganourge"]) : "";

                    withVar.NumRegUrge = (!Convert.IsDBNull(RrReserva.Tables[0].Rows[0]["gnumurge"])) ? Convert.ToString(RrReserva.Tables[0].Rows[0]["gnumurge"]) : "";
                }

                RrReserva.Close();
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:PromemorizarReserva", ex);
            }
        }

        private void proIntroducirIdentificador(int Fila)
        {
            SprPacientes.Row = Fila;
            SprPacientes.Col = Col_FecRealiza;
            ObjReservaAdmision.FechaReservaAtencion = DateTime.Parse(SprPacientes.Text).ToString("dd/MM/yyyy HH:mm");
            SprPacientes.Col = Col_IdPaciente;
            ObjReservaAdmision.IdentificadorPaciente = SprPacientes.Text.Trim();
            SprPacientes.Col = Col_FecIngreso;
            ObjReservaAdmision.FechaPrevistaIngreso = SprPacientes.Text;

            //OSCAR C ENE 2005
            SprPacientes.Col = Col_IDReserva;
            ObjReservaAdmision.idReserva = SprPacientes.Text;
            //-----------
            //Fernando Silvestre 05/12/2005
            SprPacientes.Col = Col_Prioridad;
            string vPrioridad = SprPacientes.Text;
            switch (vPrioridad.ToUpper())
            {
                case "NORMAL":
                    ObjReservaAdmision.IndicadorPrioridad = "N";
                    break;
                case "PREFERENTE":
                    ObjReservaAdmision.IndicadorPrioridad = "S";
                    break;
                default:
                    ObjReservaAdmision.IndicadorPrioridad = "";
                    break;
            }
            //-----------
        }

        public void ProAnularReserva()
        {
            string Nombre = String.Empty;
            DialogResult iresp = (DialogResult)0;

            SqlTransaction transaction = Conexion_Base_datos.RcAdmision.BeginTrans();

            try
            {
                SprPacientes.Row = SprPacientes.ActiveRowIndex;
                SprPacientes.Col = Col_Paciente;
                Nombre = SprPacientes.Text.Trim();
                //OSCAR ENE 2005
                SprPacientes.Col = Col_IDReserva;
                Nombre = Nombre + " (" + SprPacientes.Text + ")";
                //-----------
                iresp = RadMessageBox.Show("�Est� seguro de que desea eliminar la reserva de " + Nombre + "?", Application.ProductName, MessageBoxButtons.OKCancel, RadMessageIcon.Question);
                if (iresp == DialogResult.Cancel)
                { // eliminar reserva
                    return;
                }

                gUltimoPaciente = "";

                //Oscar C Febrero 2012.
                //Se unifica en la dll de reserva la anulacion de reservas
                object ob = null;
                bool lError = false;

                //DGMORENOG TODO_7_3
                /*ob = new reserva.MCRESERVA();
                //UPGRADE_TODO: (1067) Member ProAnularReserva is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                lError = Convert.ToBoolean(ob.ProAnularReserva(Conexion_Base_datos.RcAdmision, ObjReservaAdmision.idReserva.Substring(0, Math.Min(ObjReservaAdmision.idReserva.IndexOf('/'), ObjReservaAdmision.idReserva.Length)).Trim(), ObjReservaAdmision.idReserva.Substring(ObjReservaAdmision.idReserva.IndexOf('/') + 1).Trim(), Conexion_Base_datos.VstCodUsua));
                if (lError)
                {
                    //UPGRADE_ISSUE: (2064) RDO.rdoConnection method RcAdmision.RollbackTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    Conexion_Base_datos.RcAdmision.RollbackTrans(transaction);
                    RadMessageBox.Show("Error al procesar la anulaci�n de la reserva ", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }*/
                ob = null;
                //--------------------

                //''''OSCAR ENE 2005
                //''''sqlEliminarReserva = "select gidenpac from aresingrva where gidenpac='" & ObjReservaAdmision.IdentificadorPaciente & "'"
                //''''sqlGrabarAresanul = "select * from aresingr where gidenpac='" & ObjReservaAdmision.IdentificadorPaciente & "'"
                //'''sqlEliminarReserva = "select gidenpac from aresingrva where " & _
                //''''                     "     ganoregi = " & Trim(Mid(ObjReservaAdmision.idReserva, 1, InStr(1, ObjReservaAdmision.idReserva, "/") - 1)) & _
                //''''                     " AND gnumregi = " & Trim(Mid(ObjReservaAdmision.idReserva, InStr(1, ObjReservaAdmision.idReserva, "/") + 1))
                //'''sqlGrabarAresanul = "select * from aresingrva where " & _
                //''''                     "     ganoregi = " & Trim(Mid(ObjReservaAdmision.idReserva, 1, InStr(1, ObjReservaAdmision.idReserva, "/") - 1)) & _
                //''''                     " AND gnumregi = " & Trim(Mid(ObjReservaAdmision.idReserva, InStr(1, ObjReservaAdmision.idReserva, "/") + 1))
                //''''---------------
                //'''sqlEliminarReserva = sqlEliminarReserva & " and fpreving="
                //'''sqlGrabarAresanul = sqlGrabarAresanul & " and fpreving="
                //'''sqlEliminarReserva = sqlEliminarReserva & FormatFechaHM(ObjReservaAdmision.FechaPrevistaIngreso) & ""
                //'''sqlGrabarAresanul = sqlGrabarAresanul & FormatFechaHM(ObjReservaAdmision.FechaPrevistaIngreso) & ""
                //'''
                //'''Set RrEliminarReserva = RcAdmision.OpenResultset(sqlEliminarReserva, rdOpenKeyset, rdConcurValues)
                //'''Set RrGrabarAresanul = RcAdmision.OpenResultset(sqlGrabarAresanul, rdOpenKeyset, rdConcurValues)
                //'''
                //'''If RrGrabarAresanul.RowCount = 1 Then
                //'''    tstGraba = "select * from aresanul where 2=1"
                //'''        Set trRGraba = RcAdmision.OpenResultset(tstGraba, rdOpenKeyset, rdConcurValues)
                //'''        trRGraba.AddNew
                //'''        trRGraba("gidenpac") = RrGrabarAresanul("gidenpac")
                //'''        trRGraba("fpreving") = RrGrabarAresanul("fpreving")
                //'''        trRGraba("iregaloj") = RrGrabarAresanul("iregaloj")
                //'''        trRGraba("gusuario") = RrGrabarAresanul("gusuario")
                //'''        trRGraba("fanulaci") = Now
                //''''        trRGraba("gplantas") = RrGrabarAresanul("gplantas")
                //''''        trRGraba("ghabitac") = RrGrabarAresanul("ghabitac")
                //''''        trRGraba("gcamasbo") = RrGrabarAresanul("gcamasbo")
                //'''
                //'''        If IsNull(RrGrabarAresanul("gplantas")) Then
                //'''           trRGraba("gplantas") = Null
                //'''        Else
                //'''           trRGraba("gplantas") = RrGrabarAresanul("gplantas")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("ghabitac")) Then
                //'''           trRGraba("ghabitac") = Null
                //'''        Else
                //'''           trRGraba("ghabitac") = RrGrabarAresanul("ghabitac")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("gcamasbo")) Then
                //'''           trRGraba("gcamasbo") = Null
                //'''        Else
                //'''           trRGraba("gcamasbo") = RrGrabarAresanul("gcamasbo")
                //'''        End If
                //'''
                //'''
                //'''
                //'''        If IsNull(RrGrabarAresanul("gservici")) Then
                //'''           trRGraba("gservici") = Null
                //'''        Else
                //'''           trRGraba("gservici") = RrGrabarAresanul("gservici")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("gpersona")) Then
                //'''           trRGraba("gpersona") = Null
                //'''        Else
                //'''           trRGraba("gpersona") = RrGrabarAresanul("gpersona")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("gdiaging")) Then
                //'''           trRGraba("gdiaging") = Null
                //'''        Else
                //'''           trRGraba("gdiaging") = RrGrabarAresanul("gdiaging")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("finvaldi")) Then
                //'''           trRGraba("finvaldi") = Null
                //'''        Else
                //'''           trRGraba("finvaldi") = RrGrabarAresanul("finvaldi")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("odiaging")) Then
                //'''           trRGraba("odiaging") = Null
                //'''        Else
                //'''           trRGraba("odiaging") = RrGrabarAresanul("odiaging")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("gunienfe")) Then
                //'''           trRGraba("gunienfe") = Null
                //'''        Else
                //'''           trRGraba("gunienfe") = RrGrabarAresanul("gunienfe")
                //'''        End If
                //'''        trRGraba("gusualta") = VstCodUsua
                //'''
                //'''
                //'''        'O.Frias - 30/06/2009
                //'''        'Nuevos campos.
                //'''        If IsNull(RrGrabarAresanul("itipingr")) Then
                //'''           trRGraba("itipingr") = Null
                //'''        Else
                //'''           trRGraba("itipingr") = RrGrabarAresanul("itipingr")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("itipambi")) Then
                //'''           trRGraba("itipambi") = Null
                //'''        Else
                //'''           trRGraba("itipambi") = RrGrabarAresanul("itipambi")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("ghospori")) Then
                //'''           trRGraba("ghospori") = Null
                //'''        Else
                //'''           trRGraba("ghospori") = RrGrabarAresanul("ghospori")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("gsersoli")) Then
                //'''           trRGraba("gsersoli") = Null
                //'''        Else
                //'''           trRGraba("gsersoli") = RrGrabarAresanul("gsersoli")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("gpersoli")) Then
                //'''           trRGraba("gpersoli") = Null
                //'''        Else
                //'''           trRGraba("gpersoli") = RrGrabarAresanul("gpersoli")
                //'''        End If
                //'''        If IsNull(RrGrabarAresanul("dmediori")) Then
                //'''           trRGraba("dmediori") = Null
                //'''        Else
                //'''           trRGraba("dmediori") = RrGrabarAresanul("dmediori")
                //'''        End If
                //'''
                //'''        If IsNull(RrGrabarAresanul("ganourge")) Then
                //'''           trRGraba("ganourge") = Null
                //'''        Else
                //'''           trRGraba("ganourge") = RrGrabarAresanul("ganourge")
                //'''        End If
                //'''
                //'''        If IsNull(RrGrabarAresanul("gnumurge")) Then
                //'''           trRGraba("gnumurge") = Null
                //'''        Else
                //'''           trRGraba("gnumurge") = RrGrabarAresanul("gnumurge")
                //'''        End If
                //'''
                //'''       'OSCAR
                //'''       '****************************************************************************
                //'''       'ANULAMOS EL EPISODIO DE AUTORIZACION DE LA ACTIVIDAD PROGRAMADA
                //'''       '****************************************************************************
                //'''        Dim ob As Object
                //'''        Dim lError As Boolean
                //'''        Set ob = CreateObject("Autorizaciones.clsAutorizaciones")
                //'''        lError = ob.fAnulaSolicitud(RcAdmision, "P", "H", _
                //''''                                    RrGrabarAresanul("ganoregi"), RrGrabarAresanul("gnumregi"), _
                //''''                                    VstCodUsua)
                //'''        Set ob = Nothing
                //'''        If lError Then
                //'''            RcAdmision.RollbackTrans
                //'''            Exit Sub
                //'''        End If
                //'''        '****************************************************************************
                //'''
                //'''        trRGraba.Update
                //'''        trRGraba.Close
                //'''
                //'''
                //'''End If
                //'''RrGrabarAresanul.Close
                //'''If RrEliminarReserva.RowCount = 1 Then
                //'''     RrEliminarReserva.Delete
                //'''End If
                //'''RrEliminarReserva.Close
                //'''''
                Conexion_Base_datos.RcAdmision.CommitTrans(transaction);
                menu_reservas_Activated(this, new EventArgs());
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:ProAnularReserva", ex);
                Conexion_Base_datos.RcAdmision.RollbackTrans(transaction);
            }
        }

        // SE PUEDE A�ADIR Y MODIFICAR A LA VEZ PERO NO MODIFICAR O A�ADIR 2 A LA VEZ

        public void proLLamarFiliacion()
        {
            //'Dim formReservas As New Mant_reserva
            string sqlTablaReservas = String.Empty;
            DataSet RrTablaReservas = null;

            try
            {                
                Clase_Filiacion = new filiacionDLL.Filiacion();                
                Clase_Filiacion.Load(Clase_Filiacion, this, "ALTA", "Admision", Conexion_Base_datos.RcAdmision);

                // COMPROBAR QUE NO ESTA YA EN LA TABLA DE RESERVAS
                if (ObjReservaAdmision.IdentificadorPaciente == "")
                {
                    this.Show();
                    return;
                }
                //cuando tenemos el gidenpac del paciente
                // COMPROBAR QUE NO ESTA EN ADMISION, esto no se si tiene sentido hacerlo
                sqlTablaReservas = "select gidenpac from aepisadm where gidenpac='" + ObjReservaAdmision.IdentificadorPaciente + "'";
                sqlTablaReservas = sqlTablaReservas + " and faltplan is null";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTablaReservas, Conexion_Base_datos.RcAdmision);
                RrTablaReservas = new DataSet();
                tempAdapter.Fill(RrTablaReservas);
                if (RrTablaReservas.Tables[0].Rows.Count == 1)
                {
                    RadMessageBox.Show("El paciente se encuentra en admisi�n", Application.ProductName);
                    return;
                }

                //'buscamos las reservas urgentes del paciente
                //sqlTablaReservas = "select gidenpac from aresingrva where gidenpac='" & ObjReservaAdmision.IdentificadorPaciente & "' "
                //sqlTablaReservas = sqlTablaReservas & " and (itipingr = 'U' or itipingr = 'I') "
                //sqlTablaReservas = sqlTablaReservas & " and ganourge is not null "
                //Set RrTablaReservas = RcAdmision.OpenResultset(sqlTablaReservas, rdOpenKeyset)
                //If Not RrTablaReservas.EOF Then
                //    RrTablaReservas.MoveFirst
                //    irespuesta = ClassErrores.RespuestaMensaje(vNomAplicacion, vAyuda, 1125, RcAdmision, "El paciente tiene una reserva urgente", "Modificarla")
                //    If irespuesta = 1 Then
                //    'si es que si
                //        VstOperacionReserva = "MODIFICAR_RESERVA"
                //    Else
                //        VstOperacionReserva = ""
                //    End If
                //Else
                //    VstOperacionReserva = ""
                //End If
                //
                //'si no hay ninguna reserva urgente o no quiere modificarla buscamos el resto de urgencias
                //sqlTablaReservas = "select * from aresingrva where gidenpac='" & ObjReservaAdmision.IdentificadorPaciente & "' "
                //Set RrTablaReservas = RcAdmision.OpenResultset(sqlTablaReservas, rdOpenKeyset)
                //If Not RrTablaReservas.EOF Then
                //    If RrTablaReservas.RowCount = 1 Then
                //        'preguntamos si la quiere modificar
                //        If IsNull(RrTablaReservas("ganourge")) Then
                //            irespuesta = ClassErrores.RespuestaMensaje(vNomAplicacion, vAyuda, 1125, RcAdmision, "El paciente ya tiene una reserva", "Modificarla")
                //            If irespuesta = 6 Then
                //                VstOperacionReserva = "MODIFICAR_RESERVA"
                //            Else
                //                VstOperacionReserva = "A�ADIR_RESERVA"
                //            End If
                //        Else
                //            VstOperacionReserva = "A�ADIR_RESERVA"
                //        End If
                //    Else
                //        'cargamos la pantalla con reservas
                //        VstOperacionReserva = "VISUALIZAR_RESERVA"
                //    End If
                //Else
                //
                //End If
                //If Trim(VstOperacionReserva) <> "" Then
                // CARGAR LA PANTALLA DE MATENIMIENTO DE RESERVAS PARA A�ADIR UNA NUEVA

                //DGMORENOG TODO_7_3
                /*object obManteReservas = null;
                VstOperacionReserva = "A�ADIR_RESERVA";
                ObjReservaAdmision.OperacionReserva = VstOperacionReserva;
                ObjReservaAdmision.usuario = Conexion_Base_datos.VstCodUsua;
                obManteReservas = new reserva.MCRESERVA();
                //UPGRADE_TODO: (1067) Member Mante_Reserva is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                obManteReservas.Mante_Reserva(this, Conexion_Base_datos.RcAdmision, ObjReservaAdmision);
                obManteReservas = null;*/
                //End If

                //UPGRADE_WARNING: (2065) Form event menu_reservas.Activated has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
                menu_reservas_Activated(this, new EventArgs());
                //-----------
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "menu_reservas:ProLlamarFiliacion", ex);
            }

        }

        private void ProOrdenarFilas(int columna)
        {
            //Static bSort1 As Boolean, bSort2 As Boolean, bSort3 As Boolean            
            SprPacientes.Row = 1;
            SprPacientes.Row2 = SprPacientes.MaxRows;
            SprPacientes.Col = Col_Paciente;
            SprPacientes.Col2 = SprPacientes.MaxCols;
            SprPacientes.SortBy = SS_SORT_BY_ROW; //Ordeno por filas

            //O.Frias - 30/06/2009
            //Se podr� ordenar por cualquier columna.

            //'    Select Case columna
            //'      Case 1
            //'          SprPacientes.SortKey(1) = 1  ' COLUMNA CLAVE
            //'          If bSort1 Then    ' si ya estaba ordenado ascendentemente
            //'             SprPacientes.SortKeyOrder(1) = SS_SORT_ORDER_ASCENDING
            //'             ORDEN_PACIENTE = SS_SORT_ORDER_ASCENDING
            //'             bSort1 = False
            //'          Else
            //'             SprPacientes.SortKeyOrder(1) = SS_SORT_ORDER_DESCENDING
            //'             ORDEN_PACIENTE = SS_SORT_ORDER_DESCENDING
            //'             bSort1 = True
            //'          End If
            //'      Case 3
            //'          SprPacientes.SortKey(1) = 3 ' COLUMNA CLAVE
            //'          If bSort3 = True Then ' si ya estaba ordenado ascendentemente
            //'              SprPacientes.SortKeyOrder(1) = SS_SORT_ORDER_ASCENDING
            //'              ORDEN_CAMA = SS_SORT_ORDER_ASCENDING
            //'              bSort3 = False
            //'          Else
            //'              SprPacientes.SortKeyOrder(1) = SS_SORT_ORDER_DESCENDING
            //'              ORDEN_CAMA = SS_SORT_ORDER_DESCENDING
            //'              bSort3 = True
            //'          End If
            //'    End Select

            //--SprPacientes.SortKey(1) = columna  ' COLUMNA CLAVE

            if (columna == Col_FecRealiza)
            {
                columna = Col_OrdenFecRealiza;
            }
            else if (columna == Col_FecIngreso)
            {
                columna = Col_OrdenFecIngreso;
            }
            else if (columna == Col_FecInterven)
            {
                columna = Col_OrdenFecInterven;
            }

            if (SprPacientes.GetSortKey(1) != columna)
            { // si ya estaba ordenado ascendentemente
                SprPacientes.SetSortKey(1, columna);
                SprPacientes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)SS_SORT_ORDER_ASCENDING);

                ORDEN_PACIENTE = (UpgradeHelpers.Spread.SortKeyOrderConstants)SS_SORT_ORDER_ASCENDING;
                bSort1 = false;
            }
            else
            {
                SprPacientes.SetSortKey(1, columna);
                if (SprPacientes.GetSortKeyOrder(1) == UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending)
                {
                    SprPacientes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)SS_SORT_ORDER_ASCENDING);
                }
                else
                {
                    SprPacientes.SetSortKeyOrder(1, SS_SORT_ORDER_DESCENDING);
                }
                ORDEN_PACIENTE = SS_SORT_ORDER_DESCENDING;
                bSort1 = true;
            }
            SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
            SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionDeselectBlock;
        }

        public int posicionarpacienteCama(string texto)
        {
            int inf = 0;
            int sup = 0;
            int med = 0;
            string contiene = String.Empty;
            if (rbPaciente.IsChecked)
            {
                SprPacientes.Row = 1;
                SprPacientes.Col = Col_Paciente;
                SprPacientes.Row2 = SprPacientes.MaxRows;
                SprPacientes.Col2 = SprPacientes.MaxCols;
                SprPacientes.SortDescriptors.Clear();
                SprPacientes.SetSortKey(1, 1);
                SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending);
                SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
                SprPacientes.Col = Col_Paciente;
            }
            else
            {
                SprPacientes.Row = 1;
                SprPacientes.Col = Col_Paciente;
                SprPacientes.Row2 = SprPacientes.MaxRows;
                SprPacientes.Col2 = SprPacientes.MaxCols;
                SprPacientes.SortDescriptors.Clear();
                SprPacientes.SetSortKey(1, 3);
                SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending);
                SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
                SprPacientes.Col = Col_Cama;
            }
            if (texto != "")
            {
                inf = 1;
                sup = SprPacientes.GetLastNonEmptyRow(null);
                do
                {
                    med = (inf + sup) / ((int)2);
                    SprPacientes.Row = med;
                    if (String.Compare(SprPacientes.Text.ToLower(), texto.ToLower(), true) == -1)
                    {
                        inf = med + 1;
                    }
                    else if (String.Compare(SprPacientes.Text.ToLower(), texto.ToLower(), true) == 1)
                    {
                        sup = med - 1;
                    }
                    else
                    {
                        sup = inf - 1;
                    }
                }
                while (sup >= inf);
                if (String.CompareOrdinal(SprPacientes.Text.ToLower(), texto.ToLower()) > 0)
                {
                    if (SprPacientes.Row == 0)
                    {
                        SprPacientes.Row = 1;
                        return SprPacientes.Row;
                    }
                }
                if (SprPacientes.Text.ToLower() != texto.ToLower())
                {
                    if (SprPacientes.Text.Substring(0, Math.Min(texto.Trim().Length, SprPacientes.Text.Length)).ToLower() == texto.ToLower())
                    {
                        return SprPacientes.Row;
                    }

                    SprPacientes.Row++;

                    if (SprPacientes.Row > SprPacientes.GetLastNonEmptyRow(null))
                    {
                        SprPacientes.Row = SprPacientes.GetLastNonEmptyRow(null);
                        return SprPacientes.Row;
                    }
                    contiene = SprPacientes.Text.ToLower().Substring(0, Math.Min(texto.ToLower().Length, SprPacientes.Text.ToLower().Length));
                    if (contiene != texto.ToLower())
                    {
                        //SprPacientes.Row--;
                        if (SprPacientes.Row == 0)
                        {
                            SprPacientes.Row = 1;
                            return SprPacientes.Row;
                        }
                    }
                }
            }
            else
            {
                SprPacientes.Row = -1;
            }
            return SprPacientes.Row;
        }

        private string ObtenerFinaciadora(int Cod)
        {
            string sql = "select dsocieda from dsocieda where gsocieda = " + Cod.ToString() + "";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {
                return Convert.ToString(RrSql.Tables[0].Rows[0]["dsocieda"]).Trim().ToUpper();
            }
            else
            {
                return "";
            }
        }

        private string Obtenerunidad(string Cod)
        {
            string result = String.Empty;
            string sql = "select dunidenf from dunienfe where gunidenf = '" + Cod.Trim() + "'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {
                result = Convert.ToString(RrSql.Tables[0].Rows[0]["dunidenf"]).Trim().ToUpper();
            }
            else
            {
                result = "";
            }
            
            RrSql.Close();
            return result;
        }

        //OSCAR ENE 2005
        private string ObtenerInspeccion(string Cod)
        {
            string result = String.Empty;
            string sql = "select dinspecc from dinspecc where ginspecc = '" + Cod + "'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {
                result = Convert.ToString(RrSql.Tables[0].Rows[0]["dinspecc"]).Trim().ToUpper();
            }
            else
            {
                result = "";
            }

            RrSql.Close();
            return result;
        }

        private void proPeticionHistoria()
        {
            //PETICION DE HISTORIA CLINICA
            filiacionDLL.Filiacion ClassFiliacion = null;
            PeticionHistoriaClinica.ClassHTC120F1 classPeticion = null;
            Mensajes.ClassMensajes ClassErrores = null;
            string lidentificador = String.Empty;
            DataSet RrHistoria = null;
            string sqlhistoria = String.Empty;
            string sql = String.Empty;
            DataSet RrDatoshistoria = null;

            if (!bRegistroSeleccionado)
            {
                stCodPacE = "";
                
                ClassFiliacion = new filiacionDLL.Filiacion();                
                ClassFiliacion.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                ClassFiliacion = null;

                if (stCodPacE.Trim() == "")
                {
                    return;
                }
                lidentificador = stCodPacE;
            }
            else
            {
                SprPacientes.Col = Col_IdPaciente;
                lidentificador = SprPacientes.Text;
            }

            if (lidentificador.Trim() != "")
            {
                sqlhistoria = "select gidenpac from hdossier where gidenpac='" + lidentificador + "'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlhistoria, Conexion_Base_datos.RcAdmision);
                RrHistoria = new DataSet();
                tempAdapter.Fill(RrHistoria);
                if (RrHistoria.Tables[0].Rows.Count == 0)
                { // si no tiene historia
                    ClassErrores = new Mensajes.ClassMensajes();
                    short tempRefParam = 1100;
                    string[] tempRefParam2 = new string[] { "Dossier" };
                    ClassErrores.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2);
                    return;
                    ClassErrores = null;
                }
                else
                {
                    sql = "select ghistoria,icontenido,gserpropiet from hdossier where gidenpac='" + lidentificador + "'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RrDatoshistoria = new DataSet();
                    tempAdapter_2.Fill(RrDatoshistoria);

                    int ICodServicioArchivo = Convert.ToInt32(RrDatoshistoria.Tables[0].Rows[0]["gserpropiet"]);
                    string StContenido = "A";
                    string VarNHistoria = RrDatoshistoria.Tables[0].Rows[0]["ghistoria"].ToString();
                    string VarIAutomatica = "N";
                    if (RrDatoshistoria.Tables[0].Rows.Count == 2)
                    { // si tiene en icontenido "P" y "H"                        
                        classPeticion = new PeticionHistoriaClinica.ClassHTC120F1();                        
                        classPeticion.ReferenciasClass(classPeticion, this, Conexion_Base_datos.RcAdmision);                        
                        classPeticion.Recibir(Path.GetDirectoryName(Application.ExecutablePath), "H",ref ICodServicioArchivo, ref StContenido, lidentificador,ref VarNHistoria,ref VarIAutomatica, "U");
                        classPeticion = null;
                    }
                    else
                    {
                        if (RrDatoshistoria.Tables[0].Rows.Count == 1)
                        { // si tiene en icontenido "P" y "H"                            
                            classPeticion = new PeticionHistoriaClinica.ClassHTC120F1();                            
                            classPeticion.ReferenciasClass(classPeticion, this, Conexion_Base_datos.RcAdmision);
                            StContenido = RrDatoshistoria.Tables[0].Rows[0]["icontenido"].ToString();
                            VarIAutomatica = "U";
                            classPeticion.Recibir(Path.GetDirectoryName(Application.ExecutablePath), "H",ref ICodServicioArchivo,ref StContenido, lidentificador,ref VarNHistoria, ref VarIAutomatica);
                            classPeticion = null;
                        }
                    }
                }
                RrHistoria = null;
            }
        }

        public void Cancelado(object ferror = null)
        {
            //NO SE PA'Q SIRVE
        }

        public void RecogerPeticion(int error, object Prestado = null)
        {
            // procedimiento de recogida de la dll PETICION HISTORIA CLINICA
            Mensajes.ClassMensajes ClassErrores = new Mensajes.ClassMensajes();
            if (error != 0)
            {
                short tempRefParam = 1860;
                string[] tempRefParam2 = new string[] { "Petici�n de Historia no" };
                ClassErrores.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2);
            }
        }

        public void RecogerIDreserva(string ParaIdReserva)
        {
            gUltimoPaciente = ParaIdReserva;
        }
        //-------------
        private string fPersona(int intPersona)
        {
            string result = String.Empty;
            result = "";
            string stSql = "SELECT * FROM DPERSONA WHERE gpersona = " + intPersona.ToString();
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            DataSet rdoTemp = new DataSet();
            tempAdapter.Fill(rdoTemp);
            if (rdoTemp.Tables[0].Rows.Count != 0)
            {
                result = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap1pers"]) + "").Trim() + " " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap2pers"]) + "").Trim() + ", " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnompers"]) + "").Trim();
            }
            rdoTemp.Close();

            return result;
        }

        private string sValorColumna(UpgradeHelpers.Spread.FpSpread sprRejilla, int lColumna)
        {
            string result = String.Empty;
            int lColRestaurar = sprRejilla.Col;
            sprRejilla.Col = lColumna;
            result = sprRejilla.Text.Trim();
            sprRejilla.Col = lColRestaurar;
            return result;
        }

        private void SprPacientes_RowFormatting(object sender, Telerik.WinControls.UI.RowFormattingEventArgs e)
        {
            //e.RowElement.RowInfo.Height = 10;            
        }
    }
}