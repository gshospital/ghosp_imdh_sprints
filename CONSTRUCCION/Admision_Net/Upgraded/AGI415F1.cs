using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using CrystalWrapper;

namespace ADMISION
{
    public partial class movimientos_pacientes
        : RadForm
    {

        public movimientos_pacientes()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            isInitializingComponent = true;
            InitializeComponent();
            isInitializingComponent = false;
            ReLoadForm(false);
        }


        private void movimientos_pacientes_Activated(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
            {
                UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form)eventSender;
            }
        }
        string stSql = String.Empty;
        DataSet RrAMOVIMIE = null;
        string stSeleccion = String.Empty;
        int viform = 0;
        string stRango_fechas = String.Empty;
        DataSet rrtemp = new DataSet();
        DataSet ds = new DataSet();
        DataTable rrTabla = new DataTable();
        private Crystal CrystalReport1 = null;


        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {
            string LitPersona = String.Empty;
            string LitPersona1 = String.Empty;
            try
            {
                if (!pro_valida_fechas())
                {
                    return;
                }
                stRango_fechas = SdcInicio.Text + " y " + SdcFin.Text;
                this.Cursor = Cursors.WaitCursor;
                CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi415r1_CR11.rpt";
                proFormulas(viform); //limpio las formulas
                if (RbServicio.IsChecked)
                {
                    proServicio();
                }
                if (RbMedico.IsChecked)
                {
                    proMedico();
                }


                //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //*******************

                //CrystalReport1.Formulas[0] = "FORM1= \"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                CrystalReport1.Formulas["FORM1"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                //CrystalReport1.Formulas[1] = "FORM2= \"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                CrystalReport1.Formulas["FORM2"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                //CrystalReport1.Formulas[2] = "FORM3= \"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                CrystalReport1.Formulas["FORM3"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.ToUpper();
                //CrystalReport1.Formulas[3] = "Litpers= \"" + LitPersona + "\"";
                CrystalReport1.Formulas["Litpers"] = "\"" + LitPersona + "\"";

                LitPersona1 = "CAMBIOS DE SERVICIO Y M�DICO DE " + LitPersona.ToUpper() + "S";
                //CrystalReport1.Formulas[4] = "LitpersTitulo= \"" + LitPersona1 + "\"";
                CrystalReport1.Formulas["LitpersTitulo"] = "\"" + LitPersona1 + "\"";


                CrystalReport1.WindowTitle = "Listado de Movimientos";
                CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                string CadenaCrystal = "";
                string Cadenacrystal2 = "";
                Serrores.CrearCadenaDSN(ref CadenaCrystal, ref Cadenacrystal2);
                //CrystalReport1.Connect = "DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "";
                CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + "; " + Conexion_Base_datos.gContrase�a + ";";
                CrystalReport1.Destination = Parasalida;
                CrystalReport1.WindowShowPrintSetupBtn = true;

                if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                }
                CrystalReport1.Action = 1;


                CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                viform = 5;
                Conexion_Base_datos.vacia_formulas(CrystalReport1, viform);
                this.Cursor = Cursors.Default;
                stSeleccion = "";
                RbServicio.IsChecked = false;
                RbMedico.IsChecked = false;
                CbPantalla.Enabled = false;
                CbImprimir.Enabled = false;
            }
            catch (SqlException e)
            {
                this.Cursor = Cursors.Default;
                if (e.Number == 32755)
                {
                    //CANCELADA IMPRESION
                }
                else
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "movimiento_pacientes:ProImprimir", e);
                }
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

        private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
            menu_admision.DefInstance.Show();
        }

        private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
            //proImprimir(null);
        }

        private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToWindow);
            //proImprimir(null);
        }

        private bool isInitializingComponent;
        private void RbMedico_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (((RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }

                if (RbMedico.IsChecked)
                {
                    CbPantalla.Enabled = true;
                    CbImprimir.Enabled = true;
                }
            }
        }

        private void RbMedico_Leave(Object eventSender, EventArgs eventArgs)
        {
            if (RbMedico.IsChecked)
            {
                if (RbServicio.IsChecked)
                {
                    CbPantalla.Enabled = false;
                    CbImprimir.Enabled = false;
                }
            }

        }

        private void RbServicio_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (((RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                if (RbServicio.IsChecked)
                {
                    CbPantalla.Enabled = true;
                    CbImprimir.Enabled = true;
                }

            }
        }

        private void RbServicio_Leave(Object eventSender, EventArgs eventArgs)
        {
            if (RbServicio.IsChecked)
            {
                if (RbMedico.IsChecked)
                {
                    CbPantalla.Enabled = false;
                    CbImprimir.Enabled = false;
                }
            }
        }

        private void movimientos_pacientes_Load(Object eventSender, EventArgs eventArgs)
        {
            CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;


            //    Me.Height = 2355
            //    Me.Width = 3990
            CbImprimir.Enabled = false;
            CbPantalla.Enabled = false;
            RbServicio.IsChecked = false;
            RbMedico.IsChecked = false;

            string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
            LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
            movimientos_pacientes.DefInstance.Text = "Cambios de Servicio/M�dico de " + LitPersona + " - AGI415F1";
        }
        public bool pro_valida_fechas()
        {
            bool result = false;
            string stMensaje2 = String.Empty;
            string stMensaje3 = String.Empty;
            result = true;
            if (SdcInicio.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
            {
                stMensaje2 = "menor o igual";
                stMensaje3 = "Fecha del Sistema";

                short tempRefParam = 1020;
                string[] tempRefParam2 = new string[] { LbFechainicio.Text, stMensaje2, stMensaje3 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                SdcInicio.Focus();
                return false;
            }
            if (SdcFin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
            {
                stMensaje2 = "menor o igual";
                stMensaje3 = "Fecha del Sistema";

                short tempRefParam3 = 1020;
                string[] tempRefParam4 = new string[] { LbFechafin.Text, stMensaje2, stMensaje3 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                SdcFin.Focus();
                return false;
            }
            if (SdcFin.Value.Date < SdcInicio.Value.Date)
            {
                stMensaje2 = "mayor o igual";

                short tempRefParam5 = 1020;
                string[] tempRefParam6 = new string[] { LbFechafin.Text, stMensaje2, LbFechainicio.Text };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                SdcFin.Focus();
                return false;
            }
            return result;
        }
        public void proServicio()
        {
            try
            {

                stSeleccion = "Cambio de Servicio entre " + stRango_fechas;
                object tempRefParam = SdcInicio.Text + " 00:00:00";
                object tempRefParam2 = SdcFin.Text + " 23:59:59";
                stSql = " SELECT AMOVIMIE.ffinmovi, AEPISADM.fllegada, DSERVICI.dnomserv,DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers, B.dnompers, B.dap1pers, B.dap2pers, A.dnomserv, DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac"
                    + " FROM AMOVIMIE, AEPISADM, DPACIENT, DSERVICI, DPERSONA, " + " DSERVICI AS A, DPERSONA AS B " + " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " + " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " AMOVIMIE.gservior <> AMOVIMIE.gservide AND " + " AMOVIMIE.gservior = DSERVICI.gservici AND " + " AMOVIMIE.gservide = A.gservici AND " + " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " + " AMOVIMIE.gmedicde = B.gpersona and " + " amovimie.ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " amovimie.ffinmovi<=" + Serrores.FormatFechaHMS(tempRefParam2) + "\r" + "\n" + " ORDER BY DAPE1PAC,DAPE2PAC,DNOMBPAC,ffinmovi ASC ";

                //        & " AEPISADM.faltplan IS NULL AND "
                //    CrystalReport1.SortFields(0) = "+{dpacient.dape1pac}"
                //    CrystalReport1.SortFields(1) = "+{dpacient.dape2pac}"
                //    CrystalReport1.SortFields(2) = "+{dpacient.dnombpac}"
                //    CrystalReport1.SortFields(3) = "+{Amovimie.ffinmovi}"

                //CrystalReport1.Formulas[3] = "SELECCION= \"" + stSeleccion + "\"";
                CrystalReport1.Formulas["SELECCION"] = "\"" + stSeleccion + "\"";
                CrystalReport1.SQLQuery = stSql;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "movimiento_pacientes:ProServicio", ex);
            }
        }
        public void proMedico()
        {
            try
            {

                stSeleccion = "Cambio de M�dico entre " + stRango_fechas;
                object tempRefParam = SdcInicio.Text + " 00:00:00";
                object tempRefParam2 = SdcFin.Text + " 23:59:59";
                stSql = " SELECT AMOVIMIE.ffinmovi, AEPISADM.fllegada, DSERVICI.dnomserv,DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers, B.dnompers, B.dap1pers, B.dap2pers, A.dnomserv, DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac"
                    + " FROM AMOVIMIE, AEPISADM, DPACIENT, DSERVICI, DPERSONA, " + " DSERVICI AS A, DPERSONA AS B " + " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " + " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " AMOVIMIE.gservior = DSERVICI.gservici AND " + " AMOVIMIE.gservide = A.gservici AND " + " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " + " AMOVIMIE.gmedicde = B.gpersona AND " + " AMOVIMIE.gmedicor <> AMOVIMIE.gmedicde and " + " amovimie.ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " amovimie.ffinmovi<=" + Serrores.FormatFechaHMS(tempRefParam2) + "\r" + "\n" + " ORDER BY DAPE1PAC,DAPE2PAC,DNOMBPAC,ffinmovi ASC ";

                CrystalReport1.Formulas["SELECCION"] = "\"" + stSeleccion + "\"";
                CrystalReport1.SQLQuery = stSql;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "movimiento_pacientes:ProMedico", ex);
            }
        }
        public void proFormulas(int idForm)
        {
            int tiForm = 0;
            foreach (var key in CrystalReport1.Formulas.Keys)
            {
                if (tiForm <= idForm)
                    CrystalReport1.Formulas[key] = "\"" + "" + "\"";

                ++tiForm;
            }
        }
        private void movimientos_pacientes_Closed(Object eventSender, EventArgs eventArgs)
        {
        }
    }
}