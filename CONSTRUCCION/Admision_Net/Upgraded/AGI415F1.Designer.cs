using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class movimientos_pacientes
	{

		#region "Upgrade Support "
		private static movimientos_pacientes m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static movimientos_pacientes DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new movimientos_pacientes();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbImprimir", "CbCerrar", "CbPantalla", "SdcInicio", "SdcFin", "LbFechafin", "LbFechainicio", "Frame2", "RbMedico", "RbServicio", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		public Telerik.WinControls.UI.RadDateTimePicker SdcInicio;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFin;
		public Telerik.WinControls.UI.RadLabel LbFechafin;
		public Telerik.WinControls.UI.RadLabel LbFechainicio;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadRadioButton RbMedico;
		public Telerik.WinControls.UI.RadRadioButton RbServicio;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcInicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.LbFechafin = new Telerik.WinControls.UI.RadLabel();
            this.LbFechainicio = new Telerik.WinControls.UI.RadLabel();
            this.RbMedico = new Telerik.WinControls.UI.RadRadioButton();
            this.RbServicio = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechafin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechainicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbMedico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbImprimir.Location = new System.Drawing.Point(70, 96);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 3;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbCerrar.Location = new System.Drawing.Point(246, 96);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 2;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbPantalla.Location = new System.Drawing.Point(158, 96);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(81, 29);
            this.CbPantalla.TabIndex = 1;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.RbMedico);
            this.Frame1.Controls.Add(this.RbServicio);
            this.Frame1.HeaderText = "Seleccione tipo de cambio";
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(328, 89);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Seleccione tipo de cambio";
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.SdcInicio);
            this.Frame2.Controls.Add(this.SdcFin);
            this.Frame2.Controls.Add(this.LbFechafin);
            this.Frame2.Controls.Add(this.LbFechainicio);
            this.Frame2.HeaderText = "Fecha del movimiento";
            this.Frame2.Location = new System.Drawing.Point(134, 12);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(191, 69);
            this.Frame2.TabIndex = 6;
            this.Frame2.Text = "Fecha del movimiento";
            // 
            // SdcInicio
            // 
            this.SdcInicio.CustomFormat = "dd/MM/yyyy";
            this.SdcInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcInicio.Location = new System.Drawing.Point(79, 15);
            this.SdcInicio.Name = "SdcInicio";
            this.SdcInicio.Size = new System.Drawing.Size(104, 20);
            this.SdcInicio.TabIndex = 8;
            this.SdcInicio.TabStop = false;
            this.SdcInicio.Text = "22/04/16";
            this.SdcInicio.Value = new System.DateTime(2016, 4, 22, 10, 28, 34, 351);
            // 
            // SdcFin
            // 
            this.SdcFin.CustomFormat = "dd/MM/yyyy";
            this.SdcFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFin.Location = new System.Drawing.Point(78, 42);
            this.SdcFin.Name = "SdcFin";
            this.SdcFin.Size = new System.Drawing.Size(104, 20);
            this.SdcFin.TabIndex = 10;
            this.SdcFin.TabStop = false;
            this.SdcFin.Text = "22/04/16";
            this.SdcFin.Value = new System.DateTime(2016, 4, 22, 10, 28, 34, 359);
            // 
            // LbFechafin
            // 
            this.LbFechafin.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechafin.Location = new System.Drawing.Point(11, 45);
            this.LbFechafin.Name = "LbFechafin";
            this.LbFechafin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechafin.Size = new System.Drawing.Size(55, 18);
            this.LbFechafin.TabIndex = 9;
            this.LbFechafin.Text = "Fecha Fin:";
            // 
            // LbFechainicio
            // 
            this.LbFechainicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechainicio.Location = new System.Drawing.Point(12, 18);
            this.LbFechainicio.Name = "LbFechainicio";
            this.LbFechainicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechainicio.Size = new System.Drawing.Size(67, 18);
            this.LbFechainicio.TabIndex = 7;
            this.LbFechainicio.Text = "Fecha Inicio:";
            // 
            // RbMedico
            // 
            //this.RbMedico.BackColor = System.Drawing.SystemColors.Control;
            this.RbMedico.Cursor = System.Windows.Forms.Cursors.Default;
            //this.RbMedico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RbMedico.Location = new System.Drawing.Point(8, 56);
            this.RbMedico.Name = "RbMedico";
            this.RbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbMedico.Size = new System.Drawing.Size(115, 18);
            this.RbMedico.TabIndex = 5;
            this.RbMedico.Text = "Cambio de m�dico";
            this.RbMedico.CheckStateChanged += new System.EventHandler(this.RbMedico_CheckedChanged);
            this.RbMedico.Leave += new System.EventHandler(this.RbMedico_Leave);
            // 
            // RbServicio
            // 
            this.RbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            //this.RbServicio.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RbServicio.Location = new System.Drawing.Point(8, 24);
            this.RbServicio.Name = "RbServicio";
            this.RbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbServicio.Size = new System.Drawing.Size(115, 18);
            this.RbServicio.TabIndex = 4;
            this.RbServicio.Text = "Cambio de servicio";
            this.RbServicio.CheckStateChanged += new System.EventHandler(this.RbServicio_CheckedChanged);
            this.RbServicio.Leave += new System.EventHandler(this.RbServicio_Leave);
            // 
            // movimientos_pacientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(330, 126);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbPantalla);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Name = "movimientos_pacientes";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TopMost = true;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Cambios de Servicio/M�dico de pacientes - AGI415F1";
            this.Closed += new System.EventHandler(this.movimientos_pacientes_Closed);
            this.Load += new System.EventHandler(this.movimientos_pacientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechafin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechainicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbMedico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			//this.Show();
		}
		#endregion
	}
}