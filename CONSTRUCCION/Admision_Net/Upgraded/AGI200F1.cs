using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using SelEpisodio;
using CrystalWrapper;

namespace ADMISION
{
    public partial class menu_alta
        : Telerik.WinControls.UI.RadForm
    {

        bool Ipasaprimeravez = false;
        string vstNombre = String.Empty;
        string vstApellido1 = String.Empty;
        string vstApellido2 = String.Empty;
        string vstAsegurado = String.Empty;
        string vstCodigosoc = String.Empty;
        string vstHistoria = String.Empty;
        string vstDNI = String.Empty;
        string vvarIdpaciente = String.Empty;
        int lNumero_Filas = 0;
        string vstGidenpac = String.Empty;
        object nuevaalta = null;
        int Vstultima_fila = 0;
        string iPlanta = String.Empty;
        string ihab = String.Empty;
        string stcama = String.Empty;
        string stFecha_Admision = String.Empty;
        string stServicio = String.Empty;
        string stFecha_Planta = String.Empty;
        string stMotivo = String.Empty;
        string stHospital = String.Empty;
        string stDiagnostico1 = String.Empty;
        string stDiagnostico2 = String.Empty;
        string stDiagnostico3 = String.Empty;
        string stFecha_Llegada = String.Empty;
        string stGpersona = String.Empty;
        string stMensaje1 = String.Empty;
        string stMensaje2 = String.Empty;
        bool tIndicador = false;
        bool VstSeleccionado = false;
        string actualpaciente = String.Empty;
        int NaltaAno = 0;
        int NaltaNum = 0;

        string stAnoadme = String.Empty;
        string stNumadme = String.Empty;
        public Crystal control_Cristal = null;
        public OpenFileDialog control_DialogOpen = null;
        public SaveFileDialog control_DialogSave = null;
        public FontDialog control_DialogFont = null;
        public ColorDialog control_DialogColor = null;
        public PrintDialog control_DialogPrint = null;
        bool fLoad = false; //si es true se acaba de cargar la pantalla y no ha pasado por el activate
        bool AnulAlta = false; //confirma si la spreat esta vacia

        bool bPermitirCambiarAlta = false;

        int vstAnoadme = 0;
        int vstNumadme = 0;
        public int TECLAFUNCION = 0;

        public menu_alta()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
            ReLoadForm(false);
        }
        private void Menu_alta_PreviewKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
            int hotKey = IMDHOPEN.TeclaFuncionOpen(Conexion_Base_datos.RcAdmision);
            if (e.KeyCode == (Keys)Enum.Parse(typeof(Keys), hotKey.ToString()))
            {
                if (SprPacientes.ActiveRowIndex > 0)
                {
                    SprPacientes.Row = SprPacientes.ActiveRowIndex;
                    string Episodio = sValorColumna(SprPacientes, 4);
                    string Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                    string ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                    Episodio = "H" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                    string Paciente = sValorColumna(SprPacientes, 5);

                    string tempRefParam = this.Name;
                    string tempRefParam2 = "sprPacientes";
                    bool tempRefParam3 = true;
                    IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, tempRefParam, tempRefParam2, Paciente, Episodio, tempRefParam3, 0, 0);
                    IMDHOPEN = null;
                }
            }
        }

        public void procarga_grid_paciente2(UpgradeHelpers.Spread.FpSpread rejilla)
        {
            string stSql = String.Empty;
            DataSet RR = null;
            int c = 0;
            string hora = String.Empty;
            Color Color;
            FixedLengthString ALTAAMIS = new FixedLengthString(1);

            SprPacientes.BeginUpdate();
            try
            {
                ALTAAMIS.Value = ObtenerAltaAdmis(Conexion_Base_datos.RcAdmision);
                //***************************************************************************
                // Carga la tabla con los pacientes que encuentra en la consulta SQL
                //***************************************************************************
                hora = "00:00:00";
                stSql = " SELECT DAPE1PAC,DAPE2PAC,DNOMBPAC,GNUMADME,GANOADME,DPACIENT.GIDENPAC, FALTPLAN , faltaadm " +
                        " From DPACIENT , AEPISADM Where AEPISADM.gidenpac = DPACIENT.gidenpac AND ((aepisadm.faltplan is null";
                if (ALTAAMIS.Value == "N")
                {
                    stSql = stSql + " and aepisadm.fingplan is null)";
                }
                else
                {
                    stSql = stSql + ")";
                }
                object tempRefParam = DateTimeHelper.ToString(DateTime.Today) + " " + hora;
                stSql = stSql + " or " +
                        " (aepisadm.faltplan is NOT null  AND " +
                        " (aepisadm.faltaadm is  null OR " +
                        " AEPISADM.faltaadm >=" + Serrores.FormatFechaHMS(tempRefParam) + "" +
                        " )" +
                        " )" +
                        " ) ";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                RR = new DataSet();
                tempAdapter.Fill(RR);
                lNumero_Filas = RR.Tables[0].Rows.Count;
                rejilla.MaxRows = lNumero_Filas;
                               
                int tempForVar = RR.Tables[0].Rows.Count - 1;
                for (int f = 0; f <= tempForVar; f++)
                {

                    c = 1;
                    if (!Convert.IsDBNull(RR.Tables[0].Rows[f]["faltaadm"]))
                    {
                        Color = Conexion_Base_datos.Rojo;
                    }
                    else
                    {
                        if (!Convert.IsDBNull(RR.Tables[0].Rows[f]["FALTPLAN"]))
                        {
                            Color = Conexion_Base_datos.Negro;
                        }
                        else
                        {
                            Color = Conexion_Base_datos.Negro;
                        }
                    }
                    rejilla.Row = f + 1;
                    rejilla.Col = c;
                    //rejilla.ForeColor = ColorTranslator.FromOle(Convert.ToInt32(Color));
                    rejilla.foreColor = Color;
                    rejilla.Text = ObtenerCama(RR.Tables[0].Rows[f]["faltplan"], Convert.ToInt32(RR.Tables[0].Rows[f]["ganoadme"]), Convert.ToInt32(RR.Tables[0].Rows[f]["gnumadme"]));
                    rejilla.Lock = true;
                    c++;

                    rejilla.Row = f + 1;
                    rejilla.Col = c;
                    rejilla.foreColor = Color;
                    //rejilla.ForeColor = ColorTranslator.FromOle(Convert.ToInt32(Color));
                    rejilla.Text = Convert.ToString(RR.Tables[0].Rows[f]["DAPE1PAC"]).Trim() + " " + Convert.ToString(RR.Tables[0].Rows[f]["DAPE2PAC"]).Trim() + ", " + Convert.ToString(RR.Tables[0].Rows[f]["DNOMBPAC"]).Trim();
                    rejilla.Lock = true;
                    c++;

                    rejilla.Row = f + 1;
                    rejilla.Col = c;
                    rejilla.foreColor = Color;
                    //this.SprPacientes.ForeColor(Color.Red); 

                    if (Convert.IsDBNull(RR.Tables[0].Rows[f]["FALTPLAN"]))
                    {
                        rejilla.Text = " ";
                    }
                    else
                    {
                        rejilla.Text = Convert.ToDateTime(RR.Tables[0].Rows[f]["FALTPLAN"]).ToString("dd/MM/yyyy HH:mm:ss").Trim();
                    }
                    rejilla.Lock = true;
                    c++;

                    rejilla.Row = f + 1;
                    rejilla.Col = c;
                    rejilla.foreColor = Color;
                    //rejilla.ForeColor = ColorTranslator.FromOle(Convert.ToInt32(Color));
                    //        rejilla.Text = Trim$(Rr("GANOADME")) + Format(Rr("GNUMADME"), "000000")
                    rejilla.Text = Convert.ToString(RR.Tables[0].Rows[f]["GANOADME"]).Trim() + "/" + Convert.ToString(RR.Tables[0].Rows[f]["GNUMADME"]).Trim();
                    rejilla.Lock = true;
                    c++;

                    rejilla.Row = f + 1;
                    rejilla.Col = c;
                    rejilla.foreColor = Color;
                    //rejilla.ForeColor = ColorTranslator.FromOle(Convert.ToInt32(Color));
                    rejilla.Text = Convert.ToString(RR.Tables[0].Rows[f]["GIDENPAC"]).Trim();
                    rejilla.Lock = true;
                    c++;

                    rejilla.Row = f + 1;
                    rejilla.Col = c;
                    rejilla.foreColor = Color;
                    //rejilla.ForeColor = ColorTranslator.FromOle(Convert.ToInt32(Color));
                    //rejilla.Text = Convert.ToDateTime(RR.Tables[0].Rows[0]["faltaadm"]).ToString("dd/MM/yyyy HH:mm:ss").Trim();
                    rejilla.Text = Convert.ToString(RR.Tables[0].Rows[f]["faltaadm"]).Trim();
                    rejilla.Lock = true;

                    c = 1;
                }

                RR.Close();

                if (rejilla.MaxRows > 0)
                {
                    proSeleccionado();
                    //Else
                    //    VstSeleccionado = False
                }
                SprPacientes.Col = 2;
                SprPacientes.Row = 0;
                SprPacientes_CellClick(SprPacientes, null); // ORDENO POR PACIENTE
                ////rejilla.Refresh
            }
            catch (System.Exception excep)
            {
                MessageBox.Show("Error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
                //iDevolver = GestionErrorBaseDatos(Err.Number, RcAdmision)
            }
            SprPacientes.EndUpdate();
        }

        public void proSeleccionado()
        {
            int x = 1;
            bool Encontrado = false;
            SprPacientes.Col = 4;
            while (x <= SprPacientes.MaxRows && !Encontrado)
            {
                if (SprPacientes.Text.Trim() == actualpaciente.Trim())
                {
                    Encontrado = true;
                }
                else
                {
                    x++;
                    SprPacientes.Row = x;
                }
            }
            SprPacientes.Col = 1;
            if (Encontrado)
            {
                SprPacientes.Row = x;
                SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
                proPac_Sel(x);
            }
            else
            {
                SprPacientes.Row = 1;
                SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
                proPac_Sel(1);
            }
            Vstultima_fila = SprPacientes.Row;

        }


        public void procarga_grid_pacientes(UpgradeHelpers.Spread.FpSpread rejilla)
        {

            //****************************************************************************
            // Para cargar las cabeceras de la tabla
            //****************************************************************************
            int ilongi = 600;
            int x = 1;
            rejilla.MaxCols = 6;
            rejilla.MaxRows = 0;
            rejilla.Row = 0;
            rejilla.Col = x;
            rejilla.Text = "Cama";
            //rejilla.SetColWidth(x, CreateGraphics().MeasureString(rejilla.Text, Font).Width * 15 + ilongi);
            x++;
            rejilla.Col = x;
            //rejilla.Text = "Paciente";
            rejilla.Text = "Primer Apellido Segundo Apellido, Nombre";
            //rejilla.SetColWidth(x, CreateGraphics().MeasureString(rejilla.Text, Font).Width * 15 + ilongi + 3000);
            rejilla.Row = 1;
            rejilla.Text = "Primer Apellido Segundo Apellido, Nombre";
            rejilla.Row = 0;
            x++;
            rejilla.Col = x;
            rejilla.Text = "Fecha de alta en planta";
            //rejilla.SetColWidth(x, CreateGraphics().MeasureString(rejilla.Text, Font).Width * 15 + ilongi);
            x++;
            rejilla.Col = x;
            rejilla.Text = "N� de episodio";
            //rejilla.SetColWidth(x, CreateGraphics().MeasureString(rejilla.Text, Font).Width * 15 + ilongi);
            x++;
            rejilla.Col = x;
            rejilla.Text = "Identificador del Paciente";
            rejilla.SetColWidth(x, 0);
            rejilla.Col = x;
            rejilla.SetColHidden(rejilla.Col, true);
            x++;
            rejilla.Col = x;
            rejilla.Text = "altaadmision";
            rejilla.SetColWidth(x, 0);
            rejilla.Col = x;
            rejilla.SetColHidden(rejilla.Col, true);

        }
        public bool fnCondicionesPac(int Fila, string tipo)
        {
            //en este evento obtenemos los datos del paciente seleccionado
            DataSet rrtemp = null;
            string sqltemp = String.Empty;
            bool bVuelta = true;
            tIndicador = false;
            SprPacientes.Row = Fila;
            SprPacientes.Col = 4;
            int stCortar = (SprPacientes.Text.IndexOf('/') + 1);
            string stAnoadme = SprPacientes.Text.Substring(0, Math.Min(stCortar - 1, SprPacientes.Text.Length));
            string stNumadme = SprPacientes.Text.Substring(stCortar);
            NaltaAno = Convert.ToInt32(Double.Parse(stAnoadme));
            NaltaNum = Convert.ToInt32(Double.Parse(stNumadme));
            vstGidenpac = fngidenpac(stAnoadme, stNumadme);
            if (tipo == "Alta")
            {
                sqltemp = " SELECT * " + " From AEPISADM" + " Where " + " AEPISADM.ganoadme=" + NaltaAno.ToString() + " and AEPISADM.gnumadme=" + NaltaNum.ToString() + " and (faltplan is null or " + " faltaadm is null)";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count == 0 && !bPermitirCambiarAlta)
                {
                    //MsgBox ("Error:paciente dado de alta")
                    stMensaje1 = "Dar el Alta";
                    stMensaje2 = "ya est� dado de alta";
                    short tempRefParam = 1310;
                    string[] tempRefParam2 = new string[] { stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    rrtemp.Close();
                    procarga_grid_paciente2(SprPacientes);
                    bVuelta = false;
                }
                else
                {
                    rrtemp.Close();
                }
            }
            else
            {
                sqltemp = " SELECT * " + " From AEPISADM" + " Where " + " AEPISADM.ganoadme=" + stAnoadme + " and AEPISADM.gnumadme=" + stNumadme + " and faltplan is not null";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter_2.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count == 0)
                {
                    //MsgBox ("Error:paciente esta ingresado")
                    stMensaje1 = "Anular el Alta";
                    stMensaje2 = " ingresado actualmente";
                    short tempRefParam3 = 1310;
                    string[] tempRefParam4 = new string[] { stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                    rrtemp.Close();
                    procarga_grid_paciente2(SprPacientes);
                    bVuelta = false;
                }
                else
                {
                    rrtemp.Close();
                }
            }
            return bVuelta;
        }

        public void cbRefrescar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Cursor = Cursors.WaitCursor;
            procarga_grid_pacientes(SprPacientes);
            refresco();
            this.Cursor = Cursors.Default;
        }

        private void Command1_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void menu_alta_Activated(Object eventSender, EventArgs eventArgs)
        {
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                Application.DoEvents();
                int n = Application.OpenForms.Count;
                ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
                string tempRefParam = this.Name;
                IMDHOPEN.EstableceUltimoForm(tempRefParam, Conexion_Base_datos.VstCodUsua);
                _Toolbar1_Button13.VisibleInStrip = IMDHOPEN.TeclaFuncionBotonVisible(Conexion_Base_datos.RcAdmision);
                IMDHOPEN = null;
                _Toolbar1_Button13.Enabled = true;
            }
        }

        public void refresco()
        {
           
            SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;

            procarga_grid_paciente2(SprPacientes);

            //sds
            //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;

            //SprPacientes.ReDraw = True
            //dejo el primero como seleccionado
            SprPacientes_CellClick(SprPacientes, null);
            proPac_Sel(Vstultima_fila);
            SprPacientes.Row = Vstultima_fila;
            SprPacientes.Col = 1;
            SprPacientes_CellClick(SprPacientes, null);
            SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
            //Frame1.Refresh
            tIndicador = false;

            //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;

            VstSeleccionado = true;
        }

        private void menu_alta_Load(Object eventSender, EventArgs eventArgs)
        {
            //Oscar C Febrero 2010
            proRecolocaControles();
            //-------------
            Conexion_Base_datos.DescargaMenuAlta = false; //variable global para controlar la descarga de esta pantalla

            bPermitirCambiarAlta = (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "ICAMBALT", "VALFANU1").Trim().ToUpper() == "S");

            proMenusHa(true);
            //Frame1.Caption = "Pacientes pendientes de confirmar el alta"
            SprPacientes.Visible = true;
            procarga_grid_pacientes(SprPacientes);
            //dejo el primero como seleccionado
            Vstultima_fila = 1;
            fLoad = true;
            refresco();
            Vstultima_fila = 1;
            VstSeleccionado = true;
            actualpaciente = "";
            if (SprPacientes.MaxRows == 0)
            {
                _Toolbar1_Button1.Enabled = false;
                //Toolbar1.Items[0].Enabled = false;
                //  Toolbar1.Buttons(2).Enabled = False
                //    Toolbar1.Buttons(3).Enabled = False
                AnulAlta = false;
                proMenusHa(false);
                menu_admision.DefInstance.mnu_adm012[0].Enabled = false;
                VstSeleccionado = false;
            }

            string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
            LitPersona = LitPersona.ToUpper();
            Label2.Text = LitPersona + "S" + " PENDIENTES DE CONFIRMAR EL ALTA";

            LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + "s";
            LbAltaAdmision.Text = LitPersona + " pendientes de alta en admisi�n";
            LbAltaHospi.Text = LitPersona + " dados de alta en el d�a";

            
            //Toolbar1.Items[0].ToolTipText = "Alta del " + LitPersona.ToLower();
            _Toolbar1_Button1.ToolTipText = "Alta del " + LitPersona.ToLower();
            //Toolbar1.Items[4].ToolTipText = "Consumos por " + LitPersona.ToLower();
            _Toolbar1_Button5.ToolTipText = "Consumos por " + LitPersona.ToLower();

        }

        private void menu_alta_Closed(Object eventSender, EventArgs eventArgs)
        {
            //deshablito los menus
            Conexion_Base_datos.DescargaMenuAlta = true;
            proMenusHa(false);
            VstSeleccionado = false;
        }


        private void SprPacientes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = 0;
            int Row = 0;

            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;

            }
            else
            {
                Col = SprPacientes.Col;
                Row = SprPacientes.Row;
              
            }
            try
            {

                if (Row == 0)
                { //Compruebo que es la cabecera
                    //Selecciono toda la Tabla
                    SprPacientes.Row = Vstultima_fila;
                    SprPacientes.Col = 6;
                    actualpaciente = SprPacientes.Text;
                    SprPacientes.Row = 1;
                    SprPacientes.Col = 1;
                    SprPacientes.Row2 = SprPacientes.MaxRows;
                    SprPacientes.Col2 = 6;

                    SprPacientes.SortBy = UpgradeHelpers.Spread.SortByConstants.SortByRow; //Ordeno por columna
                    switch (Col)
                    {
                        case 1:
                            SprPacientes.SetSortKey(1, 1);
                            SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending);

                            break;
                        case 2:
                            SprPacientes.SetSortKey(1, 2);
                            SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending);

                            break;
                    }
                    SprPacientes.Refresh();
                    SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
                    proSeleccionado();

                   
                }
                else
                {
                    // comprobar las condiciones del paciente
                    //    If fnCondicionesPac(Row) = True Then
                    SprPacientes.Row = Row;
                    SprPacientes.Col = Col;
                    //    If SprPacientes.ForeColor = Negro Or SprPacientes.ForeColor = Negro Then
                    //        Toolbar1.Buttons(1).Enabled = True
                    //    Else
                    //        Toolbar1.Buttons(1).Enabled = False
                    //    End If
                    //    If SprPacientes.ForeColor = Rojo Or SprPacientes.ForeColor = Negro Then
                    //        Toolbar1.Buttons(3).Enabled = True
                    //    Else
                    //        Toolbar1.Buttons(3).Enabled = False
                    //    End If
                    SprPacientes.Col = 6;
                    //si no esta dado de alta en admisi�n habilita dar alta y deshabilito anular el alta
                    if (SprPacientes.Text == "" || bPermitirCambiarAlta)
                    {
                        _Toolbar1_Button1.Enabled = true;
                        // Toolbar1.Items[0].Enabled = true;
                        //            Toolbar1.Buttons(3).Enabled = False
                        AnulAlta = bPermitirCambiarAlta;
                    }
                    else
                    {
                        //si esta dado de alta en admisi�n deshabilita dar alta y habilitar anular alta
                        //Toolbar1.Items[0].Enabled = false;
                        _Toolbar1_Button1.Enabled = false;
                        //            Toolbar1.Buttons(3).Enabled = True
                        AnulAlta = true;
                    }
                    SprPacientes.Col = 3;
                    //si no esta dado de alta en planta deshabilito anular el alta
                    if (SprPacientes.Text == " ")
                    {
                        //            Toolbar1.Buttons(3).Enabled = False
                        AnulAlta = false;
                    }
                    else
                    {
                        //            Toolbar1.Buttons(3).Enabled = True
                        AnulAlta = true;
                    }

                    SprPacientes.Col = Col;
                    // si esta seleccionado lo desselecciono
                    if (Vstultima_fila != Row)
                    {
                        
                        proPac_Sel(Row);
                        VstSeleccionado = true;
                    }
                    else
                    {
                        if (VstSeleccionado)
                        {
                            //sdsalazar para que tome o no la seleccion de la fila 
                            if (eventArgs != null)
                            {
                                SprPacientes.CurrentRow = null;
                            }
                            //   SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly; //=   OperationModeNormal

                            //                Toolbar1.Buttons(3).Enabled = True

                            AnulAlta = true;
                            if (Ipasaprimeravez)
                            {
                                Conexion_Base_datos.Aepisodio = 0; //vacia el episodio del paciente, no se ha seleccionado paciente
                                Conexion_Base_datos.Nepisodio = 0;
                            }
                        }
                        else
                        {
                            
                            //sds
                            //SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
                        }
                        VstSeleccionado = !VstSeleccionado;
                     
                    }
                    Vstultima_fila = Row;
                    if (SprPacientes.MaxRows == 0)
                    {
                        //Toolbar1.Items[0].Enabled = false;
                        _Toolbar1_Button1.Enabled = false;

                        //         Toolbar1.Buttons(2).Enabled = False
                        //            Toolbar1.Buttons(3).Enabled = False
                        AnulAlta = false;
                    }
                    //        SprPacientes.Col = 4
                    //        actualpaciente = SprPacientes.Text
                    //    End If

                    //oscar 05/03/04 (anulacion alta administativa)
                    //Toolbar1.Items[4].Enabled = SprPacientes.foreColor == Conexion_Base_datos.Rojo;
                    _Toolbar1_Button7.Enabled = SprPacientes.foreColor == Conexion_Base_datos.Rojo;
                    //------
                    _Toolbar1_Button13.Enabled = VstSeleccionado;
                    //Toolbar1.Items[9].Enabled = VstSeleccionado;
                   
                }

              
                return;
            }
            catch(Exception ex)
            {
                MessageBox.Show(Information.Err().Number.ToString(), Application.ProductName);
                //    Resume
            }
        }

        private void SprPacientes_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = eventArgs.ColumnIndex + 1; // + 1
            int Row = eventArgs.ColumnIndex + 1; // + 1
            if (Row != 0)
            {
                if (SprPacientes.foreColor == Conexion_Base_datos.Negro || SprPacientes.foreColor == Conexion_Base_datos.Negro)
                {
                    proMnu_ing11(0);
                }
                else
                {
                    _Toolbar1_Button1.Enabled = false;
                }
            }
        }

        public void proMnu_ing11(int Index)
        {
            filiacionDLL.Filiacion Anfiliacion = null;
            DataSet Rr1 = null;
            string stFechaIngreso = String.Empty;
            string sql = String.Empty;
            string sql_Docu = String.Empty;
            DataSet rrDocu = null;
            Anulacion.Alta anulacion_alta = null;
            string vstFecha_fin = String.Empty;


            if (SprPacientes.MaxRows == 0)
            {
                VstSeleccionado = false;
            }

            string stSql = String.Empty;
            DataSet rrtemp = null;
            switch (Index)
            {
                case 0:
                    if (!VstSeleccionado)
                    {
                        short tempRefParam = 1270;
                        string[] tempRefParam2 = new string[] { "un paciente" };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));

                        return;

                    }
                    // Llamada ALTA DE PACIENTE 
                    // Comprobamos que se cumplen las condiciones para la fecha de alta en admision 
                    // y fecha de alta en planta 
                    // Ahora buscamos ya con un gidenpac en concreto 

                    sql = " SELECT FINGPLAN, FALTPLAN, FALTAADM " + " From AEPISADM " + " Where " + " AEPISADM.ganoadme = " + NaltaAno.ToString() + " AND" + " AEPISADM.gnumadme =" + NaltaNum.ToString();
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter.Fill(Rr1);
                    if ((!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["faltplan"]) && !Convert.IsDBNull(Rr1.Tables[0].Rows[0]["faltaadm"])) || (Convert.IsDBNull(Rr1.Tables[0].Rows[0]["fingplan"]) && !Convert.IsDBNull(Rr1.Tables[0].Rows[0]["faltaadm"])))
                    {
                        //MsgBox ("El paciente ya ha sido dado de alta")

                        if (!bPermitirCambiarAlta)
                        {
                            stMensaje1 = "Dar el Alta";
                            stMensaje2 = " dado de Alta actualmente";
                            short tempRefParam3 = 1310;
                            string[] tempRefParam4 = new string[] { stMensaje1, stMensaje2 };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                            Rr1.Close();
                            return;
                        }
                    }
                    if (Convert.IsDBNull(Rr1.Tables[0].Rows[0]["fingplan"]))
                    {
                        stFechaIngreso = "";
                    }
                    else
                    {
                        stFechaIngreso = Convert.ToString(Rr1.Tables[0].Rows[0]["fingplan"]).Trim();
                    }
                    
                    Rr1.Close();

                   control_Cristal = menu_admision.DefInstance.crystalgeneral; 
                   //UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control CommDiag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx 
                   //UPGRADE_ISSUE: (2068) MSComDlg.CommonDialog control_Dialog was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx 
                   control_DialogPrint = menu_admision.DefInstance.CommDiag_imprePrint;
                    // sdsalazar_TODO_X_3
                    /*nuevaalta = new ALTASHOSPI.ALTASHOSP(); 
                    //UPGRADE_TODO: (1067) Member proload is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                    nuevaalta.proload(nuevaalta, this, Conexion_Base_datos.RcAdmision, vstGidenpac.Trim(), vstNombre.Trim(), (iPlanta + ihab + stcama).Trim(), stFecha_Admision.Trim(), stServicio.Trim(), stFecha_Planta.Trim(), stMotivo.Trim(), stHospital.Trim(), stDiagnostico1.Trim(), stDiagnostico2.Trim(), stDiagnostico3.Trim(), stFecha_Llegada.Trim(), stGpersona.Trim(), "A", stFechaIngreso, Conexion_Base_datos.VstCodUsua, NaltaAno, NaltaNum); 
                    nuevaalta = null; 
                    control_Cristal = null; 
                    control_Dialog = null; 
                    //Load Alta_paciente 
                    //Alta_paciente.Show 
                    Vstultima_fila = SprPacientes.Row; 
                    //refresco 
                    //Vstultima_fila = SprPacientes.Row 
                    VstSeleccionado = true; 
                    this.Show(); 
                    fin sdsalazar_TODO_X_3 */
                    break;
                case 2:  // Llamada a DOCUMENTOS 
                    if (!VstSeleccionado)
                    {
                        
						// Llamada a la DLL de FILIACION para seleccionar el paciente del cual queremos
						// obtener los documentos
						Anfiliacion = new filiacionDLL.Filiacion();
						//UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Anfiliacion.Load(Anfiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
						Anfiliacion = null;						
                        this.Show();
                        //*****************************************************************************

                        menu_alta.DefInstance.Tag = vstNombre.Trim() + "$" + vstApellido1.Trim() + "$" + vstApellido2.Trim() + "$" + vstAsegurado.Trim() + "$" + vstCodigosoc.Trim() + "$" + vstHistoria.Trim() + "$" + vvarIdpaciente.Trim() + "$" + vstDNI.Trim();
                        if (Convert.ToString(menu_alta.DefInstance.Tag) == "$$$$$$$")
                        {
                            // Ha pulsado cancelar en la DLL de FILIACION
                            VstSeleccionado = true;
                            return;
                        }
                        else
                        {
                            Conexion_Base_datos.Aepisodio = 0;
                            Conexion_Base_datos.Nepisodio = 0;
                            // Ahora COMPROBAMOS QUE EL PACIENTE HA SIDO DADO DE ALTA EN EL HOSPITAL ******
                            stSql = " SELECT * " + " FROM AEPISADM " + " WHERE AEPISADM.gidenpac=' " + vvarIdpaciente + "' and " + " AEPISADM.faltplan IS NULL ";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                            rrtemp = new DataSet();
                            tempAdapter_2.Fill(rrtemp);
                            if (rrtemp.Tables[0].Rows.Count != 0)
                            {
                                //MsgBox ("El paciente seleccionado no ha sido dado de alta o no ha pasado por admision")
                                stMensaje1 = "obtener Documentos al Alta";
                                stMensaje2 = "Ingresado o no se ha registrado su paso por Admisi�n";
                                short tempRefParam5 = 1310;
                                string[] tempRefParam6 = new string[] { stMensaje1, stMensaje2 };
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                                rrtemp.Close();
                                return;
                            }
                            else
                            {
                                
								Documentos_alta.DefInstance.Tag = Conexion_Base_datos.Rojo.Name;
								Documentos_alta tempLoadForm = Documentos_alta.DefInstance;
								Documentos_alta.DefInstance.Show();
                            }
                        }
                    }
                    else
                    {
                        // ha seleccionado a una persona en el grid

                        sql_Docu = "select dnombpac,dape1pac,dape2pac,ndninifp " + " from dpacient where gidenpac='" + vstGidenpac + "'";
                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql_Docu, Conexion_Base_datos.RcAdmision);
                        rrDocu = new DataSet();
                        tempAdapter_3.Fill(rrDocu);
                        if (!Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["DNOMBPAC"]))
                        {
                            vstNombre = Convert.ToString(rrDocu.Tables[0].Rows[0]["DNOMBPAC"]);
                        }
                        else
                        {
                            vstNombre = " ";
                        }
                        if (!Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["DAPE1PAC"]))
                        {
                            vstApellido1 = Convert.ToString(rrDocu.Tables[0].Rows[0]["DAPE1PAC"]);
                        }
                        else
                        {
                            vstApellido1 = " ";
                        }
                        if (!Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["dape2pac"]))
                        {
                            vstApellido2 = Convert.ToString(rrDocu.Tables[0].Rows[0]["DAPE2PAC"]);
                        }
                        else
                        {
                            vstApellido2 = " ";
                        }
                        if (Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["NDNINIFP"]))
                        {
                            vstDNI = "";
                        }
                        else
                        {
                            vstDNI = Convert.ToString(rrDocu.Tables[0].Rows[0]["NDNINIFP"]);
                        }
                        rrDocu.Close();

                        sql_Docu = " SELECT MAX(FFINMOVI) AS MAXIMO " + " FROM AMOVIFIN " + " WHERE itiposer = 'H' AND ganoregi =" + NaltaAno.ToString() + " AND " + " gnumregi = " + NaltaNum.ToString();
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql_Docu, Conexion_Base_datos.RcAdmision);
                        rrDocu = new DataSet();
                        tempAdapter_4.Fill(rrDocu);
                        sql_Docu = "select amovifin.gsocieda,dsocieda,amovifin.nafiliac  " + " from amovifin,dsocieda " + " WHERE itiposer = 'H' and ganoregi=" + NaltaAno.ToString() + " AND " + " gnumregi = " + NaltaNum.ToString() + " and amovifin.gsocieda=dsocieda.gsocieda and ";

                        if (Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["maximo"]))
                        {
                            sql_Docu = sql_Docu + " amovifin.ffinmovi is null";
                        }
                        else
                        {
                            vstFecha_fin = Convert.ToString(rrDocu.Tables[0].Rows[0]["MAXIMO"]);
                            object tempRefParam7 = vstFecha_fin;
                            sql_Docu = sql_Docu + " amovifin.ffinmovi =" + Serrores.FormatFechaHMS(tempRefParam7) + "";
                            vstFecha_fin = Convert.ToString(tempRefParam7);
                        }

                        rrDocu.Close();

                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql_Docu, Conexion_Base_datos.RcAdmision);
                        rrDocu = new DataSet();
                        tempAdapter_5.Fill(rrDocu);
                        if (rrDocu.Tables[0].Rows.Count != 0)
                        {
                            vstCodigosoc = Convert.ToString(rrDocu.Tables[0].Rows[0]["GSOCIEDA"]);
                            vstAsegurado = Convert.ToString(rrDocu.Tables[0].Rows[0]["nafiliac"]);
                        }
                        rrDocu.Close();
                        vvarIdpaciente = vstGidenpac;
                        vstHistoria = Conexion_Base_datos.fnHistoria(vstGidenpac);

                        menu_alta.DefInstance.Tag = vstNombre.Trim() + "$" + vstApellido1.Trim() + "$" + vstApellido2.Trim() + "$" + vstAsegurado.Trim() + "$" + vstCodigosoc.Trim() + "$" + vstHistoria.Trim() + "$" + vvarIdpaciente.Trim() + "$" + vstDNI.Trim() + "$";


                        SprPacientes.Col = 3;
                        if (SprPacientes.Text != " ")
                        {  
                           Documentos_alta.DefInstance.Tag = Conexion_Base_datos.Rojo.Name;
                        }
                        else
                        {
                           Documentos_alta.DefInstance.Tag = Conexion_Base_datos.Negro.Name;
                        }
                       
                       //'''''''''''''2
                       Documentos_alta tempLoadForm2 = Documentos_alta.DefInstance;
                       //Documentos_alta.Show 1
                       Documentos_alta.DefInstance.Show();
                    }

                    break;
                case 3:  // ANULACION DE ALTAS 
                    if (!VstSeleccionado)
                    {
                        
						// Llamada a la DLL de FILIACION para seleccionar al paciente que quiero
						// anular el alta
						Anfiliacion = new filiacionDLL.Filiacion();
						//UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Anfiliacion.Load(Anfiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
						Anfiliacion = null;
                        
                        this.Show();
                        menu_alta.DefInstance.Tag = vstNombre.Trim() + " " + vstApellido1.Trim() + " " + vstApellido2.Trim() + ";" + vstAsegurado.Trim() + ";" + vstCodigosoc.Trim() + ";" + vstHistoria.Trim() + ";" + vvarIdpaciente.Trim();
                        if (Convert.ToString(menu_alta.DefInstance.Tag).Trim() == ";;;;")
                        {
                            //Load menu_alta
                            //menu_alta.Show
                            return;
                        }
                        else
                        {
                            //Load anu_alta
                            //anu_alta.Show 1
                            anulacion_alta = new Anulacion.Alta();
                            anulacion_alta.recibe_paciente(anulacion_alta, this, ref Conexion_Base_datos.RcAdmision, vstNombre.Trim() + " " +
                                                           vstApellido1.Trim() + " " + vstApellido2.Trim(), vstAsegurado.Trim(), vstCodigosoc.Trim(), vstHistoria.Trim(), vvarIdpaciente.Trim(), Conexion_Base_datos.VstCodUsua, "H");
                            anulacion_alta = null;
                            this.Show();
                        }
                    }
                    else
                    {
                        // ha seleccionado a una persona en el grid
                        sql_Docu = "select dnombpac,dape1pac,dape2pac,ndninifp " + " from dpacient where gidenpac='" + vstGidenpac + "'";
                        SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql_Docu, Conexion_Base_datos.RcAdmision);
                        rrDocu = new DataSet();
                        tempAdapter_6.Fill(rrDocu);
                        vstNombre = Convert.ToString(rrDocu.Tables[0].Rows[0]["DNOMBPAC"]);
                        vstApellido1 = Convert.ToString(rrDocu.Tables[0].Rows[0]["DAPE1PAC"]);
                        vstApellido2 = (Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["DAPE2PAC"])) ? "" : Convert.ToString(rrDocu.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
                        if (Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["NDNINIFP"]))
                        {
                            vstDNI = "";
                        }
                        else
                        {
                            vstDNI = Convert.ToString(rrDocu.Tables[0].Rows[0]["NDNINIFP"]);
                        }
                        rrDocu.Close();
                        sql_Docu = " SELECT MAX(FFINMOVI) AS MAXIMO " + " FROM AMOVIFIN " + " WHERE itiposer = 'H' AND ganoregi =" + NaltaAno.ToString() + " AND " + " gnumregi = " + NaltaNum.ToString();
                        SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql_Docu, Conexion_Base_datos.RcAdmision);
                        rrDocu = new DataSet();
                        tempAdapter_7.Fill(rrDocu);
                        sql_Docu = "select amovifin.gsocieda,dsocieda,amovifin.nafiliac  " + " from amovifin,dsocieda " + " WHERE itiposer = 'H' and ganoregi=" + NaltaAno.ToString() + " AND " + " gnumregi = " + NaltaNum.ToString() + " and amovifin.gsocieda=dsocieda.gsocieda and ";

                        if (Convert.IsDBNull(rrDocu.Tables[0].Rows[0]["maximo"]))
                        {
                            sql_Docu = sql_Docu + " amovifin.ffinmovi is null";
                        }
                        else
                        {
                            vstFecha_fin = Convert.ToString(rrDocu.Tables[0].Rows[0]["MAXIMO"]);
                            object tempRefParam8 = vstFecha_fin;
                            sql_Docu = sql_Docu + " amovifin.ffinmovi =" + Serrores.FormatFechaHMS(tempRefParam8) + "";
                            vstFecha_fin = Convert.ToString(tempRefParam8);
                        }
                        rrDocu.Close();
                        SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sql_Docu, Conexion_Base_datos.RcAdmision);
                        rrDocu = new DataSet();
                        tempAdapter_8.Fill(rrDocu);
                        vstCodigosoc = Convert.ToString(rrDocu.Tables[0].Rows[0]["GSOCIEDA"]);
                        vstAsegurado = Convert.ToString(rrDocu.Tables[0].Rows[0]["nafiliac"]) + "";
                        rrDocu.Close();
                        vvarIdpaciente = vstGidenpac;
                        vstHistoria = Conexion_Base_datos.fnHistoria(vstGidenpac);
                        menu_alta.DefInstance.Tag = vstNombre.Trim() + " " + vstApellido1.Trim() + " " + vstApellido2.Trim() + ";" + vstAsegurado.Trim() + ";" + vstCodigosoc.Trim() + ";" + vstHistoria.Trim() + ";" + vvarIdpaciente.Trim();

                        anulacion_alta = new Anulacion.Alta();
                        anulacion_alta.recibe_paciente(anulacion_alta, this, ref Conexion_Base_datos.RcAdmision, vstNombre.Trim() + " " +
                                                       vstApellido1.Trim() + " " + vstApellido2.Trim(), vstAsegurado.Trim(), vstCodigosoc.Trim(), vstHistoria.Trim(), vvarIdpaciente.Trim(), Conexion_Base_datos.VstCodUsua, "H");
                        anulacion_alta = null;
                        this.Show();
                    }
                    break;
                case 4:
                    //'      OSCAR: SE HA SACADO AL PROCEDIMIENTO PROCONSUMOSPAC PARA QUE PUDIERA 
                    //'              SER LLAMADA DESDE EL MDI MENU_ADMISION SI ESTA LA VENTANA DE 
                    //'              GESTION DE ALTAS ABIERTA 
                    ProConsumosPac();
                    //'      Set CDAyuda = menu_admision.CommDiag_impre 
                    //'      If VstSeleccionado = False Then 
                    //'           Set ClassFiliacion = CreateObject("filiacionDLL.Filiacion") 
                    //'           ClassFiliacion.Load ClassFiliacion, Me, "SELECCION", "Admision", RcAdmision 
                    //'           If menu_alta.Tag = "" Then 
                    //'                    ' Ha pulsado cancelar en la DLL de FILIACION 
                    //'                    VstSeleccionado = False 
                    //'                    Exit Sub 
                    //'           End If 
                    //'       End If 
                    //'      If Trim(vstGidenpac) <> "" Then 'si tiene gidenpac 
                    //'              sql = "select * from aepisadm Where " & _
                    //''                    " aepisadm.gidenpac =  '" & vstGidenpac & "' and " & _
                    //''                    " aepisadm.faltplan is null " 
                    //'              Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset) 
                    //'              If Rrsql.RowCount <> 0 Then 
                    //'                    Call MsgBox("El paciente est� ingresado", vbExclamation + vbOKOnly) 
                    //'              Else 
                    //'                    sql = "select max(fllegada) as fllegada from aepisadm Where  gidenpac = '" & vstGidenpac & "'" 
                    //'                    Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset) 
                    //'                    If IsNull(Rrsql("fllegada")) Then 
                    //'                        Call MsgBox("El paciente no ha estado ingresado", vbExclamation + vbOKOnly) 
                    //'                    Else 
                    //'                        sql = "select * from aepisadm, dmovfact,amovimie Where  aepisadm.gidenpac = '" & vstGidenpac & "' and " & _
                    //''                              "aepisadm.ganoadme = dmovfact.ganoregi and " & _
                    //''                              "aepisadm.gnumadme = dmovfact.gnumregi and " & _
                    //''                              "aepisadm.ganoadme = amovimie.ganoregi and " & _
                    //''                              "aepisadm.gnumadme = amovimie.gnumregi and " & _
                    //''                              "fllegada >= '" & Format(Rrsql("fllegada"), "mm/dd/yyyy hh:nn:00") & " ' and " & _
                    //''                              "fllegada <= '" & Format(Rrsql("fllegada"), "mm/dd/yyyy hh:nn:59") & " ' and " & _
                    //''                              "dmovfact.itiposer = 'H' and " & _
                    //''                              "amovimie.itiposer = 'H' and " & _
                    //''                              "dmovfact.ffechfin = aepisadm.faltplan and " & _
                    //''                              "amovimie.ffinmovi = aepisadm.faltplan and " & _
                    //''                              "dmovfact.gcodeven = 'ES'" 
                    //'                              dfechallegada = Rrsql("fllegada") 
                    //'                        Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset) 
                    //'                        If Rrsql.RowCount = 0 Then 
                    //'                            sql = "select * from aepisadm, amovimie  Where  gidenpac = '" & vstGidenpac & "' and " & _
                    //''                                  "fllegada >= '" & Format(dfechallegada, "mm/dd/yyyy hh:nn:00") & "' and " & _
                    //''                                  "fllegada <= '" & Format(dfechallegada, "mm/dd/yyyy hh:nn:59") & "'" & _
                    //''                                  "aepisadm.ganoadme = amovimie.ganoregi and " & _
                    //''                                  "aepisadm.gnumadme = amovimie.gnumregi and " & _
                    //''                                  "amovimie.itiposer = 'H' and " & _
                    //''                                  "amovimie.ffinmovi = aepisadm.faltplan " 
                    //' 
                    //'                            Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset) 
                    //'                            vstA�oAdmi = Rrsql("ganoadme") 
                    //'                            vstnumAdmi = Rrsql("gnumadme") 
                    //'                            UniEnf = Rrsql("gunenfde") 
                    //'                            If IsNull(Rrsql("fingplan")) Then 
                    //'                                Call MsgBox("Paciente no valorado", vbExclamation + vbOKOnly) 
                    //'                            Else 
                    //'                                Set ClaseConsumos = CreateObject("ConsumosPac.clsConsPac") 
                    //'                                StFechaAlta = FechaAltaEpisodio(CInt(vstA�oAdmi), CLng(vstnumAdmi)) 
                    //'                                If HaPasadoFacturacionMensual(CDate(StFechaAlta), CInt(vstA�oAdmi), CLng(vstnumAdmi)) Then 
                    //'                                    oclass.RespuestaMensaje vNomAplicacion, vAyuda, 1130, RcAdmision, "continuar.Hay", "facturaci�n de este paciente" 
                    //'                                    Me.MousePointer = 0 
                    //'                                    Me.Show 
                    //'                                    Exit Sub 
                    //'                                End If 
                    //'                                'ESTADO_DE_SALA.GRID1.Col = 17 
                    //'                                 ClaseConsumos.ParamConsumos vstGidenpac, "H", CLng(vstnumAdmi), CInt(vstA�oAdmi), _
                    //''                                    Rrsql("fingplan"), RcAdmision, LISTADO_CRYSTAL, CDAyuda, _
                    //''                                     gUsuario, gContrase�a, App.Path, UniEnf, StFechaAlta 
                    //' 
                    //'                            End If 
                    //'                        Else 
                    //'                            If IsNull(Rrsql("ffactura")) Then 
                    //'                               vstA�oAdmi = Rrsql("ganoadme") 
                    //'                                vstnumAdmi = Rrsql("gnumadme") 
                    //'                                UniEnf = Rrsql("gunenfde") 
                    //'                                Set ClaseConsumos = CreateObject("ConsumosPac.clsConsPac") 
                    //'                                StFechaAlta = FechaAltaEpisodio(CInt(vstA�oAdmi), CLng(vstnumAdmi)) 
                    //'                                If HaPasadoFacturacionMensual(CDate(StFechaAlta), CInt(vstA�oAdmi), CLng(vstnumAdmi)) Then 
                    //'                                    oclass.RespuestaMensaje vNomAplicacion, vAyuda, 1130, RcAdmision, "continuar.Hay", "facturaci�n de este paciente" 
                    //'                                    Me.MousePointer = 0 
                    //'                                    Me.Show 
                    //'                                    Exit Sub 
                    //'                                End If 
                    //'                                'ESTADO_DE_SALA.GRID1.Col = 17 
                    //'                                ClaseConsumos.ParamConsumos vstGidenpac, "H", CLng(vstnumAdmi), CInt(vstA�oAdmi), _
                    //''                                Rrsql("fingplan"), RcAdmision, LISTADO_CRYSTAL, CDAyuda, _
                    //''                                gUsuario, gContrase�a, App.Path, UniEnf, StFechaAlta 
                    //'                            Else 
                    //'                                Call MsgBox("El paciente ya est� facturado", vbExclamation + vbOKOnly) 
                    //'                            End If 
                    //'                        End If 
                    //'                    End If 
                    //'            End If 
                    //'            Rrsql.Close 
                    //'         End If 
                    break;
            }

        }



        private void SprPacientes_KeyUp(Object eventSender, KeyEventArgs eventArgs)
        {
            int KeyCode = (int)eventArgs.KeyCode;
            int Shift = (eventArgs.Shift) ? 1 : 0;
            SprPacientes_CellClick(SprPacientes, null);

        }

        private void SprPacientes_MouseUp(Object eventSender, MouseEventArgs eventArgs)
        {
            int Button = (int)eventArgs.Button;
            int Shift = (int)(Control.ModifierKeys & Keys.Shift);
            float x = (float)(eventArgs.X * 15);
            float y = (float)(eventArgs.Y * 15);
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
            int i = 0;
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            string Paciente = String.Empty;
            string Numero = String.Empty;
            string ano = String.Empty;
            string Episodio = String.Empty;
            if (eventArgs.Button == MouseButtons.Right)
            {
                if (SprPacientes.ActiveRowIndex > 0 && VstSeleccionado)
                {
                    UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
                    SprPacientes.Row = SprPacientes.ActiveRowIndex;
                    Episodio = sValorColumna(SprPacientes, 4);
                    Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                    ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                    Episodio = "H" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                    Paciente = sValorColumna(SprPacientes, 5);
                    UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
                    IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
                    string tempRefParam = this.Name;
                    string tempRefParam2 = "sprPacientes";
                    bool tempRefParam3 = false;
                    IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, tempRefParam, tempRefParam2, Paciente, Episodio, tempRefParam3, Mouse.X, Mouse.Y);
                    IMDHOPEN = null;
                }
            }
        }


        private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
        {
            Telerik.WinControls.UI.CommandBarButton Button = (Telerik.WinControls.UI.CommandBarButton)eventSender;
            this.Cursor = Cursors.WaitCursor;
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
            int i = 0;
            string Paciente = String.Empty;
            string Numero = String.Empty;
            string ano = String.Empty;
            string Episodio = String.Empty;
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            switch (radCommandBarStripElement1.Items.IndexOf(Button) + 1)
            {
                case 1:
                    if (tIndicador)
                    {
                        proPac_Sel(SprPacientes.Row);
                    }
                    if (fnCondicionesPac(SprPacientes.Row, "Alta"))
                    {
                        proMnu_ing11(0);
                    }
                    break;
                case 2:
                    proMnu_ing11(2);
                    tIndicador = true;
                    break;
                case 3:
                    if (AnulAlta)
                    {
                       if (SprPacientes.OperationMode != UpgradeHelpers.Spread.OperationModeEnums.ReadOnly)
                       {
                          if (fnCondicionesPac(SprPacientes.Row, "Anulacion"))
                            {
                                proMnu_ing11(3);
                            } 
                       }
                       else
                       {
                           proMnu_ing11(3);
                       }
                       tIndicador = true;

                        if (fnCondicionesPac(SprPacientes.Row, "Anulacion"))
                            {
                                proMnu_ing11(3);
                            }
                        else
                        {
                            proMnu_ing11(3);
                        }

                    }
                    else
                    {
                        menu_alta.DefInstance.proMnu_ing11(3);
                    }
                    break;
                case 4:
                    //presentara la ayuda 
                    //
                    //SDSALAZAR se cambia ya que no cuenta los separadores del toolbar1 
                    proMnu_ing11(4);
                    break;
                case 5:
                    //proMnu_ing11(4);
                    //oscar 05/03/04 
                    //nueva opcion para anular solamente el alta administrativa 
                    //
                    //SDSALAZAR se cambia ya que no cuenta los separadores del toolbar1 
                    ProAnulaAltaadm();
                    break;
                case 6://cambio de entidad 
                    //sdsalazar nuevo case ingresa al nueve en el vb6 
                    menu_admision.DefInstance.mnu_adm012[4].PerformClick();
                    break;
                case 7: //cambio de servicio / medico
                    //ProAnulaAltaadm();
                    //---------- 
                    //oscar 20/12/204 
                    //sdsalazar nuevo case ingresa al nueve en el vb6 
                    menu_admision.DefInstance.mnu_adm012[5].PerformClick();
                    break;
                case 8://mantenimiento datos filiacion 
                    //sdsalazar nuevo case ingresa al nueve en el vb6 
                    ProMantenimientoDatosFiliacion();
                    break;
                case 9://Listados del paciente 
                    ProListadosPacienteAlta();
                    //------
                    break;
                case 10:
                    Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
                    if (SprPacientes.ActiveRowIndex > 0 && VstSeleccionado)
                    {
                        SprPacientes.Row = SprPacientes.ActiveRowIndex;
                        Episodio = sValorColumna(SprPacientes, 4);
                        Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                        ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                        Episodio = "H" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                        Paciente = sValorColumna(SprPacientes, 5);
                        UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
                        IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
                        IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, this.Name, "OpenImdh", Paciente, Episodio, false, Mouse.X, Mouse.Y);
                        IMDHOPEN = null;
                    }

                    ////cambio de servicio / medico 
                    ////menu_admision.DefInstance.mnu_adm012_Click(menu_admision.DefInstance.mnu_adm012, new EventArgs());
                    //menu_admision.DefInstance.mnu_adm012[5].PerformClick();

                    break;
            }
            this.Cursor = Cursors.Default;
        }

        public void Recoger_datos(string Nombre2, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Idpaciente, string CodPostal, string Asegurado, string codPob, string Historia, string CodigoSoc, string Filprov, object IndActPen = null, object Inspec = null)
        {

            vstApellido1 = Apellido1; // comprueba que al menos el primer apellido est� rellenado para saber si se han recogido datos
            vstNombre = Nombre2;
            vstApellido2 = Apellido2;
            vstAsegurado = Asegurado;
            vstCodigosoc = CodigoSoc;
            vstHistoria = Historia;
            vvarIdpaciente = Idpaciente;
            vstDNI = NIF;
        }

        public void proMenusHa(bool fVer)
        {
            //habilito las opciones del menu y los botones del toolbar
            menu_admision.DefInstance.mnu_adm012[1].Enabled = fVer;
            //   menu_admision.mnu_adm012(2).Enabled = fVer
            //   menu_admision.mnu_adm012(3).Enabled = fVer
            menu_admision.DefInstance.mnu_adm012[0].Enabled = !fVer;
        }

        public void proPac_Sel(int lRow)
        {
            //en este evento obtenemos los datos del paciente seleccionado
            DataSet rrtemp = null;
            string sqltemp = String.Empty;


            tIndicador = false;
            //On Error GoTo SprPacientes_ClickERR

            //Toolbar1.Buttons(1).Enabled = True
            if (SprPacientes.MaxRows > 0)
            {
                _Toolbar1_Button2.Enabled = true;
                //Toolbar1.Items[1].Enabled = true;
            }


            //habilito las opciones del menu y los botones del toolbar
            //proMenusHa True

            // Me posiciono en la fila activada
            SprPacientes.Row = lRow;
            SprPacientes.Col = 2;

            if (SprPacientes.Text == "")
            {
                return;
            }

            SprPacientes.Col = 1;

            if (SprPacientes.Text.Length > 0)
            {
                iPlanta = SprPacientes.Text.Substring(0, Math.Min(2, SprPacientes.Text.Length));
                ihab = SprPacientes.Text.Substring(2, Math.Min(2, SprPacientes.Text.Length - 2));
                stcama = SprPacientes.Text.Substring(4, Math.Min(2, SprPacientes.Text.Length - 4));
            }
            else
            {
                // ralopezn 29/04/2016
                // cuando se integre ALTASHOSPI.ALTASHOSP, verificar esta variable.
                stcama = "";
                ihab = "";
                iPlanta = "";
            }

            if (iPlanta == "")
            {
                iPlanta = "null";
            }
            if (ihab == "")
            {
                ihab = "null";
            }
            

            SprPacientes.Col = 4;
            int stCortar = (SprPacientes.Text.IndexOf('/') + 1);
            stAnoadme = SprPacientes.Text.Substring(0, Math.Min(stCortar - 1, SprPacientes.Text.Length));
            stNumadme = SprPacientes.Text.Substring(stCortar);
            vstGidenpac = fngidenpac(stAnoadme, stNumadme);
            NaltaAno = Convert.ToInt32(Double.Parse(stAnoadme));
            NaltaNum = Convert.ToInt32(Double.Parse(stNumadme));
            Conexion_Base_datos.Aepisodio = Convert.ToInt32(Double.Parse(stAnoadme));
            Conexion_Base_datos.Nepisodio = Convert.ToInt32(Double.Parse(stNumadme));

            SprPacientes.Col = 2;
            vstNombre = SprPacientes.Text.Trim();

            SprPacientes.Col = 3;
            stFecha_Planta = SprPacientes.Text.Trim();
            if (stFecha_Planta != "")
            {
                stFecha_Planta = stFecha_Planta;
            }
            // Se abre una consulta para sacar el resto de los datos del paciente seleccionado
            // que necesito
            if (stFecha_Planta != "")
            {
                sqltemp = " SELECT * " + " From AEPISADM " + " Where AEPISADM.ganoadme = " + NaltaAno.ToString() + " AND" + " AEPISADM.gnumadme = " + NaltaNum.ToString();

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter.Fill(rrtemp);
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gserulti"]))
                {
                    stServicio = "";
                }
                else
                {
                    stServicio = Convert.ToString(rrtemp.Tables[0].Rows[0]["gserulti"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gperulti"]))
                {
                    stGpersona = "";
                }
                else
                {
                    stGpersona = Convert.ToString(rrtemp.Tables[0].Rows[0]["gperulti"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gmotalta"]))
                {
                    stMotivo = "";
                }
                else
                {
                    stMotivo = Convert.ToString(rrtemp.Tables[0].Rows[0]["gmotalta"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["ghospita"]))
                {
                    stHospital = "";
                }
                else
                {
                    stHospital = Convert.ToString(rrtemp.Tables[0].Rows[0]["ghospita"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gdiagalt"]))
                {
                    stDiagnostico1 = "";
                }
                else
                {
                    stDiagnostico1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["gdiagalt"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gdiasec1"]))
                {
                    stDiagnostico2 = "";
                }
                else
                {
                    stDiagnostico2 = Convert.ToString(rrtemp.Tables[0].Rows[0]["gdiasec1"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gdiasec2"]))
                {
                    stDiagnostico3 = "";
                }
                else
                {
                    stDiagnostico3 = Convert.ToString(rrtemp.Tables[0].Rows[0]["gdiasec2"]);
                }

                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["fllegada"]))
                {
                    stFecha_Llegada = "";
                }
                else
                {
                    stFecha_Llegada = Convert.ToString(rrtemp.Tables[0].Rows[0]["fllegada"]);
                }
                rrtemp.Close();
                // No tiene fecha de alta en planta *****************************************
            }
            else
            {
                //    sqltemp = " SELECT * " _
                //'          & " From AEPISADM, DCAMASBO" _
                //'          & " Where AEPISADM.gidenpac = '" & vstGidenpac & "' AND" _
                //'          & " AEPISADM.faltplan IS NULL AND " _
                //'          & " AEPISADM.gidenpac *= DCAMASBO.gidenpac AND " _
                //'          & " DCAMASBO.gplantas =" & iplanta & " AND" _
                //'          & " DCAMASBO.ghabitac =" & ihab & " AND " _
                //'          & " DCAMASBO.gcamasbo ='" & stcama & "' " _
                //
                sqltemp = " SELECT * " + " From AEPISADM " + " Where AEPISADM.ganoadme = " + NaltaAno.ToString() + " AND" + " AEPISADM.gnumadme = " + NaltaNum.ToString();

                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter_2.Fill(rrtemp);
                if (!Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["faltplan"]))
                {
                    //MsgBox ("Error:paciente dado de alta")
                    stMensaje1 = "Dar el Alta";
                    stMensaje2 = "ya est� dado de alta";
                    short tempRefParam = 1310;
                    string[] tempRefParam2 = new string[] { stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    rrtemp.Close();
                    return;
                }
                //If IsNull(Rrtemp("fingplan")) Then
                //    Toolbar1.Buttons(1).Enabled = True
                //Else
                //    Toolbar1.Buttons(1).Enabled = False
                //End If
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gserulti"]))
                {
                    stServicio = "";
                }
                else
                {
                    stServicio = Convert.ToString(rrtemp.Tables[0].Rows[0]["gserulti"]);
                }
                if (Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["gperulti"]))
                {
                    stGpersona = "";
                }
                else
                {
                    stGpersona = Convert.ToString(rrtemp.Tables[0].Rows[0]["gperulti"]);
                }
                stFecha_Llegada = Convert.ToString(rrtemp.Tables[0].Rows[0]["fllegada"]);
                rrtemp.Close();
            }


        }
        public string fngidenpac(string ano, string num)
        {
            string result = String.Empty;
            DataSet RrGidenpac = null;
            string SqlGidenpac = String.Empty;
            try
            {
                SqlGidenpac = "select gidenpac from aepisadm " + " where ganoadme=" + Convert.ToInt32(Double.Parse(ano)).ToString() + " and " + " gnumadme= " + Convert.ToInt32(Double.Parse(num)).ToString();
                SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlGidenpac, Conexion_Base_datos.RcAdmision);
                RrGidenpac = new DataSet();
                tempAdapter.Fill(RrGidenpac);
                if (RrGidenpac.Tables[0].Rows.Count == 1)
                {
                    result = Convert.ToString(RrGidenpac.Tables[0].Rows[0]["gidenpac"]);
                }
                else
                {
                    result = "";
                }
                RrGidenpac.Close();
                return result;
            }
            catch
            {
                MessageBox.Show("Error recogiendo gidenpac", Application.ProductName);
                return result;
            }
        }


        private string ObtenerCama(object FechaAltaPlanta, int A�o, int Numero)
        {
            string result = String.Empty;
            string sql = String.Empty;
            if (Convert.IsDBNull(FechaAltaPlanta))
            {
                sql = "select gplantor,ghabitor,gcamaori from amovimie where gnumregi =" + Numero.ToString() +
                      " and ganoregi=" + A�o.ToString() + " and ffinmovi is null " +
                      " and itiposer='H'";
            }
            else
            {
                //object tempRefParam = Conversion.Str(FechaAltaPlanta);
                object tempRefParam = FechaAltaPlanta;
                sql = "select gplantor,ghabitor,gcamaori from amovimie where gnumregi =" + Numero.ToString() +
                      " and ganoregi=" + A�o.ToString() + " and ffinmovi =" + Serrores.FormatFechaHMS(tempRefParam) + " and itiposer='H'";
            }
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RR = new DataSet();
            tempAdapter.Fill(RR);
            if (RR.Tables[0].Rows.Count == 1)
            {
                //result = StringsHelper.Format(RR.Tables[0].Rows[0][0], "00") + StringsHelper.Format(RR.Tables[0].Rows[0][1], "00") + Convert.ToString(RR.Tables[0].Rows[0][2]);
                result =  Convert.ToString(int.Parse(RR.Tables[0].Rows[0][0].ToString()).ToString("D2")) + Convert.ToString(int.Parse(RR.Tables[0].Rows[0][1].ToString()).ToString("D2")) + RR.Tables[0].Rows[0][2].ToString();
            }
            RR.Close();
            return result;
        }
        //''''''''''''''''''''''''''''''''''''''''''''''''''''
        public bool HaPasadoFacturacionMensual(System.DateTime FechaAlta, int vstA�oAdmi, int vstnumAdmi)
        {

            bool result = false;
            try
            {

                string sqlFactura = String.Empty;
                DataSet RrFactura = null;
                sqlFactura = "select max(fperfact) MAXIMAFECHA from DMOVFACT where itiposer='H' and ganoregi=" + vstA�oAdmi.ToString() +
                             " and gnumregi=" + vstnumAdmi.ToString() + " and datepart(month,fperfact)=" + DateAndTime.DatePart("m", FechaAlta,Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1).ToString() +
                             " and datepart(year,fperfact)=" + DateAndTime.DatePart("yyyy", FechaAlta, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1).ToString() + " and ffactura is not null";

                if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
                {
                    string tempRefParam = "DMOVFACT";
                    sqlFactura = sqlFactura + " UNION " +
                                 Serrores.proReplace(ref sqlFactura, tempRefParam, "DMOVFACH") +
                                 " order by 1 desc";
                }

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlFactura, Conexion_Base_datos.RcAdmision);
                RrFactura = new DataSet();
                tempAdapter.Fill(RrFactura);

                result = !(Convert.IsDBNull(RrFactura.Tables[0].Rows[0]["MAXIMAFECHA"]));

                RrFactura.Close();

                return result;
            }
            catch (Exception Ex)
            {
                Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "mnu_esal01:HaPasadoFacturacionMensual", Ex);


                return result;
            }
        }
        //'''''''''''''''''''''''''''


        public string FechaAltaEpisodio(int A�o, int Numero)
        {

            string result = String.Empty;
            result = "";

            string sql = "Select faltplan from  aepisadm ";
            sql = sql + "where ganoadme = " + A�o.ToString() + " and gnumadme = " + Numero.ToString() + " ";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {
                result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["faltplan"])) ? "" : Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm:ss");
            }

            RrSql.Close();
            return result;
        }

        //oscar
        public void MuestraCambioEntidad()
        {
            filiacionDLL.Filiacion vmcFiliacion = null;
            int var = 0;
            if (!VstSeleccionado)
            {
                
               // cambio de entidad de pacientes ya dados de alta
               //llamo a la dll de filiaci�n para buscar al paciente
               vmcFiliacion = new filiacionDLL.Filiacion();               
               vmcFiliacion.Load(vmcFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
               vmcFiliacion = null;
                
                if (vvarIdpaciente != "")
                {                                        
                   // si ha seleccionado un paciente en filiaci�n
                   // mando a la nueva ventana el nombre del paciente
                   EPISODIOS_PACIENTE.DefInstance.LbPaciente.Text = vstApellido1.Trim() + " " + vstApellido2.Trim() + "," + vstNombre.Trim();
                   // en el tag el gidenpac del paciente seleccionado
                   EPISODIOS_PACIENTE.DefInstance.Tag = vvarIdpaciente;
                   EPISODIOS_PACIENTE.DefInstance.ShowDialog();
                   
                    this.Show();
                }
            }
            else
            {
               // mando a la nueva ventana el nombre del paciente
               var = SprPacientes.Col;
               SprPacientes.Col = 2;
               EPISODIOS_PACIENTE.DefInstance.LbPaciente.Text = SprPacientes.Text; //vstNombre
               SprPacientes.Col = var;
               // en el tag el gidenpac del paciente seleccionado
               EPISODIOS_PACIENTE.DefInstance.Tag = vstGidenpac;
               EPISODIOS_PACIENTE.DefInstance.ShowDialog();               
                this.Show();
            }

        }

        public void MuestraCambioServ_Med()
        {
            filiacionDLL.Filiacion vmcFiliacion = null;
            dynamic oCambioServ = null;

            int var = 0;
            if (!VstSeleccionado)
            {                
				//cambio de servicio/m�dico de pacientes ya dados de alta
				//llamo a la dll de filiaci�n para buscar al paciente
				vmcFiliacion = new filiacionDLL.Filiacion();				
				vmcFiliacion.Load(vmcFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
				vmcFiliacion = null;
                
				//Me.Show             
                if (vvarIdpaciente != "")
                {
                 
					// si ha seleccionado un paciente en filiaci�n
					// mando a la nueva ventana el nombre del paciente
					//CAMBIO_SERMED_ALTA.LbPaciente = Trim(vstApellido1) & " " & Trim(vstApellido2) & "," & Trim(vstNombre)
					// en el tag el gidenpac del paciente seleccionado
					//CAMBIO_SERMED_ALTA.Tag = vvarIdpaciente
					//CAMBIO_SERMED_ALTA.Show

					//OSCAR C MARZO 2006
					//EL CAMBIO DE SERVICIO SE HA SACADO A UNA DLL
					oCambioServ = new CambioServicio.clsCambioServicio();
					//UPGRADE_TODO: (1067) Member proRecPacienteAlta is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					oCambioServ.proRecPacienteAlta(Conexion_Base_datos.RcAdmision, vvarIdpaciente, vstApellido1.Trim() + " " + vstApellido2.Trim() + ", " + vstNombre.Trim(), Conexion_Base_datos.VstCodUsua);

					oCambioServ = null;
					//-------------
                  
                }
            }
            else
            {
				// si ha seleccionado un paciente en filiaci�n
				// mando a la nueva ventana el nombre del paciente

				var = SprPacientes.Col;
				SprPacientes.Col = 2;
				//CAMBIO_SERMED_ALTA.LbPaciente = SprPacientes.Text    'vstNombre
				//SprPacientes.Col = var
				// en el tag el gidenpac del paciente seleccionado
				//CAMBIO_SERMED_ALTA.Tag = vstGidenpac
				//CAMBIO_SERMED_ALTA.Show
				//OSCAR C MARZO 2006
				//EL CAMBIO DE SERVICIO SE HA SACADO A UNA DLL
				oCambioServ = new CambioServicio.clsCambioServicio();
				//UPGRADE_TODO: (1067) Member proRecPacienteAlta is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				oCambioServ.proRecPacienteAlta(Conexion_Base_datos.RcAdmision, vstGidenpac, SprPacientes.Text, Conexion_Base_datos.VstCodUsua);
				oCambioServ = null;
				//-------------
				SprPacientes.Col = var;
                
            }
            this.Show();
        }

        public void ProConsumosPac()
        {

            filiacionDLL.Filiacion ClassFiliacion = null;
            string vstIdPaciente = String.Empty;
            ConsumosPac.clsConsPac ClaseConsumos = null;
            string StFechaAlta = String.Empty;
            System.DateTime dfechallegada = DateTime.FromOADate(0);
            int vstA�oAdmi = 0;
            int vstnumAdmi = 0;
            string UniEnf = String.Empty;

            if (!VstSeleccionado)
            {                
				ClassFiliacion = new filiacionDLL.Filiacion();
				//UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ClassFiliacion.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
				ClassFiliacion = null;
                
                if (vvarIdpaciente.Trim() != "")
                { //si tiene gidenpac
                    vstIdPaciente = vvarIdpaciente;
                }
                else
                {
                    return;
                }

            }
            else
            {

                if (vstGidenpac.Trim() != "")
                { //si tiene gidenpac
                    vstIdPaciente = vstGidenpac;
                }
                else
                {
                    return;
                }

            }



            //          busca si tiene datos
            string sql = "select * from aepisadm Where " +
                         " aepisadm.gidenpac =  '" + vstIdPaciente + "' and " +
                         " aepisadm.faltplan is null ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {
                RadMessageBox.Show("El paciente est� ingresado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
            else
            {
                sql = "select max(fllegada) as fllegada from aepisadm Where  gidenpac = '" + vstIdPaciente + "'";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                RrSql = new DataSet();
                tempAdapter_2.Fill(RrSql);
                if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fllegada"]))
                {
                    RadMessageBox.Show("El paciente no ha estado ingresado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
                else
                {
                    sql = "select * from aepisadm, dmovfact,amovimie Where  aepisadm.gidenpac = '" + vstIdPaciente + "' and " +
                          "aepisadm.ganoadme = dmovfact.ganoregi and " +
                          "aepisadm.gnumadme = dmovfact.gnumregi and " +
                          "aepisadm.ganoadme = amovimie.ganoregi and " +
                          "aepisadm.gnumadme = amovimie.gnumregi and " +
                          "fllegada >= '" + StringsHelper.Format(RrSql.Tables[0].Rows[0]["fllegada"], "MM/dd/yyyy HH:mm:00") + " ' and " +
                          "fllegada <= '" + StringsHelper.Format(RrSql.Tables[0].Rows[0]["fllegada"], "MM/dd/yyyy HH:mm:59") + " ' and " +
                          "dmovfact.itiposer = 'H' and " +
                          "amovimie.itiposer = 'H' and " +
                          "dmovfact.ffechfin = aepisadm.faltplan and " +
                          "amovimie.ffinmovi = aepisadm.faltplan and " +
                          "dmovfact.gcodeven = 'ES'";

                    if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
                    {
                        string tempRefParam = "DMOVFACT";
                        sql = sql + " UNION ALL " + Serrores.proReplace(ref sql, tempRefParam, "DMOVFACH");
                    }

                    dfechallegada = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]);
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RrSql = new DataSet();
                    tempAdapter_3.Fill(RrSql);
                    if (RrSql.Tables[0].Rows.Count == 0)
                    {
                        sql = "select * from aepisadm, amovimie  Where  gidenpac = '" + vstIdPaciente + "' and " +
                              "fllegada >= '" + StringsHelper.Format(dfechallegada, "MM/dd/yyyy HH:mm:00") + "' and " +
                              "fllegada <= '" + StringsHelper.Format(dfechallegada, "MM/dd/yyyy HH:mm:59") + "' AND " +
                              "aepisadm.ganoadme = amovimie.ganoregi and " +
                              "aepisadm.gnumadme = amovimie.gnumregi and " +
                              "amovimie.itiposer = 'H' and " +
                              "amovimie.ffinmovi = aepisadm.faltplan ";

                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                        RrSql = new DataSet();
                        tempAdapter_4.Fill(RrSql);
                        vstA�oAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["ganoadme"]);
                        vstnumAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["gnumadme"]);
                        UniEnf = Convert.ToString(RrSql.Tables[0].Rows[0]["gunenfde"]);
                        //If IsNull(Rrsql("fingplan")) Then
                        //        Call MsgBox("Paciente no valorado", vbExclamation + vbOKOnly)
                        //Else
                        
                        ClaseConsumos = new ConsumosPac.clsConsPac();                        
                        StFechaAlta = FechaAltaEpisodio(vstA�oAdmi, vstnumAdmi);
                        if (HaPasadoFacturacionMensual(DateTime.Parse(StFechaAlta), vstA�oAdmi, vstnumAdmi))
                        {
                            short tempRefParam2 = 1130;
                            string[] tempRefParam3 = new string[] { "continuar.Hay", "facturaci�n de este paciente" };
                            Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam2, Conexion_Base_datos.RcAdmision, tempRefParam3);
                            this.Cursor = Cursors.Default;
                            this.Show();
                            return;
                        }
                        
                       //ESTADO_DE_SALA.GRID1.Col = 17
                       //UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control CommDiag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx
                       //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                       //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                       //UPGRADE_TODO: (1067) Member ParamConsumos is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                       ClaseConsumos.ParamConsumos(vstIdPaciente, "H", vstnumAdmi, vstA�oAdmi, (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fingplan"])) ? Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"].ToString()) : Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fingplan"].ToString()), Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, menu_admision.DefInstance.CommDiag_imprePrint, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), UniEnf, Convert.ToDateTime(StFechaAlta));

                       //End If                       
                    }
                    else
                    {
                        if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ffactura"]))
                        {
                            vstA�oAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["ganoadme"]);
                            vstnumAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["gnumadme"]);
                            UniEnf = Convert.ToString(RrSql.Tables[0].Rows[0]["gunenfde"]);
                            
                            ClaseConsumos = new ConsumosPac.clsConsPac();
                             
                            StFechaAlta = FechaAltaEpisodio(vstA�oAdmi, vstnumAdmi);
                            if (HaPasadoFacturacionMensual(DateTime.Parse(StFechaAlta), vstA�oAdmi, vstnumAdmi))
                            {
                                short tempRefParam4 = 1130;
                                string[] tempRefParam5 = new string[] { "continuar.Hay", "facturaci�n de este paciente" };
                                Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, Conexion_Base_datos.RcAdmision, tempRefParam5);
                                this.Cursor = Cursors.Default;
                                this.Show();
                                return;
                            }
                            //UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control CommDiag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx
                            //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                            //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                            //UPGRADE_TODO: (1067) Member ParamConsumos is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                            
                            ClaseConsumos.ParamConsumos(vstIdPaciente, "H", vstnumAdmi, vstA�oAdmi, (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fingplan"])) ? Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"].ToString()) : Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fingplan"].ToString()), Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, menu_admision.DefInstance.CommDiag_imprePrint, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), UniEnf, Convert.ToDateTime(StFechaAlta));
                           
                            ClaseConsumos = null;
                        }
                        else
                        {
                            RadMessageBox.Show("El paciente ya est� facturado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        }
                    }
                }
            }
            RrSql.Close();


        }


        //05/03/04
        private void ProAnulaAltaadm()
        {
            SprPacientes.SortDescriptors.Clear();
            SprPacientes.Col = 4;
            SprPacientes.Row = SprPacientes.ActiveRowIndex;
            int pos = (SprPacientes.Text.IndexOf('/'));
            int Lanoadme = Convert.ToInt32(SprPacientes.Text.ToString().Substring(0, pos));
            int Lnumadme = Convert.ToInt32(SprPacientes.Text.Substring(pos + 1));
            string sql = " UPDATE AEPISADM SET FALTAADM=NULL WHERE" +
                         " GANOADME=" + Lanoadme.ToString() + " AND " +
                         " GNUMADME=" + Lnumadme.ToString();
            SqlCommand tempCommand = new SqlCommand(sql, Conexion_Base_datos.RcAdmision);
            tempCommand.ExecuteNonQuery();
            refresco();
            this.Show();
        }
        //--------
        public string ObtenerAltaAdmis(SqlConnection RrConexion)
        {
            //para ver que tengo q pintar en el literal de paciente

            string result = String.Empty;
            result = "N";

            string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'ALTADMIS' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, RrConexion);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                result = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
            }
            rrtemp.Close();

            return result;
        }

        //oscar 20/12/2004
        public object ProMantenimientoDatosFiliacion()
        {
            filiacionDLL.Filiacion vmcFiliacion = null;
            string LId = String.Empty;
            
            if (!VstSeleccionado)
			{
				vmcFiliacion = new filiacionDLL.Filiacion();				
				vmcFiliacion.Load(vmcFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
				vmcFiliacion = null;
				if (vvarIdpaciente != "")
				{
					LId = vvarIdpaciente;
				}
			}
			else
			{
				LId = vstGidenpac;
			}
			if (LId != "")
			{
				vmcFiliacion = new filiacionDLL.Filiacion();				
				vmcFiliacion.Load(vmcFiliacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, LId);
				vmcFiliacion = null;
			}            
            cbRefrescar_Click(cbRefrescar, new EventArgs());
            this.Show();

            return null;
        }



        public object ProListadosPacienteAlta()
        {
            MovimientosPac.MCMovimientosPac mcListadosPac = null;
            object ClassFiliacion = null;
            MCSelEpisodio mcSelEpisodios = null;
            int lganoregi = 0;
            int lGnumregi = 0;
            string lnombpac = String.Empty;
            string lidenpac = String.Empty;
            string stSql = String.Empty;
            DataSet RrSql = null;

            bool bbuscapac = true;

            if (VstSeleccionado)
            {
                SprPacientes.Col = 3;
                //si no esta dado de alta en planta
                bbuscapac = SprPacientes.Text == " ";
            }


            if (!bbuscapac)
            {

                SprPacientes.Col = 2;
                lnombpac = SprPacientes.Text;

                SprPacientes.Col = 4;
                lganoregi = Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(0, Math.Min(SprPacientes.Text.IndexOf('/'), SprPacientes.Text.Length))));
                lGnumregi = Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(SprPacientes.Text.IndexOf('/') + 1)));

                SprPacientes.Col = 5;
                lidenpac = SprPacientes.Text;

            }
            else
            {
                filiacionDLL.Filiacion ClassFiliacion1 = new filiacionDLL.Filiacion();				
				ClassFiliacion1.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
				ClassFiliacion1 = null;
               
                if (vvarIdpaciente.Trim() != "")
                {
                    lidenpac = vvarIdpaciente;
                    lnombpac = vstApellido1 + " " + vstApellido2 + ", " + vstNombre;
                    stSql = " SELECT * FROM AEPISADM WHERE faltplan is not null and gidenpac = '" + lidenpac + "' ";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    RrSql = new DataSet();
                    tempAdapter.Fill(RrSql);
                    if (RrSql.Tables[0].Rows.Count == 0)
                    {
                        stSql = "select * from aepisadm where AEPISADM.gidenpac ='" + lidenpac + "'";
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        RrSql = new DataSet();
                        tempAdapter_2.Fill(RrSql);
                        if (RrSql.Tables[0].Rows.Count == 0)
                        { //EL paciente no ha estado ingresado nunca
                            RadMessageBox.Show("Este paciente no ha estado ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                            return null;
                        }
                        else
                        {
                            RadMessageBox.Show("Este paciente esta ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                            return null;
                        }
                    }
                    else
                    {
                        if (RrSql.Tables[0].Rows.Count == 1)
                        {
                            lganoregi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Ganoadme"]);
                            lGnumregi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Gnumadme"]);
                        }
                        else
                        {
                            
							//hay m�s de un episodio para ese paciente,
							//muestra los episodios para sacar los listados
							//de un episodio en concreto
							mcSelEpisodios = new SelEpisodio.MCSelEpisodio();
							mcSelEpisodios.Llamada(this, Conexion_Base_datos.RcAdmision, lidenpac, Conexion_Base_datos.VstCodUsua, "A");
							mcSelEpisodios = null;
							if (vstAnoadme == 0 && vstNumadme == 0)
                            {
                                return null;
                            }

                            else
                            {
                                lganoregi = vstAnoadme;
                                lGnumregi = vstNumadme;
                            }

                        }

                    }

                    RrSql.Close();

                }

            }

            if (lidenpac != "")
            {
                
				mcListadosPac = new MovimientosPac.MCMovimientosPac();
                Conexion_Base_datos.LISTADO_CRYSTAL.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a;				
                mcListadosPac.load(ref Conexion_Base_datos.RcAdmision, lidenpac, lnombpac, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, lganoregi, lGnumregi);
                mcListadosPac = null;                 
            }

            this.Show();
            return null;
        }
        //------------

        public void RecogerEpisodio(int A�oRegistro, int NumeroRegistro, string FechaAlta, int iServicio, string fllegada)
        {
            //en este procedimiento recojo los datos que manda la pantalla de seleccionar episodio
            vstAnoadme = A�oRegistro;
            vstNumadme = NumeroRegistro;
        }

        //Oscar C Febrero 2010
        private void proRecolocaControles()
        {
            this.Top = (int)0;
            this.Left = (int)0;
            this.Height = (int)menu_admision.DefInstance.ClientRectangle.Height;
            this.Width = (int)menu_admision.DefInstance.ClientRectangle.Width - 20;
            Label2.Width = (int)(this.ClientRectangle.Width - 20);
            SprPacientes.Width = (int)(this.ClientRectangle.Width - 20);
            //SprPacientes.Height = (int) (this.ClientRectangle.Height - 133);
            SprPacientes.Height = (int)(this.ClientRectangle.Height - 200);
            Frame3.Top = (int)(SprPacientes.Height + 67);
            cbRefrescar.Top = (int)Frame3.Top;
            Command1.Top = (int)Frame3.Top;
            Command1.Left = (int)(this.ClientRectangle.Width - Command1.Width - 20);
            cbRefrescar.Left = (int)(Command1.Left - Command1.Width - 20);
        }
        //-------------
        private string sValorColumna(UpgradeHelpers.Spread.FpSpread sprRejilla, int lColumna)
        {
            string result = String.Empty;
            int lColRestaurar = sprRejilla.Col;
            sprRejilla.Col = lColumna;
            result = sprRejilla.Text.Trim();
            sprRejilla.Col = lColRestaurar;
            return result;
        }

        public void ProConvierteEpisodioHtoHD()
        {

            filiacionDLL.Filiacion ClassFiliacion = null;
            dynamic mcSelEpisodios = null;
            int lganoregi = 0;
            int lGnumregi = 0;
            string lidenpac = String.Empty;
            string stSql = String.Empty;
            DataSet RrSql = null;


            bool bbuscapac = true;
            if (VstSeleccionado)
            {
                SprPacientes.Col = 3;
                //si no esta dado de alta en planta
                if (SprPacientes.Text == " ")
                {
                    //bbuscapac = True
                    RadMessageBox.Show("El Paciente no ha sido dado de alta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
                else
                {
                    bbuscapac = false;
                }
            }


            if (!bbuscapac)
            {
                SprPacientes.Col = 4;
                lganoregi = Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(0, Math.Min(SprPacientes.Text.IndexOf('/'), SprPacientes.Text.Length))));
                lGnumregi = Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(SprPacientes.Text.IndexOf('/') + 1)));
            }
            else
            {                
				ClassFiliacion = new filiacionDLL.Filiacion();
				//UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ClassFiliacion.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
				ClassFiliacion = null;
                
                if (vvarIdpaciente.Trim() != "")
                {
                    lidenpac = vvarIdpaciente;
                    stSql = " SELECT * FROM AEPISADM WHERE faltplan is null and gidenpac = '" + lidenpac + "' and ganoadme>1900";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    RrSql = new DataSet();
                    tempAdapter.Fill(RrSql);
                    if (RrSql.Tables[0].Rows.Count > 0)
                    {
                        RadMessageBox.Show("Este paciente esta ingresado.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    stSql = "select * from aepisadm WHERE faltplan is not null and gidenpac = '" + lidenpac + "' and ganoadme>1900";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    RrSql = new DataSet();
                    tempAdapter_2.Fill(RrSql);
                    if (RrSql.Tables[0].Rows.Count == 0)
                    {
                        RadMessageBox.Show("El paciente seleccionado no tiene Episodios de Hospitalizaci�n de alta.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    else if (RrSql.Tables[0].Rows.Count == 1)
                    {
                        lganoregi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Ganoadme"]);
                        lGnumregi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Gnumadme"]);
                    }
                    else
                    {
                        
						//hay m�s de un episodio de alta
						mcSelEpisodios = new SelEpisodio.MCSelEpisodio();
						mcSelEpisodios.Llamada(this, Conexion_Base_datos.RcAdmision, lidenpac, Conexion_Base_datos.VstCodUsua, "A");
						mcSelEpisodios = null;
                        if (vstAnoadme == 0 && vstNumadme == 0)
                        {
                            return;
                        }
                        else
                        {
                            lganoregi = vstAnoadme;
                            lGnumregi = vstNumadme;
                        }
                    }
                    RrSql.Close();
                }
            }

            if (lganoregi < 1900)
            {
                if (lganoregi > 0)
                {
                    RadMessageBox.Show("El episodio seleccionado ya es un episodio de Hospital de D�a. No se puede convertir.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                }
                return;
            }
            else
            {                                
                ConversionHDaH tempLoadForm = ConversionHDaH.DefInstance;
                ConversionHaHD.DefInstance.proRecibeEpisodio(lganoregi, lGnumregi);                
                ConversionHaHD.DefInstance.ShowDialog();                
                cbRefrescar_Click(cbRefrescar, new EventArgs());
                this.Show();
            }

        }
    }
}