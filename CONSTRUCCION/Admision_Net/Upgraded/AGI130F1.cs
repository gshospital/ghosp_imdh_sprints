using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace ADMISION
{
	public partial class Anulacion_ingreso
		: RadForm
	{

		string vstGiden = String.Empty;
		string stSqEntidad = String.Empty;
		DataSet rrEntidad = null;
		string vstMax = String.Empty;
		bool fUrg1 = false, fUrg2 = false;
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		string VstSocieda = String.Empty;
		string VstInspecc = String.Empty;
		bool fCancel = false;
		bool bErroProcesoComprobar = false;
		string stDescripcionProcesoComprobar = String.Empty;
		bool ResumenCMDB = false;
		object Clase = null;
		//Procedimiento que recoge las incidencias de la Dll de ProcesoComprobar.
		//Hay que definir bErroAsociarProcesor y stDescripcionAsociarProceso en el general.
		public Anulacion_ingreso()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		public void DevolverAsociarProceso(string stError, string stDescripcionError)
		{
			if (stError.Trim() != "")
			{
				bErroProcesoComprobar = true;
				stDescripcionProcesoComprobar = stDescripcionError;
			}
		}

		public bool proFacturacion(string stCodpac)
		{
			bool result = false;
			string stSqlFacturado = String.Empty;
			DataSet rrFacturado = null;
			int iGano = 0;
			int iGnum = 0;
			result = true;
			string stSqlFacturacion = "SELECT * " + " FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodpac + "' AND" + " AEPISADM.FALTPLAN IS NULL";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlFacturacion, Conexion_Base_datos.RcAdmision);
			DataSet rrFacturacion = new DataSet();
			tempAdapter.Fill(rrFacturacion);
			DataSet RR = null;
			if (rrFacturacion.Tables[0].Rows.Count != 0)
			{
				iGano = Convert.ToInt32(rrFacturacion.Tables[0].Rows[0]["ganoadme"]);
				iGnum = Convert.ToInt32(rrFacturacion.Tables[0].Rows[0]["gnumadme"]);
				//busco si hay movimientos en dmovfact con la fecha de alta en planta
				stSqlFacturado = "select * from dmovfact where " + " ganoregi=" + iGano.ToString() + " and gnumregi=" + iGnum.ToString() + " and itiposer = 'H' and " + " gcodeven='ES' and ffactura is not null";

				if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
				{
					string tempRefParam = "DMOVFACT";
					stSqlFacturado = stSqlFacturado + " UNION ALL " + Serrores.proReplace(ref stSqlFacturado, tempRefParam, "DMOVFACH");
				}

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqlFacturado, Conexion_Base_datos.RcAdmision);
				rrFacturado = new DataSet();
				tempAdapter_2.Fill(rrFacturado);
				if (rrFacturado.Tables[0].Rows.Count == 0)
				{
					//no hay movimientos
					rrFacturado.Close();
					SqlCommand tempCommand = new SqlCommand("delete dmovfact where " + " ganoregi=" + iGano.ToString() + " and gnumregi=" + iGnum.ToString() + " and itiposer='H'", Conexion_Base_datos.RcAdmision);
					tempCommand.ExecuteNonQuery();

					//oscar 31/03/03
					//Borramos periodo facturado (APERFACT) si no esta facturado
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter("select * from sysobjects where name='APERFACT'", Conexion_Base_datos.RcAdmision);
					RR = new DataSet();
					tempAdapter_3.Fill(RR);
					if (RR.Tables[0].Rows.Count != 0)
					{
						SqlCommand tempCommand_2 = new SqlCommand("delete APERFACT where " + " ganoadme=" + iGano.ToString() + " and gnumadme=" + iGnum.ToString(), Conexion_Base_datos.RcAdmision);
						tempCommand_2.ExecuteNonQuery();
					}
					RR.Close();
					//----------

					result = true;
				}
				else
				{
					//hay movimientos facturados no se puede continuar con la anulacion
					rrFacturado.Close();
					result = false;
				}
			}
			rrFacturacion.Close();
			return result;
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			bool existetabla = false;
			bool Etiqueta = false;

			string tsSqReut = String.Empty, stAceptar = String.Empty, stSql = String.Empty;
			DataSet RrSql = null, RrAceptar = null, tRrReut = null, RrAceptarU = null;
			int iGanoregi = 0;
			int iGnumregi = 0;
			int iGservici = 0;
			string iFllegada = String.Empty;
			string fAprovechanum = String.Empty;
			bool ErrorTabla = false;
			DataSet rrTempo = null;

			int iNumQuir = 0;
			int iA�oQuir = 0;

			SqlCommand RrI = new SqlCommand();

            MtoEnferUrgAdm.MvtoEnferUrgAdm ClaseMvtoEnfemeria = null;
            ProcesoComprobar.ClComprobar ClaseFactQuirofano = null;

			bool bLlamarFactQuirofanos = false; // -> bLlamarFactQuirofanos = true cuando haya q. grabar en DMOVFACT
			// los apuntes q se borraron en el ingreso

			int iErrorQuir = 0;

			// numero y a�o de registro relacionado con urgencias desde admision
			int iNumeroRelacionadoUrg = 0; // para poder aceptar nulos
			int iA�oRelacionadoUrg = 0; // para poder aceptar nulos

			//**************************************
			try
			{
				Etiqueta = true;
				existetabla = false;
				//*******************************

				bLlamarFactQuirofanos = false;

				this.Cursor = Cursors.WaitCursor;


                Conexion_Base_datos.RcAdmision.BeginTransScope();
                //*******************************************************************************************
                stAceptar = "SELECT * " + " FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + vstGiden + "' AND" + " AEPISADM.FALTPLAN IS NULL";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter.Fill(RrAceptar);
				object ob = null;
				bool lError = false;
				if (RrAceptar.Tables[0].Rows.Count == 1)
				{
					iGanoregi = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GANOADME"]);
					iGnumregi = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GNUMADME"]);

					//****************************************************************************
					//SI EL PACIENTE TIENE CONSUMOS NO DEJAMOS ANULAR EL INGRESO
					//****************************************************************************

					stAceptar = "select * from FCONSPAC  where ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + " and itiposer = 'H' ";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
					RrAceptarU = new DataSet();
					tempAdapter_2.Fill(RrAceptarU);
					if (RrAceptarU.Tables[0].Rows.Count != 0)
					{
						RadMessageBox.Show("El paciente tiene apuntes de consumos, no se puede anular el ingreso.", Application.ProductName);
						this.Cursor = Cursors.Default;
                        Conexion_Base_datos.RcAdmision.RollbackTransScope();

                        return;
					}
					RrAceptarU.Close();
					//****************************************************************************


					//OSCAR
					//****************************************************************************
					//ANULAMOS EL EPISODIO DE AUTORIZACION DE LA ACTIVIDAD REALIZADA
					//****************************************************************************
					// ralopezn_TODO_X_4 26/04/2016
                    //ob = new Autorizaciones.clsAutorizaciones();
					//lError = Convert.ToBoolean(ob.fAnulaEpisodio(Conexion_Base_datos.RcAdmision, "R", "H", iGanoregi, iGnumregi, Conexion_Base_datos.VstCodUsua));
					//ob = null;

					if (lError)
					{
						this.Cursor = Cursors.Default;
                        Conexion_Base_datos.RcAdmision.RollbackTransScope();

                        return;
					}
					//***************************************************************************


					//End If
					//RrAceptar.Close
					//Comprobamos que se puede anular un episodio si pertenece a alg�n proceso
					bErroProcesoComprobar = false;
					stDescripcionProcesoComprobar = "";
					ClaseFactQuirofano = new ProcesoComprobar.ClComprobar();
					ClaseFactQuirofano.Iniciar(Conexion_Base_datos.RcAdmision, this, "H", iGanoregi, iGnumregi, null, "B");
					ClaseFactQuirofano = null;
					//************************************************************************************
					if (bErroProcesoComprobar)
					{
						this.Cursor = Cursors.Default;
                        Conexion_Base_datos.RcAdmision.RollbackTransScope();

                        RadMessageBox.Show(stDescripcionProcesoComprobar, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
						return;
					}
					//************************************************************************************

					//Urgente

					//**********inicio modificaci�n facturaci�n********************
					//comprueba que puede anular el ingreso
					if (!proFacturacion(vstGiden))
					{
						this.Cursor = Cursors.Default;
                        Conexion_Base_datos.RcAdmision.RollbackTransScope();

                        RadMessageBox.Show("Existen apuntes facturados, Imposible anular", Application.ProductName);
						return;
					}
					//**********fin modificaci�n facturaci�n***********************
					if (fUrg1)
					{
						if (fUrg2)
						{
                            stAceptar = "select ganourge, gnumurge, gmotalta, gnurelad, ganrelad from uepisurg " + " where gidenpac='" + vstGiden + "' order by fllegada";
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
							RrAceptarU = new DataSet();
							tempAdapter_3.Fill(RrAceptarU);
							if (RrAceptarU.Tables[0].Rows.Count != 0 && RrAceptar.Tables[0].Rows.Count != 0)
							{
								int iMoveLast = RrAceptarU.MoveLast(null);
                                RrAceptarU.Tables[0].Rows[iMoveLast]["gmotalta"] = cbbMotAlta.SelectedItem.Value;
								RrAceptarU.Tables[0].Rows[iMoveLast]["gnurelad"] = DBNull.Value;
								RrAceptarU.Tables[0].Rows[iMoveLast]["ganrelad"] = DBNull.Value;
								string tempQuery = RrAceptarU.Tables[0].TableName;
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                                tempAdapter_3.Update(RrAceptarU, RrAceptarU.Tables[0].TableName);
							}
							RrAceptarU.Close();
						}
						else
						{
							//MsgBox "Debe seleccionar un motivo de alta en urgencia", vbInformation + vbOKOnly, "Admisi�n"
							short tempRefParam = 1040;
							string[] tempRefParam2 = new string[]{Label12.Text};
							Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
							cbbMotAlta.Focus();
                            Conexion_Base_datos.RcAdmision.RollbackTransScope();

                            this.Cursor = Cursors.Default;
							return;
						}
					}


					//Antes de borrar,comprobamos que si est� activa la conexi�n con Catsalut y si hay programaciones o citas, actualizamos AEPISLES si es necesario.
					stAceptar = "SELECT isnull((select valfanu1 from sconsglo where gconsglo ='ienvicat'),'N') ienvicat,* FROM eprestac a  " + 
					            "inner join aepisles b on a.ganoregi = b.ganorela and a.gnumregi = b.gnumrela and a.itiposer = b.itiprela and a.gprestac = b.gprerela and a.fpeticio = b.fpetrela " + 
					            "WHERE a.itiposer='H' and a.ganoregi=" + iGanoregi.ToString() + " and a.gnumregi=" + iGnumregi.ToString() + " and a.iestsoli='C' and (a.itiprela='Q' or a.itiprela='C') and a.ganorela is not null ";

					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
					RrAceptarU = new DataSet();
					tempAdapter_5.Fill(RrAceptarU);
					if (RrAceptarU.Tables[0].Rows.Count > 0)
					{
						//Si est� activa la constante, actualizamos AEPISLES con la cita o con la programaci�n correspondiente....
						if (Convert.ToString(RrAceptarU.Tables[0].Rows[0]["ienvicat"]).Trim() == "S")
						{
							stAceptar = "update aepisles set itiprela=null,ganorela= " + Convert.ToString(RrAceptarU.Tables[0].Rows[0]["ganorela"]) + ", gnumrela= " + Convert.ToString(RrAceptarU.Tables[0].Rows[0]["gnumrela"]) + ",fpetrela=null, itipprue = (case when a.itiprela='Q' then 'P' else 'C' end) " + 
							            "FROM eprestac a inner join aepisles b on a.ganoregi = b.ganorela and a.gnumregi = b.gnumrela and a.itiposer = b.itiprela and a.gprestac = b.gprerela and a.fpeticio = b.fpetrela " + 
							            " WHERE itiposer='H' and a.ganoregi=" + iGanoregi.ToString() + " and a.gnumregi=" + iGnumregi.ToString() + "  and iestsoli='C' and (a.itiprela='Q' or a.itiprela='C') and a.ganorela is not null";
							SqlCommand tempCommand = new SqlCommand(stAceptar, Conexion_Base_datos.RcAdmision);
							tempCommand.ExecuteNonQuery();
						}
					}
					RrAceptarU.Close();

					//borra las prestaciones que tienen relacion con quirofanos

					SqlCommand tempCommand_2 = new SqlCommand("delete EPRESTAC where exists(select * from qintquir where " + 
					                           " eprestac.ganoregi= qintquir.ganoprin and  eprestac.gnumregi= qintquir.gnumprin and " + 
					                           " qintquir.itiposer = 'H') and eprestac.itiposer = 'H' and " + 
					                           " eprestac.ganoregi=" + iGanoregi.ToString() + " and eprestac.gnumregi=" + iGnumregi.ToString() + " ", Conexion_Base_datos.RcAdmision);
					tempCommand_2.ExecuteNonQuery();



					//borro las prestaciones que estan pendientes, comunicadas o anuladas
					SqlCommand tempCommand_3 = new SqlCommand("Delete EPRESTAC where itiposer='H' and ganoregi=" + 
					                           iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + " and (iestsoli='P' or iestsoli='C' or iestsoli='A')", Conexion_Base_datos.RcAdmision);
					tempCommand_3.ExecuteNonQuery();

					if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "OPENLAB", "VALFANU1") == "S")
					{
						SqlCommand tempCommand_4 = new SqlCommand("Delete DANALLAB where itiposer='H' and ganoregi = " + 
						                           iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                           " and exists (select '' from dopenlab where itiposer = 'H' and ganoregi = " + iGanoregi.ToString() + 
						                           " and gnumregi = " + iGnumregi.ToString() + " and dopenlab.gprestac = danallab.gprestac " + 
						                           " and dopenlab.fpeticio = danallab.fpeticio and (dopenlab.iestsoli='P' or dopenlab.iestsoli='C'))", Conexion_Base_datos.RcAdmision);
						tempCommand_4.ExecuteNonQuery();
						SqlCommand tempCommand_5 = new SqlCommand("Delete DANALSOL where itiposer='H' and ganoregi = " + 
						                           iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                           " and exists (select '' from dopenlab where itiposer = 'H' and ganoregi = " + iGanoregi.ToString() + 
						                           " and gnumregi = " + iGnumregi.ToString() + " and dopenlab.gprestac = danalsol.gprestac  " + 
						                           " and dopenlab.fpeticio = danalsol.fpeticio and (dopenlab.iestsoli='P' or dopenlab.iestsoli='C'))", Conexion_Base_datos.RcAdmision);
						tempCommand_5.ExecuteNonQuery();
						SqlCommand tempCommand_6 = new SqlCommand("Delete DOPENLAB where itiposer='H' and ganoregi = " + 
						                           iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                           " and (iestsoli='P' or iestsoli='C')", Conexion_Base_datos.RcAdmision);
						tempCommand_6.ExecuteNonQuery();
					}
					if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "IDELFIN", "VALFANU1") == "S")
					{
						SqlCommand tempCommand_7 = new SqlCommand("Delete DPETBASA where itiposer='H' and ganoregi = " + 
						                           iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                           " and (iestsoli='P' or iestsoli='C' or iestsoli= 'A')", Conexion_Base_datos.RcAdmision);
						tempCommand_7.ExecuteNonQuery();
					}
					//borro las interconsultas que estan pendientes
					SqlCommand tempCommand_8 = new SqlCommand("Delete EINTERCO where itiposer='H' and ganoregi=" + 
					                           iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + " and (iestsoli='P' or iestsoli='C' or iestsoli='A')", Conexion_Base_datos.RcAdmision);
					tempCommand_8.ExecuteNonQuery();


					//OSCAR C: AGOSTO 2009
					//Borramos los apuntes de enfermeria realizados de forma automatica al hacerse el ingreso.
					//Al anular el ingreso se deber� permitir hacerlo aunque existan registros en las tablas de enfermer�a
					//(F�rmacos, prestaciones, �rdenes m�dicas e interconsultas) con fecha de apunte igual
					//que la fecha del ingreso. Estos registros insertados de forma autom�tica se deber�n eliminar (esto ya se hace )
					//y el buz�n de ingreso deber� quedar desasociado del episodio del ingreso (itiprela, ganorela, gnumrela y gusurela con valor nulos)
					//NOTA: He probado a anular un ingreso con Buzon de Ingreso Aceptado de forma manual y no me ha dejado diciendo que existen apuntes
					//      de enfermeria, por lo que queda decidir si se modifica la fecha del apunte al aceptarse de forma automtica o se deja esto
					//      para que borre los apuntes correspondientes.
					if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "BINGAUTO", "VALFANU1") == "S")
					{

						//borro las ordenes medicas
						object tempRefParam3 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_9 = new SqlCommand("Delete EORDMEDI where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                           " AND fapuntes <= " + Serrores.FormatFechaHMS(tempRefParam3) + 
						                           " AND exists(select '' from DEPIINGR where " + 
						                           " EORDMEDI.itiposer = DEPIINGR.itiprela and EORDMEDI.ganoregi= DEPIINGR.ganorela and EORDMEDI.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_9.ExecuteNonQuery();

						object tempRefParam4 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_10 = new SqlCommand("Delete EOBSENFE where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                            " AND fapuntes <= " + Serrores.FormatFechaHMS(tempRefParam4) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " EOBSENFE.itiposer = DEPIINGR.itiprela and EOBSENFE.ganoregi= DEPIINGR.ganorela and EOBSENFE.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_10.ExecuteNonQuery();

						//borro las prestaciones:
						object tempRefParam5 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_11 = new SqlCommand("Delete EPRESTAC where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + " and (iestsoli='P' or iestsoli='C' or iestsoli='A')" + 
						                            " AND fpeticio <= " + Serrores.FormatFechaHMS(tempRefParam5) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " EPRESTAC.itiposer = DEPIINGR.itiprela and EPRESTAC.ganoregi= DEPIINGR.ganorela and EPRESTAC.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_11.ExecuteNonQuery();


						//borro las interconsultas: lo hace antes
						object tempRefParam6 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_12 = new SqlCommand("Delete EINTERCO where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + " and (iestsoli='P' or iestsoli='C' or iestsoli='A')" + 
						                            " AND fpeticio <= " + Serrores.FormatFechaHMS(tempRefParam6) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " EINTERCO.itiposer = DEPIINGR.itiprela and EINTERCO.ganoregi= DEPIINGR.ganorela and EINTERCO.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_12.ExecuteNonQuery();


						//borro los farmacos?
						object tempRefParam7 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_13 = new SqlCommand("Delete EFARMACO where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                            " AND fapuntes <= " + Serrores.FormatFechaHMS(tempRefParam7) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " EFARMACO.itiposer = DEPIINGR.itiprela and EFARMACO.ganoregi= DEPIINGR.ganorela and EFARMACO.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_13.ExecuteNonQuery();
						object tempRefParam8 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_14 = new SqlCommand("Delete EFARMMOD where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                            " AND fapuntes <= " + Serrores.FormatFechaHMS(tempRefParam8) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " EFARMMOD.itiposer = DEPIINGR.itiprela and EFARMMOD.ganoregi= DEPIINGR.ganorela and EFARMMOD.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_14.ExecuteNonQuery();


						//borro las pauta correctoras
						object tempRefParam9 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_15 = new SqlCommand("Delete EPAUCORR where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                            " AND fsistema <= " + Serrores.FormatFechaHMS(tempRefParam9) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " EPAUCORR.itiposer = DEPIINGR.itiprela and EPAUCORR.ganoregi= DEPIINGR.ganorela and EPAUCORR.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_15.ExecuteNonQuery();

						object tempRefParam10 = Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]).AddSeconds(30);
						SqlCommand tempCommand_16 = new SqlCommand("Delete DVALORDM where itiposer='H' and ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + iGnumregi.ToString() + 
						                            " AND fapuntes <= " + Serrores.FormatFechaHMS(tempRefParam10) + 
						                            " AND exists(select '' from DEPIINGR where " + 
						                            " DVALORDM.itiposer = DEPIINGR.itiprela and DVALORDM.ganoregi= DEPIINGR.ganorela and DVALORDM.gnumregi = DEPIINGR.gnumrela) ", Conexion_Base_datos.RcAdmision);
						tempCommand_16.ExecuteNonQuery();

					}

					//desasociamos el buzon de ingreso del episodio de hospitalizacion
					SqlCommand tempCommand_17 = new SqlCommand("UPDATE DEPIINGR set itiprela = NULL, ganorela = NULL, gnumrela = NULL, gusurela = NULL " + " where itiprela='H' and ganorela=" + iGanoregi.ToString() + " and gnumrela=" + iGnumregi.ToString(), Conexion_Base_datos.RcAdmision);
					tempCommand_17.ExecuteNonQuery();

					//------------------------------------------------------------------------

					// VEMOS SI HAY APUNTES DE ENFERMERIA PQ AHORA AUNQUE TENGA FINGPLAN SE LE PUEDE ANULAR SIEMPRE QUE SEA
					// AUTOMATICA -> NO SE GRABAN REGISTROS.

					stSql = "FechaMinMaxApuntes";

                    RrI = Conexion_Base_datos.RcAdmision.CreateQuery("ValidaIngreso", stSql);
                    RrI.CommandType = CommandType.StoredProcedure;

                    RrI.Parameters.Add("@FechMinMax", SqlDbType.Char);
                    RrI.Parameters.Add("@anoreg", SqlDbType.SmallInt);
                    RrI.Parameters.Add("@numreg", SqlDbType.Int);
                    RrI.Parameters.Add("@FechaApunte", SqlDbType.DateTime);
                    RrI.Parameters.Add("@TipoSer", SqlDbType.Char);
                    RrI.Parameters.Add("@Fecha", SqlDbType.DateTime);

                    RrI.Parameters[0].Direction = ParameterDirection.Input;
					RrI.Parameters[1].Direction = ParameterDirection.Input;
					RrI.Parameters[2].Direction = ParameterDirection.Input;
					RrI.Parameters[3].Direction = ParameterDirection.Input;
					RrI.Parameters[4].Direction = ParameterDirection.Input;
					RrI.Parameters[5].Direction = ParameterDirection.Output;
					RrI.Parameters[0].Value = "H"; // obtener la fecha m�nima
					RrI.Parameters[1].Value = iGanoregi;
					RrI.Parameters[2].Value = iGnumregi;
					RrI.Parameters[3].Value = RrAceptar.Tables[0].Rows[0]["fllegada"];
					RrI.Parameters[4].Value = "H";
					RrI.ExecuteNonQuery();

                    if (!Convert.IsDBNull(RrI.Parameters[5].Value))
					{ // encuentra datos en fecha de ingreso en planta
						if (Convert.ToDateTime(RrI.Parameters[5].Value) > Convert.ToDateTime(RrAceptar.Tables[0].Rows[0]["fllegada"]))
						{
                            Conexion_Base_datos.RcAdmision.RollbackTransScope();
                            stMensaje1 = "Anular el Ingreso";
							stMensaje2 = "Ingresado en Planta";
							short tempRefParam11 = 1310;
                            string[] tempRefParam12 = new string[] { stMensaje1, stMensaje2 };
							Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12));
							this.Cursor = Cursors.Default;
							return;
						}
					}

					// VALIDAR SI EXISTE UNA FILA EN EPRESTAC CON EL EPISODIO RELACIONADO DE QUIROFANOS
					// DISTINTO DE NULOS

					if (!HayRelacionQuirofanos(iGanoregi.ToString(), iGnumregi.ToString(), iNumQuir, iA�oQuir))
					{


					}

					//    If HayPrestacionQuirofanos(CStr(iGanoregi), CStr(iGnumregi), iNumQuir, iA�oQuir) Then
					//        bLlamarFactQuirofanos = True
					//
					//        ' ACTUALIZAMOS  QINTQUIR para que no tenga episodio relacionado de admision
					//        RcAdmision.Execute "Update QINTQUIR set itiposer=null,ganoprin=null,gnumprin=null " & _
					//'                           "where ganoregi=" & iA�oQuir & " and gnumregi =" & iNumQuir
					//    ElseIf HayRelacionQuirofanos(CStr(iGanoregi), CStr(iGnumregi), iNumQuir, iA�oQuir) Then
					//
					//        ' BORRAMOS LA PRESTACION
					//        'RcAdmision.Execute "Delete EPRESTAC where itiposer='H' and ganoregi=" & _
					//'        '                   iGanoregi & " and gnumregi=" & iGnumregi & " and itiprela='Q'"
					//
					//        bLlamarFactQuirofanos = True
					//
					//        ' ACTUALIZAMOS  QINTQUIR para que no tenga episodio relacionado de admision
					//        RcAdmision.Execute "Update QINTQUIR set itiposer=null,ganoprin=null,gnumprin=null " & _
					//'                           "where ganoregi=" & iA�oQuir & " and gnumregi =" & iNumQuir
					//
					//    End If



					iGservici = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GSERVICI"]);
					iFllegada = Convert.ToString(RrAceptar.Tables[0].Rows[0]["fllegada"]);
					// guardo los episodios relacionados de urgencias en caso que los tuviera
					iNumeroRelacionadoUrg = (Convert.IsDBNull(RrAceptar.Tables[0].Rows[0]["gnurelur"])) ? Convert.ToInt32(DBNull.Value) : Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["gnurelur"]);
					iA�oRelacionadoUrg = (Convert.IsDBNull(RrAceptar.Tables[0].Rows[0]["ganrelur"])) ? Convert.ToInt32(DBNull.Value) : Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["ganrelur"]);
					//''Se comente el if porque debe editar el registro de aresingr si lo tiene, vengo o no de urgencias.
					//If IsNull(iA�oRelacionadoUrg) Then
					// compruebo que si el ingreso iba contra una reserva y anulo la relaci�n
					stSql = "select gidenpac, fpreving, ganoadme, gnumadme from aresingr " + " where ganoadme =" + iGanoregi.ToString() + " and gnumadme = " + iGnumregi.ToString();
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
					RrSql = new DataSet();
					tempAdapter_6.Fill(RrSql);
					if (RrSql.Tables[0].Rows.Count == 1)
					{
						RrSql.Edit();
						RrSql.Tables[0].Rows[0]["ganoadme"] = DBNull.Value;
						RrSql.Tables[0].Rows[0]["gnumadme"] = DBNull.Value;
						string tempQuery_2 = RrSql.Tables[0].TableName;
						SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                        tempAdapter_6.Update(RrSql, RrSql.Tables[0].TableName);
					}
					RrSql.Close();
					//End If


					// BORRAMOS DE LA TABLA DE AREGIMEN
					SqlCommand tempCommand_18 = new SqlCommand(" delete AREGIMEN where gnumadme =" + iGnumregi.ToString() + 
					                            " and ganoadme =" + iGanoregi.ToString(), Conexion_Base_datos.RcAdmision);
					tempCommand_18.ExecuteNonQuery();


					//' 04/04/2014
					//' Borramos los documentos aportados a HC
					SqlCommand tempCommand_19 = new SqlCommand("DELETE FROM ADOCUMHC WHERE ganoregi = " + Conversion.Str(iGanoregi).Trim() + " AND gnumregi = " + Conversion.Str(iGnumregi).Trim(), Conexion_Base_datos.RcAdmision);
					tempCommand_19.ExecuteNonQuery();


					//NEPISNEO
					//comprobar si existe la tabla nepisneo si existe y tiene episodio relacionado poner
					//a null el ganoadne,gnumadne y gidenpac
					existetabla = true;
					Etiqueta = false;
					ErrorTabla = false;
                    SqlDataAdapter tempAdapter_8 = null;
                    try
                    {
                        stSql = "select * from nepisneo where gnumadne =" + iGnumregi.ToString() +
                                " and ganoadne =" + iGanoregi.ToString();
                        tempAdapter_8 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrTempo = new DataSet();
                        tempAdapter_8.Fill(rrTempo);
                    }
                    catch (Exception ex)
                    {
                        ErrorTabla = true;
                    }

                    if (!ErrorTabla)
                    {
                        if (rrTempo.Tables[0].Rows.Count != 0)
                        {
                            rrTempo.Edit();
                            rrTempo.Tables[0].Rows[0]["gnumadne"] = DBNull.Value;
                            rrTempo.Tables[0].Rows[0]["ganoadne"] = DBNull.Value;
                            //rrTempo("gidenpac") = Null
                            string tempQuery_3 = rrTempo.Tables[0].TableName;
                            SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_8);
                            tempAdapter_8.Update(rrTempo, rrTempo.Tables[0].TableName);
                        }
                    }
                    rrTempo.Close();
                    


					//AEPISLEQ
					//comprobar si existe algun registro asociado en AEPISLEQ se pone a nulo para desasociar
					existetabla = true;
					Etiqueta = false;
					ErrorTabla = false;

                    try
                    {
                        stSql = "UPDATE AEPISLEQ SET gnumadme = Null, ganoadme = Null where gnumadme =" + iGnumregi.ToString() +
                                " and ganoadme =" + iGanoregi.ToString();
                        SqlCommand tempCommand_20 = new SqlCommand(stSql, Conexion_Base_datos.RcAdmision);
                        tempCommand_20.ExecuteNonQuery();
                    }catch (Exception ex)
                    {
                        ErrorTabla = true;
                    }

					ClaseMvtoEnfemeria = new MtoEnferUrgAdm.MvtoEnferUrgAdm();
					ClaseMvtoEnfemeria.ReplicarTablasEnferHDAdm("0", "0", iGanoregi.ToString(), iGnumregi.ToString(), 1, Conexion_Base_datos.RcAdmision);
					ClaseMvtoEnfemeria = null;
					//--------------

					Etiqueta = true;
					existetabla = false;
					RrAceptar.Delete(tempAdapter);
					RrAceptar.Close();
				}
				else
				{
					//MsgBox "No se puede realizar esta operaci�n"
					stMensaje1 = "Anular el Ingreso";
					stMensaje2 = "Ingresado en Planta";
					short tempRefParam13 = 1310;
                    string[] tempRefParam14 = new string[] { stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, Conexion_Base_datos.RcAdmision, tempRefParam14));
					RrAceptar.Close();
                    Conexion_Base_datos.RcAdmision.RollbackTransScope();

                    this.Cursor = Cursors.Default;
					this.Close();
					return;
				}
				//********camas
				stAceptar = "SELECT gplantas, ghabitac, gcamasbo, IESTCAMA,fcamesta,GIDENPAC,itipsexo FROM DCAMASBO WHERE GIDENPAC='" + vstGiden + "'";
				SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_10.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count == 1)
				{
					RrAceptar.Edit();
					RrAceptar.Tables[0].Rows[0]["IESTCAMA"] = "L";
					RrAceptar.Tables[0].Rows[0]["gidenpac"] = DBNull.Value;
					RrAceptar.Tables[0].Rows[0]["fcamesta"] = DateTime.Now;
					RrAceptar.Tables[0].Rows[0]["itipsexo"] = DBNull.Value;
					string tempQuery_4 = RrAceptar.Tables[0].TableName;
					SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_10);
                    tempAdapter_10.Update(RrAceptar, RrAceptar.Tables[0].TableName);
					RrAceptar.Close();
				}
				//borro de amovimie,amovifin,amovrega,AAUSENCI
				stAceptar = "SELECT * " + " FROM AMOVIMIE " + " WHERE " + " AMOVIMIE.GNUMREGI = " + iGnumregi.ToString() + " AND" + " AMOVIMIE.GANOREGI=" + iGanoregi.ToString() + "";

				SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_12.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count > 0)
				{
					RrAceptar.Delete(tempAdapter_12);
				}
				stAceptar = "SELECT * " + " FROM AMOVIfin" + " WHERE " + " AMOVIfin.GNUMREGI = " + iGnumregi.ToString() + " AND" + " AMOVIfin.GANOREGI=" + iGanoregi.ToString() + "";

				SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_13.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count > 0)
				{
                    RrAceptar.Delete(tempAdapter_13);
				}
				RrAceptar.Close();
				stAceptar = "SELECT * " + " FROM AAUSENCI" + " WHERE " + " AAUSENCI.GNUMADME = " + iGnumregi.ToString() + " AND" + " AAUSENCI.GANOADME=" + iGanoregi.ToString() + "";
				SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_14.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count > 0)
				{
                    RrAceptar.Delete(tempAdapter_14);
				}
				RrAceptar.Close();
				stAceptar = "SELECT * " + " FROM AMOVREGA " + " WHERE " + " AMOVREGA.GNUMREGI = " + iGnumregi.ToString() + " AND" + " AMOVREGA.GANOREGI=" + iGanoregi.ToString() + "";

				SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_15.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count > 0)
				{
                    RrAceptar.Delete(tempAdapter_15);
				}
				RrAceptar.Close();

				//Para los casos que tenga gesti�n de producto (GPRODUCT a S)
				stAceptar = "SELECT * " + " FROM AMOVPROD " + " WHERE " + " AMOVPROD.GNUMREGI = " + iGnumregi.ToString() + " AND" + " AMOVPROD.GANOREGI=" + iGanoregi.ToString() + "";

				SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_16.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count > 0)
				{
                    RrAceptar.Delete(tempAdapter_16);
				}
				RrAceptar.Close();


				//******EPISODIOS ANULADOS
				tsSqReut = "select valfanu1 from sconsglo where gconsglo='iaproanu'";
				SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(tsSqReut, Conexion_Base_datos.RcAdmision);
				tRrReut = new DataSet();
				tempAdapter_17.Fill(tRrReut);
				if (Convert.ToString(tRrReut.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					fAprovechanum = "S";
				}
				else
				{
					fAprovechanum = "N";
				}
				stAceptar = "select * from DEPIANUL WHERE 2=1";
				SqlDataAdapter tempAdapter_18 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_18.Fill(RrAceptar);

				RrAceptar.AddNew();
				RrAceptar.Tables[0].Rows[0]["ITIPOSER"] = "H";
				RrAceptar.Tables[0].Rows[0]["GANOREGI"] = iGanoregi;
				RrAceptar.Tables[0].Rows[0]["GNUMREGI"] = iGnumregi;
				RrAceptar.Tables[0].Rows[0]["FANULACI"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
				RrAceptar.Tables[0].Rows[0]["IALTAING"] = "I";
				RrAceptar.Tables[0].Rows[0]["GUSUARIO"] = Conexion_Base_datos.VstCodUsua; //FALTA EL CODIGO DE USUARIO
				RrAceptar.Tables[0].Rows[0]["GSERVICI"] = iGservici;
				RrAceptar.Tables[0].Rows[0]["IREUTILI"] = fAprovechanum;
				RrAceptar.Tables[0].Rows[0]["GIDENPAC"] = vstGiden;
				RrAceptar.Tables[0].Rows[0]["GSOCIEDA"] = VstSocieda;
				System.DateTime TempDate = DateTime.FromOADate(0);
				RrAceptar.Tables[0].Rows[0]["FLLEGADA"] = (DateTime.TryParse(iFllegada, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : iFllegada;
				string tempQuery_5 = RrAceptar.Tables[0].TableName;
				SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_18);
                tempAdapter_18.Update(RrAceptar, RrAceptar.Tables[0].TableName);

				RrAceptar.Close();
				tRrReut.Close();

				// borramos los apuntes relacionados de enfermeria y los campos relacionados en la tabla de urgencias
				if (!Convert.IsDBNull(iNumeroRelacionadoUrg))
				{ //si tenia apuntes de urgencias
					ClaseMvtoEnfemeria = new MtoEnferUrgAdm.MvtoEnferUrgAdm();
					ClaseMvtoEnfemeria.ReplicarTablasEnferUrgAdm(iA�oRelacionadoUrg.ToString(), iNumeroRelacionadoUrg.ToString(), iGanoregi.ToString(), iGnumregi.ToString(), 1, Conexion_Base_datos.RcAdmision, false);
					ClaseMvtoEnfemeria = null;
					SqlCommand tempCommand_21 = new SqlCommand("update uepisurg set ganrelad = null,gnurelad = null where gnurelad=" + 
					                            iNumeroRelacionadoUrg.ToString() + " and ganrelad =" + iA�oRelacionadoUrg.ToString(), Conexion_Base_datos.RcAdmision);
					tempCommand_21.ExecuteNonQuery();
				}


                Conexion_Base_datos.RcAdmision.CommitTransScope();


                this.Cursor = Cursors.Arrow;
				string tempRefParam15 = vstGiden + "S";
				menu_ingreso.DefInstance.proQuitar_MatAnul(tempRefParam15);
				fCancel = false;
				this.Close();
			}
			catch (Exception excep)
			{
				if (!existetabla && !Etiqueta)
				{
					throw excep;
				}
				//************************************
				if (Etiqueta)
				{

                    Conexion_Base_datos.RcAdmision.RollbackTransScope();

                    RadMessageBox.Show("Fallo en anulaci�n Ingreso: " + Information.Err().Number.ToString() + excep.Message, Application.ProductName);
					this.Cursor = Cursors.Arrow;
					return;
				}
				if (existetabla || Etiqueta)
				{

					ErrorTabla = true;
					//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
				}
			}
            finally
            {
                Conexion_Base_datos.RcAdmision.DisposeTransScope();
            }
		}

		private void cbbMotAlta_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			if (cbbMotAlta.SelectedIndex != -1)
			{
				cbAceptar.Enabled = true;
				fUrg2 = true;
			}
		}


		private void cbbMotAlta_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void cbbMotAlta_Leave(Object eventSender, EventArgs eventArgs)
		{
			cbAceptar.Enabled = cbbMotAlta.Text != "";
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			fCancel = true;
			this.Close();
		}

		public void proRecibo_Pac(string SGidenpac, string sNombre)
		{

			lbPaciente.Text = sNombre;
			vstGiden = SGidenpac;
			proHistoria(SGidenpac);
			proSociedad(SGidenpac);
			//*********Comprobar Urgente***************
			string stUrgente = String.Empty;
			DataSet RrUrgente = null;
			string stMotAlta = String.Empty;
			DataSet RrMotAlta = null;
			string stAceptar = String.Empty;
			DataSet RrAceptar = null;
			int indice = 0;
			try
			{

				stUrgente = "SELECT ITIPINGR FROM AEPISADM " + " WHERE GIDENPAC='" + SGidenpac + " ' ORDER BY" + " FLLEGADA ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stUrgente, Conexion_Base_datos.RcAdmision);
				RrUrgente = new DataSet();
				tempAdapter.Fill(RrUrgente);
				int iMoveLast = RrUrgente.MoveLast(null);
				//cbbMotAlta.Enabled = False
				cbAceptar.Enabled = true;
				if (Convert.ToString(RrUrgente.Tables[0].Rows[iMoveLast]["itipingr"]) != "U")
				{
					cbbMotAlta.Enabled = false;
					cbAceptar.Enabled = true;
				}
				else
				{
					fUrg1 = true;
					stAceptar = "select gmotalta from uepisurg " + " where gidenpac='" + vstGiden + "' order by fllegada";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stAceptar, Conexion_Base_datos.RcAdmision);
					RrAceptar = new DataSet();
					tempAdapter_2.Fill(RrAceptar);
					if (RrAceptar.Tables[0].Rows.Count == 0)
					{ //Not RrAceptar.BOF And Not RrAceptar.EOF Then
						fUrg1 = false;
						cbbMotAlta.Enabled = false;
						cbAceptar.Enabled = true;
					}
					else
					{
						RrAceptar.MoveLast(null);
						stMotAlta = "select gmotalta,dmotalta from dmotaltava " + " order by dmotalta";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stMotAlta, Conexion_Base_datos.RcAdmision);
						RrMotAlta = new DataSet();
						tempAdapter_3.Fill(RrMotAlta);
						foreach (DataRow iteration_row in RrMotAlta.Tables[0].Rows)
						{
                            cbbMotAlta.Items.Add(new RadListDataItem(iteration_row["dmotalta"].ToString(), Convert.ToInt32(iteration_row["Gmotalta"])));

							if (Convert.ToInt32(iteration_row["Gmotalta"]) == Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["gmotalta"]))
							{
								cbbMotAlta.Text = Convert.ToString(iteration_row["dmotalta"]);
                                indice = cbbMotAlta.SelectedIndex;
							}
						}
						cbbMotAlta.SelectedIndex = indice;
					}
				}
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show("Fallo en anulaci�n " + Information.Err().Number.ToString() + excep.Message, Application.ProductName);
			}
		}
		public void proHistoria(string SGidenpac)
		{

			DataSet RrHistor = null;
			string StSqHistor = String.Empty;

			try
			{
				StSqHistor = "SELECT GHISTORIA FROM HSERARCH, HDOSSIER " + " WHERE GIDENPAC = '" + SGidenpac + "' AND " + " GSERPROPIET = GSERARCH AND " + " ICENTRAL='S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqHistor, Conexion_Base_datos.RcAdmision);
				RrHistor = new DataSet();
				tempAdapter.Fill(RrHistor);

				if (RrHistor.Tables[0].Rows.Count == 1)
				{
					lbHistoria.Text = Conversion.Str(RrHistor.Tables[0].Rows[0]["GHISTORIA"]);
				}
				else
				{
					lbHistoria.Text = "";
				}
				RrHistor.Close();
			}
			catch (System.Exception excep)
			{
                Conexion_Base_datos.RcAdmision.RollbackTransScope();
                RadMessageBox.Show("Fallo en anulaci�n " + Information.Err().Number.ToString() + excep.Message, Application.ProductName);
			}
		}

		public string fnMaximo(string stCodPac1)
		{

			string tstMaxMov = String.Empty;

			string tstSqCambio = "SELECT MAX(fmovimie) AS MAXIMO " + " FROM AEPISADM, AMOVIFIN " + " WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AEPISADM.GIDENPAC = '" + stCodPac1 + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSqCambio, Conexion_Base_datos.RcAdmision);
			DataSet RrMaximo = new DataSet();
			tempAdapter.Fill(RrMaximo);
			if (!Convert.IsDBNull(RrMaximo.Tables[0].Rows[0]["maximo"]))
			{
				tstMaxMov = Convert.ToString(RrMaximo.Tables[0].Rows[0]["maximo"]);
			}
			RrMaximo.Close();
			return tstMaxMov;

		}
		public void proSociedad(string stCodpac)
		{

			try
			{
				vstMax = fnMaximo(stCodpac);
				string SQSoci = String.Empty;
				DataSet RrSoci = null;
				if (vstMax != "")
				{


					//*********************************
					object tempRefParam = vstMax;
					stSqEntidad = "SELECT AMOVIFIN.GSOCIEDA, AMOVIFIN.ginspecc " + " FROM AEPISADM, AMOVIFIN " + " WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AMOVIfin.fMOVIMIE =" + Serrores.FormatFechaHMS(tempRefParam) + " AND " + " AEPISADM.GIDENPAC = '" + stCodpac + "'";
					vstMax = Convert.ToString(tempRefParam);
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
					rrEntidad = new DataSet();
					tempAdapter.Fill(rrEntidad);
					//**** recojo el c�digo de la sociedad ***********
					VstSocieda = Convert.ToString(rrEntidad.Tables[0].Rows[0]["gsocieda"]);
					VstInspecc = Convert.ToString(rrEntidad.Tables[0].Rows[0]["ginspecc"]) + "";
					//********Busco la sociedad

					SQSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
					RrSoci = new DataSet();
					tempAdapter_2.Fill(RrSoci);

					if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == Convert.ToDouble(rrEntidad.Tables[0].Rows[0]["GSOCIEDA"]))
					{ //SS
						lbEntFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
						proNumAseg(Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gsocieda"]));
						RrSoci.Close();
						return;
					}
					//*************************************
					SQSoci = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
					RrSoci = new DataSet();
					tempAdapter_3.Fill(RrSoci);
					if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == Convert.ToDouble(rrEntidad.Tables[0].Rows[0]["GSOCIEDA"]))
					{ // Privado
						lbEntFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
						proNumAseg(Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gsocieda"]));
						RrSoci.Close();
						return;
					}
					//***************************Sociedad
					SQSoci = "SELECT DSOCIEDA,IREGALOJ FROM DSOCIEDA WHERE GSOCIEDA = " + Convert.ToString(rrEntidad.Tables[0].Rows[0]["GSOCIEDA"]);
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
					RrSoci = new DataSet();
					tempAdapter_4.Fill(RrSoci);
					lbEntFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["DSOCIEDA"]).Trim();
					proNumAseg(Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gsocieda"]));
					RrSoci.Close();
					//***********************************

					rrEntidad.Close();
				}
				else
				{
					//Unload Me
				}
			}
			catch (System.Exception excep)
			{
                Conexion_Base_datos.RcAdmision.RollbackTransScope();
                RadMessageBox.Show("Fallo en anulaci�n " + Information.Err().Number.ToString() + excep.Message, Application.ProductName);
			}
		}



		public void proNumAseg(int iSociedad)
		{
			string stNumAseg = String.Empty;
			DataSet RrNumAseg = null;
			string stGlobal = " select nnumeri1 from sconsglo " + " where gconsglo ='PRIVADO'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stGlobal, Conexion_Base_datos.RcAdmision);
			DataSet Rrglobal = new DataSet();
			tempAdapter.Fill(Rrglobal);

			if (Convert.ToBoolean(Rrglobal.Tables[0].Rows[0]["NNUMERI1"]))
			{
				stNumAseg = " SELECT NAFILIAC FROM DPACIENT " + " WHERE GIDENPAC= '" + vstGiden + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stNumAseg, Conexion_Base_datos.RcAdmision);
				RrNumAseg = new DataSet();
				tempAdapter_2.Fill(RrNumAseg);
				if (RrNumAseg.Tables[0].Rows.Count == 1)
				{
					if (!Convert.IsDBNull(RrNumAseg.Tables[0].Rows[0]["nafiliac"]))
					{
						lbNAsegurado.Text = Convert.ToString(RrNumAseg.Tables[0].Rows[0]["NAFILIAC"]);
					}
					else
					{
						lbNAsegurado.Text = "";
					}
				}
			}
			else
			{
				stNumAseg = " SELECT NAFILIAC FROM DENTIPAC " + " WHERE GSOCIEDA=" + iSociedad.ToString() + " AND " + " GIDENPAC= '" + vstGiden + "'";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stNumAseg, Conexion_Base_datos.RcAdmision);
				RrNumAseg = new DataSet();
				tempAdapter_3.Fill(RrNumAseg);
				if (RrNumAseg.Tables[0].Rows.Count == 1)
				{
					if (!Convert.IsDBNull(RrNumAseg.Tables[0].Rows[0]["nafiliac"]))
					{
						lbNAsegurado.Text = Convert.ToString(RrNumAseg.Tables[0].Rows[0]["NAFILIAC"]);
					}
					else
					{
						lbNAsegurado.Text = "";
					}
				}
			}
			Rrglobal.Close();
			RrNumAseg.Close();
		}

		private void Anulacion_ingreso_Load(Object eventSender, EventArgs eventArgs)
		{

			cbAceptar.Enabled = false;
			fCancel = true;

			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM SCONSGLO WHERE GCONSGLO ='BDBCONEX'", Conexion_Base_datos.RcAdmision);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);
			ResumenCMDB = false;
			if (RrDatos.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					ResumenCMDB = true;
				}
			}
			RrDatos.Close();

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label14.Text = LitPersona;

			string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(Conexion_Base_datos.RcAdmision);
			if (HospitalOCentro.Trim() == "N")
			{
				cbbMotAlta.Visible = false;
				Label12.Visible = false;
			}

		}

		private void Anulacion_ingreso_Closed(Object eventSender, EventArgs eventArgs)
		{
			if (fCancel)
			{
				string tempRefParam = vstGiden + "C";
				menu_ingreso.DefInstance.proQuitar_MatAnul(tempRefParam);
			}
		}

		private bool HayRelacionQuirofanos(string A�o, string Numero, int NumQuir, int A�oQuir)
		{
			//Si existe algun episodio de quirofanos relacionado con admision se quita la relacion
			//y generan movimientos de falcturacion independientemente
			bool result = false;
            FacturacionQuirofanos.clsFacturacion ClaseFactQuirofano = null;
			int iErrorQuir = 0;
			string sqlRelacion = "select ganoprin, gnumprin, itiposer, ganoregi, gnumregi from qintquir where ";
			sqlRelacion = sqlRelacion + " itiposer='H' and ganoprin = " + A�o;
			sqlRelacion = sqlRelacion + " and gnumprin = " + Numero;
			sqlRelacion = sqlRelacion + " order by ganoregi,gnumregi";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlRelacion, Conexion_Base_datos.RcAdmision);
			DataSet RrRelacion = new DataSet();
			tempAdapter.Fill(RrRelacion);
			if (RrRelacion.Tables[0].Rows.Count > 0)
			{
				foreach (DataRow iteration_row in RrRelacion.Tables[0].Rows)
				{
					NumQuir = Convert.ToInt32(iteration_row["gnumregi"]);
					A�oQuir = Convert.ToInt32(iteration_row["ganoregi"]);
					RrRelacion.Edit();
					iteration_row["ganoprin"] = DBNull.Value;
					iteration_row["gnumprin"] = DBNull.Value;
					iteration_row["itiposer"] = DBNull.Value;
					string tempQuery = RrRelacion.Tables[0].TableName;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrRelacion, RrRelacion.Tables[0].TableName);
					iErrorQuir = 0;
					// LLAMAMOS A FACTURACION QUIROFANOS
					ClaseFactQuirofano = new FacturacionQuirofanos.clsFacturacion();
					ClaseFactQuirofano.GenerarMovimientosFacturacion(A�oQuir, NumQuir, Conexion_Base_datos.RcAdmision, null, ref iErrorQuir);
					if (iErrorQuir != 0)
					{
                        Conexion_Base_datos.RcAdmision.RollbackTransScope();
                        this.Cursor = Cursors.Arrow;
						RadMessageBox.Show("Fallo en anulaci�n Ingreso: " + iErrorQuir.ToString(), Application.ProductName);
						return false;
					}
					ClaseFactQuirofano = null;
					result = true;
				}
			}
			else
			{
				result = true;
			}
			RrRelacion.Close();
			return result;
		}
		//private bool HayPrestacionQuirofanos(string A�o, string Numero, int NumQuir, int A�oQuir)
		//{
				//bool result = false;
				//string sqlprestacion = "select count(ganoregi) TOTAL,ganorela,gnumrela from eprestac where ";
				//sqlprestacion = sqlprestacion + " itiposer='H' and ganoregi = " + A�o;
				//sqlprestacion = sqlprestacion + " and gnumregi = " + Numero;
				//sqlprestacion = sqlprestacion + " and itiprela ='Q' group by ganorela,gnumrela ORDER BY ganorela,gnumrela ";
				//
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlprestacion, Conexion_Base_datos.RcAdmision);
				//DataSet Rrprestacion = new DataSet();
				//tempAdapter.Fill(Rrprestacion);
				//if (Rrprestacion.Tables[0].Rows.Count > 0)
				//{
					//result = (Convert.ToDouble(Rrprestacion.Tables[0].Rows[0]["TOTAL"]) > 0);
					//if (result)
					//{
						//NumQuir = Convert.ToInt32(Rrprestacion.Tables[0].Rows[0]["gnumrela"]);
						//A�oQuir = Convert.ToInt32(Rrprestacion.Tables[0].Rows[0]["ganorela"]);
					//}
				//}
				//else
				//{
					//result = false;
				//}
				//Rrprestacion.Close();
				//return result;
		//}
	}
}