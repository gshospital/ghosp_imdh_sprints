using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.Drawing;
using CrystalWrapper;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ADMISION
{
    internal static class Conexion_Base_datos
    {

        public static bool BtCancelar = false;
        //public static UpgradeStubs.RDO_rdoEnvironment ReAdmision = null;
        public static SqlConnection RcAdmision = null;
        public static string BDtempo = String.Empty;
        public static string gContrase�a = String.Empty; //contrase�a del usuario
        public static string gUsuario = String.Empty; //nombre del usuario
        public static string gEnfermeria = String.Empty; //unidad de enfermeria
        public static string PathString = String.Empty; //path de la aplicaci�n
        public static string VstCrystal = String.Empty; //conexi�n de los objetos CRYSTALREPORT
        public static string stNombreDsnACCESS = String.Empty; //conexi�n

        public static string VstCodUsua = String.Empty; //c�digo del usuario
        public static mbAcceso.strControlOculto[] astrControles = null;
        public static mbAcceso.strEventoNulo[] astrEventos = null;
        public static Crystal LISTADO_CRYSTAL = null; // CRISTAL PARA LISTADOS
        public static Crystal LISTADO_LIMPIA = null; // LIMPIA EL CONTROL CRYSTAL
        public static PrintDialog Cuadro_Diag_imprePrint = null;
        public static bool DescargaMenuAlta = false; //toma valor true cuando se descarga la ventana de altas
        public static string stFORMFILI = String.Empty;
        public static string DNICEDULA = String.Empty;
        public static string DniCedulaMay = String.Empty;
        public static int Aepisodio = 0;
        public static int Nepisodio = 0;
        public static string ProvinEstado = String.Empty;

        public static bool NoHacerActivate = false;
        //****************CONSTANTES GLOBALES PARA LA CABECERA DE CRYSTAL
        public static string StBaseTemporal = String.Empty;
        public struct CabCrystal
        {
            public string VstGrupoHo;
            public string VstNombreHo;
            public string VstDireccHo;
            public bool VfCabecera;
            public string VstTelefoHo;
            public static CabCrystal CreateInstance()
            {
                CabCrystal result = new CabCrystal();
                result.VstGrupoHo = String.Empty;
                result.VstNombreHo = String.Empty;
                result.VstDireccHo = String.Empty;
                result.VstTelefoHo = String.Empty;
                return result;
            }
        }
        public static Conexion_Base_datos.CabCrystal VstrCrystal = Conexion_Base_datos.CabCrystal.CreateInstance();
        //***IMPRESORA
        public static string VstImpresora = String.Empty; //impresora original
        public static string VstDriverPrint = String.Empty;
        public static string VstPortPrint = String.Empty;
        public static int ViOrientacion = 0;
        public static string VstImpresora2 = String.Empty; //cambio de impresora
        public static string VstDriverPrint2 = String.Empty;
        public static string VstPortPrint2 = String.Empty;
        public static int ViOrientacion2 = 0;
        public static string vImpre_defecto = String.Empty;
        public static int ViNumCopias = 0;
        public static bool VfCancelPrint = false; //cancela la impresi�n
                                                  //********************
        public static bool vstActivado_Ingreso = false;
        //CONSTANTES Y FUNCIONES API PARA LA OBTENCION DE DATOS DEL REGISTRO
        public const int KEY_ALL_ACCESS = 0x3F;
        public const int REG_OPTION_NON_VOLATILE = 0;
        public static readonly int HKEY_CURRENT_USER = unchecked((int)0x80000001);
        public static readonly int HKEY_LOCAL_MACHINE = unchecked((int)0x80000002);
        public const int REG_SZ = 1;
        public const int ERROR_SUCCESS = 0;
        public static Color Rojo = Color.Red;
        public static Color Negro = Color.Black;
        //public const int Negro = 0x0;
        public const int Azul = 0xFF0000;
        public static readonly int Blanco = unchecked((int)0x80000014);
        public static readonly int Gris = unchecked((int)0x8000000F);
        public static PrinterHelper prImpre = null;
        public static string VstUsuario_NT = String.Empty;
        public const int KEY_QUERY_VALUE = 0x1;

        //Estas dos variables desapareceran con la dll de reservas
        public static int Ia�opro = 0;
        public static int lnumpro = 0;
        //Variables para el justificante del acompa�ante.
        public static string VstNomacompa = String.Empty;
        public static string VstParentescoAcompa = String.Empty;
        public static string VstHoraAcompa = String.Empty;
        public static string VstFechaAcompa = String.Empty;

        //oscar
        public static bool bGestionHD = false;
        public static bool bGestionProducto = false; //30/03/04
                                                     //---------

        //************** O.Frias 19/07/2007 **************
        //Cierre de m�dulos

        //Parametrizamos las columnas de la pantalla de ingresados para que sea mas facil a�adir nuevas ya que
        //en caso de a�adir nuevas columnas, solo habr�a que actualizar los indices.
        public const int col_Plaza = 1;
        public const int col_AutoInfor = 2;
        public const int Col_Paciente = 3;
        public const int col_Fingreso = 4;
        public const int col_Nosequecolumnaes = 5;
        public const int col_Fprevalta = 6;
        public const int col_Sociedad = 7;
        public const int col_Servingr = 8;
        public const int col_ServActual = 9;
        public const int col_MedResp = 10;
        public const int col_FAviso = 11;
        public const int col_Telcama = 12;
        public const int col_IdenPac = 13;

        internal static bool FacturaFarmacosLaSociedad(int iSociedad, string stInspFact)
        {
            // compruebo el tipo de facturaci�n de los f�rmacos de la sociedad
            string tStLeer = String.Empty;

            bool btest = true;
            if (stInspFact != "")
            {
                // la sociedad es Seguridad Social
                tStLeer = "Select ifafarma from dinspecc where ginspecc = '" + stInspFact + "'";
            }
            else
            {
                tStLeer = "Select ifafarma from dsocieda where gsocieda = " + iSociedad.ToString();
            }
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tStLeer, RcAdmision);
            DataSet tRrLeer = new DataSet();
            tempAdapter.Fill(tRrLeer);
            if (tRrLeer.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(tRrLeer.Tables[0].Rows[0]["ifafarma"]))
                {
                    //Si es "S" devolver� true y si es "N", false					
                    btest = (Convert.ToString(tRrLeer.Tables[0].Rows[0]["ifafarma"]).Trim() == "S");
                }
            }
            return btest;

        }

        internal static void GestionHD()
        {
            bGestionHD = false;
            string sql = "select VALFANU1 from sconsglo where gconsglo='VERCAMAS'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
            DataSet RR = new DataSet();
            tempAdapter.Fill(RR);
            if (RR.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
                {
                    bGestionHD = true;
                }
            }
            RR.Close();
        }

        //oscar 30/03/04
        internal static void GestionProducto()
        {
            bGestionProducto = false;
            string sql = "select VALFANU1 from sconsglo where gconsglo='GPRODUCT'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
            DataSet RR = new DataSet();
            tempAdapter.Fill(RR);
            if (RR.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
                {
                    bGestionProducto = true;
                }
            }
            RR.Close();
        }
        //------------
        //Public Function FnCambia_formato_fecha(ParaFecha As String, ParaFormato As String) As String
        //
        //Dim Dfecha As String
        //Dim Icorta As Integer
        //Dim Contadias As Integer
        //Dim Contameses As Integer
        //Dim Contaa�os As Integer
        //Dim Contahoras As Integer
        //Dim Contaminutos As Integer
        //Dim Contasegundos As Integer
        //Dim Separadias As String
        //Dim Separameses As String
        //Dim Separahoras As String
        //Dim Separaminutos As String
        //Dim Posiciondias As Integer
        //Dim Posicionmeses As Integer
        //Dim Posiciona�os As Integer
        //Dim Posicionhoras As Integer
        //Dim Posicionminutos As Integer
        //Dim Posicionsegundos As Integer
        //Separadias = ""
        //Separameses = ""
        //Separahoras = ""
        //Separaminutos = ""
        //Contadias = 0
        //Contameses = 0
        //Contaa�os = 0
        //Contahoras = 0
        //Contaminutos = 0
        //Contasegundos = 0
        //Posiciondias = 0
        //Posicionmeses = 0
        //Posiciona�os = 0
        //Posicionhoras = 0
        //Posicionminutos = 0
        //Posicionsegundos = 0
        //Dfecha = ParaFecha
        //'buscar si contiene dias
        //If InStr(1, "D", UCase(ParaFormato)) <> 0 Then
        //    'si contiene dias cuantos
        //    Posiciondias = InStr(1, "DD", UCase(ParaFormato))
        //    If Posiciondias <> 0 Then
        //        Contadias = 2
        //        If Len(ParaFormato) > 2 Then
        //            Separadias = Mid(ParaFormato, Posiciondias + 2, 1)
        //        End If
        //    Else
        //        Contadias = 1
        //        If Len(ParaFormato) > 1 Then
        //            Separadias = Mid(ParaFormato, Posiciondias + 1, 1)
        //        End If
        //    End If
        //End If
        //'buscar si contiene meses
        //If InStr(1, "M", UCase(ParaFormato)) <> 0 Then
        //    'si contiene meses cuantos
        //    Posicionmeses = InStr(1, "MM", UCase(ParaFormato))
        //    If Posicionmeses <> 0 Then
        //        Contameses = 2
        //        If Len(ParaFormato) > 2 Then
        //            If Mid(ParaFormato, Posicionmeses + 2, 1) <> Null Then
        //                Separameses = Mid(ParaFormato, Posicionmeses + 2, 1)
        //            End If
        //        End If
        //    Else
        //        Contameses = 1
        //        If Len(ParaFormato) > 1 Then
        //            If Mid(ParaFormato, Posicionmeses + 1, 1) <> Null Then
        //                Separameses = Mid(ParaFormato, Posicionmeses + 1, 1)
        //            End If
        //        End If
        //    End If
        //End If
        //'buscar si contiene a�os
        //If InStr(1, "Y", UCase(ParaFormato)) <> 0 Then
        //    'si contiene meses cuantos
        //    Posiciona�os = InStr(1, "YYYY", UCase(ParaFormato))
        //    If Posiciona�os <> 0 Then
        //        Contaa�os = 4
        //    Else
        //        Posiciona�os = InStr(1, "YYY", UCase(ParaFormato))
        //        If Posiciona�os <> 0 Then
        //            Contaa�os = 3
        //        Else
        //            Posiciona�os = InStr(1, "YY", UCase(ParaFormato))
        //            If Posiciona�os <> 0 Then
        //                Contaa�os = 2
        //            Else
        //                Contaa�os = 1
        //            End If
        //        End If
        //    End If
        //End If
        //'buscar si contiene horas
        //If InStr(1, "H", UCase(ParaFormato)) <> 0 Then
        //    'si contiene meses cuantos
        //    Posicionhoras = InStr(1, "HH", UCase(ParaFormato))
        //    If Posicionhoras <> 0 Then
        //        Contahoras = 2
        //        If Len(ParaFormato) > 2 Then
        //            If Mid(ParaFormato, Posicionhoras + 2, 1) <> Null Then
        //                Separahoras = Mid(ParaFormato, Posicionhoras + 2, 1)
        //            End If
        //        End If
        //    Else
        //        Contahoras = 1
        //        If Len(ParaFormato) > 1 Then
        //            If Mid(ParaFormato, Posicionhoras + 1, 1) <> Null Then
        //                Separahoras = Mid(ParaFormato, Posicionhoras + 1, 1)
        //            End If
        //        End If
        //    End If
        //End If
        //'buscar si contiene minutos
        //If InStr(1, "N", UCase(ParaFormato)) <> 0 Then
        //    'si contiene meses cuantos
        //    Posicionminutos = InStr(1, "NN", UCase(ParaFormato))
        //    If Posicionminutos <> 0 Then
        //        Contaminutos = 2
        //        If Len(ParaFormato) > 2 Then
        //            If Mid(ParaFormato, Posicionminutos + 2, 1) <> Null Then
        //                Separaminutos = Mid(ParaFormato, Posicionminutos + 2, 1)
        //            End If
        //        End If
        //    Else
        //        Contaminutos = 1
        //        If Len(ParaFormato) > 1 Then
        //            If Mid(ParaFormato, Posicionminutos + 1, 1) <> Null Then
        //                Separaminutos = Mid(ParaFormato, Posicionminutos + 1, 1)
        //            End If
        //        End If
        //    End If
        //End If
        //'buscar si contiene segundos
        //If InStr(1, "S", UCase(ParaFormato)) <> 0 Then
        //    'si contiene meses cuantos
        //    Posicionsegundos = InStr(1, "SS", UCase(ParaFormato))
        //    If Posicionsegundos <> 0 Then
        //        Contasegundos = 2
        //        If Len(ParaFormato) > 2 Then
        //            If Mid(ParaFormato, Posicionsegundos + 2, 1) <> Null Then
        //                Separaminutos = Mid(ParaFormato, Posicionsegundos + 2, 1)
        //            End If
        //        End If
        //    Else
        //        Contasegundos = 1
        //        If Len(ParaFormato) > 1 Then
        //            If Mid(ParaFormato, Posicionsegundos + 1, 1) <> Null Then
        //                Separaminutos = Mid(ParaFormato, Posicionsegundos + 1, 1)
        //            End If
        //        End If
        //    End If
        //End If
        //If Contadias <> 0 Then
        //    DIAS = Mid(ParaFecha, 1, Contadias)
        //End If
        //If Contameses <> O Then
        //    MESES = Mid(ParaFecha, 3, Contameses)
        //End If
        //
        //
        //
        //
        //
        //End Function

        internal static string Obtener_Uni_Enfe(string usua)
        {
            string result = String.Empty;
            string sql = String.Empty;
            DataSet RrSql = null;
            try
            {
                sql = "select dunienfe.dunidenf , dunienfe.gunidenf ";
                sql = sql + "From susuario , dunienfe Where ";
                sql = sql + "susuario.gusuario = '" + usua + "' and ";
                sql = sql + "dunienfe.gunidenf = susuario.gunidenf";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
                RrSql = new DataSet();
                tempAdapter.Fill(RrSql);
                if (RrSql.Tables[0].Rows.Count != 0)
                {
                    result = Convert.ToString(RrSql.Tables[0].Rows[0]["gunidenf"]);
                }
                RrSql.Close();
                return result;
            }
            catch (System.Exception excep)
            {
                RadMessageBox.Show(Information.Err().Number.ToString() + excep.Source + "%" + excep.Message, Application.ProductName);
                return result;
            }
        }

        internal static string FnPoblacion(string stPoblaci, SqlConnection rcConexion)
        {
            string result = String.Empty;
            string stSqlPoblaci = "select * from dpoblaci where " + "gpoblaci='" + stPoblaci + "'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlPoblaci, rcConexion);
            DataSet rrPoblaci = new DataSet();
            tempAdapter.Fill(rrPoblaci);
            if (rrPoblaci.Tables[0].Rows.Count > 0)
            {
                result = Convert.ToString(rrPoblaci.Tables[0].Rows[0]["dpoblaci"]);
            }
            else
            {
                result = "";
            }
            rrPoblaci.Close();
            return result;
        }

        internal static void proCabCrystal()
        {
            //**************************************************************************
            //Busqueda de la cabecera de la hoja
            //**************************************************************************            
            string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcAdmision);
            DataSet tRrCrystal = new DataSet();
            tempAdapter.Fill(tRrCrystal);
            VstrCrystal.VstGrupoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
            tstCab = "SELECT valfanu1, valfanu2 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RcAdmision);
            tRrCrystal = new DataSet();
            tempAdapter_2.Fill(tRrCrystal);
            VstrCrystal.VstNombreHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
            VstrCrystal.VstTelefoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU2"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
            tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RcAdmision);
            tRrCrystal = new DataSet();
            tempAdapter_3.Fill(tRrCrystal);
            VstrCrystal.VstDireccHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();

            tRrCrystal.Close();
            VstrCrystal.VfCabecera = true;
        }

        //Funci�n que obtiene los datos del servidor y del motor de base de datos
        //del registro y los devuelve como STRING para establecer la conexion
        //devolvera error si el par�metro lresult es <>0
        internal static string fnQueryRegistro(string stSubKey)
        {
            //'Dim lresult As Long 'todo va bien si es cero
            //'Dim lValueType As Long  'tipo de dato
            //'Dim strBuf As String  'cadena que contiene el valor requerido
            //'Dim lDataBufSize As Long 'tama�o de la cadena en el registro
            //'Dim hKey As Long   'ubicaci�n de la clave en el registro
            //'Dim tstKeyName  As String 'clave donde estan los datos en el registro
            //'
            //'tstKeyName = "SOFTWARE\INDRA\GHOSP"
            //'lValueType = REG_SZ
            //'
            //'lresult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tstKeyName, 0, KEY_QUERY_VALUE, hKey)
            //'lresult = RegQueryValueEx(hKey, stSubKey, 0&, lValueType, ByVal 0&, lDataBufSize)
            //'    If lresult = ERROR_SUCCESS Then
            //'        If lValueType = REG_SZ Then
            //'            strBuf = String(lDataBufSize, " ")
            //'            lresult = RegQueryValueEx(hKey, stSubKey, 0&, 0&, ByVal strBuf, lDataBufSize)
            //'            If lresult = ERROR_SUCCESS Then
            //'                 fnQueryRegistro = Mid$(strBuf, 1, lDataBufSize - 1)
            //'            End If
            //'        End If
            //'    End If
            //'    If lresult <> ERROR_SUCCESS Then
            //'        MsgBox "Pongase en contacto con inform�tica", vbCritical + vbOKOnly, "Error en conexi�n"
            //'        End
            //'    End If
            //'RegCloseKey (hKey)
            //'CURIA E MIGRACION: ESTABLECER NUEVA DE OBTENER LOS DATOS DEL SERVIDOR
            return String.Empty;
        }

        internal static bool fnAsitcama(string fecha)
        {
            //compruebo que estan todos los registros grabados en asitcama
            //si no es asi los grabo
            //Dim tstGrabar As String
            //Dim stTemp As String
            //Dim tRrtemp As rdoResultset
            //tstGrabar = App.Path & "\CENSO00.EXE a"
            //stTemp = "SELECT MAX(FSITUACI) AS MAXFECHA FROM ASITCAMA"
            //Set tRrtemp = RcAdmision.OpenResultset(stTemp, 1, 1)
            //If Not IsNull(tRrtemp("maxfecha")) Then
            //    If CDate(tRrtemp("MAXFECHA")) < fecha Then
            //        Shell tstGrabar
            //    End If
            //    fnAsitcama = True
            //Else
            //    MsgBox "Debe ejecutar: " & Mid(tstGrabar, 1, Len(tstGrabar) - 1)
            //    'Shell Mid(tstGrabar, 1, Len(tstGrabar) - 1)
            //    fnAsitcama = False
            //End If
            //tRrtemp.Close

            //01/06/04
            //OSCAR: AHORA EL CALCULO DEL CENSO NO SE REALIZA MEDIANTE UN .EXE,
            //SINO QUE SE ENCUENTRA LOCALIzADO EN UN PROC. ALMACENADO DE LA BD
            //(HABRIA QUE TENER CUIDADO YA QUE AL ESTAR EN UN
            //.BAS PODRIA INVOCARSE DESDE VARIOS MODULOS QUE INCLUYAN ESTE BAS)
            //AUNQUE UNA VEZ BUSCADO EN EL SOURCESAFE ESTE BAS, PARACE QUE SOLO ESTA EN ADMISION.
            SqlCommand CalculoCenso = new SqlCommand();
            try
            {
                CalculoCenso.Connection = RcAdmision;
                CalculoCenso.CommandText = "CALCULO_CENSO";
                CalculoCenso.CommandType = CommandType.StoredProcedure;
                CalculoCenso.ExecuteNonQuery();
                while (RcAdmision.getStillExecuting())
                {
                };
                CalculoCenso = null;
                return true;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "fnAsitCama", ex);
                return false;
                //-------
            }
        }

        //cristal Report
        internal static void proHojaClinEsta(object Dialogo_impresora, Form OVENTANA, Crystal cryInformes, RadCheckBox chbHojaClinEsta, int lGanoadme, int lGnumadme, string stTGidenpac)
        {
            //debe llamar a la dll
            object Clase = null;
            int puntero = 0;
            do {
                try
                {
                    //camaya todo_7_3
                    //Clase = new DocumentoDLL.mcDocumento();
                    //Clase.proConexion(OVENTANA, RcAdmision, cryInformes, PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword);
                    //Clase.Imprimir_HojaClinicoEstadistica(lGanoadme, lGnumadme, "H", stTGidenpac, Dialogo_impresora, StBaseTemporal, stNombreDsnACCESS);

                    Clase = null;
                    chbHojaClinEsta.CheckState = CheckState.Unchecked;
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "proHojaClinEsta:Documentos_Alta", ex);
                    puntero = 1;

                }
            }
            while (puntero >= 1);
        }

        internal static string fnSociedad(string PacId, int PacGano, int PacGnu)
        {
            //''''''''''
            string tSqlSoci = "SELECT Fmovimie,gsocieda " + " FROM  AMOVIFIN " + " WHERE " + "  AMOVIFIN.GNUMREGI=" + PacGnu.ToString() + " AND " + "  AMOVIFIN.GANOREGI= " + PacGano.ToString() + " order by fmovimie";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tSqlSoci, RcAdmision);
            DataSet tRrSoci = new DataSet();
            tempAdapter.Fill(tRrSoci);
            tRrSoci.MoveLast(null);


            int tiSoci = Convert.ToInt32(tRrSoci.Tables[0].Rows[0]["gsocieda"]);
            //busca la sociedad del paciente
            tSqlSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tSqlSoci, RcAdmision);
            tRrSoci = new DataSet();
            tempAdapter_2.Fill(tRrSoci);

            if (Convert.ToInt32(tRrSoci.Tables[0].Rows[0]["NNUMERI1"]) == tiSoci)
            { //SS				
                return Convert.ToString(tRrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim() + "/" + "SS";
            }
            //
            tSqlSoci = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tSqlSoci, RcAdmision);
            tRrSoci = new DataSet();
            tempAdapter_3.Fill(tRrSoci);

            if (Convert.ToInt32(tRrSoci.Tables[0].Rows[0]["NNUMERI1"]) == tiSoci)
            { // Privado				
                return Convert.ToString(tRrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim() + "/" + "PR";
            }
            //
            tSqlSoci = "SELECT DSOCIEDA FROM DSOCIEDA WHERE GSOCIEDA = " + tiSoci.ToString();
            SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tSqlSoci, RcAdmision);
            tRrSoci = new DataSet();
            tempAdapter_4.Fill(tRrSoci);

            return Convert.ToString(tRrSoci.Tables[0].Rows[0]["DSOCIEDA"]).Trim() + "/" + "SOC";

        }

        internal static int fnNewsociedad(string Paragidenpac, int ParaAno, int ParaNum)
        {
            int result = 0;
            string sqltemp = "select * from aepisadm where gidenpac='" + Paragidenpac + "' and ganoadme=" + ParaAno.ToString() + " and gnumadme=" + ParaNum.ToString();
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                result = Convert.ToInt32(rrtemp.Tables[0].Rows[0]["gsocipri"]);
            }
            else
            {
                result = 0;
            }
            rrtemp.Close();
            return result;
        }
        internal static string ObtenerDiagnostico(string StCodigo, ref string stFecha)
        {
            //Obtengo la descripci�n de un diagn�stico sabiendo su c�digo y la
            //fecha en la que se asign� a un episodio
            //************************ CRIS *****************************************
            string result = String.Empty;
            string stSql = "Select DNOMBDIA,dnombdiai1,dnombdiai2 from DDIAGNOS ";
            stSql = stSql + "where GCODIDIA=" + "'" + StCodigo.Trim() + "'";
            object tempRefParam = stFecha;
            stSql = stSql + " and FINVALDI<=" + "" + Serrores.FormatFecha(tempRefParam) + "";
            stFecha = Convert.ToString(tempRefParam);
            object tempRefParam2 = stFecha;
            stSql = stSql + " and (FFIVALDI>=" + "" + Serrores.FormatFecha(tempRefParam2) + "";
            stFecha = Convert.ToString(tempRefParam2);
            stSql = stSql + " or FFIVALDI IS NULL)";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, RcAdmision);
            DataSet rrDiag = new DataSet();
            tempAdapter.Fill(rrDiag);
            if (rrDiag.Tables[0].Rows.Count > 0)
            {
                if (Serrores.CampoIdioma == "IDIOMA0")
                {
                    result = (Convert.IsDBNull(rrDiag.Tables[0].Rows[0]["DNOMBDIA"])) ? "" : Convert.ToString(rrDiag.Tables[0].Rows[0]["DNOMBDIA"]).Trim();
                }
                else if (Serrores.CampoIdioma == "IDIOMA1")
                {
                    result = (Convert.IsDBNull(rrDiag.Tables[0].Rows[0]["DNOMBDIAI1"])) ? "" : Convert.ToString(rrDiag.Tables[0].Rows[0]["DNOMBDIAI1"]).Trim();
                }
                else if (Serrores.CampoIdioma == "IDIOMA2")
                {
                    result = (Convert.IsDBNull(rrDiag.Tables[0].Rows[0]["DNOMBDIAI2"])) ? "" : Convert.ToString(rrDiag.Tables[0].Rows[0]["DNOMBDIAI2"]).Trim();
                }
            }
            else
            {
                result = "";
            }
            rrDiag.Close();

            return result;
        }

        internal static string fnHistoria(string PacId)
        {
            //*******historia, si no tiene no puede seleccionar Hoja de Datos
            string result = String.Empty;
            string sql1 = "SELECT distinct(GHISTORIA) FROM HSERARCH, HDOSSIER " + " WHERE GIDENPAC = '" + PacId + "' AND " + " GSERPROPIET = GSERARCH AND icontenido='H' and " + " ICENTRAL='S'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql1, RcAdmision);
            DataSet Rr1 = new DataSet();
            tempAdapter.Fill(Rr1);
            if (Rr1.Tables[0].Rows.Count == 1)
            {
                //if rr1.RowCount				
                result = Convert.ToString(Rr1.Tables[0].Rows[0]["ghistoria"]);
            }
            else
            {
                result = "";
            }
            Rr1.Close();
            return result;
        }
        internal static void proDialogo_impre(Crystal Pcrystal, int Num_Copi = 0)
        {

            if (false || Num_Copi == 0)
            {
                Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
            }
            else
            {
                Cuadro_Diag_imprePrint.PrinterSettings.Copies = short.Parse(Num_Copi.ToString());
            }
            Pcrystal.PrinterStopPage = 0;
            //camaya todo_x_3           
            //Cuadro_Diag_imprePrint.setPrinterDefault(true);
            Cuadro_Diag_imprePrint.ShowDialog();

            Pcrystal.CopiesToPrinter = (short)Cuadro_Diag_imprePrint.PrinterSettings.Copies;

            return;

        }

        internal static int proEdad(string fnaci)
        {
            int ano = DateTime.Now.Year - Convert.ToDateTime(fnaci).Year;
            if ((DateTime.Now.Month < Convert.ToDateTime(fnaci).Month) && ano != 0)
            {
                ano--;
            }
            else
            {
                if ((DateTime.Now.Month == Convert.ToDateTime(fnaci).Month) && ano != 0)
                {
                    if (DateTime.Now.Day < Convert.ToDateTime(fnaci).Day)
                    {
                        ano--;
                    }
                }
            }
            return ano;
        }

        //internal static void vacia_formulas(AxCrystal.AxCrystalReport listado, int Numero)
        internal static void vacia_formulas(Crystal listado, int Numero)
        {            
            int tiForm = 0;
            string[] MatrizAux = null;
            MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { listado.Formulas.Count });
            if (listado.Formulas.Count > 0)
            {
                foreach (var key in listado.Formulas.Keys)
                {                    
                    MatrizAux[tiForm] = key;
                    ++tiForm;
                }

                foreach (var form in MatrizAux)
                {
                    listado.Formulas[form] = "";
                }
            }            
            listado.Reset();
        }

        internal static void Recoge_Impresora()
        {
            //Dim OImpre As Printer
            //Dim bHay As Boolean
            //bHay = False
            //For Each OImpre In Printers
            //    bHay = True
            //    Set Printer = OImpre
            //    Exit For
            //Next
            //If bHay = True Then
            try
            {
                if (PrinterHelper.Printer.DeviceName != "")
                {
                    VstImpresora = PrinterHelper.Printer.DeviceName;
                    VstPortPrint = PrinterHelper.Printer.Port;
                    VstDriverPrint = PrinterHelper.Printer.DeviceName;
                    ViOrientacion = PrinterHelper.Printer.Orientation;
                    vImpre_defecto = PrinterHelper.Printer.DeviceName;
                }
            }
            catch
            {
            }
        }

        internal static object fnNEtiqueta()
        {

            object result = null;
            string tSql1 = String.Empty;
            DataSet tRr1 = null;

            try
            {

                //numero de etiquetas a imprimir
                tSql1 = "select nnumeri1 from sconsglo where gconsglo='nroetiad'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(tSql1, RcAdmision);
                tRr1 = new DataSet();
                tempAdapter.Fill(tRr1);
                result = tRr1.Tables[0].Rows[0]["nnumeri1"];
                tRr1.Close();

                return result;
            }
            catch (SqlException ex)
            {
                //***
                Serrores.GestionErrorBaseDatos(ex, RcAdmision);
                return result;
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
                return result;
            }
        }

        internal static string procambiadia_mes(string ffecha)
        {
            string result = String.Empty;
            string stdia = String.Empty;
            string stmes = String.Empty;
            switch (Serrores.VstTipoBD)
            {
                case "SQLSERVER":

                    stdia = ffecha.Substring(0, Math.Min(2, ffecha.Length));
                    stmes = ffecha.Substring(3, Math.Min(2, ffecha.Length - 3));
                    result = stdia + "/" + stmes + "/" + ffecha.Substring(6);
                    //     Case "ORACLE", "INFORMIX" 
                    //        stdia = Mid(ffecha, 1, 2) 
                    //        stmes = Mid(ffecha, 4, 2) 
                    //        procambiadia_mes = stmes & "/" & stdia & "/" & Mid(ffecha, 7) 
                    break;
            }

            return result;
        }

        internal static void proJustificante(Crystal cryInformes, Crystal.DestinationConstants destino, dynamic Fomulario, int vstNumadme, int vstAnoadme, string stGidenpac, object Dialogo_impresora_optional)
        {
            PrintDialog Dialogo_impresora = (Dialogo_impresora_optional == Type.Missing) ? null : (PrintDialog) Dialogo_impresora_optional;

            DataSet rrQuirofano = null;
            string sqlDiagnostico = String.Empty;
            DataSet RrDiagnostico = null;
            string sqlCrystal = String.Empty;
            DLLTextosSql.TextosSql oTextoSql = null;
            string stsqltemp = String.Empty;
            DataSet rrtemp = null;
            string HospitalOCentro = String.Empty;
            string StrHospitalaria = String.Empty;
            System.DateTime fechallegada = DateTime.FromOADate(0);
            string Fecha_Llegada = String.Empty;

            Serrores.Obtener_Multilenguaje(RcAdmision, stGidenpac);
            Serrores.ObtenerTextosRPT(RcAdmision, "ADMISION", "AGI123R1");
            string MaysculaOMinuscula = "m"; //para ver si la fecha va en mayusculas o minusculas
            Serrores.ObtenerMesesSemanas(RcAdmision);

            string stAnestesia = " ";
            string stFenchaInt = " ";
            //    stIndicadorInt = "No"
            string stIndicadorInt = Serrores.VarRPT[5];
            int puntero = 1;

            do
            {
                try
                {
                    //*********cabeceras
                    if (!VstrCrystal.VfCabecera)
                    {
                        proCabCrystal();
                    }
                    cryInformes.ReportFileName = "" + PathString + "\\rpt\\Agi123r1_CR11.rpt";
                    //*******************
                    cryInformes.Formulas["{@FOR1}"] = "\"" + VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@FOR2}"] = "\"" + VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@FOR3}"] = "\"" + VstrCrystal.VstDireccHo + "\"";
                    cryInformes.Formulas["{@FOR8}"] = "\"" + VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@FOR9}"] = "\"" + VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@FOR10}"] = "\"" + VstrCrystal.VstDireccHo + "\"";
                    //******
                    stsqltemp = "select valfanu1,valfanu1i1,valfanu1i2 from sconsglo where gconsglo= 'idenpers' ";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count != 0)
                    {
                        //oscar
                        //DNICEDULA = Trim(Rs("valfanu1"))
                        switch (Serrores.CampoIdioma)
                        {
                            case "IDIOMA0":
                                DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim() + "";
                                break;
                            case "IDIOMA1":
                                DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]).Trim() + "";
                                break;
                            case "IDIOMA2":
                                DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i2"]).Trim() + "";
                                break;
                        }
                        //--------
                    }
                    rrtemp.Close();
                    DNICEDULA = DNICEDULA + ": ";
                    cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + DNICEDULA + "\"";
                    //***** "
                    cryInformes.Formulas["{@fOR16}"] = "\"" + Serrores.VarRPT[7] + "\"";
                    cryInformes.Formulas["{@fOR17}"] = "\"" + Serrores.CapturarFechaHoy(MaysculaOMinuscula) + "\"";
                    //'    If stFORMFILI = "V" Then
                    //'         cryInformes.Formulas(14) = "FOR7= 'CONSTANCIA DE HOSPITALIZACI�N'"
                    //'         cryInformes.WindowTitle = "Justificante de Constancia"
                    //'    Else
                    //        cryInformes.Formulas(14) = "FOR7= 'JUSTIFICANTE DE INGRESO'"
                    cryInformes.Formulas["{@FOR7}"] = "\"" + Serrores.VarRPT[1] + "\"";
                    //        cryInformes.WindowTitle = "Justificante de Asistencia"
                    cryInformes.WindowTitle = Serrores.VarRPT[2];
                    //'    End If
                    cryInformes.Formulas["{@LOGOIDC}"] = "\"" + Serrores.ExisteLogo(RcAdmision) + "\"";


                    sqlDiagnostico = " select gdiaging,odiaging,gdiagalt,odiagalt,fllegada,faltplan,finterve,gidenpac from aepisadm where " +
                                 " AEPISADM.gnumadme =" + vstNumadme.ToString() + " AND  AEPISADM.ganoadme=  " + vstAnoadme.ToString();

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlDiagnostico, RcAdmision);
                    RrDiagnostico = new DataSet();
                    tempAdapter_2.Fill(RrDiagnostico);
                    if (RrDiagnostico.Tables[0].Rows.Count > 0)
                    {

                        if (!Convert.IsDBNull(RrDiagnostico.Tables[0].Rows[0]["faltplan"]))
                        { //de alta en planta -> diagnosticos al alta                     

                            if (!Convert.IsDBNull(RrDiagnostico.Tables[0].Rows[0]["gdiagalt"]))
                            {
                                string tempRefParam = Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["faltplan"]);
                                cryInformes.Formulas["{@diagnostico}"] = "'" + ObtenerDiagnostico(Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["gdiagalt"]), ref tempRefParam) + "'";
                            }
                            else
                            {
                                cryInformes.Formulas["{@diagnostico}"] = "'" + ((Convert.IsDBNull(RrDiagnostico.Tables[0].Rows[0]["odiagalt"])) ? "" : Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["odiagalt"])) + "'";
                            }
                        }
                        else
                        {
                            // miramos el diagn�stico al ingreso 

                            if (!Convert.IsDBNull(RrDiagnostico.Tables[0].Rows[0]["gdiaging"]))
                            {
                                string tempRefParam2 = Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["fllegada"]);
                                cryInformes.Formulas["{@diagnostico}"] = "'" + ObtenerDiagnostico(Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["gdiaging"]), ref tempRefParam2) + "'";
                            }
                            else
                            {
                                cryInformes.Formulas["{@diagnostico}"] = "'" + ((Convert.IsDBNull(RrDiagnostico.Tables[0].Rows[0]["odiaging"])) ? "" : Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["odiaging"])) + "'";
                            }
                        }
                    }

                    //*************************************************************************************************************
                    //Buscamos las intervenciones asociadas al episodio de hospitalizaci�n

                    sqlDiagnostico = " SELECT QINTQUIR.FINTERVE, DANESTES, danestesi1, danestesi2 FROM (QINTQUIR LEFT JOIN DANESTES ON QINTQUIR.GANESTES = DANESTES.GANESTES) WHERE " +
                                     " GIDENPAC= '" + Convert.ToString(RrDiagnostico.Tables[0].Rows[0]["gidenpac"]) + "' and itiposer = 'H' and gnumprin =" + vstNumadme.ToString() + " AND  ganoprin  =  " + vstAnoadme.ToString() + " order by FINTERVE desc ";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlDiagnostico, RcAdmision);
                    rrQuirofano = new DataSet();
                    tempAdapter_3.Fill(rrQuirofano);

                    if (rrQuirofano.Tables[0].Rows.Count > 0)
                    {
                        rrQuirofano.MoveFirst();

                        if (!Convert.IsDBNull(rrQuirofano.Tables[0].Rows[0]["finterve"]))
                        {
                            stIndicadorInt = Serrores.VarRPT[6];
                            stFenchaInt = Convert.ToString(rrQuirofano.Tables[0].Rows[0]["finterve"]);
                            //relleno formulas de quirofano con datos�
                            if (Serrores.CampoIdioma == "IDIOMA0")
                            {

                                if (!Convert.IsDBNull(rrQuirofano.Tables[0].Rows[0]["danestes"]))
                                {
                                    stAnestesia = Convert.ToString(rrQuirofano.Tables[0].Rows[0]["danestes"]);
                                }
                            }
                            else if (Serrores.CampoIdioma == "IDIOMA1")
                            {
                                if (!Convert.IsDBNull(rrQuirofano.Tables[0].Rows[0]["danestesi1"]))
                                {
                                    stAnestesia = Convert.ToString(rrQuirofano.Tables[0].Rows[0]["danestesi1"]);
                                }
                            }
                            else if (Serrores.CampoIdioma == "IDIOMA2")
                            {

                                if (!Convert.IsDBNull(rrQuirofano.Tables[0].Rows[0]["danestesi2"]))
                                {
                                    stAnestesia = Convert.ToString(rrQuirofano.Tables[0].Rows[0]["danestesi2"]);
                                }
                            }
                        }
                    }

                    rrQuirofano.Close();
                    RrDiagnostico.Close();

                    //*************************************************************************************************************
                    //hacemos el cruce directamente con los datos de aepisadm

                    //''    ReDim MatrizSql(3)
                    //''    MatrizSql(1) = stGidenpac
                    //''    MatrizSql(2) = FormatFechaHMS(RrDiagnostico("fllegada"))
                    //''    MatrizSql(3) = FormatFechaHMS(RrDiagnostico("faltplan"))
                    //''    Set oTextoSql = CreateObject("DLLTEXTOSSQL.TEXTOSSQL")
                    //''    sqlQuirofano = oTextoSql.devuelvetexto(VstTipoBD, "ADMISION", "Conexion_Ba" _
                    //'''       , "proJustificante", 1, MatrizSql, RcAdmision)
                    //''    Set oTextoSql = Nothing
                    //''
                    //''    Set rrQuirofano = RcAdmision.OpenResultset(sqlQuirofano, 1, 1)
                    //''   '''''''''''''''''' si existen datos
                    //''    If rrQuirofano.RowCount > 0 Then
                    //''         'relleno formulas de quirofano con datos�
                    //''         If CampoIdioma = "IDIOMA0" Then
                    //''            If Not IsNull(rrQuirofano("danestes")) Then stAnestesia = rrQuirofano("danestes")
                    //''         ElseIf CampoIdioma = "IDIOMA1" Then
                    //''            If Not IsNull(rrQuirofano("danestesi1")) Then stAnestesia = rrQuirofano("danestesi1")
                    //''         ElseIf CampoIdioma = "IDIOMA2" Then
                    //''            If Not IsNull(rrQuirofano("danestesi2")) Then stAnestesia = rrQuirofano("danestesi2")
                    //''         End If
                    //''
                    //'''         stIndicadorInt = "S�"
                    //''         stIndicadorInt = VarRPT(6)
                    //''         stFenchaInt = rrQuirofano("finterve")
                    //''    Else
                    //''        ReDim MatrizSql(3)
                    //''        MatrizSql(1) = stGidenpac
                    //''        MatrizSql(2) = FormatFechaHMS(RrDiagnostico("fllegada"))
                    //''        MatrizSql(3) = FormatFechaHMS(RrDiagnostico("faltplan"))
                    //''        Set oTextoSql = CreateObject("DLLTEXTOSSQL.TEXTOSSQL")
                    //''        sqlQuirofano = oTextoSql.devuelvetexto(VstTipoBD, "ADMISION", "Conexion_Ba" _
                    //'''           , "proJustificante", 2, MatrizSql, RcAdmision)
                    //''        Set oTextoSql = Nothing
                    //''
                    //''        Set rrQuirofano = RcAdmision.OpenResultset(sqlQuirofano, 1, 1)
                    //''        If rrQuirofano.RowCount > 0 Then
                    //''            'relleno formulas de quirofano con datos�
                    //''         If CampoIdioma = "IDIOMA0" Then
                    //''            If Not IsNull(rrQuirofano("danestes")) Then stAnestesia = rrQuirofano("danestes")
                    //''         ElseIf CampoIdioma = "IDIOMA1" Then
                    //''            If Not IsNull(rrQuirofano("danestesi1")) Then stAnestesia = rrQuirofano("danestesi1")
                    //''         ElseIf CampoIdioma = "IDIOMA2" Then
                    //''            If Not IsNull(rrQuirofano("danestesi2")) Then stAnestesia = rrQuirofano("danestesi2")
                    //''         End If
                    //''
                    //'''            stIndicadorInt = "S�"
                    //''            stIndicadorInt = VarRPT(6)
                    //''            stFenchaInt = rrQuirofano("finterve")
                    //''         End If
                    //''    End If
                    //''    RrDiagnostico.Close
                    //''    rrQuirofano.Close
                    //********************************************************************************************************************
                    //********************************************************************************************************************

                    HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(RcAdmision);

                    if (HospitalOCentro.Trim() == "S")
                    {
                        //         StrHospitalaria = "Es dado de alta hospitalaria el d�a"
                        StrHospitalaria = Serrores.VarRPT[3];
                    }
                    else
                    {
                        //         StrHospitalaria = "Es dado de alta el d�a"
                        StrHospitalaria = Serrores.VarRPT[4];
                    }
                    cryInformes.Formulas["{@Hospitalaria}"] = "\"" + StrHospitalaria + "\"";
                    object[] MatrizSql = new object[] { String.Empty, String.Empty, String.Empty, String.Empty };
                    MatrizSql[1] = stGidenpac;
                    MatrizSql[2] = vstNumadme.ToString();
                    MatrizSql[3] = vstAnoadme.ToString();
                    oTextoSql = new DLLTextosSql.TextosSql();
                    string tempRefParam3 = "ADMISION";
                    string tempRefParam4 = "Conexion_Ba";
                    string tempRefParam5 = "proJustificante";
                    short tempRefParam6 = 3;
                    sqlCrystal = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, MatrizSql, RcAdmision));
                    oTextoSql = null;


                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlCrystal, RcAdmision);
                    rrQuirofano = new DataSet();
                    tempAdapter_4.Fill(rrQuirofano);
                    //''''''''''''''''' si existen datos
                    if (rrQuirofano.Tables[0].Rows.Count > 0)
                    {
                        fechallegada = Convert.ToDateTime(rrQuirofano.Tables[0].Rows[0]["fllegada"]);
                        Fecha_Llegada = (DateTimeHelper.ToString(fechallegada).Substring(0, Math.Min(2, DateTimeHelper.ToString(fechallegada).Length)) + " " + Serrores.VarFECHA[20] + " " + Serrores.VarFECHA[Convert.ToInt32(Double.Parse(DateTimeHelper.ToString(fechallegada).Substring(3, Math.Min(2, DateTimeHelper.ToString(fechallegada).Length - 3))))] + " " + Serrores.VarFECHA[20] + " " + DateTimeHelper.ToString(fechallegada).Substring(6, Math.Min(4, DateTimeHelper.ToString(fechallegada).Length - 6)) + " " + Serrores.VarFECHA[21] + " " + DateTimeHelper.ToString(fechallegada).Substring(11, Math.Min(5, DateTimeHelper.ToString(fechallegada).Length - 11))).ToLower();
                    }
                    rrQuirofano.Close();
                    if (stFenchaInt != " ")
                    {
                        stFenchaInt = (stFenchaInt.Substring(0, Math.Min(2, stFenchaInt.Length)) + " " + Serrores.VarFECHA[20] + " " + Serrores.VarFECHA[Convert.ToInt32(Double.Parse(stFenchaInt.Substring(3, Math.Min(2, stFenchaInt.Length - 3))))] + " " + Serrores.VarFECHA[20] + " " + stFenchaInt.Substring(6, Math.Min(4, stFenchaInt.Length - 6))).ToLower(); //& " " & VarFECHA(21) & " " & Mid(stFenchaInt, 12, 5))
                    }
                    cryInformes.Formulas["{@FOR18}"] = "\"" + Fecha_Llegada + "\"";

                    cryInformes.Formulas["{@iServicio}"] = "\"" + Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "SERINFPA", "VALFANU3") + "\"";


                    //O.Frias - 04/05/2009
                    //Se incorpora la funcion de usuario dependiendo de las constantes IUSULARG y IUSUDOCU
                    cryInformes.Formulas["{@Usuario}"] = "\"" + Serrores.fUsuarInfor(VstCodUsua, RcAdmision) + "\"";

                    cryInformes.Formulas["{@iMedico}"] = "\"" + Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "DAJUSING", "VALFANU1") + "\"";
                    cryInformes.Formulas["{@iMotivoAlta}"] = "\"" + Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "DAJUSING", "VALFANU2") + "\"";

                    var sqlSelect = @" DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.ndninifp,
                                    AEPISADM.faltplan,
                                    DSERVICI.dnomserv, DSERVICI.dnomservi1, DSERVICI.dnomservi2,
                                    DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers,
                                    DMOTALTA.dmotalta, DMOTALTA.dmotaltai1, DMOTALTA.dmotaltai2 ";

                    sqlCrystal = sqlCrystal.Replace(" * ", sqlSelect);

                    cryInformes.SQLQuery = sqlCrystal;
                    cryInformes.Connect = "" + VstCrystal + ";" + gUsuario + ";" + gContrase�a + ";" + "";
                    cryInformes.SubreportToChange = "LogotipoIDC";
                    cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                    cryInformes.Connect = "" + VstCrystal + ";" + gUsuario + ";" + gContrase�a + ";";
                    cryInformes.SubreportToChange = "";
                    cryInformes.Connect = "" + VstCrystal + ";" + gUsuario + ";" + gContrase�a + ";" + "";
                    cryInformes.SubreportToChange = "Quirofanos";
                    cryInformes.Connect = "" + VstCrystal + ";" + gUsuario + ";" + gContrase�a + ";";
                    cryInformes.Formulas["{@FOR4}"] = "\"" + stIndicadorInt + "\"";
                    cryInformes.Formulas["{@FOR5}"] = "\"" + stFenchaInt + "\"";
                    cryInformes.Formulas["{@FOR6}"] = "\"" + stAnestesia + "\"";
                    cryInformes.Formulas["{@iAnestesia}"] = "\"" + Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "DAJUSING", "VALFANU3") + "\"";

                    cryInformes.SubreportToChange = "";
                    Serrores.LLenarFormulas(RcAdmision, cryInformes, "AGI123R1", "Quirofanos", VstCrystal, gUsuario, gContrase�a);


                    char[] bocultar = new char[3];

                    bocultar[0] = 'T';
                    if (HospitalOCentro.Trim() == "S")
                    {
                        bocultar[1] = 'T';
                    }
                    else
                    {
                        bocultar[1] = 'F';
                    }

                    for (int i = 0; i <= 1; i++)
                    {
                        cryInformes.SectionFormat[i] = "DETAIL.0." + i.ToString() + ";" + Convert.ToString(bocultar[i]) + ";X";
                    }

                    cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    cryInformes.Destination = destino;
                    //cryInformes.Destination = 0
                    if (Dialogo_impresora_optional != Type.Missing)
                    {
                        cryInformes.CopiesToPrinter = (short)Dialogo_impresora.PrinterSettings.Copies;
                    }
                    cryInformes.WindowShowPrintSetupBtn = true;
                    Serrores.LLenarFormulas(RcAdmision, cryInformes, "AGI123R1", "AGI123R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT
                    cryInformes.Action = 1;
                    cryInformes.PrinterStopPage = cryInformes.PageCount;
                    vacia_formulas(cryInformes, 26);
                    Fomulario.ChbJustificante.IsChecked = false;
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    short tempRefParam8 = 1100;
                    string[] tempRefParam9 = new string[] { Fomulario.ChbJustificante.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam8, RcAdmision, tempRefParam9));
                    puntero = 1;
                }
            }
            while (puntero >= 1);
        }

        internal static void proJustificante(Crystal cryInformes, Crystal.DestinationConstants destino, Form Fomulario, int vstNumadme, int vstAnoadme, string stGidenpac)
        {
            proJustificante(cryInformes, destino, Fomulario, vstNumadme, vstAnoadme, stGidenpac, null);
        }

        internal static void ActualizarSociedadConsumos(DataSet RrCursor, string vCodSocNew, System.DateTime FechaNuevo, bool fNuevo, object vCodInspeNew_optional)
        {
            string vCodInspeNew = (vCodInspeNew_optional == Type.Missing) ? String.Empty : vCodInspeNew_optional as string;

            //************************************************************************
            //***** ACTUALIZAMOS LOS APUNTES GENERADOS EN FCONSPAC (CONSUMOS) ********
            //************************************************************************
            string stSqlFact = "Update FCONSPAC set gsocieda = '" + vCodSocNew + "'";
            if (vCodInspeNew_optional != Type.Missing)
            {
                stSqlFact = stSqlFact + ", ginspecc = '" + vCodInspeNew + "'";
            }
            else
            {
                stSqlFact = stSqlFact + ", ginspecc = NULL ";
            }
            stSqlFact = stSqlFact + " where itiposer= 'H' and ";
            stSqlFact = stSqlFact + " ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and ";
            stSqlFact = stSqlFact + " gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and ";
            stSqlFact = stSqlFact + " gsocieda='" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gsocieda"]) + "'";

            if (fNuevo)
            { // si es el �ltimo apunte o el primero

                object tempRefParam = DateTimeHelper.ToString(FechaNuevo);
                stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(tempRefParam) + "";

            }
            else
            {
                if (Convert.IsDBNull(RrCursor.Tables[0].Rows[0]["ffinmovi"]))
                {
                    object tempRefParam2 = RrCursor.Tables[0].Rows[0]["fmovimie"];
                    stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(tempRefParam2) + "";

                }
                else
                {
                    object tempRefParam3 = RrCursor.Tables[0].Rows[0]["fmovimie"];
                    stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(tempRefParam3) + "";
                    object tempRefParam4 = RrCursor.Tables[0].Rows[0]["ffinmovi"];
                    stSqlFact = stSqlFact + " and foper<=" + Serrores.FormatFechaHM(tempRefParam4) + "";
                }
            }


            SqlCommand tempCommand = new SqlCommand(stSqlFact, RcAdmision);
            tempCommand.ExecuteNonQuery();

        }

        internal static void ActualizarSociedadConsumos(DataSet RrCursor, string vCodSocNew, System.DateTime FechaNuevo, bool fNuevo)
        {
            ActualizarSociedadConsumos(RrCursor, vCodSocNew, FechaNuevo, fNuevo, Type.Missing);
        }

        internal static void ActualizarServicioConsumos(int A�o, int Numero, int vCodSerOld, string vCodSerNew, ref string FechaNuevo, bool fNuevo, ref string FechaFinMovimiento)
        {
            //************************************************************************
            //***** ACTUALIZAMOS LOS APUNTES GENERADOS EN FCONSPAC (CONSUMOS) ********
            //************************************************************************
            string stSqlFact = "Update FCONSPAC set gserreal = '" + vCodSerNew + "'";
            stSqlFact = stSqlFact + " where itiposer= 'H' and ";
            stSqlFact = stSqlFact + " ganoregi=" + A�o.ToString() + " and ";
            stSqlFact = stSqlFact + " gnumregi=" + Numero.ToString() + " and ";
            stSqlFact = stSqlFact + " gserreal='" + vCodSerOld.ToString() + "'";

            if (fNuevo)
            { // si es el �ltimo apunte o el primero

                object tempRefParam = FechaNuevo;
                stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(tempRefParam) + "";
                FechaNuevo = Convert.ToString(tempRefParam);
            }
            else
            {
                if (FechaFinMovimiento == "00:00:00")
                {
                    object tempRefParam2 = FechaNuevo;
                    stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(tempRefParam2) + "";
                    FechaNuevo = Convert.ToString(tempRefParam2);
                }
                else
                {
                    object tempRefParam3 = FechaNuevo;
                    stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(tempRefParam3) + "";
                    FechaNuevo = Convert.ToString(tempRefParam3);

                    object tempRefParam4 = FechaFinMovimiento;
                    stSqlFact = stSqlFact + " and foper<=" + Serrores.FormatFechaHM(tempRefParam4) + "";
                    FechaFinMovimiento = Convert.ToString(tempRefParam4);
                }
            }


            SqlCommand tempCommand = new SqlCommand(stSqlFact, RcAdmision);
            tempCommand.ExecuteNonQuery();

        }

        internal static object CargarComboAlfanumerico(string sql, string Campocod, string campoDes, dynamic comboacargar, bool bVaciar = false)
        {
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
            DataSet RrCombo = new DataSet();
            tempAdapter.Fill(RrCombo);
            if (bVaciar || false)
            {
                comboacargar.Clear();
                comboacargar.ColumnCount = 2;
                comboacargar.ColumnWidths = "15;0";
            }

            //camaya todo_x_3
            //comboacargar.AddItem("", RrCombo.getAbsolutePosition() - 1);            
            //comboacargar.List[RrCombo.getAbsolutePosition() - 1, 1] = "";

            foreach (DataRow iteration_row in RrCombo.Tables[0].Rows)
            {
                //camaya todo_x_3
                //comboacargar.AddItem(Strings.StrConv(Convert.ToString(iteration_row[campoDes]).Trim(), VbStrConv.ProperCase, 0), RrCombo.getAbsolutePosition() - 1);
                //comboacargar.List[RrCombo.getAbsolutePosition() - 1, 1] = iteration_row[Campocod];
            }
            RrCombo.Close();
            return null;
        }

        
        internal static void proAcompa(PrintDialog Dialogo_impresora, Crystal.DestinationConstants destino, int iGanoadme, int iGnumadme, Crystal cryInformes, Form VentDocumento)
        {
            DataSet rrAcompas = null;
            string vstFechaLlegada = String.Empty;
            string sqlCrystal = String.Empty;
            string vstGidenpac = String.Empty;
            string sql = String.Empty;

            string MaysculaOMinuscula = "m";
            int puntero = 1;
            do
            {
                try
                {

                    sql = " SELECT gidenpac From AEPISADM Where " + " AEPISADM.gnumadme =" + iGnumadme.ToString() + " AND " + " AEPISADM.ganoadme =" + iGanoadme.ToString() + " ";

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
                    rrAcompas = new DataSet();
                    tempAdapter.Fill(rrAcompas);
                    if (rrAcompas.Tables[0].Rows.Count != 0)
                    {
                        vstGidenpac = Convert.ToString(rrAcompas.Tables[0].Rows[0]["gidenpac"]);
                    }
                    rrAcompas.Close();


                    Serrores.Obtener_Multilenguaje(RcAdmision, vstGidenpac);
                    Serrores.ObtenerTextosRPT(RcAdmision, "ADMISION", "AGI12BR1");
                    Serrores.ObtenerMesesSemanas(RcAdmision);

                    sqlCrystal = " SELECT *" + " From " + " AEPISADM, DPACIENT" + " Where " + " AEPISADM.gnumadme =" + iGnumadme.ToString() + " AND " + " AEPISADM.ganoadme =" + iGanoadme.ToString() + " and " + " AEPISADM.gidenpac = DPACIENT.gidenpac ";

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlCrystal, RcAdmision);
                    rrAcompas = new DataSet();
                    tempAdapter_2.Fill(rrAcompas);
                    if (rrAcompas.Tables[0].Rows.Count != 0)
                    {
                        vstFechaLlegada = Convert.ToDateTime(rrAcompas.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        vstFechaLlegada = " ";
                    }

                    //I.S.M. 27-Mar-2000 A�adido para modificar los datos del acompa�ante
                    string sPaciente = String.Empty;

                    sPaciente = "" + Convert.ToString(rrAcompas.Tables[0].Rows[0]["dnombpac"]).Trim(); //DPACIENT.								
                    sPaciente = (sPaciente + ((Convert.IsDBNull(rrAcompas.Tables[0].Rows[0]["dape1pac"])) ? "" : " " + Convert.ToString(rrAcompas.Tables[0].Rows[0]["dape1pac"]))).Trim();
                    sPaciente = (sPaciente + ((Convert.IsDBNull(rrAcompas.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(rrAcompas.Tables[0].Rows[0]["dape2pac"]))).Trim();

                    Justificante_asistencia_acompa�ante.DefInstance.VentanaDocumento = VentDocumento;

                    Justificante_asistencia_acompa�ante.DefInstance.lblPaciente2.Text = sPaciente;
                    Justificante_asistencia_acompa�ante.DefInstance.sdcFechaIni.Value = DateTime.Parse(vstFechaLlegada);
                    Justificante_asistencia_acompa�ante.DefInstance.sdcFechaIni.MinDate = DateTime.Parse(vstFechaLlegada);

                    if (Convert.IsDBNull(rrAcompas.Tables[0].Rows[0]["faltaadm"]))
                    {
                        Justificante_asistencia_acompa�ante.DefInstance.sdcFechaIni.MaxDate = DateTime.Now;
                    }
                    else
                    {
                        Justificante_asistencia_acompa�ante.DefInstance.sdcFechaIni.MaxDate = Convert.ToDateTime(rrAcompas.Tables[0].Rows[0]["faltaadm"]);
                    }

                    Justificante_asistencia_acompa�ante.DefInstance.mskHora.Text = Convert.ToDateTime(rrAcompas.Tables[0].Rows[0]["fllegada"]).ToString("HH:mm");
                    VstNomacompa = "";
                    VstParentescoAcompa = "";
                    VstHoraAcompa = "";
                    VstFechaAcompa = "";

                    BtCancelar = false;

                    //I.S.M. 27-Mar-2000 A�adido para modificar los datos del acompa�ante
                    Justificante_asistencia_acompa�ante.DefInstance.ShowDialog();

                    Application.DoEvents();

                    if (BtCancelar || (VstNomacompa == "" && VstParentescoAcompa == "" && VstHoraAcompa == "" && VstFechaAcompa == ""))
                    {
                        cryInformes.Reset();
                        return;
                    }

                    //proDialogo_impre cryInformes 'Pantalla para elegir impresora

                    cryInformes.ReportFileName = "" + PathString + "\\rpt\\Agi12br1_CR11.rpt";
                    //*********cabeceras
                    if (!VstrCrystal.VfCabecera)
                    {
                        proCabCrystal();
                    }
                    //*******************
                    cryInformes.Formulas["{@FOR1}"] = "\"" + VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@FOR2}"] = "\"" + VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@FOR3}"] = "\"" + VstrCrystal.VstDireccHo + "\"";
                    //I.S.M. 27-Mar-2000 Modificado para los datos del acompa�ante
                    //cryInformes.Formulas(3) = "Fecha= """ & vstFechaLlegada & """"
                    cryInformes.Formulas["{@Acomp}"] = "'" + VstNomacompa + "'";
                    cryInformes.Formulas["{@Hora}"] = "'" + VstHoraAcompa + "'";
                    cryInformes.Formulas["{@fecha}"] = "'" + Convert.ToDateTime(VstFechaAcompa).ToShortDateString() + "'";
                    cryInformes.Formulas["{@Parentesco}"] = "'" + VstParentescoAcompa + "'";

                    string stsqltemp = String.Empty;
                    DataSet rrtemp = null;
                    string dnicedula1 = String.Empty;

                    stsqltemp = "select valfanu1,valfanu1i1,valfanu1i2 from sconsglo where gconsglo= 'idenpers' ";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsqltemp, RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_3.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count != 0)
                    {
                        //oscar
                        //DNICEDULA = Trim(Rs("valfanu1"))
                        switch (Serrores.CampoIdioma)
                        {
                            case "IDIOMA0":
                                dnicedula1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim() + "";
                                break;
                            case "IDIOMA1":
                                dnicedula1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]).Trim() + "";
                                break;
                            case "IDIOMA2":
                                dnicedula1 = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i2"]).Trim() + "";
                                break;
                        }
                        //--------
                    }
                    rrtemp.Close();

                    cryInformes.Formulas["{@Dni/cedula}"] = "'" + ((stFORMFILI != "E") ? dnicedula1.ToLower() : dnicedula1) + "'";
                    cryInformes.Formulas["{@LOGOIDC}"] = "\"" + Serrores.ExisteLogo(RcAdmision) + "\"";
                    cryInformes.Formulas["{@FechaHoy}"] = "\"" + Serrores.CapturarFechaHoy(MaysculaOMinuscula) + "\"";

                    //O.Frias - 04/05/2009
                    //Se incorpora la funcion de usuario dependiendo de las constantes IUSULARG y IUSUDOCU
                    cryInformes.Formulas["{@Usuario}"] = "\"" + Serrores.fUsuarInfor(VstCodUsua, RcAdmision) + "\"";


                    //    cryInformes.WindowTitle = "Justificante de Asistencia del acompa�ante"
                    cryInformes.WindowTitle = Serrores.VarRPT[1];

                    var sqlSelect = " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.ndninifp ";
                    sqlCrystal = sqlCrystal.Replace(" * ", sqlSelect);

                    cryInformes.SQLQuery = sqlCrystal;
                    cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    cryInformes.Destination = destino;
                    cryInformes.WindowShowPrintSetupBtn = true;
                    cryInformes.CopiesToPrinter = (short)Dialogo_impresora.PrinterSettings.Copies;
                    cryInformes.Connect = "" + VstCrystal + ";" + gUsuario + ";" + gContrase�a + "" + "";
                    cryInformes.SubreportToChange = "LogotipoIDC";
                    cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                    cryInformes.Connect = "" + VstCrystal + ";" + gUsuario + ";" + gContrase�a + "";
                    cryInformes.SubreportToChange = "";

                    Serrores.LLenarFormulas(RcAdmision, cryInformes, "AGI12BR1", "AGI12BR1", VstCrystal, gUsuario, gContrase�a); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT

                    cryInformes.Action = 1;
                    cryInformes.PrinterStopPage = cryInformes.PageCount;
                    vacia_formulas(cryInformes, 10);
                    cryInformes.Reset();
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    //I.S.M. 29-Mar-2000 A�adido para controlar los errores                
                    if (Information.Err().Number == 32755 || Information.Err().Number == 20545 || Information.Err().Number == 28663)
                    { //Cancel, Cancel printing o No hay impresora
                        cryInformes.Reset();
                        return;
                    }
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "proAcompa:DocumentosIngreso", ex);

                    puntero = 0;
                }
            }

            while (puntero >= 1);

        }
    }
}