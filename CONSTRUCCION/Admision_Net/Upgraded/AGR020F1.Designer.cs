using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class docu_reservas
	{

		#region "Upgrade Support "
		private static docu_reservas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static docu_reservas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new docu_reservas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "LsbLista1", "_CbInsTodo_0", "_CbInsUno_0", "_CbDelTodo_0", "_CbDelUno_0", "LsbLista2", "lbPaciente", "Frame3", "cbAceptar", "cbCancelar", "CbDelTodo", "CbDelUno", "CbInsTodo", "CbInsUno", "listBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadListControl LsbLista1;
		private Telerik.WinControls.UI.RadButton _CbInsTodo_0;
		private Telerik.WinControls.UI.RadButton _CbInsUno_0;
		private Telerik.WinControls.UI.RadButton _CbDelTodo_0;
		private Telerik.WinControls.UI.RadButton _CbDelUno_0;
		public Telerik.WinControls.UI.RadListControl LsbLista2;
        public Telerik.WinControls.UI.RadLabel lbPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton[] CbDelTodo = new Telerik.WinControls.UI.RadButton[1];
		public Telerik.WinControls.UI.RadButton[] CbDelUno = new Telerik.WinControls.UI.RadButton[1];
		public Telerik.WinControls.UI.RadButton[] CbInsTodo = new Telerik.WinControls.UI.RadButton[1];
		public Telerik.WinControls.UI.RadButton[] CbInsUno = new Telerik.WinControls.UI.RadButton[1];
		private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
        
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.LsbLista1 = new Telerik.WinControls.UI.RadListControl();
            this._CbInsTodo_0 = new Telerik.WinControls.UI.RadButton();
            this._CbInsUno_0 = new Telerik.WinControls.UI.RadButton();
            this._CbDelTodo_0 = new Telerik.WinControls.UI.RadButton();
            this._CbDelUno_0 = new Telerik.WinControls.UI.RadButton();
            this.LsbLista2 = new Telerik.WinControls.UI.RadListControl();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbPaciente = new Telerik.WinControls.UI.RadLabel();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // LsbLista1
            // 
            this.LsbLista1.BackColor = System.Drawing.SystemColors.Window;
            //this.LsbLista1.ColumnWidth = 116;
            this.LsbLista1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista1.ForeColor = System.Drawing.SystemColors.WindowText;
            //this.LsbLista1.IntegralHeight = false;
            this.LsbLista1.Location = new System.Drawing.Point(8, 59);
            this.LsbLista1.Name = "LsbLista1";
            this.LsbLista1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            //this.listBoxHelper1.SetSelectionMode(this.LsbLista1, System.Windows.Forms.SelectionMode.One);
            this.LsbLista1.Size = new System.Drawing.Size(233, 165);
            this.LsbLista1.TabIndex = 9;
            this.LsbLista1.TabStop = false;
            this.LsbLista1.DoubleClick += new System.EventHandler(this.LsbLista1_DoubleClick);
            // 
            // _CbInsTodo_0
            // 
            this._CbInsTodo_0.BackColor = System.Drawing.SystemColors.Control;
            this._CbInsTodo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbInsTodo_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CbInsTodo_0.Location = new System.Drawing.Point(264, 59);
            this._CbInsTodo_0.Name = "_CbInsTodo_0";
            this._CbInsTodo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbInsTodo_0.Size = new System.Drawing.Size(33, 21);
            this._CbInsTodo_0.TabIndex = 8;
            this._CbInsTodo_0.Text = ">>";
            this._CbInsTodo_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this._CbInsTodo_0.UseVisualStyleBackColor = false;
            this._CbInsTodo_0.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // _CbInsUno_0
            // 
            this._CbInsUno_0.BackColor = System.Drawing.SystemColors.Control;
            this._CbInsUno_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbInsUno_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CbInsUno_0.Location = new System.Drawing.Point(264, 106);
            this._CbInsUno_0.Name = "_CbInsUno_0";
            this._CbInsUno_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbInsUno_0.Size = new System.Drawing.Size(33, 21);
            this._CbInsUno_0.TabIndex = 7;
            this._CbInsUno_0.Text = ">";
            this._CbInsUno_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this._CbInsUno_0.UseVisualStyleBackColor = false;
            this._CbInsUno_0.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // _CbDelTodo_0
            // 
            this._CbDelTodo_0.BackColor = System.Drawing.SystemColors.Control;
            this._CbDelTodo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbDelTodo_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CbDelTodo_0.Location = new System.Drawing.Point(264, 152);
            this._CbDelTodo_0.Name = "_CbDelTodo_0";
            this._CbDelTodo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbDelTodo_0.Size = new System.Drawing.Size(33, 21);
            this._CbDelTodo_0.TabIndex = 6;
            this._CbDelTodo_0.Text = "<<";
            this._CbDelTodo_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this._CbDelTodo_0.UseVisualStyleBackColor = false;
            this._CbDelTodo_0.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // _CbDelUno_0
            // 
            this._CbDelUno_0.BackColor = System.Drawing.SystemColors.Control;
            this._CbDelUno_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbDelUno_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CbDelUno_0.Location = new System.Drawing.Point(264, 198);
            this._CbDelUno_0.Name = "_CbDelUno_0";
            this._CbDelUno_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbDelUno_0.Size = new System.Drawing.Size(33, 21);
            this._CbDelUno_0.TabIndex = 5;
            this._CbDelUno_0.Text = "<";
            this._CbDelUno_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this._CbDelUno_0.UseVisualStyleBackColor = false;
            this._CbDelUno_0.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // LsbLista2
            // 
            this.LsbLista2.BackColor = System.Drawing.SystemColors.Window;
            //this.LsbLista2.ColumnWidth = 116;
            this.LsbLista2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista2.ForeColor = System.Drawing.SystemColors.WindowText;
            //this.LsbLista2.IntegralHeight = false;
            this.LsbLista2.Location = new System.Drawing.Point(320, 59);
            this.LsbLista2.Name = "LsbLista2";
            this.LsbLista2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            //this.listBoxHelper1.SetSelectionMode(this.LsbLista2, System.Windows.Forms.SelectionMode.One);
            this.LsbLista2.Size = new System.Drawing.Size(233, 165);
            this.LsbLista2.TabIndex = 4;
            this.LsbLista2.TabStop = false;
            this.LsbLista2.DoubleClick += new System.EventHandler(this.LsbLista2_DoubleClick);
            // 
            // Frame3
            // 
            //this.Frame3.BackColor = System.Drawing.SystemColors.Control;
            this.Frame3.Controls.Add(this.lbPaciente);
            //this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame3.Location = new System.Drawing.Point(0, 0);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(554, 45);
            this.Frame3.TabIndex = 2;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Paciente";
            // 
            // lbPaciente
            // 
            //this.lbPaciente.BackColor = System.Drawing.SystemColors.Control;
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbPaciente.Location = new System.Drawing.Point(8, 17);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(2, 2);
            this.lbPaciente.TabIndex = 3;
            // 
            // cbAceptar
            // 
            //this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbAceptar.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbAceptar.Location = new System.Drawing.Point(385, 229);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 0;
            this.cbAceptar.Text = "&Imprimir";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCancelar
            // 
            //this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbCancelar.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbCancelar.Location = new System.Drawing.Point(474, 228);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 1;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // docu_reservas
            // 
            this.AcceptButton = this.cbAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          //  this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(562, 262);
            this.Controls.Add(this.LsbLista1);
            this.Controls.Add(this._CbInsTodo_0);
            this.Controls.Add(this._CbInsUno_0);
            this.Controls.Add(this._CbDelTodo_0);
            this.Controls.Add(this._CbDelUno_0);
            this.Controls.Add(this.LsbLista2);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCancelar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "docu_reservas";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Documentos - AGR020F1";
            this.Closed += new System.EventHandler(this.docu_reservas_Closed);
            this.Load += new System.EventHandler(this.docu_reservas_Load);
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeCbInsUno();
			InitializeCbInsTodo();
			InitializeCbDelUno();
			InitializeCbDelTodo();
		}
		void InitializeCbInsUno()
		{
			this.CbInsUno = new Telerik.WinControls.UI.RadButton[1];
			this.CbInsUno[0] = _CbInsUno_0;
		}
		void InitializeCbInsTodo()
		{
			this.CbInsTodo = new Telerik.WinControls.UI.RadButton[1];
			this.CbInsTodo[0] = _CbInsTodo_0;
		}
		void InitializeCbDelUno()
		{
			this.CbDelUno = new Telerik.WinControls.UI.RadButton[1];
			this.CbDelUno[0] = _CbDelUno_0;
		}
		void InitializeCbDelTodo()
		{
			this.CbDelTodo = new Telerik.WinControls.UI.RadButton[1];
			this.CbDelTodo[0] = _CbDelTodo_0;
		}
		#endregion
	}
}