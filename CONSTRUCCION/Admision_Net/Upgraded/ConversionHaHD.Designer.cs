using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class ConversionHaHD
	{

		#region "Upgrade Support "
		private static ConversionHaHD m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static ConversionHaHD DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new ConversionHaHD();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "rbInmediato", "rbUrgente", "rbProgramado", "Fringreso", "Label7", "lbFechaAlta", "Label21", "lbFechaIngreso", "Frame1", "Label9", "lbIdEpisodio", "Label4", "Label3", "Label1", "lbServicio", "lbDiagnostico", "lbResponsable", "Frame3", "CbCancelar", "CbAceptar", "RbHombre", "RbMujer", "Frmsexo", "lbHistoria", "Label12", "lbNombrePaciente", "Label17", "Frame2", "tbCama", "CbSeleccion", "Label2", "Frame4", "Label8", "cbbPlazaDia", "Label6", "lbInfo"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadRadioButton rbInmediato;
		public Telerik.WinControls.UI.RadRadioButton rbUrgente;
		public Telerik.WinControls.UI.RadRadioButton rbProgramado;
		public Telerik.WinControls.UI.RadGroupBox Fringreso;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadTextBox lbFechaAlta;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadTextBox lbFechaIngreso;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadTextBox lbIdEpisodio;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox lbServicio;
		public Telerik.WinControls.UI.RadTextBox lbDiagnostico;
		public Telerik.WinControls.UI.RadTextBox lbResponsable;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadRadioButton RbHombre;
		public Telerik.WinControls.UI.RadRadioButton RbMujer;
		public Telerik.WinControls.UI.RadGroupBox Frmsexo;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadTextBox lbNombrePaciente;
		public Telerik.WinControls.UI.RadLabel Label17;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadTextBoxControl tbCama;
		public Telerik.WinControls.UI.RadButton CbSeleccion;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Telerik.WinControls.UI.RadLabel Label8;
		//public AxMSForms.AxComboBox cbbPlazaDia;

        public Telerik.WinControls.UI.RadDropDownList cbbPlazaDia;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel lbInfo;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Fringreso = new Telerik.WinControls.UI.RadGroupBox();
            this.rbInmediato = new Telerik.WinControls.UI.RadRadioButton();
            this.rbUrgente = new Telerik.WinControls.UI.RadRadioButton();
            this.rbProgramado = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.lbFechaAlta = new Telerik.WinControls.UI.RadTextBox();
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this.lbFechaIngreso = new Telerik.WinControls.UI.RadTextBox();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.lbIdEpisodio = new Telerik.WinControls.UI.RadTextBox();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lbServicio = new Telerik.WinControls.UI.RadTextBox();
            this.lbDiagnostico = new Telerik.WinControls.UI.RadTextBox();
            this.lbResponsable = new Telerik.WinControls.UI.RadTextBox();
            this.CbCancelar = new Telerik.WinControls.UI.RadButton();
            this.CbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frmsexo = new Telerik.WinControls.UI.RadGroupBox();
            this.RbHombre = new Telerik.WinControls.UI.RadRadioButton();
            this.RbMujer = new Telerik.WinControls.UI.RadRadioButton();
            this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.lbNombrePaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label17 = new Telerik.WinControls.UI.RadLabel();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbCama = new Telerik.WinControls.UI.RadTextBoxControl();
            this.CbSeleccion = new Telerik.WinControls.UI.RadButton();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label8 = new Telerik.WinControls.UI.RadLabel();
            this.cbbPlazaDia = new Telerik.WinControls.UI.RadDropDownList();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.lbInfo = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Fringreso)).BeginInit();
            this.Fringreso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbInmediato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbUrgente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbProgramado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFechaIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIdEpisodio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDiagnostico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbResponsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frmsexo)).BeginInit();
            this.Frmsexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbHombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbMujer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombrePaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbSeleccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPlazaDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Fringreso
            // 
            this.Fringreso.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Fringreso.Controls.Add(this.rbInmediato);
            this.Fringreso.Controls.Add(this.rbUrgente);
            this.Fringreso.Controls.Add(this.rbProgramado);
            this.Fringreso.HeaderText = "Tipo Ingreso";
            this.Fringreso.Location = new System.Drawing.Point(4, 308);
            this.Fringreso.Name = "Fringreso";
            this.Fringreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Fringreso.Size = new System.Drawing.Size(108, 70);
            this.Fringreso.TabIndex = 28;
            this.Fringreso.Text = "Tipo Ingreso";
            // 
            // rbInmediato
            // 
            this.rbInmediato.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbInmediato.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbInmediato.Location = new System.Drawing.Point(8, 16);
            this.rbInmediato.Name = "rbInmediato";
            this.rbInmediato.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbInmediato.Size = new System.Drawing.Size(71, 18);
            this.rbInmediato.TabIndex = 31;
            this.rbInmediato.Text = "Inmediato";
            this.rbInmediato.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // rbUrgente
            // 
            this.rbUrgente.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbUrgente.Location = new System.Drawing.Point(8, 32);
            this.rbUrgente.Name = "rbUrgente";
            this.rbUrgente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbUrgente.Size = new System.Drawing.Size(61, 18);
            this.rbUrgente.TabIndex = 30;
            this.rbUrgente.Text = "Urgente";
            // 
            // rbProgramado
            // 
            this.rbProgramado.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbProgramado.Location = new System.Drawing.Point(8, 48);
            this.rbProgramado.Name = "rbProgramado";
            this.rbProgramado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbProgramado.Size = new System.Drawing.Size(82, 18);
            this.rbProgramado.TabIndex = 29;
            this.rbProgramado.Text = "Programado";
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Frame1);
            this.Frame3.Controls.Add(this.Label9);
            this.Frame3.Controls.Add(this.lbIdEpisodio);
            this.Frame3.Controls.Add(this.Label4);
            this.Frame3.Controls.Add(this.Label3);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.lbServicio);
            this.Frame3.Controls.Add(this.lbDiagnostico);
            this.Frame3.Controls.Add(this.lbResponsable);
            this.Frame3.HeaderText = "Datos del Episodio de Hospitalizaci�n";
            this.Frame3.Location = new System.Drawing.Point(4, 84);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(529, 167);
            this.Frame3.TabIndex = 14;
            this.Frame3.Text = "Datos del Episodio de Hospitalizaci�n";
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.lbFechaAlta);
            this.Frame1.Controls.Add(this.Label21);
            this.Frame1.Controls.Add(this.lbFechaIngreso);
            this.Frame1.HeaderText = "Fecha";
            this.Frame1.Location = new System.Drawing.Point(4, 120);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(509, 41);
            this.Frame1.TabIndex = 15;
            this.Frame1.Text = "Fecha";
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Location = new System.Drawing.Point(260, 18);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(59, 18);
            this.Label7.TabIndex = 27;
            this.Label7.Text = "Fecha alta:";
            // 
            // lbFechaAlta
            // 
            this.lbFechaAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFechaAlta.Enabled = false;
            this.lbFechaAlta.Location = new System.Drawing.Point(322, 15);
            this.lbFechaAlta.Name = "lbFechaAlta";
            this.lbFechaAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFechaAlta.Size = new System.Drawing.Size(117, 20);
            this.lbFechaAlta.TabIndex = 26;
            // 
            // Label21
            // 
            this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label21.Location = new System.Drawing.Point(50, 18);
            this.Label21.Name = "Label21";
            this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label21.Size = new System.Drawing.Size(78, 18);
            this.Label21.TabIndex = 17;
            this.Label21.Text = "Fecha ingreso:";
            // 
            // lbFechaIngreso
            // 
            this.lbFechaIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFechaIngreso.Enabled = false;
            this.lbFechaIngreso.Location = new System.Drawing.Point(132, 15);
            this.lbFechaIngreso.Name = "lbFechaIngreso";
            this.lbFechaIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFechaIngreso.Size = new System.Drawing.Size(117, 20);
            this.lbFechaIngreso.TabIndex = 16;
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Location = new System.Drawing.Point(308, 16);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(64, 18);
            this.Label9.TabIndex = 35;
            this.Label9.Text = "Id Episodio:";
            // 
            // lbIdEpisodio
            // 
            this.lbIdEpisodio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIdEpisodio.Enabled = false;
            this.lbIdEpisodio.Location = new System.Drawing.Point(384, 12);
            this.lbIdEpisodio.Name = "lbIdEpisodio";
            this.lbIdEpisodio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIdEpisodio.Size = new System.Drawing.Size(136, 20);
            this.lbIdEpisodio.TabIndex = 34;
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(8, 66);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(68, 18);
            this.Label4.TabIndex = 23;
            this.Label4.Text = "Diagn�stico:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(8, 42);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(47, 18);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "Servicio:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 89);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(109, 18);
            this.Label1.TabIndex = 21;
            this.Label1.Text = "M�dico responsable:";
            // 
            // lbServicio
            // 
            this.lbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbServicio.Enabled = false;
            this.lbServicio.Location = new System.Drawing.Point(123, 40);
            this.lbServicio.Name = "lbServicio";
            this.lbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbServicio.Size = new System.Drawing.Size(397, 20);
            this.lbServicio.TabIndex = 20;
            // 
            // lbDiagnostico
            // 
            this.lbDiagnostico.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDiagnostico.Enabled = false;
            this.lbDiagnostico.Location = new System.Drawing.Point(123, 64);
            this.lbDiagnostico.Name = "lbDiagnostico";
            this.lbDiagnostico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDiagnostico.Size = new System.Drawing.Size(397, 20);
            this.lbDiagnostico.TabIndex = 19;
            // 
            // lbResponsable
            // 
            this.lbResponsable.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbResponsable.Enabled = false;
            this.lbResponsable.Location = new System.Drawing.Point(123, 88);
            this.lbResponsable.Name = "lbResponsable";
            this.lbResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbResponsable.Size = new System.Drawing.Size(397, 20);
            this.lbResponsable.TabIndex = 18;
            // 
            // CbCancelar
            // 
            this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbCancelar.Location = new System.Drawing.Point(450, 348);
            this.CbCancelar.Name = "CbCancelar";
            this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCancelar.Size = new System.Drawing.Size(81, 29);
            this.CbCancelar.TabIndex = 13;
            this.CbCancelar.Text = "&Cancelar";
            this.CbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
            // 
            // CbAceptar
            // 
            this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbAceptar.Location = new System.Drawing.Point(352, 348);
            this.CbAceptar.Name = "CbAceptar";
            this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbAceptar.Size = new System.Drawing.Size(81, 29);
            this.CbAceptar.TabIndex = 12;
            this.CbAceptar.Text = "&Aceptar";
            this.CbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.Frmsexo);
            this.Frame2.Controls.Add(this.lbHistoria);
            this.Frame2.Controls.Add(this.Label12);
            this.Frame2.Controls.Add(this.lbNombrePaciente);
            this.Frame2.Controls.Add(this.Label17);
            this.Frame2.HeaderText = "";
            this.Frame2.Location = new System.Drawing.Point(100, 4);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(433, 76);
            this.Frame2.TabIndex = 4;
            // 
            // Frmsexo
            // 
            this.Frmsexo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frmsexo.Controls.Add(this.RbHombre);
            this.Frmsexo.Controls.Add(this.RbMujer);
            this.Frmsexo.HeaderText = "Sexo";
            this.Frmsexo.Location = new System.Drawing.Point(260, 30);
            this.Frmsexo.Name = "Frmsexo";
            this.Frmsexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frmsexo.Size = new System.Drawing.Size(161, 39);
            this.Frmsexo.TabIndex = 5;
            this.Frmsexo.Text = "Sexo";
            // 
            // RbHombre
            // 
            this.RbHombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbHombre.Enabled = false;
            this.RbHombre.Location = new System.Drawing.Point(17, 14);
            this.RbHombre.Name = "RbHombre";
            this.RbHombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbHombre.Size = new System.Drawing.Size(61, 18);
            this.RbHombre.TabIndex = 7;
            this.RbHombre.Text = "Hombre";
            // 
            // RbMujer
            // 
            this.RbMujer.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbMujer.Enabled = false;
            this.RbMujer.Location = new System.Drawing.Point(96, 14);
            this.RbMujer.Name = "RbMujer";
            this.RbMujer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbMujer.Size = new System.Drawing.Size(49, 18);
            this.RbMujer.TabIndex = 6;
            this.RbMujer.Text = "Mujer";
            // 
            // lbHistoria
            // 
            this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHistoria.Enabled = false;
            this.lbHistoria.Location = new System.Drawing.Point(92, 44);
            this.lbHistoria.Name = "lbHistoria";
            this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHistoria.Size = new System.Drawing.Size(97, 20);
            this.lbHistoria.TabIndex = 11;
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(8, 44);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(81, 18);
            this.Label12.TabIndex = 10;
            this.Label12.Text = "Historia cl�nica:";
            // 
            // lbNombrePaciente
            // 
            this.lbNombrePaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNombrePaciente.Enabled = false;
            this.lbNombrePaciente.Location = new System.Drawing.Point(92, 9);
            this.lbNombrePaciente.Name = "lbNombrePaciente";
            this.lbNombrePaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNombrePaciente.Size = new System.Drawing.Size(333, 20);
            this.lbNombrePaciente.TabIndex = 9;
            // 
            // Label17
            // 
            this.Label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label17.Location = new System.Drawing.Point(8, 10);
            this.Label17.Name = "Label17";
            this.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label17.Size = new System.Drawing.Size(51, 18);
            this.Label17.TabIndex = 8;
            this.Label17.Text = "Paciente:";
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.tbCama);
            this.Frame4.Controls.Add(this.CbSeleccion);
            this.Frame4.Controls.Add(this.Label2);
            this.Frame4.HeaderText = "Plaza de D�a Destino";
            this.Frame4.Location = new System.Drawing.Point(4, 252);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(529, 53);
            this.Frame4.TabIndex = 0;
            this.Frame4.Text = "Plaza de D�a Destino";
            // 
            // tbCama
            // 
            this.tbCama.AcceptsReturn = true;
            this.tbCama.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCama.Location = new System.Drawing.Point(88, 24);
            this.tbCama.MaxLength = 0;
            this.tbCama.Name = "tbCama";
            this.tbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCama.Size = new System.Drawing.Size(81, 20);
            this.tbCama.TabIndex = 2;
            this.tbCama.TextChanged += new System.EventHandler(this.tbCama_TextChanged);
            this.tbCama.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCama_KeyDown);
            this.tbCama.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCama_KeyPress);
            this.tbCama.Leave += new System.EventHandler(this.tbCama_Leave);
            // 
            // CbSeleccion
            // 
            this.CbSeleccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbSeleccion.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbSeleccion.Location = new System.Drawing.Point(176, 24);
            this.CbSeleccion.Name = "CbSeleccion";
            this.CbSeleccion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbSeleccion.Size = new System.Drawing.Size(70, 18);
            this.CbSeleccion.TabIndex = 1;
            this.CbSeleccion.Text = "Selecci�n";
            this.CbSeleccion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbSeleccion.Click += new System.EventHandler(this.CbSeleccion_Click);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(18, 25);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(70, 18);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Plaza de D�a:";
            // 
            // Label8
            // 
            this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label8.Location = new System.Drawing.Point(120, 316);
            this.Label8.Name = "Label8";
            this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label8.Size = new System.Drawing.Size(95, 18);
            this.Label8.TabIndex = 33;
            this.Label8.Text = "Tipo de Plaza D�a:";
            // 
            // cbbPlazaDia
            // 
            this.cbbPlazaDia.Location = new System.Drawing.Point(226, 312);
            this.cbbPlazaDia.Name = "cbbPlazaDia";
            this.cbbPlazaDia.Size = new System.Drawing.Size(307, 20);
            this.cbbPlazaDia.TabIndex = 32;
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(8, 4);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(95, 32);
            this.Label6.TabIndex = 25;
            this.Label6.Text = "H > HD";
            // 
            // lbInfo
            // 
            this.lbInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbInfo.Location = new System.Drawing.Point(120, 344);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbInfo.Size = new System.Drawing.Size(89, 18);
            this.lbInfo.TabIndex = 24;
            this.lbInfo.Text = "PROCESANDO....";
            this.lbInfo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.lbInfo.Visible = false;
            // 
            // ConversionHaHD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CbCancelar;
            this.ClientSize = new System.Drawing.Size(540, 386);
            this.Controls.Add(this.Fringreso);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.CbCancelar);
            this.Controls.Add(this.CbAceptar);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Frame4);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.cbbPlazaDia);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.lbInfo);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConversionHaHD";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conversi�n de Episodio de Hospitalizaci�n a Hospital de D�a";
            this.Activated += new System.EventHandler(this.ConversionHaHD_Activated);
            this.Closed += new System.EventHandler(this.ConversionHaHD_Closed);
            this.Load += new System.EventHandler(this.ConversionHaHD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Fringreso)).EndInit();
            this.Fringreso.ResumeLayout(false);
            this.Fringreso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbInmediato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbUrgente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbProgramado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFechaIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIdEpisodio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDiagnostico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbResponsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frmsexo)).EndInit();
            this.Frmsexo.ResumeLayout(false);
            this.Frmsexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbHombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbMujer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombrePaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbSeleccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPlazaDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}