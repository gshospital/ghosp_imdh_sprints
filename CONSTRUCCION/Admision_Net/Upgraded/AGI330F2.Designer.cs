using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class List_censo_de_camas
	{

		#region "Upgrade Support "
		private static List_censo_de_camas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static List_censo_de_camas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new List_censo_de_camas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "ChBHosGeneral", "ChBHosDia", "FrmHospital", "cbPantalla", "cbSalir", "cbImprimir", "RbUnidad", "RbMedico", "RbFecha", "RbHistoria", "rbPlanta", "rbApellidos", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadCheckBox ChBHosGeneral;
		public Telerik.WinControls.UI.RadCheckBox ChBHosDia;
		public Telerik.WinControls.UI.RadGroupBox FrmHospital;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadButton cbSalir;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadRadioButton RbUnidad;
		public Telerik.WinControls.UI.RadRadioButton RbMedico;
		public Telerik.WinControls.UI.RadRadioButton RbFecha;
		public Telerik.WinControls.UI.RadRadioButton RbHistoria;
		public Telerik.WinControls.UI.RadRadioButton rbPlanta;
		public Telerik.WinControls.UI.RadRadioButton rbApellidos;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.FrmHospital = new Telerik.WinControls.UI.RadGroupBox();
            this.ChBHosGeneral = new Telerik.WinControls.UI.RadCheckBox();
            this.ChBHosDia = new Telerik.WinControls.UI.RadCheckBox();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.cbSalir = new Telerik.WinControls.UI.RadButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.RbUnidad = new Telerik.WinControls.UI.RadRadioButton();
            this.RbMedico = new Telerik.WinControls.UI.RadRadioButton();
            this.RbFecha = new Telerik.WinControls.UI.RadRadioButton();
            this.RbHistoria = new Telerik.WinControls.UI.RadRadioButton();
            this.rbPlanta = new Telerik.WinControls.UI.RadRadioButton();
            this.rbApellidos = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).BeginInit();
            this.FrmHospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbMedico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPlanta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbApellidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // FrmHospital
            // 
            this.FrmHospital.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmHospital.Controls.Add(this.ChBHosGeneral);
            this.FrmHospital.Controls.Add(this.ChBHosDia);
            this.FrmHospital.HeaderText = "Tipo de Ingreso";
            this.FrmHospital.Location = new System.Drawing.Point(8, 192);
            this.FrmHospital.Name = "FrmHospital";
            this.FrmHospital.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmHospital.Size = new System.Drawing.Size(311, 41);
            this.FrmHospital.TabIndex = 10;
            this.FrmHospital.Text = "Tipo de Ingreso";
            // 
            // ChBHosGeneral
            // 
            this.ChBHosGeneral.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosGeneral.Location = new System.Drawing.Point(8, 16);
            this.ChBHosGeneral.Name = "ChBHosGeneral";
            this.ChBHosGeneral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosGeneral.Size = new System.Drawing.Size(138, 18);
            this.ChBHosGeneral.TabIndex = 12;
            this.ChBHosGeneral.Text = "Hospitalizaci�n General";
            this.ChBHosGeneral.CheckStateChanged += new System.EventHandler(this.ChBHosGeneral_CheckStateChanged);
            // 
            // ChBHosDia
            // 
            this.ChBHosDia.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChBHosDia.Location = new System.Drawing.Point(176, 16);
            this.ChBHosDia.Name = "ChBHosDia";
            this.ChBHosDia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosDia.Size = new System.Drawing.Size(97, 18);
            this.ChBHosDia.TabIndex = 11;
            this.ChBHosDia.Text = "Hospital de D�a";
            this.ChBHosDia.CheckStateChanged += new System.EventHandler(this.ChBHosDia_CheckStateChanged);
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.Location = new System.Drawing.Point(146, 243);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(81, 29);
            this.cbPantalla.TabIndex = 3;
            this.cbPantalla.Text = "&Pantalla";
            this.cbPantalla.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // cbSalir
            // 
            this.cbSalir.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbSalir.Location = new System.Drawing.Point(238, 243);
            this.cbSalir.Name = "cbSalir";
            this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbSalir.Size = new System.Drawing.Size(81, 29);
            this.cbSalir.TabIndex = 2;
            this.cbSalir.Text = "&Cerrar";
            this.cbSalir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Location = new System.Drawing.Point(56, 243);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(81, 29);
            this.cbImprimir.TabIndex = 1;
            this.cbImprimir.Text = "&Impresora";
            this.cbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.RbUnidad);
            this.Frame1.Controls.Add(this.RbMedico);
            this.Frame1.Controls.Add(this.RbFecha);
            this.Frame1.Controls.Add(this.RbHistoria);
            this.Frame1.Controls.Add(this.rbPlanta);
            this.Frame1.Controls.Add(this.rbApellidos);
            this.Frame1.HeaderText = "Seleccione ordenaci�n del listado";
            this.Frame1.Location = new System.Drawing.Point(7, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(311, 175);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Seleccione ordenaci�n del listado";
            // 
            // RbUnidad
            // 
            this.RbUnidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbUnidad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RbUnidad.Location = new System.Drawing.Point(40, 146);
            this.RbUnidad.Name = "RbUnidad";
            this.RbUnidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbUnidad.Size = new System.Drawing.Size(129, 18);
            this.RbUnidad.TabIndex = 9;
            this.RbUnidad.Text = "Unidad de Enfermer�a";
            this.RbUnidad.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbUnidad_CheckedChanged);
            
            // 
            // RbMedico
            // 
            this.RbMedico.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbMedico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RbMedico.Location = new System.Drawing.Point(40, 128);
            this.RbMedico.Name = "RbMedico";
            this.RbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbMedico.Size = new System.Drawing.Size(58, 18);
            this.RbMedico.TabIndex = 8;
            this.RbMedico.Text = "M�dico";
            this.RbMedico.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbMedico_CheckedChanged);
            // 
            // RbFecha
            // 
            this.RbFecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbFecha.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RbFecha.Location = new System.Drawing.Point(40, 104);
            this.RbFecha.Name = "RbFecha";
            this.RbFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbFecha.Size = new System.Drawing.Size(105, 18);
            this.RbFecha.TabIndex = 7;
            this.RbFecha.Text = "Fecha de ingreso";
            this.RbFecha.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbFecha_CheckedChanged);
            // 
            // RbHistoria
            // 
            this.RbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbHistoria.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RbHistoria.Location = new System.Drawing.Point(40, 78);
            this.RbHistoria.Name = "RbHistoria";
            this.RbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbHistoria.Size = new System.Drawing.Size(89, 18);
            this.RbHistoria.TabIndex = 6;
            this.RbHistoria.Text = "N� de historia";
            this.RbHistoria.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbHistoria_CheckedChanged);
            // 
            // rbPlanta
            // 
            this.rbPlanta.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbPlanta.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbPlanta.Location = new System.Drawing.Point(40, 50);
            this.rbPlanta.Name = "rbPlanta";
            this.rbPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbPlanta.Size = new System.Drawing.Size(139, 18);
            this.rbPlanta.TabIndex = 5;
            this.rbPlanta.IsChecked = true;
            this.rbPlanta.IsChecked = false;
            this.rbPlanta.Text = "Planta/habitaci�n/cama";
            this.rbPlanta.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbPlanta_CheckedChanged);
            // 
            // rbApellidos
            // 
            this.rbApellidos.Cursor = System.Windows.Forms.Cursors.Default;
            //this.rbApellidos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbApellidos.Location = new System.Drawing.Point(40, 22);
            this.rbApellidos.Name = "rbApellidos";
            this.rbApellidos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbApellidos.Size = new System.Drawing.Size(66, 18);
            this.rbApellidos.TabIndex = 4;
            this.rbApellidos.Text = "Apellidos";
            this.rbApellidos.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbApellidos_CheckedChanged);
            // 
            // List_censo_de_camas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(327, 282);
            this.Controls.Add(this.FrmHospital);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.cbSalir);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "List_censo_de_camas";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Listado de Pacientes Ingresados- AGI330F2";
            this.Closed += new System.EventHandler(this.List_censo_de_camas_Closed);
            this.Load += new System.EventHandler(this.List_censo_de_camas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).EndInit();
            this.FrmHospital.ResumeLayout(false);
            this.FrmHospital.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbMedico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPlanta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbApellidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}