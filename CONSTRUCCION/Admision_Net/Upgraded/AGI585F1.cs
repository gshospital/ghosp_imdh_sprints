using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ADMISION
{
	internal partial class Seleccion_episodios
		: Telerik.WinControls.UI.RadForm
	{

		public Seleccion_episodios()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void Seleccion_episodios_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		int iAñoEpisodio = 0;
		int lNumeroEpisodio = 0;
		string stFAltaEpisodio = String.Empty;
		int iSerultiEpisodio = 0;
		string Fllegada = String.Empty;
		string stGidenpac = String.Empty;

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			Documentos_alta.DefInstance.RecogerEpisodio(iAñoEpisodio, lNumeroEpisodio, stFAltaEpisodio, iSerultiEpisodio, Fllegada);
			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			Documentos_alta.DefInstance.RecogerEpisodio(0, 0, "", 0, "");
			this.Close();
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void Seleccion_episodios_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);

			SprEpisodios.MaxRows = 0;
			SprEpisodios.Col = 7;
			SprEpisodios.SetColHidden(SprEpisodios.Col, true);
			SprEpisodios.Col = 8;
			SprEpisodios.SetColHidden(SprEpisodios.Col, true);
			SprEpisodios.Col = 9;
			SprEpisodios.SetColHidden(SprEpisodios.Col, true);
			SprEpisodios.Col = 10;
			SprEpisodios.SetColHidden(SprEpisodios.Col, true);

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
			Label1.Text = LitPersona + ":";
			Frame1.Text = "Datos del " + LitPersona;
			//con el identificador que nos llega buscamos los datos DEL PACIENTE
			//para cargar los datos en pantalla
			BuscarDatosPaciente(stGidenpac);

			CargarSprEpisodios(stGidenpac);
			return;


			Serrores.AnalizaError("Seleccion Episodio", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "SeleccionEpisodio:Form_load", null);

		}
		public void RecogerGidenpac(string Identificador)
		{
			stGidenpac = Identificador;

		}
		private void BuscarDatosPaciente(string Paciente)
		{
			string stCadena = String.Empty;

			string sql = "Select DPACIENT.dnombpac,DPACIENT.dape1pac,DPACIENT.dape2pac,HDOSSIER.GHISTORIA ";
			sql = sql + " from DPACIENT,HDOSSIER ";
			sql = sql + "where DPACIENT.GIDENPAC = '" + Paciente + "' and ";
			sql = sql + " DPACIENT.gidenpac = HDOSSIER.gidenpac ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				LB_Hist.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["ghistoria"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = stCadena + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"])) ? "" : " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim());
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = stCadena + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim());
				lb_paciente.Text = stCadena;
			}
			else
			{
				LB_Hist.Text = "";
				lb_paciente.Text = "";
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

		}
		private void CargarSprEpisodios(string Identificador)
		{
			//Carga en una spread todos los episodios
			string comando = String.Empty;
			DataSet RrSql = null;

			try
			{

				comando = "Select B.DNOMSERV,A.FLLEGADA,C.DNOMSERV,";
				comando = comando + "A.FALTPLAN,A.GANOADME,A.GNUMADME ,A.GSERVICI,A.GSERULTI ";
				comando = comando + "from AEPISADM A   ";
				comando = comando + "INNER JOIN DSERVICI B ON A.GSERVICI=B.GSERVICI ";
				comando = comando + "LEFT JOIN DSERVICI C ON A.GSERULTI=C.GSERVICI ";
				comando = comando + "where   ";
				comando = comando + " A.GIDENPAC='" + Identificador.Trim() + "'";
				comando = comando + " and A.FALTPLAN is not NULL";
				comando = comando + " order by A.FLLEGADA";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Conexion_Base_datos.RcAdmision);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
				{
					SprEpisodios.MaxRows++;
					SprEpisodios.Row = SprEpisodios.MaxRows;
					SprEpisodios.Col = 1;
					SprEpisodios.Text = Strings.StrConv(Convert.ToString(iteration_row[0]).Trim(), VbStrConv.ProperCase, 0); //DNOMSERV
					SprEpisodios.Col = 2;
                    // INCIO MAMORALES
                    //SprEpisodios.CellType = new FarPoint.Win.Spread.CellType.DateTimeCellType(); //SS_CELL_TYPE_DATE 
                    SprEpisodios.Row = SprEpisodios.MaxRows; //SS_CELL_TYPE_DATE
                    //SprEpisodios.TypeHAlign = FarPoint.Win.Spread.CellHorizontalAlignment.Center; //CENTER 
                    SprEpisodios.TypeHAlign = System.Drawing.ContentAlignment.MiddleCenter; //CENTER 
                    // FIN MAMORALES
                    //UGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeDateCentury was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    // MAMORALES COMENT SPRINT_6_6
                    //SprEpisodios.TypeDateFormat =  UpgradeHelpers.Spread.TypeDateFormatConstants.TypeDateFormatDDMMYY; //mamorales
                    //SprEpisodios.setTypeDateCentury(true);
                    SprEpisodios.TypeDateFormat = UpgradeHelpers.Spread.TypeDateFormatConstants.TypeDateFormatDDMMYY; //SS_CELL_DATE_FORMAT_DDMMYY
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeDateMin was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    // MAMORALES COMENT SPRINT_6_6
					//SprEpisodios.setTypeDateMin("01011100");
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeDateMax was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    // MAMORALES COMENT SPRINT_6_6
                    //SprEpisodios.setTypeDateMax("01013000");
                    
                    System.DateTime TempDate = DateTime.FromOADate(0);
					SprEpisodios.Text = ((DateTime.TryParse(Convert.ToString(iteration_row[1]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[1]).Trim()).Substring(0, Math.Min(10, ((DateTime.TryParse(Convert.ToString(iteration_row[1]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[1]).Trim()).Length)); //FLLEGADA
					SprEpisodios.Col = 3;
                    // INICIO MAMORALES
                    //SprEpisodios.CellType = new FarPoint.Win.Spread.CellType.DateTimeCellType(); //SS_CELL_TYPE_TIME
                    SprEpisodios.Row = SprEpisodios.MaxRows;
                    // FIN MAMORALES
                    //UPGRADE_ISSUE: (2064) FPSpread.TypeTime24HourConstants property TypeTime24HourConstants.TypeTime24Hour24HourClock was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTime24Hour was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    // MAMORALES TODO SPRINT_6_6
                    //SprEpisodios.setTypeTime24Hour(UpgradeStubs.FPSpread_TypeTime24HourConstants.getTypeTime24Hour24HourClock()); //SS_CELL_TIME_24_HOUR_CLOCK
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTimeSeconds was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    // MAMORALES TODO SPRINT_6_6
                    //SprEpisodios.setTypeTimeSeconds(true);

                    System.DateTime TempDate2 = DateTime.FromOADate(0);
					SprEpisodios.Text = (DateTime.TryParse(Convert.ToString(iteration_row[1]).Trim(), out TempDate2)) ? TempDate2.ToString("HH:mm:ss") : Convert.ToString(iteration_row[1]).Trim(); //FLLEGADA
					SprEpisodios.Col = 4; //DNOMSER
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(iteration_row[2]))
					{
						SprEpisodios.Text = "";
					}
					else
					{
						SprEpisodios.Text = Strings.StrConv(Convert.ToString(iteration_row[2]).Trim(), VbStrConv.ProperCase, 0);
					}
					SprEpisodios.Col = 5; //FALTPLAN
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(iteration_row[3]))
					{
						SprEpisodios.Text = "";
						SprEpisodios.Col = 6;
						SprEpisodios.Text = "";
					}
					else
					{
                        // INICIO MAMORALES
                        //SprEpisodios.CellType = new FarPoint.Win.Spread.CellType.DateTimeCellType(); //SS_CELL_TYPE_DATE 
                        SprEpisodios.Row = SprEpisodios.MaxRows;
                        //SprEpisodios.TypeHAlign = FarPoint.Win.Spread.CellHorizontalAlignment.Center; //CENTER
                        SprEpisodios.TypeHAlign = System.Drawing.ContentAlignment.MiddleCenter; //CENTER
                        // FIN MAMORALES
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeDateCentury was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        // MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeDateCentury(true);
                        SprEpisodios.TypeDateFormat = UpgradeHelpers.Spread.TypeDateFormatConstants.TypeDateFormatDDMMYY; //SS_CELL_DATE_FORMAT_DDMMYY
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeDateMin was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        // MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeDateMin("01011100");
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeDateMax was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        // MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeDateMax("01013000");
                        System.DateTime TempDate3 = DateTime.FromOADate(0);
						SprEpisodios.Text = ((DateTime.TryParse(Convert.ToString(iteration_row[3]).Trim(), out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[3]).Trim()).Substring(0, Math.Min(10, ((DateTime.TryParse(Convert.ToString(iteration_row[3]).Trim(), out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[3]).Trim()).Length)); //FALTPLAN
						SprEpisodios.Col = 6;
                        // Define cell as type TIME
                        // INCIO MAMORALES
                        //SprEpisodios.CellType = new FarPoint.Win.Spread.CellType.DateTimeCellType(); //SS_CELL_TYPE_TIME
                        SprEpisodios.Row = SprEpisodios.MaxRows; //SS_CELL_TYPE_TIME
                        //SprEpisodios.TypeHAlign = FarPoint.Win.Spread.CellHorizontalAlignment.Center; //CENTER  
                        SprEpisodios.TypeHAlign = System.Drawing.ContentAlignment.MiddleCenter; //CENTER
                        // FIN MAMORALES
                        //UPGRADE_ISSUE: (2064) FPSpread.TypeTime24HourConstants property TypeTime24HourConstants.TypeTime24Hour24HourClock was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTime24Hour was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeTime24Hour(UpgradeStubs.FPSpread_TypeTime24HourConstants.getTypeTime24Hour24HourClock()); //SS_CELL_TIME_24_HOUR_CLOCK
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTimeSeconds was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeTimeSeconds(false);
                        //MAMORALES TODO SPRINT_6_6
                        //(SprEpisodios.ActiveSheet.Cells[SprEpisodios.Row, SprEpisodios.Col].CellType as FarPoint.Win.Spread.CellType.NumberCellType).SpinButton = false;
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTimeSeparator was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeTimeSeparator(Strings.Asc(':'));
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTimeMin was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeTimeMin("000000");//
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprEpisodios.TypeTimeMax was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //MAMORALES TODO SPRINT_6_6
                        //SprEpisodios.setTypeTimeMax("235959");
                        System.DateTime TempDate4 = DateTime.FromOADate(0);
						SprEpisodios.Text = (DateTime.TryParse(Convert.ToString(iteration_row[3]).Trim(), out TempDate4)) ? TempDate4.ToString("HH:mm") : Convert.ToString(iteration_row[3]).Trim(); //Faltplan
					}

					SprEpisodios.Col = 7;
					SprEpisodios.Text = Conversion.Val(Convert.ToString(iteration_row["GANOADME"])).ToString();
					SprEpisodios.Col = 8;
					SprEpisodios.Text = Conversion.Val(Convert.ToString(iteration_row["GNUMADME"])).ToString();
					SprEpisodios.Col = 9;
					SprEpisodios.Text = Conversion.Val(Convert.ToString(iteration_row["GSERULTI"])).ToString();
					SprEpisodios.Col = 10;
					SprEpisodios.Text = Convert.ToDateTime(iteration_row["FALTPLAN"]).ToString("dd/MM/yyyy HH:mm:ss");
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrSql.Close();
				if (SprEpisodios.MaxRows != 0)
				{
					SprEpisodios.Row = 1;
					CargarDatos(1);
					SprEpisodios.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
				}
			}
			catch
			{
				// Call ErrorSelect
			}

		}

		private void CargarDatos(int Fila)
		{
			SprEpisodios.Row = Fila;
			SprEpisodios.Col = 2;
			Fllegada = SprEpisodios.Text.Trim();

			SprEpisodios.Col = 3;

			Fllegada = Fllegada + " " + SprEpisodios.Text.Trim();

			SprEpisodios.Col = 10;
			stFAltaEpisodio = SprEpisodios.Text.Trim();


			SprEpisodios.Col = 9;
			iSerultiEpisodio = Convert.ToInt32(Double.Parse(SprEpisodios.Text));

			SprEpisodios.Col = 7;
			iAñoEpisodio = Convert.ToInt32(Conversion.Val(SprEpisodios.Text));

			SprEpisodios.Col = 8;
			lNumeroEpisodio = Convert.ToInt32(Double.Parse(SprEpisodios.Text));


		}


		private void SprEpisodios_CellClick(object eventSender, FarPoint.Win.Spread.CellClickEventArgs eventArgs)
		{
			int Col = eventArgs.Column;
			int Row = eventArgs.Row;
			//cuando selecciono un episodio
			SprEpisodios.Row = Row;
			if (SprEpisodios.Row == 0)
			{
				return;
			}
			else
			{
				CargarDatos(SprEpisodios.Row);
			}
		}

		private void SprEpisodios_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == ((int) Keys.Down) || KeyCode == ((int) Keys.Up))
			{
				if (KeyCode == ((int) Keys.Down))
				{
                    // INCIO MAMORALES
                    //SprEpisodios.Row = SprEpisodios.ActiveSheet.ActiveRowIndex + 1;
                    SprEpisodios.Row = SprEpisodios.ActiveRowIndex + 1;
                    // FIN MAMORALES
                }
                else if (KeyCode == ((int) Keys.Up))
				{
                    // INCIO MAMORALES
                    //SprEpisodios.Row = SprEpisodios.ActiveSheet.ActiveRowIndex - 1;
                    SprEpisodios.Row = SprEpisodios.ActiveRowIndex - 1;
                    // FIN MAMORALES
                }
            }
		}

		private void SprEpisodios_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float X = (float) (eventArgs.X * 15);
			float Y = (float) (eventArgs.Y * 15);
            // INICIO MAMORALES
            //SprEpisodios.Row = SprEpisodios.ActiveSheet.ActiveRowIndex;
            SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
            // FIN MAMORALES
        }
        private void Seleccion_episodios_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}