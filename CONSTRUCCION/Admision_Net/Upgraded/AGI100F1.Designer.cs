using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class menu_ingreso
	{

		#region "Upgrade Support "
		private static menu_ingreso m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static menu_ingreso DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new menu_ingreso();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprPacientes", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "_Toolbar1_Button4", "_Toolbar1_Button5", "_Toolbar1_Button6", "_Toolbar1_Button7", "_Toolbar1_Button8", "_Toolbar1_Button9", "_Toolbar1_Button10", "_Toolbar1_Button11", "_Toolbar1_Button12", "_Toolbar1_Button13", "_Toolbar1_Button14", "_Toolbar1_Button15", "_Toolbar1_Button16", "_Toolbar1_Button17", "_Toolbar1_Button18", "_Toolbar1_Button19", "_Toolbar1_Button20", "_Toolbar1_Button21", "_Toolbar1_Button22", "_Toolbar1_Button23", "Toolbar1", "Line1", "Label1", "Line2", "Label3", "Label4", "Line3", "Frame1", "cbAceptarPrevi", "sdcPrevision", "sdcAviso", "tbHoraPrevision", "tbHoraAviso", "Label6", "Label5", "Frame3", "cbRefrescar", "Command1", "pctAutoInfor", "lblAutoInfor", "ImageList1", "Label2", "ShapeContainer3", "SprPacientes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprPacientes;         
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button2;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button4;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button5;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button6;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button7;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button8;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button9;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button10;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button11;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button12;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button13;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button14;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button15;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button16;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button17;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button18;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button19;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button20;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button21;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button22;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button23;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        public Telerik.WinControls.UI.RadCommandBar Toolbar1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
        public Telerik.WinControls.UI.RadLabel Label1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line2;
        public Telerik.WinControls.UI.RadLabel Label3;
        public Telerik.WinControls.UI.RadLabel Label4;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line3;
		public Telerik.WinControls.UI.RadPanel Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptarPrevi;
		public Telerik.WinControls.UI.RadDateTimePicker sdcPrevision;
		public Telerik.WinControls.UI.RadDateTimePicker sdcAviso;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraPrevision;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraAviso;
		public Telerik.WinControls.UI.RadLabel Label6;
        public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadButton cbRefrescar;
		public Telerik.WinControls.UI.RadButton Command1;
		public System.Windows.Forms.PictureBox pctAutoInfor;
		public Telerik.WinControls.UI.RadLabel lblAutoInfor;
		public System.Windows.Forms.ImageList ImageList1;
        public Telerik.WinControls.UI.RadLabel Label2;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer3;
		//private FarPoint.Win.Spread.SheetView SprPacientes_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu_ingreso));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();            
			this.SprPacientes = new UpgradeHelpers.Spread.FpSpread();
			this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();
			this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button2 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button4 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button5 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button6 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button7 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button8 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button9 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button10 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button11 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button12 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button13 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button14 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button15 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button16 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button17 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button18 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button19 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button20 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button21 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button22 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button23 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Frame1 = new Telerik.WinControls.UI.RadPanel();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Line2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Line3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbAceptarPrevi = new Telerik.WinControls.UI.RadButton();
			this.sdcPrevision = new Telerik.WinControls.UI.RadDateTimePicker();
			this.sdcAviso = new Telerik.WinControls.UI.RadDateTimePicker();
			this.tbHoraPrevision = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.tbHoraAviso = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.cbRefrescar = new Telerik.WinControls.UI.RadButton();
			this.Command1 = new Telerik.WinControls.UI.RadButton();
			this.pctAutoInfor = new System.Windows.Forms.PictureBox();
			this.lblAutoInfor = new Telerik.WinControls.UI.RadLabel();
			this.ImageList1 = new System.Windows.Forms.ImageList();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Toolbar1.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.SuspendLayout();
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(Menu_alta_PreviewKeyDown);
            this.KeyPreview = true;
			// 
			// ShapeContainer3
			// 
			this.ShapeContainer3.Location = new System.Drawing.Point(3, 16);
			this.ShapeContainer3.Size = new System.Drawing.Size(269, 45);
			this.ShapeContainer3.Shapes.Add(Line1);
			this.ShapeContainer3.Shapes.Add(Line2);
			this.ShapeContainer3.Shapes.Add(Line3);
            this.ShapeContainer3.Margin = new System.Windows.Forms.Padding();
            // 
            // SprPacientes
            // 
            this.SprPacientes.Location = new System.Drawing.Point(6, 54);
			this.SprPacientes.Name = "SprPacientes";
			this.SprPacientes.Size = new System.Drawing.Size(729, 364);
            this.SprPacientes.TabIndex = 2;
			this.SprPacientes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellClick);
			this.SprPacientes.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellDoubleClick);
			this.SprPacientes.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SprPacientes_KeyUp);
			this.SprPacientes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprPacientes_MouseUp);
            this.SprPacientes.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            // 
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // Toolbar1
            // 
            this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
			this.Toolbar1.ImageList = ImageList1;
			this.Toolbar1.Location = new System.Drawing.Point(0, 0);
			this.Toolbar1.Name = "Toolbar1";
			this.Toolbar1.ShowItemToolTips = false;
			this.Toolbar1.Size = new System.Drawing.Size(739, 28);
			this.Toolbar1.TabIndex = 1;
           
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
                this.radCommandBarLineElement1
            });
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.StretchHorizontally = true;
            this.radCommandBarStripElement1.StretchVertically = true;
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this._Toolbar1_Button1,
            this._Toolbar1_Button2,
            this._Toolbar1_Button3,
            this._Toolbar1_Button4,
            this._Toolbar1_Button5,
            this._Toolbar1_Button6,
            this._Toolbar1_Button7,
            this._Toolbar1_Button8,
            this._Toolbar1_Button9,
            this._Toolbar1_Button10,
            this._Toolbar1_Button11,
            this._Toolbar1_Button12,
            this._Toolbar1_Button13,
            this._Toolbar1_Button14,
            this._Toolbar1_Button15,
            this._Toolbar1_Button16,
            this._Toolbar1_Button17,
            this._Toolbar1_Button18,
            this._Toolbar1_Button19,
            this._Toolbar1_Button20,
            this._Toolbar1_Button21,
            this._Toolbar1_Button22,
            this._Toolbar1_Button23});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
			// 
			// _Toolbar1_Button1
			// 
			//this._Toolbar1_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button1.ImageIndex = 0;
			this._Toolbar1_Button1.Name = "";
			this._Toolbar1_Button1.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button1.Tag = "";
			this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button1.ToolTipText = "Ingreso del paciente";
			this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button2
			// 
			//this._Toolbar1_Button2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button2.ImageIndex = 1;
			this._Toolbar1_Button2.Name = "";
			this._Toolbar1_Button2.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button2.Tag = "";
			this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button2.ToolTipText = "Mantenimiento de datos de filiaci�n";
			this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button3
			// 
			//this._Toolbar1_Button3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button3.ImageIndex = 2;
			this._Toolbar1_Button3.Name = "";
			this._Toolbar1_Button3.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button3.Tag = "";
			this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button3.ToolTipText = "Cambio de Servicio/M�dico";
			this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button4
			// 
			//this._Toolbar1_Button4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button4.ImageIndex = 3;			
			this._Toolbar1_Button4.Name = "";
			this._Toolbar1_Button4.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button4.Tag = "";
			this._Toolbar1_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button4.ToolTipText = "Cambio de entidad";
			this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button5
			// 
			//this._Toolbar1_Button5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button5.ImageIndex = 4;			
			this._Toolbar1_Button5.Name = "";
			this._Toolbar1_Button5.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button5.Tag = "";
			this._Toolbar1_Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button5.ToolTipText = "Anulaci�n del ingreso";
			this._Toolbar1_Button5.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button6
			// 
			//this._Toolbar1_Button6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button6.ImageIndex = 5;			
			this._Toolbar1_Button6.Name = "";
			this._Toolbar1_Button6.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button6.Tag = "";
			this._Toolbar1_Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button6.ToolTipText = "Documentos del ingreso";
			this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button7
			// 
			//this._Toolbar1_Button7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button7.ImageIndex = 7;			
			this._Toolbar1_Button7.Name = "";
			this._Toolbar1_Button7.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button7.Tag = "";
			this._Toolbar1_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button7.ToolTipText = "Petici�n de n�mero de historia";
			this._Toolbar1_Button7.Visibility =  Telerik.WinControls.ElementVisibility.Collapsed;
			this._Toolbar1_Button7.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button8
			// 
			//this._Toolbar1_Button8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button8.ImageIndex = 8;			
			this._Toolbar1_Button8.Name = "";
			this._Toolbar1_Button8.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button8.Tag = "";
			this._Toolbar1_Button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button8.ToolTipText = "Cambio identificaci�n del paciente";
			this._Toolbar1_Button8.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button9
			// 
			//this._Toolbar1_Button9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button9.ImageIndex = 9;			
			this._Toolbar1_Button9.Name = "";
			this._Toolbar1_Button9.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button9.Tag = "";
			this._Toolbar1_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button9.ToolTipText = "Listados del paciente";
			this._Toolbar1_Button9.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button10
			// 
			//this._Toolbar1_Button10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button10.ImageIndex = 17;			
			this._Toolbar1_Button10.Name = "";
			this._Toolbar1_Button10.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button10.Tag = "";
			this._Toolbar1_Button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button10.ToolTipText = "Cambio de R�gimen";
			this._Toolbar1_Button10.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button11
			// 
			//this._Toolbar1_Button11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button11.ImageIndex = 19;
			
			this._Toolbar1_Button11.Name = "";
			this._Toolbar1_Button11.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button11.Tag = "";
			this._Toolbar1_Button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button11.ToolTipText = "Consumos Por Paciente";
			this._Toolbar1_Button11.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button12
			// 
			//this._Toolbar1_Button12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button12.ImageIndex = 20;			
			this._Toolbar1_Button12.Name = "";
			this._Toolbar1_Button12.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button12.Tag = "";
			this._Toolbar1_Button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button12.ToolTipText = "Ingreso en Plaza de D�a";
			this._Toolbar1_Button12.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button13
			// 
			//this._Toolbar1_Button13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button13.ImageIndex = 24;			
			this._Toolbar1_Button13.Name = "";
			this._Toolbar1_Button13.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button13.Tag = "";
			this._Toolbar1_Button13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button13.ToolTipText = "Registro de datos de resoluci�n del juzgado";
			this._Toolbar1_Button13.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button14
			// 
			//this._Toolbar1_Button14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button14.ImageIndex = 21;			
			this._Toolbar1_Button14.Name = "";
			this._Toolbar1_Button14.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button14.Tag = "";
			this._Toolbar1_Button14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button14.ToolTipText = "Control Citas Externas";
			this._Toolbar1_Button14.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button15
			// 
			//this._Toolbar1_Button15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button15.ImageIndex = 25;			
			this._Toolbar1_Button15.Name = "";
			this._Toolbar1_Button15.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button15.Tag = "";
			this._Toolbar1_Button15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button15.ToolTipText = "Cambio de Producto";
			this._Toolbar1_Button15.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button16
			// 
			//this._Toolbar1_Button16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button16.ImageIndex = 34;
			
			this._Toolbar1_Button16.Name = "";
			this._Toolbar1_Button16.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button16.Tag = "";
			this._Toolbar1_Button16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button16.ToolTipText = "Pacientes Hospitalizados";
			this._Toolbar1_Button16.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button17
			// 
			//this._Toolbar1_Button17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button17.ImageIndex = 35;			
			this._Toolbar1_Button17.Name = "";
			this._Toolbar1_Button17.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button17.Tag = "";
			this._Toolbar1_Button17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button17.ToolTipText = "Pacientes No Hospitalizados";
			this._Toolbar1_Button17.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button18
			// 
			//this._Toolbar1_Button18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button18.ImageIndex = 7;			
			this._Toolbar1_Button18.Name = "";
			this._Toolbar1_Button18.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button18.Tag = "";
			this._Toolbar1_Button18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button18.ToolTipText = "Consulta Historia Cl�nica";
			this._Toolbar1_Button18.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button19
			// 
			//this._Toolbar1_Button19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button19.ImageIndex = 30;			
			this._Toolbar1_Button19.Name = "";
			this._Toolbar1_Button19.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button19.Tag = "";
			this._Toolbar1_Button19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button19.ToolTipText = "Sondeo de Episodios";
			this._Toolbar1_Button19.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button20
			// 
			//this._Toolbar1_Button20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button20.ImageIndex = 36;			
			this._Toolbar1_Button20.Name = "";
			this._Toolbar1_Button20.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button20.Tag = "";
			this._Toolbar1_Button20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button20.ToolTipText = "Cambio de R�gimen de Alojamiento";
			this._Toolbar1_Button20.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button21
			// 
			//this._Toolbar1_Button21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button21.ImageIndex = 37;			
			this._Toolbar1_Button21.Name = "";
			this._Toolbar1_Button21.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button21.Tag = "";
			this._Toolbar1_Button21.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button21.ToolTipText = "Presupuestos y dep�sito";
			this._Toolbar1_Button21.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button22
			// 
			//this._Toolbar1_Button22.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button22.ImageIndex = 39;			
			this._Toolbar1_Button22.Name = "";
			this._Toolbar1_Button22.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button22.Tag = "";
			this._Toolbar1_Button22.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button22.ToolTipText = "IMDH-Open";
			this._Toolbar1_Button22.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button23
			// 
			//this._Toolbar1_Button23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button23.ImageIndex = 41;			
			this._Toolbar1_Button23.Name = "";
			this._Toolbar1_Button23.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button23.Tag = "";
			this._Toolbar1_Button23.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button23.ToolTipText = "Documentaci�n agregada al sobre Historia Cl�nica";
			this._Toolbar1_Button23.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// Frame1
			// 
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			//this.Frame1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Frame1.Enabled = true;			
			this.Frame1.Location = new System.Drawing.Point(344, 428);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(269, 45);
			this.Frame1.TabIndex = 13;
			this.Frame1.Visible = true;
            this.Frame1.PanelElement.PanelBorder.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;                       
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.Red;
			this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line1.BorderWidth = 5;
			this.Line1.Enabled = false;
			this.Line1.Name = "Line1";
			this.Line1.Visible = true;
			this.Line1.X1 = (int) 8;
			this.Line1.X2 = (int) 51;
			this.Line1.Y1 = (int) 41;
			this.Line1.Y2 = (int) 41;
			// 
			// Label1
			// 			
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(62, 31);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(192, 18);
			this.Label1.TabIndex = 16;
			this.Label1.Text = "Pacientes ausentes temporalmente";
			// 
			// Line2
			// 
			this.Line2.BorderColor = System.Drawing.Color.Blue;
			this.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line2.BorderWidth = 5;
			this.Line2.Enabled = false;
			this.Line2.Name = "Line2";
			this.Line2.Visible = false;
			this.Line2.X1 = (int) 8;
			this.Line2.X2 = (int) 51;
			this.Line2.Y1 = (int) 8;
			this.Line2.Y2 = (int) 8;
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(62, 0);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(192, 16);
			this.Label3.TabIndex = 15;
			this.Label3.Text = "Plazas de D�a";
			this.Label3.Visible = false;
			// 
			// Label4
			// 			
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;			
			this.Label4.Location = new System.Drawing.Point(62, 16);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(192, 16);
			this.Label4.TabIndex = 14;
			this.Label4.Text = "Plazas de D�a ausentes temporalmente";
			this.Label4.Visible = false;
			// 
			// Line3
			// 
			this.Line3.BorderColor = System.Drawing.Color.Fuchsia;
			this.Line3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line3.BorderWidth = 5;
			this.Line3.Enabled = false;
			this.Line3.Name = "Line3";
			this.Line3.Visible = false;
			this.Line3.X1 = (int) 8;
			this.Line3.X2 = (int) 51;
			this.Line3.Y1 = (int) 26;
			this.Line3.Y2 = (int) 26;
			// 
			// Frame3
			// 			
			this.Frame3.Controls.Add(this.cbAceptarPrevi);
			this.Frame3.Controls.Add(this.sdcPrevision);
			this.Frame3.Controls.Add(this.sdcAviso);
			this.Frame3.Controls.Add(this.tbHoraPrevision);
			this.Frame3.Controls.Add(this.tbHoraAviso);
			this.Frame3.Controls.Add(this.Label6);
			this.Frame3.Controls.Add(this.Label5);
			this.Frame3.Enabled = true;			
			this.Frame3.Location = new System.Drawing.Point(6, 420);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(332, 60);
			this.Frame3.TabIndex = 5;
			this.Frame3.Visible = true;
			// 
			// cbAceptarPrevi
			// 			
			this.cbAceptarPrevi.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptarPrevi.Enabled = false;			
			this.cbAceptarPrevi.Location = new System.Drawing.Point(256, 28);
			this.cbAceptarPrevi.Name = "cbAceptarPrevi";
			this.cbAceptarPrevi.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptarPrevi.Size = new System.Drawing.Size(62, 28);
			this.cbAceptarPrevi.TabIndex = 6;
			this.cbAceptarPrevi.Text = "&Aceptar";
			//this.cbAceptarPrevi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbAceptarPrevi.Click += new System.EventHandler(this.cbAceptarPrevi_Click);
			this.cbAceptarPrevi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbAceptarPrevi_KeyDown);
			this.cbAceptarPrevi.Leave += new System.EventHandler(this.cbAceptarPrevi_Leave);
			// 
			// sdcPrevision
			// 
			//this.sdcPrevision.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcPrevision.CustomFormat = "dd/MM/yyyy";
            this.sdcPrevision.NullableValue = null;
            this.sdcPrevision.SetToNullValue();
            this.sdcPrevision.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcPrevision.Location = new System.Drawing.Point(94, 10);
            this.sdcPrevision.MinDate = System.DateTime.Parse("1899/12/30");
            this.sdcPrevision.Name = "sdcPrevision";
			this.sdcPrevision.Size = new System.Drawing.Size(104, 20);
			this.sdcPrevision.TabIndex = 7;
			this.sdcPrevision.TabStop = false;
			//this.sdcPrevision.Value = System.DateTime.Today;
			this.sdcPrevision.Enter += new System.EventHandler(this.sdcPrevision_Enter);
			this.sdcPrevision.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sdcPrevision_KeyPress);
			// 
			// sdcAviso
			// 
			//this.sdcAviso.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcAviso.CustomFormat = "dd/MM/yyyy";
            this.sdcAviso.NullableValue = null;
            this.sdcAviso.SetToNullValue();
            this.sdcAviso.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcAviso.Location = new System.Drawing.Point(94, 36);
			this.sdcAviso.MinDate = System.DateTime.Parse("1899/12/30");
			this.sdcAviso.Name = "sdcAviso";
			this.sdcAviso.Size = new System.Drawing.Size(104, 20);
			this.sdcAviso.TabIndex = 8;
			this.sdcAviso.TabStop = false;
			this.sdcAviso.Enter += new System.EventHandler(this.sdcAviso_Enter);
			this.sdcAviso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sdcAviso_KeyPress);
			// 
			// tbHoraPrevision
			// 
			this.tbHoraPrevision.AllowPromptAsInput = false;
			//this.tbHoraPrevision.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.tbHoraPrevision.Location = new System.Drawing.Point(200, 10);
            this.tbHoraPrevision.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraPrevision.Mask = "##:##";
			this.tbHoraPrevision.Name = "tbHoraPrevision";
			this.tbHoraPrevision.PromptChar = ' ';
			this.tbHoraPrevision.Size = new System.Drawing.Size(42, 20);
			this.tbHoraPrevision.TabIndex = 11;
			this.tbHoraPrevision.Enter += new System.EventHandler(this.tbHoraPrevision_Enter);
			this.tbHoraPrevision.Leave += new System.EventHandler(this.tbHoraPrevision_Leave);            
            // 
            // tbHoraAviso
            // 
            this.tbHoraAviso.AllowPromptAsInput = false;
			//this.tbHoraAviso.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.tbHoraAviso.Location = new System.Drawing.Point(200, 36);
            this.tbHoraAviso.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraAviso.Mask = "##:##";
			this.tbHoraAviso.Name = "tbHoraAviso";
			this.tbHoraAviso.PromptChar = ' ';
			this.tbHoraAviso.Size = new System.Drawing.Size(42, 20);
			this.tbHoraAviso.TabIndex = 12;
			this.tbHoraAviso.Enter += new System.EventHandler(this.tbHoraAviso_Enter);
			this.tbHoraAviso.Leave += new System.EventHandler(this.tbHoraAviso_Leave);
			// 
			// Label6
			// 			
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;			
			this.Label6.Location = new System.Drawing.Point(10, 8);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			//this.Label6.Size = new System.Drawing.Size(83, 26);
            this.Label6.Size = new System.Drawing.Size(83, 52);
            this.Label6.AutoSize = true;
            this.Label6.TabIndex = 10;
			this.Label6.Text = "Fecha previsi�n de alta";
			// 
			// Label5
			// 			
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;			
			this.Label5.Location = new System.Drawing.Point(10, 38);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(73, 14);
			this.Label5.TabIndex = 9;
			this.Label5.Text = "Fecha aviso";
			// 
			// cbRefrescar
			// 			
			this.cbRefrescar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbRefrescar.Location = new System.Drawing.Point(590, 443);
			this.cbRefrescar.Name = "cbRefrescar";
			this.cbRefrescar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbRefrescar.Size = new System.Drawing.Size(73, 29);
			this.cbRefrescar.TabIndex = 4;
			this.cbRefrescar.Text = "&Refrescar";
			//this.cbRefrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.cbRefrescar.Click += new System.EventHandler(this.cbRefrescar_Click);
			// 
			// Command1
			// 
			this.Command1.Cursor = System.Windows.Forms.Cursors.Default;			
			this.Command1.Location = new System.Drawing.Point(666, 443);
			this.Command1.Name = "Command1";
			this.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Command1.Size = new System.Drawing.Size(71, 29);
			this.Command1.TabIndex = 0;
			this.Command1.Text = "C&errar";
			//this.Command1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// pctAutoInfor
			// 			
			this.pctAutoInfor.Cursor = System.Windows.Forms.Cursors.Default;
			this.pctAutoInfor.Enabled = true;
			this.pctAutoInfor.Image = (System.Drawing.Image) resources.GetObject("pctAutoInfor.Image");
			this.pctAutoInfor.Location = new System.Drawing.Point(625, 422);
			this.pctAutoInfor.Name = "pctAutoInfor";
			this.pctAutoInfor.Size = new System.Drawing.Size(20, 20);
			this.pctAutoInfor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pctAutoInfor.Visible = true;
			// 
			// lblAutoInfor
			// 			
			this.lblAutoInfor.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblAutoInfor.Font = new System.Drawing.Font("Arial", 6f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);			
			this.lblAutoInfor.Location = new System.Drawing.Point(657, 428);
			this.lblAutoInfor.Name = "lblAutoInfor";
			this.lblAutoInfor.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblAutoInfor.Size = new System.Drawing.Size(149, 11);
			this.lblAutoInfor.TabIndex = 17;
			this.lblAutoInfor.Text = "No se Autoriza informaci�n";
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("ImageList1.ImageStream");
			this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
			this.ImageList1.Images.SetKeyName(0, "");
			this.ImageList1.Images.SetKeyName(1, "");
			this.ImageList1.Images.SetKeyName(2, "");
			this.ImageList1.Images.SetKeyName(3, "");
			this.ImageList1.Images.SetKeyName(4, "");
			this.ImageList1.Images.SetKeyName(5, "");
			this.ImageList1.Images.SetKeyName(6, "");
			this.ImageList1.Images.SetKeyName(7, "");
			this.ImageList1.Images.SetKeyName(8, "");
			this.ImageList1.Images.SetKeyName(9, "");
			this.ImageList1.Images.SetKeyName(10, "");
			this.ImageList1.Images.SetKeyName(11, "");
			this.ImageList1.Images.SetKeyName(12, "");
			this.ImageList1.Images.SetKeyName(13, "");
			this.ImageList1.Images.SetKeyName(14, "");
			this.ImageList1.Images.SetKeyName(15, "");
			this.ImageList1.Images.SetKeyName(16, "");
			this.ImageList1.Images.SetKeyName(17, "");
			this.ImageList1.Images.SetKeyName(18, "");
			this.ImageList1.Images.SetKeyName(19, "");
			this.ImageList1.Images.SetKeyName(20, "");
			this.ImageList1.Images.SetKeyName(21, "");
			this.ImageList1.Images.SetKeyName(22, "");
			this.ImageList1.Images.SetKeyName(23, "");
			this.ImageList1.Images.SetKeyName(24, "");
			this.ImageList1.Images.SetKeyName(25, "");
			this.ImageList1.Images.SetKeyName(26, "");
			this.ImageList1.Images.SetKeyName(27, "");
			this.ImageList1.Images.SetKeyName(28, "");
			this.ImageList1.Images.SetKeyName(29, "");
			this.ImageList1.Images.SetKeyName(30, "");
			this.ImageList1.Images.SetKeyName(31, "");
			this.ImageList1.Images.SetKeyName(32, "");
			this.ImageList1.Images.SetKeyName(33, "");
			this.ImageList1.Images.SetKeyName(34, "");
			this.ImageList1.Images.SetKeyName(35, "");
			this.ImageList1.Images.SetKeyName(36, "");
			this.ImageList1.Images.SetKeyName(37, "");
			this.ImageList1.Images.SetKeyName(38, "");
			this.ImageList1.Images.SetKeyName(39, "");
			this.ImageList1.Images.SetKeyName(40, "");
			this.ImageList1.Images.SetKeyName(41, "");
            // 
            // Label2
            //             
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
			this.Label2.Location = new System.Drawing.Point(7, 33);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(728, 22);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "PACIENTES INGRESADOS";
            this.Label2.AutoSize = false;
            this.Label2.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            
            // 
            // menu_ingreso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
            //this.ClientSize = new System.Drawing.Size(739, 481);
            this.ClientSize = new System.Drawing.Size(658, 300);
            this.Controls.Add(this.SprPacientes);
			this.Controls.Add(this.Toolbar1);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.cbRefrescar);
			this.Controls.Add(this.Command1);
			this.Controls.Add(this.pctAutoInfor);
			this.Controls.Add(this.lblAutoInfor);
			this.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(ShapeContainer3);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			//this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Location = new System.Drawing.Point(1637, 172);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "menu_ingreso";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "GESTI�N DE INGRESOS - AGI100F1";
			this.Activated += new System.EventHandler(this.menu_ingreso_Activated);
			this.Closed += new System.EventHandler(this.menu_ingreso_Closed);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.menu_ingreso_FormClosing);
			this.Load += new System.EventHandler(this.menu_ingreso_Load);
			this.Toolbar1.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.ResumeLayout(false);
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
		}

        void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}