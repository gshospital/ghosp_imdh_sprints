using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using System.Transactions;

namespace ADMISION
{
	public partial class EPISODIOS_PACIENTE
		: RadForm
	{

		// campos para guardar origen y destino del cambio
		string sSociedadA = String.Empty;
		string sPolizaA = String.Empty;
		string sActivoPenA = String.Empty;
		string sInspeccionA = String.Empty;
		string sSociedadDe = String.Empty;
		string sPolizaDe = String.Empty;
		string sActivoPenDe = String.Empty;
		string sInspeccionDe = String.Empty;

		// episodio seleccionado en la spread
		string sA�o = String.Empty;
		string sNumero = String.Empty;

		// guarda identificador del paciente recibido
		string vstGidenpac = String.Empty;
		bool bVuelvedeFiliacion = false;
		public EPISODIOS_PACIENTE()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}






		public bool proComprobarDmovfact(DataSet RrCursor)
		{
			//funcion que comprueba si hay registros en DMOVFACT
			//si hay movimientos devuelve un true

			object tempRefParam = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
			string stSqlFact = "select * from dmovfact where" + " itiposer = 'H' and " + " ganoregi=" + Conversion.Val(sA�o).ToString() + " and " + " gnumregi=" + Conversion.Val(sNumero).ToString() + " and " + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";

			if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
			{
				string tempRefParam2 = "DMOVFACT";
				stSqlFact = stSqlFact + " UNION ALL " + Serrores.proReplace(ref stSqlFact, tempRefParam2, "DMOVFACH");
			}

			bool fencontrado = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
			DataSet rrFact = new DataSet();
			tempAdapter.Fill(rrFact);
			//si hay filas en DMOVFACT
			if (rrFact.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rrFact.Tables[0].Rows)
				{
					if (!Convert.IsDBNull(iteration_row["ffactura"]))
					{
						fencontrado = true;
					}
				}
			}
			rrFact.Close();

			return fencontrado;
		}
		public void proActualizarDmovfact(DataSet RrCursor)
		{



			string stSqlFact = "update dmovfact set gsocieda = " + Conversion.Val(sSociedadA).ToString() + " ";
			if (sPolizaA != "")
			{
				stSqlFact = stSqlFact + ",nafiliac = '" + sPolizaA.Trim() + "'";
			}
			else
			{
				stSqlFact = stSqlFact + ",nafiliac = null";
			}
			if (sInspeccionA != "")
			{
				stSqlFact = stSqlFact + ",ginspecc = '" + sInspeccionA + "'";
			}
			else
			{
				stSqlFact = stSqlFact + ",ginspecc = null";
			}

			//stSqlFact = stSqlFact & ",ffactura = null "
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + Conversion.Val(sA�o).ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + Conversion.Val(sNumero).ToString() + " and ";
			stSqlFact = stSqlFact + " gsocieda=" + Conversion.Val(sSociedadDe).ToString() + " and ";
			if (sPolizaDe.Trim() != "")
			{
				stSqlFact = stSqlFact + " nafiliac='" + sPolizaDe.Trim() + "' and ";
			}
			stSqlFact = stSqlFact + " itiporig <> 'Q' and ";
			object tempRefParam = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";
			object tempRefParam2 = RrCursor.Tables[0].Rows[0]["FFINMOVI"];
			stSqlFact = stSqlFact + " and (ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + " or ffechfin is null)";

            SqlCommand tempCommand = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
			tempCommand.ExecuteNonQuery();

		}

		public void proActualizarFarmacosDmovfact(DataSet RrCursor)
		{

			string stSqlFact = String.Empty;
			DataSet RrDmovfact = null;
			//Dim estancias     As Object
			float siFrascos = 0;
			bool NoActualizarDMOVFACT = false;
			int iFarmacia = 0;


			//Comprobamos si hay l�neas de f�rmacos de la antigua sociedad.
			//Si la nueva sociedad no factura f�macos y la antigua los ten�a, los f�rmacos que en la funci�n proActualizarDmovfact se cambiarion a la nueva, hay que borrarlos.
			//Si la nueva sociedad factura f�rmacos y la antigua no los ten�a, hay que crearlos
			//
			//Empezemos, empez�n....
			//Miramos si con la de ahora hay l�neas.
			stSqlFact = stSqlFact + "select count(*) numero from dmovfact where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + Conversion.Val(sA�o).ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + Conversion.Val(sNumero).ToString() + " and ";
			stSqlFact = stSqlFact + " gsocieda=" + Conversion.Val(sSociedadA).ToString() + " and ";
			if (sPolizaDe.Trim() != "")
			{
				stSqlFact = stSqlFact + " nafiliac='" + sPolizaA.Trim() + "' and ";
			}
			stSqlFact = stSqlFact + " itiporig <> 'Q' and ";
			object tempRefParam = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";
			object tempRefParam2 = RrCursor.Tables[0].Rows[0]["FFINMOVI"];
			stSqlFact = stSqlFact + " and (ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + " or ffechfin is null) and  gcodeven='FA'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
			DataSet rrFact = new DataSet();
			tempAdapter.Fill(rrFact);
			bool bHay = (Convert.ToDouble(rrFact.Tables[0].Rows[0]["numero"]) > 0);
			rrFact.Close();
			//Comprobamos si la nueva sociedad factura los f�rmacos.
			bool bSocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Double.Parse(sSociedadA)), sInspeccionA);
			//Si hay l�nea y la nueva sociedad no factura, se borran.
			if (bHay && !bSocFacFar)
			{
				stSqlFact = stSqlFact + "Delete dmovfact where itiposer= 'H' and ";
				stSqlFact = stSqlFact + " ganoregi=" + Conversion.Val(sA�o).ToString() + " and ";
				stSqlFact = stSqlFact + " gnumregi=" + Conversion.Val(sNumero).ToString() + " and ";
				stSqlFact = stSqlFact + " gsocieda=" + Conversion.Val(sSociedadA).ToString() + " and ";
				if (sPolizaDe.Trim() != "")
				{
					stSqlFact = stSqlFact + " nafiliac='" + sPolizaA.Trim() + "' and ";
				}
				stSqlFact = stSqlFact + " itiporig <> 'Q' and ";
				object tempRefParam3 = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
				stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam3) + " ";
				object tempRefParam4 = RrCursor.Tables[0].Rows[0]["FFINMOVI"];
				stSqlFact = stSqlFact + " and (ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam4) + " or ffechfin is null) and  gcodeven='FA' ";
				SqlCommand tempCommand = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
				tempCommand.ExecuteNonQuery();
				//quitamos la marca de facturado
				object tempRefParam5 = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
				object tempRefParam6 = RrCursor.Tables[0].Rows[0]["FFINMOVI"];
				stSqlFact = "update ETOMAALT set fpasfact = null where ganoregi = " + sA�o.Trim() + "  And gnumregi = " + sNumero.Trim() + " And " + " fpasfact is not null and  cdosimpu is not null and  ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam5) + " and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam6) + " and  itiposer='H' ";
				SqlCommand tempCommand_2 = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
				tempCommand_2.ExecuteNonQuery();


			}
			//Si no hay l�nea y la nueva sociedad factura, se insertan.
			if (!bHay && bSocFacFar)
			{
				//Si la constante global "CONTALMA" en el valfanu3 tiene valor "N" y el episodio
				//es de hospitalizacion no se graban los farmacos en DMOVFACT
				NoActualizarDMOVFACT = false;
				stSqlFact = "select valfanu3 from sconsglo where gconsglo='CONTALMA'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
				rrFact = new DataSet();
				tempAdapter_2.Fill(rrFact);
				if (rrFact.Tables[0].Rows.Count != 0)
				{
					NoActualizarDMOVFACT = ((Convert.ToString(rrFact.Tables[0].Rows[0]["valfanu3"]) + "").Trim().ToUpper() == "N");
				}
				rrFact.Close();

				stSqlFact = "select nnumeri1, nnumeri2 from sconsglo where gconsglo = 'SERVREAL'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
				rrFact = new DataSet();
				tempAdapter_3.Fill(rrFact);
				iFarmacia = Convert.ToInt32(rrFact.Tables[0].Rows[0]["nnumeri1"]);
				rrFact.Close();

				if (!NoActualizarDMOVFACT)
				{
					stSqlFact = "select distinct etomaalt.gfarmaco, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival,";
					stSqlFact = stSqlFact + " gsocieda, nafiliac, ginspecc,gservior  from etomaalt, DCODFARMVT, AMOVIFIN,AMOVIMIE where etomaalt.ganoregi=" + sA�o + " and ";
					stSqlFact = stSqlFact + " etomaalt.gnumregi=" + sNumero + "  and   etomaalt.fpasfact is null and (etomaalt.iproppac is null or etomaalt.iproppac <> 'S') and ";
					object tempRefParam7 = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
					stSqlFact = stSqlFact + " etomaalt.itiposer='H' and  cdosimpu is not null and gfarmadm is null and amovifin.FMOVIMIE =" + Serrores.FormatFechaHMS(tempRefParam7) + " and ";
					stSqlFact = stSqlFact + " amovifin.itiposer='H' and amovifin.ganoregi=" + sA�o + "  and amovifin.gnumregi=" + sNumero + " and   amovimie.itiposer=amovifin.itiposer and ";
					stSqlFact = stSqlFact + " amovimie.ganoregi=amovifin.ganoregi and amovimie.gnumregi=amovifin.gnumregi and   (amovifin.fmovimie<=etomaalt.ftomasfa and ";
					stSqlFact = stSqlFact + " (amovifin.ffinmovi>=etomaalt.ftomasfa or amovifin.ffinmovi is null)) and  ";
					stSqlFact = stSqlFact + " (amovimie.fmovimie<=etomaalt.ftomasfa and (amovimie.ffinmovi>=etomaalt.ftomasfa or amovimie.ffinmovi is null)) and   ";
					stSqlFact = stSqlFact + " etomaalt.gfarmaco = dcodfarmvt.gfarmaco group by etomaalt.gfarmaco, nequival, cporenva, gsocieda, nafiliac,ginspecc , gservior ";

					stSqlFact = stSqlFact + " Union All ";
					stSqlFact = stSqlFact + " select distinct etomaalt.gfarmadm, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival,  ";
					stSqlFact = stSqlFact + " gsocieda, nafiliac, ginspecc, gservior  from etomaalt, DCODFARMVT, AMOVIFIN, AMOVIMIE where etomaalt.ganoregi=" + sA�o + " and  ";
					stSqlFact = stSqlFact + " etomaalt.gnumregi=" + sNumero + " and   etomaalt.fpasfact is null and (etomaalt.iproppac is null or etomaalt.iproppac <> 'S') and   ";
					object tempRefParam8 = RrCursor.Tables[0].Rows[0]["FMOVIMIE"];
					stSqlFact = stSqlFact + " etomaalt.itiposer='H' and  cdosimpu is not null and gfarmadm is not null and amovifin.FMOVIMIE =" + Serrores.FormatFechaHMS(tempRefParam8) + " and   ";
					stSqlFact = stSqlFact + " amovifin.itiposer='H' and amovifin.ganoregi=" + sA�o + "  and  amovifin.gnumregi=" + sNumero + " and  amovimie.itiposer=amovifin.itiposer and ";
					stSqlFact = stSqlFact + " amovimie.ganoregi=amovifin.ganoregi and amovimie.gnumregi=amovifin.gnumregi and (amovifin.fmovimie<=etomaalt.ftomasfa and   ";
					stSqlFact = stSqlFact + " (amovifin.ffinmovi>=etomaalt.ftomasfa or amovifin.ffinmovi is null)) and   ";
					stSqlFact = stSqlFact + " (amovimie.fmovimie<=etomaalt.ftomasfa and (amovimie.ffinmovi>=etomaalt.ftomasfa or amovimie.ffinmovi is null)) and etomaalt.gfarmaco = dcodfarmvt.gfarmaco ";
					stSqlFact = stSqlFact + " group by etomaalt.gfarmadm,nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior  ORDER BY 1, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
					rrFact = new DataSet();
					tempAdapter_4.Fill(rrFact);
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter("select * from dmovfacc where 1=2", Conexion_Base_datos.RcAdmision);
					RrDmovfact = new DataSet();
					tempAdapter_5.Fill(RrDmovfact);
					foreach (DataRow iteration_row in rrFact.Tables[0].Rows)
					{
						//grabo la fila en DMOVFACT por cada f�rmaco pendiente de facturar
						// primero paso la cantidad consumida a unidades de medida
						if (!Convert.IsDBNull(iteration_row["nequival"]) && !Convert.IsDBNull(iteration_row["cporenva"]))
						{
							siFrascos = ((float) (Conversion.Val(Convert.ToString(iteration_row["cantidad"])) / Convert.ToDouble(iteration_row["nequival"]))); //* rrFact("nequival")) / rrFact("cporenva")
						}
						else
						{
							// si no est�n rellenos los campos en farmacia no le cobro porque no
							// puedo calcular la cantidad consumida
							siFrascos = 0;
						}
						if (siFrascos != 0)
						{
							// si cumple las condiciones en base al servicio responsable y tipo de facturaci�n
							// de la sociedad, se graba en DMOVFACT
							//grabar registro en dmovfact
							//Fecha de alta.
							SprEpisodios.Col = 4;
							string tempRefParam9 = siFrascos.ToString().Trim();
							string tempRefParam10 = Serrores.ObtenerSeparadorDecimalBD(Conexion_Base_datos.RcAdmision);
							object tempRefParam11 = iteration_row["fechamin"];
							object tempRefParam12 = iteration_row["fechamax"];
							object tempRefParam13 = SprEpisodios.Text;
							stSqlFact = "insert into dmovfact (gidenpac,ITPACFAC,ganoregi,gnumregi,itiposer,itiporig,ganoorig,gnumorig,ncantida,gconcfac,gsocieda,nafiliac,ginspecc,gcodeven,ffinicio,ffechfin,fsistema,gserresp,gsersoli,gserreal,fperfact) values ( " + 
							            "'" + vstGidenpac + "','I'," + sA�o + "," + sNumero + ",'H','H'," + sA�o + "," + sNumero + "," + Serrores.ConvertirDecimales(ref tempRefParam9, ref tempRefParam10, 2) + ",'" + Convert.ToString(iteration_row["gfarmaco"]) + "'," + 
							            Convert.ToString(iteration_row["gsocieda"]) + "," + ((Convert.IsDBNull(iteration_row["nafiliac"])) ? "Null" : "'" + Convert.ToString(iteration_row["nafiliac"]).Trim() + "'") + "," + ((Convert.IsDBNull(iteration_row["ginspecc"])) ? "Null" : "'" + Convert.ToString(iteration_row["ginspecc"]).Trim() + "'") + "," + 
							            "'FA'," + Serrores.FormatFechaHMS(tempRefParam11) + "," + Serrores.FormatFechaHMS(tempRefParam12) + ",getdate()," + fnBuscaSerResp(Convert.ToString(iteration_row["fechamin"]), Convert.ToInt32(Conversion.Val(sA�o)), Convert.ToInt32(Conversion.Val(sNumero))).ToString() + ", " + 
							            fnBuscaSerResp(Convert.ToString(iteration_row["fechamin"]), Convert.ToInt32(Conversion.Val(sA�o)), Convert.ToInt32(Conversion.Val(sNumero))).ToString() + ", " + iFarmacia.ToString() + "," + Serrores.FormatFechaHMS(tempRefParam13) + ")";
							SqlCommand tempCommand_3 = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
							tempCommand_3.ExecuteNonQuery();

							// pongo marca de pasado a facturaci�n de todos los acumulados
							object tempRefParam14 = SprEpisodios.Text + ":59";
							object tempRefParam15 = iteration_row["fechamin"];
							object tempRefParam16 = iteration_row["fechamax"];
							stSqlFact = "update ETOMAALT set fpasfact = " + Serrores.FormatFechaHMS(tempRefParam14) + " where " + " ganoregi = " + sA�o + " and gnumregi=" + sNumero + " and  " + " fpasfact is null and " + " cdosimpu is not null and " + " gfarmaco = '" + Convert.ToString(iteration_row["gfarmaco"]) + "' and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam15) + " and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam16) + " and " + " itiposer='H' ";
							SqlCommand tempCommand_4 = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
							tempCommand_4.ExecuteNonQuery();
						} //fin de hay frascos

					}
					RrDmovfact.Close();
					rrFact.Close();
				}
			}

		}
		public int fnBuscaSerResp(string ParaFecha, int ParaAno, int ParaNum)
		{
			//ParaFecha tiene formato "dd/mm/yyyy hh:nn:ss"
			int stServicio = 0;
			object tempRefParam = ParaFecha;
			object tempRefParam2 = ParaFecha;
			string sstemp = "select dservici.gservici from amovimie,dservici where ganoregi=" + ParaAno.ToString() + " and gnumregi=" + ParaNum.ToString() + " and fmovimie<=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " (ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam2) + " or ffinmovi is null) and amovimie.gservior=dservici.gservici";
			ParaFecha = Convert.ToString(tempRefParam2);
			ParaFecha = Convert.ToString(tempRefParam);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sstemp, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{
				stServicio = Convert.ToInt32(rrtemp.Tables[0].Rows[0]["gservici"]);
			}
			rrtemp.Close();
			return stServicio;
		}

		public void proActualizarAmovifin(DataSet rrEntidad)
		{
			// actualiza en amovifin los datos cambiados
			rrEntidad.Edit();
			rrEntidad.Tables[0].Rows[0]["gsocieda"] = sSociedadA;
			if (sPolizaA.Trim() != "")
			{
				rrEntidad.Tables[0].Rows[0]["nafiliac"] = sPolizaA.Trim();
			}
			else
			{
				rrEntidad.Tables[0].Rows[0]["nafiliac"] = " ";
			}
			if (sActivoPenA.Trim() != "")
			{
				rrEntidad.Tables[0].Rows[0]["IACTPENS"] = sActivoPenA.Trim();
			}
			else
			{
				rrEntidad.Tables[0].Rows[0]["IACTPENS"] = DBNull.Value;
			}
			if (sInspeccionA.Trim() != "")
			{
				rrEntidad.Tables[0].Rows[0]["ginspecc"] = sInspeccionA.Trim();
			}
			else
			{
				rrEntidad.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
			}
			string tempQuery = rrEntidad.Tables[0].TableName;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tempQuery, "");
			SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
			tempAdapter.Update(rrEntidad, rrEntidad.Tables[0].TableName);

		}
		public void proSociedad(int CodSOC, Control lbSoc)
		{
			//*****
			try
			{
				//***
				//********Busco la sociedad
				string SQSoci = String.Empty;
				DataSet RrSoci = null;

				SQSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter.Fill(RrSoci);

				if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == CodSOC)
				{ //SS
					lbSoc.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
					RrSoci.Close();
					return;
				}
				//*************************************
				SQSoci = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_2.Fill(RrSoci);
				if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == CodSOC)
				{ // Privado
					lbSoc.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
					RrSoci.Close();
					return;
				}
				//***************************Sociedad
				SQSoci = "SELECT DSOCIEDA,IREGALOJ FROM DSOCIEDA WHERE GSOCIEDA = " + CodSOC.ToString();
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_3.Fill(RrSoci);
				lbSoc.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["DSOCIEDA"]).Trim();
				RrSoci.Close();
				//***********************************
			}
			catch (SqlException ex)
			{
				//*********
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }


		public void proInspeccion(string codInsp, Control lbInspeccion)
		{
			try
			{

				DataSet RrSoci = null;


				SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT dinspecc FROM  DINSPECCVA WHERE ginspecc = '" + codInsp + "'", Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter.Fill(RrSoci);

				if (RrSoci.Tables[0].Rows.Count != 0)
				{
					lbInspeccion.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["dinspecc"]).Trim();
				}
				RrSoci.Close();
			}
			catch (SqlException ex)
			{
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }



		public void proInicDetalle()
		{
			// rutina para inicializar el detalle del cambio
			cbAceptar.Enabled = false;
			CbCambio.Enabled = false;
			cbCancelar.Enabled = false;
			tbEntidadde.Text = "";
			tbNafiliaDe.Text = "";
			tbEntidadA.Text = "";
			tbNafiliaA.Text = "";

			//O.Frias - 10/03/2014
			//Limpiamos los nuevos campos de Inspecci�n.
			tbInspeccDe.Text = "";
			tbInspeccA.Text = "";


		}

		public void proRellena_Grid()
		{


			// inicializo cabeceras 2� spread
			int longi = 250;
			int longi2 = 250;
			//O.Frias - 10/03/2014
			//' Agrego la columna de la descripci�n de Inspeccion
			SprEntidades.MaxCols = 7; //6
			SprEntidades.MaxRows = 0;
			//SprEntidades.SetRowHeight(0, longi2);
			SprEntidades.Row = 0;
			SprEntidades.Col = 1;
			SprEntidades.Text = "Fecha del cambio";
			SprEntidades.SetColWidth(1, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi + 100);
			SprEntidades.Col = 2;
			SprEntidades.Text = "Entidad";
			SprEntidades.SetColWidth(2, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi + 3300);
			SprEntidades.Col = 3;
			SprEntidades.Text = "N� afiliaci�n";
			SprEntidades.SetColWidth(3, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi + 1400);
			SprEntidades.Col = 4;
			SprEntidades.SetColHidden(SprEntidades.Col, true);
			SprEntidades.Col = 5;
			SprEntidades.SetColHidden(SprEntidades.Col, true);
			SprEntidades.Col = 6;
			SprEntidades.SetColHidden(SprEntidades.Col, true);

			SprEntidades.Col = 7;
			SprEntidades.Text = "Inspecci�n";
			SprEntidades.SetColWidth(7, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi + 3300);


			// inicializo cabeceras 1� spread
			longi = 500;
			longi2 = 250;

			SprEpisodios.MaxCols = 4;
			SprEpisodios.MaxRows = 0;
			//SprEpisodios.SetRowHeight(0, longi2);
			SprEpisodios.Row = 0;
			SprEpisodios.Col = 1;
			SprEpisodios.Text = "A�o";
            SprEpisodios.SetColWidth(1, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 600);
            SprEpisodios.Col = 2;
			SprEpisodios.Text = "N�mero";
			SprEpisodios.SetColWidth(2, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 600);
			SprEpisodios.Col = 3;
			SprEpisodios.Text = "Fecha ingreso";
			SprEpisodios.SetColWidth(3, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 1200);
			SprEpisodios.Col = 4;
			SprEpisodios.Text = "Fecha alta";
			SprEpisodios.SetColWidth(4, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 1200);
			string sqlEpisodios = "SELECT GNUMADME, GANOADME, FLLEGADA, FALTPLAN " + " FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + Convert.ToString(this.Tag).Trim() + "' and" + " FALTPLAN is not null " + " order by fllegada desc";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEpisodios, Conexion_Base_datos.RcAdmision);
			DataSet rREpisodios = new DataSet();
			tempAdapter.Fill(rREpisodios);
			if (rREpisodios.Tables[0].Rows.Count != 0)
			{
                SprEpisodios.MaxRows = rREpisodios.Tables[0].Rows.Count;
                rREpisodios.MoveFirst();
                longi = 1;
                cbAceptar.Enabled = false;

                foreach (DataRow iteration_row in rREpisodios.Tables[0].Rows)
                {
                    //SprEpisodios.SetRowHeight(longi, longi2);
                    SprEpisodios.Row = longi;
                    SprEpisodios.Col = 1;
                    SprEpisodios.Text = Convert.ToString(iteration_row["ganoadme"]);
                    SprEpisodios.Col = 2;
                    SprEpisodios.Text = Convert.ToString(iteration_row["gnumadme"]);
                    SprEpisodios.Col = 3;
                    SprEpisodios.Text = Convert.ToDateTime(iteration_row["fllegada"]).ToString("dd/MM/yyyy HH:mm");
                    SprEpisodios.Col = 4;
                    SprEpisodios.Text = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy HH:mm");
                    longi++;
                }

                SprEpisodios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode; // single selection
                SprEpisodios.setSelModeIndex(0);
                // inicializo detalle para que al cambiar de episodio no se quede
                proInicDetalle();
            }
			else
			{
                //si no hay ning�n episodio, mando mensaje y descargo la ventana

                short tempRefParam = 1520;
                string[] tempRefParam2 = new string[] { "episodios", LbPaciente.Text.Trim() };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                this.Close();
            }
			rREpisodios.Close();
		}
		public void proRellena_Grid2()
		{


			SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
			SprEpisodios.Col = 1;
			sA�o = SprEpisodios.Text;
			SprEpisodios.Col = 2;
			sNumero = SprEpisodios.Text;
			int longi = 500;
			int longi2 = 250;
			//O.Frias - 10/03/2014
			// Ajusto la select
			string sqlEnti = "SELECT GIDENPAC,FMOVIMIE,GNUMADME,GANOADME,DSOCIEDA,AMOVIFIN.NAFILIAC,AMOVIFIN.GSOCIEDA, " + "GINSPECC,IACTPENS " + " FROM AEPISADM, AMOVIFIN,DSOCIEDA " + " WHERE " + " AEPISADM.GANOADME = " + Conversion.Val(sA�o).ToString() + " AND " + " AEPISADM.GNUMADME = " + Conversion.Val(sNumero).ToString() + " AND AMOVIFIN.ITIPOSER = 'H' AND " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " FALTPLAN is not null AND DSOCIEDA.GSOCIEDA=AMOVIFIN.GSOCIEDA" + " order by fmovimie";


			sqlEnti = "SELECT GIDENPAC,FMOVIMIE,GNUMADME,GANOADME,DSOCIEDA,AMOVIFIN.NAFILIAC,AMOVIFIN.GSOCIEDA, AMOVIFIN.GINSPECC,IACTPENS, DINSPECC.dinspecc " + 
			          "From AEPISADM " + 
			          "INNER JOIN AMOVIFIN ON AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND  AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND AMOVIFIN.ITIPOSER = 'H' " + 
			          "INNER JOIN DSOCIEDA ON DSOCIEDA.GSOCIEDA=AMOVIFIN.GSOCIEDA " + 
			          "LEFT JOIN DINSPECC ON AMOVIFIN.GINSPECC = DINSPECC.GINSPECC " + 
			          "WHERE AEPISADM.GANOADME = " + Conversion.Val(sA�o).ToString() + " AND  AEPISADM.GNUMADME = " + Conversion.Val(sNumero).ToString() + " AND AEPISADM.FALTPLAN IS NOT NULL";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEnti, Conexion_Base_datos.RcAdmision);
			DataSet rrEnti = new DataSet();
			tempAdapter.Fill(rrEnti);
			if (rrEnti.Tables[0].Rows.Count != 0)
			{

				SprEntidades.MaxRows = rrEnti.Tables[0].Rows.Count;
				vstGidenpac = Convert.ToString(rrEnti.Tables[0].Rows[0]["gidenpac"]);
				longi = 1;
				foreach (DataRow iteration_row in rrEnti.Tables[0].Rows)
				{
					//SprEntidades.SetRowHeight(longi, longi2);
					SprEntidades.Row = longi;
					SprEntidades.Col = 1;
					SprEntidades.Text = Convert.ToString(iteration_row["FMOVIMIE"]); //Format(rrEnti("FMOVIMIE"), "dd/mm/yyyy hh:nn")
					SprEntidades.Col = 2;
					SprEntidades.Text = Convert.ToString(iteration_row["DSOCIEDA"]);
					SprEntidades.Col = 3;
					if (Convert.IsDBNull(iteration_row["NAFILIAC"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["NAFILIAC"]);
					}
					SprEntidades.Col = 4;
					SprEntidades.Text = Convert.ToString(iteration_row["gsocieda"]);
					SprEntidades.Col = 5;
					if (Convert.IsDBNull(iteration_row["gINSPECC"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["gINSPECC"]);
					}
					SprEntidades.Col = 6;
					if (Convert.IsDBNull(iteration_row["IACTPENS"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["IACTPENS"]);
					}
					//' O.Frias - 10/03/2014
					//' Descripci�n de la inspeccion
					SprEntidades.Col = 7;
					if (Convert.IsDBNull(iteration_row["dinspecc"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["dinspecc"]);
					}
					longi++;
				}
				SprEntidades.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode; // single selection
				SprEntidades.setSelModeIndex(0);

				tbEntidadA.Text = "";
				tbNafiliaA.Text = "";
			}
			rrEnti.Close();
		}
		public void Recoger_datos(string Nombre2, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Idpaciente, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen = "", string Inspec = "")
		{

			bVuelvedeFiliacion = true;
			// vuelta de filiaci�n
			cbCancelar.Enabled = true;
			if (Idpaciente != "")
			{

				// si no ha cancelado filiaci�n
				cbAceptar.Enabled = true;

				// guardo los valores recibidos aunque no hayan variado
				sSociedadA = CodSOC;
				sPolizaA = Asegurado.Trim();
				sActivoPenA = IndActPen.Trim();
				sInspeccionA = Inspec.Trim();
				tbNafiliaA.Text = Asegurado.Trim();
				proSociedad(Convert.ToInt32(Double.Parse(CodSOC)), tbEntidadA);
				// O.Frias 10/03/2014
				// Agregamos inspecci�n
				proInspeccion(sInspeccionA, tbInspeccA);
			}
			else
			{

				// si ha cancelado filiaci�n
				cbAceptar.Enabled = false;
			}

		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
            string fFecha_ultimo = String.Empty;
			string stSqEntidad = String.Empty;
			DataSet rrEntidad = null;

			//****************************
			try
			{
				//****************************
				this.Cursor = Cursors.WaitCursor;

				//en fecha_ultimo cargo fmovimie de la fila seleccionada
				SprEntidades.Row = SprEntidades.ActiveRowIndex;
				SprEntidades.Col = 1;
				fFecha_ultimo = DateTime.Parse(SprEntidades.Text).ToString("dd/MM/yyyy HH:mm:ss");

				//recuperamos de AMOVIFIN el registro seleccionado
				// bas�ndonos en el episodio seleccionado en la spread 1
				// y en la fecha del movimiento
				object tempRefParam = fFecha_ultimo;
				stSqEntidad = "SELECT * " + " FROM AMOVIFIN WHERE " + " AMOVIFIN.ITIPOSER = 'H' AND " + " AMOVIFIN.GANOREGI = " + Conversion.Val(sA�o).ToString() + " AND " + " AMOVIFIN.GNUMREGI = " + Conversion.Val(sNumero).ToString() + " AND " + " FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam) + "";
				fFecha_ultimo = Convert.ToString(tempRefParam);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
				rrEntidad = new DataSet();
				tempAdapter.Fill(rrEntidad);
				if (rrEntidad.Tables[0].Rows.Count != 0)
				{
                    using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                    {
                        if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                            Conexion_Base_datos.RcAdmision.Close();
                        Conexion_Base_datos.RcAdmision.Open();

                        if (proComprobarDmovfact(rrEntidad))
                        {
                            //******modificacion 15/07/1999
                            //si hay movimientos en dmovfact FACTURADOS se manda un mensaje
                            //no se permite modificar nada
                            short tempRefParam2 = 1940;
                            string[] tempRefParam3 = new string[] { "modificar", "ya ha sido facturado" };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam2, Conexion_Base_datos.RcAdmision, tempRefParam3));
                            rrEntidad.Close();
                        }
                        else
                        {
                            //***inicio antes 15/07/1999
                            // si hay movimientos sin facturar
                            // se actualiza dmovfact
                            proActualizarDmovfact(rrEntidad);
                            //       End if
                            //***fin antes 17/07/1999
                            if (sInspeccionA.Trim() == "")
                            {
                                Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, sSociedadA, DateTime.Now, false);
                            }
                            else
                            {
                                Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, sSociedadA, DateTime.Now, false, sInspeccionA.Trim());
                            }
                            //se actualizan los datos en AMOVIFIN
                            proActualizarAmovifin(rrEntidad);
                            //se modifican las consultas asociadas a prestaciones realizadas en ese intervalo
                            proCambiar_Consultas(rrEntidad);
                            //Modificar los f�rmacos.
                            proActualizarFarmacosDmovfact(rrEntidad);

                            scope.Complete();
                            rrEntidad.Close();
                        } //a�adido 15/07/1999
                    }
				}
				this.Cursor = Cursors.Default;
				proRellena_Grid2();
				proInicDetalle();
			}
			catch (SqlException ex)
			{
                //**********************
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void CbCambio_Click(Object eventSender, EventArgs eventArgs)
		{
			//llamo a la dll de filiaci�n
			object vncFiliacion = null;

			try
			{
				//******

				this.Cursor = Cursors.WaitCursor;
                // ralopezn_TODO_X_4 25/04/2016
				//vncFiliacion = new filiacionDLL.Filiacion();
				////UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//vncFiliacion.Load(vncFiliacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, vstGidenpac);
				//vncFiliacion = null;
				//Me.Show
				this.Cursor = Cursors.Default;
			}
			catch (SqlException ex)
			{
				//*********
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }

        }



		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			proInicDetalle();
		}

		private void cbSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void EPISODIOS_PACIENTE_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
				Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);

				string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
				LitPersona = LitPersona.ToLower() + "s";

				bVuelvedeFiliacion = false;
				EPISODIOS_PACIENTE.DefInstance.Text = "Cambio de entidad de " + LitPersona + " dados de alta";
				if (!bVuelvedeFiliacion)
				{
					// relleno el grid de episodios
					proRellena_Grid();
				}

			}
		}

		private void EPISODIOS_PACIENTE_Load(Object eventSender, EventArgs eventArgs)
		{

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label28.Text = LitPersona;

			// recibe en el tag el gidenpac del paciente seleccionado
		}

        private void SprEntidades_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			int actual_fila = Row;
			int actual_colu = Col;
			SprEntidades.Row = Row;

			cbAceptar.Enabled = false;
			CbCambio.Enabled = true;
			cbCancelar.Enabled = true;

			// inicializo la entidad/p�liza destino
			tbEntidadA.Text = "";
			tbNafiliaA.Text = "";

			if (Row != 0)
			{
				// si hay una fila seleccionada

				// activo/desactivo botones
				cbAceptar.Enabled = false;
				CbCambio.Enabled = true;
				cbCancelar.Enabled = true;

				// guardo valores actuales de la fila seleccionada
				// muestro la entidad/p�liza origen
				SprEntidades.Col = 2;
				tbEntidadde.Text = SprEntidades.Text;
				SprEntidades.Col = 3;
				tbNafiliaDe.Text = SprEntidades.Text;
				sPolizaDe = SprEntidades.Text;
				SprEntidades.Col = 4;
				sSociedadDe = SprEntidades.Text;
				SprEntidades.Col = 5;
				sInspeccionDe = SprEntidades.Text;
				SprEntidades.Col = 6;
				sActivoPenDe = SprEntidades.Text;

				SprEntidades.Col = 7;
				tbInspeccDe.Text = SprEntidades.Text;

			}
			else
			{
				// si no hay una fila seleccionada
				// inicializo detalle
				proInicDetalle();
			}
		}

        private void SprEpisodios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			if (Row != 0)
			{
				// si ha seleccionado una fila
				// relleno la spread de movimientos de financiaci�n
				proRellena_Grid2();
				proInicDetalle();
			}
		}

		private void proCambiar_Consultas(DataSet RrCursor)
		{

			//funcion que busca las consultas relacionadas a las prestaciones y les cambia la sociedad
			object tempRefParam = RrCursor.Tables[0].Rows[0]["fmovimie"];
			string tstSql = "select * from cconsult C inner join epresalt  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and e.gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and e.itiposer='h' " + " and e.frealiza >=" + Serrores.FormatFechaHMS(tempRefParam);

			if (!Convert.IsDBNull(RrCursor.Tables[0].Rows[0]["FFINMOVI"]))
			{
				object tempRefParam2 = RrCursor.Tables[0].Rows[0]["FFINMOVI"];
				tstSql = tstSql + " and e.frealiza <=" + Serrores.FormatFechaHMS(tempRefParam2);
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, Conexion_Base_datos.RcAdmision);
			DataSet tRr1 = new DataSet();
			tempAdapter.Fill(tRr1);

			foreach (DataRow iteration_row in tRr1.Tables[0].Rows)
			{
				tRr1.Edit();
				iteration_row["gsocieda"] = sSociedadA.Trim();
				if (sPolizaA.Trim() == "")
				{
					iteration_row["nafiliac"] = DBNull.Value;
				}
				else
				{
					iteration_row["nafiliac"] = sPolizaA.Trim();
				}
				if (sInspeccionA.Trim() == "")
				{
					iteration_row["ginspecc"] = DBNull.Value;
				}
				else
				{
					iteration_row["ginspecc"] = sInspeccionA.Trim();
				}
				string tempQuery = tRr1.Tables[0].TableName;
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
				tempAdapter_2.Update(tRr1, tRr1.Tables[0].TableName);
			}
			tRr1.Close();

			//Tambi�n se cambian los consumos.
			object tempRefParam3 = RrCursor.Tables[0].Rows[0]["fmovimie"];
			tstSql = "select f.* from cconsult C inner join epresalt  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela inner join fconspac f on c.ganoregi=f.ganoregi  and " + " c.gnumregi=f.gnumregi and f.itiposer='C' where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and e.gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and e.itiposer='h' " + " and e.frealiza >=" + Serrores.FormatFechaHMS(tempRefParam3);
			if (!Convert.IsDBNull(RrCursor.Tables[0].Rows[0]["FFINMOVI"]))
			{
				object tempRefParam4 = RrCursor.Tables[0].Rows[0]["FFINMOVI"];
				tstSql = tstSql + " and e.frealiza <=" + Serrores.FormatFechaHMS(tempRefParam4);
			}

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstSql, Conexion_Base_datos.RcAdmision);
			tRr1 = new DataSet();
			tempAdapter_3.Fill(tRr1);

			foreach (DataRow iteration_row_2 in tRr1.Tables[0].Rows)
			{
				tRr1.Edit();
				iteration_row_2["gsocieda"] = sSociedadA.Trim();
				if (sInspeccionA.Trim() == "")
				{
					iteration_row_2["ginspecc"] = DBNull.Value;
				}
				else
				{
					iteration_row_2["ginspecc"] = sInspeccionA.Trim();
				}
				string tempQuery_2 = tRr1.Tables[0].TableName;
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery_2, "");
				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
				tempAdapter_4.Update(tRr1, tRr1.Tables[0].TableName);
			}
			tRr1.Close();
		}
		private void EPISODIOS_PACIENTE_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}