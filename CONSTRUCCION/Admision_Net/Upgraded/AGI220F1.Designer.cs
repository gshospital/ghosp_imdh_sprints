using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Documentos_alta
	{

		#region "Upgrade Support "
		private static Documentos_alta m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Documentos_alta DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Documentos_alta();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbImprimir", "cbCancelar", "ChbAltaInc", "ChbFallecInc", "ChbHaceconstar", "chbVolanteSociedad", "ChBHojaReferencia", "chbFactSombDocu", "chbIncapacidad", "chbHojaClinEsta", "chbAltaMedica", "chbFacturaSombra", "ChbJusAcompa", "ChbJustificante", "lbPaciente", "Label1", "Frame3", "ChbAltavolun", "chbFactura", "ChbVolante", "ChbMedico", "ChbAlta", "Chboficio", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadCheckBox ChbAltaInc;
		public Telerik.WinControls.UI.RadCheckBox ChbFallecInc;
		public Telerik.WinControls.UI.RadCheckBox ChbHaceconstar;
		public Telerik.WinControls.UI.RadCheckBox chbVolanteSociedad;
		public Telerik.WinControls.UI.RadCheckBox ChBHojaReferencia;
		public Telerik.WinControls.UI.RadCheckBox chbFactSombDocu;
		public Telerik.WinControls.UI.RadCheckBox chbIncapacidad;
		public Telerik.WinControls.UI.RadCheckBox chbHojaClinEsta;
		public Telerik.WinControls.UI.RadCheckBox chbAltaMedica;
		public Telerik.WinControls.UI.RadCheckBox chbFacturaSombra;
		public Telerik.WinControls.UI.RadCheckBox ChbJusAcompa;
		public Telerik.WinControls.UI.RadCheckBox ChbJustificante;
		public Telerik.WinControls.UI.RadLabel lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadCheckBox ChbAltavolun;
		public Telerik.WinControls.UI.RadCheckBox chbFactura;
		public Telerik.WinControls.UI.RadCheckBox ChbVolante;
		public Telerik.WinControls.UI.RadCheckBox ChbMedico;
		public Telerik.WinControls.UI.RadCheckBox ChbAlta;
        //public AxMSForms.AxCheckBox Chboficio;

        public Telerik.WinControls.UI.RadCheckBox Chboficio;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ChbAltaInc = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbFallecInc = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbHaceconstar = new Telerik.WinControls.UI.RadCheckBox();
            this.chbVolanteSociedad = new Telerik.WinControls.UI.RadCheckBox();
            this.ChBHojaReferencia = new Telerik.WinControls.UI.RadCheckBox();
            this.chbFactSombDocu = new Telerik.WinControls.UI.RadCheckBox();
            this.chbIncapacidad = new Telerik.WinControls.UI.RadCheckBox();
            this.chbHojaClinEsta = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAltaMedica = new Telerik.WinControls.UI.RadCheckBox();
            this.chbFacturaSombra = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbJusAcompa = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbJustificante = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbPaciente = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.ChbAltavolun = new Telerik.WinControls.UI.RadCheckBox();
            this.chbFactura = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbVolante = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbMedico = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbAlta = new Telerik.WinControls.UI.RadCheckBox();
            this.Chboficio = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAltaInc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbFallecInc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbHaceconstar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbVolanteSociedad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHojaReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFactSombDocu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIncapacidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbHojaClinEsta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAltaMedica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFacturaSombra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJusAcompa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJustificante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAltavolun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFactura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbVolante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbMedico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chboficio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Enabled = false;
            this.cbImprimir.Location = new System.Drawing.Point(190, 434);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(81, 31);
            this.cbImprimir.TabIndex = 15;
            this.cbImprimir.Text = "&Imprimir";
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(278, 434);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 31);
            this.cbCancelar.TabIndex = 16;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.ChbAltaInc);
            this.Frame1.Controls.Add(this.ChbFallecInc);
            this.Frame1.Controls.Add(this.ChbHaceconstar);
            this.Frame1.Controls.Add(this.chbVolanteSociedad);
            this.Frame1.Controls.Add(this.ChBHojaReferencia);
            this.Frame1.Controls.Add(this.chbFactSombDocu);
            this.Frame1.Controls.Add(this.chbIncapacidad);
            this.Frame1.Controls.Add(this.chbHojaClinEsta);
            this.Frame1.Controls.Add(this.chbAltaMedica);
            this.Frame1.Controls.Add(this.chbFacturaSombra);
            this.Frame1.Controls.Add(this.ChbJusAcompa);
            this.Frame1.Controls.Add(this.ChbJustificante);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.ChbAltavolun);
            this.Frame1.Controls.Add(this.chbFactura);
            this.Frame1.Controls.Add(this.ChbVolante);
            this.Frame1.Controls.Add(this.ChbMedico);
            this.Frame1.Controls.Add(this.ChbAlta);
            this.Frame1.Controls.Add(this.Chboficio);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(2, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(359, 429);
            this.Frame1.TabIndex = 0;
            // 
            // ChbAltaInc
            // 
            this.ChbAltaInc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbAltaInc.Location = new System.Drawing.Point(14, 382);
            this.ChbAltaInc.Name = "ChbAltaInc";
            this.ChbAltaInc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbAltaInc.Size = new System.Drawing.Size(176, 18);
            this.ChbAltaInc.TabIndex = 23;
            this.ChbAltaInc.Text = "Notificación al Juzgado del alta";
            this.ChbAltaInc.Visible = false;
            this.ChbAltaInc.CheckStateChanged += new System.EventHandler(this.ChbAltaInc_CheckStateChanged);
            // 
            // ChbFallecInc
            // 
            this.ChbFallecInc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbFallecInc.Location = new System.Drawing.Point(14, 401);
            this.ChbFallecInc.Name = "ChbFallecInc";
            this.ChbFallecInc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbFallecInc.Size = new System.Drawing.Size(222, 18);
            this.ChbFallecInc.TabIndex = 22;
            this.ChbFallecInc.Text = "Notificación al Juzgado del fallecimiento";
            this.ChbFallecInc.Visible = false;
            this.ChbFallecInc.CheckStateChanged += new System.EventHandler(this.ChbFallecInc_CheckStateChanged);
            // 
            // ChbHaceconstar
            // 
            this.ChbHaceconstar.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbHaceconstar.Location = new System.Drawing.Point(14, 364);
            this.ChbHaceconstar.Name = "ChbHaceconstar";
            this.ChbHaceconstar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbHaceconstar.Size = new System.Drawing.Size(129, 18);
            this.ChbHaceconstar.TabIndex = 21;
            this.ChbHaceconstar.Text = "Compromiso de Pago";
            this.ChbHaceconstar.CheckStateChanged += new System.EventHandler(this.ChbHaceconstar_CheckStateChanged);
            // 
            // chbVolanteSociedad
            // 
            this.chbVolanteSociedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbVolanteSociedad.Location = new System.Drawing.Point(14, 344);
            this.chbVolanteSociedad.Name = "chbVolanteSociedad";
            this.chbVolanteSociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbVolanteSociedad.Size = new System.Drawing.Size(107, 18);
            this.chbVolanteSociedad.TabIndex = 20;
            this.chbVolanteSociedad.Text = "Volante Sociedad";
            this.chbVolanteSociedad.CheckStateChanged += new System.EventHandler(this.chbVolanteSociedad_CheckStateChanged);
            // 
            // ChBHojaReferencia
            // 
            this.ChBHojaReferencia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHojaReferencia.Location = new System.Drawing.Point(14, 240);
            this.ChBHojaReferencia.Name = "ChBHojaReferencia";
            this.ChBHojaReferencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHojaReferencia.Size = new System.Drawing.Size(114, 18);
            this.ChBHojaReferencia.TabIndex = 10;
            this.ChBHojaReferencia.Text = "Hoja de Referencia";
            this.ChBHojaReferencia.CheckStateChanged += new System.EventHandler(this.ChBHojaReferencia_CheckStateChanged);
            // 
            // chbFactSombDocu
            // 
            this.chbFactSombDocu.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFactSombDocu.Location = new System.Drawing.Point(14, 322);
            this.chbFactSombDocu.Name = "chbFactSombDocu";
            this.chbFactSombDocu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFactSombDocu.Size = new System.Drawing.Size(99, 18);
            this.chbFactSombDocu.TabIndex = 14;
            this.chbFactSombDocu.Text = "Factura Sombra";
            this.chbFactSombDocu.Visible = false;
            this.chbFactSombDocu.CheckStateChanged += new System.EventHandler(this.chbFactSombDocu_CheckStateChanged);
            // 
            // chbIncapacidad
            // 
            this.chbIncapacidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbIncapacidad.Location = new System.Drawing.Point(14, 302);
            this.chbIncapacidad.Name = "chbIncapacidad";
            this.chbIncapacidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbIncapacidad.Size = new System.Drawing.Size(117, 18);
            this.chbIncapacidad.TabIndex = 13;
            this.chbIncapacidad.Text = "Incapacidad laboral";
            this.chbIncapacidad.Visible = false;
            this.chbIncapacidad.CheckStateChanged += new System.EventHandler(this.chbIncapacidad_CheckStateChanged);
            // 
            // chbHojaClinEsta
            // 
            this.chbHojaClinEsta.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbHojaClinEsta.Location = new System.Drawing.Point(14, 282);
            this.chbHojaClinEsta.Name = "chbHojaClinEsta";
            this.chbHojaClinEsta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbHojaClinEsta.Size = new System.Drawing.Size(136, 18);
            this.chbHojaClinEsta.TabIndex = 12;
            this.chbHojaClinEsta.Text = "Hoja Clínico-Estadística";
            this.chbHojaClinEsta.Visible = false;
            this.chbHojaClinEsta.CheckStateChanged += new System.EventHandler(this.chbHojaClinEsta_CheckStateChanged);
            // 
            // chbAltaMedica
            // 
            this.chbAltaMedica.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbAltaMedica.Location = new System.Drawing.Point(14, 220);
            this.chbAltaMedica.Name = "chbAltaMedica";
            this.chbAltaMedica.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbAltaMedica.Size = new System.Drawing.Size(79, 18);
            this.chbAltaMedica.TabIndex = 9;
            this.chbAltaMedica.Text = "Alta médica";
            this.chbAltaMedica.CheckStateChanged += new System.EventHandler(this.chbAltaMedica_CheckStateChanged);
            // 
            // chbFacturaSombra
            // 
            this.chbFacturaSombra.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFacturaSombra.Location = new System.Drawing.Point(14, 200);
            this.chbFacturaSombra.Name = "chbFacturaSombra";
            this.chbFacturaSombra.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFacturaSombra.Size = new System.Drawing.Size(99, 18);
            this.chbFacturaSombra.TabIndex = 8;
            this.chbFacturaSombra.Text = "Factura Sombra";
            this.chbFacturaSombra.CheckStateChanged += new System.EventHandler(this.chbFacturaSombra_CheckStateChanged);
            // 
            // ChbJusAcompa
            // 
            this.ChbJusAcompa.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbJusAcompa.Location = new System.Drawing.Point(14, 180);
            this.ChbJusAcompa.Name = "ChbJusAcompa";
            this.ChbJusAcompa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbJusAcompa.Size = new System.Drawing.Size(165, 18);
            this.ChbJusAcompa.TabIndex = 7;
            this.ChbJusAcompa.Text = "Justificante del acompañante";
            this.ChbJusAcompa.CheckStateChanged += new System.EventHandler(this.ChbJusAcompa_CheckStateChanged);
            // 
            // ChbJustificante
            // 
            this.ChbJustificante.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbJustificante.Location = new System.Drawing.Point(14, 160);
            this.ChbJustificante.Name = "ChbJustificante";
            this.ChbJustificante.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbJustificante.Size = new System.Drawing.Size(142, 18);
            this.ChbJustificante.TabIndex = 6;
            this.ChbJustificante.Text = "Justificante de asistencia";
            this.ChbJustificante.CheckStateChanged += new System.EventHandler(this.ChbJustificante_CheckStateChanged);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.lbPaciente);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.HeaderText = "Paciente";
            this.Frame3.Location = new System.Drawing.Point(8, 11);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(342, 45);
            this.Frame3.TabIndex = 17;
            this.Frame3.Text = "Paciente";
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(60, 20);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(2, 2);
            this.lbPaciente.TabIndex = 19;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(9, 20);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(48, 18);
            this.Label1.TabIndex = 18;
            this.Label1.Text = "Nombre";
            // 
            // ChbAltavolun
            // 
            this.ChbAltavolun.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbAltavolun.Location = new System.Drawing.Point(14, 80);
            this.ChbAltavolun.Name = "ChbAltavolun";
            this.ChbAltavolun.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbAltavolun.Size = new System.Drawing.Size(149, 18);
            this.ChbAltavolun.TabIndex = 2;
            this.ChbAltavolun.Text = "Informe de alta voluntaria";
            this.ChbAltavolun.CheckStateChanged += new System.EventHandler(this.ChbAltavolun_CheckStateChanged);
            // 
            // chbFactura
            // 
            this.chbFactura.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFactura.Location = new System.Drawing.Point(14, 100);
            this.chbFactura.Name = "chbFactura";
            this.chbFactura.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFactura.Size = new System.Drawing.Size(57, 18);
            this.chbFactura.TabIndex = 3;
            this.chbFactura.Text = "Factura";
            this.chbFactura.CheckStateChanged += new System.EventHandler(this.chbFactura_CheckStateChanged);
            // 
            // ChbVolante
            // 
            this.ChbVolante.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbVolante.Location = new System.Drawing.Point(14, 120);
            this.ChbVolante.Name = "ChbVolante";
            this.ChbVolante.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbVolante.Size = new System.Drawing.Size(135, 18);
            this.ChbVolante.TabIndex = 4;
            this.ChbVolante.Text = "Volante de ambulancia";
            this.ChbVolante.CheckStateChanged += new System.EventHandler(this.ChbVolante_CheckStateChanged);
            // 
            // ChbMedico
            // 
            this.ChbMedico.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbMedico.Location = new System.Drawing.Point(14, 140);
            this.ChbMedico.Name = "ChbMedico";
            this.ChbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbMedico.Size = new System.Drawing.Size(132, 18);
            this.ChbMedico.TabIndex = 5;
            this.ChbMedico.Text = "Informe médico al alta";
            this.ChbMedico.CheckStateChanged += new System.EventHandler(this.ChbMedico_CheckStateChanged);
            // 
            // ChbAlta
            // 
            this.ChbAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbAlta.Location = new System.Drawing.Point(14, 60);
            this.ChbAlta.Name = "ChbAlta";
            this.ChbAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbAlta.Size = new System.Drawing.Size(96, 18);
            this.ChbAlta.TabIndex = 1;
            this.ChbAlta.Text = "Informe de alta";
            this.ChbAlta.CheckStateChanged += new System.EventHandler(this.ChbAlta_CheckStateChanged);
            // 
            // Chboficio
            // 
            this.Chboficio.Location = new System.Drawing.Point(14, 262);
            this.Chboficio.Name = "Chboficio";
            this.Chboficio.Size = new System.Drawing.Size(50, 18);
            this.Chboficio.TabIndex = 11;
            this.Chboficio.Text = "Oficio";
            this.Chboficio.Visible = false;
            // 
            // Documentos_alta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 472);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Documentos_alta";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Documentación al alta -AGI220F1";
            this.Activated += new System.EventHandler(this.Documentos_alta_Activated);
            this.Closed += new System.EventHandler(this.Documentos_alta_Closed);
            this.Load += new System.EventHandler(this.Documentos_alta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAltaInc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbFallecInc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbHaceconstar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbVolanteSociedad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHojaReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFactSombDocu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIncapacidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbHojaClinEsta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAltaMedica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFacturaSombra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJusAcompa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbJustificante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAltavolun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFactura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbVolante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbMedico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chboficio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			//this.Show();
		}
		#endregion
	}
}