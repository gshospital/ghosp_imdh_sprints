using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI.Localization;
using CrystalWrapper;

namespace ADMISION
{
	public partial class menu_admision
		: Telerik.WinControls.UI.RadForm
	{
         
        Listado_Genereal_Camas.mcList_Camas cc = null;
		string vstServer2 = String.Empty;
		string vstDriver2 = String.Empty;
		string vstBasedat = String.Empty;
		mantecamas.manteclass nvisualizacion = null;
		string vstIdPaciente = String.Empty;
		string vstPaciente = String.Empty;
		string stApellido1 = String.Empty;
		string stApellido2 = String.Empty;
		string stNombre2 = String.Empty;
		string vstAsegurado = String.Empty;
		string vstCodigosoc = String.Empty;
		string vstHistoria = String.Empty;
		string vstDNI = String.Empty;
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		//I.S.M. 5-May-2000. A�adido para distinguir desde d�nde se llama
		public bool bOficial = false;

		int vstAnoadme = 0;
		int vstNumadme = 0;
		string Formato_Fecha_SQL = String.Empty;
		string Separador_Fecha_SQL = String.Empty;
		string Separador_Hora_SQL = String.Empty;
		public menu_admision()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				m_vb6FormDefInstance = this;
			}
			//This call is required by the Windows Form Designer.
            //Localizaci�n para controles telerik
            RadMessageLocalizationProvider.CurrentProvider = new ClassRadMessageLocalizationProvider();
            RadGridLocalizationProvider.CurrentProvider = new ClassRadGridLocalizationProvider();
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}

        public void Recoger_datos(string Nombre2, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Idpaciente, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen = "", string Inspec = "")
		{
			vstIdPaciente = Idpaciente;
			vstPaciente = Apellido1.Trim() + " " + Apellido2.Trim() + "," + Nombre2.Trim();
			stApellido1 = Apellido1.Trim();
			stApellido2 = Apellido2.Trim();
			stNombre2 = Nombre2.Trim();
			vstAsegurado = Asegurado.Trim();
			vstCodigosoc = CodSOC.Trim();
			vstDNI = NIF.Trim();
		}

		private void menu_admision_Activated(Object eventSender, EventArgs eventArgs)
		{
            if (ActivateHelper.myActiveForm != null &&
                ActivateHelper.myActiveForm != eventSender &&
                (Information.IsNothing(Convert.ToString(ActivateHelper.myActiveForm)) ||
                !ActivateHelper.myActiveForm.IsMdiChild ||
                ActivateHelper.myActiveForm.Parent.Parent != eventSender))
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                Conexion_Base_datos.vstActivado_Ingreso = false;
                this.Activate();
            }
            MainMenu1.Enabled = true;
        }
        public void cambia_crystal()
        {
            //DGMORENOG_TODO_6_6_PROPIEDADES NO MIGRADAS EN EL WRAPER DE CRYSTAL
            //crystalgeneral.PrinterDriver = ""; //CrystalGeneral2.PrinterDriver
            //crystalgeneral.PrinterName = ""; //CrystalGeneral2.PrinterName
            //crystalgeneral.PrinterPort = ""; //CrystalGeneral2.PrinterPort
            Conexion_Base_datos.LISTADO_CRYSTAL = crystalgeneral;
        }

        private void menu_admision_Load(Object eventSender, EventArgs eventArgs)
		{
			int Resul = 0, Resul2 = 0;
			FixedLengthString stDatabase = new FixedLengthString(50);
			bool vstError = false;
			FixedLengthString stDirectorio = new FixedLengthString(100);

            //*****
            //On Error GoTo Etiqueta
            //******************USUARIO Y CONTRASE�A

            //App.HelpFile = App.Path & "\AYUDA_ADMISION.HLP"
            try
            {
                Conexion_Base_datos.VstCodUsua = mbAcceso.ValidaPrincipal();
                if (Conexion_Base_datos.VstCodUsua == "No Activa")
                {
                    Environment.Exit(0);
                }
                if (Conexion_Base_datos.VstCodUsua == "")
                {
                    RadMessageBox.Show("3000:" + "\r" + "El M�dulo de Admisi�n" + "\r" + "s�lo se puede ejecutar desde Principal", "Gesti�n Asistencial y Departamental", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    Environment.Exit(0);
                }


                    
                //If Not AplicarControlAcceso(VstCodUsua, "A", Me, "AGI000F1", astrControles(), astrEventos()) Then
                //    mnu_adm01_Click 6
                //End If


                //**RECOGE CONFIGURACION DE LA IMPRESORA
                Conexion_Base_datos.Recoge_Impresora();

                //oscar
                //vstServer2 = fnQueryRegistro("SERVIDOR")
                //vstDriver2 = fnQueryRegistro("DRIVER")
                //vstBasedat = fnQueryRegistro("BASEDATOS")
                vstServer2 = mbAcceso.gstServerActual;
                vstDriver2 = mbAcceso.gstDriver;
                vstBasedat = mbAcceso.gstBasedatosActual;
                //----------

                //''***********************
                //'
                //tstConexion = "UID=" & gUsuario & ";PWD=" & gContrase�a & ";" _
                //'    & "DATABASE=" & vstBasedat & ";" _
                //'    & "SERVER=" & vstServer2 & ";" _
                //'    & "DRIVER=" & vstDriver2 & ";" _
                //'    & " DSN='';"
                //Set ReAdmision = rdoEngine.rdoEnvironments(0)
                //Set RcAdmision = ReAdmision.OpenConnection( _
                //'    dsname:="", _
                //'    prompt:=rdDriverNoPrompt, _
                //'    Connect:=tstConexion)

                // establezco conexion con el servidor
                Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();

                //oscar
                // obtengo el usuario de windows
                //Dim stBuffer As String
                //Dim lSize As Long
                //stBuffer = Space$(256)
                //lSize = Len(stBuffer)
                //Call GetUserName(stBuffer, lSize)
                //If lSize > 0 Then
                //    VstUsuario_NT = Left$(stBuffer, lSize)
                //Else
                //    VstUsuario_NT = ""
                //End If
                Conexion_Base_datos.VstUsuario_NT = Serrores.obtener_usuario();

                //Instancia.Conexion RcAdmision, "A", VstUsuario_NT, vstError, gUsuario, gContrase�a
                string tempRefParam = "A";
                string tempRefParam2 = "0";
                string tempRefParam3 = "0";
                string tempRefParam4 = "ADMISION";
                instancia.Conexion(ref Conexion_Base_datos.RcAdmision, tempRefParam, Conexion_Base_datos.VstUsuario_NT, ref vstError, ref Conexion_Base_datos.gUsuario, ref Conexion_Base_datos.gContrase�a, mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD, tempRefParam2, tempRefParam3, tempRefParam4, Conexion_Base_datos.VstCodUsua);
                //------

                if (!vstError)
                {
                    RadMessageBox.Show("Error de conexi�n con la Base de Datos", Application.ProductName);
                    Environment.Exit(0);
                }

                if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "ICOMPMOD ", "VALFANU1") != "N")
                {
                    Process[] processes = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
                    if (processes.Length > 1 && Process.GetCurrentProcess().StartTime != processes[0].StartTime)
                    {
                        RadMessageBox.Show("2030:" + "\r" + "Ya existe una Copia Activa " + "\r" + " de la Aplicaci�n", "Gesti�n Asistencial y Departamental", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        Environment.Exit(0);
                    }
                }

                Conexion_Base_datos.BDtempo = instancia.NonMaquina() + "Admision.mdb";
                //***********nombre base de datos temporal de listados
                Conexion_Base_datos.StBaseTemporal = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo;
                //***********si existe borrarla ******************'
                prokill(Conexion_Base_datos.StBaseTemporal);

                Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion_Base_datos.RcAdmision);
                Conexion_Base_datos.gEnfermeria = Conexion_Base_datos.Obtener_Uni_Enfe(Conexion_Base_datos.VstCodUsua);

                //''''''Obtener_Multilenguaje RcAdmision
                //''''''ObtenerMesesSemanas RcAdmision

                //****Path y confirmaci�n de la existencia de
                //una conexi�n DSN para los controles Crystal de la aplicaci�n para los RPT

                //VstCrystal = "GHOSP_" & CStr(ConexionTS) & "_" & instancia.NonMaquina
                //stNombreDsnACCESS = "GHOSP_ACCESS_" & CStr(ConexionTS) & "_" & instancia.NonMaquina
                Serrores.CrearCadenaDSN(ref Conexion_Base_datos.VstCrystal, ref Conexion_Base_datos.stNombreDsnACCESS);


                //VstCrystal = gstUsuarioBD
                instancia = null;
                //PathString = App.Path
                Conexion_Base_datos.PathString = Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ELEMENTOSCOMUNES";
                string tstConexion = "Description=" + "SQL Server en  SERVICURIA" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=" + vstServer2 + "" + "\r" + "Database=" + vstBasedat + "";
                int tiDriver = (vstDriver2.IndexOf('{') + 1);
                vstDriver2 = vstDriver2.Substring(tiDriver);
                tiDriver = (vstDriver2.IndexOf('}') + 1);
                vstDriver2 = vstDriver2.Substring(0, Math.Min(tiDriver - 1, vstDriver2.Length));
                //UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.VstCrystal, vstDriver2, true, tstConexion);
                
                //**************LLAMADA ERRORES

                Serrores.oClass = new Mensajes.ClassMensajes();
                Conexion_Base_datos.Cuadro_Diag_imprePrint = CommDiag_imprePrint;
                Conexion_Base_datos.LISTADO_CRYSTAL = null;
                Conexion_Base_datos.LISTADO_CRYSTAL = crystalgeneral;

                // indra - ralopezn - 01/04/2016
                // Se crea singleton para CDAyuda.
                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_ADMISION);


                Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(Conexion_Base_datos.RcAdmision);
                Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();
                //FORMATO DE FILIACION
                SqlDataAdapter tempAdapter = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'FORMFILI'", Conexion_Base_datos.RcAdmision);
                DataSet Rrglobal = new DataSet();
                tempAdapter.Fill(Rrglobal);
                Conexion_Base_datos.stFORMFILI = Convert.ToString(Rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim();
                Rrglobal.Close();

                //'   If stFORMFILI = "V" Then
                //'        mnu_adm01601.Item(0).Caption = "Informe General de Casos Hospitalizados por Periodo"
                //'        mnu_adm01601.Item(4).Caption = "&Movimiento del Centro"
                //'   End If
                Serrores.AjustarRelojCliente(Conexion_Base_datos.RcAdmision);

                string StSqlTemp = "select valfanu1 from sconsglo where gconsglo= 'idenpers' ";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSqlTemp, Conexion_Base_datos.RcAdmision);
                DataSet rrtemp = new DataSet();
                tempAdapter_2.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();
                    Conexion_Base_datos.DniCedulaMay = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper(); //en mayusculas
                }
                rrtemp.Close();

                StSqlTemp = "select valfanu1 from sconsglo where gconsglo= 'INOSEGSO' ";
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSqlTemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter_3.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    //mnu_adm01601.Item(1).Enabled = False
                    //mnu_adm01501.Item(6).Enabled = False
                    //oscar
                    if (Convert.ToString(rrtemp.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
                    {
                        mnu_adm01601[1].Enabled = false;
                        mnu_adm01501[6].Enabled = false;
                    }
                    //----
                }
                rrtemp.Close();

                //oscar 11/12/2003
                Conexion_Base_datos.GestionHD();
                if (Conexion_Base_datos.bGestionHD)
                {
                    mnu_adm011[17].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    mnu_adm011[18].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    mnu_adm011[19].Visibility = Telerik.WinControls.ElementVisibility.Visible;

                }
                else
                {
                    mnu_adm011[17].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    mnu_adm011[18].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    mnu_adm011[19].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
                //-------

                //oscar 30/03/04
                //Se visualiza la opcion d emenu "cambio de producto" si la constante gloval GESPRODUCT
                //esta establecida a "S"
                Conexion_Base_datos.GestionProducto();
                mnu_adm011[15].Visibility = (Conexion_Base_datos.bGestionProducto)
                    ? Telerik.WinControls.ElementVisibility.Visible
                    : Telerik.WinControls.ElementVisibility.Collapsed;
                //-----

                //Operaciones de pedidos Jes�s 23/09/2011
                StSqlTemp = "select valfanu1,valfanu2,valfanu1i1,valfanu2i1  from sconsglo where gconsglo= 'OPERALMA' ";
                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSqlTemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter_4.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    if ((Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]) + "").Trim() == "")
                    {
                        mnu_adm01[5].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    }
                    else
                    {
                        mnu_adm01[5].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                        mnu_pedidos[0].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                        if ((Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]) + "").Trim() != "")
                        {
                            mnu_pedidos[0].Text = (Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]) + "").Trim();
                        }
                    }
                    if ((Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2"]) + "").Trim() == "")
                    {
                        mnu_pedidos[1].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    }
                    else
                    {
                        mnu_adm01[5].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                        mnu_pedidos[1].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                        if ((Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2i1"]) + "").Trim() != "")
                        {
                            mnu_pedidos[1].Text = (Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2i1"]) + "").Trim();
                        }
                    }
                }
                else
                {
                    mnu_adm01[5].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
                rrtemp.Close();

                //Cambiar paciente seg�n corresponda en las opciones de menu
                string HospitalizadoIngresado = String.Empty;


                string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(Conexion_Base_datos.RcAdmision);
                if (HospitalOCentro.Trim().ToUpper() == "N")
                {
                    HospitalizadoIngresado = "Ingresados";
                }
                else
                {
                    HospitalizadoIngresado = "Hospitalizados";
                }

                string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();

                mnu_adm011[0].Text = "&" + LitPersona + "s" + " Ingresados"; //"Pacientes Ingresados"
                mnu_adm011[1].Text = "&Nuevo " + LitPersona; //"&Nuevo Paciente"
                mnu_adm011[3].Text = "&Mantenimiento datos " + LitPersona.ToLower(); //"&Mantenimiento datos paciente"
                mnu_adm0112[2].Text = "Cam&biar Datos Identificaci�n " + LitPersona; //"Cam&biar Datos Identificaci�n Paciente"
                mnu_adm011[8].Text = "Listados de " + LitPersona + "s"; //"Listados de Pacien&tes"
                mnu_adm011[12].Text = "C&onsumos por " + LitPersona.ToLower(); //"C&onsumos por paciente"
                mnu_adm012[6].Text = "C&onsumos por " + LitPersona.ToLower(); //"C&onsumos por paciente"
                mnu_adm01[3].Text = "&Listados y Localizaci�n de " + LitPersona.ToLower() + "s"; //"&Listados y Localizaci�n de pacientes"
                mnu_adm01501[0].Text = "Listado general de " + LitPersona + "s" + " &Ingresados"; //"Listado general de Pacientes &Ingresados"
                mnu_adm01601[1].Text = "Mo&vimiento de " + LitPersona + "s" + " para Seguridad Social"; //"Mo&vimiento de Pacientes para Seguridad Social"
                mnu_adm014[4].Text = "&Cambio de Servicio/M�dico de " + LitPersona + "s"; //"&Cambio de Servicio/M�dico de pacientes"
                mnu_adm014[5].Text = "Locali&zaci�n de " + LitPersona + "s"; //"Locali&zaci�n de Pacientes"
                mnu_adm01401[0].Text = LitPersona + "s " + "&" + HospitalizadoIngresado; //"Pacientes &Hospitalizados"
                mnu_adm01401[1].Text = LitPersona + "s " + "&No " + HospitalizadoIngresado; //"Pacientes &No Hospitalizados"
                mnu_adm014[12].Text = LitPersona + "s" + " ausentes"; //"Pacie&ntes ausentes"
                mnu_adm014[13].Text = "Listado de Interconsultas de " + LitPersona + "s"; //"Listado de Interconsultas de Pacientes"
                mnu_adm014[14].Text = LitPersona + "s" + " Referidos a Otros Centros"; //"Pacientes Referidos a Otros Centros"
                mnu_adm014[15].Text = LitPersona + "s" + " Remitidos de Otros Centros"; //"Pacientes Remitidos de Otros Centros"
                mnu_adm017[4].Text = "&Imprimir " + LitPersona.ToLower() + "s" + " con procesos abiertos"; //"&Imprimir pacientes con procesos abiertos"
                mnu_adm018[2].Text = "&Gesti�n " + LitPersona + "s"; //"&Gesti�n Pacientes"
                mnu_adm01801[1].Text = "Listado de " + LitPersona + "s" + " en lista de espera"; //"Listado de pacientes en lista de espera"
                mnu_adm01501[8].Text = "Relaci�n de " + LitPersona + "s" + " con Citas Externas";


                //********************************
                _Toolbar1_Button1.ToolTipText = "Ingreso de " + LitPersona.ToLower() + "s";
                _Toolbar1_Button2.ToolTipText = "Alta de " + LitPersona.ToLower() + "s";

                //OSCAR C FEB 2006
                mnu_adm018[6].Visibility = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "RULEQDIA", "VALFANU1") == "S"
                    ? Telerik.WinControls.ElementVisibility.Visible
                    : Telerik.WinControls.ElementVisibility.Collapsed;
                //---------

                mnu_adm01[10].Visibility = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FSIGESTI", "VALFANU1") == "S"
                    ? Telerik.WinControls.ElementVisibility.Visible
                    : Telerik.WinControls.ElementVisibility.Collapsed;




                if (!mbAcceso.AplicarControlAcceso(Conexion_Base_datos.VstCodUsua, "A", this, "AGI000F1", ref Conexion_Base_datos.astrControles, ref Conexion_Base_datos.astrEventos))
                {
                    mnu_adm01[11].PerformClick();
                }
                //************** O.Frias 19/07/2007 **************
                //Cierre de m�dulos
                mbProcesos.proActivaTimer(Conexion_Base_datos.RcAdmision, tmrPrincipal);

                return;
            }
            catch(Exception ex)
            {
                //************

                //GEstionErrorBaseDatos 40002, RcAdmision
                RadMessageBox.Show("Error en apertura conexi�n", Application.ProductName);
                Environment.Exit(0);
            }
		}

		private bool isInitializingComponent;
		private void menu_admision_Resize(Object eventSender, EventArgs eventArgs)
		{
            if (isInitializingComponent)
            {
                return;
            }
            // ralopezn_TODO_X_3 26/03/2016
            //ConfiguracionRegional.clsConfigRegional ObjConfigurarEquipo = null;
            //try
            //{
            //	ObjConfigurarEquipo = new ConfiguracionRegional.clsConfigRegional();
            //}
            //catch
            //{
            //}
            //if (WindowState == FormWindowState.Maximized)
            //{
            //	try
            //	{
            //		ObjConfigurarEquipo.ConfigurarEquipo();
            //	}
            //	catch
            //	{
            //	}
            //}

        }

        private void menu_admision_Closed(Object eventSender, EventArgs eventArgs)
		{

            //Conexion_Base_datos.LISTADO_CRYSTAL = null;
            //Conexion_Base_datos.LISTADO_LIMPIA = null;
            CDAyuda.Instance.Dispose();
            //RcAdmision.Close
            //ReAdmision.Close
            MemoryHelper.ReleaseMemory();
        }

        public void mnu_adm001_Click(Object eventSender, EventArgs eventArgs)
		{
            int Index = Array.IndexOf(this.mnu_adm001, eventSender);
            try
            {
                filiacionDLL.Filiacion objFiliacion = null;

                switch (Index)
                {
                    case 0:
                        menu_reservas.DefInstance.Show();

                        break;
                    case 1:  // carga la ventana de reserva nueva 
                        menu_reservas.DefInstance.proLLamarFiliacion();

                        break;
                    case 2:  // carga la ventana de modificaci�n de reserva 
                        menu_reservas.DefInstance.SprPacientes_CellDoubleClick(menu_reservas.DefInstance.SprPacientes, null);

                        break;
                    case 3:
                        // ANULACION DE LA RESERVA 
                        menu_reservas.DefInstance.ProAnularReserva();

                        break;
                    case 4:  //Carga la ventana de impresi�n de documentos 
                        menu_reservas.DefInstance.proMemorizarReserva();
                        docu_reservas.DefInstance.ShowDialog();
                        menu_reservas.DefInstance.HabilitarBotones();
                        // DOCUMENTOS DE LAS RESERVAS 

                        break;
                    case 5:  //LLama  a la DLL de mantenimiento de filiacion 
                        this.Cursor = Cursors.WaitCursor;
                        
                        objFiliacion = new filiacionDLL.Filiacion();                        
                        objFiliacion.Load(objFiliacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, menu_reservas.DefInstance.ObjReservaAdmision.IdentificadorPaciente);
                        //Set ObjFiliacion = CreateObject("EFiliacionPaciente.Efiliacion") 
                        //ObjFiliacion.RecibirParametros Me, "ADMISION", RcAdmision, menu_reservas.ObjReservaAdmision.IdentificadorPaciente 
                        objFiliacion = null;
                        this.Show();
                        this.Cursor = Cursors.Hand;
                        break;
                }
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Menu_Admision:mnu_adm001_click", ex);
            }
        }

		public void mnu_adm01_Click(Object eventSender, EventArgs eventArgs)
		{
            try
            {
                int Index = Array.IndexOf(this.mnu_adm01, eventSender);
                switch (Index)
                {
                    case 7:  //lista de espera 

                        break;
                    case 11:
                        this.Close();
                        prokill(Conexion_Base_datos.StBaseTemporal);
                        Environment.Exit(0);

                        break;
                }

            } catch (Exception ex)
            {
                Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "mnu_adm01_Click", null);
            }
		}




		public void mnu_adm0102_Click(Object eventSender, EventArgs eventArgs)
		{

            int Index = Array.IndexOf(this.mnu_adm0102, eventSender);
            //Hay que registrar el DSN de Access
            string tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + "";
            //UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion);

            // ralopezn_TODO_X_3 26/03/2016
            //object oEstadistica = new EstadisticasComunesAdm.MCEstComunesAdm();
            ////UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control Cuadro_Diag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx
            ////UPGRADE_TODO: (1067) Member ProEstadistica is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //oEstadistica.ProEstadistica(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.Cuadro_Diag_impre, Conexion_Base_datos.VstCodUsua, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo, Conexion_Base_datos.PathString, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Index);
            //oEstadistica = null;
            this.Show();
        }

        public void mnu_adm0108_Click(Object eventSender, EventArgs eventArgs)
		{
            
            int Index = Array.IndexOf(this.mnu_adm0108, eventSender);
            string destino = String.Empty;

            // ralopezn_TODO_X_3 26/03/2016
            DocumentosPaciente.MCDenpaciente objDocPac = new DocumentosPaciente.MCDenpaciente();

            if (Index == StringsHelper.ToDoubleSafe("1"))
            {
                destino = "REGISTRO";
            }
            else
            {
                destino = "CONSULTA";
            }

            //UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control Cuadro_Diag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx
            //UPGRADE_TODO: (1067) Member Llamada is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
           objDocPac.Llamada(this, objDocPac, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua.ToUpper(), "Admision", destino, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.Cuadro_Diag_imprePrint, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo, Conexion_Base_datos.PathString + "\\rpt\\", Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a,Type.Missing);
           objDocPac = null;
        }

        public void mnu_adm011_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm011, eventSender);
			// validacion de si hay algun paciente seleccionado
			if (Index == 2 || Index == 4 || Index == 5 || Index == 7 || Index == 8 || Index == 9 || Index == 10 || Index == 11 || Index == 12)
			{
                if (!(menu_ingreso.DefInstance.SprPacientes.ActiveRowIndex > 0))
                {
                    short tempRefParam = 1270;
                    string[] tempRefParam2 = new string[] { "un paciente" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    return;
                }
            }
			switch(Index)
			{
				case 0 : 
					menu_ingreso.DefInstance.Show(); 
					 
					break;
				case 1 : 
					menu_ingreso.DefInstance.proMnu_ing01(0); 
					 
					break;
				case 2 :  //Carga la ventana de impresi�n de documentos 
					menu_ingreso.DefInstance.proMenu_doc(); 
					break;
				case 3 : 
					//Mantenimiento datos 0112 
					break;
				case 4 : 
					menu_ingreso.DefInstance.proMnu_ing01(3); 
					break;
				case 5 : 
					menu_ingreso.DefInstance.proMnu_ing01(4); 
					break;
				case 6 : 
					menu_ingreso.DefInstance.proMnu_ing01(5); 
					break;
				case 7 : 
					menu_ingreso.DefInstance.proMnu_ing01(7); 
					break;
				case 8 : 
					menu_ingreso.DefInstance.proMnu_ing01(10); 
					break;
				case 9 : 
					menu_ingreso.DefInstance.proMnu_ing01(11); 
					break;
				case 10 : 
					menu_ingreso.DefInstance.proMnu_ing01(12); 
					break;
				case 11 : 
					menu_ingreso.DefInstance.proMnu_ing01(13); 
					break;
				case 12 : 
					menu_ingreso.DefInstance.proMnu_ing01(14); 
					//oscar 
					break;
				case 13 :  //Resolucion del juzgado 18/03/2004 
					menu_ingreso.DefInstance.proMnu_ing01(16); 
					break;
				case 14 :  //Control de citas externas '29/03/04 
					if (FormularioActivo("menu_ingreso"))
					{
						menu_ingreso.DefInstance.proMnu_ing01(17);
					}
					else
					{
						LlamaCitasExternas();
					} 
					 
					break;
				case 15 :  //Cambio de producto 30/03/04 
					menu_ingreso.DefInstance.proMnu_ing01(18); 
					break;
				case 16 :  //Cambio de Regimen de Alojamiento 27/03/06 
					menu_ingreso.DefInstance.proMnu_ing01(19); 
					break;
				case 18 :  //Ingreso en plaza de dia 
					menu_ingreso.DefInstance.proMnu_ing01(15); 
					//----- 
					//Oscar C 11/01/2009 
					break;
				case 19 : 
					menu_ingreso.DefInstance.proMnu_ing01(20); 
					//------------------ 
					break;
				case 20 : 
					menu_ingreso.DefInstance.proMnu_ing01(21); 
					break;
				case 21 : 
					menu_ingreso.DefInstance.proMnu_ing01(23); 
					break;
			}

		}






		public void mnu_adm0112_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm0112, eventSender);
            if (!(menu_ingreso.DefInstance.SprPacientes.ActiveRowIndex > 0))
            {
                short tempRefParam = 1270;
                string[] tempRefParam2 = new string[] { "un paciente" };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                return;
            }
            switch (Index)
			{
				case 0 : 
					//Datos filiacion 
					menu_ingreso.DefInstance.proMant_Datos(0); 
					break;
				case 1 : 
					//datos ingreso 
					menu_ingreso.DefInstance.proMant_Datos(1); 
					break;
				case 2 : 
					menu_ingreso.DefInstance.proMnu_ing01(9); 
					break;
				case 3 : 
					break;
			}
		}

		public void mnu_adm012_Click(Object eventSender, EventArgs eventArgs)
		{
            int Index = Array.IndexOf(this.mnu_adm012, eventSender);
            //Dim dfechallegada As String
            ConsumosPac.clsConsPac ClaseConsumos = null;
            int vstA�oAdmi = 0;
            int vstnumAdmi = 0;
            //Dim CDAyuda As Object
            string UniEnf = String.Empty;
            filiacionDLL.Filiacion ClassFiliacion = null;
            //Definiciones
            filiacionDLL.Filiacion vmcFiliacion = null; // para llamar a b�squeda pacientes

            MovimientosPac.MCMovimientosPac mcListadosPac = null;
            dynamic mcSelEpisodios = null;


            string stSql = String.Empty;
            DataSet rrtemp = null;
            filiacionDLL.Filiacion Anfiliacion = null;
            Anulacion.Alta anulacion_alta = null;
            CambioServicio.clsCambioServicio oCambioServ = null;
            string StFechaAlta = String.Empty;
            string sql = String.Empty;
            DataSet RrSql = null;
            System.DateTime dfechallegada = DateTime.FromOADate(0);
            switch (Index)
            {
                case 0:
                    menu_alta.DefInstance.Show();
                    break;
                case 1:
                    menu_alta.DefInstance.proMnu_ing11(0);
                    break;
                case 2:
                    // menu_alta.proMnu_ing11 2 
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.proMnu_ing11(2);
                    }
                    else
                    {
                        //llamo a la dll de filiaci�n para buscar al paciente                        
                        vmcFiliacion = new filiacionDLL.Filiacion();                        
                        vmcFiliacion.Load(vmcFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                        vmcFiliacion = null;
                        if (vstIdPaciente != "")
                        {
                            // si ha seleccionado un paciente en filiaci�n
                            this.Cursor = Cursors.WaitCursor;
                            menu_alta.DefInstance.Tag = stNombre2.Trim() + "$" + stApellido1.Trim() + "$" + stApellido2.Trim() + "$" + vstAsegurado.Trim() + "$" + vstCodigosoc.Trim() + "$" + vstHistoria.Trim() + "$" + vstIdPaciente.Trim() + "$" + vstDNI.Trim();

                            if (Convert.ToString(menu_alta.DefInstance.Tag) == "$$$$$$$")
                            {
                                // Ha pulsado cancelar en la DLL de FILIACION
                                this.Cursor = Cursors.Default;
                                return;
                            }
                            else
                            {
                                Conexion_Base_datos.Aepisodio = 0;
                                Conexion_Base_datos.Nepisodio = 0;
                                // Ahora COMPROBAMOS QUE EL PACIENTE HA SIDO DADO DE ALTA EN EL HOSPITAL ******
                                stSql = " SELECT * " + " FROM AEPISADM " + " WHERE AEPISADM.gidenpac=' " + vstIdPaciente + "' and " + " AEPISADM.faltplan IS NULL ";
                                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                                rrtemp = new DataSet();
                                tempAdapter.Fill(rrtemp);
                                if (rrtemp.Tables[0].Rows.Count != 0)
                                {
                                    RadMessageBox.Show("El paciente seleccionado no ha sido dado de alta o no ha pasado por admision", Application.ProductName);
                                    stMensaje1 = "obtener Documentos al Alta";
                                    stMensaje2 = "Ingresado o no se ha registrado su paso por Admisi�n";
                                    short tempRefParam = 1310;
                                    string[] tempRefParam2 = new string[] { stMensaje1, stMensaje2 };
                                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                                    rrtemp.Close();
                                    return;
                                }
                                else
                                {
                                    
                                    Documentos_alta.DefInstance.Tag = Conexion_Base_datos.Rojo.ToString();
                                    Documentos_alta tempLoadForm2 = Documentos_alta.DefInstance;
                                    this.Cursor = Cursors.Default;
                                    Documentos_alta.DefInstance.Show();
                                }
                            }
                            // mando a la nueva ventana el nombre del paciente
                            // EPISODIOS_PACIENTE.lbPaciente = vstPaciente
                            // ' en el tag el gidenpac del paciente seleccionado
                            // EPISODIOS_PACIENTE.Tag = vstIdPaciente
                            // EPISODIOS_PACIENTE.Show
                            // Me.MousePointer = Default
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                        }

                    }
                    break;
                case 3:
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.proMnu_ing11(3);
                    }
                    else
                    {
                        // Llamada a la DLL de FILIACION para seleccionar al paciente que quiero
                        // anular el alta
                        // ralopezn_TODO_X_3 26/03/2016
                        Anfiliacion = new filiacionDLL.Filiacion();                        
                        Anfiliacion.Load(Anfiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                        Anfiliacion = null;
                        this.Show();

                        menu_alta.DefInstance.Tag = stNombre2.Trim() + " " + stApellido1.Trim() + " " + stApellido2.Trim() + ";" + vstAsegurado.Trim() + ";" + vstCodigosoc.Trim() + ";" + vstHistoria.Trim() + ";" + vstIdPaciente.Trim();
                        if (Convert.ToString(menu_alta.DefInstance.Tag).Trim() == ";;;;")
                        {
                            //Load menu_alta
                            //menu_alta.Show
                            return;
                        }
                        else
                        {
                            //Load anu_alta
                            //anu_alta.Show 1
                            anulacion_alta = new Anulacion.Alta();
                            anulacion_alta.recibe_paciente(anulacion_alta, menu_alta.DefInstance, ref Conexion_Base_datos.RcAdmision, stNombre2.Trim() + " " +
                                                           stApellido1.Trim() + " " + stApellido2.Trim(), vstAsegurado.Trim(), vstCodigosoc.Trim(), vstHistoria.Trim(), vstIdPaciente.Trim(), Conexion_Base_datos.VstCodUsua, "H");
                            anulacion_alta = null;
                            this.Show();
                        }
                    }
                    break;
                case 4:
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.MuestraCambioEntidad();
                    }
                    else
                    {
                        // cambio de entidad de pacientes ya dados de alta
                        this.Cursor = Cursors.WaitCursor;
                        //llamo a la dll de filiaci�n para buscar al paciente                        
                        vmcFiliacion = new filiacionDLL.Filiacion();                        
                        vmcFiliacion.Load(vmcFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                        vmcFiliacion = null;
                        //Me.Show
                        if (vstIdPaciente != "")
                        {
                            // si ha seleccionado un paciente en filiaci�n
                            this.Cursor = Cursors.WaitCursor;
                            // mando a la nueva ventana el nombre del paciente
                            EPISODIOS_PACIENTE.DefInstance.LbPaciente.Text = vstPaciente;
                            // en el tag el gidenpac del paciente seleccionado
                            EPISODIOS_PACIENTE.DefInstance.Tag = vstIdPaciente;
                            EPISODIOS_PACIENTE.DefInstance.ShowDialog();
                            this.Show();
                            this.Cursor = Cursors.Default;
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                        }
                    }

                    break;
                case 5:

                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.MuestraCambioServ_Med();
                    }
                    else
                    {

                        //cambio de servicio/m�dico de pacientes ya dados de alta
                        this.Cursor = Cursors.WaitCursor;
                        //llamo a la dll de filiaci�n para buscar al paciente                        
                        vmcFiliacion = new filiacionDLL.Filiacion();                        
                        vmcFiliacion.Load(vmcFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                        vmcFiliacion = null;                        
                        //Me.Show
                        if (vstIdPaciente != "")
                        {
                            // si ha seleccionado un paciente en filiaci�n
                            this.Cursor = Cursors.WaitCursor;
                            // mando a la nueva ventana el nombre del paciente
                            //CAMBIO_SERMED_ALTA.LbPaciente = vstPaciente
                            // en el tag el gidenpac del paciente seleccionado
                            //CAMBIO_SERMED_ALTA.Tag = vstIdPaciente
                            //CAMBIO_SERMED_ALTA.Show

                            //OSCAR C MARZO 2006
                            //EL CAMBIO DE SERVICIO SE HA SACADO A UNA DLL
                            // ralopezn_TODO_X_3 26/03/2016
                            oCambioServ = new CambioServicio.clsCambioServicio();
                            //UPGRADE_TODO: (1067) Member proRecPacienteAlta is not defined in type object. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                            oCambioServ.proRecPacienteAlta(Conexion_Base_datos.RcAdmision, vstIdPaciente, vstPaciente, Conexion_Base_datos.VstCodUsua);
                            oCambioServ = null;
                            this.Show();
                            //-------------

                            this.Cursor = Cursors.Default;
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                        }

                    }

                    break;
                case 6:
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.ProConsumosPac();
                    }
                    else
                    {
                        // ralopezn_TODO_X_3 26/03/2016
                        ClassFiliacion = new filiacionDLL.Filiacion();                        
                        ClassFiliacion.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);

                        if (vstIdPaciente.Trim() != "")
                        { //si tiene gidenpac
                            dfechallegada = DateTime.FromOADate(0);

                            //          busca si tiene datos
                            sql = "select * from aepisadm Where " +
                                  " aepisadm.gidenpac =  '" + vstIdPaciente + "' and " +
                                  " aepisadm.faltplan is null ";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                            RrSql = new DataSet();
                            tempAdapter_2.Fill(RrSql);
                            if (RrSql.Tables[0].Rows.Count != 0)
                            {
                                RadMessageBox.Show("El paciente est� ingresado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                            }
                            else
                            {
                                sql = "select max(fllegada) as fllegada from aepisadm Where  gidenpac = '" + vstIdPaciente + "'";
                                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                                RrSql = new DataSet();
                                tempAdapter_3.Fill(RrSql);
                                if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fllegada"]))
                                {
                                    RadMessageBox.Show("El paciente no ha estado ingresado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                                }
                                else
                                {
                                    sql = "select * from aepisadm, dmovfact,amovimie Where  aepisadm.gidenpac = '" + vstIdPaciente + "' and " +
                                          "aepisadm.ganoadme = dmovfact.ganoregi and " +
                                          "aepisadm.gnumadme = dmovfact.gnumregi and " +
                                          "aepisadm.ganoadme = amovimie.ganoregi and " +
                                          "aepisadm.gnumadme = amovimie.gnumregi and " +
                                          "fllegada >= '" + StringsHelper.Format(RrSql.Tables[0].Rows[0]["fllegada"], "MM/dd/yyyy HH:mm:00") + " ' and " +
                                          "fllegada <= '" + StringsHelper.Format(RrSql.Tables[0].Rows[0]["fllegada"], "MM/dd/yyyy HH:mm:59") + " ' and " +
                                          "dmovfact.itiposer = 'H' and " +
                                          "amovimie.itiposer = 'H' and " +
                                          "dmovfact.ffechfin = aepisadm.faltplan and " +
                                          "amovimie.ffinmovi = aepisadm.faltplan and " +
                                          "dmovfact.gcodeven = 'ES'";
                                    dfechallegada = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]);

                                    if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
                                    {
                                        string tempRefParam3 = "DMOVFACT";
                                        sql = sql + " UNION ALL " + Serrores.proReplace(ref sql, tempRefParam3, "DMOVFACH");
                                    }

                                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                                    RrSql = new DataSet();
                                    tempAdapter_4.Fill(RrSql);
                                    if (RrSql.Tables[0].Rows.Count == 0)
                                    {
                                        sql = "select * from aepisadm, amovimie  Where  gidenpac = '" + vstIdPaciente + "' and " +
                                              "fllegada >= '" + StringsHelper.Format(dfechallegada, "mm/dd/yyyy hh:nn:00") + "' and " +
                                              "fllegada <= '" + StringsHelper.Format(dfechallegada, "mm/dd/yyyy hh:nn:59") + "' AND " +
                                              "aepisadm.ganoadme = amovimie.ganoregi and " +
                                              "aepisadm.gnumadme = amovimie.gnumregi and " +
                                              "amovimie.itiposer = 'H' and " +
                                              "amovimie.ffinmovi = aepisadm.faltplan ";

                                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                                        RrSql = new DataSet();
                                        tempAdapter_5.Fill(RrSql);
                                        vstA�oAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["ganoadme"]);
                                        vstnumAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["gnumadme"]);
                                        UniEnf = Convert.ToString(RrSql.Tables[0].Rows[0]["gunenfde"]);
                                        //If IsNull(Rrsql("fingplan")) Then
                                        //    Call MsgBox("Paciente no valorado", vbExclamation + vbOKOnly)
                                        //Else
                                         
                                        ClaseConsumos = new ConsumosPac.clsConsPac();
                                        StFechaAlta = FechaAltaEpisodio(vstA�oAdmi, vstnumAdmi);
                                        if (HaPasadoFacturacionMensual(DateTime.Parse(StFechaAlta), vstA�oAdmi, vstnumAdmi))
                                        {
                                            short tempRefParam4 = 1130;
                                            string[] tempRefParam5 = new string[] { "continuar.Hay", "facturaci�n de este paciente" };
                                            Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, Conexion_Base_datos.RcAdmision, tempRefParam5);
                                            this.Cursor = Cursors.Default;
                                            this.Show();
                                            return;
                                        }
                                        //ESTADO_DE_SALA.GRID1.Col = 17
                                        //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                                        //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                                        //UPGRADE_TODO: (1067) Member ParamConsumos is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx                                        
                                        ClaseConsumos.ParamConsumos(vstIdPaciente, "H", vstnumAdmi, vstA�oAdmi, (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fingplan"])) ? Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"].ToString()) : Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fingplan"].ToString()), Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_impreColor, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), UniEnf, Convert.ToDateTime(StFechaAlta));
                                        //End If
                                    }
                                    else
                                    {
                                        if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ffactura"]))
                                        {
                                            vstA�oAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["ganoadme"]);
                                            vstnumAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["gnumadme"]);
                                            UniEnf = Convert.ToString(RrSql.Tables[0].Rows[0]["gunenfde"]);
                                            
                                            ClaseConsumos = new ConsumosPac.clsConsPac();
                                            StFechaAlta = FechaAltaEpisodio(vstA�oAdmi, vstnumAdmi);
                                            if (HaPasadoFacturacionMensual(DateTime.Parse(StFechaAlta), vstA�oAdmi, vstnumAdmi))
                                            {
                                                short tempRefParam6 = 1130;
                                                string[] tempRefParam7 = new string[] { "continuar.Hay", "facturaci�n de este paciente" };
                                                Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, Conexion_Base_datos.RcAdmision, tempRefParam7);
                                                this.Cursor = Cursors.Default;
                                                this.Show();
                                                return;
                                            }
                                            //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                                            //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                                            //UPGRADE_TODO: (1067) Member ParamConsumos is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                                            
                                            ClaseConsumos.ParamConsumos(vstIdPaciente, "H", vstnumAdmi, vstA�oAdmi, (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fingplan"])) ? Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"].ToString()) : Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fingplan"].ToString()), Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, menu_admision.DefInstance.CommDiag_imprePrint, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), UniEnf, Convert.ToDateTime(StFechaAlta));
                                            ClaseConsumos = null;
                                        }
                                        else
                                        {
                                            RadMessageBox.Show("El paciente ya est� facturado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                                        }
                                    }
                                }
                            }
                            RrSql.Close();
                        }
                    }
                    //oscar 20/12/2004 
                    break;
                case 7:
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.ProMantenimientoDatosFiliacion();
                    }
                    else
                    {                        
                        ClassFiliacion = new filiacionDLL.Filiacion();                        
                        ClassFiliacion.Load(ClassFiliacion, this, "MODIFICACION", "ADMISION", Conexion_Base_datos.RcAdmision);
                        ClassFiliacion = null;
                    }
                    break;
                case 8:
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.ProListadosPacienteAlta();
                    }
                    else
                    {                        
                        ClassFiliacion = new filiacionDLL.Filiacion();                        
                        ClassFiliacion.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                        ClassFiliacion = null;

                        if (vstIdPaciente.Trim() != "")
                        {
                            stSql = " SELECT * FROM AEPISADM WHERE faltplan is not null and gidenpac = '" + vstIdPaciente + "' ";
                            SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                            RrSql = new DataSet();
                            tempAdapter_6.Fill(RrSql);
                            if (RrSql.Tables[0].Rows.Count == 0)
                            {
                                stSql = "select * from aepisadm where AEPISADM.gidenpac ='" + vstIdPaciente + "'";
                                SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                                RrSql = new DataSet();
                                tempAdapter_7.Fill(RrSql);
                                if (RrSql.Tables[0].Rows.Count == 0)
                                { //EL paciente no ha estado ingresado nunca
                                    RadMessageBox.Show("Este paciente no ha estado ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                                    return;
                                }
                                else
                                {
                                    RadMessageBox.Show("Este paciente esta ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                                    return;
                                }
                            }
                            else
                            {
                                if (RrSql.Tables[0].Rows.Count == 1)
                                {
                                    vstA�oAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Ganoadme"]);
                                    vstnumAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Gnumadme"]);
                                }
                                else
                                {
                                    //hay m�s de un paciente, muestra los episodios para sacar los listados
                                    //de un episodio en concreto
                                    mcSelEpisodios = new SelEpisodio.MCSelEpisodio();
                                    mcSelEpisodios.Llamada(this, Conexion_Base_datos.RcAdmision, vstIdPaciente, Conexion_Base_datos.VstCodUsua, "A");
                                    mcSelEpisodios = null;
                                    if (vstAnoadme == 0 && vstNumadme == 0)
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        vstA�oAdmi = vstAnoadme;
                                        vstnumAdmi = vstNumadme;
                                    }
                                }
                            }
                            RrSql.Close();
                                                                                    
                            mcListadosPac = new MovimientosPac.MCMovimientosPac();
                            /*//DGMORENOG_TODO_X_4 - IMPLEMENTA CRYSTALREPORT
                            Conexion_Base_datos.LISTADO_CRYSTAL.Connect = "DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a;
                            mcListadosPac.load(Conexion_Base_datos.RcAdmision, vstIdPaciente, vstPaciente, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, vstA�oAdmi, vstnumAdmi);*/
                            mcListadosPac.load(ref Conexion_Base_datos.RcAdmision, vstIdPaciente, vstPaciente, null, Conexion_Base_datos.PathString, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, vstA�oAdmi, vstnumAdmi);
                            mcListadosPac = null;
                            this.Show();
                        }
                    }
                    //----------- 
                    break;
                case 9:
                    if (EstaCargada_menu_alta())
                    {
                        menu_alta.DefInstance.ProConvierteEpisodioHtoHD();
                    }
                    else
                    {                        
                        ClassFiliacion = new filiacionDLL.Filiacion();                        
                        ClassFiliacion.Load(ClassFiliacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                        ClassFiliacion = null;
                        if (vstIdPaciente.Trim() != "")
                        {
                            stSql = " SELECT * FROM AEPISADM WHERE faltplan is null and gidenpac = '" + vstIdPaciente + "' and ganoadme>1900";
                            SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                            RrSql = new DataSet();
                            tempAdapter_8.Fill(RrSql);
                            if (RrSql.Tables[0].Rows.Count > 0)
                            {
                                RadMessageBox.Show("Este paciente esta ingresado.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                                return;
                            }
                            stSql = "select * from aepisadm WHERE faltplan is not null and gidenpac = '" + vstIdPaciente + "' and ganoadme>1900 ";
                            SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                            RrSql = new DataSet();
                            tempAdapter_9.Fill(RrSql);
                            if (RrSql.Tables[0].Rows.Count == 0)
                            {
                                RadMessageBox.Show("El paciente seleccionado no tiene Episodios de Hospitalizaci�n de alta.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                                return;
                            }
                            else if (RrSql.Tables[0].Rows.Count == 1)
                            {
                                vstA�oAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Ganoadme"]);
                                vstnumAdmi = Convert.ToInt32(RrSql.Tables[0].Rows[0]["Gnumadme"]);
                            }
                            else
                            {
                                //hay m�s de un episodio de alta
                                mcSelEpisodios = new SelEpisodio.MCSelEpisodio();
                                mcSelEpisodios.Llamada(this, Conexion_Base_datos.RcAdmision, vstIdPaciente, Conexion_Base_datos.VstCodUsua, "A");
                                mcSelEpisodios = null;
                                if (vstAnoadme == 0 && vstNumadme == 0)
                                {
                                    return;
                                }
                                else
                                {
                                    vstA�oAdmi = vstAnoadme;
                                    vstnumAdmi = vstNumadme;
                                }
                            }
                            RrSql.Close();

                            if (vstA�oAdmi < 1900)
                            {
                                RadMessageBox.Show("El episodio seleccionado ya es un episodio de Hospital de D�a. No se puede convertir.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                                return;
                            }
                            else
                            {
                                //ConversionHDaH tempLoadForm3 = ConversionHDaH.DefInstance;
                                //ConversionHaHD.DefInstance.proRecibeEpisodio(vstA�oAdmi, vstnumAdmi);
                                ////UPGRADE_WARNING: (7009) Multiple invocations to ShowDialog in Forms with ActiveX Controls might throw runtime exceptions More Information: http://www.vbtonet.com/ewis/ewi7009.aspx
                                //ConversionHaHD.DefInstance.ShowDialog();
                                //RefrescaPacientes();
                            }
                        }
                    }
                    this.Show();
                    break;
            }
        }

        //********************************************************************************************************
        //*                                                                                                      *
        //*  Procedimiento: Toolbar1_ButtonClick                                                                 *
        //*                                                                                                      *
        //*  Modificaci�n:                                                                                       *
        //*                                                                                                      *
        //*    O.Frias (16/11/2006) Se incorpora el �ltimo par�metro de la llamada a mantecamas.manteclass para  *
        //*                         que muestre el boton de reserva.                                             *
        //*                                                                                                      *
        //********************************************************************************************************

        public void mnu_adm013_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm013, eventSender);
			switch(Index)
			{
				case 0 : 
					nvisualizacion = new mantecamas.manteclass(); 
					//******************* O.Frias (14/11/2006) Inicio ******************* 
					//nvisualizacion.proLoad nvisualizacion, Me, RcAdmision, "H", VstCodUsua, "M" 
					object tempRefParam = this; 
					string tempRefParam2 = "H"; 
					string tempRefParam3 = "M"; 
					string tempRefParam4 = ""; 
					bool tempRefParam5 = false; 
					string tempRefParam6 = ""; 
					bool tempRefParam7 = true; 
					nvisualizacion.proload(nvisualizacion, tempRefParam, ref Conexion_Base_datos.RcAdmision, tempRefParam2, Conexion_Base_datos.VstCodUsua, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, tempRefParam7); 
					//******************** O.Frias (14/11/2006) Fin  ******************** 

					 
					nvisualizacion = null; 
					this.Show(); 
					break;
				case 1 :
                    
                    cc = new Listado_Genereal_Camas.mcList_Camas();
                    ////cc.proReferencias cc, Me, RcAdmision, PathString, "H", "ENF1" 
                    //DGMORENOG_TODO_X_4 - IMPLEMENTA CRYSTALREPORT
                    //cc.proReferencias(cc, this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.PathString, "H", "", Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 
                    cc.proReferencias(cc, this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.PathString, "H", "", null, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a);
                    cc = null; 
                    this.Show(); 
					break;
			}
		}

        //UPGRADE_NOTE: (7001) The following declaration (mnu_adm0133_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        //private void mnu_adm0133_Click(int Index)
        //{
        //switch(Index)
        //{
        //case 0 : 
        // Llamada a la DLL que presenta la pantalla del listado general de camas 
        // Le pasamos la conexi�n, y la unidad donde estoy 
        //cc = new Listado_Genereal_Camas.mcList_Camas(); 
        //cc.proReferencias cc, Me, RcAdmision, PathString, "H", "ENF1" 
        ////UPGRADE_TODO: (1067) Member proReferencias is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
        //cc.proReferencias(cc, this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.PathString, "H", "", Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 
        //cc = null; 
        //this.Show(); 
        //break;
        //case 1 : 
        //List_censo_de_camas tempLoadForm = List_censo_de_camas.DefInstance; 
        //List_censo_de_camas.DefInstance.Show(); 
        //break;
        //case 2 : 
        //List_traslados tempLoadForm2 = List_traslados.DefInstance; 
        //List_traslados.DefInstance.Show(); 
        //break;
        //}
        //}

        public void mnu_adm014_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm014, eventSender);
			filiacionDLL.Filiacion instancia = null;
			string tstConexion = String.Empty;
            ListadosIngresados.ListIngresados listados;
            ReservasPendIngreso.MCReservas Reservas = null;

		//	object Referencia = null;
			switch(Index)
			{
				case 0 :
                    //libro de registro 
                   libro_regi.DefInstance.Show();
                    break;
				case 1 : 
					//general de movimientos 
                    // ralopezn_TODO_X_3 30/03/2016
                    general_movimiento tempLoadForm2 = general_movimiento.DefInstance; 
                    general_movimiento.DefInstance.Show(); 
					break;
				case 2 : 
					//prevision de altas 
                    prevision_alta.DefInstance.Show(); 
					break;
				case 3 : 
					//anulacon de ingresos altas                   
                    Anulacion_ingresos_alta.DefInstance.Show(); 
					break;
				case 4 : 
					// cambio de servicio medico 
                    movimientos_pacientes.DefInstance.Show(); 
					break;
				case 5 : 
					//MsgBox ("Conecta con DLL Localizacion de paciente") 
					 
					break;
				case 6 : 
					//OSCAR C 26/07/2005 
					//Se ha sacado a una DLL 
					//ingresos/ingresados servicio/m�dico/entidad 
					//I.S.M. 5-May-2000. A�adido para distinguir desde d�nde se llama 
					//bOficial = False 
					//Load Ingresados_servicio 
					//Ingresados_servicio.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI416F1", "ADMISION"); 
                    listados = null; 
					this.Show(); 
					//------- 
					break;
				case 7 :
                    //O.Frias - 09/03/2009 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI416F2", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					 
					break;
				case 8 : 
					// consulta de la historia 
					//MsgBox ("Opci�n temporalmente no disponible") 
					tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + "";
                    // ralopezn_TODO_X_3 28/03/2016
                    //UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
                    //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion); 
                    //instancia = new HistoriaClinicaPaciente.MCHClinicaPac(); 
                    ////UPGRADE_TODO: (1067) Member Llamada is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                    //instancia.Llamada(this, instancia, Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.stNombreDsnACCESS, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.BDtempo); 
                    //instancia = null; 
                    break;
				case 9 : 
					//Consulta de episodios 
					this.Cursor = Cursors.WaitCursor;                    
                    instancia = new filiacionDLL.Filiacion();                    
                    instancia.Load(instancia, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                    instancia = null;
                    this.Show(); 
					this.Cursor = Cursors.Default;
                    
                    if (vstIdPaciente != "")
					{                                                
                        MOVI_PACI_ENTI.DefInstance.Tag = vstIdPaciente + "*" + vstPaciente;
                        MOVI_PACI_ENTI.DefInstance.Show();
					} 
					break;
				case 10 : 
					//Programaciones de intervenciones quirurgicas 
					this.Cursor = Cursors.WaitCursor;
                    rptPacInterProg.rptPacProgInt rptPacProg = new rptPacInterProg.rptPacProgInt();
                    rptPacProg.Muestra(this, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.StBaseTemporal, Conexion_Base_datos.LISTADO_CRYSTAL, ref Conexion_Base_datos.RcAdmision, "");
                    rptPacProg = null; 
                    this.Show(); 
					this.Cursor = Cursors.Default; 
					break;
				case 11 :
                    //listado reservas pendientes de ingreso 
                    //        Load Reservas_Pend_Ingreso 
                    //        Reservas_Pend_Ingreso.Show vbModal 
                    //llamar a la dll 
                    Reservas = new ReservasPendIngreso.MCReservas();
                    Reservas.LoadClase(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.Cuadro_Diag_imprePrint, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.StBaseTemporal);
                    Reservas = null;

                    break;
				case 12 :
                    //OSCAR C 12/09/2005: Se ha sacado a una DLL 
                    //Load Listado_ausencias 
                    //Listado_ausencias.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI570F1", "ADMISION"); 
                    listados = null; 
                    //------- 
                    break;
				case 13 :                    
                    InforInterco.ClInforInterco Instancia = new InforInterco.ClInforInterco();                     
                    Instancia.LoadClase(Conexion_Base_datos.RcAdmision, "H", Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.stNombreDsnACCESS, Conexion_Base_datos.BDtempo, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.PathString, Conexion_Base_datos.Cuadro_Diag_imprePrint); 
                    instancia = null; 
                    break;
				case 14 :
                    //tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + ""; 
                    PacOtrosCentrosDLL.PacOtrosCentros Referencia = new PacOtrosCentrosDLL.PacOtrosCentros(); 
                    Referencia.proLlamada(this, Conexion_Base_datos.RcAdmision, "H", Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.gUsuario, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS); 
                    Referencia = null; 
                    break;
				case 15 :
                    PacRemitidosDLL.MCPacRemitidos pacRemitidos = new PacRemitidosDLL.MCPacRemitidos();
                    pacRemitidos.proLlamada(this, Conexion_Base_datos.RcAdmision, "A", Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.gUsuario, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_imprePrint); 
                    //instancia = null; 
                    break;
				case 16 :                     
                    ReservasAnuladas.DefInstance.Show(); 
					break;
				case 17 :                                         
                    Listado_ambulancias.DefInstance.Show(); 
					break;
			}
		}

		public void mnu_adm01401_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm01401, eventSender);
			DLLPacHosp.MCPACHOSP paci_hospita = null;
            DLLPacNoHosp.MCPACNOHOSP paci_no_hospita = null;
			filiacionDLL.Filiacion paci_filiacion = null;
			string sql = String.Empty;
			DataSet RR = null;
			string Idpaciente = String.Empty;
			switch(Index)
			{
				case 0 :
                    //pacientes hospitalizados                     
                    paci_hospita = new DLLPacHosp.MCPACHOSP(); 
                    foreach (Form a in Application.OpenForms)
                    {
                        if (a.Name == "menu_ingreso")
                        {                            
                            menu_ingreso.DefInstance.SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
                            sql = "SELECT gidenpac FROM aepisadm where " +
                                  " ganoadme=" + menu_ingreso.DefInstance.SprPacientes.Text.Trim().Substring(0, Math.Min(menu_ingreso.DefInstance.SprPacientes.Text.Trim().IndexOf('/'), menu_ingreso.DefInstance.SprPacientes.Text.Trim().Length)) +
                                  " and gnumadme=" + menu_ingreso.DefInstance.SprPacientes.Text.Trim().Substring(menu_ingreso.DefInstance.SprPacientes.Text.Trim().IndexOf('/') + 1);
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                            RR = new DataSet();
                            tempAdapter.Fill(RR);
                            Idpaciente = Convert.ToString(RR.Tables[0].Rows[0]["gidenpac"]);
                            RR.Close();
                        }
                    }

                    if (Idpaciente == "")
                    {                        
                        paci_hospita.load(paci_hospita, this, Conexion_Base_datos.RcAdmision);
                    }
                    else
                    {                        
                        paci_hospita.load(paci_hospita, this, Conexion_Base_datos.RcAdmision, Idpaciente);
                    }
                    paci_hospita = null;
                    this.Show();
					break;
				case 1 : 
					//pacientes no hospitalizados 
					//llamo a filiacion para obtener el gidenpac 
					this.Cursor = Cursors.WaitCursor;                    
                    paci_filiacion = new filiacionDLL.Filiacion();
                    //UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                    paci_filiacion.Load(paci_filiacion, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
                    paci_filiacion = null;

                    if (vstIdPaciente != "")
                    {
                        paci_no_hospita = new DLLPacNoHosp.MCPACNOHOSP();                        
                        paci_no_hospita.load(paci_no_hospita, this,ref Conexion_Base_datos.RcAdmision, vstIdPaciente);
                        paci_no_hospita = null;
                    }
                    this.Show(); 
					this.Cursor = Cursors.Default; 
					break;
			}
		}

		public void mnu_adm01501_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm01501, eventSender);
            ListadosIngresados.ListIngresados listados = null;
			switch(Index)
			{
				case 0 :
                    //listado general de pacientes ingresados 
                    List_censo_de_camas.DefInstance.Show();
                    break;
				case 1 :
                    //listado de traslados de pacientes ingresados 
                    List_traslados.DefInstance.Show();
                    break;
				case 2 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    //ingresados por regimen de alojamiento 
                    //Load INGRESADOS_REGALOJ 
                    //INGRESADOS_REGALOJ.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI420F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 3 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    // ingresados por planta 
                    // Load INGRESADOS_PLANTA 
                    //INGRESADOS_PLANTA.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI430F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 4 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    // listado servicio<-> entidad 
                    //Load Ingresados_servicio_entidad 
                    //Ingresados_servicio_entidad.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI450F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 5 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    // listado por unidad de hospitalizaci�n 
                    //Load Ingresados_hospitalizacion 
                    //Ingresados_hospitalizacion.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI460F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 6 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    //ingresados seguridad social 
                    //Load INGRESADOS_SS 
                    //INGRESADOS_SS.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI425F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 7 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    //Load INGRESADOS_LOCALIDAD 
                    //INGRESADOS_LOCALIDAD.Show 
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI435F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 8 : 
					//Relaci�n de pacientes con Citas Externas 
					//        IResume = oclass.RespuestaMensaje(vNomAplicacion, vAyuda, 1820, _
					//'        RcAdmision, "Relaci�n de Pacientes con Citas Externas") 
					//        If IResume = vbYes Then 
					Imprimir_pacientes_citas_externas(); 
					//        End If 
					break;
				case 9 : 
					//Listado de cuentas bancarias para pacientes ingresados 
					short tempRefParam = 1820; 
					string[] tempRefParam2 = new string[]{"Listado de pacientes ingresados por CIAS"}; 
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2)); 
					if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.Yes)
					{
						Imprimir_CIAS();
					} 
					break;
				case 10 :
                    listados = new ListadosIngresados.ListIngresados();
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI700F1", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					break;
			}
		}

		public void mnu_adm01601_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnu_adm01601, eventSender);
			//object instancia = null;
			string tstConexion = String.Empty;
            ListadosIngresados.ListIngresados listados;

            switch (Index)
			{
				case 0 : 
					// numeros ine 
                    Morbilidad_hospitalaria tempLoadForm = Morbilidad_hospitalaria.DefInstance; 
                    Morbilidad_hospitalaria.DefInstance.Show(); 
					break;
				case 1 : 
					//comprobaci�n n�meros de salida 
                    Movimientos_SS tempLoadForm2 = Movimientos_SS.DefInstance; 
                    Movimientos_SS.DefInstance.Show(); 
					break;
				case 2 : 
					// generacion del fichero para mailing 
					FiMailing.DefInstance.Show(); 
					break;
				case 3 :
                    //listado de exitus 
                    tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + ""; 
                    ////UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
                    //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion); 

                    DLLExitus.MCExitus instancia = new DLLExitus.MCExitus();                    
                    instancia.load(instancia, this, Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.stNombreDsnACCESS, Conexion_Base_datos.BDtempo); 
                    instancia = null; 
                    //MsgBox ("Opci�n temporalmente no disponible") 
                    break;
				case 4 : 
					//Movimientos por entidades                    
                    ingresos_por_entidades tempLoadForm4 = ingresos_por_entidades.DefInstance; 
                    ingresos_por_entidades.DefInstance.Show(); 
					 
					//I.S.M. 5-May-2000. A�adido para Imprimir un listado nuevo 
					break;
				case 5 :
                    //OSCAR C 26/07/2005 
                    //Se ha sacado a una DLL 
                    //Ingresados por Entidad/Servicio 
                    //bOficial = True 'Para distinguir desde d�nde se llama 
                    //Load Ingresados_servicio 
                    //Ingresados_servicio.Show 
                    listados = new ListadosIngresados.ListIngresados(); 
                    listados.listado(Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.LISTADO_CRYSTAL, CommDiag_imprePrint, Conexion_Base_datos.stNombreDsnACCESS, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "AGI416F1-O", "ADMISION"); 
                    listados = null; 
                    this.Show(); 
					//------- 
					break;
				case 6 : 
					//Relaci�n de partes emitidos a juzgado 
					short tempRefParam = 1820; 
					string[] tempRefParam2 = new string[]{"Relaci�n de partes emitidos a juzgado"}; 
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2)); 
					if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.Yes)
					{
						Imprimir_partes_emitidos_juzgado();
					} 
					break;
				case 7 : 
					short tempRefParam3 = 1820; 
					string[] tempRefParam4 = new string[]{"Relaci�n de notificaciones al juzgado pendientes de expediente"}; 
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4)); 
					if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.Yes)
					{
						Imprimir_notificaciones_pendientes_expediente();
					} 
					break;
				case 8 : 
					//Notificacion de permanencia en el centro 
					short tempRefParam5 = 1820; 
					string[] tempRefParam6 = new string[]{"Notificaci�n de permanencia en el centro"}; 
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6)); 
					if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.Yes)
					{
						Imprimir_Notificacion_permanencia_centro();
					} 
					 
					break;
			}
		}
		private void Imprimir_partes_emitidos_juzgado()
		{
            //OSCAR C 16/09/2005: Se ha sacado a la dll DatosJuzgado
            //'On Error GoTo Etiqueta_Error
            //'Me.MousePointer = 11
            //'Dim sql As String
            //'sql = " SELECT " & _
            //''" AEPISADM.nnumexp1, AEPISADM.fresolu1, AEPISADM.fultnotif, " & _
            //''" DJUZGADO.dnomjuzg, DJUZGADO.ddirjuzg, DJUZGADO.gcodipos, " & _
            //''" DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " & _
            //''" DPACIENT.ndninifp , DPROVINC.dnomprov, DPOBLACI.DPOBLACI " & _
            //''" From " & _
            //''" (((AEPISADM INNER JOIN DPACIENT ON " & _
            //''" AEPISADM.gidenpac = DPACIENT.gidenpac) " & _
            //''" INNER JOIN DJUZGADO ON " & _
            //''" AEPISADM.gjuzgad1 = DJUZGADO.gjuzgado) " & _
            //''" LEFT OUTER JOIN DPOBLACI ON " & _
            //''" DJUZGADO.gpoblaci = DPOBLACI.gpoblaci) " & _
            //''" LEFT OUTER JOIN DPROVINC ON " & _
            //''" DJUZGADO.gprovinc = DPROVINC.gprovinc " & _
            //''" Where " & _
            //''" aepisadm.faltplan is null and " & _
            //''" aepisadm.gjuzgad1 is not null " & _
            //''" Order By DJUZGADO.dnomjuzg Asc, DPACIENT.dape1pac ASC, " & _
            //''" DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC "
            //'If VstrCrystal.VfCabecera = False Then
            //'    proCabCrystal
            //'End If
            //''hay que cambiar el destino del informe y la ubicacion
            //'crystalgeneral.ReportFileName = App.Path & "\..\Elementoscomunes\rpt\AIC4120R1.rpt"
            //'crystalgeneral.Formulas(0) = "FORM1=""" & VstrCrystal.VstGrupoHo & """"
            //'crystalgeneral.Formulas(1) = "FORM2=""" & VstrCrystal.VstNombreHo & """"
            //'crystalgeneral.Formulas(2) = "FORM3=""" & VstrCrystal.VstDireccHo & """"
            //'
            //''DV
            //'Dim LitPersona As String
            //'LitPersona = ObtenerLiteralPersona(RcAdmision)
            //''LitPersona = UCase(LitPersona)
            //'crystalgeneral.Formulas(3) = "LitPers=""" & LitPersona & """"
            //'
            //'Dim sTitulo As String
            //'sTitulo = "RELACI�N DE PARTES EMITIDOS AL JUZGADO DE " & UCase(LitPersona) & "S INGRESADOS"
            //'crystalgeneral.Formulas(4) = "TituloLitPers=""" & sTitulo & """"
            //'
            //''RELACI�N DE PARTES EMITIDOS AL JUZGADO DE PACIENTES INGRESADOS
            //'
            //'crystalgeneral.Destination = crptToWindow
            //'crystalgeneral.WindowState = crptMaximized
            //'crystalgeneral.WindowTitle = "Relaci�n de partes emitidos a juzgado"
            //'crystalgeneral.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""
            //'crystalgeneral.SQLQuery = sql
            //'crystalgeneral.Action = 1
            //'crystalgeneral.Reset
            //'Me.MousePointer = 99
            //'Exit Sub
            //'Etiqueta_Error:
            //'    crystalgeneral.Reset
            //'    If UCase(Err.Source) = "CRYSTALREPORT" Then
            //'        GestionErrorCrystal Err.Number, RcAdmision
            //'    Else
            //'     AnalizaError "admision", App.Path, VstCodUsua, "Menu_Admision:imprimir partes emitidos juzgado", RcAdmision
            //'    End If
            //'    Me.MousePointer = 99

            DLLDatosJuzgado.clsDatos_juzgado OBJ = new DLLDatosJuzgado.clsDatos_juzgado();
            OBJ.Imprimir(crystalgeneral, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, "AIC4120R1");

        }


        private void Imprimir_notificaciones_pendientes_expediente()
		{
            //OSCAR C 16/09/2005: Se ha sacado a la dll DatosJuzgado
            //'On Error GoTo Etiqueta_Error
            //'Me.MousePointer = 11
            //'Dim sql As String
            //'sql = " SELECT " & _
            //''    " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.ndninifp," & _
            //''    " AEPISADM.fultnotif From  aepisadm INNER JOIN dpacient ON " & _
            //''    " AEPISADM.gidenpac = DPACIENT.gidenpac" & _
            //''    " Where " & _
            //''    " AEPISADM.NNUMEXP1 IS NULL AND (aepisadm.ienfment = 'S' or " & _
            //''    " aepisadm.fnotifincap is not null)and " & _
            //''    " AEPISADM.FALTAADM IS NULL "
            //'If VstrCrystal.VfCabecera = False Then
            //'    proCabCrystal
            //'End If
            //''hay que cambiar el destino del informe y la ubicacion
            //'crystalgeneral.ReportFileName = App.Path & "\..\Elementoscomunes\rpt\AIC4121R1.rpt"
            //'crystalgeneral.Formulas(0) = "FORM1=""" & VstrCrystal.VstGrupoHo & """"
            //'crystalgeneral.Formulas(1) = "FORM2=""" & VstrCrystal.VstNombreHo & """"
            //'crystalgeneral.Formulas(2) = "FORM3=""" & VstrCrystal.VstDireccHo & """"
            //'
            //''DV
            //'Dim LitPersona As String
            //'LitPersona = ObtenerLiteralPersona(RcAdmision)
            //''LitPersona = UCase(LitPersona)
            //'crystalgeneral.Formulas(3) = "LitPers=""" & LitPersona & """"
            //'
            //'crystalgeneral.Destination = crptToWindow
            //'crystalgeneral.WindowState = crptMaximized
            //'crystalgeneral.WindowTitle = "Relaci�n de notificaciones al juzgado pendientes de expediente"
            //'crystalgeneral.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""
            //'crystalgeneral.SQLQuery = sql
            //'' la sql la tiene guardada con el report
            //'crystalgeneral.Action = 1
            //'crystalgeneral.Reset
            //'Me.MousePointer = 99
            //'Exit Sub
            //'Etiqueta_Error:
            //'    crystalgeneral.Reset
            //'    If UCase(Err.Source) = "CRYSTALREPORT" Then
            //'        GestionErrorCrystal Err.Number, RcAdmision
            //'    Else
            //'     AnalizaError "admision", App.Path, VstCodUsua, "Menu_Admision:Imprimir_notificaciones_pendientes_expediente", RcAdmision
            //'    End If
            //'    Me.MousePointer = 99

            DLLDatosJuzgado.clsDatos_juzgado OBJ = new DLLDatosJuzgado.clsDatos_juzgado();
            OBJ.Imprimir(crystalgeneral, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, "AIC4121R1");
        }


        public void mnu_adm017_Click(Object eventSender, EventArgs eventArgs)
		{
            int Index = Array.IndexOf(this.mnu_adm017, eventSender);
            //object instancia = null;
            string stSql = String.Empty;
            DataSet rrUsuario = null;
            string stServUsuario = String.Empty;

            if (Index == 1)
            {
                stServUsuario = "";
                stSql = "SELECT gservici FROM susuario WHERE gusuario = '" + Conexion_Base_datos.VstCodUsua + "'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rrUsuario = new DataSet();
                tempAdapter.Fill(rrUsuario);
                if (rrUsuario.Tables[0].Rows.Count > 0)
                {
                    stServUsuario = Convert.ToString(rrUsuario.Tables[0].Rows[0]["gservici"]) + "";
                }


              ProcesoAbrir.ClApertura instancia = new ProcesoAbrir.ClApertura();
            instancia.Iniciar(Conexion_Base_datos.RcAdmision, stServUsuario, Conexion_Base_datos.VstCodUsua);
            }
            else if (Index == 2)
            {
             ProcesoModificar.ClModificar clModificar = new ProcesoModificar.ClModificar();
              clModificar.Iniciar(ref Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua);
            }
            else if (Index == 3)
            {
                ProcesoCerrar.ClCerrar instancia = new ProcesoCerrar.ClCerrar();
                instancia = new ProcesoCerrar.ClCerrar();
                instancia.Iniciar(ref Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua);
            }


            else
            {                
                ProcesoImpAbiertos.ClImpAbiertos instancia = new ProcesoImpAbiertos.ClImpAbiertos();                
                instancia.Iniciar(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.PathString + "\\rpt", Serrores.vNomAplicacion);
                instancia = null;
            }


        }

        public void mnu_adm018_Click(Object eventSender, EventArgs eventArgs)
		{
            // ralopezn_TODO_X_3 28/03/2016
            //int Index = Array.IndexOf(this.mnu_adm018, eventSender);
            //string tstConexion = String.Empty;
            //object instancia = null;
            //switch(Index)
            //{
            //	case 1 :  //Gestion marco qur�rgico 
            //		tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + ""; 
            //		//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
            //		UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion); 

            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, Index, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		break;
            //	case 2 :  //Gesti�n Pacientes 
            //		tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + ""; 
            //		//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
            //		UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion); 

            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, Index, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		break;
            //	case 4 :  //Estad�stica 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, Index + 1, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 
            //		//OSCAR C Dic 2005: descarga LEQ 
            //		//----- 
            //		break;
            //	case 5 : 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, Index + 1, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		//OSCAR C FEB 2006: Derivaciones RULEQ 
            //		break;
            //	case 6 : 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, 9, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		//OSCAR C OCTUBRE 2012: Gestion de Envios a CatSalud 
            //		//'    Case 7 

            //		//'        Load EnviosCatSalud 
            //		//'        EnviosCatSalud.Show 

            //		//'    Case 8 
            //		//' 
            //		//'        Set instancia = CreateObject("ComparativaCatSalut.clsCompCatSalut") 
            //		//' 
            //		//'        instancia.proIniciaComparacion RcAdmision, Me 

            //		break;
            //}
            //instancia = null;
        }

        public void mnu_adm01801_Click(Object eventSender, EventArgs eventArgs)
		{
            // ralopezn_TODO_X_3 28/03/2016
            //int Index = Array.IndexOf(this.mnu_adm01801, eventSender);
            //object instancia = null;
            //string tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + "";
            ////UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion);
            //switch(Index)
            //{
            //	case 1 :  //Listado de pac.en lista de espera 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, Index + 2, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		break;
            //	case 2 :  //Listado de pacientes en LE por Servicio 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, 8, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		break;
            //	case 3 :  //Listado de pac.en lista de espera 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, 4, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 

            //		break;
            //	case 4 :  //Relaci�n de Episodios (Codificados/No Codificados) 
            //		instancia = new ListadeEsperaDLL.MCListaEspera(); 
            //		//UPGRADE_TODO: (1067) Member MantenimientoPrograma is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //		instancia.MantenimientoPrograma(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.LISTADO_CRYSTAL, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.stNombreDsnACCESS, CommDiag_impre, 7, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a); 


            //		break;
            //}
        }

        public void mnu_adm21_Click(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.HelpNavigator = HelpNavigator.Index;
            CDAyuda.Instance.ShowHelp();
        }

        public void mnu_adm22_Click(Object eventSender, EventArgs eventArgs)
		{
            // 0x101 &H101& HelpKey
            CDAyuda.Instance.HelpNavigator = HelpNavigator.KeywordIndex;
            CDAyuda.Instance.ShowHelp();
        }



        public void mnu_adm31_Click(Object eventSender, EventArgs eventArgs)
		{
            // ralopezn_TODO_X_3 28/03/2016
            ////Private Sub mnu_accesootras_Click()
            //object ifms = new Dll_Menu.Menu();
            this.Cursor = Cursors.WaitCursor;
            //string CodEmpresa = mbAcceso.ObtenerCodigoSConsglo("FOPEMENU", "valfanu1", Conexion_Base_datos.RcAdmision);
            //string CodDpto = mbAcceso.ObtenerCodigoSConsglo("FOPEMENU", "valfanu2", Conexion_Base_datos.RcAdmision);
            ////UPGRADE_TODO: (1067) Member cargar_m is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //int X = Convert.ToInt32(ifms.cargar_m(Conexion_Base_datos.VstCodUsua, CodEmpresa, CodDpto, "SQLSERVER", Conexion_Base_datos.RcAdmision, "ADMISION", "", this));
            int X = 0;
            this.Cursor = Cursors.Default;
            if (X != 0)
            {
                RadMessageBox.Show("Error al abrir men� n�mero :" + Conversion.Str(X).Trim(), Application.ProductName);
            }

        }



        public void mnu_FSI_Click(Object eventSender, EventArgs eventArgs)
		{
            
            int Index = Array.IndexOf(this.mnu_FSI, eventSender);

            Crystal LISTADO_LOCAL = menu_admision.DefInstance.crystalgeneral;

            // ralopezn_TODO_X_3 28/03/2016
            //object pC = new FSI.cFSI();

            //switch (Index)
            //{
            //    case 1:
            //        //UPGRADE_TODO: (1067) Member LoadFacturaSanitariaIndividual is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //        pC.LoadFacturaSanitariaIndividual(Conexion_Base_datos.RcAdmision, LISTADO_LOCAL, Conexion_Base_datos.VstCodUsua, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, "Q", 0, 0);
            //        break;
            //    case 2:
            //        //UPGRADE_TODO: (1067) Member LoadFacturaSanitariaMasiva is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //        pC.LoadFacturaSanitariaMasiva(Conexion_Base_datos.RcAdmision, LISTADO_LOCAL, Conexion_Base_datos.gUsuario, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.BDtempo, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a);
            //        break;
            //    case 3:
            //        //UPGRADE_TODO: (1067) Member LoadRegistroFacturasEntregadas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //        pC.LoadRegistroFacturasEntregadas(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua);
            //        break;
            //}

            //pC = null;
            LISTADO_LOCAL = null;

        }

        public void mnu_pedidos_Click(Object eventSender, EventArgs eventArgs)
		{
            // ralopezn_TODO_X_3 28/03/2016
            //int Index = Array.IndexOf(this.mnu_pedidos, eventSender);
            //try
            //{

            //	object ClasePedidos = null; //New LlamarOperacionesFMS.clsOperFMS

            //	ClasePedidos = new LlamarOperacionesFMS.clsOperFMS();

            //	if (Index == 0)
            //	{
            //		//UPGRADE_TODO: (1067) Member LlamarOperacionesFMS is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		ClasePedidos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "PEDIDOS");
            //	}
            //	else
            //	{
            //		//UPGRADE_TODO: (1067) Member LlamarOperacionesFMS is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		ClasePedidos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "PEDIDOS2");
            //	}

            //	this.Show();
            //}
            //catch
            //{
            //	Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "mnu_pedidos_Click", Conexion_Base_datos.RcAdmision);
            //}

        }

        //OSCAR C MAYO 2008
        //NUEVO REQUERIMIENTO DE DIALISIS
        public void mnu_tto_Click(Object eventSender, EventArgs eventArgs)
		{
            int Index = Array.IndexOf(this.mnu_tto, eventSender);
            string vstIdEpisodio = String.Empty;
            // ralopezn_TODO_X_3 28/03/2016
            //object mcDialisis = new DIALISIS.MCLDialisis();
            ////UPGRADE_TODO: (1067) Member Inicializa is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //mcDialisis.Inicializa(this, Conexion_Base_datos.RcAdmision, CommDiag_impre, crystalgeneral, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Elementoscomunes\\rpt\\", Conexion_Base_datos.BDtempo, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
            switch (Index)
            {
                case 1:
                    vstIdEpisodio = "";
                    if (FormularioActivo("menu_ingreso"))
                    {
                        menu_ingreso.DefInstance.SprPacientes.Row = menu_ingreso.DefInstance.SprPacientes.ActiveRowIndex;
                        menu_ingreso.DefInstance.SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
                        string tempRefParam = menu_ingreso.DefInstance.SprPacientes.Text.Trim();
                        string tempRefParam2 = "/";
                        vstIdEpisodio = "H" + Serrores.proReplace(ref tempRefParam, tempRefParam2, "");
                    }
                    // ralopezn_TODO_X_3 28/03/2016
                    //UPGRADE_TODO: (1067) Member Mantenimiento_Pacientes is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                    //mcDialisis.Mantenimiento_Pacientes(vstIdEpisodio);
                    break;
                case 2:
                    // ralopezn_TODO_X_3 28/03/2016
                    //UPGRADE_TODO: (1067) Member Ingreso_Pacientes is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                    //mcDialisis.Ingreso_Pacientes();
                    break;
                case 3:
                    // ralopezn_TODO_X_3 28/03/2016
                    //UPGRADE_TODO: (1067) Member Alta_Pacientes is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                    //mcDialisis.Alta_Pacientes();
                    break;
            }

            //Ejemplo de llamada al Proceso Automatico de Ingresos en Hospital de Dia Simulado en la DLL DIALISIS
            //Dim stmensaje As String
            //stmensaje = mcDialisis.ProIngresoAutomatico_HD(, 8, "00131", "indra", 2010, 97) 'Tratamiento periodico por cita
            //stmensaje = mcDialisis.ProIngresoAutomatico_HD(2009, 3, "00142", "indra")  'Tratamiento Periodico por periodicidad            
            //Set mcDialisis = Nothing
            //MsgBox stmensaje
            this.Show();
		}
		//----------------------

		//********************************************************************************************************
		//*                                                                                                      *
		//*  Procedimiento: Toolbar1_ButtonClick                                                                 *
		//*                                                                                                      *
		//*  Modificaci�n:                                                                                       *
		//*                                                                                                      *
		//*    O.Frias (14/11/2006) Se incorpora el �ltimo par�metro de la llamada a mantecamas.manteclass para  *
		//*                         que muestre el boton de reserva.                                             *
		//*                                                                                                      *
		//********************************************************************************************************
		private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
            Telerik.WinControls.UI.CommandBarButton Button = (Telerik.WinControls.UI.CommandBarButton) eventSender;

            switch (radCommandBarStripElement1.Items.IndexOf(Button) + 1)
			{
				case 1 : 
					menu_ingreso.DefInstance.Show(); 
					mnu_adm011[0].Enabled = true; 
					break;
				case 2 :
                    mnu_adm012[0].PerformClick();
					break;
				case 3 : 
					this.Cursor = Cursors.WaitCursor; 
					nvisualizacion = new mantecamas.manteclass(); 
					 
					//******************* O.Frias (14/11/2006) Inicio ******************* 
					//   nvisualizacion.proLoad nvisualizacion, Me, RcAdmision, "H", VstCodUsua, "M" 
					object tempRefParam = this; 
					string tempRefParam2 = "H"; 
					string tempRefParam3 = "M"; 
					string tempRefParam4 = ""; 
					bool tempRefParam5 = false; 
					string tempRefParam6 = ""; 
					bool tempRefParam7 = true; 
					nvisualizacion.proload(nvisualizacion, tempRefParam, ref Conexion_Base_datos.RcAdmision, tempRefParam2, Conexion_Base_datos.VstCodUsua, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, tempRefParam7); 
					//******************** O.Frias (14/11/2006) Fin  ******************** 

					 
					nvisualizacion = null; 
					this.Show(); 
					this.Cursor = Cursors.Default; 
					if (Conexion_Base_datos.vstActivado_Ingreso)
					{
						menu_ingreso.DefInstance.refreco_demantecamas();
					} 
					//Load VISUALIZACION 
					//VISUALIZACION.Show 
					break;
				case 4 :
                    mnu_adm001[0].PerformClick();
					break;
				case 5 :
                    // 0x3 &H3& HelpIndex
                    CDAyuda.Instance.HelpNavigator = HelpNavigator.Index;
                    CDAyuda.Instance.ShowHelp();

                    break;
				case 6 : 
					this.Close(); 
					prokill(Conexion_Base_datos.StBaseTemporal); 
					Environment.Exit(0); 
					 
					break;
			}
		}

		public void mnu_adm11_Click()
		{
			RadMessageBox.Show("presenta la ayuda en pantalla", Application.ProductName);
		}

		public void prokill(string tablakill)
		{
			if (FileSystem.Dir(tablakill, FileAttribute.Normal) != "")
			{
				//  Kill (tablakill)
			}
		}
		//''''''''''''''''''''''''''''''''''''''''''''''''''''
		public bool HaPasadoFacturacionMensual(System.DateTime FechaAlta, int vstA�oAdmi, int vstnumAdmi)
		{

			bool result = false;
			try
			{

				string sqlFactura = String.Empty;
				DataSet RrFactura = null;
				sqlFactura = "select max(fperfact) MAXIMAFECHA from DMOVFACT where itiposer='H' and ganoregi=" + vstA�oAdmi.ToString() + 
				             " and gnumregi=" + vstnumAdmi.ToString() + " and datepart(month,fperfact)=" + FechaAlta.Month + 
				             " and datepart(year,fperfact)=" + FechaAlta.Year + " and ffactura is not null";

				if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
				{
					string tempRefParam = "DMOVFACT";
					sqlFactura = sqlFactura + " UNION " + 
					             Serrores.proReplace(ref sqlFactura, tempRefParam, "DMOVFACH") + 
					             " order by 1 desc";
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlFactura, Conexion_Base_datos.RcAdmision);
				RrFactura = new DataSet();
				tempAdapter.Fill(RrFactura);

				result = !(Convert.IsDBNull(RrFactura.Tables[0].Rows[0]["MAXIMAFECHA"]));

				RrFactura.Close();

				return result;
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "mnu_esal01:HaPasadoFacturacionMensual", ex);


				return result;
			}
		}
		//'''''''''''''''''''''''''''

		public string FechaAltaEpisodio(int A�o, int Numero)
		{

			string result = String.Empty;
			result = "";

			string sql = "Select faltplan from  aepisadm ";
			sql = sql + "where ganoadme = " + A�o.ToString() + " and gnumadme = " + Numero.ToString() + " ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["faltplan"])) ? "" : Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm:ss");
			}

			RrSql.Close();
			return result;
		}


		//oscar
		private bool EstaCargada_menu_alta()
		{
			bool result = false;
			foreach (Form formu in Application.OpenForms)
			{
				if (formu.Name == "menu_alta")
				{
					result = true;
					break;
				}
			}
			return result;
		}


		public void RecogerEpisodio(int A�oRegistro, int NumeroRegistro, string FechaAlta, int iServicio, string fllegada)
		{
			//en este procedimiento recojo los datos que manda la pantalla de seleccionar episodio
			vstAnoadme = A�oRegistro;
			vstNumadme = NumeroRegistro;
		}


		private void Imprimir_pacientes_citas_externas()
		{
            //OSCAR C 16/09/2005: Se ha sacado a la dll ECitaExterna
            //'On Error GoTo Etiqueta_Error
            //'Me.MousePointer = 11
            //'Dim sql As String
            //'sql = "select " & _
            //''" acontcex.fcitaext, dpacient.dnombpac, dpacient.dape1pac, " & _
            //''" dpacient.dape2pac, acontcex.dnomcent, acontcex.ddirecen, " & _
            //''" dprovinc.dnomprov, dpoblaci.dpoblaci, acontcex.gcodipos, " & _
            //''" acontcex.inecambu , acontcex.inotifam, acontcex.fnotifam, " & _
            //''" acontcex.DNOMESPE , acontcex.iNECACOM " & _
            //''" From " & _
            //''" acontcex , aepisadm, dpacient, dprovinc, dpoblaci " & _
            //''" Where " & _
            //''" acontcex.ganoadme = aepisadm.ganoadme and " & _
            //''" acontcex.gnumadme = aepisadm.gnumadme and " & _
            //''" acontcex.gidenpac = aepisadm.gidenpac and " & _
            //''" (acontcex.fcitaext >= getdate() or " & _
            //''" acontcex.fcitaext is null ) and " & _
            //''" aepisadm.faltplan is null and " & _
            //''" acontcex.gidenpac = dpacient.gidenpac and " & _
            //''" acontcex.gprovinc *= dprovinc.gprovinc and " & _
            //''" acontcex.gpoblaci *= dpoblaci.gpoblaci " & _
            //''" Order By acontcex.fcitaext,dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac "
            //'If VstrCrystal.VfCabecera = False Then
            //'    proCabCrystal
            //'End If
            //''hay que cambiar el destino del informe y la ubicacion
            //'crystalgeneral.ReportFileName = App.Path & "\..\Elementoscomunes\rpt\AGI194R1.rpt"
            //'crystalgeneral.Formulas(0) = "FORM1=""" & VstrCrystal.VstGrupoHo & """"
            //'crystalgeneral.Formulas(1) = "FORM2=""" & VstrCrystal.VstNombreHo & """"
            //'crystalgeneral.Formulas(2) = "FORM3=""" & VstrCrystal.VstDireccHo & """"
            //'
            //''dv
            //'Dim LitPersona As String
            //'LitPersona = ObtenerLiteralPersona(RcAdmision)
            //''LitPersona = UCase(LitPersona)
            //'crystalgeneral.Formulas(3) = "LitPers=""" & LitPersona & """"
            //'
            //'Dim sTitulo As String
            //'sTitulo = "LISTADO DE " & UCase(LitPersona) & "S CON CITAS EXTERNAS"
            //'crystalgeneral.Formulas(4) = "TituloLitPers=""" & sTitulo & """"
            //'
            //''crystalgeneral.Formulas(3) = "Fdesde=""" & Me.IntervaloFechas1.FechaInicio & """"
            //''crystalgeneral.Formulas(4) = "Fhasta=""" & IntervaloFechas1.FechaFin & """"
            //'crystalgeneral.Destination = crptToWindow
            //'crystalgeneral.WindowState = crptMaximized
            //'crystalgeneral.WindowTitle = "Listado de pacientes con citas externas"
            //'crystalgeneral.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""
            //''crystalgeneral.WindowShowPrintSetupBtn = True
            //'crystalgeneral.SQLQuery = sql
            //'crystalgeneral.Action = 1
            //'crystalgeneral.Reset
            //'Me.MousePointer = 99
            //'Exit Sub
            //'Etiqueta_Error:
            //'    crystalgeneral.Reset
            //'    If Err.Source = "CRYSTALREPORT" Then
            //'        GestionErrorCrystal Err.Number, RcAdmision
            //'    Else
            //'    AnalizaError "admision", App.Path, VstCodUsua, "Menu_Admision:imprimir pacientes citas externas", RcAdmision
            //'    End If
            //'    Me.MousePointer = 99

            ECitaExterna.ECitasExternas OBJ = new ECitaExterna.ECitasExternas();
            OBJ.Imprimir_pacientes_citas_externas(crystalgeneral, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.VstCrystal, true, "AD");
        }

        private void Imprimir_Notificacion_permanencia_centro()
		{
            //'OSCAR C 19/09/2005: Se ha sacado a la dll Datosjuzgado
            //'On Error GoTo Etiqueta_Error
            //'Me.MousePointer = 11
            //'Dim sql As String
            //'Dim Rrsql As rdoResultset
            //'Dim Sql_Datos As String
            //'Dim RrSql_Datos As rdoResultset
            //'Dim sql_actualizar As String
            //'Dim Rrsql_actualizar As rdoResultset
            //'Dim stJuzgado As String
            //'Dim stDireccion As String
            //'Dim stNJuzgado As String
            //'Dim stNombre_Director As String
            //'Dim stNHospit As String
            //'Dim stDDireccion  As String
            //'Dim stpaciente As String
            //'Dim stPoblacion  As String
            //'Dim stProvincia As String
            //'Dim Fecha_ultimo_movimiento As String
            //'Dim stNumero As String
            //'Dim stA�o As String
            //'
            //'Dim stFechaHoy   As String
            //'Dim stFechaBusqueda As String
            //'Dim stFechaFormateada As String
            //'
            //''Busca el nombre del director
            //'sql = "select valfanu1  from sconsglo where gconsglo = 'ndnidire'"
            //'Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset)
            //'stNombre_Director = Trim(UCase(Rrsql("valfanu1")))
            //''Busca el nombre del hospital
            //'sql = "select valfanu1  from sconsglo where gconsglo = 'nombreho'"
            //'Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset)
            //'stNHospit = Trim(Rrsql("valfanu1"))
            //''busca la direcci�n del hospital
            //'sql = "select valfanu1,valfanu2   from sconsglo where gconsglo = 'direccho'"
            //'Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset)
            //'stDDireccion = Trim(Rrsql("valfanu1")) & " " & Trim(Rrsql("valfanu2"))
            //''
            //'sql = "select dnomprov From sconsglo , dprovinc Where sconsglo.gconsglo = 'CODPROVI' and " & _
            //''" substring(valfanu1,1,2)=gprovinc"
            //'Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset)
            //'stPoblacion = Trim(Rrsql("dnomprov"))
            //''antes de hacer la select hay que cargar fecha del dia menos 10 dias para que lo haga
            //''10 dias antes 15/10/1998
            //'
            //''tenemos que cargar la fecha del dia m�s 10 dias para que avise 10 dias antes de que suceda el evento
            //'stFechaHoy = Now
            //'stFechaBusqueda = DateAdd("d", Now, 10)
            //'stFechaBusqueda = stFechaBusqueda & " " & Format(stFechaHoy, "HH:NN:SS")
            //''despues hay que formatear la fecha
            //'
            //'ObtenerValoresTipoBD
            //'
            //''stFechaFormateada = FormatFecha(CDate(stFechaBusqueda))
            //'stFechaFormateada = (CDate(stFechaBusqueda))
            //'If InStr(InStr(stFechaFormateada, Separador_Hora_SQL) + 1, stFechaFormateada, Separador_Hora_SQL) = 0 Then
            //'    stFechaFormateada = Mid(stFechaFormateada, 1, 10)
            //'    stFechaFormateada = stFechaFormateada & " " & Format(stFechaHoy, "hh:nn:SS")
            //'Else
            //'    stFechaFormateada = Mid(stFechaFormateada, 1, InStr(InStr(stFechaFormateada, Separador_Hora_SQL) + 1, stFechaFormateada, Separador_Hora_SQL) - 1) & Separador_Hora_SQL & Format(stFechaHoy, "SS")
            //'End If
            //''en la select cambio el getdate() por '" & stFechaFormateada & "'
            //'
            //''***********fin 15/10/1998 **********
            //'sql = "select " & _
            //''" dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, " & _
            //''" dpacient.ndninifp , aepisadm.nnumexp1, aepisadm.gjuzgad1, " & _
            //''" aepisadm.ganoadme, aepisadm.gnumadme, aepisadm.gidenpac, " & _
            //''" aepisadm.fultnotif " & _
            //''" From aepisadm , dpacient Where " & _
            //''" aepisadm.faltplan is null and aepisadm.gjuzgad1 is not null and " & _
            //''" aepisadm.gidenpac = dpacient.gidenpac and " & _
            //''" ((aepisadm.fultnotif is null and aepisadm.fresolu1 is not null and " & _
            //''" " & FormatFechaHMS(stFechaFormateada) & " >= dateadd(mm, 6,aepisadm.fresolu1)) " & _
            //''" or " & _
            //''" (aepisadm.fultnotif is not null and " & _
            //''" " & FormatFechaHMS(stFechaFormateada) & " >= dateadd(mm, 6, aepisadm.fultnotif)))"
            //'Set Rrsql = RcAdmision.OpenResultset(sql, rdOpenKeyset, rdConcurValues)
            //'If Rrsql.RowCount <> 0 Then
            //'    Rrsql.MoveFirst
            //'    RcAdmision.BeginTrans
            //'    While Not Rrsql.EOF
            //'        Sql_Datos = "select * from djuzgado where gjuzgado = '" & Rrsql("gjuzgad1") & "'"
            //'        Set RrSql_Datos = RcAdmision.OpenResultset(Sql_Datos, rdOpenKeyset)
            //'        If Not RrSql_Datos.EOF Then
            //'            stJuzgado = Trim(RrSql_Datos("dnomjuzg")) & ""
            //'            stNJuzgado = Trim(RrSql_Datos("dnomnum")) & ""
            //'            stDireccion = Trim(RrSql_Datos("ddirjuzg")) & ""
            //'            stProvincia = Trim(RrSql_Datos("gcodipos")) & ""
            //'            If Not IsNull(RrSql_Datos("gprovinc")) Then
            //'                stProvincia = stProvincia & " " & probuscar_provincia(RrSql_Datos("gprovinc"))
            //'            Else
            //'                stProvincia = stProvincia & " "
            //'            End If
            //'        End If
            //'        RrSql_Datos.Close
            //'        crystalgeneral.Formulas(0) = "Juzgado= """ & stJuzgado & """"
            //'        crystalgeneral.Formulas(1) = "NJuzgado= """ & stNJuzgado & """"
            //'        crystalgeneral.Formulas(2) = "Direccion= """ & stDireccion & """"
            //'        crystalgeneral.Formulas(3) = "Provincia= """ & stProvincia & """"
            //'        crystalgeneral.Formulas(4) = "Nombre_Director= """ & stNombre_Director & """"
            //'        crystalgeneral.Formulas(5) = "NHospit= """ & stNHospit & """"
            //'        crystalgeneral.Formulas(6) = "DDireccion= """ & stDDireccion & """"
            //'        stpaciente = ""
            //'        If Not IsNull(Rrsql("dnombpac")) Then stpaciente = Trim(Rrsql("dnombpac"))
            //'        If Not IsNull(Rrsql("dape1pac")) Then stpaciente = stpaciente & " " & Trim(Rrsql("dape1pac"))
            //'        If Not IsNull(Rrsql("dape2pac")) Then stpaciente = stpaciente & " " & Trim(Rrsql("dape2pac"))
            //'        crystalgeneral.Formulas(7) = "Paciente= """ & stpaciente & """"
            //'        If Not IsNull(Rrsql("ndninifp")) Then crystalgeneral.Formulas(8) = "DNINIF= """ & Rrsql("ndninifp") & """"
            //'        crystalgeneral.Formulas(9) = "Nexpediente= """ & Rrsql("nnumexp1") & """"
            //'        crystalgeneral.Formulas(10) = "Poblacion= """ & stPoblacion & """"
            //'        crystalgeneral.ReportFileName = App.Path & "\..\Elementoscomunes\rpt\AGI174R1.rpt"
            //'        crystalgeneral.Destination = crptToPrinter
            //'        crystalgeneral.WindowState = crptMaximized
            //'        crystalgeneral.WindowTitle = "Notificaci�n de permanencia en el centro"
            //'        'Aqui se realiza la modificacion de cada registro
            //'        'en caso de que falle algo ni se actualiza el registro
            //'        'ni lanza el informe.
            //'        stA�o = Rrsql("ganoadme")
            //'        stNumero = Rrsql("gnumadme")
            //'        Fecha_ultimo_movimiento = Format(Rrsql("fultnotif"), "dd/mm/yyyy hh:nn")
            //'        Rrsql.Edit
            //'            Rrsql("fultnotif") = Format(Now, "dd/mm/yyyy hh:nn")
            //'        Rrsql.Update
            //'        crystalgeneral.CopiesToPrinter = 2
            //'        crystalgeneral.Action = 1
            //'        crystalgeneral.Reset
            //'        Rrsql.MoveNext
            //'    Wend
            //'    RcAdmision.CommitTrans
            //'Else
            //'    IResume = oClass.RespuestaMensaje(vNomAplicacion, vAyuda, 1520, _
            //''    RcAdmision, "pacientes", "notificacion de permanencia en el centro")
            //'End If
            //'Rrsql.Close
            //'Me.MousePointer = 99
            //'Exit Sub
            //'Etiqueta_Error:
            //'    RcAdmision.RollbackTrans
            //'    crystalgeneral.Reset
            //'    If UCase(Err.Source) = "CRYSTALREPORT" Then
            //'        GestionErrorCrystal Err.Number, RcAdmision
            //'    Else
            //'        AnalizaError "admision", App.Path, VstCodUsua, "Menu_Admision:imprimir notificacion permanencia centro", RcAdmision
            //'    End If
            //'    Me.MousePointer = 99
            //'Exit Sub

            DLLDatosJuzgado.clsDatos_juzgado OBJ = new DLLDatosJuzgado.clsDatos_juzgado();
            OBJ.Imprimir(crystalgeneral, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, "AGI174R1");
        }

        //UPGRADE_NOTE: (7001) The following declaration (probuscar_provincia) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        //private string probuscar_provincia(string Codigo)
        //{
        //string result = String.Empty;
        //string sql = String.Empty;
        //DataSet RrSql = null;
        //string provincia = String.Empty;
        //try
        //{
        //sql = "select dnomprov from dprovinc where gprovinc = '" + Codigo + "'";
        //SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
        //RrSql = new DataSet();
        //tempAdapter.Fill(RrSql);
        //if (RrSql.Tables[0].Rows.Count != 0)
        //{
        ////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
        ////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
        //if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnomprov"]))
        //{
        ////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
        //provincia = Convert.ToString(RrSql.Tables[0].Rows[0]["dnomprov"]).Trim();
        //}
        //else
        //{
        //provincia = "";
        //}
        //}
        //else
        //{
        //provincia = "";
        //}
        //result = provincia;
        ////UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
        //RrSql.Close();
        //return result;
        //}
        //catch
        //{
        //Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Menu_Admision:probuscar_provincia", Conexion_Base_datos.RcAdmision);
        //return result;
        //}
        //}

        public void ObtenerValoresTipoBD()
		{
			string formato = String.Empty;
			int i = 0;
			string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, Conexion_Base_datos.RcAdmision);
			DataSet tRr = new DataSet();
			tempAdapter.Fill(tRr);
			switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
			{
				case "SQL SERVER 6.5" : 
					Formato_Fecha_SQL = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
					formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Fecha_SQL == "")
						{
							Separador_Fecha_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Fecha_SQL != "")
						{
							break;
						}
					} 
					formato = formato.Substring(Strings.InStr(i + 1, formato, Separador_Fecha_SQL, CompareMethod.Binary)); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Hora_SQL == "")
						{
							Separador_Hora_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Hora_SQL != "")
						{
							break;
						}
					} 
					break;
			}
			tRr.Close();

		}

        private void Imprimir_CIAS()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string sql = String.Empty;
                string lCodSS = String.Empty;
                DataSet rrtemp = null;
                crystalgeneral.ReportFileName = Path.GetDirectoryName(Application.ExecutablePath) + "\\Elementoscomunes\\rpt\\agi196r1_CR11.rpt";

                sql = " select valfanu1,nnumeri1 from sconsglo where gconsglo= 'segursoc' ";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter.Fill(rrtemp);
                lCodSS = Convert.ToString(rrtemp.Tables[0].Rows[0]["nnumeri1"]).Trim();
                rrtemp.Close();

                sql = "select AEPISADM.fllegada, DENTIPAC.nafiliac, DENTIPAC.gcodcias," +
                        "DCODCIASVA.gcodcias, DCODCIASVA.dnombmed, DPACIENT.dnombpac," +
                        "DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.fnacipac From " +
                      " aepisadm  " +
                      " INNER JOIN dpacient  on aepisadm.gidenpac = dpacient.gidenpac   " +
                      " INNER JOIN dentipac on aepisadm.gidenpac = dentipac.gidenpac  " +
                      " LEFT JOIN dcodciasva on dentipac.gcodcias = dcodciasva.gcodcias   " +
                      " Where " +
                      " aepisadm.faltplan IS NULL And" +
                      " dentipac.gsocieda ='" + lCodSS + "'  " + "\n" + "\r" +
                      " Order By dentipac.gcodcias,dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac";

                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //hay que cambiar el destino del informe y la ubicacion
                crystalgeneral.Formulas["FORM1"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                crystalgeneral.Formulas["FORM2"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                crystalgeneral.Formulas["FORM3"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                //crystalgeneral.Formulas(3) = "Fdesde=""" & Me.IntervaloFechas1.FechaInicio & """"
                //crystalgeneral.Formulas(4) = "Fhasta=""" & IntervaloFechas1.FechaFin & """"
                //crystalgeneral.Destination = Crystal.DestinationConstants.crptToWindow;
                //crystalgeneral.WindowState = Crystal.WindowStateConstants.crptMaximized;
                crystalgeneral.WindowTitle = "Listado de pacientes ingresados por CIAS";
                crystalgeneral.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + "; " + Conexion_Base_datos.gContrase�a + ";";


                //crystalgeneral.WindowShowPrintSetupBtn = True
                crystalgeneral.SQLQuery = sql;
                crystalgeneral.Action = 1;
                crystalgeneral.Reset();

                crystalgeneral.PrinterStopPage = crystalgeneral.PageCount;

                Conexion_Base_datos.vacia_formulas(crystalgeneral, 2);
                this.Cursor = Cursors.Hand;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                crystalgeneral.Reset();
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Menu_Admision:imprimir CIAS", ex);
            }
        }


        private void tmrPrincipal_Tick(Object eventSender, EventArgs eventArgs)
		{
			// Si Principal no est� activo, habr� que mostrar un error y salir de todos los procesos

			if (!mbProcesos.estaPrincipalActivo())
			{
				mbProcesos.proCerrarModulos(Conexion_Base_datos.RcAdmision, "Admision.exe");
			}

		}

		//OSCAR C MAYO 2008
		//Funcion public para refrescar los pacientes ingresados en funcion de las pantallas que
		//esten cargadas en pantalla cuando se realizan los ingresos o las altas masivas desde la nueva
		//dll de pacientes en tratamiento de hospital de dia.
		public object RefrescaPacientes()
		{
			foreach (Form Form in Application.OpenForms)
			{
				if (Form.Name.Trim().ToUpper() == "MENU_INGRESO")
				{
                    menu_ingreso.DefInstance.cbRefrescar.PerformClick();
				}
				if (Form.Name.Trim().ToUpper() == "MENU_ALTA")
				{
                    menu_alta.DefInstance.cbRefrescar.PerformClick();
				}
			}
			return null;
		}
		//-------

		private void LlamaCitasExternas()
		{
            string stSql = String.Empty;
            DataSet rdoTemp = null;
            int intAnio = 0;
            int lngRegi = 0;

            vstIdPaciente = "";
            
            filiacionDLL.Filiacion clsinstancia = new filiacionDLL.Filiacion();            
            clsinstancia.Load(clsinstancia, this, "SELECCION", "Admision", Conexion_Base_datos.RcAdmision);
            clsinstancia = null;

            if (vstIdPaciente != "")
            {
                stSql = "SELECT ganoadme, gnumadme " +
                        "FROM AEPISADM WHERE gidenpac = '" + vstIdPaciente + "' and faltplan is null";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);
                if (rdoTemp.Tables[0].Rows.Count != 0)
                {
                    intAnio = Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["ganoadme"]);
                    lngRegi = Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["gnumadme"]);

                    ECitaExterna.ECitasExternas clsinstanci = new ECitaExterna.ECitasExternas();
                    clsinstanci.proload(vstIdPaciente, intAnio, lngRegi, crystalgeneral, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.VstCrystal);
                    clsinstanci = null;
                }
                else
                {
                    RadMessageBox.Show("El paciente no se encuentra ingresado.", "Consultas Externas", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        public bool FormularioActivo(string stFormulario)
		{
			bool result = false;
			foreach (Form Formulario in Application.OpenForms)
			{
				if (Formulario.Name.ToUpper() == stFormulario.ToUpper())
				{
					result = true;
					break;
				}
			}
			return result;
		}

		[STAThread]
		static void Main()
		{
			Application.Run(new menu_admision());
		}

        private void menu_admision_Deactivate(object sender, EventArgs e)
        {
            MainMenu1.Enabled = false;
        }
    }
}