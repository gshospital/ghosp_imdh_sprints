using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace ADMISION
{
	public partial class ConversionHaHD
		: RadForm
	{
		int mGanoadme = 0;
		int mGnumadme = 0;
		string mGidenpac = String.Empty; // String con c�digo del paciente

		bool fCama = false;
		bool fCambiaCama = false;
		int inPlanta = 0;
		int inHabitac = 0;
		string stnCama = String.Empty;
		string StRegaloj = String.Empty;
		string stTipoIng = String.Empty;
		string stSexo = String.Empty;
		string stProc = String.Empty;
		bool Ferror_histo = false;
		public ConversionHaHD()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public void proRecibeEpisodio(int ParaAno, int ParaNum)
		{
			mGanoadme = ParaAno;
			mGnumadme = ParaNum;
		}


        public int getQueryTimeout(SqlConnection instance)
        {
            int queryTimeout = 30;
            string tstTimeout = "select nnumeri1 from sconsglo where gconsglo='TTIMEOUT'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstTimeout, instance);
            DataSet tRrTimeout = new DataSet();
            tempAdapter.Fill(tRrTimeout);
            if (tRrTimeout.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToDouble(tRrTimeout.Tables[0].Rows[0]["nnumeri1"]) >= 30)
                {
                    //Nombre_Conexion.setQueryTimeout(Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]));
                    queryTimeout = Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]);
                }
                else
                {
                    //Nombre_Conexion.setQueryTimeout(30);
                    queryTimeout = 30;
                }
            }
            else
            {
                //Nombre_Conexion.setQueryTimeout(30);
                queryTimeout = 30;
            }
            tRrTimeout.Close();
            return queryTimeout;
        }

		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string sqlAux = String.Empty;
			DataSet RRAux = null;
			int iTimeOut = 0;
			int gNewNumRegi = 0;
			int gNewAnoRegi = 0;
			try
			{				
				iTimeOut = getQueryTimeout(Conexion_Base_datos.RcAdmision);

				//Se realizar�n las siguientes validaciones:
				//-   El a�o y n�mero de episodio proporcionados corresponden a un episodio de hospitalizaci�n existente y dado de alta.
				sqlAux = "select * from aepisadm where ganoadme=" + mGanoadme.ToString() + " and gnumadme=" + mGnumadme.ToString() + " and faltplan is not null and ganoadme>1900";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
				RRAux = new DataSet();
				tempAdapter.Fill(RRAux);
				if (RRAux.Tables[0].Rows.Count == 0)
				{
					RadMessageBox.Show("El paciente no ha sido dado de alta todav�a", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
					return;
				}
				
				RRAux.Close();

				//-   El episodio proporcionado no est� facturado.
				sqlAux = "select * from dmovfacu where " + " ganoregi=" + mGanoadme.ToString() + " and gnumregi=" + mGnumadme.ToString() + " and itiposer = 'H' and ffactura is not null";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
				RRAux = new DataSet();
				tempAdapter_2.Fill(RRAux);
				if (RRAux.Tables[0].Rows.Count > 0)
				{					
					short tempRefParam = 1420;
                    string[] tempRefParam2 = new string[] { "El Episodio", "Facturado" };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					return;
				}
				
				RRAux.Close();

				sqlAux = "select * from fconspac where " + " ganoregi=" + mGanoadme.ToString() + " and gnumregi=" + mGnumadme.ToString() + " and itiposer = 'H' and ffactura is not null";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
				RRAux = new DataSet();
				tempAdapter_3.Fill(RRAux);
				if (RRAux.Tables[0].Rows.Count > 0)
				{					
					short tempRefParam3 = 1420;
                    string[] tempRefParam4 = new string[] { "El Episodio", "Facturado" };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
					return;
				}				
				RRAux.Close();

				//' O.Frias 06/04/2015
				//' Obligar la introduci�n de la cama segun constante.
				sqlAux = "select ISNULL(valfanu1, 'N') as valfanu1 from SCONSGLO where gconsglo = 'OBLCMHHD'";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
				RRAux = new DataSet();
				tempAdapter_4.Fill(RRAux);
				if (RRAux.Tables[0].Rows.Count > 0)
				{					
					if (Convert.ToString(RRAux.Tables[0].Rows[0]["valfanu1"]).Trim() == "S" && cbbPlazaDia.SelectedIndex == -1)
					{						
						short tempRefParam5 = 1040;
                        string[] tempRefParam6 = new string[] { Label8.Text };
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
						return;
					}
				}				
				RRAux.Close();

				//-   El c�digo de usuario proporcionado existe.

				//-   En caso de proporcionar cama (par�metro @camaHD) se comprueba que corresponde a una cama existente y con indicador de plaza de d�a (tipo de cama con indicador isillon a S).
				//    Si no se proporciona valor tomar� la primera cama con indicador de plaza de d�a a "S" existente.
				if (tbCama.Text.Trim() != "")
				{
					sqlAux = "select * from DCAMASBO, DTIPOCAM " + "WHERE GPLANTAS = " + inPlanta.ToString() + " AND GHABITAC = " + inHabitac.ToString() + " AND GCAMASBO = '" + stnCama + "'" + " AND DCAMASBO.gtipocam = DTIPOCAM.gtipocam " + " AND DTIPOCAM.isillon ='S'";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
					RRAux = new DataSet();
					tempAdapter_5.Fill(RRAux);
					if (RRAux.Tables[0].Rows.Count == 0)
					{
						//la cama no existe o no esta libre o es una plaza de dia						
						short tempRefParam7 = 1320;
                        string[] tempRefParam8 = new string[] { "inexistente o No Plaza de D�a" };
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
						return;
					}
					else
					{
						//la cama existe
						if (!fnComprobar_cama())
						{
							return;
						}
					}					
					RRAux.Close();
				}
				//-   Si no se proporciona tipo de ingreso (par�metro @TipoIngreso) o el valor proporcionado es distinto I, U o P se tomar� por defecto I.
				//-   Si no se proporciona tipo de plaza (par�metro @TipoPlaza) o el valor proporcionado es distinto de Q, M o D se tomar� por defecto M.

				if (rbInmediato.IsChecked)
				{
					stTipoIng = "I";
				}
				else if (rbUrgente.IsChecked)
				{ 
					stTipoIng = "U";
				}
				else if (rbProgramado.IsChecked)
				{ 
					stTipoIng = "P";
				}

				this.Cursor = Cursors.WaitCursor;

				lbInfo.Text = "PROCESANDO....";
				lbInfo.Visible = true;
				this.Refresh();
				Application.DoEvents();

				//4. Ejecutamos el procedimiento almacenado que realiza en si la conversion.
				SqlCommand rdoQProceso = new SqlCommand();
                rdoQProceso = Conexion_Base_datos.RcAdmision.CreateQuery("Llamada", "CONVERSION_H_TO_HD");
                rdoQProceso.CommandType = CommandType.StoredProcedure;
                rdoQProceso.CommandTimeout = iTimeOut;
                
                rdoQProceso.Parameters.Add("@ganoregiH", SqlDbType.SmallInt).Size = 20;
                rdoQProceso.Parameters.Add("@gnumregiH", SqlDbType.Int).Size = 20;
                rdoQProceso.Parameters.Add("@camaHD", SqlDbType.VarChar).Size = 20;
                rdoQProceso.Parameters.Add("@TipoIngreso", SqlDbType.VarChar).Size = 20;
                rdoQProceso.Parameters.Add("@TipoPlaza", SqlDbType.VarChar).Size = 20;
                rdoQProceso.Parameters.Add("@Usuario", SqlDbType.VarChar).Size = 100;
                rdoQProceso.Parameters.Add("@derror", SqlDbType.VarChar).Size = 100;
                rdoQProceso.Parameters.Add("@ganoregiHD", SqlDbType.VarChar).Size = 100;
                rdoQProceso.Parameters.Add("@gnumregiHD", SqlDbType.VarChar).Size = 100;

                rdoQProceso.Parameters[0].Direction = ParameterDirection.Input;
				rdoQProceso.Parameters[1].Direction = ParameterDirection.Input;
				rdoQProceso.Parameters[2].Direction = ParameterDirection.Input;
				rdoQProceso.Parameters[3].Direction = ParameterDirection.Input;
				rdoQProceso.Parameters[4].Direction = ParameterDirection.Input;
				rdoQProceso.Parameters[5].Direction = ParameterDirection.Input;
				rdoQProceso.Parameters[6].Direction = ParameterDirection.Output;
				rdoQProceso.Parameters[7].Direction = ParameterDirection.Output;
				rdoQProceso.Parameters[8].Direction = ParameterDirection.Output;


				rdoQProceso.Parameters[0].Value = mGanoadme;
				rdoQProceso.Parameters[1].Value = mGnumadme;

				if (tbCama.Text.Trim() != "")				
					rdoQProceso.Parameters[2].Value = tbCama.Text.Trim();
				
				if (stTipoIng.Trim() != "")				
					rdoQProceso.Parameters[3].Value = stTipoIng;
				
				if (cbbPlazaDia.SelectedIndex >= 0)				                    
                    rdoQProceso.Parameters[4].Value = cbbPlazaDia.SelectedItem.Value;
                                
				rdoQProceso.Parameters[5].Value = Conexion_Base_datos.VstCodUsua;
				rdoQProceso.ExecuteNonQuery();				
				
				this.Refresh();
				Application.DoEvents();
				this.Cursor = Cursors.Default;

				if ((Convert.ToString(rdoQProceso.Parameters[6].Value) + "").Trim() != "")
				{
					lbInfo.BackColor = Color.FromArgb(255, 128, 128);
					lbInfo.Text = Convert.ToString(rdoQProceso.Parameters[6].Value);
				}
				else
				{
					lbInfo.BackColor = Color.FromArgb(128, 255, 128);
					lbInfo.Text = "EPISODIO CONVERTIDO";
					CbAceptar.Enabled = false;
				}
				//Variables con el nuevo numero del Episodio de Hospital de Dia, por si se quere hacer algo con el.
				gNewAnoRegi = Convert.ToInt32(rdoQProceso.Parameters[7].Value);
				gNewNumRegi = Convert.ToInt32(rdoQProceso.Parameters[8].Value);
				this.Refresh();
				Application.DoEvents();
			}
			catch (Exception ex)
			{                
				this.Cursor = Cursors.Default;
				this.Refresh();
				Application.DoEvents();
				RadMessageBox.Show("Se ha producido un error Cr�tico: " + ex.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void CbSeleccion_Click(Object eventSender, EventArgs eventArgs)
		{
			mantecamas.manteclass Sel_cama = new mantecamas.manteclass(); //Para crear una instancia de la ventana de selecci�n de camas
			object tempRefParam = this;
			string tempRefParam2 = "H";
			string tempRefParam3 = "I";
			bool tempRefParam4 = false;
			string tempRefParam5 = "C";
			bool tempRefParam6 = false;
			Sel_cama.proload(Sel_cama, tempRefParam, ref Conexion_Base_datos.RcAdmision, tempRefParam2, Conexion_Base_datos.VstCodUsua, tempRefParam3, stSexo, tempRefParam4, tempRefParam5, tempRefParam6);
			Sel_cama = null;
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void ConversionHaHD_Load(Object eventSender, EventArgs eventArgs)
		{
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);

			string stSql = "Select * from ATIPLAZA order by 2";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
			DataSet Rr = new DataSet();
			tempAdapter.Fill(Rr);

            foreach (DataRow iteration_row in Rr.Tables[0].Rows)
            {
                if (Convert.ToString(iteration_row["dtiplaza"]) != "")                
                    cbbPlazaDia.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dtiplaza"]).Trim(), Convert.ToString(iteration_row["gtiplaza"]).Trim()));                
            }                        
            Rr.Close();            
            cbbPlazaDia.SelectedIndex = -1;
        }

		private void ConversionHaHD_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				string stSql = "SELECT " + " DPACIENT.DAPE1PAC, DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC, DPACIENT.ITIPSEXO, DPACIENT.IFILPROV," + " HDOSSIER.GHISTORIA," + " AEPISADM.gidenpac, AEPISADM.GSERULTI, DSERVICI.DNOMSERV," + " AEPISADM.GDIAGINI, AEPISADM.ODIAGINI, DDIAGNOS.DNOMBDIA," + " AEPISADM.GPERULTI, DPERSONA.DAP1PERS,DPERSONA.DAP2PERS, DPERSONA.DNOMPERS, " + " AEPISADM.FLLEGADA, AEPISADM.FALTPLAN, AEPISADM.GTIPLAZA," + " AEPISADM.GANOADME, AEPISADM.GNUMADME, AEPISADM.ITIPINGR" + " FROM AEPISADM " + "  INNER JOIN DPACIENT ON DPACIENT.GIDENPAC = AEPISADM.GIDENPAC " + "  INNER JOIN DPERSONA ON DPERSONA.GPERSONA = AEPISADM.GPERULTI " + "  INNER JOIN DSERVICI ON DSERVICI.GSERVICI = AEPISADM.GSERULTI " + "  LEFT JOIN HDOSSIER ON AEPISADM.GIDENPAC = HDOSSIER.GIDENPAC AND HDOSSIER.icontenido='H' " + "  LEFT JOIN DDIAGNOS ON DDIAGNOS.GCODIDIA = AEPISADM.GDIAGINI AND ( AEPISADM.FLLEGADA BETWEEN FINVALDI AND FFIVALDI OR ( AEPISADM.FLLEGADA >= FINVALDI and ffivaldi IS  NULL))";
				stSql = stSql + " WHERE " + " AEPISADM.ganoadme= " + mGanoadme.ToString() + " AND gnumadme=" + mGnumadme.ToString();
				//& " AND AEPISADM.FALTPLAN IS NOT NULL"

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
				DataSet RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{					
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["faltplan"]))
					{
						RadMessageBox.Show("El Paciente no ha sido dado de alta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
						CbAceptar.Enabled = false;
						this.Close();
					}
					else
					{						
						lbNombrePaciente.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAPE1PAC"]) + "").Trim() + " " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAPE2PAC"]) + "").Trim() + ", " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMBPAC"]) + "").Trim();
						
						lbIdEpisodio.Text = Convert.ToString(RrDatos.Tables[0].Rows[0]["ganoadme"]) + " / " + Convert.ToString(RrDatos.Tables[0].Rows[0]["gnumadme"]);
						
						switch(Convert.ToString(RrDatos.Tables[0].Rows[0]["ITIPSEXO"]))
						{
							case "H" : 
								RbHombre.IsChecked = true; 
								RbMujer.IsChecked = false; 
								stSexo = "H"; 
								break;
							case "M" : 
								RbHombre.IsChecked = false; 
								RbMujer.IsChecked = true; 
								stSexo = "M"; 
								break;
							default:
								RbHombre.IsChecked = false; 
								RbMujer.IsChecked = false; 
								stSexo = "I"; 
								break;
						}
						
						lbHistoria.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["ghistoria"]) + "").Trim();
						
						lbServicio.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMSERV"]) + "").Trim();
						
						if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["GDIAGINI"]))
						{							
							lbDiagnostico.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMBDIA"]) + "").Trim();
						}
						else if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ODIAGINI"]))
						{ 							
							lbDiagnostico.Text = Convert.ToString(RrDatos.Tables[0].Rows[0]["ODIAGINI"]) + "";
						}
						
						lbResponsable.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAP1PERS"]) + "").Trim() + " " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAP2PERS"]) + "").Trim() + ", " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMPERS"]) + "").Trim();
						
						lbFechaIngreso.Text = Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm");
						
						lbFechaAlta.Text = Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm");
						
						switch(Convert.ToString(RrDatos.Tables[0].Rows[0]["ITIPINGR"]) + "")
						{
							case "U" : 
								rbUrgente.IsChecked = true; 
								stTipoIng = "U"; 
								break;
							case "P" : 
								rbProgramado.IsChecked = true; 
								stTipoIng = "P"; 
								break;
							default:
								rbInmediato.IsChecked = true; 
								stTipoIng = "I"; 
								break;
						}
						StRegaloj = "C";
					}
				}				
				RrDatos.Close();
			}
		}

		private void tbCama_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			//si cambia la cama desde la ventana de camas se valida
			if (fCama)
			{
				fCambiaCama = true;
				inPlanta = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length))));
				inHabitac = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2))));
				stnCama = tbCama.Text.Substring(4).Trim();
			}
		}

		private void tbCama_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			fCambiaCama = true;
			fCama = false; //la cama es tecleada
		}

		private void tbCama_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			fCambiaCama = true;
			fCama = false; //la cama es tecleada
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbCama_Leave(Object eventSender, EventArgs eventArgs)
		{
			try
			{
				if (tbCama.Text != "")
				{
					fCama = false;
					tbCama.Text = tbCama.Text.ToUpper();
					inPlanta = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length))));
					inHabitac = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2))));
					stnCama = tbCama.Text.Substring(4).Trim();
				}
			}
			//******
			catch (InvalidCastException e)
			{				
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{"cama no v�lida"};
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				tbCama.Focus();
				return;
			}
		}

		public void proRecojo_Cama(string AcepCan, string Cama)
		{
			string sql = String.Empty;
			DataSet Rr = null;
			if (AcepCan == "A")
			{
				fCama = true;
				tbCama.Text = Cama;
				// como al asignarle la cama que ha seleccionado Mantecamas no se dispara el
				// el change de la caja de texto TbCama -> lo simulo

				//Solo se permiten elegir tipos de cama con indicador de sillon a 'S'
				sql = "SELECT isillon FROM" + " DCAMASBOva ,DTIPOCAM " + " WHERE " + " DCAMASBOVA.gtipocam = DTIPOCAM.gtipocam AND " + " DCAMASBOva.GPLANTAS = " + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)))).ToString() + " AND " + " DCAMASBOva.GHABITAC = " + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)))).ToString() + " AND " + " DCAMASBOva.gcamasbo ='" + tbCama.Text.Substring(4).Trim() + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
				Rr = new DataSet();
				tempAdapter.Fill(Rr);
				if (Rr.Tables[0].Rows.Count != 0)
				{					
					if (Convert.ToString(Rr.Tables[0].Rows[0]["isillon"]).Trim().ToUpper() != "S")
					{
						fCama = false;
						tbCama.Text = "";
						RadMessageBox.Show("Debe seleccionar una Plaza de D�a", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
					}
				}

				tbCama_TextChanged(tbCama, new EventArgs());
			}
			else if (AcepCan == "C")
			{ 
				fCama = false;
			}
		}

		public void proRespuestaCama(string stCamas)
		{
			if (stCamas != "")
			{
				fCama = true;
			}
		}

		public bool fnComprobar_cama()
		{
			//'DE MOMENTO NO SE QUIERE NINGUNA VALIDACION SOBRE LA CAMA NADA MAS QUE SEA DE PLAZA DE DIA
			return true;
		}

		private void ConversionHaHD_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
            DefInstance.Dispose();
		}
	}
}