using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Cambio_producto
	{

		#region "Upgrade Support "
		private static Cambio_producto m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Cambio_producto DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Cambio_producto();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprProductos", "CbEliminar", "Label28", "LbPaciente", "Frame2", "CbbServicioA", "TbServicioDe", "Label1", "Label2", "Frame1", "tbHoraCambio", "SdcFecha", "Label21", "Label19", "Frame3", "CbCancelar", "CbAceptar", "SprProductos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprProductos;
		public Telerik.WinControls.UI.RadButton CbEliminar;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
        public Telerik.WinControls.UI.RadDropDownList CbbServicioA;
		public Telerik.WinControls.UI.RadTextBoxControl TbServicioDe;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraCambio;
        public Telerik.WinControls.UI.RadDateTimePicker SdcFecha;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		//private FarPoint.Win.Spread.SheetView SprProductos_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cambio_producto));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.SprProductos = new UpgradeHelpers.Spread.FpSpread();
			this.CbEliminar = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbbServicioA = new Telerik.WinControls.UI.RadDropDownList();
			this.TbServicioDe = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.tbHoraCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label21 = new Telerik.WinControls.UI.RadLabel();
			this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame2.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// SprProductos
			// 
			this.SprProductos.Location = new System.Drawing.Point(6, 44);
			this.SprProductos.Name = "SprProductos";
			this.SprProductos.Size = new System.Drawing.Size(466, 97);
			this.SprProductos.TabIndex = 0;
			this.SprProductos.TabStop = false;
            this.SprProductos.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(SprProductos_CellClick);
			// 
			// CbEliminar
			// 
			this.CbEliminar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbEliminar.Location = new System.Drawing.Point(484, 46);
			this.CbEliminar.Name = "CbEliminar";
			this.CbEliminar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbEliminar.Size = new System.Drawing.Size(81, 29);
			this.CbEliminar.TabIndex = 16;
			this.CbEliminar.Text = "&Eliminar";
			this.CbEliminar.Click += new System.EventHandler(this.CbEliminar_Click);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Controls.Add(this.LbPaciente);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(4, 0);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(568, 39);
			this.Frame2.TabIndex = 13;
			this.Frame2.Visible = true;
			// 
			// Label28
			// 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(14, 14);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(51, 17);
			this.Label28.TabIndex = 15;
			this.Label28.Text = "Paciente:";
            // 
            // LbPaciente
            // 
            this.LbPaciente.Enabled = false;
			this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbPaciente.Location = new System.Drawing.Point(78, 12);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(478, 19);
			this.LbPaciente.TabIndex = 14;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.Frame1);
			this.Frame3.Controls.Add(this.tbHoraCambio);
			this.Frame3.Controls.Add(this.SdcFecha);
			this.Frame3.Controls.Add(this.Label21);
			this.Frame3.Controls.Add(this.Label19);
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(0, 145);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(570, 97);
			this.Frame3.TabIndex = 3;
			this.Frame3.Visible = true;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.CbbServicioA);
			this.Frame1.Controls.Add(this.TbServicioDe);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(8, 40);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(553, 49);
			this.Frame1.TabIndex = 4;
			this.Frame1.Text = "Producto";
			this.Frame1.Visible = true;
			// 
			// CbbServicioA
			// 
			this.CbbServicioA.CausesValidation = true;
			this.CbbServicioA.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbServicioA.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.CbbServicioA.Enabled = true;
			this.CbbServicioA.Location = new System.Drawing.Point(312, 22);
			this.CbbServicioA.Name = "CbbServicioA";
			this.CbbServicioA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbbServicioA.Size = new System.Drawing.Size(237, 21);
			this.CbbServicioA.TabIndex = 6;
			this.CbbServicioA.TabStop = true;
			this.CbbServicioA.Visible = true;
			this.CbbServicioA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbServicioA_KeyPress);
            this.CbbServicioA.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbServicioA_SelectedIndexChanged);
			// 
			// TbServicioDe
			// 
			this.TbServicioDe.AcceptsReturn = true;
			this.TbServicioDe.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.TbServicioDe.Enabled = false;
			this.TbServicioDe.Location = new System.Drawing.Point(32, 23);
			this.TbServicioDe.MaxLength = 0;
			this.TbServicioDe.Name = "TbServicioDe";
			this.TbServicioDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TbServicioDe.Size = new System.Drawing.Size(253, 19);
			this.TbServicioDe.TabIndex = 5;
			this.TbServicioDe.TabStop = false;
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(8, 26);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(17, 17);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "De:";
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Location = new System.Drawing.Point(296, 26);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(17, 17);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "A:";
			// 
			// tbHoraCambio
			// 
            this.tbHoraCambio.MaskType = Telerik.WinControls.UI.MaskType.Standard;
			this.tbHoraCambio.AllowPromptAsInput = false;
			this.tbHoraCambio.Location = new System.Drawing.Point(232, 16);
			this.tbHoraCambio.Mask = "99:99";
			this.tbHoraCambio.Name = "tbHoraCambio";
			this.tbHoraCambio.PromptChar = ' ';
			this.tbHoraCambio.Size = new System.Drawing.Size(45, 20);
			this.tbHoraCambio.TabIndex = 9;
			this.tbHoraCambio.Leave += new System.EventHandler(this.tbHoraCambio_Leave);
			// 
			// SdcFecha
			// 
			this.SdcFecha.CustomFormat = "dd/MM/yyyy";
			this.SdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFecha.Location = new System.Drawing.Point(48, 16);
			this.SdcFecha.Name = "SdcFecha";
			this.SdcFecha.Size = new System.Drawing.Size(129, 17);
			this.SdcFecha.TabIndex = 10;
			// 
			// Label21
			// 
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label21.Location = new System.Drawing.Point(8, 16);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(75, 17);
			this.Label21.TabIndex = 12;
			this.Label21.Text = "Fecha:";
			// 
			// Label19
			// 
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label19.Location = new System.Drawing.Point(192, 16);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(33, 17);
			this.Label19.TabIndex = 11;
			this.Label19.Text = "Hora:";
			// 
			// CbCancelar
			// 
			this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCancelar.Location = new System.Drawing.Point(487, 249);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 29);
			this.CbCancelar.TabIndex = 2;
			this.CbCancelar.Text = "&Cancelar";
			this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
			// 
			// CbAceptar
			// 
			this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbAceptar.Location = new System.Drawing.Point(399, 249);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 29);
			this.CbAceptar.TabIndex = 1;
			this.CbAceptar.Text = "&Aceptar";
			this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
			// 
			// Cambio_producto
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(575, 285);
			this.Controls.Add(this.SprProductos);
			this.Controls.Add(this.CbEliminar);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.CbCancelar);
			this.Controls.Add(this.CbAceptar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Cambio_producto";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Cambio de Producto - AGI153F1";
			this.Activated += new System.EventHandler(this.Cambio_producto_Activated);
			this.Closed += new System.EventHandler(this.Cambio_producto_Closed);
			this.Load += new System.EventHandler(this.Cambio_producto_Load);
			this.Frame2.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}