using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using CrystalWrapper;

namespace ADMISION
{
    public partial class Morbilidad_hospitalaria
        : RadForm
    {

        public Morbilidad_hospitalaria()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
            ReLoadForm(false);
        }


        private void Morbilidad_hospitalaria_Activated(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
            {
                UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form)eventSender;
            }
        }

        //*************************************************************
        //CRIS

        //Esta estructura me sirve para guardar los datos que posteriormente
        //ir�n en un fichero de texto
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct TyDisco
        {
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 8)]
            public string StCodHospi; //Provincia&municipio&Nhospital
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 5)]
            public string StNumEntrada; //numeroentrada
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 8)]
            public string StFechaIngre; //fecha de ingreso
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 5)]
            public string StNumSalida; //numerosalida
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 8)]
            public string StFechaNaci; //fecha de nacimiento
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string stSexo; //sexo
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string StEstadoCivil; //estadocivil
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 2)]
            public string StCodProvin; //residenciahabitual
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string StTipEntrada; //diagnostico entrada
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string StMotIngre; //motivo de ingreso
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 8)]
            public string StFechaAlta; //fecha de alta
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string StTipCaso; //caso
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 4)]
            public string StDiagIngre; //mid(codigo cie,1,3)& mid(codigo cie,5,1)
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string StClasifi; //clasificacion basica
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 1)]
            public string stMotAlta; //motivo de alta
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 60)]
            public string StDiagnos; //literal del diagnostico
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 9)]
            public string StNumeroHistoria; //blancos en el numero de historia
            public static TyDisco CreateInstance()
            {
                TyDisco result = new TyDisco();
                result.StCodHospi = new string('\0', 8);
                result.StNumEntrada = new string('\0', 5);
                result.StFechaIngre = new string('\0', 8);
                result.StNumSalida = new string('\0', 5);
                result.StFechaNaci = new string('\0', 8);
                result.stSexo = new string('\0', 1);
                result.StEstadoCivil = new string('\0', 1);
                result.StCodProvin = new string('\0', 2);
                result.StTipEntrada = new string('\0', 1);
                result.StMotIngre = new string('\0', 1);
                result.StFechaAlta = new string('\0', 8);
                result.StTipCaso = new string('\0', 1);
                result.StDiagIngre = new string('\0', 4);
                result.StClasifi = new string('\0', 1);
                result.stMotAlta = new string('\0', 1);
                result.StDiagnos = new string('\0', 60);
                result.StNumeroHistoria = new string('\0', 9);
                return result;
            }
        }
        Morbilidad_hospitalaria.TyDisco[] AFichero = null;
        string StHOSPI = String.Empty;
        string StHOMBRE = String.Empty;
        string StIniHOMBRE = String.Empty;
        string StINDET = String.Empty;
        string StIniINDET = String.Empty;
        string StMUJER = String.Empty;
        string StIniMUJER = String.Empty;
        int Itotal = 0;
        DataSet Rrs = null;
        string StComando = String.Empty;

        //*************************************************************
        string stMensaje1 = String.Empty;
        string stMensaje2 = String.Empty;
        string stMensaje3 = String.Empty;
        string stIntervalo = String.Empty;
        string stFechaini = String.Empty;
        string stFechafin = String.Empty;
        string vSqlc = String.Empty;
        string stTerminado = String.Empty;
        string stConexion = String.Empty;
        string stInforme = String.Empty;

        string stFormato = String.Empty;

        int viform = 0;

        //UPGRADE_ISSUE: (2068) Workspace object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //Workspace Wk = null;
        //UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //Database DbInforme = null;
        bool ttemp = false;
        public Crystal cryInformes = null;

        private void CodigoHospital()
        {
            //****************CRIS***************************************
            //Saco el c�digo del hospital de la tabla de constantes globales

            StComando = "Select isNull(VALFANU3,'') valfanu3 from SCONSGLO ";
            StComando = StComando + "where GCONSGLO='CODHOSPI'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, Conexion_Base_datos.RcAdmision);
            Rrs = new DataSet();
            tempAdapter.Fill(Rrs);
            StHOSPI = Convert.ToString(Rrs.Tables[0].Rows[0]["VALFANU3"]).Trim();
            Rrs.Close();

        }

        private void GenerarDisco()
        {
            StringBuilder stFICHERO = new StringBuilder();
            //****************CRIS***************************************

            CodigoHospital();
            ObtenerSexo();
            ObtenerDatos();


            //' Establecer CancelError a True
            //CommonDialog1.CancelError = True

            //establecemos la extension por defecto
            CommonDialog1Save.DefaultExt = ".txt";
            // Establecer los indicadores cdlOFNOverwritePrompt,cdlOFNHideReadOnly,
            //cdlOFNLongNames

            //Establecer el directorio
            CommonDialog1Save.InitialDirectory = "C:\\";
            // Establecer los filtros
            CommonDialog1Save.Filter = "Archivos de texto" +
                                        "(*.txt)|*.txt";
            // Especificar el filtro predeterminado
            CommonDialog1Save.FilterIndex = 2;
            // Presentar el cuadro de di�logo Guardar
            DialogResult dr = CommonDialog1Save.ShowDialog();
            if (dr == DialogResult.OK)
            {
                try
                {
                    FileSystem.FileOpen(1, CommonDialog1Save.FileName, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
                    if (Itotal != 0)
                    {
                        //JavierM cambio 0 por 1 en el for
                        for (int i = 1; i <= Itotal; i++)
                        {
                            stFICHERO = new StringBuilder(AFichero[i].StCodHospi);
                            stFICHERO.Append(AFichero[i].StNumEntrada);
                            stFICHERO.Append(AFichero[i].StFechaIngre);
                            stFICHERO.Append(AFichero[i].StNumSalida);
                            stFICHERO.Append(AFichero[i].StFechaNaci);
                            stFICHERO.Append(AFichero[i].stSexo);
                            stFICHERO.Append(AFichero[i].StEstadoCivil);
                            stFICHERO.Append(AFichero[i].StCodProvin);
                            stFICHERO.Append(AFichero[i].StTipEntrada);
                            stFICHERO.Append(AFichero[i].StMotIngre);
                            stFICHERO.Append(AFichero[i].StFechaAlta);
                            stFICHERO.Append(AFichero[i].StTipCaso);
                            stFICHERO.Append(AFichero[i].StDiagIngre);
                            stFICHERO.Append(AFichero[i].StClasifi);
                            stFICHERO.Append(AFichero[i].stMotAlta);
                            stFICHERO.Append(AFichero[i].StDiagnos);
                            stFICHERO.Append(AFichero[i].StNumeroHistoria);
                            FileSystem.PrintLine(1, stFICHERO.ToString());
                        }
                    }
                    FileSystem.FileClose(1);
                    this.Close();
                }
                catch (Exception exception)
                {

                    //Procedimiento al que va cada vez que se produce un error

                    //    Dim er As rdoError
                    //
                    //    For Each er In rdoErrors
                    //        'GestionErrorBaseDatos(rdoErrors.number)
                    //        Call MsgBox(er.Number & "-" & er.Description, vbCritical + vbOKOnly)
                    //    Next er
                    //
                    //    rdoErrors.Clear
                    if (Information.Err().Number == (int)DialogResult.Cancel)
                    {
                        // El usuario ha hecho clic en el bot�n Cancelar
                        return;
                    }
                    else
                    {
                        if (Information.Err().Number == 55)
                        { //El archivo ya est� abierto
                            FileSystem.FileClose(1);
                        }
                        switch (Serrores.GestionErrorDisquete(Information.Err().Number, exception.Message, Conexion_Base_datos.RcAdmision))
                        {
                            case 0:
                                return;
                            case 1:
                                //UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx 
                                //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
                                break;
                            case 2:
                                //UPGRADE_TODO: (1065) Error handling statement (Resume) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx 
                                //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Statement");
                                break;
                        }
                    }
                }
            }

        }


        private void ObtenerDatos()
        {
            //Obtengo los datos que necesito grabar en el disco
            //**************** CRIS ***************************************

            int ILong = 0;
            string[] MatrizSql = null;
            DLLTextosSql.TextosSql oTextoSql = null;
            //Obtengo los datos necesarios para grabar en el fichero
            //24-5-00 JavierM inicializo Itotal = 1

            Itotal = 1;
            //StComando = "Select aepisadm.GNUMADME,aepisadm.FLLEGADA,"
            //StComando = StComando & "aepisadm.NNUMADMS,dpacient.FNACIPAC,"
            //StComando = StComando & "dpacient.ITIPSEXO,dpacient.GESTACIV,"
            //StComando = StComando & "destaciv.CESTAOFI,dpacient.GPROVINC,"
            //StComando = StComando & "aepisadm.ITIPINGR,dmotingr.CMOTIOFI,"
            //StComando = StComando & "aepisadm.FALTPLAN,aepisadm.IREVISIO,"
            //StComando = StComando & "aepisadm.GDIAGING,aepisadm.GDIAGALT,"
            //StComando = StComando & "aepisadm.GMOTALTA,"
            //StComando = StComando & "dmotalta.CMOTAOFI,aepisadm.ODIAGALT "
            //StComando = StComando & "from AEPISADM,DPACIENT,DESTACIV,"
            //StComando = StComando & "DMOTINGR,DMOTALTA "
            //StComando = StComando & "Where aepisadm.GIDENPAC = dpacient.GIDENPAC "
            //StComando = StComando & "and dpacient.GESTACIV*=destaciv.GESTACIV "
            //StComando = StComando & "and aepisadm.GMOTINGR=dmotingr.GMOTINGR "
            //StComando = StComando & "and aepisadm.GMOTALTA*=dmotalta.GMOTALTA "
            //StComando = StComando & "and aepisadm.FALTPLAN is not NULL "
            //StComando = StComando & "and aepisadm.FALTPLAN>=" & "" & FormatFecha(SdcFechaIni.Text) & ""
            //StComando = StComando & " and aepisadm.FALTPLAN<=" & "" & FormatFechaHM(SdcFechaFin.Text & " 23:59") & ""
            //If TxtTerminado.Text <> "" Then
            //    StComando = StComando & " and SUBSTRING(STR(aepisadm.NNUMADMS,6),6,1)=" & "'" & Trim(TxtTerminado.Text) & "'"
            //End If
            //StComando = StComando & " order by aepisadm.NNUMADMS "

            if (TxtTerminado.Text != "")
            {
                MatrizSql = new string[] { String.Empty, String.Empty, String.Empty, String.Empty };
                object tempRefParam = SdcFechaIni.Text;
                MatrizSql[1] = Serrores.FormatFecha(tempRefParam);
                object tempRefParam2 = SdcFechaFin.Text + " 23:59";
                MatrizSql[2] = Serrores.FormatFechaHM(tempRefParam2);
                MatrizSql[3] = TxtTerminado.Text.Trim();
                oTextoSql = new DLLTextosSql.TextosSql();
                string tempRefParam3 = "ADMISION";
                string tempRefParam4 = "AGI418F1";
                string tempRefParam5 = "ObtenerDatos";
                short tempRefParam6 = 1;
                StComando = oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, MatrizSql, Conexion_Base_datos.RcAdmision);
                oTextoSql = null;
            }
            else
            {
                MatrizSql = new string[] { String.Empty, String.Empty, String.Empty };
                object tempRefParam8 = SdcFechaIni.Text;
                MatrizSql[1] = Serrores.FormatFecha(tempRefParam8);
                object tempRefParam9 = SdcFechaFin.Text + " 23:59";
                MatrizSql[2] = Serrores.FormatFechaHM(tempRefParam9);
                oTextoSql = new DLLTextosSql.TextosSql();
                string tempRefParam10 = "ADMISION";
                string tempRefParam11 = "AGI418F1";
                string tempRefParam12 = "ObtenerDatos";
                short tempRefParam13 = 2;
                StComando = oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam10, tempRefParam11, tempRefParam12, tempRefParam13, MatrizSql, Conexion_Base_datos.RcAdmision);
                oTextoSql = null;
            }
            SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, Conexion_Base_datos.RcAdmision);
            Rrs = new DataSet();
            tempAdapter.Fill(Rrs);
            if (Rrs.Tables[0].Rows.Count != 0)
            {
                // 24-5-00 Rrs.RowCount - 1 quito ese menos 1 para que cuando venga un solo registro lo grabe en el fichero
                AFichero = new Morbilidad_hospitalaria.TyDisco[Rrs.Tables[0].Rows.Count + 1];

                foreach (DataRow iteration_row in Rrs.Tables[0].Rows)
                {
                    //Codigo Hospital
                    AFichero[Itotal] = Morbilidad_hospitalaria.TyDisco.CreateInstance();
                    AFichero[Itotal].StCodHospi = StHOSPI.Trim();
                    //A�adimos ceros por la izquierda hasta completar 8 posiciones
                    while (AFichero[Itotal].StCodHospi.Length < 8)
                    {
                        AFichero[Itotal].StCodHospi = AFichero[Itotal].StCodHospi + " ";
                    }

                    //N � entrada  GNUMADME
                    AFichero[Itotal].StNumEntrada = Convert.ToString(iteration_row[0]);
                    //A�adimos ceros por la izquierda hasta completar 5 posiciones
                    while (AFichero[Itotal].StNumEntrada.Trim().Length < 5)
                    {
                        AFichero[Itotal].StNumEntrada = "0" + AFichero[Itotal].StNumEntrada;
                    }

                    //Fecha de ingreso  FLLEGADA DDMMAAAA
                    //        AFichero(Itotal).StFechaIngre = Format(Rrs(1), "ddmmyyyy")
                    //(maplaza)(08/09/2006)El formato de fecha (definido por una constante de SQL) para los campos del fichero
                    //de morbilidad s�lo aplica cuando nos referimos al resto de fechas contenidas en el fichero; para el caso
                    //de que nos refiramos a la fecha de ingreso, el formato es DDMMAAAA (independientemente del valor de la constante).
                    //AFichero(Itotal).StFechaIngre = Format(Rrs(1), stFormato)
                    AFichero[Itotal].StFechaIngre = Convert.ToDateTime(iteration_row[1]).ToString("ddMMyyyy");
                    //-----

                    //N� de salida  NNUMADMS
                    if (!Convert.IsDBNull(iteration_row[2]))
                    {
                        AFichero[Itotal].StNumSalida = Convert.ToString(iteration_row[2]);
                    }
                    else
                    {
                        //Si es nulo le ponemos 5 ceros
                        AFichero[Itotal].StNumSalida = "00000";
                    }
                    //A�adimos ceros por la izquierda hasta completar 5 posiciones
                    while (AFichero[Itotal].StNumSalida.Trim().Length < 5)
                    {
                        AFichero[Itotal].StNumSalida = "0" + AFichero[Itotal].StNumSalida;
                    }

                    //Fecha de nacimiento  FNACIPAC  AAAAMMDD
                    if (!Convert.IsDBNull(iteration_row[3]))
                    {
                        //AFichero(Itotal).StFechaNaci = Mid(Format(Rrs(3), "ddmmyyyy"), 2)
                        //            AFichero(Itotal).StFechaNaci = Format(Rrs(3), "ddmmyyyy")
                        AFichero[Itotal].StFechaNaci = StringsHelper.Format(iteration_row[3], stFormato);
                    }

                    //Sexo  ITIPSEXO
                    //comparo el campo ITIPSEXO con los adtos obtenidos de
                    //SCONSGLO
                    string switchVar = Convert.ToString(iteration_row[4]).Trim();
                    if (switchVar == StIniMUJER.Trim())
                    {
                        AFichero[Itotal].stSexo = StMUJER.Trim();
                    }
                    else if (switchVar == StIniHOMBRE.Trim())
                    {
                        AFichero[Itotal].stSexo = StHOMBRE.Trim();
                    }
                    else if (switchVar == StIniINDET.Trim())
                    {
                        AFichero[Itotal].stSexo = StINDET.Trim();
                    }

                    //Estado civil  CESTAOFI
                    if (!Convert.IsDBNull(iteration_row[5]) && !Convert.IsDBNull(iteration_row[6]))
                    { //GESTACIV  CESTAOFI
                        AFichero[Itotal].StEstadoCivil = Convert.ToString(iteration_row[6]); //CESTAOFI
                    }

                    //Provincia  GPROVINC
                    if (!Convert.IsDBNull(iteration_row[7]))
                    {
                        AFichero[Itotal].StCodProvin = Convert.ToString(iteration_row[7]).Trim();
                    }

                    //Tipo de entrada  ITIPINGR
                    if (Convert.ToString(iteration_row[8]).Trim() == "U")
                    {
                        AFichero[Itotal].StTipEntrada = "2";
                    }
                    else
                    {
                        AFichero[Itotal].StTipEntrada = "1";
                    }

                    //Motivo del ingreso  CMOTIOFI
                    if (!Convert.IsDBNull(iteration_row[9]))
                    {
                        if (Convert.ToDouble(iteration_row[9]) == 9)
                        {
                            AFichero[Itotal].StMotIngre = "6";
                        }
                        else
                        {
                            AFichero[Itotal].StMotIngre = Convert.ToString(iteration_row[9]).Trim();
                        }
                    }

                    //Fecha de alta  FALTPLAN  AAMMDD
                    //AFichero(Itotal).StFechaAlta = Format(Rrs(10), "yyyymmdd")
                    //         AFichero(Itotal).StFechaAlta = Format(Rrs(10), "ddmmyyyy")
                    AFichero[Itotal].StFechaAlta = StringsHelper.Format(iteration_row[10], stFormato);

                    //Tipo de caso  IREVISIO
                    if (Convert.ToString(iteration_row[11]).Trim() == "S")
                    {
                        AFichero[Itotal].StTipCaso = "2";
                    }
                    else if (Convert.ToString(iteration_row[11]).Trim() == "N")
                    {
                        AFichero[Itotal].StTipCaso = "1";
                    }

                    //Diagn�stico del ingreso  GDIAGING
                    //cojo las primeras posiciones del campo gdiaging
                    //hasta el punto decimal. Si no tiene punto decimal
                    //lo inserto sin m�s. En cualquiera de los dos casos,
                    //relleno con espacios por la izquierda hasta 4 posiciones
                    //en la cuarta posicion va la posicion siguiente al punto o blanco
                    if (!Convert.IsDBNull(iteration_row[13]))
                    {
                        ILong = (Convert.ToString(iteration_row[13]).Trim().IndexOf('.') + 1);
                        if (ILong != 0)
                        {
                            ILong--;
                            AFichero[Itotal].StDiagIngre = Convert.ToString(iteration_row[13]).Trim().Substring(0, Math.Min(ILong, Convert.ToString(iteration_row[13]).Trim().Length)) + Convert.ToString(iteration_row[13]).Trim().Substring(ILong + 1, Math.Min(1, Convert.ToString(iteration_row[13]).Trim().Length - (ILong + 1)));
                        }
                        else
                        {
                            AFichero[Itotal].StDiagIngre = Convert.ToString(iteration_row[13]).Trim();
                        }
                    }

                    //Clasificaci�n  GDIAGALT
                    if (!Convert.IsDBNull(iteration_row[13]))
                    {
                        //buscar la clasificacion basica del diagnostico
                        AFichero[Itotal].StClasifi = ClasificacionBasica(AFichero[Itotal].StDiagIngre.Substring(0, Math.Min(3, AFichero[Itotal].StDiagIngre.Length)));
                    }

                    //        If Not IsNull(Rrs(13)) And IsNumeric(Mid(Trim(Rrs(13)), 1, 1)) Then
                    //            If 1 <= Val(Mid(Trim(Rrs(13)), 1, 1)) And Val(Mid(Trim(Rrs(13)), 1, 1)) <= 6 Then
                    //               AFichero(Itotal).StClasifi = Mid(Trim(Rrs(13)), 1, 1)
                    //            End If
                    //        End If

                    //Motivo del alta  CMOTAOFI
                    //apartir de 4 lleva 4 Otros
                    if (!Convert.IsDBNull(iteration_row[14]))
                    {
                        AFichero[Itotal].stMotAlta = MotivoAlta(Convert.ToString(iteration_row[14]));
                    }
                    //        If Not IsNull(Rrs(14)) And Not IsNull(Rrs(15)) Then 'GMOTALTA  'CMOTAOFI
                    //            AFichero(Itotal).stMotAlta = Trim(Rrs(15))  'CMOTAOFI
                    //        End If

                    //Diagn�sticos  GDIAGALT  ODIAGALT
                    if (!Convert.IsDBNull(iteration_row[13]))
                    { //GDIAGALT     FALTPLAN
                        string tempRefParam15 = Convert.ToString(iteration_row[10]).Trim();
                        AFichero[Itotal].StDiagnos = Conexion_Base_datos.ObtenerDiagnostico(Convert.ToString(iteration_row[13]).Trim(), ref tempRefParam15);
                    }
                    else if (!Convert.IsDBNull(iteration_row[16]))
                    {  //ODIAGALT
                        AFichero[Itotal].StDiagnos = Convert.ToString(iteration_row[16]).Trim();
                    }
                    AFichero[Itotal].StNumeroHistoria = "         "; //Space(9)
                    Itotal++;
                }
            }
            if (Itotal != 0)
            {
                Itotal--;
            }
            else
            {
                Itotal = 0;
            }
            Rrs.Close();
        }




        private void ObtenerSexo()
        {
            //Obtenemos para cada tipo de sexo (HOMBRE,MUJER e INDETERMINADO),
            //la letra que los identifica en DPACIENT y el c�digo oficial
            //del sexo
            //****************CRIS***************************************
            StComando = "Select GCONSGLO,VALFANU3,VALFANU1 from SCONSGLO where GCONSGLO='SMUJER' ";
            StComando = StComando + "or GCONSGLO='SHOMBRE' or GCONSGLO='SINDET' ";
            StComando = StComando + "order by 1";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, Conexion_Base_datos.RcAdmision);
            Rrs = new DataSet();
            tempAdapter.Fill(Rrs);
            foreach (DataRow iteration_row in Rrs.Tables[0].Rows)
            {
                switch (Convert.ToString(iteration_row["GCONSGLO"]).Trim().ToUpper())
                {
                    case "SHOMBRE":
                        StHOMBRE = Convert.ToString(iteration_row["VALFANU3"]).Trim();
                        StIniHOMBRE = Convert.ToString(iteration_row["VALFANU1"]).Trim();
                        break;
                    case "SINDET":
                        StINDET = Convert.ToString(iteration_row["VALFANU3"]).Trim();
                        StIniINDET = Convert.ToString(iteration_row["VALFANU1"]).Trim();
                        break;
                    case "SMUJER":
                        StMUJER = Convert.ToString(iteration_row["VALFANU3"]).Trim();
                        StIniMUJER = Convert.ToString(iteration_row["VALFANU1"]).Trim();
                        break;
                }
            }
            Rrs.Close();
        }


        private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
        {
            //If ttemp Then
            //    DbInforme.Close
            //    Wk.Close
            //End If
            this.Close();
            //menu_admision.prokill (stInforme)
        }

        private void CbDisquete_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Cursor = Cursors.WaitCursor;
            GenerarDisco();
            this.Cursor = Cursors.Default;
        }


        private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }

        private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }

        //Public Sub proFormulas(idForm As Integer)
        //    Dim tiForm As Integer
        //    For tiForm = 0 To idForm
        //        cryInformes.Formulas(tiForm) = ""
        //    Next tiForm
        //End Sub

        // ralopezn_TODO_X_4 22/04/2016
        public void proImprimir(Crystal.DestinationConstants stAccion)
        {

            DataRow RsINFORME = null;
            DataSet DbInforme = null;
            string stAepisadm = String.Empty;
            string stDpacient = String.Empty;
            string stDpoblaci = String.Empty;
            string stAmovifin = String.Empty;
            string stHistoria = String.Empty;
            string stDmotalta = String.Empty;
            StringBuilder stDdiagnos = new StringBuilder();

            DataSet RrAEPISADM = null;
            DataSet RrDPACIENT = null;
            DataSet RrDPOBLACI = null;
            DataSet RrAMOVIFIN = null;
            DataSet RrHistoria = null;
            DataSet RrDMOTALTA = null;
            DataSet RrDDIAGNOS = null;
            DataTable Tabla = null;

            bool bExiste_base = false;
            string VstNomtabla = String.Empty;
            StringBuilder stNombrePaciente = new StringBuilder();

            object[] MatrizSql = null;
            DLLTextosSql.TextosSql oTextoSql = null;
            string LitPersona = String.Empty;

            try
            {
                if (!cbEntrada.IsChecked && !cbSalida.IsChecked)
                {
                    MessageBox.Show("es obligatorio la selecci�n de un criterio", Application.ProductName);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                // Realizamos las validaciones de las fechas introducidas *******************
                if (SdcFechaIni.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje2 = "menor o igual";
                    stMensaje3 = "Fecha del Sistema";
                    short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { Label1.Text, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    SdcFechaIni.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje2 = "menor o igual";
                    stMensaje3 = "Fecha del Sistema";
                    short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { Label2.Text, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                    SdcFechaFin.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date < SdcFechaIni.Value.Date)
                {
                    stMensaje2 = "menor o igual";
                    short tempRefParam5 = 1020;
                    string[] tempRefParam6 = new string[] { Label1.Text, stMensaje2, Label2.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                    SdcFechaIni.Focus();
                    return;
                }
                // **************************************************************************
                stIntervalo = "Desde: " + SdcFechaIni.Value.Date + "  Hasta: " + SdcFechaFin.Value.Date;

                //**************Comprueba si tiene datos sobre las cabeceras******************
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //****************************************************************************
                //proFormulas viform   'limpio las formulas

                stFechaini = SdcFechaIni.Value.Date.ToShortDateString() + " " + "00:00:00";
                stFechafin = SdcFechaFin.Value.Date.ToShortDateString() + " " + "23:59:59";
                // =========================================================================
                //'''''''' CREAMOS UNA TABLA TEMPORAL EN ACCESS QUE NOS SIRVE PARA HACER EL
                //'''''''' LISTADO Y QUE UNA VEZ SACADO ESTE SE ELIMINA
                // =========================================================================

                // BUSCAR SI EXISTE LA TABLA
                VstNomtabla = "CR11_AGI418R1";
                bExiste_base = false;


                DbInforme = new DataSet();


                DbInforme.CreateTable(VstNomtabla, "" + Conexion_Base_datos.PathString + "\\rpt\\CR11_AGI418R1.xsd");
                //DbInforme.Execute("create index tempoindex on " + VstNomtabla + " (fecha)");
                ttemp = true;

                // REGISTRAMOS EL DSN PARA EL CRYSTAL
                //stConexion = "Description= BASE TEMPORAL EN ACCESS" + "\r" + "Oemtoansi=No" + "\r" + "server = (local)" + "\r" + "dbq=" + stInforme + "";

                // GRABAMOS EN LA TABLA QUE HEMOS CREADO LOS CAMPOS QUE NOS VAN
                // HACER FALTA EN EL LISTADO


                Tabla = DbInforme.Tables[VstNomtabla];

                object tempRefParam7 = stFechaini;
                object tempRefParam8 = stFechafin;
                stAepisadm = "SELECT * " + " FROM AEPISADM " + " WHERE AEPISADM.faltplan IS NOT NULL AND " + " AEPISADM.faltplan >=" + Serrores.FormatFechaHMS(tempRefParam7) + " AND " + " AEPISADM.faltplan <=" + Serrores.FormatFechaHMS(tempRefParam8) + "";
                stFechafin = Convert.ToString(tempRefParam8);
                stFechaini = Convert.ToString(tempRefParam7);

                if (stTerminado.Trim() != "")
                {
                    stAepisadm = stAepisadm + " and  substring(str(nnumadms,6),6,1) ='" + stTerminado + "'";
                }

                stAepisadm = stAepisadm + " and AEPISADM.ganoadme > 1900"; //para q no salga hospital de dia


                if (cbEntrada.IsChecked)
                {
                    stAepisadm = stAepisadm + " order by GANOADME asc, GNUMADME asc ";
                }
                else
                {
                    stAepisadm = stAepisadm + " order by NANOADMS ASC, NNUMADMS ASC ";
                }

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stAepisadm, Conexion_Base_datos.RcAdmision);
                RrAEPISADM = new DataSet();
                tempAdapter.Fill(RrAEPISADM);
                RrAEPISADM.MoveFirst();
                foreach (DataRow iteration_row in RrAEPISADM.Tables[0].Rows)
                {
                    // Creamos UNA FILA en la TABLA TEMPORAL                    
                    RsINFORME = Tabla.NewRow();
                    RsINFORME["NSALIDA"] = Convert.ToString(iteration_row["nnumadms"]).Trim();
                    RsINFORME["ASALIDA"] = Convert.ToString(iteration_row["nanoadms"]).Trim();
                    RsINFORME["FECHA"] = iteration_row["fllegada"];
                    RsINFORME["fechaa"] = iteration_row["faltplan"];
                    RsINFORME["NINGRESO"] = Convert.ToString(iteration_row["gnumadme"]).Trim();
                    RsINFORME["AINGRESO"] = Convert.ToString(iteration_row["ganoadme"]).Trim();
                    RsINFORME["tipo"] = iteration_row["irevisio"];
                    if (Convert.ToString(iteration_row["ITIPINGR"]).Trim().ToUpper() == "U")
                    {
                        RsINFORME["TipoIngreso"] = iteration_row["ITIPINGR"];
                    }
                    else
                    {
                        RsINFORME["TipoIngreso"] = "O";
                    }

                    // Consulta para obtener datos del paciente sobre DPACIENT
                    //            stDpacient = "SELECT * " _
                    //'                & " FROM DPACIENT, DPROVINC, DESTACIV " _
                    //'                & " WHERE " _
                    //'                & " DPACIENT.gidenpac = '" & RrAEPISADM("gidenpac") & "' AND " _
                    //'                & " DPACIENT.gprovinc *= DPROVINC.gprovinc AND " _
                    //'                & " DPACIENT.gestaciv *= DESTACIV.gestaciv "

                    MatrizSql = new object[2];
                    MatrizSql[1] = iteration_row["gidenpac"];
                    oTextoSql = new DLLTextosSql.TextosSql();
                    string tempRefParam9 = "ADMISION";
                    string tempRefParam10 = "AGI418F1";
                    string tempRefParam11 = "proImprimir";
                    short tempRefParam12 = 1;
                    stDpacient = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam9, tempRefParam10, tempRefParam11, tempRefParam12, MatrizSql, Conexion_Base_datos.RcAdmision));
                    oTextoSql = null;
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stDpacient, Conexion_Base_datos.RcAdmision);
                    RrDPACIENT = new DataSet();
                    tempAdapter_2.Fill(RrDPACIENT);
                    stNombrePaciente = new StringBuilder((Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dape1pac"]).Trim());
                    stNombrePaciente.Append((Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dape2pac"]).Trim());
                    stNombrePaciente.Append((Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dnombpac"])) ? "" : ", " + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dnombpac"]).Trim());
                    RsINFORME["PACIENTE"] = stNombrePaciente.ToString();
                    RsINFORME["NACIMIENTO"] = RrDPACIENT.Tables[0].Rows[0]["fnacipac"];
                    RsINFORME["SEXO"] = RrDPACIENT.Tables[0].Rows[0]["itipsexo"];
                    RsINFORME["DNI"] = RrDPACIENT.Tables[0].Rows[0]["ndninifp"];
                    RsINFORME["ESTADO"] = RrDPACIENT.Tables[0].Rows[0]["destaciv"];
                    RsINFORME["direccion"] = RrDPACIENT.Tables[0].Rows[0]["ddirepac"];
                    RsINFORME["CODIGO"] = RrDPACIENT.Tables[0].Rows[0]["gcodipos"];
                    RsINFORME["PROVINCIA"] = RrDPACIENT.Tables[0].Rows[0]["dnomprov"];
                    if (Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["gpoblaci"]))
                    {
                        // Si no hay codigo de la POBLACION ponemos la descripcion
                        // que hayan introducido en la tabla de DPACIENT
                        RsINFORME["poblacion"] = RrDPACIENT.Tables[0].Rows[0]["dpoblaci"];
                    }
                    else
                    {
                        // Hay codigo y sacamos la poblacion a la que corresponde
                        stDpoblaci = "SELECT * " + " FROM DPOBLACI " + " WHERE " + " DPOBLACI.gpoblaci ='" + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["gpoblaci"]) + "' ";
                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stDpoblaci, Conexion_Base_datos.RcAdmision);
                        RrDPOBLACI = new DataSet();
                        tempAdapter_3.Fill(RrDPOBLACI);
                        RsINFORME["poblacion"] = RrDPOBLACI.Tables[0].Rows[0]["dpoblaci"];
                        RrDPOBLACI.Close();
                    }
                    if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["dpaisres"]))
                    {
                        if (!Convert.IsDBNull(RsINFORME["poblacion"]))
                        {
                            RsINFORME["poblacion"] = (((string)RsINFORME["poblacion"]) + "(" + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dpaisres"]) + ")");
                        }
                        else
                        {
                            RsINFORME["poblacion"] = (((string)RsINFORME["poblacion"]) + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["dpaisres"]));
                        }

                    }
                    RsINFORME["filiacion"] = RrDPACIENT.Tables[0].Rows[0]["nafiliac"];
                    RrDPACIENT.Close();
                    // Consulta para obtener la FINANCIADORA sobre AMOVIFIN
                    //            stAmovifin = "SELECT * " _
                    //'                & " FROM AMOVIFIN, DSOCIEDA, DINSPECC " _
                    //'                & " WHERE " _
                    //'                & " AMOVIFIN.ganoregi = " & RrAEPISADM("ganoadme") & " AND " _
                    //'                & " AMOVIFIN.gnumregi = " & RrAEPISADM("gnumadme") & " AND " _
                    //'                & " AMOVIFIN.ffinmovi = " & FormatFechaHMS(RrAEPISADM("faltplan")) & " AND " _
                    //'                & " AMOVIFIN.gsocieda = DSOCIEDA.gsocieda AND " _
                    //'                & " AMOVIFIN.GINSPECC *=  DINSPECC.GINSPECC "
                    MatrizSql = new object[4];
                    MatrizSql[1] = iteration_row["ganoadme"];
                    MatrizSql[2] = iteration_row["gnumadme"];
                    object tempRefParam14 = iteration_row["faltplan"];
                    MatrizSql[3] = Serrores.FormatFechaHMS(tempRefParam14);
                    oTextoSql = new DLLTextosSql.TextosSql();
                    string tempRefParam15 = "ADMISION";
                    string tempRefParam16 = "AGI418F1";
                    string tempRefParam17 = "proImprimir";
                    short tempRefParam18 = 2;
                    stAmovifin = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam15, tempRefParam16, tempRefParam17, tempRefParam18, MatrizSql, Conexion_Base_datos.RcAdmision));
                    oTextoSql = null;

                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stAmovifin, Conexion_Base_datos.RcAdmision);
                    RrAMOVIFIN = new DataSet();
                    tempAdapter_4.Fill(RrAMOVIFIN);
                    if (RrAMOVIFIN.Tables[0].Rows.Count > 0)
                    {
                        RsINFORME["financiadora"] = Convert.ToString(RrAMOVIFIN.Tables[0].Rows[0]["dsocieda"]).Trim();
                    }
                    else
                    {
                        RsINFORME["financiadora"] = "";
                    }
                    if (!Convert.IsDBNull(RrAMOVIFIN.Tables[0].Rows[0]["DINSPECC"]))
                    {
                        RsINFORME["INSPECCION"] = Convert.ToString(RrAMOVIFIN.Tables[0].Rows[0]["DINSPECC"]).Trim();
                    }
                    RrAMOVIFIN.Close();
                    // Consulta para sacar la HISTORIA
                    stHistoria = " SELECT * " + " FROM HDOSSIER, HSERARCH " + " WHERE " + " HDOSSIER.gidenpac = '" + Convert.ToString(iteration_row["gidenpac"]) + "' AND " + " HDOSSIER.gserpropiet = HSERARCH.gserarch AND " + " HSERARCH.icentral = 'S' ";
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stHistoria, Conexion_Base_datos.RcAdmision);
                    RrHistoria = new DataSet();
                    tempAdapter_5.Fill(RrHistoria);
                    if (RrHistoria.Tables[0].Rows.Count != 0)
                    {
                        RsINFORME["historia"] = RrHistoria.Tables[0].Rows[0]["ghistoria"];
                        RrHistoria.Close();
                    }
                    else
                    {
                        RsINFORME["historia"] = "";
                        RrHistoria.Close();
                    }
                    // Consulta para sacar DESCRIPCIONES DE LOS CODIGOS DE AEPISADM
                    // para motivo alta, motivo ingreso, servicio
                    stDmotalta = " SELECT * " + " FROM DMOTALTA, DMOTINGR, DSERVICI " + " WHERE " + " DMOTALTA.gmotalta =" + Convert.ToString(iteration_row["gmotalta"]) + " AND " + " DMOTINGR.gmotingr =" + Convert.ToString(iteration_row["gmotingr"]) + " AND " + " DSERVICI.gservici =" + Convert.ToString(iteration_row["gservici"]) + " ";
                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stDmotalta, Conexion_Base_datos.RcAdmision);
                    RrDMOTALTA = new DataSet();
                    tempAdapter_6.Fill(RrDMOTALTA);
                    RsINFORME["motivoi"] = RrDMOTALTA.Tables[0].Rows[0]["dmotingr"];
                    RsINFORME["motivoa"] = RrDMOTALTA.Tables[0].Rows[0]["dmotalta"];
                    RsINFORME["servicio"] = RrDMOTALTA.Tables[0].Rows[0]["dnomserv"];
                    RrDMOTALTA.Close();
                    // Consulta para sacar el DIAGNOSTICO AL INGRESO
                    //*************
                    if (!Convert.IsDBNull(iteration_row["gdiagini"]))
                    {
                        stDdiagnos = new StringBuilder("Select DNOMBDIA from DDIAGNOS ");
                        stDdiagnos.Append("where GCODIDIA=" + "'" + Convert.ToString(iteration_row["gdiagini"]) + "'");
                        object tempRefParam20 = iteration_row["fllegada"];
                        stDdiagnos.Append(" and FINVALDI<=" + "" + Serrores.FormatFechaHM(tempRefParam20) + "");
                        object tempRefParam21 = iteration_row["fllegada"];
                        stDdiagnos.Append(" and (FFIVALDI>=" + "" + Serrores.FormatFechaHM(tempRefParam21) + "");
                        stDdiagnos.Append(" or FFIVALDI IS NULL)");

                        SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stDdiagnos.ToString(), Conexion_Base_datos.RcAdmision);
                        RrDDIAGNOS = new DataSet();
                        tempAdapter_7.Fill(RrDDIAGNOS);
                        if (RrDDIAGNOS.Tables[0].Rows.Count != 0)
                        {
                            RsINFORME["diagnosticoi"] = ((Convert.IsDBNull(RrDDIAGNOS.Tables[0].Rows[0]["dnombdia"])) ? "" : Convert.ToString(RrDDIAGNOS.Tables[0].Rows[0]["dnombdia"]).Trim());
                        }
                        RrDDIAGNOS.Close();
                        stDdiagnos = new StringBuilder("");

                    }
                    else
                    {
                        RsINFORME["diagnosticoi"] = ((Convert.IsDBNull(iteration_row["odiagini"])) ? "" : Convert.ToString(iteration_row["odiagini"]).Trim());
                    }
                    //*************
                    //            If IsNull(RrAEPISADM("odiagini")) Then
                    //                RsINFORME("diagnosticoi") = IIf(IsNull(RrAEPISADM("odiagini")), "", Trim(RrAEPISADM("odiagini")))
                    //            Else
                    //'                stDdiagnos = " SELECT * " _
                    //''                    & " FROM DDIAGNOS " _
                    //''                    & " WHERE " _
                    //''                    & " DDIAGNOS.gcodidia ='" & RrAEPISADM("gdiaging") & "' "
                    //
                    //                stDdiagnos = "Select DNOMBDIA from DDIAGNOS "
                    //                stDdiagnos = stDdiagnos & "where GCODIDIA=" & "'" & RrAEPISADM("gdiagini") & "'"
                    //                stDdiagnos = stDdiagnos & " and FINVALDI<=" & "" & FormatFechaHM(RrAEPISADM("fllegada")) & ""
                    //                stDdiagnos = stDdiagnos & " and (FFIVALDI>=" & "" & FormatFechaHM(RrAEPISADM("fllegada")) & ""
                    //                stDdiagnos = stDdiagnos & " or FFIVALDI IS NULL)"
                    //
                    //                Set RrDDIAGNOS = RcAdmision.OpenResultset(stDdiagnos, 1, 1)
                    //                If RrDDIAGNOS.RowCount = 1 Then
                    //                    RsINFORME("diagnosticoi") = Trim(RrDDIAGNOS("dnombdia"))
                    //                End If
                    //                RrDDIAGNOS.Close
                    //                stDdiagnos = ""
                    //            End If
                    // Consulta para sacar el DIAGNOSTICO AL ALTA
                    if (Convert.IsDBNull(iteration_row["gdiagalt"]))
                    {
                        RsINFORME["diagnosticoa"] = Convert.ToString(iteration_row["odiagalt"]).Trim();
                        RsINFORME["clasificacion"] = iteration_row["gdiagalt"];
                    }
                    else
                    {

                        stDdiagnos = new StringBuilder("Select DNOMBDIA from DDIAGNOS ");
                        stDdiagnos.Append("where GCODIDIA=" + "'" + Convert.ToString(iteration_row["gdiagalt"]) + "'");
                        object tempRefParam22 = iteration_row["faltplan"];
                        stDdiagnos.Append(" and FINVALDI<=" + "" + Serrores.FormatFechaHM(tempRefParam22) + "");
                        object tempRefParam23 = iteration_row["faltplan"];
                        stDdiagnos.Append(" and (FFIVALDI>=" + "" + Serrores.FormatFechaHM(tempRefParam23) + "");
                        stDdiagnos.Append(" or FFIVALDI IS NULL)");

                        SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stDdiagnos.ToString(), Conexion_Base_datos.RcAdmision);
                        RrDDIAGNOS = new DataSet();
                        tempAdapter_8.Fill(RrDDIAGNOS);
                        if (RrDDIAGNOS.Tables[0].Rows.Count == 1)
                        {
                            RsINFORME["clasificacion"] = iteration_row["gdiagalt"];
                            RsINFORME["diagnosticoa"] = Convert.ToString(RrDDIAGNOS.Tables[0].Rows[0]["dnombdia"]).Trim();
                        }
                        RrDDIAGNOS.Close();
                    }
                    if (!Convert.IsDBNull(iteration_row["gdiagalt"]))
                    {
                        RsINFORME["CLASIFICACION"] = ClasificacionBasica2(Convert.ToString(iteration_row["gdiagalt"]));
                    }

                    //RsINFORME.Update();
                    Tabla.Rows.Add(RsINFORME);
                }
                RrAEPISADM.Close();


                //'        If stFORMFILI = "V" Then
                //'            ProvinEstado = "ESTADO"
                //'        Else
                Conexion_Base_datos.ProvinEstado = "PROVINCIA";
                //'        End If
                // ********************************************************************
                // si es pantalla o impresora generar el listado
                //        If stAccion = "P" Or stAccion = "I" Then
                // Llamada al CRYSTAL para sacar el informe, CONEXION
                //cryInformes.Connect = "dsn=GHOSP_ACCESS"
                //cryInformes.Connect = "" + Conexion_Base_datos.stNombreDsnACCESS;
                cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\Agi418r1_CR11.rpt";
                cryInformes.WindowTitle = "Listado Anulaci�n de Ingresos";
                cryInformes.DataFiles[0] = Tabla;
                //cryInformes.SQLQuery = " SELECT * FROM " + VstNomtabla;
                // ************************************************************************
                cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                cryInformes.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
                cryInformes.Formulas["{@dni/cedula}"] = "\"" + Conexion_Base_datos.DniCedulaMay + "\"";
                cryInformes.Formulas["{@TexProvincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";
                //'            If stFORMFILI = "V" Then
                //'                cryInformes.Formulas(6) = "FORM4= 'INFORME GENERAL DE CASOS HOSPITALIZADOS POR PERIODO'"
                //'            Else
                cryInformes.Formulas["{@FORM4}"] = "'ENCUESTA DE MORBILIDAD HOSPITALARIA'";
                //'            End If

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.ToUpper();

                cryInformes.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";

                cryInformes.WindowTitle = "Comprobaci�n de N�meros de Salida";
                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                // cryInformes.Connect = "dsn=GHOSP_ACCESS"
                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                cryInformes.Destination = stAccion;
                cryInformes.WindowShowPrintSetupBtn = true;
                if (cryInformes.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(cryInformes);
                }
                cryInformes.Action = 1;
                cryInformes.PrinterStopPage = cryInformes.PageCount;
                viform = 9;
                Conexion_Base_datos.vacia_formulas(cryInformes, viform);
                SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
                SdcFechaFin.Text = SdcFechaIni.Text;
                TxtTerminado.Text = "";
                this.Cursor = Cursors.Default;
                //        End If
            }
            catch (Exception e)
            {
                this.Cursor = Cursors.Default;
                if (Information.Err().Number == 32755)
                {
                    //CANCELADA IMPREISON
                }
                else
                {
                    MessageBox.Show("error en el listado", Application.ProductName);
                }
            }
        }


        private void Morbilidad_hospitalaria_Load(Object eventSender, EventArgs eventArgs)
        {

            bool MostrarHospitales = false;

            string stsqltemp = "select isNull(valfanu1,'S') valfanu1 from SCONSGLO where gconsglo = 'IFECMORB'";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);

            if (rrtemp.Tables[0].Rows.Count == 0)
            {
                stFormato = "ddMMyyyy";
            }
            else if (Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "N")
            {
                stFormato = "yyyyMMdd";
            }
            else
            {
                stFormato = "ddMMyyyy";
            }

            rrtemp.Close();

            // ************************************************************************
            // Creamos aqui la BASE DE DATOS TEMPORAL, para que cuando eliga las opciones
            // para el informe solo tengamos que crear la TABLA TEMPORAL
            cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;
            //stInforme = App.Path & "\INFORME.mdb"
            stInforme = Conexion_Base_datos.StBaseTemporal;
            //If Dir(stInforme) <> "" Then Kill stInforme ' comprueba que esta eliminada
            if (FileSystem.Dir(stInforme, FileAttribute.Normal) != "")
            { // comprueba que esta eliminada
              //Set Wk = DBEngine.Workspaces(0)
              //Set DbInforme = Wk.CreateDatabase(stInforme, dbLangSpanish)
                ttemp = true;

            }
            else
            {
                //Set Wk = DBEngine.Workspaces(0)
                //Set DbInforme = Wk.OpenDatabase(stInforme)
                //DbInforme.Execute " DROP TABLE TEMPORAL;"
                ttemp = false;
            }
            // ************************************************************************

            //'    If stFORMFILI = "V" Then
            //'        Morbilidad_hospitalaria.Caption = "Informe general de casos hospitalizados por periodo - AGI418F1"
            //'    End If
        }

        private void Morbilidad_hospitalaria_Closed(Object eventSender, EventArgs eventArgs)
        {
            cryInformes = null;
        }





        private void TxtTerminado_TextChanged(Object eventSender, EventArgs eventArgs)
        {
            stTerminado = TxtTerminado.Text;
        }

        private string ClasificacionBasica2(string StCodigo)
        {
            string result = String.Empty;
            result = "";
            string sql = "select  cclasbas, dclasbas from dcladiag ";
            sql = sql + "where cdiagdes <= '" + StCodigo.Trim() + "' and cdiaghas >= '" + StCodigo.Trim() + "' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);

            if (RrSql.Tables[0].Rows.Count != 0)
            {
                result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["cclasbas"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["cclasbas"]).Trim();
                result = result + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dclasbas"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dclasbas"]).Trim());
            }
            RrSql.Close();
            return result;
        }


        private string ClasificacionBasica(string StCodigo)
        {


            string result = String.Empty;
            result = "";
            string sql = "select  cclasbas from dcladiag ";
            sql = sql + "where cdiagdes <= '" + StCodigo.Trim() + "' and cdiaghas >= '" + StCodigo.Trim() + "' ";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);

            if (RrSql.Tables[0].Rows.Count != 0)
            {
                result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["cclasbas"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["cclasbas"]).Trim();
            }

            RrSql.Close();
            return result;
        }

        private string MotivoAlta(string StCodigo)
        {

            string result = String.Empty;
            result = "";
            string sql = "select * from dmotalta where gmotalta = " + Convert.ToInt32(Double.Parse(StCodigo)).ToString();

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);

            if (RrSql.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToString(RrSql.Tables[0].Rows[0]["itraslad"]) == "S")
                {
                    result = "2";
                }
                else if (Convert.ToString(RrSql.Tables[0].Rows[0]["idomicil"]) == "S")
                {
                    result = "1";
                }
                else if (Convert.ToString(RrSql.Tables[0].Rows[0]["icexitus"]) == "S")
                {
                    result = "3";
                }
                else
                {
                    result = "4";
                }
            }

            RrSql.Close();
            return result;
        }
    }
}