using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class ConversionHDaH
	{

		#region "Upgrade Support "
		private static ConversionHDaH m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static ConversionHDaH DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new ConversionHDaH();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "rbInmediato", "Fringreso", "_rbProceso_0", "_rbProceso_1", "FrProceso", "CbSeleccion", "rbCompartido", "rbIndividual", "Frame5", "tbCama", "Label2", "Frame4", "RbMujer", "RbHombre", "Frmsexo", "Label17", "lbNombrePaciente", "Label12", "lbHistoria", "Frame2", "CbAceptar", "CbCancelar", "lbFechaIngreso", "Label21", "Frame1", "lbResponsable", "lbDiagnostico", "lbServicio", "Label1", "Label3", "Label4", "Frame3", "lbInfo", "Label6", "Image1", "rbProceso"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadRadioButton rbInmediato;
		public Telerik.WinControls.UI.RadGroupBox Fringreso;
		private Telerik.WinControls.UI.RadRadioButton _rbProceso_0;
		private Telerik.WinControls.UI.RadRadioButton _rbProceso_1;
		public Telerik.WinControls.UI.RadGroupBox FrProceso;
		public Telerik.WinControls.UI.RadButton CbSeleccion;
		public Telerik.WinControls.UI.RadRadioButton rbCompartido;
		public Telerik.WinControls.UI.RadRadioButton rbIndividual;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public Telerik.WinControls.UI.RadTextBoxControl tbCama;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Telerik.WinControls.UI.RadRadioButton RbMujer;
		public Telerik.WinControls.UI.RadRadioButton RbHombre;
		public Telerik.WinControls.UI.RadGroupBox Frmsexo;
		public Telerik.WinControls.UI.RadLabel Label17;
		public Telerik.WinControls.UI.RadTextBox lbNombrePaciente;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadTextBox lbFechaIngreso;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadTextBox lbResponsable;
		public Telerik.WinControls.UI.RadTextBox lbDiagnostico;
		public Telerik.WinControls.UI.RadTextBox lbServicio;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadLabel lbInfo;
		public Telerik.WinControls.UI.RadLabel Label6;
		public System.Windows.Forms.PictureBox Image1;
		public Telerik.WinControls.UI.RadRadioButton[] rbProceso = new Telerik.WinControls.UI.RadRadioButton[2];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConversionHDaH));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Fringreso = new Telerik.WinControls.UI.RadGroupBox();
            this.rbInmediato = new Telerik.WinControls.UI.RadRadioButton();
            this.FrProceso = new Telerik.WinControls.UI.RadGroupBox();
            this._rbProceso_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbProceso_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbSeleccion = new Telerik.WinControls.UI.RadButton();
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbCompartido = new Telerik.WinControls.UI.RadRadioButton();
            this.rbIndividual = new Telerik.WinControls.UI.RadRadioButton();
            this.tbCama = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frmsexo = new Telerik.WinControls.UI.RadGroupBox();
            this.RbMujer = new Telerik.WinControls.UI.RadRadioButton();
            this.RbHombre = new Telerik.WinControls.UI.RadRadioButton();
            this.Label17 = new Telerik.WinControls.UI.RadLabel();
            this.lbNombrePaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.CbAceptar = new Telerik.WinControls.UI.RadButton();
            this.CbCancelar = new Telerik.WinControls.UI.RadButton();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbFechaIngreso = new Telerik.WinControls.UI.RadTextBox();
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this.lbResponsable = new Telerik.WinControls.UI.RadTextBox();
            this.lbDiagnostico = new Telerik.WinControls.UI.RadTextBox();
            this.lbServicio = new Telerik.WinControls.UI.RadTextBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.lbInfo = new Telerik.WinControls.UI.RadLabel();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Image1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Fringreso)).BeginInit();
            this.Fringreso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbInmediato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrProceso)).BeginInit();
            this.FrProceso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbProceso_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbProceso_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbSeleccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbCompartido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbIndividual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frmsexo)).BeginInit();
            this.Frmsexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbMujer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbHombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombrePaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbFechaIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbResponsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDiagnostico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Fringreso
            // 
            this.Fringreso.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Fringreso.Controls.Add(this.rbInmediato);
            this.Fringreso.HeaderText = "Tipo Ingreso";
            this.Fringreso.Location = new System.Drawing.Point(4, 296);
            this.Fringreso.Name = "Fringreso";
            this.Fringreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Fringreso.Size = new System.Drawing.Size(108, 72);
            this.Fringreso.TabIndex = 24;
            this.Fringreso.Text = "Tipo Ingreso";
            // 
            // rbInmediato
            // 
            this.rbInmediato.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbInmediato.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbInmediato.Enabled = false;
            this.rbInmediato.Location = new System.Drawing.Point(16, 32);
            this.rbInmediato.Name = "rbInmediato";
            this.rbInmediato.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbInmediato.Size = new System.Drawing.Size(71, 18);
            this.rbInmediato.TabIndex = 25;
            this.rbInmediato.Text = "Inmediato";
            this.rbInmediato.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.rbInmediato.CheckStateChanged += new System.EventHandler(this.rbInmediato_CheckedChanged);
            // 
            // FrProceso
            // 
            this.FrProceso.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrProceso.Controls.Add(this._rbProceso_0);
            this.FrProceso.Controls.Add(this._rbProceso_1);
            this.FrProceso.HeaderText = "Proceso";
            this.FrProceso.Location = new System.Drawing.Point(114, 296);
            this.FrProceso.Name = "FrProceso";
            this.FrProceso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrProceso.Size = new System.Drawing.Size(93, 72);
            this.FrProceso.TabIndex = 21;
            this.FrProceso.Text = "Proceso";
            // 
            // _rbProceso_0
            // 
            this._rbProceso_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbProceso_0.Enabled = false;
            this._rbProceso_0.Location = new System.Drawing.Point(8, 20);
            this._rbProceso_0.Name = "_rbProceso_0";
            this._rbProceso_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbProceso_0.Size = new System.Drawing.Size(58, 18);
            this._rbProceso_0.TabIndex = 23;
            this._rbProceso_0.Text = "M�dico";
            this._rbProceso_0.CheckStateChanged += new System.EventHandler(this.rbProceso_CheckedChanged);
            // 
            // _rbProceso_1
            // 
            this._rbProceso_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbProceso_1.Enabled = false;
            this._rbProceso_1.Location = new System.Drawing.Point(8, 48);
            this._rbProceso_1.Name = "_rbProceso_1";
            this._rbProceso_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbProceso_1.Size = new System.Drawing.Size(73, 18);
            this._rbProceso_1.TabIndex = 22;
            this._rbProceso_1.Text = "Quir�rgico";
            this._rbProceso_1.CheckStateChanged += new System.EventHandler(this.rbProceso_CheckedChanged);
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.CbSeleccion);
            this.Frame4.Controls.Add(this.Frame5);
            this.Frame4.Controls.Add(this.tbCama);
            this.Frame4.Controls.Add(this.Label2);
            this.Frame4.HeaderText = "Cama Destino";
            this.Frame4.Location = new System.Drawing.Point(4, 240);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(533, 53);
            this.Frame4.TabIndex = 17;
            this.Frame4.Text = "Cama Destino";
            // 
            // CbSeleccion
            // 
            this.CbSeleccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbSeleccion.Location = new System.Drawing.Point(450, 23);
            this.CbSeleccion.Name = "CbSeleccion";
            this.CbSeleccion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbSeleccion.Size = new System.Drawing.Size(70, 18);
            this.CbSeleccion.TabIndex = 33;
            this.CbSeleccion.Text = "Selecci�n";
            this.CbSeleccion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbSeleccion.Click += new System.EventHandler(this.CbSeleccion_Click);
            // 
            // Frame5
            // 
            this.Frame5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame5.Controls.Add(this.rbCompartido);
            this.Frame5.Controls.Add(this.rbIndividual);
            this.Frame5.HeaderText = "";
            this.Frame5.Location = new System.Drawing.Point(12, 15);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(284, 33);
            this.Frame5.TabIndex = 19;
            // 
            // rbCompartido
            // 
            this.rbCompartido.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbCompartido.Location = new System.Drawing.Point(134, 10);
            this.rbCompartido.Name = "rbCompartido";
            this.rbCompartido.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.rbCompartido.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbCompartido.Size = new System.Drawing.Size(133, 18);
            this.rbCompartido.TabIndex = 4;
            this.rbCompartido.Text = "R�gimen Compartido: ";
            this.rbCompartido.CheckStateChanged += new System.EventHandler(this.rbCompartido_CheckedChanged);
            // 
            // rbIndividual
            // 
            this.rbIndividual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbIndividual.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbIndividual.Location = new System.Drawing.Point(4, 10);
            this.rbIndividual.Name = "rbIndividual";
            this.rbIndividual.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.rbIndividual.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbIndividual.Size = new System.Drawing.Size(119, 18);
            this.rbIndividual.TabIndex = 3;
            this.rbIndividual.Text = "R�gimen Individual:";
            this.rbIndividual.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.rbIndividual.CheckStateChanged += new System.EventHandler(this.rbIndividual_CheckedChanged);
            // 
            // tbCama
            // 
            this.tbCama.AcceptsReturn = true;
            this.tbCama.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCama.Location = new System.Drawing.Point(346, 22);
            this.tbCama.MaxLength = 0;
            this.tbCama.Name = "tbCama";
            this.tbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCama.Size = new System.Drawing.Size(97, 20);
            this.tbCama.TabIndex = 5;
            this.tbCama.TextChanged += new System.EventHandler(this.tbCama_TextChanged);
            this.tbCama.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCama_KeyDown);
            this.tbCama.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCama_KeyPress);
            this.tbCama.Leave += new System.EventHandler(this.tbCama_Leave);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(302, 23);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(37, 18);
            this.Label2.TabIndex = 18;
            this.Label2.Text = "Cama:";
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.Frmsexo);
            this.Frame2.Controls.Add(this.Label17);
            this.Frame2.Controls.Add(this.lbNombrePaciente);
            this.Frame2.Controls.Add(this.Label12);
            this.Frame2.Controls.Add(this.lbHistoria);
            this.Frame2.HeaderText = "";
            this.Frame2.Location = new System.Drawing.Point(76, 4);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(461, 76);
            this.Frame2.TabIndex = 12;
            // 
            // Frmsexo
            // 
            this.Frmsexo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frmsexo.Controls.Add(this.RbMujer);
            this.Frmsexo.Controls.Add(this.RbHombre);
            this.Frmsexo.HeaderText = "Sexo";
            this.Frmsexo.Location = new System.Drawing.Point(260, 28);
            this.Frmsexo.Name = "Frmsexo";
            this.Frmsexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frmsexo.Size = new System.Drawing.Size(161, 41);
            this.Frmsexo.TabIndex = 30;
            this.Frmsexo.Text = "Sexo";
            // 
            // RbMujer
            // 
            this.RbMujer.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbMujer.Enabled = false;
            this.RbMujer.Location = new System.Drawing.Point(96, 15);
            this.RbMujer.Name = "RbMujer";
            this.RbMujer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbMujer.Size = new System.Drawing.Size(49, 18);
            this.RbMujer.TabIndex = 32;
            this.RbMujer.Text = "Mujer";
            this.RbMujer.CheckStateChanged += new System.EventHandler(this.RbMujer_CheckedChanged);
            // 
            // RbHombre
            // 
            this.RbHombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbHombre.Enabled = false;
            this.RbHombre.Location = new System.Drawing.Point(17, 15);
            this.RbHombre.Name = "RbHombre";
            this.RbHombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbHombre.Size = new System.Drawing.Size(61, 18);
            this.RbHombre.TabIndex = 31;
            this.RbHombre.Text = "Hombre";
            this.RbHombre.CheckStateChanged += new System.EventHandler(this.RbHombre_CheckedChanged);
            // 
            // Label17
            // 
            this.Label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label17.Location = new System.Drawing.Point(8, 8);
            this.Label17.Name = "Label17";
            this.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label17.Size = new System.Drawing.Size(51, 18);
            this.Label17.TabIndex = 14;
            this.Label17.Text = "Paciente:";
            // 
            // lbNombrePaciente
            // 
            this.lbNombrePaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNombrePaciente.Enabled = false;
            this.lbNombrePaciente.Location = new System.Drawing.Point(98, 8);
            this.lbNombrePaciente.Name = "lbNombrePaciente";
            this.lbNombrePaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNombrePaciente.Size = new System.Drawing.Size(355, 20);
            this.lbNombrePaciente.TabIndex = 1;
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(8, 44);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(81, 18);
            this.Label12.TabIndex = 13;
            this.Label12.Text = "Historia cl�nica:";
            // 
            // lbHistoria
            // 
            this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHistoria.Enabled = false;
            this.lbHistoria.Location = new System.Drawing.Point(98, 44);
            this.lbHistoria.Name = "lbHistoria";
            this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHistoria.Size = new System.Drawing.Size(97, 20);
            this.lbHistoria.TabIndex = 2;
            // 
            // CbAceptar
            // 
            this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbAceptar.Location = new System.Drawing.Point(356, 336);
            this.CbAceptar.Name = "CbAceptar";
            this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbAceptar.Size = new System.Drawing.Size(81, 29);
            this.CbAceptar.TabIndex = 6;
            this.CbAceptar.Text = "&Aceptar";
            this.CbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
            // 
            // CbCancelar
            // 
            this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbCancelar.Location = new System.Drawing.Point(454, 336);
            this.CbCancelar.Name = "CbCancelar";
            this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCancelar.Size = new System.Drawing.Size(81, 29);
            this.CbCancelar.TabIndex = 7;
            this.CbCancelar.Text = "&Cancelar";
            this.CbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Frame1);
            this.Frame3.Controls.Add(this.lbResponsable);
            this.Frame3.Controls.Add(this.lbDiagnostico);
            this.Frame3.Controls.Add(this.lbServicio);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.Label3);
            this.Frame3.Controls.Add(this.Label4);
            this.Frame3.HeaderText = "Datos del Episodio de Hospital de D�a";
            this.Frame3.Location = new System.Drawing.Point(4, 84);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(533, 151);
            this.Frame3.TabIndex = 0;
            this.Frame3.Text = "Datos del Episodio de Hospital de D�a";
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.lbFechaIngreso);
            this.Frame1.Controls.Add(this.Label21);
            this.Frame1.HeaderText = "Fecha ingreso";
            this.Frame1.Location = new System.Drawing.Point(4, 100);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(509, 41);
            this.Frame1.TabIndex = 15;
            this.Frame1.Text = "Fecha ingreso";
            // 
            // lbFechaIngreso
            // 
            this.lbFechaIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFechaIngreso.Enabled = false;
            this.lbFechaIngreso.Location = new System.Drawing.Point(193, 15);
            this.lbFechaIngreso.Name = "lbFechaIngreso";
            this.lbFechaIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFechaIngreso.Size = new System.Drawing.Size(117, 20);
            this.lbFechaIngreso.TabIndex = 29;
            // 
            // Label21
            // 
            this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label21.Location = new System.Drawing.Point(110, 15);
            this.Label21.Name = "Label21";
            this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label21.Size = new System.Drawing.Size(78, 18);
            this.Label21.TabIndex = 16;
            this.Label21.Text = "Fecha ingreso:";
            // 
            // lbResponsable
            // 
            this.lbResponsable.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbResponsable.Enabled = false;
            this.lbResponsable.Location = new System.Drawing.Point(125, 68);
            this.lbResponsable.Name = "lbResponsable";
            this.lbResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbResponsable.Size = new System.Drawing.Size(397, 20);
            this.lbResponsable.TabIndex = 28;
            // 
            // lbDiagnostico
            // 
            this.lbDiagnostico.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDiagnostico.Enabled = false;
            this.lbDiagnostico.Location = new System.Drawing.Point(125, 44);
            this.lbDiagnostico.Name = "lbDiagnostico";
            this.lbDiagnostico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDiagnostico.Size = new System.Drawing.Size(397, 20);
            this.lbDiagnostico.TabIndex = 27;
            // 
            // lbServicio
            // 
            this.lbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbServicio.Enabled = false;
            this.lbServicio.Location = new System.Drawing.Point(125, 20);
            this.lbServicio.Name = "lbServicio";
            this.lbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbServicio.Size = new System.Drawing.Size(397, 20);
            this.lbServicio.TabIndex = 26;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 69);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(109, 18);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "M�dico responsable:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(8, 22);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(47, 18);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "Servicio:";
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(8, 46);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(68, 18);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "Diagn�stico:";
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = false;
            this.lbInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbInfo.Location = new System.Drawing.Point(216, 304);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbInfo.Size = new System.Drawing.Size(319, 18);
            this.lbInfo.TabIndex = 20;
            this.lbInfo.Text = "PROCESANDO....";
            this.lbInfo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.lbInfo.Visible = false;
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(40, 4);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(38, 42);
            this.Label6.TabIndex = 11;
            this.Label6.Text = "H";
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(4, 4);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(32, 31);
            this.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 25;
            this.Image1.TabStop = false;
            // 
            // ConversionHDaH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CbCancelar;
            this.ClientSize = new System.Drawing.Size(544, 376);
            this.Controls.Add(this.Fringreso);
            this.Controls.Add(this.FrProceso);
            this.Controls.Add(this.Frame4);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.CbAceptar);
            this.Controls.Add(this.CbCancelar);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Image1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConversionHDaH";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Conversi�n de Episodio de Hospital de D�a a Hospitalizaci�n";
            this.Activated += new System.EventHandler(this.ConversionHDaH_Activated);
            this.Closed += new System.EventHandler(this.ConversionHDaH_Closed);
            this.Load += new System.EventHandler(this.ConversionHDaH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Fringreso)).EndInit();
            this.Fringreso.ResumeLayout(false);
            this.Fringreso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbInmediato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrProceso)).EndInit();
            this.FrProceso.ResumeLayout(false);
            this.FrProceso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbProceso_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbProceso_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbSeleccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbCompartido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbIndividual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frmsexo)).EndInit();
            this.Frmsexo.ResumeLayout(false);
            this.Frmsexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbMujer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbHombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombrePaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbFechaIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbResponsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDiagnostico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializerbProceso();
		}
		void InitializerbProceso()
		{
			this.rbProceso = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbProceso[0] = _rbProceso_0;
			this.rbProceso[1] = _rbProceso_1;
		}
		#endregion
	}
}