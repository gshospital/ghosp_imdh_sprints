using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Justificante_asistencia_acompañante
	{

		#region "Upgrade Support "
		private static Justificante_asistencia_acompañante m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Justificante_asistencia_acompañante DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Justificante_asistencia_acompañante();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "ChbCalificacion", "cmdAceptar", "cmdCancelar", "txtParentesco", "txtDNI", "txtNombre", "lstSinConsen", "lblDNI", "lblParentesco", "lblNombre", "LblSinConsen", "fraDatos", "sdcFechaIni", "mskHora", "lblHora", "lblFecha", "lblPaciente2", "lblPaciente", "listBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadCheckBox ChbCalificacion;
		public Telerik.WinControls.UI.RadButton cmdAceptar;
		public Telerik.WinControls.UI.RadButton cmdCancelar;
		public Telerik.WinControls.UI.RadTextBoxControl txtParentesco;
		public Telerik.WinControls.UI.RadTextBoxControl txtDNI;
		public Telerik.WinControls.UI.RadTextBoxControl txtNombre;
		public Telerik.WinControls.UI.RadListControl lstSinConsen;
		public Telerik.WinControls.UI.RadLabel lblDNI;
		public Telerik.WinControls.UI.RadLabel lblParentesco;
		public Telerik.WinControls.UI.RadLabel lblNombre;
		public Telerik.WinControls.UI.RadLabel LblSinConsen;
		public Telerik.WinControls.UI.RadGroupBox fraDatos;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaIni;
		public Telerik.WinControls.UI.RadMaskedEditBox mskHora;
		public Telerik.WinControls.UI.RadLabel lblHora;
		public Telerik.WinControls.UI.RadLabel lblFecha;
		public Telerik.WinControls.UI.RadLabel lblPaciente2;
		public Telerik.WinControls.UI.RadLabel lblPaciente;
		private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ChbCalificacion = new Telerik.WinControls.UI.RadCheckBox();
            this.cmdAceptar = new Telerik.WinControls.UI.RadButton();
            this.cmdCancelar = new Telerik.WinControls.UI.RadButton();
            this.fraDatos = new Telerik.WinControls.UI.RadGroupBox();
            this.txtParentesco = new Telerik.WinControls.UI.RadTextBoxControl();
            this.txtDNI = new Telerik.WinControls.UI.RadTextBoxControl();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBoxControl();
            this.lstSinConsen = new Telerik.WinControls.UI.RadListControl();
            this.lblDNI = new Telerik.WinControls.UI.RadLabel();
            this.lblParentesco = new Telerik.WinControls.UI.RadLabel();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.LblSinConsen = new Telerik.WinControls.UI.RadLabel();
            this.sdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
            this.mskHora = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblHora = new Telerik.WinControls.UI.RadLabel();
            this.lblFecha = new Telerik.WinControls.UI.RadLabel();
            this.lblPaciente2 = new Telerik.WinControls.UI.RadLabel();
            this.lblPaciente = new Telerik.WinControls.UI.RadLabel();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ChbCalificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDatos)).BeginInit();
            this.fraDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtParentesco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDNI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstSinConsen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDNI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblParentesco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblSinConsen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaIni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mskHora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ChbCalificacion
            // 
            this.ChbCalificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbCalificacion.Location = new System.Drawing.Point(40, 215);
            this.ChbCalificacion.Name = "ChbCalificacion";
            this.ChbCalificacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbCalificacion.Size = new System.Drawing.Size(276, 18);
            this.ChbCalificacion.TabIndex = 16;
            this.ChbCalificacion.Text = "Calificación inicial de la gravedad de la enfermedad";
            this.ChbCalificacion.Visible = false;
            // 
            // cmdAceptar
            // 
            this.cmdAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdAceptar.Location = new System.Drawing.Point(206, 236);
            this.cmdAceptar.Name = "cmdAceptar";
            this.cmdAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdAceptar.Size = new System.Drawing.Size(80, 29);
            this.cmdAceptar.TabIndex = 13;
            this.cmdAceptar.Text = "&Aceptar";
            this.cmdAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cmdAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdAceptar.Click += new System.EventHandler(this.cmdAceptar_Click);
            // 
            // cmdCancelar
            // 
            this.cmdCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdCancelar.Location = new System.Drawing.Point(300, 236);
            this.cmdCancelar.Name = "cmdCancelar";
            this.cmdCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdCancelar.Size = new System.Drawing.Size(80, 29);
            this.cmdCancelar.TabIndex = 14;
            this.cmdCancelar.Text = "&Cancelar";
            this.cmdCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cmdCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdCancelar.Click += new System.EventHandler(this.cmdCancelar_Click);
            // 
            // fraDatos
            // 
            this.fraDatos.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fraDatos.Controls.Add(this.txtParentesco);
            this.fraDatos.Controls.Add(this.txtDNI);
            this.fraDatos.Controls.Add(this.txtNombre);
            this.fraDatos.Controls.Add(this.lstSinConsen);
            this.fraDatos.Controls.Add(this.lblDNI);
            this.fraDatos.Controls.Add(this.lblParentesco);
            this.fraDatos.Controls.Add(this.lblNombre);
            this.fraDatos.Controls.Add(this.LblSinConsen);
            this.fraDatos.HeaderText = "Datos del Familiar";
            this.fraDatos.Location = new System.Drawing.Point(40, 68);
            this.fraDatos.Name = "fraDatos";
            this.fraDatos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraDatos.Size = new System.Drawing.Size(340, 122);
            this.fraDatos.TabIndex = 2;
            this.fraDatos.TabStop = false;
            this.fraDatos.Text = "Datos del Familiar";
            // 
            // txtParentesco
            // 
            this.txtParentesco.AcceptsReturn = true;
            this.txtParentesco.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtParentesco.Location = new System.Drawing.Point(125, 78);
            this.txtParentesco.MaxLength = 15;
            this.txtParentesco.Name = "txtParentesco";
            this.txtParentesco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtParentesco.Size = new System.Drawing.Size(121, 20);
            this.txtParentesco.TabIndex = 6;
            this.txtParentesco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtParentesco_KeyPress);
            // 
            // txtDNI
            // 
            this.txtDNI.AcceptsReturn = true;
            this.txtDNI.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDNI.Location = new System.Drawing.Point(125, 50);
            this.txtDNI.MaxLength = 15;
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDNI.Size = new System.Drawing.Size(121, 20);
            this.txtDNI.TabIndex = 8;
            this.txtDNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // txtNombre
            // 
            this.txtNombre.AcceptsReturn = true;
            this.txtNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNombre.Location = new System.Drawing.Point(125, 23);
            this.txtNombre.MaxLength = 40;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNombre.Size = new System.Drawing.Size(200, 20);
            this.txtNombre.TabIndex = 4;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // lstSinConsen
            // 
            this.lstSinConsen.Cursor = System.Windows.Forms.Cursors.Default;
            this.lstSinConsen.Location = new System.Drawing.Point(125, 74);
            this.lstSinConsen.Name = "lstSinConsen";
            this.lstSinConsen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lstSinConsen.Size = new System.Drawing.Size(210, 43);
            this.lstSinConsen.TabIndex = 17;
            this.lstSinConsen.Visible = false;
            // 
            // lblDNI
            // 
            this.lblDNI.Location = new System.Drawing.Point(21, 57);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(2, 2);
            this.lblDNI.TabIndex = 7;
            // 
            // lblParentesco
            // 
            this.lblParentesco.Location = new System.Drawing.Point(21, 85);
            this.lblParentesco.Name = "lblParentesco";
            this.lblParentesco.Size = new System.Drawing.Size(2, 2);
            this.lblParentesco.TabIndex = 5;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(21, 29);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(2, 2);
            this.lblNombre.TabIndex = 3;
            // 
            // LblSinConsen
            // 
            this.LblSinConsen.Location = new System.Drawing.Point(20, 78);
            this.LblSinConsen.Name = "LblSinConsen";
            this.LblSinConsen.Size = new System.Drawing.Size(2, 2);
            this.LblSinConsen.TabIndex = 15;
            this.LblSinConsen.Visible = false;
            // 
            // sdcFechaIni
            // 
            this.sdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaIni.Location = new System.Drawing.Point(160, 195);
            this.sdcFechaIni.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.sdcFechaIni.Name = "sdcFechaIni";
            this.sdcFechaIni.Size = new System.Drawing.Size(105, 20);
            this.sdcFechaIni.TabIndex = 10;
            this.sdcFechaIni.TabStop = false;            
            this.sdcFechaIni.Value = new System.DateTime(2016, 4, 26, 11, 1, 43, 894);
            // 
            // mskHora
            // 
            this.mskHora.Location = new System.Drawing.Point(312, 195);
            this.mskHora.Mask = "##:##";
            this.mskHora.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.mskHora.Name = "mskHora";
            this.mskHora.PromptChar = ' ';
            this.mskHora.Size = new System.Drawing.Size(36, 20);
            this.mskHora.TabIndex = 12;
            this.mskHora.TabStop = false;
            this.mskHora.Text = "  :  ";
            // 
            // lblHora
            // 
            this.lblHora.Location = new System.Drawing.Point(280, 199);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(2, 2);
            this.lblHora.TabIndex = 11;
            // 
            // lblFecha
            // 
            this.lblFecha.Location = new System.Drawing.Point(60, 199);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(2, 2);
            this.lblFecha.TabIndex = 9;
            // 
            // lblPaciente2
            // 
            this.lblPaciente2.Location = new System.Drawing.Point(40, 32);
            this.lblPaciente2.Name = "lblPaciente2";
            this.lblPaciente2.Size = new System.Drawing.Size(2, 2);
            this.lblPaciente2.TabIndex = 1;
            // 
            // lblPaciente
            // 
            this.lblPaciente.Location = new System.Drawing.Point(40, 10);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(2, 2);
            this.lblPaciente.TabIndex = 0;
            // 
            // Justificante_asistencia_acompañante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 281);
            this.Controls.Add(this.ChbCalificacion);
            this.Controls.Add(this.cmdAceptar);
            this.Controls.Add(this.cmdCancelar);
            this.Controls.Add(this.fraDatos);
            this.Controls.Add(this.sdcFechaIni);
            this.Controls.Add(this.mskHora);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lblPaciente2);
            this.Controls.Add(this.lblPaciente);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Justificante_asistencia_acompañante";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Justificante de Asistencia del Acompañante - AGI12BF1";
            this.Closed += new System.EventHandler(this.Justificante_asistencia_acompañante_Closed);
            this.Load += new System.EventHandler(this.Justificante_asistencia_acompañante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ChbCalificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDatos)).EndInit();
            this.fraDatos.ResumeLayout(false);
            this.fraDatos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtParentesco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDNI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstSinConsen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDNI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblParentesco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblSinConsen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaIni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mskHora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}