using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Cambio_R�gimen
	{

		#region "Upgrade Support "
		private static Cambio_R�gimen m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Cambio_R�gimen DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Cambio_R�gimen();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbHoraCambio", "SdcFecha", "Label21", "Label19", "Frame3", "SprCambios", "cbNuevo", "cbCambio", "cbBorrar", "Frame5", "CbCancelar", "CbAceptar", "LbPaciente", "Label28", "Frame2", "SprCambios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;		
        public Telerik.WinControls.RadToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadMaskedEditBox tbHoraCambio;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFecha;
        public Telerik.WinControls.UI.RadLabel Label21;
        public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public UpgradeHelpers.Spread.FpSpread SprCambios;
		public Telerik.WinControls.UI.RadButton cbNuevo;
		public Telerik.WinControls.UI.RadButton cbCambio;
		public Telerik.WinControls.UI.RadButton cbBorrar;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CbAceptar;
        public Telerik.WinControls.UI.RadTextBox LbPaciente;
        public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadGroupBox Frame2;		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cambio_R�gimen));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbHoraCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
			this.SprCambios = new UpgradeHelpers.Spread.FpSpread();
			this.cbNuevo = new Telerik.WinControls.UI.RadButton();
			this.cbCambio = new Telerik.WinControls.UI.RadButton();
			this.cbBorrar = new Telerik.WinControls.UI.RadButton();
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.Frame3.SuspendLayout();
			this.Frame5.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.tbHoraCambio);
			this.Frame3.Controls.Add(this.SdcFecha);
			this.Frame3.Controls.Add(this.Label21);
			this.Frame3.Controls.Add(this.Label19);
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(8, 172);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(367, 36);
			this.Frame3.TabIndex = 12;
			this.Frame3.Visible = true;
			// 
			// tbHoraCambio
			// 
            this.tbHoraCambio.MaskType = Telerik.WinControls.UI.MaskType.Standard;
			this.tbHoraCambio.AllowPromptAsInput = false;
			this.tbHoraCambio.Location = new System.Drawing.Point(276, 12);
			this.tbHoraCambio.Mask = "99:99";
			this.tbHoraCambio.Name = "tbHoraCambio";
			this.tbHoraCambio.PromptChar = ' ';
			this.tbHoraCambio.Size = new System.Drawing.Size(45, 20);
			this.tbHoraCambio.TabIndex = 5;
			this.tbHoraCambio.Enter += new System.EventHandler(this.tbHoraCambio_Enter);
			this.tbHoraCambio.Leave += new System.EventHandler(this.tbHoraCambio_Leave);
			this.tbHoraCambio.TextChanged += new System.EventHandler(this.tbHoraCambio_TextChanged);
			// 
			// SdcFecha
			// 
            this.SdcFecha.CustomFormat = "dd/MM/yyyy";
			this.SdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFecha.Location = new System.Drawing.Point(90, 12);
			this.SdcFecha.Name = "SdcFecha";
			this.SdcFecha.Size = new System.Drawing.Size(129, 17);
			this.SdcFecha.TabIndex = 4;
			// 
			// Label21
			// 
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label21.Location = new System.Drawing.Point(14, 12);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(89, 17);
			this.Label21.TabIndex = 14;
			this.Label21.Text = "Fecha cambio:";
			// 
			// Label19
			// 
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label19.Location = new System.Drawing.Point(228, 12);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(33, 17);
			this.Label19.TabIndex = 13;
			this.Label19.Text = "Hora:";
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.SprCambios);
			this.Frame5.Controls.Add(this.cbNuevo);
			this.Frame5.Controls.Add(this.cbCambio);
			this.Frame5.Controls.Add(this.cbBorrar);
			this.Frame5.Enabled = true;
			this.Frame5.Location = new System.Drawing.Point(8, 50);
			this.Frame5.Name = "Frame5";
			this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame5.Size = new System.Drawing.Size(367, 121);
			this.Frame5.TabIndex = 11;
			this.Frame5.Visible = true;
			// 
			// SprCambios
			// 
			this.SprCambios.Location = new System.Drawing.Point(10, 16);
			this.SprCambios.Name = "SprCambios";
            this.SprCambios.MaxCols = 3;
			this.SprCambios.Size = new System.Drawing.Size(231, 96);
			this.SprCambios.TabIndex = 0;
            this.SprCambios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(SprCambios_CellClick);
			// 
			// cbNuevo
			// 
			this.cbNuevo.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbNuevo.Location = new System.Drawing.Point(266, 16);
			this.cbNuevo.Name = "cbNuevo";
			this.cbNuevo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbNuevo.Size = new System.Drawing.Size(77, 27);
			this.cbNuevo.TabIndex = 1;
			this.cbNuevo.Text = "&Nuevo";
            this.cbNuevo.Click += new System.EventHandler(this.cbNuevo_Click);
			// 
			// cbCambio
			// 
			this.cbCambio.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCambio.Location = new System.Drawing.Point(266, 84);
			this.cbCambio.Name = "cbCambio";
			this.cbCambio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCambio.Size = new System.Drawing.Size(77, 27);
			this.cbCambio.TabIndex = 3;
			this.cbCambio.Text = "&Cambio";
            this.cbCambio.Click += new System.EventHandler(this.cbCambio_Click);
			// 
			// cbBorrar
			// 
			this.cbBorrar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbBorrar.Location = new System.Drawing.Point(266, 50);
			this.cbBorrar.Name = "cbBorrar";
			this.cbBorrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBorrar.Size = new System.Drawing.Size(77, 27);
			this.cbBorrar.TabIndex = 2;
			this.cbBorrar.Text = "&Eliminar";
            this.cbBorrar.Click += new System.EventHandler(this.cbBorrar_Click);
			// 
			// CbCancelar
			// 
			this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCancelar.Location = new System.Drawing.Point(294, 218);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 29);
			this.CbCancelar.TabIndex = 7;
			this.CbCancelar.Text = "&Cerrar";
            this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
			// 
			// CbAceptar
			// 
			this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbAceptar.Location = new System.Drawing.Point(206, 218);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 29);
			this.CbAceptar.TabIndex = 6;
			this.CbAceptar.Text = "&Aceptar";
            this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.LbPaciente);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(8, 2);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(368, 47);
			this.Frame2.TabIndex = 8;
			this.Frame2.Visible = true;
            // 
            // LbPaciente
            // 
            this.LbPaciente.Enabled = false;
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbPaciente.Location = new System.Drawing.Point(78, 14);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(278, 19);
			this.LbPaciente.TabIndex = 10;
            this.LbPaciente.AutoSize = false;       
			// 
			// Label28
			// 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(14, 14);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(51, 17);
			this.Label28.TabIndex = 9;
			this.Label28.Text = "Paciente:";
			// 
			// Cambio_R�gimen
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(383, 256);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.Frame5);
			this.Controls.Add(this.CbCancelar);
			this.Controls.Add(this.CbAceptar);
			this.Controls.Add(this.Frame2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Cambio_R�gimen";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Cambio de R�gimen - AGI125F1";
			this.Activated += new System.EventHandler(this.Cambio_R�gimen_Activated);
			this.Closed += new System.EventHandler(this.Cambio_R�gimen_Closed);
			this.Load += new System.EventHandler(this.Cambio_R�gimen_Load);
			this.Frame3.ResumeLayout(false);
			this.Frame5.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
            //this.Show();
		}
		#endregion
	}
}