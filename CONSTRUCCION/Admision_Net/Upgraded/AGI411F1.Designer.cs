using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class general_movimiento
	{

		#region "Upgrade Support "
		private static general_movimiento m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static general_movimiento DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new general_movimiento();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "ChBHosDia", "ChBHosGeneral", "FrmHospital", "chbAlta", "chbIngreso", "frmTipo", "SdcFecha", "frmFecha", "Frame1", "CbImprimir", "CbCerrar", "CbPantalla"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadCheckBox ChBHosDia;
		public Telerik.WinControls.UI.RadCheckBox ChBHosGeneral;
		public Telerik.WinControls.UI.RadGroupBox FrmHospital;
        //public AxMSForms.AxCheckBox chbAlta;
        //public AxMSForms.AxCheckBox chbIngreso;

        public Telerik.WinControls.UI.RadCheckBox chbAlta;
        public Telerik.WinControls.UI.RadCheckBox chbIngreso;
		public Telerik.WinControls.UI.RadGroupBox frmTipo;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFecha;
		public Telerik.WinControls.UI.RadGroupBox frmFecha;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.FrmHospital = new Telerik.WinControls.UI.RadGroupBox();
            this.ChBHosDia = new Telerik.WinControls.UI.RadCheckBox();
            this.ChBHosGeneral = new Telerik.WinControls.UI.RadCheckBox();
            this.frmTipo = new Telerik.WinControls.UI.RadGroupBox();
            this.chbAlta = new Telerik.WinControls.UI.RadCheckBox();
            this.chbIngreso = new Telerik.WinControls.UI.RadCheckBox();
            this.frmFecha = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).BeginInit();
            this.FrmHospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTipo)).BeginInit();
            this.frmTipo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmFecha)).BeginInit();
            this.frmFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.FrmHospital);
            this.Frame1.Controls.Add(this.frmTipo);
            this.Frame1.Controls.Add(this.frmFecha);
            this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(3, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(273, 204);
            this.Frame1.TabIndex = 3;
            // 
            // FrmHospital
            // 
            this.FrmHospital.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmHospital.Controls.Add(this.ChBHosDia);
            this.FrmHospital.Controls.Add(this.ChBHosGeneral);
            this.FrmHospital.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FrmHospital.HeaderText = "Tipo de Ingreso";
            this.FrmHospital.Location = new System.Drawing.Point(8, 152);
            this.FrmHospital.Name = "FrmHospital";
            this.FrmHospital.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmHospital.Size = new System.Drawing.Size(259, 41);
            this.FrmHospital.TabIndex = 9;
            this.FrmHospital.Text = "Tipo de Ingreso";
            // 
            // ChBHosDia
            // 
            this.ChBHosDia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosDia.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChBHosDia.Location = new System.Drawing.Point(152, 16);
            this.ChBHosDia.Name = "ChBHosDia";
            this.ChBHosDia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosDia.Size = new System.Drawing.Size(97, 18);
            this.ChBHosDia.TabIndex = 11;
            this.ChBHosDia.Text = "Hospital de D�a";
            this.ChBHosDia.CheckStateChanged += new System.EventHandler(this.ChBHosDia_CheckStateChanged);
            // 
            // ChBHosGeneral
            // 
            this.ChBHosGeneral.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosGeneral.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChBHosGeneral.Location = new System.Drawing.Point(8, 16);
            this.ChBHosGeneral.Name = "ChBHosGeneral";
            this.ChBHosGeneral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosGeneral.Size = new System.Drawing.Size(138, 18);
            this.ChBHosGeneral.TabIndex = 10;
            this.ChBHosGeneral.Text = "Hospitalizaci�n General";
            this.ChBHosGeneral.CheckStateChanged += new System.EventHandler(this.ChBHosGeneral_CheckStateChanged);
            // 
            // frmTipo
            // 
            this.frmTipo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmTipo.Controls.Add(this.chbAlta);
            this.frmTipo.Controls.Add(this.chbIngreso);
            this.frmTipo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmTipo.HeaderText = "Seleccione tipo de movimiento";
            this.frmTipo.Location = new System.Drawing.Point(5, 92);
            this.frmTipo.Name = "frmTipo";
            this.frmTipo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmTipo.Size = new System.Drawing.Size(261, 56);
            this.frmTipo.TabIndex = 6;
            this.frmTipo.Text = "Seleccione tipo de movimiento";
            // 
            // chbAlta
            // 
            this.chbAlta.Location = new System.Drawing.Point(138, 21);
            this.chbAlta.Name = "chbAlta";
            this.chbAlta.Size = new System.Drawing.Size(40, 18);
            this.chbAlta.TabIndex = 8;
            this.chbAlta.Text = "Alta";
            this.chbAlta.CheckStateChanged += new System.EventHandler(chbAlta_CheckStateChanged);
            // 
            // chbIngreso
            // 
            this.chbIngreso.Location = new System.Drawing.Point(24, 21);
            this.chbIngreso.Name = "chbIngreso";
            this.chbIngreso.Size = new System.Drawing.Size(58, 18);
            this.chbIngreso.TabIndex = 7;
            this.chbIngreso.Text = "Ingreso";
            this.chbIngreso.CheckStateChanged += new System.EventHandler(chbIngreso_CheckStateChanged);
            // 
            // frmFecha
            // 
            this.frmFecha.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmFecha.Controls.Add(this.SdcFecha);
            this.frmFecha.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmFecha.HeaderText = "Seleccione fecha ingreso/alta";
            this.frmFecha.Location = new System.Drawing.Point(8, 16);
            this.frmFecha.Name = "frmFecha";
            this.frmFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmFecha.Size = new System.Drawing.Size(259, 73);
            this.frmFecha.TabIndex = 4;
            this.frmFecha.Text = "Seleccione fecha ingreso/alta";
            // 
            // SdcFecha
            // 
            this.SdcFecha.CustomFormat = "dd/MM/yyyy";
            this.SdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFecha.Location = new System.Drawing.Point(63, 36);
            this.SdcFecha.Name = "SdcFecha";
            this.SdcFecha.Size = new System.Drawing.Size(105, 20);
            this.SdcFecha.TabIndex = 5;
            this.SdcFecha.TabStop = false;
            this.SdcFecha.Text = "22/06/2016";
            this.SdcFecha.Value = new System.DateTime(2016, 6, 22, 15, 50, 10, 573);
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.Location = new System.Drawing.Point(3, 219);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 2;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbImprimir.Click += new System.EventHandler(CbImprimir_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(195, 219);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 1;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.Location = new System.Drawing.Point(99, 219);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(81, 29);
            this.CbPantalla.TabIndex = 0;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbPantalla.Click += new System.EventHandler(CbPantalla_Click);
            // 
            // general_movimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(280, 255);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbPantalla);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "general_movimiento";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "General de movimientos - AGI411F1";
            this.Closed += new System.EventHandler(this.general_movimiento_Closed);
            this.Load += new System.EventHandler(this.general_movimiento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).EndInit();
            this.FrmHospital.ResumeLayout(false);
            this.FrmHospital.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTipo)).EndInit();
            this.frmTipo.ResumeLayout(false);
            this.frmTipo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmFecha)).EndInit();
            this.frmFecha.ResumeLayout(false);
            this.frmFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}