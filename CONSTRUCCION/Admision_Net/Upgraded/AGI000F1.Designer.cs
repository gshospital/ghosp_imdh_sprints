using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class menu_admision
	{

		#region "Upgrade Support "
		private static menu_admision m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static menu_admision DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new menu_admision();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_mnu_adm001_0", "_mnu_adm001_1", "_mnu_adm001_2", "_mnu_adm001_3", "_mnu_adm001_4", "_mnu_adm001_5", "_mnu_adm00_0", "_mnu_adm011_0", "_mnu_adm011_1", "_mnu_adm011_2", "_mnu_adm0112_0", "_mnu_adm0112_1", "_mnu_adm0112_2", "_mnu_adm011_3", "_mnu_adm011_4", "_mnu_adm011_5", "_mnu_adm011_6", "_mnu_adm011_7", "_mnu_adm011_8", "_mnu_adm011_9", "_mnu_adm011_10", "_mnu_adm011_11", "_mnu_adm011_12", "_mnu_adm011_13", "_mnu_adm011_14", "_mnu_adm011_15", "_mnu_adm011_16", "_mnu_adm011_17", "_mnu_adm011_18", "_mnu_adm011_19", "_mnu_adm011_20", "_mnu_adm011_21", "_mnu_adm01_0", "_mnu_adm012_0", "_mnu_adm012_1", "_mnu_adm012_2", "_mnu_adm012_3", "_mnu_adm012_4", "_mnu_adm012_5", "_mnu_adm012_6", "_mnu_adm012_7", "_mnu_adm012_8", "_mnu_adm012_9", "_mnu_adm01_1", "_mnu_adm013_0", "_mnu_adm013_1", "_mnu_adm01_2", "_mnu_adm01501_0", "_mnu_adm01501_1", "_mnu_adm01501_2", "_mnu_adm01501_3", "_mnu_adm01501_4", "_mnu_adm01501_5", "_mnu_adm01501_6", "_mnu_adm01501_7", "_mnu_adm01501_8", "_mnu_adm01501_9", "_mnu_adm01501_10", "_mnu_adm015_0", "_mnu_adm01601_0", "_mnu_adm01601_1", "_mnu_adm01601_2", "_mnu_adm01601_3", "_mnu_adm01601_4", "_mnu_adm01601_5", "_mnu_adm01601_6", "_mnu_adm01601_7", "_mnu_adm01601_8", "_mnu_adm016_0", "_mnu_adm014_0", "_mnu_adm014_1", "_mnu_adm014_2", "_mnu_adm014_3", "_mnu_adm014_4", "_mnu_adm01401_0", "_mnu_adm01401_1", "_mnu_adm014_5", "_mnu_adm014_6", "_mnu_adm014_7", "_mnu_adm014_8", "_mnu_adm014_9", "_mnu_adm014_10", "_mnu_adm014_11", "_mnu_adm014_12", "_mnu_adm014_13", "_mnu_adm014_14", "_mnu_adm014_15", "_mnu_adm014_16", "_mnu_adm014_17", "_mnu_adm01_3", "_mnu_adm0102_0", "_mnu_adm0102_1", "_mnu_adm0102_2", "_mnu_adm0102_3", "_mnu_adm0102_4", "_mnu_adm0102_5", "_mnu_adm0102_6", "_mnu_adm0102_7", "_mnu_adm01_4", "_mnu_pedidos_0", "_mnu_pedidos_1", "_mnu_adm01_5", "_mnu_adm017_1", "_mnu_adm017_2", "_mnu_adm017_3", "_mnu_adm017_4", "_mnu_adm01_6", "_mnu_adm018_1", "_mnu_adm018_2", "_mnu_adm01801_1", "_mnu_adm01801_2", "_mnu_adm01801_3", "_mnu_adm01801_4", "_mnu_adm018_3", "_mnu_adm018_4", "_mnu_adm018_5", "_mnu_adm018_6", "_mnu_adm01_7", "_mnu_adm0108_1", "_mnu_adm0108_2", "_mnu_adm01_8", "_mnu_tto_1", "_mnu_tto_2", "_mnu_tto_3", "_mnu_adm01_9", "_mnu_FSI_1", "_mnu_FSI_2", "_mnu_FSI_3", "_mnu_adm01_10", "_mnu_adm01_11", "mnu_adm0", "mnu_adm1", "mnu_adm21", "mnu_adm22", "mnu_adm2", "mnu_adm31", "mnu_adm3", "MainMenu1", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "_Toolbar1_Button4", "_Toolbar1_Button5", "_Toolbar1_Button6", "_Toolbar1_Button7", "Toolbar1", "tmrPrincipal", "crystalgeneral", "ImageList1", "mnu_FSI", "mnu_adm00", "mnu_adm001", "mnu_adm01", "mnu_adm0102", "mnu_adm0108", "mnu_adm011", "mnu_adm0112", "mnu_adm012", "mnu_adm013", "mnu_adm014", "mnu_adm01401", "mnu_adm015", "mnu_adm01501", "mnu_adm016", "mnu_adm01601", "mnu_adm017", "mnu_adm018", "mnu_adm01801", "mnu_pedidos", "mnu_tto"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm001_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm001_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm001_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm001_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm001_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm001_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm00_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0112_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0112_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0112_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_8;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_9;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_10;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_11;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_12;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_13;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_14;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_15;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_16;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _mnu_adm011_17;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_18;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_19;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_20;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm011_21;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_8;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm012_9;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm013_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm013_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_8;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_9;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01501_10;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm015_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01601_8;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm016_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01401_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01401_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_8;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_9;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_10;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_11;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_12;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_13;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_14;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_15;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_16;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm014_17;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0102_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_pedidos_0;
		private Telerik.WinControls.UI.RadMenuItem _mnu_pedidos_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm017_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm017_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm017_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm017_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm018_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm018_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01801_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01801_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01801_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01801_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm018_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm018_4;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm018_5;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm018_6;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_7;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0108_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm0108_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_8;
		private Telerik.WinControls.UI.RadMenuItem _mnu_tto_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_tto_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_tto_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_9;
		private Telerik.WinControls.UI.RadMenuItem _mnu_FSI_1;
		private Telerik.WinControls.UI.RadMenuItem _mnu_FSI_2;
		private Telerik.WinControls.UI.RadMenuItem _mnu_FSI_3;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_10;
		private Telerik.WinControls.UI.RadMenuItem _mnu_adm01_11;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm0;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm1;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm21;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm22;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm2;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm31;
		public Telerik.WinControls.UI.RadMenuItem mnu_adm3;
		public Telerik.WinControls.UI.RadMenu MainMenu1;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button2;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button4;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _Toolbar1_Button5;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button6;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button7;
		public Telerik.WinControls.UI.RadCommandBar Toolbar1;
		public System.Windows.Forms.Timer tmrPrincipal;
		public System.Windows.Forms.OpenFileDialog CommDiag_impreOpen;
		public System.Windows.Forms.SaveFileDialog CommDiag_impreSave;
		public System.Windows.Forms.FontDialog CommDiag_impreFont;
		public System.Windows.Forms.ColorDialog CommDiag_impreColor;
		public System.Windows.Forms.PrintDialog CommDiag_imprePrint;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        public CrystalWrapper.Crystal crystalgeneral = new CrystalWrapper.Crystal();
        public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		public System.Windows.Forms.ImageList ImageList1;
		public Telerik.WinControls.RadItem[] mnu_FSI = new Telerik.WinControls.RadItem[4];
		public Telerik.WinControls.RadItem[] mnu_adm00 = new Telerik.WinControls.RadItem[1];
		public Telerik.WinControls.RadItem[] mnu_adm001 = new Telerik.WinControls.RadItem[6];
		public Telerik.WinControls.RadItem[] mnu_adm01 = new Telerik.WinControls.RadItem[12];
		public Telerik.WinControls.RadItem[] mnu_adm0102 = new Telerik.WinControls.RadItem[8];
		public Telerik.WinControls.RadItem[] mnu_adm0108 = new Telerik.WinControls.RadItem[3];
		public Telerik.WinControls.RadItem[] mnu_adm011 = new Telerik.WinControls.RadItem[22];
		public Telerik.WinControls.RadItem[] mnu_adm0112 = new Telerik.WinControls.RadItem[3];
		public Telerik.WinControls.RadItem[] mnu_adm012 = new Telerik.WinControls.RadItem[10];
		public Telerik.WinControls.RadItem[] mnu_adm013 = new Telerik.WinControls.RadItem[2];
		public Telerik.WinControls.RadItem[] mnu_adm014 = new Telerik.WinControls.RadItem[18];
		public Telerik.WinControls.RadItem[] mnu_adm01401 = new Telerik.WinControls.RadItem[2];
		public Telerik.WinControls.RadItem[] mnu_adm015 = new Telerik.WinControls.RadItem[1];
		public Telerik.WinControls.RadItem[] mnu_adm01501 = new Telerik.WinControls.RadItem[11];
		public Telerik.WinControls.RadItem[] mnu_adm016 = new Telerik.WinControls.RadItem[1];
		public Telerik.WinControls.RadItem[] mnu_adm01601 = new Telerik.WinControls.RadItem[9];
		public Telerik.WinControls.RadItem[] mnu_adm017 = new Telerik.WinControls.RadItem[5];
		public Telerik.WinControls.RadItem[] mnu_adm018 = new Telerik.WinControls.RadItem[7];
		public Telerik.WinControls.RadItem[] mnu_adm01801 = new Telerik.WinControls.RadItem[5];
		public Telerik.WinControls.RadItem[] mnu_pedidos = new Telerik.WinControls.RadItem[2];
		public Telerik.WinControls.RadItem[] mnu_tto = new Telerik.WinControls.RadItem[4];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu_admision));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.MainMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.mnu_adm0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm00_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm001_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm001_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm001_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm001_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm001_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm001_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0112_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0112_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0112_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_9 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_10 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_11 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_12 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_13 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_14 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_15 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_16 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_17 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._mnu_adm011_18 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_19 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_20 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm011_21 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm012_9 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm013_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm013_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm015_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_9 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01501_10 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm016_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01601_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01401_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01401_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_9 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_10 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_11 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_12 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_13 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_14 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_15 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_16 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm014_17 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0102_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_pedidos_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_pedidos_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm017_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm017_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm017_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm017_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm018_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm018_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm018_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01801_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01801_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01801_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01801_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm018_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm018_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm018_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0108_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm0108_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_9 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_tto_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_tto_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_tto_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_10 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_FSI_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_FSI_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_FSI_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnu_adm01_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_adm1 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_adm2 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_adm21 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_adm22 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_adm3 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_adm31 = new Telerik.WinControls.UI.RadMenuItem();
            this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button2 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button4 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button6 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button7 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.tmrPrincipal = new System.Windows.Forms.Timer(this.components);
            this.CommDiag_impreOpen = new System.Windows.Forms.OpenFileDialog();
            this.CommDiag_impreSave = new System.Windows.Forms.SaveFileDialog();
            this.CommDiag_impreFont = new System.Windows.Forms.FontDialog();
            this.CommDiag_impreColor = new System.Windows.Forms.ColorDialog();
            this.CommDiag_imprePrint = new System.Windows.Forms.PrintDialog();
            this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
            this.CDAyudaFont = new System.Windows.Forms.FontDialog();
            this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
            this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MainMenu1
            // 
            this.MainMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnu_adm0,
            this.mnu_adm1,
            this.mnu_adm2,
            this.mnu_adm3});
            this.MainMenu1.Location = new System.Drawing.Point(0, 0);
            this.MainMenu1.Name = "MainMenu1";
            this.MainMenu1.Size = new System.Drawing.Size(515, 20);
            this.MainMenu1.TabIndex = 1;
            // 
            // mnu_adm0
            // 
            this.mnu_adm0.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm00_0,
            this._mnu_adm01_0,
            this._mnu_adm01_1,
            this._mnu_adm01_2,
            this._mnu_adm01_3,
            this._mnu_adm01_4,
            this._mnu_adm01_5,
            this._mnu_adm01_6,
            this._mnu_adm01_7,
            this._mnu_adm01_8,
            this._mnu_adm01_9,
            this._mnu_adm01_10,
            this._mnu_adm01_11});
            this.mnu_adm0.Name = "mnu_adm0";
            this.mnu_adm0.Text = "&Admisi�n";
            // 
            // _mnu_adm00_0
            // 
            this._mnu_adm00_0.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm001_0,
            this._mnu_adm001_1,
            this._mnu_adm001_2,
            this._mnu_adm001_3,
            this._mnu_adm001_4,
            this._mnu_adm001_5});
            this._mnu_adm00_0.Name = "_mnu_adm00_0";
            this._mnu_adm00_0.Text = "&Reserva";
            // 
            // _mnu_adm001_0
            // 
            this._mnu_adm001_0.Name = "_mnu_adm001_0";
            this._mnu_adm001_0.Text = "Reservas &pendientes";
            this._mnu_adm001_0.Click += new System.EventHandler(this.mnu_adm001_Click);
            // 
            // _mnu_adm001_1
            // 
            this._mnu_adm001_1.Enabled = false;
            this._mnu_adm001_1.Name = "_mnu_adm001_1";
            this._mnu_adm001_1.Text = "Nue&va";
            this._mnu_adm001_1.Click += new System.EventHandler(this.mnu_adm001_Click);
            // 
            // _mnu_adm001_2
            // 
            this._mnu_adm001_2.Enabled = false;
            this._mnu_adm001_2.Name = "_mnu_adm001_2";
            this._mnu_adm001_2.Text = "Modi&ficaci�n ";
            this._mnu_adm001_2.Click += new System.EventHandler(this.mnu_adm001_Click);
            // 
            // _mnu_adm001_3
            // 
            this._mnu_adm001_3.Enabled = false;
            this._mnu_adm001_3.Name = "_mnu_adm001_3";
            this._mnu_adm001_3.Text = "&Anulaci�n";
            this._mnu_adm001_3.Click += new System.EventHandler(this.mnu_adm001_Click);
            // 
            // _mnu_adm001_4
            // 
            this._mnu_adm001_4.Enabled = false;
            this._mnu_adm001_4.Name = "_mnu_adm001_4";
            this._mnu_adm001_4.Text = "&Documentos";
            this._mnu_adm001_4.Click += new System.EventHandler(this.mnu_adm001_Click);
            // 
            // _mnu_adm001_5
            // 
            this._mnu_adm001_5.Enabled = false;
            this._mnu_adm001_5.Name = "_mnu_adm001_5";
            this._mnu_adm001_5.Text = "&Mantenimiento de datos de filiaci�n";
            this._mnu_adm001_5.Click += new System.EventHandler(this.mnu_adm001_Click);
            // 
            // _mnu_adm01_0
            // 
            this._mnu_adm01_0.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm011_0,
            this._mnu_adm011_1,
            this._mnu_adm011_2,
            this._mnu_adm011_3,
            this._mnu_adm011_4,
            this._mnu_adm011_5,
            this._mnu_adm011_6,
            this._mnu_adm011_7,
            this._mnu_adm011_8,
            this._mnu_adm011_9,
            this._mnu_adm011_10,
            this._mnu_adm011_11,
            this._mnu_adm011_12,
            this._mnu_adm011_13,
            this._mnu_adm011_14,
            this._mnu_adm011_15,
            this._mnu_adm011_16,
            this._mnu_adm011_17,
            this._mnu_adm011_18,
            this._mnu_adm011_19,
            this._mnu_adm011_20,
            this._mnu_adm011_21});
            this._mnu_adm01_0.Name = "_mnu_adm01_0";
            this._mnu_adm01_0.Text = "&Ingreso";
            this._mnu_adm01_0.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm011_0
            // 
            this._mnu_adm011_0.Name = "_mnu_adm011_0";
            this._mnu_adm011_0.Text = " &Pacientes Ingresados";
            this._mnu_adm011_0.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_1
            // 
            this._mnu_adm011_1.Enabled = false;
            this._mnu_adm011_1.Name = "_mnu_adm011_1";
            this._mnu_adm011_1.Text = "&Nuevo Paciente";
            this._mnu_adm011_1.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_2
            // 
            this._mnu_adm011_2.Enabled = false;
            this._mnu_adm011_2.Name = "_mnu_adm011_2";
            this._mnu_adm011_2.Text = "&Documentos";
            this._mnu_adm011_2.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_3
            // 
            this._mnu_adm011_3.Enabled = false;
            this._mnu_adm011_3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm0112_0,
            this._mnu_adm0112_1,
            this._mnu_adm0112_2});
            this._mnu_adm011_3.Name = "_mnu_adm011_3";
            this._mnu_adm011_3.Text = "&Mantenimiento datos paciente";
            this._mnu_adm011_3.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm0112_0
            // 
            this._mnu_adm0112_0.Name = "_mnu_adm0112_0";
            this._mnu_adm0112_0.Text = "Datos &Filiaci�n";
            this._mnu_adm0112_0.Click += new System.EventHandler(this.mnu_adm0112_Click);
            // 
            // _mnu_adm0112_1
            // 
            this._mnu_adm0112_1.Name = "_mnu_adm0112_1";
            this._mnu_adm0112_1.Text = "Datos &Ingreso";
            this._mnu_adm0112_1.Click += new System.EventHandler(this.mnu_adm0112_Click);
            // 
            // _mnu_adm0112_2
            // 
            this._mnu_adm0112_2.Name = "_mnu_adm0112_2";
            this._mnu_adm0112_2.Text = "Cam&biar Datos Identificaci�n Paciente";
            this._mnu_adm0112_2.Click += new System.EventHandler(this.mnu_adm0112_Click);
            // 
            // _mnu_adm011_4
            // 
            this._mnu_adm011_4.Enabled = false;
            this._mnu_adm011_4.Name = "_mnu_adm011_4";
            this._mnu_adm011_4.Text = "Cambio de &Servicio/M�dico";
            this._mnu_adm011_4.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_5
            // 
            this._mnu_adm011_5.Enabled = false;
            this._mnu_adm011_5.Name = "_mnu_adm011_5";
            this._mnu_adm011_5.Text = "Cambio de &Entidad";
            this._mnu_adm011_5.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_6
            // 
            this._mnu_adm011_6.Enabled = false;
            this._mnu_adm011_6.Name = "_mnu_adm011_6";
            this._mnu_adm011_6.Text = "A&nulaci�n";
            this._mnu_adm011_6.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_7
            // 
            this._mnu_adm011_7.Enabled = false;
            this._mnu_adm011_7.Name = "_mnu_adm011_7";
            this._mnu_adm011_7.Text = "A&usencias Temporales";
            this._mnu_adm011_7.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_8
            // 
            this._mnu_adm011_8.Enabled = false;
            this._mnu_adm011_8.Name = "_mnu_adm011_8";
            this._mnu_adm011_8.Text = "Listados de Pacien&tes";
            this._mnu_adm011_8.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_9
            // 
            this._mnu_adm011_9.Enabled = false;
            this._mnu_adm011_9.Name = "_mnu_adm011_9";
            this._mnu_adm011_9.Text = "&Cambio de R�&gimen";
            this._mnu_adm011_9.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_10
            // 
            this._mnu_adm011_10.Enabled = false;
            this._mnu_adm011_10.Name = "_mnu_adm011_10";
            this._mnu_adm011_10.Text = "&Accidente laboral";
            this._mnu_adm011_10.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_11
            // 
            this._mnu_adm011_11.Enabled = false;
            this._mnu_adm011_11.Name = "_mnu_adm011_11";
            this._mnu_adm011_11.Text = "Acci&dente tr�fico";
            this._mnu_adm011_11.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_12
            // 
            this._mnu_adm011_12.Enabled = false;
            this._mnu_adm011_12.Name = "_mnu_adm011_12";
            this._mnu_adm011_12.Text = "C&onsumos por paciente";
            this._mnu_adm011_12.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_13
            // 
            this._mnu_adm011_13.Enabled = false;
            this._mnu_adm011_13.Name = "_mnu_adm011_13";
            this._mnu_adm011_13.Text = "Registro de datos de resoluci�n del &Juzgado";
            this._mnu_adm011_13.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_14
            // 
            this._mnu_adm011_14.Name = "_mnu_adm011_14";
            this._mnu_adm011_14.Text = "Control de Citas Externas";
            this._mnu_adm011_14.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_15
            // 
            this._mnu_adm011_15.Enabled = false;
            this._mnu_adm011_15.Name = "_mnu_adm011_15";
            this._mnu_adm011_15.Text = "Cambio de &Producto";
            this._mnu_adm011_15.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_16
            // 
            this._mnu_adm011_16.Enabled = false;
            this._mnu_adm011_16.Name = "_mnu_adm011_16";
            this._mnu_adm011_16.Text = "Cambio de R�gimen de Alojamiento";
            this._mnu_adm011_16.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_17
            // 
            this._mnu_adm011_17.Name = "_mnu_adm011_17";
            this._mnu_adm011_17.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._mnu_adm011_17.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_18
            // 
            this._mnu_adm011_18.Enabled = false;
            this._mnu_adm011_18.Name = "_mnu_adm011_18";
            this._mnu_adm011_18.Text = "Ingreso Plaza de D�a";
            this._mnu_adm011_18.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_19
            // 
            this._mnu_adm011_19.Enabled = false;
            this._mnu_adm011_19.Name = "_mnu_adm011_19";
            this._mnu_adm011_19.Text = "Conversi�n de Episodio de Hospital de D�a a Hospitalizaci�n";
            this._mnu_adm011_19.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_20
            // 
            this._mnu_adm011_20.Enabled = false;
            this._mnu_adm011_20.Name = "_mnu_adm011_20";
            this._mnu_adm011_20.Text = "Presupuestos y dep�sitos";
            this._mnu_adm011_20.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm011_21
            // 
            this._mnu_adm011_21.Enabled = false;
            this._mnu_adm011_21.Name = "_mnu_adm011_21";
            this._mnu_adm011_21.Text = "Documentaci�n a&gregada al sobre de Historia Cl�nica";
            this._mnu_adm011_21.Click += new System.EventHandler(this.mnu_adm011_Click);
            // 
            // _mnu_adm01_1
            // 
            this._mnu_adm01_1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm012_0,
            this._mnu_adm012_1,
            this._mnu_adm012_2,
            this._mnu_adm012_3,
            this._mnu_adm012_4,
            this._mnu_adm012_5,
            this._mnu_adm012_6,
            this._mnu_adm012_7,
            this._mnu_adm012_8,
            this._mnu_adm012_9});
            this._mnu_adm01_1.Name = "_mnu_adm01_1";
            this._mnu_adm01_1.Text = "&Altas";
            this._mnu_adm01_1.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm012_0
            // 
            this._mnu_adm012_0.Name = "_mnu_adm012_0";
            this._mnu_adm012_0.Text = "&Gesti�n Altas";
            this._mnu_adm012_0.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_1
            // 
            this._mnu_adm012_1.Enabled = false;
            this._mnu_adm012_1.Name = "_mnu_adm012_1";
            this._mnu_adm012_1.Text = "A&ltas";
            this._mnu_adm012_1.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_2
            // 
            this._mnu_adm012_2.Name = "_mnu_adm012_2";
            this._mnu_adm012_2.Text = "&Documentos ";
            this._mnu_adm012_2.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_3
            // 
            this._mnu_adm012_3.Name = "_mnu_adm012_3";
            this._mnu_adm012_3.Text = "A&nulaci�n";
            this._mnu_adm012_3.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_4
            // 
            this._mnu_adm012_4.Name = "_mnu_adm012_4";
            this._mnu_adm012_4.Text = "Cambio de &entidad ";
            this._mnu_adm012_4.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_5
            // 
            this._mnu_adm012_5.Name = "_mnu_adm012_5";
            this._mnu_adm012_5.Text = "Cambio de &Servicio/M�dico";
            this._mnu_adm012_5.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_6
            // 
            this._mnu_adm012_6.Name = "_mnu_adm012_6";
            this._mnu_adm012_6.Text = "C&onsumos por paciente";
            this._mnu_adm012_6.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_7
            // 
            this._mnu_adm012_7.Name = "_mnu_adm012_7";
            this._mnu_adm012_7.Text = "Mantenimiento datos Filiaci�n";
            this._mnu_adm012_7.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_8
            // 
            this._mnu_adm012_8.Name = "_mnu_adm012_8";
            this._mnu_adm012_8.Text = "Listados del Paciente";
            this._mnu_adm012_8.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm012_9
            // 
            this._mnu_adm012_9.Name = "_mnu_adm012_9";
            this._mnu_adm012_9.Text = "Conversi�n de Episodio de Hospitalizaci�n a  Hospital de D�a";
            this._mnu_adm012_9.Click += new System.EventHandler(this.mnu_adm012_Click);
            // 
            // _mnu_adm01_2
            // 
            this._mnu_adm01_2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm013_0,
            this._mnu_adm013_1});
            this._mnu_adm01_2.Name = "_mnu_adm01_2";
            this._mnu_adm01_2.Text = "&Camas";
            this._mnu_adm01_2.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm013_0
            // 
            this._mnu_adm013_0.Name = "_mnu_adm013_0";
            this._mnu_adm013_0.Text = "&Mantenimiento";
            this._mnu_adm013_0.Click += new System.EventHandler(this.mnu_adm013_Click);
            // 
            // _mnu_adm013_1
            // 
            this._mnu_adm013_1.Name = "_mnu_adm013_1";
            this._mnu_adm013_1.Text = "Listado &General de camas";
            this._mnu_adm013_1.Click += new System.EventHandler(this.mnu_adm013_Click);
            // 
            // _mnu_adm01_3
            // 
            this._mnu_adm01_3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm015_0,
            this._mnu_adm016_0,
            this._mnu_adm014_0,
            this._mnu_adm014_1,
            this._mnu_adm014_2,
            this._mnu_adm014_3,
            this._mnu_adm014_4,
            this._mnu_adm014_5,
            this._mnu_adm014_6,
            this._mnu_adm014_7,
            this._mnu_adm014_8,
            this._mnu_adm014_9,
            this._mnu_adm014_10,
            this._mnu_adm014_11,
            this._mnu_adm014_12,
            this._mnu_adm014_13,
            this._mnu_adm014_14,
            this._mnu_adm014_15,
            this._mnu_adm014_16,
            this._mnu_adm014_17});
            this._mnu_adm01_3.Name = "_mnu_adm01_3";
            this._mnu_adm01_3.Text = "&Listados y Localizaci�n de pacientes";
            this._mnu_adm01_3.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm015_0
            // 
            this._mnu_adm015_0.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm01501_0,
            this._mnu_adm01501_1,
            this._mnu_adm01501_2,
            this._mnu_adm01501_3,
            this._mnu_adm01501_4,
            this._mnu_adm01501_5,
            this._mnu_adm01501_6,
            this._mnu_adm01501_7,
            this._mnu_adm01501_8,
            this._mnu_adm01501_9,
            this._mnu_adm01501_10});
            this._mnu_adm015_0.Name = "_mnu_adm015_0";
            this._mnu_adm015_0.Text = "Listado de &Ingresados";
            // 
            // _mnu_adm01501_0
            // 
            this._mnu_adm01501_0.Name = "_mnu_adm01501_0";
            this._mnu_adm01501_0.Text = "Listado general de Pacientes &Ingresados";
            this._mnu_adm01501_0.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_1
            // 
            this._mnu_adm01501_1.Name = "_mnu_adm01501_1";
            this._mnu_adm01501_1.Text = "&Traslados realizados";
            this._mnu_adm01501_1.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_2
            // 
            this._mnu_adm01501_2.Name = "_mnu_adm01501_2";
            this._mnu_adm01501_2.Text = "Listado de Ingresados por &R�gimen de Alojamiento";
            this._mnu_adm01501_2.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_3
            // 
            this._mnu_adm01501_3.Name = "_mnu_adm01501_3";
            this._mnu_adm01501_3.Text = "Listado de Ingresados por Plan&tas";
            this._mnu_adm01501_3.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_4
            // 
            this._mnu_adm01501_4.Name = "_mnu_adm01501_4";
            this._mnu_adm01501_4.Text = "Listado Ingresados por &Servicio<->Entidad";
            this._mnu_adm01501_4.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_5
            // 
            this._mnu_adm01501_5.Name = "_mnu_adm01501_5";
            this._mnu_adm01501_5.Text = "Listado de Ingresados por &Unidad de Enfermer�a";
            this._mnu_adm01501_5.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_6
            // 
            this._mnu_adm01501_6.Name = "_mnu_adm01501_6";
            this._mnu_adm01501_6.Text = "Listado de Ingresados S&eguridad Social";
            this._mnu_adm01501_6.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_7
            // 
            this._mnu_adm01501_7.Name = "_mnu_adm01501_7";
            this._mnu_adm01501_7.Text = "Listado de Ingresados por &Localidad";
            this._mnu_adm01501_7.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_8
            // 
            this._mnu_adm01501_8.Name = "_mnu_adm01501_8";
            this._mnu_adm01501_8.Text = "Relaci�n de pacientes con Citas Externas";
            this._mnu_adm01501_8.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_9
            // 
            this._mnu_adm01501_9.Name = "_mnu_adm01501_9";
            this._mnu_adm01501_9.Text = "Listado de Ingresados por CI&AS";
            this._mnu_adm01501_9.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm01501_10
            // 
            this._mnu_adm01501_10.Name = "_mnu_adm01501_10";
            this._mnu_adm01501_10.Text = "Listado de Ingresados con Cobertura Farmacol�gica";
            this._mnu_adm01501_10.Click += new System.EventHandler(this.mnu_adm01501_Click);
            // 
            // _mnu_adm016_0
            // 
            this._mnu_adm016_0.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm01601_0,
            this._mnu_adm01601_1,
            this._mnu_adm01601_2,
            this._mnu_adm01601_3,
            this._mnu_adm01601_4,
            this._mnu_adm01601_5,
            this._mnu_adm01601_6,
            this._mnu_adm01601_7,
            this._mnu_adm01601_8});
            this._mnu_adm016_0.Name = "_mnu_adm016_0";
            this._mnu_adm016_0.Text = "Listados &Oficiales/Documentos externos";
            // 
            // _mnu_adm01601_0
            // 
            this._mnu_adm01601_0.Name = "_mnu_adm01601_0";
            this._mnu_adm01601_0.Text = "Encuesta de Morbilidad Hospitalaria";
            this._mnu_adm01601_0.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_1
            // 
            this._mnu_adm01601_1.Name = "_mnu_adm01601_1";
            this._mnu_adm01601_1.Text = "Mo&vimiento de Pacientes para Seguridad Social";
            this._mnu_adm01601_1.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_2
            // 
            this._mnu_adm01601_2.Name = "_mnu_adm01601_2";
            this._mnu_adm01601_2.Text = "&Fichero para el Mailing";
            this._mnu_adm01601_2.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_3
            // 
            this._mnu_adm01601_3.Name = "_mnu_adm01601_3";
            this._mnu_adm01601_3.Text = "Listado de E&xitus";
            this._mnu_adm01601_3.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_4
            // 
            this._mnu_adm01601_4.Name = "_mnu_adm01601_4";
            this._mnu_adm01601_4.Text = "&Movimiento por Entidades";
            this._mnu_adm01601_4.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_5
            // 
            this._mnu_adm01601_5.Name = "_mnu_adm01601_5";
            this._mnu_adm01601_5.Text = "Listado de Ingresados por &Entidad/Servicio";
            this._mnu_adm01601_5.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_6
            // 
            this._mnu_adm01601_6.Name = "_mnu_adm01601_6";
            this._mnu_adm01601_6.Text = "Relaci�n de Partes Emitidos a &Juzgado";
            this._mnu_adm01601_6.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_7
            // 
            this._mnu_adm01601_7.Name = "_mnu_adm01601_7";
            this._mnu_adm01601_7.Text = "&Notificaciones al juz&gado pendientes de expediente";
            this._mnu_adm01601_7.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm01601_8
            // 
            this._mnu_adm01601_8.Name = "_mnu_adm01601_8";
            this._mnu_adm01601_8.Text = "&Notificaci�n de permanencia en el centro";
            this._mnu_adm01601_8.Click += new System.EventHandler(this.mnu_adm01601_Click);
            // 
            // _mnu_adm014_0
            // 
            this._mnu_adm014_0.Name = "_mnu_adm014_0";
            this._mnu_adm014_0.Text = "&Libro de registro";
            this._mnu_adm014_0.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_1
            // 
            this._mnu_adm014_1.Name = "_mnu_adm014_1";
            this._mnu_adm014_1.Text = "&General de movimiento";
            this._mnu_adm014_1.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_2
            // 
            this._mnu_adm014_2.Name = "_mnu_adm014_2";
            this._mnu_adm014_2.Text = "&Previsi�n de altas";
            this._mnu_adm014_2.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_3
            // 
            this._mnu_adm014_3.Name = "_mnu_adm014_3";
            this._mnu_adm014_3.Text = "An&ulaci�n de Ingresos/Altas";
            this._mnu_adm014_3.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_4
            // 
            this._mnu_adm014_4.Name = "_mnu_adm014_4";
            this._mnu_adm014_4.Text = "&Cambio de Servicio/M�dico de pacientes";
            this._mnu_adm014_4.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_5
            // 
            this._mnu_adm014_5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm01401_0,
            this._mnu_adm01401_1});
            this._mnu_adm014_5.Name = "_mnu_adm014_5";
            this._mnu_adm014_5.Text = "Locali&zaci�n de Pacientes";
            this._mnu_adm014_5.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm01401_0
            // 
            this._mnu_adm01401_0.Name = "_mnu_adm01401_0";
            this._mnu_adm01401_0.Text = "Pacientes &Hospitalizados";
            this._mnu_adm01401_0.Click += new System.EventHandler(this.mnu_adm01401_Click);
            // 
            // _mnu_adm01401_1
            // 
            this._mnu_adm01401_1.Name = "_mnu_adm01401_1";
            this._mnu_adm01401_1.Text = "Pacientes &No Hospitalizados";
            this._mnu_adm01401_1.Click += new System.EventHandler(this.mnu_adm01401_Click);
            // 
            // _mnu_adm014_6
            // 
            this._mnu_adm014_6.Name = "_mnu_adm014_6";
            this._mnu_adm014_6.Text = "Ingre&sos/Ingresados por Servicio/M�dico/Entidad";
            this._mnu_adm014_6.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_7
            // 
            this._mnu_adm014_7.Name = "_mnu_adm014_7";
            this._mnu_adm014_7.Text = "Ate&nci�n/Estancias por Servicio/M�dico";
            this._mnu_adm014_7.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_8
            // 
            this._mnu_adm014_8.Name = "_mnu_adm014_8";
            this._mnu_adm014_8.Text = "Consulta de &Ingresos";
            this._mnu_adm014_8.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_9
            // 
            this._mnu_adm014_9.Name = "_mnu_adm014_9";
            this._mnu_adm014_9.Text = "Consulta de cambios de &Entidades por Episodio";
            this._mnu_adm014_9.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_10
            // 
            this._mnu_adm014_10.Name = "_mnu_adm014_10";
            this._mnu_adm014_10.Text = "Programaciones de intervenciones &quir�rgicas";
            this._mnu_adm014_10.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_11
            // 
            this._mnu_adm014_11.Name = "_mnu_adm014_11";
            this._mnu_adm014_11.Text = "Listado de Reser&vas pendientes de Ingreso";
            this._mnu_adm014_11.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_12
            // 
            this._mnu_adm014_12.Name = "_mnu_adm014_12";
            this._mnu_adm014_12.Text = "Pacie&ntes ausentes";
            this._mnu_adm014_12.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_13
            // 
            this._mnu_adm014_13.Name = "_mnu_adm014_13";
            this._mnu_adm014_13.Text = "Listado de Interconsultas de Pacientes";
            this._mnu_adm014_13.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_14
            // 
            this._mnu_adm014_14.Name = "_mnu_adm014_14";
            this._mnu_adm014_14.Text = "Pacientes Referidos a Otros Centros";
            this._mnu_adm014_14.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_15
            // 
            this._mnu_adm014_15.Name = "_mnu_adm014_15";
            this._mnu_adm014_15.Text = "Pacientes Remitidos de Otros Centros";
            this._mnu_adm014_15.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_16
            // 
            this._mnu_adm014_16.Name = "_mnu_adm014_16";
            this._mnu_adm014_16.Text = "Listado de Reservas Anuladas";
            this._mnu_adm014_16.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm014_17
            // 
            this._mnu_adm014_17.Name = "_mnu_adm014_17";
            this._mnu_adm014_17.Text = "Listado de Ambulancias";
            this._mnu_adm014_17.Click += new System.EventHandler(this.mnu_adm014_Click);
            // 
            // _mnu_adm01_4
            // 
            this._mnu_adm01_4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm0102_0,
            this._mnu_adm0102_1,
            this._mnu_adm0102_2,
            this._mnu_adm0102_3,
            this._mnu_adm0102_4,
            this._mnu_adm0102_5,
            this._mnu_adm0102_6,
            this._mnu_adm0102_7});
            this._mnu_adm01_4.Name = "_mnu_adm01_4";
            this._mnu_adm01_4.Text = "&Estad�sticas";
            this._mnu_adm01_4.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm0102_0
            // 
            this._mnu_adm0102_0.Name = "_mnu_adm0102_0";
            this._mnu_adm0102_0.Text = "&Diaria de la actividad";
            this._mnu_adm0102_0.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_1
            // 
            this._mnu_adm0102_1.Name = "_mnu_adm0102_1";
            this._mnu_adm0102_1.Text = "&Ingresos por servicio";
            this._mnu_adm0102_1.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_2
            // 
            this._mnu_adm0102_2.Name = "_mnu_adm0102_2";
            this._mnu_adm0102_2.Text = "&Altas por servicio";
            this._mnu_adm0102_2.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_3
            // 
            this._mnu_adm0102_3.Name = "_mnu_adm0102_3";
            this._mnu_adm0102_3.Text = "Cuadro de &mando general";
            this._mnu_adm0102_3.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_4
            // 
            this._mnu_adm0102_4.Name = "_mnu_adm0102_4";
            this._mnu_adm0102_4.Text = "Cuadro de mando por &unidad de enfermer�a";
            this._mnu_adm0102_4.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_5
            // 
            this._mnu_adm0102_5.Name = "_mnu_adm0102_5";
            this._mnu_adm0102_5.Text = "Cuadro de mando-indicadores diarios";
            this._mnu_adm0102_5.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_6
            // 
            this._mnu_adm0102_6.Name = "_mnu_adm0102_6";
            this._mnu_adm0102_6.Text = "Sondeo de episodios";
            this._mnu_adm0102_6.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm0102_7
            // 
            this._mnu_adm0102_7.Name = "_mnu_adm0102_7";
            this._mnu_adm0102_7.Text = "Notificaciones EDO";
            this._mnu_adm0102_7.Click += new System.EventHandler(this.mnu_adm0102_Click);
            // 
            // _mnu_adm01_5
            // 
            this._mnu_adm01_5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_pedidos_0,
            this._mnu_pedidos_1});
            this._mnu_adm01_5.Name = "_mnu_adm01_5";
            this._mnu_adm01_5.Text = "Pedidos";
            this._mnu_adm01_5.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_pedidos_0
            // 
            this._mnu_pedidos_0.Name = "_mnu_pedidos_0";
            this._mnu_pedidos_0.Text = "Pedidos 1";
            this._mnu_pedidos_0.Click += new System.EventHandler(this.mnu_pedidos_Click);
            // 
            // _mnu_pedidos_1
            // 
            this._mnu_pedidos_1.Name = "_mnu_pedidos_1";
            this._mnu_pedidos_1.Text = "Pedidos 2";
            this._mnu_pedidos_1.Click += new System.EventHandler(this.mnu_pedidos_Click);
            // 
            // _mnu_adm01_6
            // 
            this._mnu_adm01_6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm017_1,
            this._mnu_adm017_2,
            this._mnu_adm017_3,
            this._mnu_adm017_4});
            this._mnu_adm01_6.Name = "_mnu_adm01_6";
            this._mnu_adm01_6.Text = "Proces&os/Programas";
            this._mnu_adm01_6.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm017_1
            // 
            this._mnu_adm017_1.Name = "_mnu_adm017_1";
            this._mnu_adm017_1.Text = "&Abrir";
            this._mnu_adm017_1.Click += new System.EventHandler(this.mnu_adm017_Click);
            // 
            // _mnu_adm017_2
            // 
            this._mnu_adm017_2.Name = "_mnu_adm017_2";
            this._mnu_adm017_2.Text = "&Modificar";
            this._mnu_adm017_2.Click += new System.EventHandler(this.mnu_adm017_Click);
            // 
            // _mnu_adm017_3
            // 
            this._mnu_adm017_3.Name = "_mnu_adm017_3";
            this._mnu_adm017_3.Text = "&Cerrar";
            this._mnu_adm017_3.Click += new System.EventHandler(this.mnu_adm017_Click);
            // 
            // _mnu_adm017_4
            // 
            this._mnu_adm017_4.Name = "_mnu_adm017_4";
            this._mnu_adm017_4.Text = "&Imprimir pacientes con procesos abiertos";
            this._mnu_adm017_4.Click += new System.EventHandler(this.mnu_adm017_Click);
            // 
            // _mnu_adm01_7
            // 
            this._mnu_adm01_7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm018_1,
            this._mnu_adm018_2,
            this._mnu_adm018_3,
            this._mnu_adm018_4,
            this._mnu_adm018_5/*,
            this._mnu_adm018_6*/});
            this._mnu_adm01_7.Name = "_mnu_adm01_7";
            this._mnu_adm01_7.Text = "Lis&ta de Espera";
            this._mnu_adm01_7.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm018_1
            // 
            this._mnu_adm018_1.Name = "_mnu_adm018_1";
            this._mnu_adm018_1.Text = "Gesti�n Programas Marco";
            this._mnu_adm018_1.Click += new System.EventHandler(this.mnu_adm018_Click);
            // 
            // _mnu_adm018_2
            // 
            this._mnu_adm018_2.Name = "_mnu_adm018_2";
            this._mnu_adm018_2.Text = "&Gesti�n Pacientes";
            this._mnu_adm018_2.Click += new System.EventHandler(this.mnu_adm018_Click);
            // 
            // _mnu_adm018_3
            // 
            this._mnu_adm018_3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm01801_1,
            this._mnu_adm01801_2,
            this._mnu_adm01801_3,
            this._mnu_adm01801_4});
            this._mnu_adm018_3.Name = "_mnu_adm018_3";
            this._mnu_adm018_3.Text = "&Listados";
            this._mnu_adm018_3.Click += new System.EventHandler(this.mnu_adm018_Click);
            // 
            // _mnu_adm01801_1
            // 
            this._mnu_adm01801_1.Name = "_mnu_adm01801_1";
            this._mnu_adm01801_1.Text = "Listado de pacientes en lista de espera";
            this._mnu_adm01801_1.Click += new System.EventHandler(this.mnu_adm01801_Click);
            // 
            // _mnu_adm01801_2
            // 
            this._mnu_adm01801_2.Name = "_mnu_adm01801_2";
            this._mnu_adm01801_2.Text = "Listado de pacientes en lista de espera por Servicio";
            this._mnu_adm01801_2.Click += new System.EventHandler(this.mnu_adm01801_Click);
            // 
            // _mnu_adm01801_3
            // 
            this._mnu_adm01801_3.Name = "_mnu_adm01801_3";
            this._mnu_adm01801_3.Text = "Listado de ambulancias";
            this._mnu_adm01801_3.Click += new System.EventHandler(this.mnu_adm01801_Click);
            // 
            // _mnu_adm01801_4
            // 
            this._mnu_adm01801_4.Name = "_mnu_adm01801_4";
            this._mnu_adm01801_4.Text = "Relaci�n de Episodios (Codificados/No Codificados)";
            this._mnu_adm01801_4.Click += new System.EventHandler(this.mnu_adm01801_Click);
            // 
            // _mnu_adm018_4
            // 
            this._mnu_adm018_4.Name = "_mnu_adm018_4";
            this._mnu_adm018_4.Text = "Estad�sticas";
            this._mnu_adm018_4.Click += new System.EventHandler(this.mnu_adm018_Click);
            // 
            // _mnu_adm018_5
            // 
            this._mnu_adm018_5.Name = "_mnu_adm018_5";
            this._mnu_adm018_5.Text = "Descarga LEQ Instituto Madrile�o de la Salud";
            this._mnu_adm018_5.Click += new System.EventHandler(this.mnu_adm018_Click);
            // 
            // _mnu_adm018_6
            // 
            //this._mnu_adm018_6.Name = "_mnu_adm018_6";
            //this._mnu_adm018_6.Text = "Pacientes derivados de RULEQ pendientes inclusi�n LEQ";
            //this._mnu_adm018_6.Click += new System.EventHandler(this.mnu_adm018_Click);
            // 
            // _mnu_adm01_8
            // 
            this._mnu_adm01_8.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_adm0108_1,
            this._mnu_adm0108_2});
            this._mnu_adm01_8.Name = "_mnu_adm01_8";
            this._mnu_adm01_8.Text = "&Documentaci�n Entregada/Aportada Pacientes";
            this._mnu_adm01_8.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_adm0108_1
            // 
            this._mnu_adm0108_1.Name = "_mnu_adm0108_1";
            this._mnu_adm0108_1.Text = "Registro de documentaci�n";
            this._mnu_adm0108_1.Click += new System.EventHandler(this.mnu_adm0108_Click);
            // 
            // _mnu_adm0108_2
            // 
            this._mnu_adm0108_2.Name = "_mnu_adm0108_2";
            this._mnu_adm0108_2.Text = "Consulta de documentaci�n";
            this._mnu_adm0108_2.Click += new System.EventHandler(this.mnu_adm0108_Click);
            // 
            // _mnu_adm01_9
            // 
            this._mnu_adm01_9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_tto_1,
            this._mnu_tto_2,
            this._mnu_tto_3});
            this._mnu_adm01_9.Name = "_mnu_adm01_9";
            this._mnu_adm01_9.Text = "Tratamiento peri�dico en Hospital de D�a";
            this._mnu_adm01_9.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_tto_1
            // 
            this._mnu_tto_1.Name = "_mnu_tto_1";
            this._mnu_tto_1.Text = "Mantenimiento de pacientes en tratamiento peri�dico";
            this._mnu_tto_1.Click += new System.EventHandler(this.mnu_tto_Click);
            // 
            // _mnu_tto_2
            // 
            this._mnu_tto_2.Name = "_mnu_tto_2";
            this._mnu_tto_2.Text = "Ingresos masivos";
            this._mnu_tto_2.Click += new System.EventHandler(this.mnu_tto_Click);
            // 
            // _mnu_tto_3
            // 
            this._mnu_tto_3.Name = "_mnu_tto_3";
            this._mnu_tto_3.Text = "Altas masivas";
            this._mnu_tto_3.Click += new System.EventHandler(this.mnu_tto_Click);
            // 
            // _mnu_adm01_10
            // 
            this._mnu_adm01_10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnu_FSI_1,
            this._mnu_FSI_2,
            this._mnu_FSI_3});
            this._mnu_adm01_10.Name = "_mnu_adm01_10";
            this._mnu_adm01_10.Text = "Factura Sanitaria Informativa";
            this._mnu_adm01_10.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // _mnu_FSI_1
            // 
            this._mnu_FSI_1.Name = "_mnu_FSI_1";
            this._mnu_FSI_1.Text = "Gesti�n de Facturas";
            this._mnu_FSI_1.Click += new System.EventHandler(this.mnu_FSI_Click);
            // 
            // _mnu_FSI_2
            // 
            this._mnu_FSI_2.Name = "_mnu_FSI_2";
            this._mnu_FSI_2.Text = "Impresi�n Masiva";
            this._mnu_FSI_2.Click += new System.EventHandler(this.mnu_FSI_Click);
            // 
            // _mnu_FSI_3
            // 
            this._mnu_FSI_3.Name = "_mnu_FSI_3";
            this._mnu_FSI_3.Text = "Registro Masivo de Entrega";
            this._mnu_FSI_3.Click += new System.EventHandler(this.mnu_FSI_Click);
            // 
            // _mnu_adm01_11
            // 
            this._mnu_adm01_11.Name = "_mnu_adm01_11";
            this._mnu_adm01_11.Text = "&Salir";
            this._mnu_adm01_11.Click += new System.EventHandler(this.mnu_adm01_Click);
            // 
            // mnu_adm1
            // 
            this.mnu_adm1.MdiList = true;
            this.mnu_adm1.Name = "mnu_adm1";
            this.mnu_adm1.Text = "&Ventana";
            // 
            // mnu_adm2
            // 
            this.mnu_adm2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnu_adm21,
            this.mnu_adm22});
            this.mnu_adm2.Name = "mnu_adm2";
            this.mnu_adm2.Text = "A&yuda";
            // 
            // mnu_adm21
            // 
            this.mnu_adm21.Name = "mnu_adm21";
            this.mnu_adm21.Text = "&Temas de ayuda";
            this.mnu_adm21.Click += new System.EventHandler(this.mnu_adm21_Click);
            // 
            // mnu_adm22
            // 
            this.mnu_adm22.Name = "mnu_adm22";
            this.mnu_adm22.Text = "I&ndice ";
            this.mnu_adm22.Click += new System.EventHandler(this.mnu_adm22_Click);
            // 
            // mnu_adm3
            // 
            this.mnu_adm3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnu_adm31});
            this.mnu_adm3.Name = "mnu_adm3";
            this.mnu_adm3.Text = "Otras opciones";
            // 
            // mnu_adm31
            // 
            this.mnu_adm31.Name = "mnu_adm31";
            this.mnu_adm31.Text = "Acceso a otras opciones";
            this.mnu_adm31.Click += new System.EventHandler(this.mnu_adm31_Click);
            // 
            // Toolbar1
            // 
            this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Toolbar1.ImageList = this.ImageList1;
            this.Toolbar1.Location = new System.Drawing.Point(0, 20);
            this.Toolbar1.Name = "Toolbar1";
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.radCommandBarLineElement1});
            this.Toolbar1.Size = new System.Drawing.Size(515, 30);
            this.Toolbar1.TabIndex = 0;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "");
            this.ImageList1.Images.SetKeyName(5, "");
            this.ImageList1.Images.SetKeyName(6, "");
            // 
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this._Toolbar1_Button1,
            this._Toolbar1_Button2,
            this._Toolbar1_Button3,
            this._Toolbar1_Button4,
            this._Toolbar1_Button6,
            this._Toolbar1_Button7});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
            // 
            // 
            // 
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarStripElement1.StretchHorizontally = true;
            this.radCommandBarStripElement1.StretchVertically = true;
            this.radCommandBarStripElement1.Text = "";
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.radCommandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // _Toolbar1_Button1
            // 
            this._Toolbar1_Button1.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button1.Image")));
            this._Toolbar1_Button1.ImageIndex = 0;
            this._Toolbar1_Button1.Name = "_Toolbar1_1";
            this._Toolbar1_Button1.StretchHorizontally = false;
            this._Toolbar1_Button1.StretchVertically = true;
            this._Toolbar1_Button1.Tag = "";
            this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button1.ToolTipText = "Ingreso de pacientes";
            this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button2
            // 
            this._Toolbar1_Button2.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button2.Image")));
            this._Toolbar1_Button2.ImageIndex = 1;
            this._Toolbar1_Button2.Name = "_Toolbar1_2";
            this._Toolbar1_Button2.Tag = "";
            this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button2.ToolTipText = "Alta de pacientes";
            this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button3
            // 
            this._Toolbar1_Button3.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button3.Image")));
            this._Toolbar1_Button3.ImageIndex = 5;
            this._Toolbar1_Button3.Name = "_Toolbar1_3";
            this._Toolbar1_Button3.Tag = "";
            this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button3.ToolTipText = "Mantenimiento de camas";
            this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button4
            // 
            this._Toolbar1_Button4.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button4.Image")));
            this._Toolbar1_Button4.ImageIndex = 6;
            this._Toolbar1_Button4.Name = "_Toolbar1_4";
            this._Toolbar1_Button4.Tag = "";
            this._Toolbar1_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button4.ToolTipText = "Reservas";
            this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button6
            // 
            this._Toolbar1_Button6.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button6.Image")));
            this._Toolbar1_Button6.ImageIndex = 3;
            this._Toolbar1_Button6.Name = "_Toolbar1_6";
            this._Toolbar1_Button6.Tag = "";
            this._Toolbar1_Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button6.ToolTipText = "Ayuda";
            this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button7
            // 
            this._Toolbar1_Button7.Image = ((System.Drawing.Image)(resources.GetObject("_Toolbar1_Button7.Image")));
            this._Toolbar1_Button7.ImageIndex = 4;
            this._Toolbar1_Button7.Name = "_Toolbar1_7";
            this._Toolbar1_Button7.Tag = "";
            this._Toolbar1_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button7.ToolTipText = "Salir";
            this._Toolbar1_Button7.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button5
            // 
            this._Toolbar1_Button5.Name = "_Toolbar1_5";
            this._Toolbar1_Button5.Tag = "";
            this._Toolbar1_Button5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._Toolbar1_Button5.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // tmrPrincipal
            // 
            this.tmrPrincipal.Interval = 1;
            this.tmrPrincipal.Tick += new System.EventHandler(this.tmrPrincipal_Tick);
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            // 
            // menu_admision
            // 
            this.ClientSize = new System.Drawing.Size(515, 389);
            this.Controls.Add(this.Toolbar1);
            this.Controls.Add(this.MainMenu1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Location = new System.Drawing.Point(-13, 81);
            this.Name = "menu_admision";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ADMISI�N - AGI000F1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.menu_admision_Activated);
            this.Closed += new System.EventHandler(this.menu_admision_Closed);
            this.Load += new System.EventHandler(this.menu_admision_Load);
            this.Deactivate += new System.EventHandler(this.menu_admision_Deactivate);
            this.Resize += new System.EventHandler(this.menu_admision_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        void ReLoadForm(bool addEvents)
		{
			Initializemnu_tto();
			Initializemnu_pedidos();
			Initializemnu_adm01801();
			Initializemnu_adm018();
			Initializemnu_adm017();
			Initializemnu_adm01601();
			Initializemnu_adm016();
			Initializemnu_adm01501();
			Initializemnu_adm015();
			Initializemnu_adm01401();
			Initializemnu_adm014();
			Initializemnu_adm013();
			Initializemnu_adm012();
			Initializemnu_adm0112();
			Initializemnu_adm011();
			Initializemnu_adm0108();
			Initializemnu_adm0102();
			Initializemnu_adm01();
			Initializemnu_adm001();
			Initializemnu_adm00();
			Initializemnu_FSI();
		}
		void Initializemnu_tto()
		{
			this.mnu_tto = new Telerik.WinControls.RadItem[4];
			this.mnu_tto[1] = _mnu_tto_1;
			this.mnu_tto[2] = _mnu_tto_2;
			this.mnu_tto[3] = _mnu_tto_3;
		}
		void Initializemnu_pedidos()
		{
			this.mnu_pedidos = new Telerik.WinControls.RadItem[2];
			this.mnu_pedidos[0] = _mnu_pedidos_0;
			this.mnu_pedidos[1] = _mnu_pedidos_1;
		}
		void Initializemnu_adm01801()
		{
			this.mnu_adm01801 = new Telerik.WinControls.RadItem[5];
			this.mnu_adm01801[1] = _mnu_adm01801_1;
			this.mnu_adm01801[2] = _mnu_adm01801_2;
			this.mnu_adm01801[3] = _mnu_adm01801_3;
			this.mnu_adm01801[4] = _mnu_adm01801_4;
		}
		void Initializemnu_adm018()
		{
			this.mnu_adm018 = new Telerik.WinControls.RadItem[7];
			this.mnu_adm018[1] = _mnu_adm018_1;
			this.mnu_adm018[2] = _mnu_adm018_2;
			this.mnu_adm018[3] = _mnu_adm018_3;
			this.mnu_adm018[4] = _mnu_adm018_4;
			this.mnu_adm018[5] = _mnu_adm018_5;
			this.mnu_adm018[6] = _mnu_adm018_6;
		}
		void Initializemnu_adm017()
		{
			this.mnu_adm017 = new Telerik.WinControls.RadItem[5];
			this.mnu_adm017[1] = _mnu_adm017_1;
			this.mnu_adm017[2] = _mnu_adm017_2;
			this.mnu_adm017[3] = _mnu_adm017_3;
			this.mnu_adm017[4] = _mnu_adm017_4;
		}
		void Initializemnu_adm01601()
		{
			this.mnu_adm01601 = new Telerik.WinControls.RadItem[9];
			this.mnu_adm01601[0] = _mnu_adm01601_0;
			this.mnu_adm01601[1] = _mnu_adm01601_1;
			this.mnu_adm01601[2] = _mnu_adm01601_2;
			this.mnu_adm01601[3] = _mnu_adm01601_3;
			this.mnu_adm01601[4] = _mnu_adm01601_4;
			this.mnu_adm01601[5] = _mnu_adm01601_5;
			this.mnu_adm01601[6] = _mnu_adm01601_6;
			this.mnu_adm01601[7] = _mnu_adm01601_7;
			this.mnu_adm01601[8] = _mnu_adm01601_8;
		}
		void Initializemnu_adm016()
		{
			this.mnu_adm016 = new Telerik.WinControls.RadItem[1];
			this.mnu_adm016[0] = _mnu_adm016_0;
		}
		void Initializemnu_adm01501()
		{
			this.mnu_adm01501 = new Telerik.WinControls.RadItem[11];
			this.mnu_adm01501[0] = _mnu_adm01501_0;
			this.mnu_adm01501[1] = _mnu_adm01501_1;
			this.mnu_adm01501[2] = _mnu_adm01501_2;
			this.mnu_adm01501[3] = _mnu_adm01501_3;
			this.mnu_adm01501[4] = _mnu_adm01501_4;
			this.mnu_adm01501[5] = _mnu_adm01501_5;
			this.mnu_adm01501[6] = _mnu_adm01501_6;
			this.mnu_adm01501[7] = _mnu_adm01501_7;
			this.mnu_adm01501[8] = _mnu_adm01501_8;
			this.mnu_adm01501[9] = _mnu_adm01501_9;
			this.mnu_adm01501[10] = _mnu_adm01501_10;
		}
		void Initializemnu_adm015()
		{
			this.mnu_adm015 = new Telerik.WinControls.RadItem[1];
			this.mnu_adm015[0] = _mnu_adm015_0;
		}
		void Initializemnu_adm01401()
		{
			this.mnu_adm01401 = new Telerik.WinControls.RadItem[2];
			this.mnu_adm01401[0] = _mnu_adm01401_0;
			this.mnu_adm01401[1] = _mnu_adm01401_1;
		}
		void Initializemnu_adm014()
		{
			this.mnu_adm014 = new Telerik.WinControls.RadItem[18];
			this.mnu_adm014[0] = _mnu_adm014_0;
			this.mnu_adm014[1] = _mnu_adm014_1;
			this.mnu_adm014[2] = _mnu_adm014_2;
			this.mnu_adm014[3] = _mnu_adm014_3;
			this.mnu_adm014[4] = _mnu_adm014_4;
			this.mnu_adm014[5] = _mnu_adm014_5;
			this.mnu_adm014[6] = _mnu_adm014_6;
			this.mnu_adm014[7] = _mnu_adm014_7;
			this.mnu_adm014[8] = _mnu_adm014_8;
			this.mnu_adm014[9] = _mnu_adm014_9;
			this.mnu_adm014[10] = _mnu_adm014_10;
			this.mnu_adm014[11] = _mnu_adm014_11;
			this.mnu_adm014[12] = _mnu_adm014_12;
			this.mnu_adm014[13] = _mnu_adm014_13;
			this.mnu_adm014[14] = _mnu_adm014_14;
			this.mnu_adm014[15] = _mnu_adm014_15;
			this.mnu_adm014[16] = _mnu_adm014_16;
			this.mnu_adm014[17] = _mnu_adm014_17;
		}
		void Initializemnu_adm013()
		{
			this.mnu_adm013 = new Telerik.WinControls.RadItem[2];
			this.mnu_adm013[0] = _mnu_adm013_0;
			this.mnu_adm013[1] = _mnu_adm013_1;
		}
		void Initializemnu_adm012()
		{
			this.mnu_adm012 = new Telerik.WinControls.RadItem[10];
			this.mnu_adm012[0] = _mnu_adm012_0;
			this.mnu_adm012[1] = _mnu_adm012_1;
			this.mnu_adm012[2] = _mnu_adm012_2;
			this.mnu_adm012[3] = _mnu_adm012_3;
			this.mnu_adm012[4] = _mnu_adm012_4;
			this.mnu_adm012[5] = _mnu_adm012_5;
			this.mnu_adm012[6] = _mnu_adm012_6;
			this.mnu_adm012[7] = _mnu_adm012_7;
			this.mnu_adm012[8] = _mnu_adm012_8;
			this.mnu_adm012[9] = _mnu_adm012_9;
		}
		void Initializemnu_adm0112()
		{
			this.mnu_adm0112 = new Telerik.WinControls.RadItem[3];
			this.mnu_adm0112[0] = _mnu_adm0112_0;
			this.mnu_adm0112[1] = _mnu_adm0112_1;
			this.mnu_adm0112[2] = _mnu_adm0112_2;
		}
		void Initializemnu_adm011()
		{
			this.mnu_adm011 = new Telerik.WinControls.RadItem[22];
			this.mnu_adm011[0] = _mnu_adm011_0;
			this.mnu_adm011[1] = _mnu_adm011_1;
			this.mnu_adm011[2] = _mnu_adm011_2;
			this.mnu_adm011[3] = _mnu_adm011_3;
			this.mnu_adm011[4] = _mnu_adm011_4;
			this.mnu_adm011[5] = _mnu_adm011_5;
			this.mnu_adm011[6] = _mnu_adm011_6;
			this.mnu_adm011[7] = _mnu_adm011_7;
			this.mnu_adm011[8] = _mnu_adm011_8;
			this.mnu_adm011[9] = _mnu_adm011_9;
			this.mnu_adm011[10] = _mnu_adm011_10;
			this.mnu_adm011[11] = _mnu_adm011_11;
			this.mnu_adm011[12] = _mnu_adm011_12;
			this.mnu_adm011[13] = _mnu_adm011_13;
			this.mnu_adm011[14] = _mnu_adm011_14;
			this.mnu_adm011[15] = _mnu_adm011_15;
			this.mnu_adm011[16] = _mnu_adm011_16;
			this.mnu_adm011[17] = _mnu_adm011_17;
			this.mnu_adm011[18] = _mnu_adm011_18;
			this.mnu_adm011[19] = _mnu_adm011_19;
			this.mnu_adm011[20] = _mnu_adm011_20;
			this.mnu_adm011[21] = _mnu_adm011_21;
		}
		void Initializemnu_adm0108()
		{
			this.mnu_adm0108 = new Telerik.WinControls.RadItem[3];
			this.mnu_adm0108[1] = _mnu_adm0108_1;
			this.mnu_adm0108[2] = _mnu_adm0108_2;
		}
		void Initializemnu_adm0102()
		{
			this.mnu_adm0102 = new Telerik.WinControls.RadItem[8];
			this.mnu_adm0102[0] = _mnu_adm0102_0;
			this.mnu_adm0102[1] = _mnu_adm0102_1;
			this.mnu_adm0102[2] = _mnu_adm0102_2;
			this.mnu_adm0102[3] = _mnu_adm0102_3;
			this.mnu_adm0102[4] = _mnu_adm0102_4;
			this.mnu_adm0102[5] = _mnu_adm0102_5;
			this.mnu_adm0102[6] = _mnu_adm0102_6;
			this.mnu_adm0102[7] = _mnu_adm0102_7;
		}
		void Initializemnu_adm01()
		{
			this.mnu_adm01 = new Telerik.WinControls.RadItem[12];
			this.mnu_adm01[0] = _mnu_adm01_0;
			this.mnu_adm01[1] = _mnu_adm01_1;
			this.mnu_adm01[2] = _mnu_adm01_2;
			this.mnu_adm01[3] = _mnu_adm01_3;
			this.mnu_adm01[4] = _mnu_adm01_4;
			this.mnu_adm01[5] = _mnu_adm01_5;
			this.mnu_adm01[6] = _mnu_adm01_6;
			this.mnu_adm01[7] = _mnu_adm01_7;
			this.mnu_adm01[8] = _mnu_adm01_8;
			this.mnu_adm01[9] = _mnu_adm01_9;
			this.mnu_adm01[10] = _mnu_adm01_10;
			this.mnu_adm01[11] = _mnu_adm01_11;
		}
		void Initializemnu_adm001()
		{
			this.mnu_adm001 = new Telerik.WinControls.RadItem[6];
			this.mnu_adm001[0] = _mnu_adm001_0;
			this.mnu_adm001[1] = _mnu_adm001_1;
			this.mnu_adm001[2] = _mnu_adm001_2;
			this.mnu_adm001[3] = _mnu_adm001_3;
			this.mnu_adm001[4] = _mnu_adm001_4;
			this.mnu_adm001[5] = _mnu_adm001_5;
		}
		void Initializemnu_adm00()
		{
			this.mnu_adm00 = new Telerik.WinControls.RadItem[1];
			this.mnu_adm00[0] = _mnu_adm00_0;
		}
		void Initializemnu_FSI()
		{
			this.mnu_FSI = new Telerik.WinControls.RadItem[4];
			this.mnu_FSI[1] = _mnu_FSI_1;
			this.mnu_FSI[2] = _mnu_FSI_2;
			this.mnu_FSI[3] = _mnu_FSI_3;
		}
        #endregion

        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
    }
}