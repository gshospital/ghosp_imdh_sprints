using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;
using Telerik.WinControls.UI;

namespace ADMISION
{
    public partial class List_traslados
        : Telerik.WinControls.UI.RadForm
    {

        public List_traslados()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
            ReLoadForm(false);
        }


        private void List_traslados_Activated(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
            {
                UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form)eventSender;
            }
        }
        string vSqlc = String.Empty;
        string sql2 = String.Empty;
        string sql3 = String.Empty;
        string sql4 = String.Empty;
        //Dim PathString As String
        string stIntervalo = String.Empty;
        string[] stUnidad_Origen = new string[] { String.Empty, String.Empty, String.Empty, String.Empty, String.Empty };

        string[] stUnidad_Destino = new string[] { String.Empty, String.Empty, String.Empty, String.Empty, String.Empty };
        string[] mCarga_Lista1 = null;
        string[] mCarga_Lista2 = null;
        string[] mCarga_Lista3 = null;
        string[] mCarga_Lista4 = null;
        string stFechaini = String.Empty;
        string stFechafin = String.Empty;
        string[] mTemp2 = null;
        string[] mTemp4 = null;
        string stMensaje1 = String.Empty;
        string stMensaje2 = String.Empty;
        string stMensaje3 = String.Empty;

        int iNumero = 0;
        int iLista1 = 0;
        int iLista3 = 0;
        int iLista2 = 0;
        int iLista4 = 0;
        int viform = 0;

        DataSet Rr1 = null;
        DataSet Rr2 = null;
        DataSet Rr3 = null;
        DataSet Rr4 = null;
        Crystal CrystalReport1 = null;

        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {
            string DireccHo = String.Empty;
            string GrupoHo = String.Empty;
            string LitPersona = String.Empty;
            string NombreHo = String.Empty;

            try
            {
                if (LsbLista2.Items.Count == 0 && LsbLista4.Items.Count == 0)
                {
                    //MsgBox ("No hay seleccionada ninguna Unidad Hospitalaria")
                    stMensaje1 = "una Unidad Hospitalaria de Origen o Destino";
                    short tempRefParam = 1270;
                    string[] tempRefParam2 = new string[] { stMensaje1 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    return;
                }
                else if (LsbLista2.Items.Count == 0)
                {
                    //MsgBox ("No hay seleccionada ninguna Unidad Hospitalaria de Origen")
                    stMensaje1 = "una Unidad Hospitalaria de Origen";
                    short tempRefParam3 = 1270;
                    string[] tempRefParam4 = new string[] { stMensaje1 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                    return;
                }
                else if (LsbLista4.Items.Count == 0)
                {
                    //MsgBox ("No hay seleccionada ninguna Unidad Hospitalaria de Destino")
                    stMensaje1 = "una Unidad Hospitalaria de Destino";
                    short tempRefParam5 = 1270;
                    string[] tempRefParam6 = new string[] { stMensaje1 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                    return;
                }
                //Jes�s 31/03/2006 validamos que no pueda haber seleccionadas mas de
                // unidades y menos de todas
                if (((LsbLista2.Items.Count > iNumero) && (LsbLista1.Items.Count > 0)) || ((LsbLista4.Items.Count > iNumero) && (LsbLista3.Items.Count > 0)))
                {
                    stMensaje1 = "" + iNumero.ToString();
                    stMensaje2 = "Unidades Hospitalarias y menos de Todas";
                    short tempRefParam7 = 1280;
                    string[] tempRefParam8 = new string[] { stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
                    return;
                }
                //validamos que la fecha de fin sea mayor que la de inicio y que no sea
                // mayor que la del sistema
                if (sdcFechini.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    //MsgBox "La fecha inicial no puede ser mayor que la del sistema"
                    stMensaje1 = "Fecha de " + Label1.Text;
                    stMensaje2 = "menor o igual";
                    stMensaje3 = "Fecha del Sistema";
                    short tempRefParam9 = 1020;
                    string[] tempRefParam10 = new string[] { stMensaje1, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                    sdcFechini.Focus();
                    return;
                }
                if (sdcFechfin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    //MsgBox ("La Fecha de fin no puede ser mayor que la del sistema")
                    stMensaje1 = "Fecha de " + Label2.Text;
                    stMensaje2 = "menor o igual";
                    stMensaje3 = "Fecha del Sistema";
                    short tempRefParam11 = 1020;
                    string[] tempRefParam12 = new string[] { stMensaje1, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12));
                    sdcFechfin.Focus();
                    return;
                }
                if (sdcFechfin.Value.Date < sdcFechini.Value.Date)
                {
                    //MsgBox "Fecha de fin debe ser mayor que fecha inicio"
                    stMensaje1 = "Fecha de " + Label2.Text;
                    stMensaje2 = "mayor o igual";
                    stMensaje3 = "Fecha de " + Label1.Text;
                    short tempRefParam13 = 1020;
                    string[] tempRefParam14 = new string[] { stMensaje1, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, Conexion_Base_datos.RcAdmision, tempRefParam14));
                    sdcFechfin.Focus();
                    return;
                }

                iLista2 = LsbLista2.Items.Count;
                iLista4 = LsbLista4.Items.Count;

                CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\Agi330f3_CR11.rpt";

                proFormulas(viform); //limpio las formulas
                                     //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //*******************
                this.Cursor = Cursors.WaitCursor;
                proListado_Query();
                CrystalReport1.SQLQuery = vSqlc;
                //    PathString = App.Path
                

                sql2 = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
                sql3 = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
                sql4 = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql2, Conexion_Base_datos.RcAdmision);
                Rr2 = new DataSet();
                tempAdapter.Fill(Rr2);
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql3, Conexion_Base_datos.RcAdmision);
                Rr3 = new DataSet();
                tempAdapter_2.Fill(Rr3);
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql4, Conexion_Base_datos.RcAdmision);
                Rr4 = new DataSet();
                tempAdapter_3.Fill(Rr4);
                GrupoHo = Convert.ToString(Rr2.Tables[0].Rows[0]["VALFANU1"]).Trim();
                NombreHo = Convert.ToString(Rr3.Tables[0].Rows[0]["VALFANU1"]).Trim();
                DireccHo = Convert.ToString(Rr4.Tables[0].Rows[0]["VALFANU1"]).Trim();
                stIntervalo = "Desde: " + sdcFechini.Value.Date + "  Hasta: " + sdcFechfin.Value.Date;
                CrystalReport1.Formulas["FORM1"] = "\"" + GrupoHo + "\"";
                CrystalReport1.Formulas["FORM2"] = "\"" + NombreHo + "\"";
                CrystalReport1.Formulas["FORM3"] = "\"" + DireccHo + "\"";
                CrystalReport1.Formulas["INTERVALO"] = "\"" + stIntervalo + "\"";
                CrystalReport1.Formulas["UNIDADOR"] = "\"" + stUnidad_Origen[0] + "\"";
                CrystalReport1.Formulas["UNIDADOR2"] = "\"" + stUnidad_Origen[1] + "\"";
                CrystalReport1.Formulas["UNIDADOR3"] = "\"" + stUnidad_Origen[2] + "\"";
                CrystalReport1.Formulas["UNIDADOR4"] = "\"" + stUnidad_Origen[3] + "\"";
                CrystalReport1.Formulas["UNIDADOR5"] = "\"" + stUnidad_Origen[4] + "\"";
                CrystalReport1.Formulas["UNIDADDE"] = "\"" + stUnidad_Destino[0] + "\"";
                CrystalReport1.Formulas["UNIDADDE2"] = "\"" + stUnidad_Destino[1] + "\"";
                CrystalReport1.Formulas["UNIDADDE3"] = "\"" + stUnidad_Destino[2] + "\"";
                CrystalReport1.Formulas["UNIDADDE4"] = "\"" + stUnidad_Destino[3] + "\"";
                CrystalReport1.Formulas["UNIDADDE5"] = "\"" + stUnidad_Destino[4] + "\"";

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.ToUpper();
                CrystalReport1.Formulas["LitPers"] = "\"" + LitPersona + "\"";


                CrystalReport1.SortFields[0] = "+{AMOVIMIE.ffinmovi}";
                CrystalReport1.WindowTitle = "Listado de los Traslados Realizados";
                CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                CrystalReport1.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                CrystalReport1.Destination = Parasalida;
                CrystalReport1.WindowShowPrintSetupBtn = true;
                if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                }
                CrystalReport1.Action = 1;
                CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                viform = 16;
                Conexion_Base_datos.vacia_formulas(CrystalReport1, viform);
                cbImprimir.Enabled = false;
                cbPantalla.Enabled = false;
                LsbLista2.Items.Clear();
                LsbLista4.Items.Clear();
                LsbLista1.Items.Clear();
                LsbLista3.Items.Clear();
                proCarga_Lista1();
                proCarga_Lista3();
                CbInsTodo[0].Enabled = true;
                CbInsTodo[1].Enabled = true;
                CbInsUno[0].Enabled = true;
                CbInsUno[1].Enabled = true;
                CbDelTodo[0].Enabled = false;
                CbDelTodo[1].Enabled = false;
                CbDelUno[0].Enabled = false;
                CbDelUno[1].Enabled = false;
                this.Cursor = Cursors.Default;
                //sdcFechini.Text = CStr(Date)
                //sdcFechini.Text = sdcFechfin.Text
            }
            catch (Exception e)
            {
                if (false)//e.Number == 32755)
                {
                    //CANCELADA IMPRESION
                }
                else
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "List_traslados:proImprimir", e);
                }
                //iDevolver = GestionOtrosErrores(, RcAdmision)
            }
        }

        private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
            List_traslados.DefInstance.Close();
		}

		private void CbDelTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			if (CbDelTodo[0].Focused)
			{
				LsbLista1.Items.Clear();
				LsbLista2.Items.Clear();
				proCarga_Lista1();
				CbDelTodo[0].Enabled = false;
				CbDelUno[0].Enabled = false;
				CbInsTodo[0].Enabled = true;
				CbInsUno[0].Enabled = false;
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
			}
			else if (CbDelTodo[1].Focused)
			{ 
				LsbLista3.Items.Clear();
				LsbLista4.Items.Clear();
				proCarga_Lista3();
				CbDelTodo[1].Enabled = false;
				CbDelUno[1].Enabled = false;
				CbInsTodo[1].Enabled = true;
				CbInsUno[1].Enabled = false;
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
			}
		}

		private void CbDelUno_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.CbDelUno, eventSender);

			if (Index == 0)
			{
			        if (LsbLista2.SelectedItems.Count == 0)
                    {
					//MsgBox ("Debe haber seleccionado alguna Unidad Hospitalaria")
					stMensaje1 = "una Unidad Hospitalaria";
					short tempRefParam = 1270;
                    string[] tempRefParam2 = new string[] { stMensaje1 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					return;
				}
			}
			else
			{
			        if (LsbLista4.SelectedItems.Count == 0)
                    {
					//MsgBox ("Debe haber seleccionado alguna Unidad Hospitalaria")
					stMensaje1 = "una Unidad Hospitalaria";
					short tempRefParam3 = 1270;
                    string[] tempRefParam4 = new string[] { stMensaje1 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
					return;
				}
			}

			if (Index == 0)
			{
                LsbLista1.Items.Add(LsbLista2.Items[LsbLista2.SelectedIndex].Text);
                LsbLista2.Items.Remove(LsbLista2.SelectedItem);
                //LsbLista2.Refresh();
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
				CbInsUno[0].Enabled = false;
				CbInsTodo[0].Enabled = true;
			}

			//If CbDelUno.Item(1) Then
			if (Index == 1)
			{
			    LsbLista3.Items.Add(LsbLista4.Items[LsbLista4.SelectedIndex].Text);
                LsbLista4.Items.Remove(LsbLista4.SelectedItem);
                LsbLista4.Refresh();
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
				CbInsUno[1].Enabled = true;
				CbInsTodo[1].Enabled = true;
			}
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }

        private void CbInsTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			if (CbInsTodo[0].Focused)
			{
				LsbLista1.Items.Clear();
				proCarga_Lista2();
				CbInsTodo[0].Enabled = false;
				CbDelTodo[0].Enabled = true;
				CbInsUno[0].Enabled = false;
				CbDelUno[0].Enabled = false;
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
			}
			else if (CbInsTodo[1].Focused)
			{ 
				LsbLista3.Items.Clear();
				proCarga_Lista4();
				CbInsTodo[1].Enabled = false;
				CbDelTodo[1].Enabled = true;
				CbInsUno[1].Enabled = false;
				CbDelUno[1].Enabled = false;
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
			}
		}

		private void CbInsUno_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.CbInsUno, eventSender);

			//If CbInsUno.Item(0) Then
			if (Index == 0)
			{
			        if (LsbLista1.SelectedItems.Count == 0)
                    {
					//MsgBox ("Debe haber seleccionado alguna Unidad Hospitalaria")
					stMensaje1 = "una Unidad Hospitalaria";
					short tempRefParam = 1270;
                    string[] tempRefParam2 = new string[] { stMensaje1 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					return;
				}
				else if (LsbLista2.Items.Count == iNumero)
				{ 
					//MsgBox ("No se pueden seleccionar mas de " & iNumero & " Unidades Hospitalarias")
					stMensaje1 = "" + iNumero.ToString();
					stMensaje2 = "Unidades Hospitalarias";
					short tempRefParam3 = 1280;
                    string[] tempRefParam4 = new string[] { stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
					return;
				}
			}
			else
			{
				    if (LsbLista3.SelectedItems.Count == 0)
                    {
					//MsgBox ("Debe haber seleccionado alguna Unidad Hospitalaria")
					stMensaje1 = "una Unidad Hospitalaria";
					short tempRefParam5 = 1270;
                    string[] tempRefParam6 = new string[] { stMensaje1 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
					return;
				}
				else if (LsbLista4.Items.Count == iNumero)
				{ 
					//MsgBox ("No se pueden seleccionar mas de " & iNumero & " Unidades Hospitalarias")
					stMensaje1 = "" + iNumero.ToString();
					stMensaje2 = "Unidades Hospitalarias";
					short tempRefParam7 = 1280;
                    string[] tempRefParam8 = new string[] { stMensaje1, stMensaje2 };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
					return;
				}
			}

			//If CbInsUno.Item(0) Then
			if (Index == 0)
			{
                LsbLista2.Items.Add(LsbLista1.Items[LsbLista1.SelectedIndex].Text);
				LsbLista1.Items.Remove(LsbLista1.SelectedItem);
				LsbLista1.Refresh();
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
				CbDelUno[0].Enabled = true;
				CbDelTodo[0].Enabled = true;
			}

			//If CbInsUno.Item(1) Then
			if (Index == 1)
			{
				LsbLista4.Items.Add(LsbLista3.Items[LsbLista3.SelectedIndex].Text);
                LsbLista3.Items.Remove(LsbLista3.SelectedItem);
                LsbLista3.Refresh();
				cbPantalla.Enabled = true;
				cbImprimir.Enabled = true;
				CbDelUno[1].Enabled = true;
				CbDelTodo[1].Enabled = true;
			}
		}

		private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }
        
        private void List_traslados_Load(Object eventSender, EventArgs eventArgs)
		{
            CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;
            CrystalReport1.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            iNumero = 4;
			LsbLista2.Items.Clear();
			cbImprimir.Enabled = false;
			cbPantalla.Enabled = false;
			proCarga_Lista1();
			proCarga_Lista3();
			CbInsTodo[0].Enabled = true;
			CbInsUno[0].Enabled = false;
			CbDelTodo[0].Enabled = false;
			CbDelUno[0].Enabled = false;
			CbInsTodo[1].Enabled = true;
			CbInsUno[1].Enabled = false;
			CbDelTodo[1].Enabled = false;
			CbDelUno[1].Enabled = false;
		}

		private void LsbLista1_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsUno_Click(CbInsUno[0], new EventArgs());
		}

		private void LsbLista2_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelUno_Click(CbDelUno[0], new EventArgs());
		}

		private void LsbLista3_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsUno_Click(CbInsUno[1], new EventArgs());

		}

		private void LsbLista4_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelUno_Click(CbDelUno[1], new EventArgs());

		}

		public void proListado_Query()
		{

			// Formateo de las variables de fecha al formato SQL, y variables a cero para
			// las f�rmulas
			int n = 0;
			bool fEntra = false;
			try
			{
				vSqlc = " ";

                if (stUnidad_Origen != null)
				{
					Array.Clear(stUnidad_Origen, 0, stUnidad_Origen.Length);
				}

                if (stUnidad_Destino != null)
				{
					Array.Clear(stUnidad_Destino, 0, stUnidad_Destino.Length);
				}
				stIntervalo = "";
				stFechaini = "";
				stFechafin = "";
				// Damos formato a las fechas para hacer la consulta *************************
				//stFechaini = sdcFechini.Value.Date + " " + "00:00:00";
				//stFechafin = sdcFechfin.Value.Date + " " + "23:59:59";

                stFechaini = sdcFechini.Value.ToShortDateString() + " " + "00:00:00";
                stFechafin = sdcFechfin.Value.ToShortDateString() + " " + "23:59:59";
                // ***************************************************************************

                //**************************************************************************
                // SQL inicial a la que le concatenar� los trozos segun las unidades hopitalarias
                // que se hayan seleccionado en las listas

                object tempRefParam = stFechaini;
				object tempRefParam2 = stFechafin;
				vSqlc = " SELECT AMOVIMIE.fmovimie, AMOVIMIE.gplantor, AMOVIMIE.ghabitor, " + " AMOVIMIE.gcamaori, AMOVIMIE.gplantde, AMOVIMIE.ghabitde, " + " AMOVIMIE.gcamades, AMOVIMIE.ffinmovi, " + " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + " DMOTTRAS.dmotitra, " + " A.dunidenf, B.dunidenf " + " FROM AMOVIMIE, AEPISADM, DPACIENT, DMOTTRAS, DUNIENFE A, DUNIENFE B " + " WHERE " + " AMOVIMIE.itipotra IS NOT NULL AND " + " AMOVIMIE.ffinmovi IS NOT NULL AND " + " AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " + " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " AMOVIMIE.gmottras = DMOTTRAS.gmottras AND " + " AMOVIMIE.ffinmovi >=" + Serrores.FormatFechaHMS(tempRefParam) + " AND " + " AMOVIMIE.ffinmovi <=" + Serrores.FormatFechaHMS(tempRefParam2) + " AND " + " AMOVIMIE.gunenfor = A.gunidenf AND " + " AMOVIMIE.gunenfde = B.gunidenf ";
				stFechafin = Convert.ToString(tempRefParam2);
				stFechaini = Convert.ToString(tempRefParam);
				//***************************************************************************



				if (LsbLista2.Items.Count == iLista1 && LsbLista4.Items.Count == iLista3)
				{
					// Ha seleccionado todas lista 1 y todas lista 3
					//''vSqlc = vSqlc + " ORDER BY AMOVIMIE.ffinmovi ASC"
					stUnidad_Origen[0] = " TODAS";
					stUnidad_Destino[0] = " TODAS";
				}
				else if (LsbLista2.Items.Count == iLista1 && LsbLista4.Items.Count < iLista3 && LsbLista4.Items.Count != 0)
				{ 
					// Ha seleccionado todas lista 1 y algunas lista 3
					// En mTemp4() tengo los valores de la lista 4
					n = 0;
					fEntra = false;

					while(LsbLista4.Items.Count != 0)
					{
						n = LsbLista4.Items.Count - 1;
						//mTemp4[n] = LsbLista4.GetListItem(n);
                        mTemp4[n] = LsbLista4.Items[n].Text.ToString();
                        LsbLista4.Items.RemoveAt(n);
						LsbLista4.Refresh();
						n--;
					};
					vSqlc = vSqlc + " AND ";
					for (int x = 1; x <= iLista4; x++)
					{
						if (!fEntra)
						{
							vSqlc = vSqlc + " (B.dunidenf ='" + mTemp4[x - 1] + "' ";
						}
						fEntra = true;
						if (mTemp4[x] == "")
						{
							vSqlc = vSqlc + ")";
							break;
						}
						vSqlc = vSqlc + " OR B.dunidenf ='" + mTemp4[x] + "' ";
					}
					stUnidad_Origen[0] = " TODAS";
					//stUnidad_Destino(0) = "UNIDAD DE DESTINO: " & mTemp4(0) & " " & mTemp4(1) & " " _
					//& mTemp4(2) & " " & mTemp4(3) & " " & mTemp4(4)
					stUnidad_Destino[0] = mTemp4[0];
					stUnidad_Destino[1] = mTemp4[1];
					stUnidad_Destino[2] = mTemp4[2];
					stUnidad_Destino[3] = mTemp4[3];
					stUnidad_Destino[4] = mTemp4[4];

				}
				else if (LsbLista2.Items.Count < iLista1 && LsbLista2.Items.Count != 0 && LsbLista4.Items.Count < iLista3 && LsbLista4.Items.Count != 0)
				{ 
					// Ha seleccionado algunas lista 1 y algunas lista 3
					n = 0;
					fEntra = false;

					while(LsbLista2.Items.Count != 0)
					{
						n = LsbLista2.Items.Count - 1;
                        //mTemp2[n] = LsbLista2.GetListItem(n);
                        //LsbLista2.RemoveItem(n);
                        mTemp2[n] = LsbLista2.Items[n].Text.ToString();
						LsbLista2.Items.RemoveAt(n);
						LsbLista2.Refresh();
						n--;
					};
					n = 0;

					while(LsbLista4.Items.Count != 0)
					{
						n = LsbLista4.Items.Count - 1;
						mTemp4[n] = LsbLista4.Items[n].Text.ToString();
						LsbLista4.Items.RemoveAt(n);
						LsbLista4.Refresh();
						n--;
					};
					vSqlc = vSqlc + " AND ";
					for (int x = 1; x <= iLista2; x++)
					{
						if (!fEntra)
						{
							vSqlc = vSqlc + " (A.dunidenf ='" + mTemp2[x - 1] + "' ";
						}
						fEntra = true;
						if (mTemp2[x] == "")
						{
							vSqlc = vSqlc + ")";
							break;
						}
						vSqlc = vSqlc + " OR A.dunidenf ='" + mTemp2[x] + "' ";
					}
					vSqlc = vSqlc + " AND ";
					fEntra = false;
					for (int x = 1; x <= iLista4; x++)
					{
						if (!fEntra)
						{
							vSqlc = vSqlc + " (B.dunidenf ='" + mTemp4[x - 1] + "' ";
						}
						fEntra = true;
						if (mTemp4[x] == "")
						{
							vSqlc = vSqlc + ")";
							break;
						}
						vSqlc = vSqlc + " OR B.dunidenf ='" + mTemp4[x] + "' ";
					}
					//stUnidad_Origen = "UNIDAD DE ORIGEN: " & mTemp2(0) & " " & mTemp2(1) & " " _
					//& mTemp2(2) & " " & mTemp2(3) & " " & mTemp2(4)
					stUnidad_Origen[0] = mTemp2[0];
					stUnidad_Origen[1] = mTemp2[1];
					stUnidad_Origen[2] = mTemp2[2];
					stUnidad_Origen[3] = mTemp2[3];
					stUnidad_Origen[4] = mTemp2[4];
					//stUnidad_Destino = "UNIDAD DE DESTINO: " & mTemp4(0) & " " & mTemp4(1) & " " _
					//& mTemp4(2) & " " & mTemp4(3) & " " & mTemp4(4)
					stUnidad_Destino[0] = mTemp4[0];
					stUnidad_Destino[1] = mTemp4[1];
					stUnidad_Destino[2] = mTemp4[2];
					stUnidad_Destino[3] = mTemp4[3];
					stUnidad_Destino[4] = mTemp4[4];

				}
				else if (LsbLista2.Items.Count < iLista1 && LsbLista2.Items.Count != 0 && LsbLista4.Items.Count == iLista3)
				{ 
					// Ha seleccionado algunas lista 1 y todas lista 3
					// En mTemp2() tengo los valores de la lista 2
					n = 0;
					fEntra = false;

					while(LsbLista2.Items.Count != 0)
					{
						n = LsbLista2.Items.Count - 1;
						mTemp2[n] = LsbLista2.Items[n].Text.ToString();
						LsbLista2.Items.RemoveAt(n);
						LsbLista2.Refresh();
						n--;
					};
					vSqlc = vSqlc + " AND ";
					for (int x = 1; x <= iLista2; x++)
					{
						if (!fEntra)
						{
							vSqlc = vSqlc + " (A.dunidenf ='" + mTemp2[x - 1] + "' ";
						}
						fEntra = true;
						if (mTemp2[x] == "")
						{
							vSqlc = vSqlc + ")";
							break;
						}
						vSqlc = vSqlc + " OR A.dunidenf ='" + mTemp2[x] + "' ";
					}
					stUnidad_Destino[0] = " TODAS";
					//stUnidad_Origen = "UNIDAD DE ORIGEN: " & mTemp2(0) & " " & mTemp2(1) & " " _
					//& mTemp2(2) & " " & mTemp2(3) & " " & mTemp2(4)
					stUnidad_Origen[0] = mTemp2[0];
					stUnidad_Origen[1] = mTemp2[1];
					stUnidad_Origen[2] = mTemp2[2];
					stUnidad_Origen[3] = mTemp2[3];
					stUnidad_Origen[4] = mTemp2[4];
				}
				vSqlc = vSqlc + "\r" + "\n";
				vSqlc = vSqlc + " ORDER BY AMOVIMIE.ffinmovi ASC";
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "List_traslados:proListado_Query", ex);
			}
		}

		public void proCarga_Lista1()
		{
			int i = 0;
			string sqlLista = String.Empty;
			try
			{
				sqlLista = "SELECT * FROM DUNIENFE WHERE itiposer= 'H'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["dunidenf"]) != "")
					{
                        mCarga_Lista1 = ArraysHelper.RedimPreserve(mCarga_Lista1, new int[]{i + 1});
						mCarga_Lista1[i] = Convert.ToString(iteration_row["dunidenf"]).Trim();
                        //LsbLista1.SetItemData(LsbLista1.GetNewIndex(), i);
                        LsbLista1.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dunidenf"]).Trim(), i));
                        i++;
					}
				}
				mTemp2 = ArraysHelper.RedimPreserve(mTemp2, new int[]{i + 1});
				mTemp4 = ArraysHelper.RedimPreserve(mTemp4, new int[]{i + 1});
				iLista1 = LsbLista1.Items.Count;

                Rr1.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "List_traslados:proCarga_lista1", ex);
			}
		}

		public void proCarga_Lista3()
		{
			int n = 0;
			string sqlLista = String.Empty;
			try
			{
				sqlLista = "SELECT * FROM DUNIENFE WHERE itiposer= 'H'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["dunidenf"]) != "")
					{
						
                        mCarga_Lista3 = ArraysHelper.RedimPreserve(mCarga_Lista3, new int[]{n + 1});
						mCarga_Lista3[n] = Convert.ToString(iteration_row["dunidenf"]).Trim();
                        //LsbLista3.SetItemData(LsbLista3.GetNewIndex(), n);
                        LsbLista3.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dunidenf"]).Trim(), n));
                        n++;
					}
				}
				iLista3 = LsbLista3.Items.Count;

                Rr1.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "List_traslados:proCarga_lista3", ex);
			}
		}

		public void proCarga_Lista2()
		{
			int n = 0;
			string sqlLista = String.Empty;
			try
			{
				LsbLista2.Items.Clear();
				sqlLista = "SELECT * FROM DUNIENFE WHERE itiposer= 'H'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["dunidenf"]) != "")
					{
                        mCarga_Lista2 = ArraysHelper.RedimPreserve(mCarga_Lista2, new int[]{n + 1});
						mCarga_Lista2[n] = Convert.ToString(iteration_row["dunidenf"]).Trim();
                        //LsbLista2.SetItemData(LsbLista2.GetNewIndex(), n);
                        LsbLista2.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dunidenf"]).Trim(), n));
                        n++;
					}
				}

                Rr1.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "List_traslados:proCarga_lista2", ex);
			}
		}

		public void proCarga_Lista4()
		{
			int n = 0;
			string sqlLista = String.Empty;
			try
			{
				LsbLista4.Items.Clear();
				sqlLista = "SELECT * FROM DUNIENFE WHERE itiposer= 'H'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["dunidenf"]) != "")
					{
                        mCarga_Lista4 = ArraysHelper.RedimPreserve(mCarga_Lista4, new int[]{n + 1});
						mCarga_Lista4[n] = Convert.ToString(iteration_row["dunidenf"]).Trim();
                        //LsbLista4.SetItemData(LsbLista4.GetNewIndex(), n);
                        LsbLista4.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dunidenf"]).Trim(), n));
                        n++;
					}
				}

                Rr1.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "List_traslados:proCarga_lista4", ex);
			}
		}
		public void proFormulas(int idForm)
		{
            int tiForm = 0;
            foreach (var key in CrystalReport1.Formulas.Keys)
            {
                if (tiForm <= idForm)
                    CrystalReport1.Formulas[key] = "";
            }
        }
		private void List_traslados_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}

        private void LsbLista1_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            CbInsUno[0].Enabled = true;
        }

        private void LsbLista2_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            CbDelUno[0].Enabled = true;
        }

        private void LsbLista3_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            CbInsUno[1].Enabled = true;
        }

        private void LsbLista4_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            CbDelUno[1].Enabled = true;
        }
    }
}