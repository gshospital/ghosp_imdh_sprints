using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Seleccion_Impresoras
	{

		#region "Upgrade Support "
		private static Seleccion_Impresoras m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Seleccion_Impresoras DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Seleccion_Impresoras();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCancelar", "cbImprimir", "tbCopias", "cbbImpresoras", "lbCopias", "lbImpresora"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadTextBoxControl tbCopias;
		public Telerik.WinControls.UI.RadDropDownList cbbImpresoras;
		public Telerik.WinControls.UI.RadLabel lbCopias;
		public Telerik.WinControls.UI.RadLabel lbImpresora;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.tbCopias = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbImpresoras = new Telerik.WinControls.UI.RadDropDownList();
            this.lbCopias = new Telerik.WinControls.UI.RadLabel();
            this.lbImpresora = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCopias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImpresoras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(197, 132);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(86, 29);
            this.cbCancelar.TabIndex = 3;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Location = new System.Drawing.Point(98, 132);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(86, 29);
            this.cbImprimir.TabIndex = 2;
            this.cbImprimir.Text = "&Aceptar";
            this.cbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // tbCopias
            // 
            this.tbCopias.AcceptsReturn = true;
            this.tbCopias.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCopias.Location = new System.Drawing.Point(187, 68);
            this.tbCopias.MaxLength = 0;
            this.tbCopias.Name = "tbCopias";
            this.tbCopias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCopias.Size = new System.Drawing.Size(39, 20);
            this.tbCopias.TabIndex = 1;
            this.tbCopias.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCopias_KeyDown);
            this.tbCopias.Leave += new System.EventHandler(this.tbCopias_Leave);
            // 
            // cbbImpresoras
            // 
            this.cbbImpresoras.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbImpresoras.Location = new System.Drawing.Point(17, 31);
            this.cbbImpresoras.Name = "cbbImpresoras";
            this.cbbImpresoras.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbImpresoras.Size = new System.Drawing.Size(264, 20);
            this.cbbImpresoras.TabIndex = 0;
            this.cbbImpresoras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbImpresoras_KeyPress);
            this.cbbImpresoras.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbImpresoras_SelectionChangeCommitted);
            // 
            // lbCopias
            // 
            //this.lbCopias.BackColor = System.Drawing.SystemColors.Control;
            this.lbCopias.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbCopias.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbCopias.Location = new System.Drawing.Point(26, 72);
            this.lbCopias.Name = "lbCopias";
            this.lbCopias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCopias.Size = new System.Drawing.Size(148, 21);
            this.lbCopias.TabIndex = 5;
            this.lbCopias.Text = "Seleccione n�mero de copias:";
            // 
            // lbImpresora
            // 
            //this.lbImpresora.BackColor = System.Drawing.SystemColors.Control;
            this.lbImpresora.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbImpresora.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbImpresora.Location = new System.Drawing.Point(16, 12);
            this.lbImpresora.Name = "lbImpresora";
            this.lbImpresora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbImpresora.Size = new System.Drawing.Size(248, 16);
            this.lbImpresora.TabIndex = 4;
            this.lbImpresora.Text = "Seleccione Impresora para imprimir las etiquetas:";
            // 
            // Seleccion_Impresoras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(283, 165);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.tbCopias);
            this.Controls.Add(this.cbbImpresoras);
            this.Controls.Add(this.lbCopias);
            this.Controls.Add(this.lbImpresora);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Seleccion_Impresoras";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Impresoras-AGI150F2";
            this.Activated += new System.EventHandler(this.Seleccion_Impresoras_Activated);
            this.Closed += new System.EventHandler(this.Seleccion_Impresoras_Closed);
            this.Load += new System.EventHandler(this.Seleccion_Impresoras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCopias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImpresoras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}