using Microsoft.VisualBasic;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace ADMISION
{
    public partial class Anulacion_ingresos_alta
        : Telerik.WinControls.UI.RadForm
    {
        string stMensaje1 = String.Empty;
        string stMensaje2 = String.Empty;
        string stTipo = String.Empty;
        string stFanulaciI = String.Empty;
        string stFanulaciF = String.Empty;
        string stSeleccion = String.Empty;
        string stIntervalo = String.Empty;

        Crystal CrystalReport1 = new Crystal();
        int viform = 0;
        string stSql = String.Empty;
        public Anulacion_ingresos_alta()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            isInitializingComponent = true;
            InitializeComponent();
            isInitializingComponent = false;
            ReLoadForm(false);
        }


        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {
            string LitPersona = String.Empty;

            try
            {
                // Realizamos las validaciones de las fechas introducidas *******************
                if (SdcFechaIni.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje1 = "menor o igual";
                    stMensaje2 = "Fecha del Sistema";

                    short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { Label1.Text, stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    SdcFechaIni.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje1 = "menor o igual";
                    stMensaje2 = "Fecha del Sistema";

                    short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { Label2.Text, stMensaje1, stMensaje2 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                    SdcFechaFin.Focus();
                    return;
                }
                if (SdcFechaFin.Value.Date < SdcFechaIni.Value.Date)
                {
                    stMensaje1 = "menor o igual";

                    short tempRefParam5 = 1020;
                    string[] tempRefParam6 = new string[] { Label2.Text, stMensaje1, Label1.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                    SdcFechaIni.Focus();
                    return;
                }
                // **************************************************************************
                // FORMATEO DE LA FECHA, para mandar en la consulta SQL un rango de fecha, en
                // nuestro caso el dia entero
                stFanulaciI = SdcFechaIni.Text + " 00:00:00";
                stFanulaciF = SdcFechaFin.Text + " 23:59:59";
                // **************************************************************************
                stIntervalo = "Desde: " + SdcFechaIni.Value.Date + "  Hasta: " + SdcFechaFin.Value.Date;
                //    PathString = App.Path   ' Cojo la ruta de acceso para buscar el informe
                switch (stTipo)
                {
                    case "INGRESO":
                        stSeleccion = "Tipo de Movimiento: Ingreso";
                        CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi414r1_CR11.rpt";
                        CrystalReport1.WindowTitle = "Listado Anulaci�n de Ingresos";
                        proConsulta("I");
                        break;
                    case "ALTA":
                        stSeleccion = "Tipo de Movimiento: Alta";
                        CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi414r1_CR11.rpt";
                        CrystalReport1.WindowTitle = "Listado Anulaci�n de Altas";
                        proConsulta("A");
                        break;
                }
                this.Cursor = Cursors.WaitCursor;

                //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //*******************
                CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
                CrystalReport1.Formulas["{@SELECCION}"] = "\"" + stSeleccion + "\"";
                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.ToUpper();

                CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                CrystalReport1.Destination = Parasalida;
                CrystalReport1.WindowShowPrintSetupBtn = true;
                if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                }
                CrystalReport1.Action = 1;
                CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                viform = 5;
                Conexion_Base_datos.vacia_formulas(CrystalReport1, viform);
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
                RbIngreso.IsChecked = false;
                RbAlta.IsChecked = false;
                SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
                SdcFechaIni.Text = SdcFechaFin.Text;
                this.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                this.Cursor = Cursors.Default;
                //UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
                /*if (e.Number == 32755)
                {
                    //se ha pulsado cancelar en el cuadro de dialogo imprimir
                }
                else
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Anulaci�n_ingresos_alta:proImprimir", e);
                }*/
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
                RbIngreso.IsChecked = false;
                RbAlta.IsChecked = false;
                SdcFechaIni.Text = DateTime.Today.ToString("dd/MM/yyyy");
                SdcFechaIni.Text = SdcFechaFin.Text;
            }
        }

        private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
            menu_admision.DefInstance.Show();
        }
        private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }
        private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }
        private void Anulacion_ingresos_alta_Load(Object eventSender, EventArgs eventArgs)
        {
            CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;
            this.Height = 226;
            this.Width = 309;
            CbImprimir.Enabled = false;
            CbPantalla.Enabled = false;
            RbIngreso.IsChecked = false;
            RbAlta.IsChecked = false;
        }

        private bool isInitializingComponent;
        private void RbAlta_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                CbImprimir.Enabled = true;
                CbPantalla.Enabled = true;
                stTipo = "ALTA";
            }
        }
        private void RbIngreso_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                CbImprimir.Enabled = true;
                CbPantalla.Enabled = true;
                stTipo = "INGRESO";
            }
        }
        public void proConsulta(string tipo)
        {
            try
            {
                switch (tipo)
                {
                    case "I":
                        object tempRefParam = stFanulaciI;
                        object tempRefParam2 = stFanulaciF;
                        stSql = " SELECT * " + " FROM DEPIANUL, DPACIENT, DSOCIEDA, DPERSONA, DSERVICI, SUSUARIO " + " WHERE DEPIANUL.itiposer = 'H' AND " + " DEPIANUL.ialtaing = 'I' AND " + " DEPIANUL.fanulaci >" + Serrores.FormatFechaHMS(tempRefParam) + " AND " + " DEPIANUL.fanulaci <" + Serrores.FormatFechaHMS(tempRefParam2) + " AND " + " DEPIANUL.gidenpac = DPACIENT.gidenpac AND " + " DEPIANUL.gsocieda = DSOCIEDA.gsocieda AND " + " DEPIANUL.gusuario = SUSUARIO.gusuario AND " + " SUSUARIO.gpersona = DPERSONA.gpersona AND " + " DEPIANUL.gservici = DSERVICI.gservici " + " ORDER BY fanulaci ASC";
                        stFanulaciF = Convert.ToString(tempRefParam2);
                        stFanulaciI = Convert.ToString(tempRefParam);
                        CrystalReport1.SQLQuery = stSql;
                        break;
                    case "A":
                        object tempRefParam3 = stFanulaciI;
                        object tempRefParam4 = stFanulaciF;
                        stSql = " SELECT * " + " FROM DPACIENT, DEPIANUL,DSOCIEDA, DPERSONA, " + " DSERVICI, SUSUARIO " + " WHERE DEPIANUL.gidenpac =  DPACIENT.gidenpac AND " + " DEPIANUL.gusuario = SUSUARIO.gusuario AND " + " DEPIANUL.fanulaci >" + Serrores.FormatFechaHMS(tempRefParam3) + " AND " + " DEPIANUL.fanulaci <" + Serrores.FormatFechaHMS(tempRefParam4) + " AND " + " DEPIANUL.itiposer ='H' AND " + " DEPIANUL.ialtaing ='A' AND " + " DEPIANUL.gsocieda = DSOCIEDA.gsocieda AND" + " DEPIANUL.gservici = DSERVICI.gservici AND " + " SUSUARIO.gpersona = DPERSONA.gpersona " + " ORDER BY fanulaci ASC";
                        stFanulaciF = Convert.ToString(tempRefParam4);
                        stFanulaciI = Convert.ToString(tempRefParam3);
                        // & " AEPISADM.ganoadme = AMOVIFIN.ganoregi AND " _
                        //' & " AEPISADM.gnumadme = AMOVIFIN.gnumregi AND " _
                        //' & " AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " 
                        //Crystalreport1.SortFields(0) = "+{DEPIANUL.FANULACI}" 						
                        CrystalReport1.SQLQuery = stSql;
                        break;
                }
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Anulaci�n_ingresos_alta:proConsulta", ex);
            }
        }
        private void Anulacion_ingresos_alta_Closed(Object eventSender, EventArgs eventArgs)
        {
        }
    }
}