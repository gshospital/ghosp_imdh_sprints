using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using CrystalWrapper;

namespace ADMISION
{
	public partial class List_censo_de_camas
         : Telerik.WinControls.UI.RadForm
    {
        DataSet Dbt1 = new DataSet();

        public List_censo_de_camas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}


		private void List_censo_de_camas_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		string sql = String.Empty;
		string sql1 = String.Empty;
		string sql2 = String.Empty;
		string sql3 = String.Empty;
		string NombreHo = String.Empty;
		string GrupoHo = String.Empty;
		string DireccHo = String.Empty;
		//Dim PathString As String
		string stSeleccion = String.Empty;
		DataSet Rr1 = null;
		DataSet Rr2 = null;
		DataSet Rr3 = null;
        Crystal CrystalReport1 = null;
        string pagina = String.Empty;



		public void listado_query()
		{

			if (!RbMedico.IsChecked && !RbUnidad.IsChecked)
			{

				listado_con_hijos();
			}
			else
			{
				listado_sin_hijos();
			}
		}

        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {
            string LitPersona = String.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                listado_query();
                
                //    PathString = App.Path
                //CrystalReport1.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""

                if (rbApellidos.IsChecked || rbPlanta.IsChecked || RbHistoria.IsChecked || RbFecha.IsChecked)
                {
                    CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\Agi330f2_CR11.rpt";
                }
                else
                {
                    CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\Agi330f5_CR11.rpt";
                    if (pagina == "s")
                    {
                        CrystalReport1.SectionFormat[0]= "GH1;X;T;X;X;X;X;X;X";
                    }
                    else
                    {
                        CrystalReport1.SectionFormat[0] = "GH1;X;F;X;X;X;X;X;X";
                    }
                }

                if (!RbMedico.IsChecked && !RbUnidad.IsChecked)
                {
                    CrystalReport1.DataFiles[0] = Dbt1.Tables[0];
                }
                else
                {
                    CrystalReport1.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + "";
                    //CrystalReport1.Connect = Conexion_Base_datos.RcAdmision.ConnectionString;
                    CrystalReport1.SQLQuery = sql;
                }

                //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //*******************
                CrystalReport1.Destination = Parasalida;
                CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                CrystalReport1.Formulas["{@SELECCION}"] = "\"" + stSeleccion + "\"";

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = "LISTADO GENERAL DE " + LitPersona.ToUpper() + "S" + " INGRESADOS";
                CrystalReport1.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";

                CrystalReport1.WindowTitle = "Listado de " + LitPersona + "s Ingresados";
                CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                CrystalReport1.Destination = Parasalida;
                CrystalReport1.WindowShowPrintSetupBtn = true;
                if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                }

                //oscar 30/03/04
                CrystalReport1.Formulas["{@VERPRODUCTO}"] = "'" + ((Conexion_Base_datos.bGestionProducto) ? "S" : "N") + "'";
                //-------

                CrystalReport1.Action = 1;
                CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                CrystalReport1.Reset();
                Conexion_Base_datos.vacia_formulas(CrystalReport1, 6);
                cbImprimir.Enabled = false;
                cbPantalla.Enabled = false;
                rbApellidos.IsChecked = false;
                rbPlanta.IsChecked = false;
                RbMedico.IsChecked = false;
                RbUnidad.IsChecked = false;
                if (!FrmHospital.Visible)
                {
                    ChBHosDia.CheckState = CheckState.Checked;
                    ChBHosGeneral.CheckState = CheckState.Checked;
                }
                else
                {
                    ChBHosDia.CheckState = CheckState.Unchecked;
                    ChBHosGeneral.CheckState = CheckState.Unchecked;
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                if (Information.Err().Number == 32755)
                {
                    // CANCELADA IMPRESION
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    RadMessageBox.Show(Information.Err().Number.ToString() + " " + e.Message, Application.ProductName);
                }
            }
        }

        private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{

			paginacion();
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }

        private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
			paginacion();
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }

        private void cbSalir_Click(Object eventSender, EventArgs eventArgs)
		{

			List_censo_de_camas.DefInstance.Close();

		}



		private void ChBHosDia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar();
		}

		private void ChBHosGeneral_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar();
		}

		private void List_censo_de_camas_Load(Object eventSender, EventArgs eventArgs)
		{

			bool MostrarHospitales = false;

			string StSqlTemp = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='VERCAMAS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqlTemp, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
				{
					MostrarHospitales = true;
				}
			}

            rrtemp.Close();

			if (!MostrarHospitales)
			{
				this.Height = 242;
				this.Width = 333;
				FrmHospital.Visible = false;
				cbImprimir.Top = 187;
				cbPantalla.Top = 187;
				this.cbSalir.Top = 187;
				this.ChBHosDia.CheckState = CheckState.Checked;
				this.ChBHosGeneral.CheckState = CheckState.Checked;
			}

			//cambiar los literales en el tipo de ingreso
			StSqlTemp = "SELECT VALFANU1,VALFANU2 FROM SCONSGLO WHERE GCONSGLO='DETIPING'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSqlTemp, Conexion_Base_datos.RcAdmision);
			rrtemp = new DataSet();
			tempAdapter_2.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{
				ChBHosGeneral.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();
				ChBHosDia.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2"]).Trim();
			}

            rrtemp.Close();

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			this.Text = "Listado de " + LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + "s" + " Ingresados- AGI330F2 ";

            CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;

            //    Me.Height = 2160
            //    Me.Width = 4590
            cbImprimir.Enabled = false;
			cbPantalla.Enabled = false;
			//    Printer.Orientation = 2
		}


		private bool isInitializingComponent;
		private void rbApellidos_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				proActivar();
			}
		}

		private void RbFecha_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				proActivar();
			}
		}

		private void RbHistoria_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				proActivar();
			}
		}

		private void RbMedico_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				proActivar();
			}
		}

		private void rbPlanta_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				proActivar();
			}
		}

		private void RbUnidad_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				proActivar();
			}
		}
		private void proActivar()
		{
			if (((((RbUnidad.IsChecked) ? -1 : 0) | ((rbPlanta.IsChecked) ? -1 : 0) | ((RbMedico.IsChecked) ? -1 : 0) | ((RbHistoria.IsChecked) ? -1 : 0) | ((RbFecha.IsChecked) ? -1 : 0) | ((rbApellidos.IsChecked) ? -1 : 0)) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState))) != 0)
			{
				cbImprimir.Enabled = true;
				cbPantalla.Enabled = true;
			}
			else
			{
				cbImprimir.Enabled = false;
				cbPantalla.Enabled = false;
			}

		}

		private void paginacion()
		{
			string texto = String.Empty;


			pagina = " ";
			int irespuesta = 0;
			if (RbMedico.IsChecked || RbUnidad.IsChecked)
			{
				if (RbMedico.IsChecked)
				{
					texto = "M�dico";
				}
				else
				{
					if (RbUnidad.IsChecked)
					{
						texto = "Unidad de enfermer�a";
					}
				}
				irespuesta = (int) RadMessageBox.Show("Desea que muestre una p�gina por cada " + texto, Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button1);
				if (irespuesta == 6)
				{
					pagina = "s";
				}
			}
		}
		private void listado_con_hijos()
		{
            object dbLangSpanish = null;
            object DBEngine = null;
            string LitPersona = String.Empty;
            object oTextoSql = null;
            

            string stsql = "Description=BASE TEMPORAL PARA CUADRO MANDO EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + "";
            string stNomtabla = "CR11_AGI330F2";
            //SI NO EXISTE LA BASE DE DATOS CREARLA
            bool bExiste_base = false;
            Dbt1.CreateTable(stNomtabla, Conexion_Base_datos.PathString + "\\rpt\\CR11_Agi330f2.xsd");
            //crear tabla
            //Obtenemos el diagn�stico al ingreso

            //RELLENAR LA TABLA CON EL DIAGN�STICO Y LAS PRESTACIONES
            //stsql = "SELECT * From EPRESTAC Where (iestsoli='P' or iestsoli='R') and ganoregi = " & viGanoadme & " AND gnumregi = " & viGnumadme

            string sqlTemporal = " SELECT DUNIENFE.dunidenf, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac," + "dpacient.fnacipac,  DCAMASBOVA.gplantas, DCAMASBOva.gcamasbo,DCAMASBOVA.ghabitac," + "DCAMASBOVA.gcamasbo,  HDOSSIER.ghistoria,aepisadm.fllegada,DPERSONA.dap1pERS," + "DPERSONA.dap2pERS, DPERSONA.dnomPERS,  DSOCIEDA.gsocieda,AMOVIFIN.nafiliac," + "DPACIENT.ntelefo1, AEPISADM.gmotause , DPRODUCT.dproducto,count(NEPISNEO.ganoadme) as hijos, DSOCIEDA.dsocieda, dunienfe.itiposer, AEPISADM.faltaadm " + "From " + "   DCAMASBOVA inner join  DPACIENT on  dcamasbova.gidenpac = DPACIENT.gidenpac" + "    inner join  AEPISADM on  dcamasbova.gidenpac = AEPISADM.gidenpac" + "  inner join dunienfe on   dcamasbova.gcounien = DUNIENFE.gunidenf" + "    inner join amovifin on   AMOVIFIN.GANOREGI = AEPISADM.Ganoadme" + "  And AMOVIFIN.GNUMREGI = AEPISADM.Gnumadme" + "   inner join dsocieda on  AMOVIFIN.gsocieda = DSOCIEDA.gsocieda" + "  inner join dpersona on  AEPISADM.gperulti = dpersona.gpersona" + "  left join dproduct on  AEPISADM.gproducto = DPRODUCT.gproducto" + " left join hdossier on   DPACIENT.gidenpac = HDOSSIER.gidenpac" + "  left join nepisneo on  AEPISADM.Ganoadme = nepisneo.Ganoadme and AEPISADM.Gnumadme = nepisneo.Gnumadme" + " Where" + "  dcamasbova.iestcama = 'O' AND" + " DUNIENFE.itiposer = 'H' AND  AMOVIFIN.FFINMOVI IS NULL AND faltplan IS null";

            if (this.ChBHosDia.CheckState == CheckState.Checked && this.ChBHosGeneral.CheckState == CheckState.Unchecked)
            {
                sqlTemporal = sqlTemporal + " and AEPISADM.ganoadme < 1900";
            }
            if (this.ChBHosDia.CheckState == CheckState.Unchecked && this.ChBHosGeneral.CheckState == CheckState.Checked)
            {
                sqlTemporal = sqlTemporal + " and AEPISADM.ganoadme > 1900";
            }


            sqlTemporal = sqlTemporal + " group by DUNIENFE.dunidenf, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac," + "   dpacient.fnacipac,  DCAMASBOVA.gplantas, DCAMASBOva.gcamasbo,DCAMASBOVA.ghabitac," + "  DCAMASBOVA.gcamasbo,  HDOSSIER.ghistoria,aepisadm.fllegada,DPERSONA.dap1pERS," + "  DPERSONA.dap2pERS, DPERSONA.dnomPERS,  DSOCIEDA.gsocieda,AMOVIFIN.nafiliac," + "  DPACIENT.ntelefo1 , AEPISADM.gmotause, DPRODUCT.dproducto, NEPISNEO.Ganoadme, DSOCIEDA.dsocieda, amovifin.nafiliac,dunienfe.itiposer,AEPISADM.faltaadm ";


            sqlTemporal = sqlTemporal + "\n" + "\r" + " ORDER BY ";
            oTextoSql = null;
            stSeleccion = "";
            if (rbApellidos.IsChecked)
            {
                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision).ToLower();
                stSeleccion = "Apellidos y nombre del " + LitPersona;
                sqlTemporal = sqlTemporal + " DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC ";
            }
            if (rbPlanta.IsChecked)
            {
                stSeleccion = "Planta/Habitaci�n/Cama";
                sqlTemporal = sqlTemporal + " DCAMASBOVA.gplantas ASC, DCAMASBOVA.ghabitac ASC, DCAMASBOVA.gcamasbo asc ";
            }
            if (RbHistoria.IsChecked)
            {
                stSeleccion = "N� de Historia";
                sqlTemporal = sqlTemporal + " HDOSSIER.ghistoria ASC, DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";
            }
            if (RbFecha.IsChecked)
            {
                stSeleccion = "Fecha de ingreso";
                sqlTemporal = sqlTemporal + " aepisadm.fllegada ASC, DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";
            }
            if (RbMedico.IsChecked)
            {
                stSeleccion = "M�dico";
                sqlTemporal = sqlTemporal + " DPERSONA.dap1pERS ASC, DPERSONA.dap2pERS ASC, DPERSONA.dnomPERS ASC, DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";
            }
            if (RbUnidad.IsChecked)
            {
                stSeleccion = "Unidad de Enfermer�a";
                sqlTemporal = sqlTemporal + " DUNIENFE.dunidenf ASC,DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";
            }




            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemporal, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            DataTable tabla = Dbt1.Tables[stNomtabla];
            //  Set rrTabla = Dbt1.OpenRecordset("SELECT * FROM " & sqlTemporal, 1, 1)
            //Si tenemos alguna prestaci�n o un diagn�stico, lo registramos.
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow iteration_row in rrtemp.Tables[0].Rows)
                {
                    DataRow rrTabla = tabla.NewRow();

                    //rrTabla["dunidenf"] = iteration_row["dunidenf"];
                    rrTabla["dape1pac"] = iteration_row["dape1pac"];
                    rrTabla["dape2pac"] = iteration_row["dape2pac"];
                    rrTabla["dnombpac"] = iteration_row["dnombpac"];
                    rrTabla["fnacipac"] = Convert.ToDateTime(iteration_row["fnacipac"]).ToString("dd/MM/yyyy");
                    rrTabla["gplantas"] = iteration_row["gplantas"];

                    rrTabla["gcamasbo"] = iteration_row["gcamasbo"];
                    rrTabla["ghabitac"] = iteration_row["ghabitac"];
                    //rrTabla["gcamasbo1"] = iteration_row["gcamasbo"];
                    rrTabla["ghistoria"] = iteration_row["ghistoria"];
                    rrTabla["fllegada"] = Convert.ToDateTime(iteration_row["fllegada"]).ToString("dd/MM/yyyy HH:mm");
                    rrTabla["dap1pERS"] = iteration_row["dap1pERS"];
                    rrTabla["dap2pERS"] = iteration_row["dap2pERS"];
                    rrTabla["dnomPERS"] = iteration_row["dnomPERS"];
                    //rrTabla["gsocieda"] = iteration_row["gsocieda"];
                    rrTabla["nafiliac"] = iteration_row["nafiliac"];
                    rrTabla["ntelefo1"] = iteration_row["ntelefo1"];
                    rrTabla["gmotause"] = iteration_row["gmotause"];
                    rrTabla["dproducto"] = iteration_row["dproducto"];
                    rrTabla["hijos"] = iteration_row["hijos"];
                    rrTabla["dsocieda"] = iteration_row["dsocieda"];
                    rrTabla["itiposer"] = iteration_row["itiposer"];
                    rrTabla["faltaadm"] = iteration_row["faltaadm"];
                    tabla.Rows.Add(rrTabla);
                }
            }

            rrtemp.Close();
        }
        private void listado_sin_hijos()
		{
			string LitPersona = String.Empty;
			object[] MatrizSql = new object[1];
			DLLTextosSql.TextosSql oTextoSql = new DLLTextosSql.TextosSql();
			string tempRefParam = "ADMISION";
			string tempRefParam2 = "AGI330F2";
			string tempRefParam3 = "listado_query";
			short tempRefParam4 = 1;
			sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, MatrizSql, Conexion_Base_datos.RcAdmision));

            sql = sql.Replace("DUNIENFE.dunidenf,", "DUNIENFE.dunidenf,AEPISADM.gdiaging,AEPISADM.odiaging,");


			if (this.ChBHosDia.CheckState == CheckState.Checked && this.ChBHosGeneral.CheckState == CheckState.Unchecked)
			{
				sql = sql + " and AEPISADM.ganoadme < 1900";
			}
			if (this.ChBHosDia.CheckState == CheckState.Unchecked && this.ChBHosGeneral.CheckState == CheckState.Checked)
			{
				sql = sql + " and AEPISADM.ganoadme > 1900";
			}


			sql = sql + "\n" + "\r" + " ORDER BY ";
			oTextoSql = null;
			stSeleccion = "";
			if (rbApellidos.IsChecked)
			{
				LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision).ToLower();
				stSeleccion = "Apellidos y nombre del " + LitPersona;
				sql = sql + " DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC ";
			}
			if (rbPlanta.IsChecked)
			{
				stSeleccion = "Planta/Habitaci�n/Cama";
				sql = sql + " DCAMASBOVA.gplantas ASC, DCAMASBOVA.ghabitac ASC, DCAMASBOVA.gcamasbo asc ";
			}
			if (RbHistoria.IsChecked)
			{
				stSeleccion = "N� de Historia";
				sql = sql + " HDOSSIER.ghistoria ASC, DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";
			}
			if (RbFecha.IsChecked)
			{
				stSeleccion = "Fecha de ingreso";
				sql = sql + " aepisadm.fllegada ASC, DPACIENT.dape1pac ASC, DPACIENT.dape2pac ASC, DPACIENT.dnombpac ASC ";
			}
			if (RbMedico.IsChecked)
			{
				stSeleccion = "M�dico";
				sql = sql + " DPERSONA.dap1pERS ASC, DPERSONA.dap2pERS ASC, DPERSONA.dnomPERS ASC, gplantas ASC,ghabitac ASC, DCAMASBOVA.gcamasbo asc ";
			}
			if (RbUnidad.IsChecked)
			{
				stSeleccion = "Unidad de Enfermer�a";
				sql = sql + " DUNIENFE.dunidenf ASC,gplantas ASC,ghabitac ASC, DCAMASBOVA.gcamasbo asc ";
			}

		}
		private void List_censo_de_camas_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}