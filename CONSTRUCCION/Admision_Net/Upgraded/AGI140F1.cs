using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using System.Transactions;
using Telerik.WinControls;

namespace ADMISION
{
	public partial class Cambio_entidad
		: Telerik.WinControls.UI.RadForm
    {


		string vstfecha_hora_sys = String.Empty;
		string vstfecha = String.Empty;
		string vsthora = String.Empty;

		string Vst_Fechacambio = String.Empty;
		bool Vst_ultimo = false;
		DataSet rrEntidad = null;
		string stSqEntidad = String.Empty; //string sql de la 1� entidad
		string stCodPacE = String.Empty; //gidenpac del paciente
		object vncFiliacion = null;
		string vCodSoc = String.Empty; //c�digo de la 2� sociedad
		string vstActPen = String.Empty; //c�digo act/pensionista
		string vstInspec = String.Empty; //c�digo inspecci�n SS
		string vstSegundos = String.Empty; //segundos de la operaci�n
		string vCod1Soc = String.Empty; //c�digo de la 1� sociedad
		string vstNumfil = String.Empty; //n� de filiaci�n
		int sthorasystem = 0; // horas del sistema
		int stminutossystem = 0; //minutos del sistema
		string dfMax = String.Empty; //hora del �ltimo movimiento
		string Vstfecha_ingreso = String.Empty;
		string Vstfecha_inicial = String.Empty;
		string vstActPenOLD = String.Empty;
		string vstInspecOLD = String.Empty;
		bool fNuevo = false;
		bool fBerror = false;
		bool bVuelvedeFiliacion = false;
		public Cambio_entidad()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}



		public void borra_fact(int Pano, int PNum, string PFini, string PFfin)
		{
			//vaciar fpasfact de amovimie
			//stSqlBorra = "select * from amovimie where " _
			//'            & " ganoregi=" & PAno & " and gnumregi=" & PNum _
			//'            & " and fmovimie>='" & PFini & "' and (ffinmovi<='" & PFini _
			//'            & "' or ffinmovi is null)"
			//Set rrBorra = RcAdmision.OpenResultset(stSqlBorra, 1, 3)
			//If rrBorra.RowCount <> 0 Then
			//    rrBorra.MoveFirst
			//    While Not rrBorra.EOF
			//        rrBorra.Edit
			//        rrBorra("fpasfact") = Null
			//        rrBorra.Update
			//        rrBorra.MoveNext
			//    Wend
			//End If
			//rrBorra.Close
			//'vaciar fpasfact de amovifin
			//stSqlBorra = "select * from amovifin where " _
			//'            & " ganoregi=" & PAno & " and gnumregi=" & PNum _
			//'            & " and fmovimie>='" & PFini & "' and (ffinmovi<='" & PFini _
			//'            & "' or ffinmovi is null)"
			//Set rrBorra = RcAdmision.OpenResultset(stSqlBorra, 1, 3)
			//If rrBorra.RowCount <> 0 Then
			//    rrBorra.MoveFirst
			//    While Not rrBorra.EOF
			//        rrBorra.Edit
			//        rrBorra("fpasfact") = Null
			//        rrBorra.Update
			//        rrBorra.MoveNext
			//    Wend
			//End If
			//rrBorra.Close
			//'vaciar fpasfact de amovrega
			//stSqlBorra = "select * from amovrega where " _
			//'            & " ganoregi=" & PAno & " and gnumregi=" & PNum _
			//'            & " and fmovimie>='" & PFini & "' and (ffinmovi<='" & PFini _
			//'            & "' or ffinmovi is null)"
			//Set rrBorra = RcAdmision.OpenResultset(stSqlBorra, 1, 3)
			//If rrBorra.RowCount <> 0 Then
			//    rrBorra.MoveFirst
			//    While Not rrBorra.EOF
			//        rrBorra.Edit
			//        rrBorra("fpasfact") = Null
			//        rrBorra.Update
			//        rrBorra.MoveNext
			//    Wend
			//End If
			//rrBorra.Close
			//'vaciar fpasfact de aausenci
			//stSqlBorra = "select * from aausenci where " _
			//'            & " ganoADME=" & PAno & " and gnumADME=" & PNum _
			//'            & " and fausenci>='" & PFini & "' and (freavuel<='" & PFini _
			//'            & "' or freavuel is null)"
			//Set rrBorra = RcAdmision.OpenResultset(stSqlBorra, 1, 3)
			//If rrBorra.RowCount <> 0 Then
			//    rrBorra.MoveFirst
			//    While Not rrBorra.EOF
			//        rrBorra.Edit
			//        rrBorra("fpasfact") = Null
			//        rrBorra.Update
			//        rrBorra.MoveNext
			//    Wend
			//End If
			//rrBorra.Close
			//borrar registros de dmovfact
			string stSqlBorra = "select * from dmovfact where " + " ganoregi=" + Pano.ToString() + " and gnumregi=" + PNum.ToString() + " and fsistema>='" + PFini + "' and fsistema<='" + PFini + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlBorra, Conexion_Base_datos.RcAdmision);
			DataSet rrBorra = new DataSet();
			tempAdapter.Fill(rrBorra);
			if (rrBorra.Tables[0].Rows.Count != 0)
			{
			    int i=0;
				foreach (DataRow iteration_row in rrBorra.Tables[0].Rows)
				{
					rrBorra.Delete(tempAdapter, i);
                    i++;
				}
			}
			rrBorra.Close();
		}

		private void proCambiar_Consultas(DataSet RrCursor, string stInspecc)
		{

            //funcion que busca las consultas relacionadas a las prestaciones y les cambia la sociedad
            object tempRefParam = RrCursor.Tables[0].Rows[0]["fmovimie"];
            string tstSql = "select * from cconsult C inner join eprestac  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["Ganoregi"]) + " and e.gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["Gnumregi"]) + " and e.itiposer='h' " + " and e.frealiza >=" + Serrores.FormatFechaHMS(tempRefParam);
            if (!Convert.IsDBNull(RrCursor.Tables[0].Rows[0]["ffinmovi"]))
            {
                object tempRefParam2 = RrCursor.Tables[0].Rows[0]["ffinmovi"];
                tstSql = tstSql + " and e.frealiza <=" + Serrores.FormatFechaHMS(tempRefParam2);
            }
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, Conexion_Base_datos.RcAdmision);
            DataSet tRr1 = new DataSet();
            tempAdapter.Fill(tRr1);

            foreach (DataRow iteration_row in tRr1.Tables[0].Rows)
            {
                tRr1.Edit();
                iteration_row["gsocieda"] = vCodSoc;
                iteration_row["nafiliac"] = tbNafilia.Text.Trim();
                if (stInspecc == "")
                {   
                    iteration_row["ginspecc"] = DBNull.Value;
                }
                else
                {
                    iteration_row["ginspecc"] = stInspecc;
                }
                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(tRr1, tRr1.Tables[0].TableName);
            }            
            tRr1.Close();

            //Tambi�n se cambian los consumos.
            object tempRefParam3 = RrCursor.Tables[0].Rows[0]["fmovimie"];
            tstSql = "select f.* from cconsult C inner join eprestac  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela inner join fconspac f on c.ganoregi=f.ganoregi  and " + " c.gnumregi=f.gnumregi and f.itiposer='C' where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["Ganoregi"]) + " and e.gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["Gnumregi"]) + " and e.itiposer='h' " + " and e.frealiza >=" + Serrores.FormatFechaHMS(tempRefParam3);
            if (!Convert.IsDBNull(RrCursor.Tables[0].Rows[0]["ffinmovi"]))
            {
                object tempRefParam4 = RrCursor.Tables[0].Rows[0]["ffinmovi"];
                tstSql = tstSql + " and e.frealiza <=" + Serrores.FormatFechaHMS(tempRefParam4);
            }
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstSql, Conexion_Base_datos.RcAdmision);
            tRr1 = new DataSet();
            tempAdapter_3.Fill(tRr1);

            foreach (DataRow iteration_row_2 in tRr1.Tables[0].Rows)
            {
                tRr1.Edit();
                iteration_row_2["gsocieda"] = vCodSoc;
                if (stInspecc == "")
                {
                    iteration_row_2["ginspecc"] = DBNull.Value;
                }
                else
                {
                    iteration_row_2["ginspecc"] = stInspecc;
                }
                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
                tempAdapter_3.Update(tRr1, tRr1.Tables[0].TableName);
            }
            tRr1.Close();
        }

		public bool procambiofactura(DataSet RrCursor, string fechaini, string fechafin)
		{
			StringBuilder stSqlFact = new StringBuilder();
			DataSet rrFact = null;
			bool fencontrado = false;
			if (RrCursor.Tables[0].Rows.Count != 0)
			{
				fencontrado = false;
				RrCursor.MoveFirst();
				foreach (DataRow iteration_row in RrCursor.Tables[0].Rows)
				{
					object tempRefParam = iteration_row["fmovimie"];
					stSqlFact = new StringBuilder("select ffactura,fsistema from dmovfact where" + " ganoregi=" + Convert.ToString(iteration_row["ganoregi"]) + " and " + " gnumregi=" + Convert.ToString(iteration_row["gnumregi"]) + " and " + " gsocieda=" + Convert.ToString(iteration_row["gsocieda"]) + " and itiposer = 'H' and " + " nafiliac='" + Convert.ToString(iteration_row["nafiliac"]) + "' and " + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "");
					//quito las estancias porque puede haber otros registros
					if (!Convert.IsDBNull(iteration_row["ffinmovi"]))
					{
						object tempRefParam2 = iteration_row["ffinmovi"];
						stSqlFact.Append(" and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + "");
					}
					stSqlFact.Append(" order by fsistema");
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlFact.ToString(), Conexion_Base_datos.RcAdmision);
					rrFact = new DataSet();
					tempAdapter.Fill(rrFact);
					if (rrFact.Tables[0].Rows.Count != 0)
					{
						rrFact.MoveFirst();
						object tempRefParam3 = rrFact.Tables[0].Rows[0]["fsistema"];
						fechaini = Serrores.FormatFechaHMS(tempRefParam3);
						foreach (DataRow iteration_row_2 in rrFact.Tables[0].Rows)
						{
							if (!Convert.IsDBNull(iteration_row_2["ffactura"]))
							{
								fencontrado = true;
							}
							object tempRefParam4 = iteration_row_2["fsistema"];
							fechafin = Serrores.FormatFechaHMS(tempRefParam4);
						}
					}
					rrFact.Close();
				}
				return !fencontrado;
			}
			else
			{
				return true;
			}
		}

		public void proReciboError(bool fError)
		{
			fBerror = fError;
		}

		public void Recoger_datos(string Nombre2, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Idpaciente, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen = "", string Inspec = "")
		{
			Conexion_Base_datos.NoHacerActivate = true;
			bVuelvedeFiliacion = true;
			if (Idpaciente != "")
			{
				//cambia de sociedad o de numero de filiaci�n
				//O DE INSPECCION O DE PENSIONISTA/ACTIVO
				if (vCod1Soc.Trim() != CodSOC.Trim() || vstNumfil != Asegurado || vstActPenOLD != IndActPen || vstInspecOLD != Inspec)
				{
					vCodSoc = CodSOC;
					tbNafilia.Text = Asegurado;
					vstActPen = IndActPen;
					vstInspec = Inspec;
					proSociedad(Convert.ToInt32(Double.Parse(CodSOC)), tbEntidadA);
					//cbCancelar.Enabled = False
					cbAceptar.Enabled = true;
				}
			}
			else
			{
				//cbCancelar.Enabled = True
				cbAceptar.Enabled = false;
			}
		}


		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
           
            int iGanoregi = 0;
            int iGnumregi = 0;
            string vstFecha_cambio = String.Empty;
            string fIni_cambio = String.Empty;
            string fFin_cambio = String.Empty;
            string fIni_cambio2 = String.Empty;
            string fFin_cambio2 = String.Empty;
            object OEstancias = null;
            string fFecha_ultimo = String.Empty;
            string vstFechaFactura = String.Empty;
            //****************************
            try
            {
                using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                {
                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                        Conexion_Base_datos.RcAdmision.Close();
                    Conexion_Base_datos.RcAdmision.Open();

                    //****************************
                    vstfecha_hora_sys = DateTimeHelper.ToString(DateTime.Now);
                    if (tbHoraCambio.Text == "  :  ")
                    {
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1150, Conexion_Base_datos.RcAdmision, Label19.Text));
                        tbHoraCambio.Focus();
                        cbAceptar.Enabled = false;
                        return;
                    }
                    else if (((int)DateAndTime.DateDiff("n", DateTime.Parse(Vstfecha_ingreso), DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 0)
                    {
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La fecha del cambio", "mayor o igual", "la del ingreso " + Vstfecha_ingreso));
                        tbHoraCambio.Focus();
                        cbAceptar.Enabled = false;
                        return;
                    }
                    //si no ha cambiado de entidad no hacer el movimiento
                    //si cambia la entidad,el n� filiacion,fecha del apunte
                    //aqui empiezo las modificaciones 07/01/1999
                    //si hay algun cambio
                    DataSet trrAnterior = null;
                    if (tbEntidadde.Text.Trim() != tbEntidadA.Text.Trim() || tbNafilia.Text.Trim() != "" || Vstfecha_inicial != sdcFecha.Text + " " + tbHoraCambio.Text + ":" + vstSegundos)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        //RcAdmision.BeginTrans
                        Vst_Fechacambio = DateTimeHelper.ToString(DateTime.Parse(DateTime.Parse(sdcFecha.Text).ToString("dd/MM/yyyy") + " " + tbHoraCambio.Text + ":" + vstSegundos.Trim()));
                        if (Vst_ultimo || fNuevo)
                        {
                            //si es el ultimo apunte o se ha pulsado el boton nuevo
                            //se recupera de amovifin el ultimo movimiento
                            stSqEntidad = "SELECT AMOVIFIN.GSOCIEDA,AMOVIFIN.GINSPECC,NAFILIAC,FMOVIMIE,FFINMOVI,GNUMADME,GANOADME, " + " IACTPENS,GINSPECC,ITIPOSER,GNUMREGI,GANOREGI, AMOVIFIN.gusuario, AMOVIFIN.fsistema  " + " FROM AEPISADM, AMOVIFIN WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AMOVIFIN.ITIPOSER = 'H' AND " + " AEPISADM.GIDENPAC = '" + stCodPacE + "' and" + " AMOVIFIN.FFINMOVI is null";



                            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                            rrEntidad = new DataSet();
                            tempAdapter.Fill(rrEntidad);
                            if (rrEntidad.Tables[0].Rows.Count != 0)
                            {
                                //se recupera un registro
                                iGanoregi = Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["GANOADME"]);
                                iGnumregi = Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gnumadme"]);
                                if (fNuevo)
                                { //si se ha pulsado el boton nuevo
                                  //la fecha del apunte tiene que ser mayor que la del apunte anterior
                                  //si la fecha del apunte es menor que la fecha del sistema hay que
                                  //   comprobar que en DMOVFACT no hay filas con FFechfin > que la fecha del apunte
                                  //si pasa la validacion de fechas hay que modificar FFINMOVI en AMOVIFIN
                                  //grabar un nuevo registro en AMOVIFIN
                                    fFecha_ultimo = Convert.ToDateTime(rrEntidad.Tables[0].Rows[0]["FMOVIMIE"]).ToString("dd/MM/yyyy HH:mm:ss");
                                    if (DateTime.Parse(Vst_Fechacambio) < DateTime.Parse(fFecha_ultimo))
                                    {
                                        //si la fecha del apunte es menor que la fecha del ultimo apunte
                                        //al usar el TransactionScope no se necesita el rollback
                                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                                        rrEntidad.Close();
                                        this.Cursor = Cursors.Default;
                                        Vst_ultimo = false;
                                        fNuevo = false;
                                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La fecha del cambio", "mayor o igual", "la del �ltimo cambio " + Conexion_Base_datos.procambiadia_mes(dfMax)));
                                        if (tbHoraCambio.Enabled)
                                        {
                                            tbHoraCambio.Focus();
                                        }
                                        cbAceptar.Enabled = false;
                                        return;
                                    }
                                    if (DateTime.Parse(Vst_Fechacambio) < DateTime.Now)
                                    {
                                        //si la fecha es menor que la del sistema
                                        //comprobar que en DMOVFACT no hay filas con FFechfin > que la fecha del apunte
                                        vstFechaFactura = "";
                                        if (ComprobarDMOVFACT(rrEntidad, Vst_Fechacambio, vstFechaFactura))
                                        {
                                            //Usando el TransactionScope no se necesita el rollback
                                            //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                                            rrEntidad.Close();
                                            this.Cursor = Cursors.Default;
                                            Vst_ultimo = false;
                                            fNuevo = false;

                                            System.DateTime TempDate = DateTime.FromOADate(0);
                                            string[] tempRefParam8 = new string[] { "La fecha del cambio", "mayor", "la del �ltimo periodo facturado " + ((DateTime.TryParse(vstFechaFactura, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : vstFechaFactura) };
                                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, tempRefParam8));
                                            if (tbHoraCambio.Enabled)
                                            {
                                                tbHoraCambio.Focus();
                                            }
                                            cbAceptar.Enabled = false;
                                            return;
                                        }
                                    }

                                    //se actualiza la fecha de fin de movimiento en amovifin
                                    if (vstInspec.Trim() == "")
                                    {
                                        Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo);
                                    }
                                    else
                                    {
                                        Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo, vstInspec.Trim());
                                    }

                                    rrEntidad.Edit();

                                    rrEntidad.Tables[0].Rows[0]["FFINMOVI"] = DateTime.Parse(sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos);

                                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(rrEntidad, rrEntidad.Tables[0].TableName);

                                    stSqEntidad = "SELECT * FROM AMOVIFIN where 2=1";
                                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                                    rrEntidad = new DataSet();
                                    tempAdapter_3.Fill(rrEntidad);
                                    //y se a�ade un nuevo registro

                                    rrEntidad.AddNew();
                                    rrEntidad.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                    rrEntidad.Tables[0].Rows[0]["FSISTEMA"] = vstfecha_hora_sys;
                                    rrEntidad.Tables[0].Rows[0]["fmovimie"] = DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
                                    rrEntidad.Tables[0].Rows[0]["ganoregi"] = iGanoregi;
                                    rrEntidad.Tables[0].Rows[0]["gnumregi"] = iGnumregi;
                                    rrEntidad.Tables[0].Rows[0]["gsocieda"] = vCodSoc;
                                    rrEntidad.Tables[0].Rows[0]["nafiliac"] = tbNafilia.Text.Trim();
                                    rrEntidad.Tables[0].Rows[0]["itiposer"] = "H";
                                    if (vstActPen != "")
                                    {
                                        rrEntidad.Tables[0].Rows[0]["IACTPENS"] = vstActPen;
                                    }
                                    else
                                    {
                                        rrEntidad.Tables[0].Rows[0]["IACTPENS"] = DBNull.Value;
                                    }
                                    if (vstInspec != "")
                                    {
                                        rrEntidad.Tables[0].Rows[0]["ginspecc"] = vstInspec;
                                    }
                                    else
                                    {
                                        rrEntidad.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
                                    }
                                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
                                    tempAdapter_3.Update(rrEntidad, rrEntidad.Tables[0].TableName);

                                    //oscar 14/03/03
                                    //Nuevo Requerimiento
                                    if (!ACTUALIZAMOVIMIENTOS_DMOVFACT(iGanoregi, iGnumregi))
                                    {
                                        //al usar el TransactionScope no se necesita el rollback
                                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                                        this.Cursor = Cursors.Default;
                                        return;
                                    }
                                    stSqEntidad = "SELECT * FROM AMOVIFIN where GANOREGI = " + iGanoregi.ToString() + " and GNUMREGI = " + iGnumregi.ToString() + " and FFINMOVI is null";
                                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                                    rrEntidad = new DataSet();
                                    tempAdapter_5.Fill(rrEntidad);
                                    if (rrEntidad.Tables[0].Rows.Count != 0)
                                    {
                                        //se modifican las consultas asociadas a prestaciones realizadas en ese intervalo
                                        proCambiar_Consultas(rrEntidad, vstInspec);
                                    }
                                    //------------------

                                    scope.Complete();
                                }
                                else
                                {
                                    //es una modificacion del ultimo movimiento
                                    //en el ultimo movimiento se puede cambiar todo hasta la fecha del apunte
                                    //siempre que no existan movimientos en DMOVFACT
                                    fFecha_ultimo = Convert.ToDateTime(rrEntidad.Tables[0].Rows[0]["FMOVIMIE"]).ToString("dd/MM/yyyy HH:mm:ss");
                                    //si la fecha del apunte es menor que la fecha del ultimo
                                    if (DateTime.Parse(Vst_Fechacambio) < DateTime.Parse(fFecha_ultimo))
                                    {
                                        //al usar el TransactionScope no se necesita el rollback
                                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                                        rrEntidad.Close();
                                        this.Cursor = Cursors.Default;
                                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La fecha del cambio", "mayor o igual", "la del �ltimo cambio " + Conexion_Base_datos.procambiadia_mes(dfMax)));
                                        if (tbHoraCambio.Enabled)
                                        {
                                            tbHoraCambio.Focus();
                                        }
                                        cbAceptar.Enabled = false;
                                        return;
                                    }
                                    else
                                    {
                                        //si la secuencia de fechas es correcta
                                        //hay que comprobar que no existen registros en DMOVFACT
                                        vstFechaFactura = "";
                                        if (ComprobarDMOVFACT(rrEntidad, Vst_Fechacambio, vstFechaFactura))
                                        {
                                            //al usar el TransactionScope no se necesita el rollback
                                            //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                                            
                                            rrEntidad.Close();
                                            this.Cursor = Cursors.Default;
                                            //hay movimientos facturados
                                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1940, Conexion_Base_datos.RcAdmision, "modificar", "ya ha sido facturado"));
                                            if (tbHoraCambio.Enabled)
                                            {
                                                tbHoraCambio.Focus();
                                            }
                                            cbAceptar.Enabled = false;
                                            return;
                                        }
                                        else
                                        {
                                            //no hay registros en DMOVFACT se actualizan los datos en AMOVIFIN
                                            if (vstInspec.Trim() == "")
                                            {
                                                Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo);
                                            }
                                            else
                                            {
                                                Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo, vstInspec.Trim());
                                            }

                                            //oscar 08/05/03
                                            ActualizarDMOVFACT(rrEntidad);
                                            //-------

                                            //cambiamos la fecha de fin de movimiento del registro anterior
                                            object tempRefParam13 = rrEntidad.Tables[0].Rows[0]["fmovimie"];
                                            stSqEntidad = "SELECT ffinmovi " + " FROM AMOVIFIN WHERE " + " AMOVIFIN.GNUMREGI = " + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and AMOVIFIN.GANOREGI = " + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) + " and AMOVIFIN.ITIPOSER = 'H' AND " + " AMOVIFIN.FFINMOVI =" + Serrores.FormatFechaHMS(tempRefParam13) + "";
                                            SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                                            trrAnterior = new DataSet();
                                            tempAdapter_6.Fill(trrAnterior);

                                            if (trrAnterior.Tables[0].Rows.Count != 0)
                                            {
                                                trrAnterior.Edit();
                                                trrAnterior.Tables[0].Rows[0]["ffinmovi"] = DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
                                                SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_6);
                                                tempAdapter_6.Update(trrAnterior, trrAnterior.Tables[0].TableName);

                                            }
                                            rrEntidad.Edit();
                                            if (SprEntidades.MaxRows - 1 != 0)
                                            {
                                                rrEntidad.Tables[0].Rows[0]["fmovimie"] = DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
                                            }
                                            if (vCodSoc.Trim() != "")
                                            {
                                                rrEntidad.Tables[0].Rows[0]["gsocieda"] = vCodSoc;
                                            }
                                            rrEntidad.Tables[0].Rows[0]["nafiliac"] = tbNafilia.Text.Trim();
                                            rrEntidad.Tables[0].Rows[0]["itiposer"] = "H";
                                            if (vstActPen != "")
                                            {
                                                rrEntidad.Tables[0].Rows[0]["IACTPENS"] = vstActPen;
                                            }
                                            else
                                            {
                                                rrEntidad.Tables[0].Rows[0]["IACTPENS"] = DBNull.Value;
                                            }
                                            if (vstInspec != "")
                                            {
                                                rrEntidad.Tables[0].Rows[0]["ginspecc"] = vstInspec;
                                            }
                                            else
                                            {
                                                rrEntidad.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
                                            }
                                            rrEntidad.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                            rrEntidad.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;

                                            //se tiene que actualizar en AEPISADM porque es el ultimo registro
                                            //rrEntidad.Update
                                            //se modifican las consultas asociadas a prestaciones realizadas en ese intervalo
                                            proCambiar_Consultas(rrEntidad, vstInspec);

                                            SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter);
                                            tempAdapter.Update(rrEntidad, rrEntidad.Tables[0].TableName);
                                            if (!ActualizamosMovimientos_De_Farmacos_En_Dmovfact_YAsiDeLargaHaQuedado_YOtroPocoMas(vCodSoc, vstInspec))
                                            {
                                                //al usar el TransactionScope no se necesita el rollback
                                                rrEntidad.Close();
                                                this.Cursor = Cursors.Default;
                                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1170, Conexion_Base_datos.RcAdmision, "tempRefParam15"));
                                                return;
                                            }
                                            scope.Complete();
                                        }
                                    }
                                } //fin modificacion ultimo movimiento
                            }
                            else
                            {
                                //no se recupera ningun registro de AMOVIFIN
                            }
                            //UPGRADE_ISSUE: (2064) RDO.rdoResultset method rrEntidad.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                            rrEntidad.Close();
                        }
                        else
                        {
                            //es un apunte intermedio o se ha pulsado cambio,si es el ultimo entrara por el primer punto
                            //en los movimientos intermedios se puede modificar todo excepto la fecha del apunte
                            //recuperamos de AMOVIFIN el registro seleccionado
                            //en fecha_ultimo cargo fmovimie que he seleccionado
                            if (SprEntidades.CurrentRow.Index >= 0)
                            {
                                SprEntidades.Row = SprEntidades.CurrentRow.Index + 1;
                                SprEntidades.Col = 1;
                                fFecha_ultimo = DateTime.Parse(SprEntidades.Text).ToString("dd/MM/yyyy HH:mm:ss");
                                //en fecha_ultimo se carga la fecha del registro seleccionado
                            }
                            object tempRefParam16 = fFecha_ultimo;
                            stSqEntidad = "SELECT AMOVIFIN.* " + " FROM AEPISADM, AMOVIFIN WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AMOVIFIN.ITIPOSER = 'H' AND " + " AEPISADM.GIDENPAC = '" + stCodPacE + "' and" + " FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam16) + "";
                            fFecha_ultimo = Convert.ToString(tempRefParam16);

                            SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                            rrEntidad = new DataSet();
                            tempAdapter_9.Fill(rrEntidad);
                            if (rrEntidad.Tables[0].Rows.Count != 0)
                            {
                                if (ComprobarFacturaDMOVFACT(rrEntidad))
                                {
                                    //comprobar si existen en DMOVFACT registros para el movimiento seleccionado
                                    //modificacion 15/07/1999
                                    string[] tempRefParam18 = new string[] { "modificar", "ya ha sido facturado" };
                                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1940, Conexion_Base_datos.RcAdmision, "modificar", "ya ha sido facturado"));
                                    rrEntidad.Close();
                                    Vst_ultimo = true;
                                    proRellena_Grid();
                                    tbEntidadA.Text = "";
                                    tbNafilia.Text = "";
                                    cbAceptar.Enabled = false;
                                    this.Cursor = Cursors.Default;
                                    return;
                                    //***inicio antes 15/07/1999
                                    //                'mandar mensaje de que existen movimimentos facturados
                                    //                IResume = oClass.RespuestaMensaje(vNomAplicacion, vAyuda, 1170, RcAdmision, "actualizar", "la entidad seleccionada")
                                    //                If IResume = 7 Or IResume = 2 Then 'si se responde que no o 2 cancelar
                                    //                'se deja todo como estaba
                                    //                    RrEntidad.Close
                                    //                    Vst_ultimo = True
                                    //                    proRellena_Grid
                                    //                    cbAceptar.Enabled = False
                                    //                    Me.MousePointer = 0
                                    //                    Exit Sub
                                    //                End If
                                    //***fin antes 15/07/1999

                                }

                                //si se responde que si o no hay movimientos facturados
                                //se actualizan los datos en dmovfact
                                ActualizarDMOVFACT(rrEntidad);

                                if (vstInspec.Trim() == "")
                                {
                                    Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo);
                                }
                                else
                                {
                                    Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo, vstInspec.Trim());
                                }

                                //oscar 26/03/03
                                //Nuevo requerimiento
                                proCambiar_Consultas(rrEntidad, vstInspec);

                                //---------------

                                //se actualizan los datos en AMOVIFIN
                                rrEntidad.Edit();

                                rrEntidad.Tables[0].Rows[0]["gsocieda"] = vCodSoc;
                                rrEntidad.Tables[0].Rows[0]["nafiliac"] = tbNafilia.Text.Trim();
                                rrEntidad.Tables[0].Rows[0]["itiposer"] = "H";
                                if (vstActPen != "")
                                {
                                    rrEntidad.Tables[0].Rows[0]["IACTPENS"] = vstActPen;
                                }
                                else
                                {
                                    rrEntidad.Tables[0].Rows[0]["IACTPENS"] = DBNull.Value;
                                }
                                if (vstInspec != "")
                                {
                                    rrEntidad.Tables[0].Rows[0]["ginspecc"] = vstInspec;
                                }
                                else
                                {
                                    rrEntidad.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
                                }
                                rrEntidad.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                rrEntidad.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;

                                SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_9);
                                tempAdapter_9.Update(rrEntidad, rrEntidad.Tables[0].TableName);

                                scope.Complete();
                            }
                            else
                            {
                                //no hay filas en DMOVFACT
                                //se actualizan los datos en AMOVIFIN excepto la fecha que no se permite
                                //para que no descabale la secuencia
                                if (vstInspec.Trim() == "")
                                {
                                    Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo);
                                }
                                else
                                {
                                    Conexion_Base_datos.ActualizarSociedadConsumos(rrEntidad, vCodSoc, DateTime.Parse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text), fNuevo, vstInspec.Trim());
                                }

                                //oscar 26/03/03
                                //Nuevo requerimiento
                                proCambiar_Consultas(rrEntidad, vstInspec);
                                //---------------

                                rrEntidad.Edit();
                                rrEntidad.Tables[0].Rows[0]["gsocieda"] = vCodSoc;
                                rrEntidad.Tables[0].Rows[0]["nafiliac"] = tbNafilia.Text.Trim();
                                rrEntidad.Tables[0].Rows[0]["itiposer"] = "H";
                                if (vstActPen != "")
                                {
                                    rrEntidad.Tables[0].Rows[0]["IACTPENS"] = vstActPen;
                                }
                                else
                                {
                                    rrEntidad.Tables[0].Rows[0]["IACTPENS"] = DBNull.Value;
                                }
                                if (vstInspec != "")
                                {
                                    rrEntidad.Tables[0].Rows[0]["ginspecc"] = vstInspec;
                                }
                                else
                                {
                                    rrEntidad.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
                                }

                                rrEntidad.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                rrEntidad.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;

                                SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapter_9);
                                tempAdapter_9.Update(rrEntidad, rrEntidad.Tables[0].TableName);
                            }

                            rrEntidad.Close();
                            if (!ActualizamosMovimientos_De_Farmacos_En_Dmovfact_YAsiDeLargaHaQuedado_YOtroPocoMas(vCodSoc, vstInspec))
                            {
                                //al usar el TransactionScope no se necesita el rollback
                                //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                                rrEntidad.Close();
                                this.Cursor = Cursors.Default;
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1170, Conexion_Base_datos.RcAdmision, "actualizar", "la entidad seleccionada"));
                                return;
                            }
                        }
                    }
                    else
                    {
                        //no hay ningun cambio
                    }

                    // ir al �ltimo
                    if (SprEntidades.MaxRows != 0)
                    {
                        SprEntidades.Row = SprEntidades.MaxRows;
                        SprEntidades.Col = 2;
                        tbEntidadde.Text = SprEntidades.Text;
                    }
                    else
                    {
                        tbEntidadde.Text = " ";
                    }
                    scope.Complete();

                    this.Cursor = Cursors.Default;
                    Vst_ultimo = true;
                    fNuevo = false;
                    proRellena_Grid();
                    tbEntidadA.Text = "";
                    tbNafilia.Text = "";
                    cbAceptar.Enabled = false;
                }//Fin Transacci�n
            }
            catch (SqlException ex)
            {
                //**********************
                //al usar el TransactionScope no se necesita el rollback
                //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                this.Cursor = Cursors.Default;
                Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }


        private void CbCambio_Click(Object eventSender, EventArgs eventArgs)
		{
			//llamo a la dll de filiaci�n
			try
			{
				//******
				fNuevo = false;
				this.Cursor = Cursors.WaitCursor;
                /*INDRA jproche_TODO_X_4
                //vncFiliacion = new filiacionDLL.Filiacion();
				//UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				vncFiliacion.Load(vncFiliacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, stCodPacE);
                */
            vncFiliacion = null;
				this.Show();
				this.Cursor = Cursors.Default;
			}
			catch
			{
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, Conexion_Base_datos.RcAdmision, Label19.Text));
			}
		}

		public void proRecPaciente(string stCodpac, string stNombPac)
		{
			//********
			try
			{
				//*********
				fnMaximo(stCodpac); //obtiene la fecha y hora del �ltimo movimiento
				LbPaciente.Text = stNombPac;
				stCodPacE = stCodpac;
				tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
				vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));
				//*********************************
				stSqEntidad = "SELECT AMOVIFIN.GSOCIEDA,dpacient.nafiliac," + " AMOVIFIN.GINSPECC,AMOVIFIN.IACTPENS " + " FROM AEPISADM, AMOVIFIN,dpacient " + " WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AEPISADM.Gidenpac = dpacient.gidenpac AND " + " AMOVIfin.FMOVIMIE = " + Serrores.FormatFechaHMS(dfMax) + " AND " + " AEPISADM.GIDENPAC = '" + stCodpac + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
				rrEntidad = new DataSet();
				tempAdapter.Fill(rrEntidad);
				vCod1Soc = Convert.ToString(rrEntidad.Tables[0].Rows[0]["gsocieda"]);
				if (Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["NAFILIAC"]))
				{
					vstNumfil = "";
				}
				else
				{
					vstNumfil = Convert.ToString(rrEntidad.Tables[0].Rows[0]["nafiliac"]);
				}
				proSociedad(Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gsocieda"]), tbEntidadde);
				if (Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["GINSPECC"]))
				{
					vstInspecOLD = "";
				}
				else
				{
					vstInspecOLD = Convert.ToString(rrEntidad.Tables[0].Rows[0]["GINSPECC"]);
				}
				if (Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["IACTPENS"]))
				{
					vstActPenOLD = "";
				}
				else
				{
					vstActPenOLD = Convert.ToString(rrEntidad.Tables[0].Rows[0]["IACTPENS"]);
				}
				rrEntidad.Close();
			}
			catch (SqlException ex)
			{
				//**************
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbEliminar_Click(Object eventSender, EventArgs eventArgs)
		{
           string stfecha = String.Empty;
           string stSqlFact = String.Empty;
           DataSet rrFact = null;
           string stSqEntidad = String.Empty;
           string stSqConsultas = String.Empty;
           DataSet rrEntidad = null;
           DataSet rrConsultas = null;
           int sociedad = 0;
           string stInspBorrada = String.Empty;
           bool NoActualizarDMOVFACT = false;
           int iFarmacia = 0;
           float siFrascos = 0;

           try
           {
               this.Cursor = Cursors.WaitCursor;
               vstfecha_hora_sys = DateTimeHelper.ToString(DateTime.Now);

               //solo se activa cuando es el ultimo registro
               //y solo se permite borrar cuando no hay registros en DMOVFACT
               bool bNSocFacFar = false;
               bool bASocFacFar = false; //Antigua sociedad //Nueva sociedad
               if (SprEntidades.CurrentRow.Index + 1 == SprEntidades.MaxRows && SprEntidades.MaxRows > 1)
               { //si estamos en el ultimo registro
                   SprEntidades.Row = SprEntidades.CurrentRow.Index + 1;
                   SprEntidades.Col = 1;
                   stfecha = DateTime.Parse(SprEntidades.Text).ToString("dd/MM/yyyy HH:mm:ss");
                   //buscamos el movimiento en amovifin
                   stSqEntidad = "SELECT AMOVIFIN.*,gidenpac " + " FROM AEPISADM, AMOVIFIN WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AMOVIFIN.ITIPOSER = 'H' AND " + " AEPISADM.GIDENPAC = '" + stCodPacE + "' and" + " FMOVIMIE = " + Serrores.FormatFechaHMS(stfecha) + "";

                   SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                   rrEntidad = new DataSet();
                   tempAdapter.Fill(rrEntidad);
                   if (rrEntidad.Tables[0].Rows.Count != 0)
                   {
                       //comprobar si existen filas en DMOVFACT
                       //oscar 08/05/03
                       sociedad = Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gsocieda"]);
                       stInspBorrada = (Convert.ToString(rrEntidad.Tables[0].Rows[0]["ginspecc"]) + "").Trim();
                       //---------

                       object tempRefParam2 = rrEntidad.Tables[0].Rows[0]["fmovimie"];
                       stSqlFact = "select * from dmovfact where" + " itiposer = 'H' and " + " ganoregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) + " and " + " gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and " + " gsocieda=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gsocieda"]) + " and " + " nafiliac='" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["nafiliac"]) + "' " + " and ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam2) + "" + " and ffactura is not null ";

                       if (!Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["ffinmovi"]))
                       {
                           stSqlFact = stSqlFact + " and ffechfin <=" + Serrores.FormatFechaHMS(stfecha) + "";
                       }

                       if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
                       {
                           string tempRefParam4 = "DMOVFACT";
                           stSqlFact = stSqlFact + " UNION ALL " + Serrores.proReplace(ref stSqlFact, tempRefParam4, "DMOVFACH");
                       }

                       SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
                       rrFact = new DataSet();
                       tempAdapter_2.Fill(rrFact);
                        if (rrFact.Tables[0].Rows.Count != 0)
                        {
                            //si hay filas en DMOVFACT no se le permite eliminar los registros
                            rrEntidad.Close();
                            this.Cursor = Cursors.Default;
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1940, Conexion_Base_datos.RcAdmision, "eliminar", "ya ha sido facturado"));
                        }
                        else
                        {
                            //si no hay filas en dmovfact se puede borrar el ultimo registro
                            rrEntidad.Close();
                            //UPGRADE_ISSUE: (2064) RDO.rdoConnection method RcAdmision.BeginTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                            {
                                //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                                if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                                    Conexion_Base_datos.RcAdmision.Close();
                                Conexion_Base_datos.RcAdmision.Open();

                                //Comprobamos si est� activo el volcado de f�rmacos a DMOVFAC
                                NoActualizarDMOVFACT = false;
                                //Si la constante global "CONTALMA" en el valfanu3 tiene valor "N" y para episodios
                                //hospitalizacion no se graban los farmacos en DMOVFACT
                                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter("select valfanu3 from sconsglo where gconsglo='CONTALMA'", Conexion_Base_datos.RcAdmision);
                                rrConsultas = new DataSet();
                                tempAdapter_3.Fill(rrConsultas);
                                if (rrConsultas.Tables[0].Rows.Count != 0)
                                {
                                    NoActualizarDMOVFACT = ((Convert.ToString(rrConsultas.Tables[0].Rows[0]["valfanu3"]) + "").Trim().ToUpper() == "N");
                                }
                                rrConsultas.Close();


                                //Borramos el registro financiero.
                                stSqEntidad = "SELECT * FROM AMOVIFIN " + " WHERE STR(GANOREGI)+STR(GNUMREGI) IN (" + " SELECT STR(GANOADME)+STR(GNUMADME) FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodPacE + "' AND " + " FALTPLAN IS NULL) and ITIPOSER = 'H' AND " + " FMOVIMIE = " + Serrores.FormatFechaHMS(stfecha) + "";

                                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                                rrEntidad = new DataSet();
                                tempAdapter_4.Fill(rrEntidad);
                                if (rrEntidad.Tables[0].Rows.Count == 1)
                                {
                                    rrEntidad.Delete(tempAdapter_4, 0);
                                }
                                rrEntidad.Close();
                                //se tiene que modificar los datos del ultimo movimiento
                                //en el que ffinmovi es igual que fmovimie del que he borrado
                                stSqEntidad = "SELECT * FROM AMOVIFIN " + " WHERE STR(GANOREGI)+STR(GNUMREGI) IN (" + " SELECT STR(GANOADME)+STR(GNUMADME) FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodPacE + "' AND " + " FALTPLAN IS NULL) and ITIPOSER = 'H' AND " + " FFINMOVI= " + Serrores.FormatFechaHMS(stfecha) + "";

                                SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSqEntidad, Conexion_Base_datos.RcAdmision);
                                rrEntidad = new DataSet();
                                tempAdapter_5.Fill(rrEntidad);
                                //modifica el �ltimo movimiento con los cambios realizados
                                if (rrEntidad.Tables[0].Rows.Count != 0)
                                {
                                    rrEntidad.Edit();
                                    rrEntidad.Tables[0].Rows[0]["FFINMOVI"] = DBNull.Value;
                                    rrEntidad.Tables[0].Rows[0]["gusuario"] = Conexion_Base_datos.VstCodUsua;
                                    rrEntidad.Tables[0].Rows[0]["Fsistema"] = vstfecha_hora_sys;
                                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_5);
                                    tempAdapter_5.Update(rrEntidad, rrEntidad.Tables[0].TableName);

                                    // habra que  actualizar las filas de FCONSPAC con la �ltima sociedad que quede.

                                    object tempRefParam9 = stfecha;
                                    SqlCommand tempCommand = new SqlCommand(" Update FCONSPAC set gsocieda='" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gsocieda"]) + "', ginspecc='" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ginspecc"]) + "' " +
                                                             " where itiposer='H' and ganoregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) +
                                                             " and gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and foper>=" + Serrores.FormatFechaHMS(tempRefParam9) + "", Conexion_Base_datos.RcAdmision);
                                    tempCommand.ExecuteNonQuery();
                                    stfecha = Convert.ToString(tempRefParam9);

                                    //oscar 08/05/03
                                    // habra que  actualizar las filas de DMOVFACT con la �ltima sociedad que quede.
                                    object tempRefParam10 = stfecha;
                                    SqlCommand tempCommand_2 = new SqlCommand(" Update DMOVFACT set gsocieda=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gsocieda"]) + ", ginspecc='" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ginspecc"]) + "',NAFILIAC='" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["NAFILIAC"]) + "'" +
                                                               " where itiposer='H' and ganoregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) +
                                                               " and gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) +
                                                               " and ffinicio>=" + Serrores.FormatFechaHMS(tempRefParam10) + "" +
                                                               " and gsocieda=" + sociedad.ToString(), Conexion_Base_datos.RcAdmision);
                                    tempCommand_2.ExecuteNonQuery();
                                    stfecha = Convert.ToString(tempRefParam10);
                                    //------
                                    //concha 24/09/03
                                    //se modifican las consultas asociadas a prestaciones realizadas con la sociedad
                                    //activa en ese momento
                                    object tempRefParam11 = stfecha;
                                    stSqConsultas = "select * from cconsult C inner join eprestac  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) + " and e.gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and e.itiposer='H' and e.frealiza >=" + Serrores.FormatFechaHMS(tempRefParam11);
                                    stfecha = Convert.ToString(tempRefParam11);
                                    SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                    rrConsultas = new DataSet();
                                    tempAdapter_7.Fill(rrConsultas);
                                    foreach (DataRow iteration_row in rrConsultas.Tables[0].Rows)
                                    {
                                        rrConsultas.Edit();
                                        iteration_row["gsocieda"] = rrEntidad.Tables[0].Rows[0]["gsocieda"];
                                        iteration_row["nafiliac"] = rrEntidad.Tables[0].Rows[0]["nafiliac"];
                                        if (Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["ginspecc"]))
                                        {
                                            iteration_row["ginspecc"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            iteration_row["ginspecc"] = rrEntidad.Tables[0].Rows[0]["ginspecc"];
                                        }

                                        SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_7);
                                        tempAdapter_7.Update(rrConsultas, rrConsultas.Tables[0].TableName);
                                    }
                                    //se modifican los consumos de las consultas asociadas a prestaciones realizadas con la sociedad
                                    //activa en ese momento
                                    object tempRefParam12 = stfecha;
                                    stSqConsultas = "select f.* from cconsult C inner join eprestac  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela inner join fconspac f on c.ganoregi=f.ganoregi  and " + " c.gnumregi=f.gnumregi and f.itiposer='C' where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) + " and e.gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and e.itiposer='h' " + " and e.frealiza >=" + Serrores.FormatFechaHMS(tempRefParam12);
                                    stfecha = Convert.ToString(tempRefParam12);
                                    SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                    rrConsultas = new DataSet();
                                    tempAdapter_9.Fill(rrConsultas);

                                    foreach (DataRow iteration_row_2 in rrConsultas.Tables[0].Rows)
                                    {
                                        rrConsultas.Edit();
                                        iteration_row_2["gsocieda"] = rrEntidad.Tables[0].Rows[0]["gsocieda"];
                                        if (Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["ginspecc"]))
                                        {
                                            iteration_row_2["ginspecc"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            iteration_row_2["ginspecc"] = rrEntidad.Tables[0].Rows[0]["ginspecc"];
                                        }
                                        SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_9);
                                        tempAdapter_9.Update(rrConsultas, rrConsultas.Tables[0].TableName);
                                    }
                                    rrConsultas.Close();
                                    //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    //si existe facturaci�n desde ifms en una fecha posterior a la fecha de inicio de movimiento de la sociedad borrada y est� activa la constante contalma,
                                    //entonces, comprobamos si es necesario a�adir o borrar f�rmacos en funci�n de la antigua y nueva sociedad.
                                    object tempRefParam13 = stfecha;
                                    stSqConsultas = "SELECT max(FULTFACT) FULTFACT FROM APERFACT WHERE GANOADME = " + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) + " and " +
                                                    " GNUMADME = " + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and " +
                                                    " FULTFACT > " + Serrores.FormatFechaHMS(tempRefParam13);
                                    stfecha = Convert.ToString(tempRefParam13);
                                    SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                    rrConsultas = new DataSet();
                                    tempAdapter_11.Fill(rrConsultas);
                                    if (!Convert.IsDBNull(rrConsultas.Tables[0].Rows[0]["FULTFACT"]) && !NoActualizarDMOVFACT)
                                    {
                                        //Comprobamos si la nueva sociedad factura los f�rmacos.
                                        bNSocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(rrEntidad.Tables[0].Rows[0]["gsocieda"]), (Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["ginspecc"])) ? "" : Convert.ToString(rrEntidad.Tables[0].Rows[0]["ginspecc"]).Trim());
                                        //Comprobamos si la antigua sociedad facturaba los f�rmacos.
                                        bASocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Conversion.Val(sociedad.ToString())), stInspBorrada);

                                        //Si s� pasa y la eliminada no los pasaba, hay que introducir los f�rmacos
                                        if (bNSocFacFar && !bASocFacFar)
                                        {
                                            //                            vCod1Soc = sociedad
                                            //                            vstInspecOLD = stInspBorrada
                                            //
                                            //                            If Not ActualizamosMovimientos_De_Farmacos_En_Dmovfact_YAsiDeLargaHaQuedado_YOtroPocoMas(rrEntidad("gsocieda"), IIf(IsNull(rrEntidad("ginspecc")), "", Trim(rrEntidad("ginspecc")))) Then
                                            //                                RcAdmision.RollbackTrans
                                            //                                Me.MousePointer = 0
                                            //                                GestionErrorBaseDatos Err.Number, RcAdmision
                                            //                            End If







                                            //Obtenemos el servicio de farmacia
                                            stSqConsultas = "select nnumeri1, nnumeri2 from sconsglo where gconsglo = 'SERVREAL'";
                                            SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                            rrFact = new DataSet();
                                            tempAdapter_12.Fill(rrFact);
                                            iFarmacia = Convert.ToInt32(rrFact.Tables[0].Rows[0]["nnumeri1"]);
                                            //rrFact.Close

                                            //'Comprobamos si existe alguna toma que se hab�a facturado en el periodo en cuesti�n pero que se elimin� en un cambio anterior y, con la nueva sociedad hay que incorporarlo.
                                            object tempRefParam14 = rrEntidad.Tables[0].Rows[0]["FMOVIMIE"];
                                            object tempRefParam15 = rrConsultas.Tables[0].Rows[0]["FULTFACT"];
                                            stSqConsultas = "select distinct A.GANOADME,A.GNUMADME,B.gfarmaco, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " +
                                                            "FROM  AEPISADM A INNER JOIN ETOMASFA B ON A.GANOADME = B.GANOREGI AND A.GNUMADME = B.GNUMREGI AND 'H' = B.ITIPOSER " +
                                                            "INNER JOIN AMOVIFIN C ON B.GANOREGI = C.GANOREGI AND B.GNUMREGI = C.GNUMREGI AND B.ITIPOSER = C.ITIPOSER " +
                                                            "INNER JOIN AMOVIMIE D ON D.itiposer=C.itiposer and D.ganoregi=C.ganoregi and D.gnumregi=C.gnumregi " +
                                                            "inner join DCODFARMVT E on B.gfarmaco = E.gfarmaco " +
                                                            "where B.fpasfact is null and (B.iproppac is null or B.iproppac <> 'S') and cdosimpu is not null and gfarmadm is null and " +
                                                            "A.GIDENPAC = '" + stCodPacE + "' and A.FALTPLAN IS NULL  and B.FTOMASFA >= " + Serrores.FormatFechaHMS(tempRefParam14) + " and " +
                                                            "B.FTOMASFA <= " + Serrores.FormatFechaHMS(tempRefParam15) + " and " +
                                                            "(C.fmovimie<=B.ftomasfa and (C.ffinmovi>=B.ftomasfa or C.ffinmovi is null)) and " +
                                                            "(D.fmovimie<=B.ftomasfa and (D.ffinmovi>=B.ftomasfa or D.ffinmovi is null))  " +
                                                            " group by B.gfarmaco, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior,A.GANOADME,A.GNUMADME ";
                                            stSqConsultas = stSqConsultas + "Union All ";

                                            object tempRefParam16 = rrEntidad.Tables[0].Rows[0]["FMOVIMIE"];
                                            object tempRefParam17 = rrConsultas.Tables[0].Rows[0]["FULTFACT"];
                                            stSqConsultas = stSqConsultas + "select distinct A.GANOADME,A.GNUMADME,B.gfarmadm, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " +
                                                            "FROM  AEPISADM A INNER JOIN ETOMASFA B ON A.GANOADME = B.GANOREGI AND A.GNUMADME = B.GNUMREGI AND 'H' = B.ITIPOSER " +
                                                            "INNER JOIN AMOVIFIN C ON B.GANOREGI = C.GANOREGI AND B.GNUMREGI = C.GNUMREGI AND B.ITIPOSER = C.ITIPOSER " +
                                                            "INNER JOIN AMOVIMIE D ON D.itiposer=C.itiposer and D.ganoregi=C.ganoregi and D.gnumregi=C.gnumregi " +
                                                            "inner join DCODFARMVT E on B.gfarmadm = E.gfarmaco  " +
                                                            "WHERE B.fpasfact is null and (B.iproppac is null or B.iproppac <> 'S') AND cdosimpu is not null and gfarmadm is NOT null and " +
                                                            "A.GIDENPAC = '" + stCodPacE + "' and A.FALTPLAN IS NULL  and B.FTOMASFA >= " + Serrores.FormatFechaHMS(tempRefParam16) + " and " +
                                                            "B.FTOMASFA <= " + Serrores.FormatFechaHMS(tempRefParam17) + " and " +
                                                            "(C.fmovimie<=B.ftomasfa and (C.ffinmovi>=B.ftomasfa or C.ffinmovi is null)) and " +
                                                            "(D.fmovimie<=B.ftomasfa and (D.ffinmovi>=B.ftomasfa or D.ffinmovi is null))  " +
                                                            "group by B.gfarmadm, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior,A.GANOADME,A.GNUMADME ";

                                            SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                            rrFact = new DataSet();
                                            tempAdapter_13.Fill(rrFact);

                                            foreach (DataRow iteration_row_3 in rrFact.Tables[0].Rows)
                                            {
                                                //grabo una fila en DMOVFACT por cada f�rmaco pendiente de facturar
                                                // primero paso la cantidad consumida a unidades de medida

                                                if (!Convert.IsDBNull(iteration_row_3["nequival"]) && !Convert.IsDBNull(iteration_row_3["cporenva"]))
                                                {
                                                    siFrascos = ((float)(Conversion.Val(Convert.ToString(iteration_row_3["cantidad"])) / Convert.ToDouble(iteration_row_3["nequival"])));
                                                }
                                                else
                                                {
                                                    // si no est�n rellenos los campos en farmacia no le cobro porque no
                                                    // puedo calcular la cantidad consumida
                                                    siFrascos = 0;
                                                }
                                                if (siFrascos != 0)
                                                {

                                                    //grabar registro en dmovfact
                                                    string tempRefParam18 = siFrascos.ToString().Trim();
                                                    string tempRefParam19 = Serrores.ObtenerSeparadorDecimalBD(Conexion_Base_datos.RcAdmision);
                                                    object tempRefParam20 = iteration_row_3["fechamin"];
                                                    object tempRefParam21 = iteration_row_3["fechamax"];
                                                    object tempRefParam22 = rrConsultas.Tables[0].Rows[0]["FULTFACT"];
                                                    stSqConsultas = "insert into dmovfact (gidenpac,ITPACFAC,ganoregi,gnumregi,itiposer,itiporig,ganoorig,gnumorig,ncantida,gconcfac,gsocieda,nafiliac,ginspecc,gcodeven,ffinicio,ffechfin,fsistema,gserresp,gsersoli,gserreal,fperfact) values ( " +
                                                                    "'" + stCodPacE + "','I'," + Convert.ToString(iteration_row_3["GANOADME"]).Trim() + "," + Convert.ToString(iteration_row_3["GNUMADME"]).Trim() + ",'H','H'," + Convert.ToString(iteration_row_3["GANOADME"]).Trim() + "," + Convert.ToString(iteration_row_3["GNUMADME"]).Trim() + "," + Serrores.ConvertirDecimales(ref tempRefParam18, ref tempRefParam19, 2) + ",'" + Convert.ToString(iteration_row_3["gfarmaco"]) + "'," +
                                                                    Convert.ToString(iteration_row_3["gsocieda"]) + "," + ((Convert.IsDBNull(iteration_row_3["nafiliac"])) ? "Null" : "'" + Convert.ToString(iteration_row_3["nafiliac"]).Trim() + "'") + "," + ((Convert.IsDBNull(iteration_row_3["ginspecc"])) ? "Null" : "'" + Convert.ToString(iteration_row_3["ginspecc"]).Trim() + "'") + "," +
                                                                    "'FA'," + Serrores.FormatFechaHMS(tempRefParam20) + "," + Serrores.FormatFechaHMS(tempRefParam21) + ",getdate()," + fnBuscaSerResp(Convert.ToString(iteration_row_3["fechamin"]), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row_3["GANOADME"]))), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row_3["GNUMADME"])))).ToString() + ", " +
                                                                    fnBuscaSerResp(Convert.ToString(iteration_row_3["fechamin"]), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row_3["GANOADME"]))), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row_3["GNUMADME"])))).ToString() + ", " + iFarmacia.ToString() + "," + Serrores.FormatFechaHMS(tempRefParam22) + ")";
                                                    SqlCommand tempCommand_3 = new SqlCommand(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                                    tempCommand_3.ExecuteNonQuery();

                                                    // pongo marca de pasado a facturaci�n de todos los acumulados
                                                    object tempRefParam23 = rrConsultas.Tables[0].Rows[0]["FULTFACT"];
                                                    object tempRefParam24 = iteration_row_3["fechamin"];
                                                    object tempRefParam25 = rrConsultas.Tables[0].Rows[0]["FULTFACT"];
                                                    stSqConsultas = "update ETOMASFA set fpasfact = " + Serrores.FormatFechaHMS(tempRefParam23) + " where " + " ganoregi = " + Convert.ToString(iteration_row_3["GANOADME"]).Trim() + " and gnumregi=" + Convert.ToString(iteration_row_3["GNUMADME"]).Trim() + " and  " + " fpasfact is null and " + " cdosimpu is not null and " + " gfarmaco = '" + Convert.ToString(iteration_row_3["gfarmaco"]) + "' and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam24) + " and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam25) + " and " + " itiposer='H' ";
                                                    SqlCommand tempCommand_4 = new SqlCommand(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                                    tempCommand_4.ExecuteNonQuery();


                                                } //fin de hay frascos

                                            }
                                            //rrFact.Close
                                        }
                                        //Si no pasa y la eliminada s� los pasaba, .....
                                        if (!bNSocFacFar && bASocFacFar)
                                        {
                                            //.....hay que eliminar los f�rmacos que haya en DMOVFACT cuya fecha de inicio sea mayor o igual que la fecha de inicio del periodo de la sociedad borrada
                                            object tempRefParam26 = stfecha;
                                            SqlCommand tempCommand_5 = new SqlCommand(" DELETE DMOVFACT where itiporig <> 'Q' and  gcodeven='FA' and itiposer='H' and ganoregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]) +
                                                                       " and gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]) + " and ffinicio>=" + Serrores.FormatFechaHMS(tempRefParam26) + " and gsocieda=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gsocieda"]), Conexion_Base_datos.RcAdmision);
                                            tempCommand_5.ExecuteNonQuery();
                                            stfecha = Convert.ToString(tempRefParam26);
                                            //..... y poner a nulo el campo fpasfact de ETOMASFA
                                            object tempRefParam27 = stfecha;
                                            stSqConsultas = "update ETOMASFA set fpasfact = NULL where " + " ganoregi = " + Convert.ToString(rrEntidad.Tables[0].Rows[0]["ganoregi"]).Trim() + " and gnumregi=" + Convert.ToString(rrEntidad.Tables[0].Rows[0]["gnumregi"]).Trim() + " and  " + " fpasfact is NOT null and " + " cdosimpu is not null and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam27) + " and " + " itiposer='H' ";
                                            stfecha = Convert.ToString(tempRefParam27);

                                            SqlCommand tempCommand_6 = new SqlCommand(stSqConsultas, Conexion_Base_datos.RcAdmision);
                                            tempCommand_6.ExecuteNonQuery();
                                        }
                                    }
                                    rrConsultas.Close();
                                }
                                scope.Complete();
                                rrEntidad.Close();

                            } //fin no hay filas en DMOVFACT
                            rrFact.Close();
                        }//Fin Transacci�n
                   }
                   else
                   {
                       //no se encuentra el registro en AMOVIFIN
                       rrEntidad.Close();
                   }
               } //fin trato ultimo registro

               this.Cursor = Cursors.Default;
               Vst_ultimo = true;
               proRellena_Grid();
               cbAceptar.Enabled = false;
               tbEntidadA.Text = "";
               tbNafilia.Text = "";
           }
           catch (SqlException ex)
           {
               //al usar el TransactionScope no se necesita el rollback
               //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
               this.Cursor = Cursors.Default;
               Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
           }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

       private void CbNuevo_Click(Object eventSender, EventArgs eventArgs)
       {
           //llamo a la dll de filiaci�n
           try
           {
               //******
               fNuevo = true;
               //sdcFecha.Text = Format$(Now, "DD/MM/YYYY")
               //tbHoraCambio.Text = Format$(Now, "HH") & ":" & Format$(Now, "NN")
               vstSegundos = DateTime.Now.ToString("ss");
               tbEntidadA.Text = "";
               tbNafilia.Text = "";
               this.Cursor = Cursors.WaitCursor;
               /*INDRA jproche_TODO_X_4
               vncFiliacion = new filiacionDLL.Filiacion();
               //UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
               vncFiliacion.Load(vncFiliacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, stCodPacE);
               vncFiliacion = null;
               */
            //Me.Show
            this.Cursor = Cursors.Default;
			}
			catch
			{	
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, Conexion_Base_datos.RcAdmision, Label19.Text));
			}
		}

		private void Cambio_entidad_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;


				if (!bVuelvedeFiliacion)
				{
					// relleno el grid de episodios
					Vst_ultimo = true;
					proRellena_Grid();
					vstActPen = "";
					vstInspec = "";
					vstActPenOLD = "";
					vstInspecOLD = "";
				}
				bVuelvedeFiliacion = false;
			}
		}

		private void Cambio_entidad_Load(Object eventSender, EventArgs eventArgs)
		{
			//Me.Height = 3735
			//Me.Width = 6390
			cbAceptar.Enabled = false;
			sdcFecha.MaxDate = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label28.Text = LitPersona;
			fNuevo = false;
		}


		private void Cambio_entidad_Closed(Object eventSender, EventArgs eventArgs)
		{
			menu_ingreso.DefInstance.proQuitar_entidad(stCodPacE);
		}

		public void proRellena_Grid()
		{
			int longi = 500;
			int longi2 = 300;
			SprEntidades.MaxCols = 6;
			
			SprEntidades.Row = 0;
			SprEntidades.Col = 1;
			SprEntidades.Text = "Fecha del cambio";
			SprEntidades.SetColWidth(1, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi - 150);
			SprEntidades.Col = 2;
			SprEntidades.Text = "Entidad";
			SprEntidades.SetColWidth(2, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi + 3000);
			SprEntidades.Col = 3;
			SprEntidades.Text = "N� afiliaci�n";
			SprEntidades.SetColWidth(3, CreateGraphics().MeasureString(SprEntidades.Text, Font).Width * 15 + longi + 800);
			SprEntidades.Col = 4;
			SprEntidades.SetColHidden(SprEntidades.Col, true);
			SprEntidades.Col = 5;
			SprEntidades.SetColHidden(SprEntidades.Col, true);
			SprEntidades.Col = 6;
			SprEntidades.SetColHidden(SprEntidades.Col, true);
			string sqlEnti = "SELECT FMOVIMIE,GNUMADME,GANOADME,DSOCIEDA,AMOVIFIN.NAFILIAC,AMOVIFIN.GSOCIEDA, " + "GINSPECC,IACTPENS " + " FROM AEPISADM, AMOVIFIN,DSOCIEDA " + " WHERE " + " AEPISADM.GNUMADME = AMOVIFIN.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIFIN.GANOREGI AND " + " AEPISADM.GIDENPAC = '" + stCodPacE + "' and" + " FALTPLAN is null AND DSOCIEDA.GSOCIEDA=AMOVIFIN.GSOCIEDA" + " order by fmovimie";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEnti, Conexion_Base_datos.RcAdmision);
			DataSet rrEnti = new DataSet();
			tempAdapter.Fill(rrEnti);
			if (rrEnti.Tables[0].Rows.Count != 0)
			{
				SprEntidades.MaxRows = rrEnti.Tables[0].Rows.Count;
				longi = 1;
				foreach (DataRow iteration_row in rrEnti.Tables[0].Rows)
				{
					SprEntidades.Row = longi;
					SprEntidades.Col = 1;
					SprEntidades.Text = Convert.ToString(iteration_row["FMOVIMIE"]);
					SprEntidades.Col = 2;
					SprEntidades.Text = Convert.ToString(iteration_row["DSOCIEDA"]);
					SprEntidades.Col = 3;
					if (Convert.IsDBNull(iteration_row["NAFILIAC"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["NAFILIAC"]);
					}
					SprEntidades.Col = 4;
					SprEntidades.Text = Convert.ToString(iteration_row["gsocieda"]);
					SprEntidades.Col = 5;
					if (Convert.IsDBNull(iteration_row["gINSPECC"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["gINSPECC"]);
					}
					SprEntidades.Col = 6;
					if (Convert.IsDBNull(iteration_row["IACTPENS"]))
					{
						SprEntidades.Text = "";
					}
					else
					{
						SprEntidades.Text = Convert.ToString(iteration_row["IACTPENS"]);
					}
					longi++;
					Vst_Fechacambio = Convert.ToString(iteration_row["FMOVIMIE"]);
					Vst_ultimo = true;
					sdcFecha.Enabled = true;
					tbHoraCambio.Enabled = true;
					sdcFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
					tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
					Vstfecha_inicial = sdcFecha.Text + " " + tbHoraCambio.Text;
					vstSegundos = DateTime.Now.ToString("ss");
					SprEntidades.Col = 4;
					vCod1Soc = SprEntidades.Text;
					SprEntidades.Col = 3;
					vstNumfil = SprEntidades.Text;
				}
				SprEntidades.Col = 1;
                SprEntidades.Rows[rrEnti.Tables[0].Rows.Count - 1].IsCurrent = true;
				//    tbEntidadA.Caption = ""
				//    tbNafilia.Caption = ""
			}
			rrEnti.Close();
		}

		public string fnMaximo(string stCodPac1)
		{ //Date

			string result = String.Empty;
			string stSqCambio = String.Empty;
			DataSet RrMaximo = null;
			//***
			try
			{
				//*********
				stSqCambio = "SELECT FLLEGADA AS MAXIMO " + " FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodPac1 + "' and " + " AEPISADM.FALTPLAN IS NULL";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, Conexion_Base_datos.RcAdmision);
				RrMaximo = new DataSet();
				tempAdapter.Fill(RrMaximo);
				if (!Convert.IsDBNull(RrMaximo.Tables[0].Rows[0]["maximo"]))
				{
					sdcFecha.MinDate = DateTime.Parse(Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["maximo"]).ToString("dd/MM/yyyy"));
					Vstfecha_ingreso = Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["maximo"]).ToString("dd/MM/yyyy HH") + ":" + 
					                   Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["maximo"]).ToString("mm:ss");
				}
				else
				{
					sdcFecha.MinDate = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
					Vstfecha_ingreso = Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["maximo"]).ToString("dd/MM/yyyy HH") + ":" + 
					                   Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["maximo"]).ToString("mm:ss");
				}

				RrMaximo.Close();
				stSqCambio = "SELECT MAX(FMOVIMIE) AS MAXIMO " + " FROM AMOVIFIN,AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodPac1 + "' and " + " GANOADME=GANOREGI AND GNUMADME=GNUMREGI AND " + " AEPISADM.FALTPLAN IS NULL";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqCambio, Conexion_Base_datos.RcAdmision);
				RrMaximo = new DataSet();
				tempAdapter_2.Fill(RrMaximo);
				
				if (!Convert.IsDBNull(RrMaximo.Tables[0].Rows[0]["maximo"]))
				{
					result = Convert.ToString(RrMaximo.Tables[0].Rows[0]["maximo"]);
					dfMax = Convert.ToString(RrMaximo.Tables[0].Rows[0]["maximo"]);
				}
				return result;
			}
			catch (SqlException ex)
			{
				//**********
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
				return result;
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
                return result;
            }
        }


		private void sdcFecha_Leave(Object eventSender, EventArgs eventArgs)
		{
			int sthora = 0;
			int stminutos = 0;
			string stfecha = String.Empty;
			//compruebo que es formato de hora valido
			//******************
			try
			{
				//***************
				int tiI = 0;
				if (Vst_ultimo && SprEntidades.MaxRows == 1 && !fNuevo)
				{
					cbAceptar.Enabled = false;
				}
				else
				{
					if (tbHoraCambio.Text != "  :  ")
					{
						tiI = (tbHoraCambio.Text.IndexOf(':') + 1);
						if (Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(0, Math.Min(tiI - 1, tbHoraCambio.Text.Length)))) > 24 || Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(tiI))) > 60)
						{
							Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, Conexion_Base_datos.RcAdmision, Label19.Text));
							tbHoraCambio.Focus();
							cbAceptar.Enabled = false;
							return;
						}
						sthora = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("HH")));
						stminutos = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("mm")));
						stfecha = DateTime.Now.ToString("dd/MM/yyyy");
						if (stfecha == sdcFecha.Text)
						{
							if (sthora == sthorasystem)
							{
								if (stminutos > stminutossystem)
								{
									//ERROR MINUTOS MAYOR QUE LOS DEL SYSTEMA
									Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La hora no", "mayor", "la del sistema"));
									cbAceptar.Enabled = false;
									return;
								}
							}
							else
							{
								if (sthora > sthorasystem)
								{
									//ERROR HORA MAYOR QUE LA DEL SYSTEMA
									Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La hora no", "mayor", "la del sistema"));
									cbAceptar.Enabled = false;
									return;
								}
							}
						}
						cbAceptar.Enabled = true;
						tbHoraCambio.Text = DateTime.Parse(tbHoraCambio.Text).ToString("HH") + ":" + DateTime.Parse(tbHoraCambio.Text).ToString("mm");
					}
					vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));
				}
			}
			//**********
			catch (InvalidCastException e)
			{
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1150, Conexion_Base_datos.RcAdmision, Label19.Text));
				tbHoraCambio.Focus();
				return;
			}
		}
        

		private void SprEntidades_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
            int Col = eventArgs.ColumnIndex;
            int Row = eventArgs.RowIndex + 1;
            int actual_fila = Row;
			int actual_colu = Col;
			SprEntidades.Row = Row;
			SprEntidades.Col = 2;
			tbEntidadde.Text = SprEntidades.Text;
			if (Row != 0)
			{
				if (Row == SprEntidades.MaxRows)
				{
					//si se trata del ultimo registro se puede eliminar siempre que haya mas de uno
					Vst_ultimo = true;
					if (Row == 1)
					{
						cbEliminar.Enabled = false;
						sdcFecha.Enabled = false;
						tbHoraCambio.Enabled = false;
					}
					else
					{
						cbEliminar.Enabled = true;
						sdcFecha.Enabled = true;
						tbHoraCambio.Enabled = true;
					}
					SprEntidades.Col = 1;
					sdcFecha.Text = DateTime.Parse(SprEntidades.Text).ToString("dd/MM/yyyy");
					tbHoraCambio.Text = DateTime.Parse(SprEntidades.Text).ToString("HH:mm");
					vstSegundos = DateTime.Parse(SprEntidades.Text).ToString("ss");
					SprEntidades.Col = 4;
					vCod1Soc = SprEntidades.Text;
					SprEntidades.Col = 3;
					vstNumfil = SprEntidades.Text;
					SprEntidades.Col = 6;
					vstActPenOLD = SprEntidades.Text;
					SprEntidades.Col = 5;
					vstInspecOLD = SprEntidades.Text;
				}
				else
				{
					//Cuando se selecciona un registro que no es el ultimo
					Vst_ultimo = false;
					cbEliminar.Enabled = false;
					SprEntidades.Col = 1;
					sdcFecha.Text = DateTime.Parse(SprEntidades.Text).ToString("dd/MM/yyyy");
					tbHoraCambio.Text = DateTime.Parse(SprEntidades.Text).ToString("HH:mm");
					sdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Vst_Fechacambio = DateTime.Parse(SprEntidades.Text).ToString("dd/MM/yyyy HH:mm:ss");
					vstSegundos = DateTime.Parse(SprEntidades.Text).ToString("ss");
					SprEntidades.Col = 4;
					vCod1Soc = SprEntidades.Text;
					SprEntidades.Col = 3;
					vstNumfil = SprEntidades.Text;
					SprEntidades.Col = 6;
					vstActPenOLD = SprEntidades.Text;
					SprEntidades.Col = 5;
					vstInspecOLD = SprEntidades.Text;

				}
				SprEntidades.Col = actual_colu;
			}
		}

		private void tbHoraCambio_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			sthorasystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("HH")));
			stminutossystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("mm")));
		}

		private void tbHoraCambio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbHoraCambio.SelectionStart = 0;
			tbHoraCambio.SelectionLength = Strings.Len(tbHoraCambio.Text);
		}

		private void tbHoraCambio_Leave(Object eventSender, EventArgs eventArgs)
		{
			int sthora = 0;
			int stminutos = 0;
			string stfecha = String.Empty;
			//compruebo que es formato de hora valido
			//******************
			try
			{
				//***************
				int tiI = 0;
				if (Vst_ultimo && SprEntidades.MaxRows == 1 && !fNuevo)
				{
					cbAceptar.Enabled = false;
				}
				else
				{
					if (tbHoraCambio.Text != "  :  ")
					{
						tiI = (tbHoraCambio.Text.IndexOf(':') + 1);
						if (Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(0, Math.Min(tiI - 1, tbHoraCambio.Text.Length)))) > 24 || Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(tiI))) > 60)
						{
							Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, Conexion_Base_datos.RcAdmision, Label19.Text));
							tbHoraCambio.Focus();
							cbAceptar.Enabled = false;
							return;
						}
						sthora = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("HH")));
						stminutos = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("mm")));
						stfecha = DateTime.Now.ToString("dd/MM/yyyy");
						if (stfecha == sdcFecha.Text)
						{
							if (sthora == sthorasystem)
							{
								if (stminutos > stminutossystem)
								{
									//ERROR MINUTOS MAYOR QUE LOS DEL SYSTEMA
									Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La hora no", "mayor", "la del sistema"));
									cbAceptar.Enabled = false;
									return;
								}
							}
							else
							{
								if (sthora > sthorasystem)
								{
									//ERROR HORA MAYOR QUE LA DEL SYSTEMA
									Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, Conexion_Base_datos.RcAdmision, "La hora no", "mayor", "la del sistema"));
									cbAceptar.Enabled = false;
									return;
								}
							}
						}
						cbAceptar.Enabled = true;
						tbHoraCambio.Text = DateTime.Parse(tbHoraCambio.Text).ToString("HH") + ":" + DateTime.Parse(tbHoraCambio.Text).ToString("mm");
					}
					vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));
				}
			}
			//**********
			catch (InvalidCastException e)
			{
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1150, Conexion_Base_datos.RcAdmision, Label19.Text));
				tbHoraCambio.Focus();
				return;
			}
		}
		public void proSociedad(int CodSOC, RadTextBox lbSoc)
		{
			//*****
			try
			{
				//***
				//********Busco la sociedad
				string SQSoci = String.Empty;
				DataSet RrSoci = null;

				SQSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter.Fill(RrSoci);

				if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == CodSOC)
				{ //SS
					lbSoc.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
					RrSoci.Close();
					return;
				}
				//*************************************
				SQSoci = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_2.Fill(RrSoci);
				if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == CodSOC)
				{ // Privado
					lbSoc.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
					RrSoci.Close();
					return;
				}
				//***************************Sociedad
				SQSoci = "SELECT DSOCIEDA,IREGALOJ FROM DSOCIEDA WHERE GSOCIEDA = " + CodSOC.ToString();
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQSoci, Conexion_Base_datos.RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_3.Fill(RrSoci);
				lbSoc.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["DSOCIEDA"]).Trim();
				RrSoci.Close();
				//***********************************
			}
			catch (SqlException ex)
			{
				//*********
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		public bool ComprobarDMOVFACT(DataSet RrCursor, string fapunte, string fechafactura)
		{
			//funcion que comprueba si hay registros en DMOVFACT con fecha superior a la que le pasamos
			//si hay movimientos devuelve un true
            bool result = false;
			string stSqlFact = "select * from dmovfact where" + " itiposer = 'H' and " + " ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and " + " gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and " + " gsocieda=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gsocieda"]) + " and " + " nafiliac='" + Convert.ToString(RrCursor.Tables[0].Rows[0]["nafiliac"]) + "' ";

			if (fNuevo)
			{
				stSqlFact = stSqlFact + " and ffechfin >=" + Serrores.FormatFechaHMS(fapunte) + "";
			}
			else
			{
				stSqlFact = stSqlFact + " and ffinicio >=" + Serrores.FormatFechaHMS(RrCursor.Tables[0].Rows[0]["fmovimie"]);
			}

			//oscar 08/05/03
			stSqlFact = stSqlFact + " and ffactura is not null ";
			//-----

			if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
			{
				string tempRefParam3 = "DMOVFACT";
				stSqlFact = stSqlFact + " UNION ALL " + Serrores.proReplace(ref stSqlFact, tempRefParam3, "DMOVFACH");
			}

			stSqlFact = stSqlFact + " order by fsistema";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
			DataSet rrFact = new DataSet();
			tempAdapter.Fill(rrFact);
			if (rrFact.Tables[0].Rows.Count != 0)
			{
				result = true;
				fechafactura = Convert.ToDateTime(rrFact.Tables[0].Rows[0]["FFECHFIN"]).ToString("dd/MM/yyyy HH:mm:ss");
			}
			else
			{
				result = false;
			}
			rrFact.Close();
			return result;
		}

		public bool ComprobarFacturaDMOVFACT(DataSet RrCursor)
		{
			//funcion que comprueba si hay registros en DMOVFACT y si estan facturados
			//si hay movimientos devuelve un true

			object tempRefParam = RrCursor.Tables[0].Rows[0]["fmovimie"];
			string stSqlFact = "select * from dmovfact where" + " itiposer = 'H' and " + " ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and " + " gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and " + " gsocieda=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gsocieda"]) + " and " + " nafiliac='" + Convert.ToString(RrCursor.Tables[0].Rows[0]["nafiliac"]) + "' and " + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";

			
			if (!Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["ffinmovi"]))
			{
				object tempRefParam2 = rrEntidad.Tables[0].Rows[0]["FFINMOVI"];
				stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + "";
			}

			if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
			{
				string tempRefParam3 = "DMOVFACT";
				stSqlFact = stSqlFact + " UNION ALL " + Serrores.proReplace(ref stSqlFact, tempRefParam3, "DMOVFACH");
			}

			stSqlFact = stSqlFact + " order by fsistema";

			bool fencontrado = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqlFact, Conexion_Base_datos.RcAdmision);
			DataSet rrFact = new DataSet();
			tempAdapter.Fill(rrFact);
			//si hay filas en DMOVFACT
			if (rrFact.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rrFact.Tables[0].Rows)
				{
					if (!Convert.IsDBNull(iteration_row["ffactura"]))
					{
						fencontrado = true;
					}
				}
			}
			rrFact.Close();

			return fencontrado;
		}

		private void ActualizarDMOVFACT(DataSet RrCursor)
		{

			string stSqlFact = "update dmovfact set gsocieda = " + vCodSoc + ",";
			stSqlFact = stSqlFact + "nafiliac = '" + tbNafilia.Text.Trim() + "',";
			if (vstInspec != "")
			{
				stSqlFact = stSqlFact + "ginspecc = '" + vstInspec + "'";
			}
			else
			{
				stSqlFact = stSqlFact + "ginspecc = null";
			}
			// para modificar movimientos facturados se hace por I-FMS  21/7/99
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and ";
			stSqlFact = stSqlFact + " gsocieda=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gsocieda"]) + " and ";
			stSqlFact = stSqlFact + " nafiliac='" + Convert.ToString(RrCursor.Tables[0].Rows[0]["nafiliac"]) + "' and ";
			object tempRefParam = RrCursor.Tables[0].Rows[0]["fmovimie"];
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";
            
			if (!Convert.IsDBNull(rrEntidad.Tables[0].Rows[0]["ffinmovi"]))
			{
				object tempRefParam2 = rrEntidad.Tables[0].Rows[0]["FFINMOVI"];
				stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + "";
			}

			SqlCommand tempCommand = new SqlCommand(stSqlFact, Conexion_Base_datos.RcAdmision);
			tempCommand.ExecuteNonQuery();

		}

		//oscar 14/03/03
		private bool ACTUALIZAMOVIMIENTOS_DMOVFACT(int PAnoRegi, int PNumregi)
		{
            bool result = false;
            string stSqlFarm = String.Empty;
            DataSet RrEditarA�adir = null;
            double cantidad = 0;
            string stFarmaco = String.Empty;
            double nequival = 0;
            int i = 0;
            bool bSocFacFar = false;


            //En el caso de que existan movimientos en DMOVFACT con el campo FFECHFIN mayor que la fecha del apunte,
            //se actuara de diferente forma segun el campo GCODEVEN
            object tempRefParam = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
            object tempRefParam2 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * FROM DMOVFACT WHERE" + " itiposer = 'H' and " + " ganoregi=" + PAnoRegi.ToString() + " and " + " gnumregi=" + PNumregi.ToString() + " and " + " (ffechfin >" + Serrores.FormatFechaHMS(tempRefParam) + " or ffinicio >" + Serrores.FormatFechaHMS(tempRefParam2) + ") ");

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), Conexion_Base_datos.RcAdmision);
            DataSet RR = new DataSet();
            tempAdapter.Fill(RR);
            if (RR.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow iteration_row in RR.Tables[0].Rows)
                {

                    switch (Convert.ToString(iteration_row["GCODEVEN"]).Trim().ToUpper())
                    {
                        case "AL":
                        case "AN":
                        case "DQ":
                        case "PR":
                        case "AY":
                        case "HM":
                        case "IN":

                            //En estos casos se comprobara que la fecha de inicio del movimiento es superior a la fecha de cambio. 
                            //En este caso,se modificaran los campos GSOCIEDA, NAFILIAC y GINSPECC con los valores correspondientes 
                            //a la nueva sociedad. 

                            System.DateTime TempDate = DateTime.FromOADate(0);
                            if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:mm:ss")) > DateTime.Parse((DateTime.TryParse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
                            {
                                sql = new StringBuilder("UPDATE DMOVFACT SET " +
                                      "GSOCIEDA = " + vCodSoc + "," +
                                      "NAFILIAC = '" + tbNafilia.Text.Trim() + "',");
                                if (vstInspec != "")
                                {
                                    sql.Append("GINSPECC = '" + vstInspec + "'");
                                }
                                else
                                {
                                    sql.Append("GINSPECC = NULL ");
                                }
                                sql.Append(
                                           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                SqlCommand tempCommand = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                tempCommand.ExecuteNonQuery();
                            }

                            break;
                        case "AU":
                        case "ES":

                            //En el caso que la fecha de inicio sea menor que la fecha del apunte, se modificaran las lineas correspondentes de 
                            //DMOVFACT poniendo en FFECHFIN la fecha del apunte. Por cada linea modificada en DMOVFACT, se grabara una linea nueva 
                            //con los campos GSOCIEDA, NAFILIAC y GINSPECC correspondientes a la nueva sociedad, poniendo como FFINICIO la fecha 
                            //del apunte y como FFECHFIN la fecha de fin de movimiento que tuviera antes de ser modificada. 

                            System.DateTime TempDate2 = DateTime.FromOADate(0);
                            if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse((DateTime.TryParse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
                            {

                                sql = new StringBuilder("SELECT * FROM DMOVFACT WHERE 2=1");
                                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                RrEditarA�adir = new DataSet();
                                tempAdapter_2.Fill(RrEditarA�adir);
                                RrEditarA�adir.AddNew();

                                i = 0;
                                
                                while (RrEditarA�adir.Tables[0].Columns.Count != i)
                                {
                                    //Debug.Print RrEditarA�adir.rdoColumns(i).Name
                                    switch (RrEditarA�adir.Tables[0].Columns[i].ColumnName.ToUpper())
                                    {
                                        case "FILA_ID":
                                            break;
                                        case "GSOCIEDA":
                                            RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = vCodSoc;
                                            break;
                                        case "NAFILIAC":
                                            RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = tbNafilia.Text.Trim();
                                            break;
                                        case "FFINICIO":
                                            System.DateTime TempDate3 = DateTime.FromOADate(0);
                                            RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = DateTime.Parse((DateTime.TryParse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm:ss") : sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos);
                                            break;
                                        case "GINSPECC":
                                            if (vstInspec != "")
                                            {
                                                RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = vstInspec;
                                            }
                                            else
                                            {   
                                                RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
                                            }
                                            break;
                                        default:
                                            if (!Convert.IsDBNull(iteration_row[RrEditarA�adir.Tables[0].Columns[i].ColumnName]))
                                            {
                                                RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = iteration_row[RrEditarA�adir.Tables[0].Columns[i].ColumnName];
                                            }
                                            else
                                            {
                                                RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
                                            }
                                            break;
                                    }
                                    i++;
                                };

                                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                tempAdapter_2.Update(RrEditarA�adir, RrEditarA�adir.Tables[0].TableName);

                                object tempRefParam3 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
                                sql = new StringBuilder("UPDATE DMOVFACT SET " +
                                      "FFECHFIN=" + Serrores.FormatFechaHMS(tempRefParam3) +
                                      " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                SqlCommand tempCommand_2 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                tempCommand_2.ExecuteNonQuery();

                                RrEditarA�adir.Close();
                            }
                            else
                            {

                                //''''''20/05/03''''''''''
                                sql = new StringBuilder("UPDATE DMOVFACT SET " +
                                      "GSOCIEDA = " + vCodSoc + "," +
                                      "NAFILIAC = '" + tbNafilia.Text.Trim() + "',");
                                if (vstInspec != "")
                                {
                                    sql.Append("GINSPECC = '" + vstInspec + "'");
                                }
                                else
                                {
                                    sql.Append("GINSPECC = NULL ");
                                }
                                sql.Append(
                                           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                SqlCommand tempCommand_3 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                tempCommand_3.ExecuteNonQuery();
                                //''''''''''''''''

                            }

                            break;
                        case "FA":

                            //En el caso que la fecha de inicio sea menor que la fecha del apunte, se realizara el calculo 
                            //de farmacos administrados en el periodo. Una vez calculado, se actualizara el campo NCANTIDA de 
                            //DMOVFACT y se generara otra linea para el periodo desde fecha de cambio hasta fecha fin con los 
                            //mismos valores que el anterior, pero con los nuevos valores para GSOCIEDA, NAFILIAC y GINSPECC 
                            //correspondientes a las tomas de dicho periodo. 

                            //Comporbamos si la nueva sociedad factura los f�rmacos. Se tendr� en cuenta esta validaci�n para no insertar nuevas l�neas cuando 
                            //la sociedad no los facture. Se supone que si hay l�neas de f�rmaco es porque la sociedad antigua factura f�rmacos. 
                            bSocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Double.Parse(vCodSoc)), vstInspec);

                            System.DateTime TempDate4 = DateTime.FromOADate(0);
                            if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse((DateTime.TryParse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:mm:ss") : sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
                            {

                                object tempRefParam4 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
                                object tempRefParam5 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
                                object tempRefParam6 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
                                sql = new StringBuilder("SELECT GFARMACO,SUM(CDOSISTO) CANTIDAD " +
                                      "FROM ETOMASFA E INNER JOIN DMOVFACT D ON " +
                                      "E.ITIPOSER = D.ITIPOSER AND " +
                                      "E.GANOREGI = D.GANOREGI AND " +
                                      "E.GNUMREGI = D.GNUMREGI AND " +
                                      "E.FPASFACT = D.FPERFACT AND " +
                                      "E.GFARMACO = D.GCONCFAC " +
                                      "WHERE GCODEVEN='FA' AND " +
                                      "D.FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]) + " AND " +
                                      "FFINICIO<=" + Serrores.FormatFechaHMS(tempRefParam4) + " AND " +
                                      "FFECHFIN>=" + Serrores.FormatFechaHMS(tempRefParam5) + " and " +
                                      "FTOMASFA>= FFINICIO and " +
                                      "FTOMASFA<" + Serrores.FormatFechaHMS(tempRefParam6) + " " +
                                      "GROUP BY GFARMACO ORDER BY GFARMACO");
                                stSqlFarm = sql.ToString();
                                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                RrEditarA�adir = new DataSet();
                                tempAdapter_4.Fill(RrEditarA�adir);
                                if (RrEditarA�adir.Tables[0].Rows.Count != 0)
                                {
                                    stFarmaco = Convert.ToString(RrEditarA�adir.Tables[0].Rows[0]["GFARMACO"]);
                                    string tempRefParam7 = Convert.ToString(RrEditarA�adir.Tables[0].Rows[0]["CANTIDAD"]);
                                    cantidad = (Convert.IsDBNull(RrEditarA�adir.Tables[0].Rows[0]["CANTIDAD"])) ? 0 : NumeroConvertido(tempRefParam7);
                                    //RrEditarA�adir.Close

                                    sql = new StringBuilder("SELECT nequival FROM DCODFARMVA WHERE gfarmaco='" + stFarmaco + "'");
                                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                    RrEditarA�adir = new DataSet();
                                    tempAdapter_5.Fill(RrEditarA�adir);
                                    if (RrEditarA�adir.Tables[0].Rows.Count != 0)
                                    {
                                        string tempRefParam8 = Convert.ToString(RrEditarA�adir.Tables[0].Rows[0][0]);
                                        nequival = (Convert.IsDBNull(RrEditarA�adir.Tables[0].Rows[0][0])) ? 0 : NumeroConvertido(tempRefParam8);
                                    }
                                    //RrEditarA�adir.Close

                                    if (nequival == 0)
                                    {
                                        RadMessageBox.Show("ERROR al obtener la Unidad de equivalencia de alg�n(os) de los Farmaco(s) implicados.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                                        return result;
                                    }
                                    //Si la nueva sociedad factura f�rmacos, los que hab�a en DMOVFACT hay que facturarlos a la nueva sociedad en el nuevo periodo
                                    if (bSocFacFar)
                                    {
                                        sql = new StringBuilder("SELECT * FROM DMOVFACT WHERE 2=1");
                                        SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                        RrEditarA�adir = new DataSet();
                                        tempAdapter_6.Fill(RrEditarA�adir);
                                        RrEditarA�adir.AddNew();

                                        i = 0;
                                        
                                        while (RrEditarA�adir.Tables[0].Columns.Count != i)
                                        {
                                            //Debug.Print RrEditarA�adir.rdoColumns(i).Name
                                            switch (RrEditarA�adir.Tables[0].Columns[i].ColumnName.ToUpper())
                                            {
                                                case "FILA_ID":
                                                    break;
                                                case "GSOCIEDA":
                                                    RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = vCodSoc;
                                                    break;
                                                case "NAFILIAC":
                                                    RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = tbNafilia.Text.Trim();
                                                    break;
                                                case "FFINICIO":
                                                    System.DateTime TempDate5 = DateTime.FromOADate(0);
                                                    RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = DateTime.Parse((DateTime.TryParse(sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy HH:mm:ss") : sdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
                                                    break;
                                                case "GINSPECC":
                                                    if (vstInspec != "")
                                                    {
                                                        RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = vstInspec;
                                                    }
                                                    else
                                                    {
                                                        RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
                                                    }
                                                    break;
                                                case "NCANTIDA":
                                                    string tempRefParam10 = Convert.ToString(iteration_row[RrEditarA�adir.Tables[0].Columns[i].ColumnName]);
                                                    string tempRefParam9 = (NumeroConvertido(tempRefParam10) - (cantidad / nequival)).ToString();
                                                    string tempRefParam11 = Serrores.ObtenerSeparadorDecimalBD(Conexion_Base_datos.RcAdmision);
                                                    RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = Serrores.ConvertirDecimales(ref tempRefParam9, ref tempRefParam11, 2);
                                                    break;
                                                default:
                                                    if (!Convert.IsDBNull(iteration_row[RrEditarA�adir.Tables[0].Columns[i].ColumnName]))
                                                    {   
                                                        RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = iteration_row[RrEditarA�adir.Tables[0].Columns[i].ColumnName];
                                                    }
                                                    else
                                                    {   
                                                        RrEditarA�adir.Tables[0].Rows[0][RrEditarA�adir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
                                                    }
                                                    break;
                                            }
                                            i++;
                                        };
                                        SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                                        tempAdapter_6.Update(RrEditarA�adir, RrEditarA�adir.Tables[0].TableName);
                                    }
                                    else
                                    {
                                        //Actualizamos a null los campos fpasfact de etomasfa que no insertamos.
                                        sql = new StringBuilder("update etomasfa set fpasfact = null " + stSqlFarm.Substring(stSqlFarm.IndexOf("FROM ")).Substring(0, Math.Min(stSqlFarm.Substring(stSqlFarm.IndexOf("FROM ")).IndexOf("GROUP BY "), stSqlFarm.Substring(stSqlFarm.IndexOf("FROM ")).Length)));
                                        SqlCommand tempCommand_4 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                        tempCommand_4.ExecuteNonQuery();
                                    }

                                    //El las l�neas que quedan fuera del nuevo periodo, se modifican la cantidad y la fecha de fin.
                                    string tempRefParam12 = (cantidad / nequival).ToString();
                                    string tempRefParam13 = Serrores.ObtenerSeparadorDecimalBD(Conexion_Base_datos.RcAdmision);
                                    object tempRefParam14 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
                                    sql = new StringBuilder("UPDATE DMOVFACT SET " +
                                          "NCANTIDA=" + Serrores.ConvertirDecimales(ref tempRefParam12, ref tempRefParam13, 2) +
                                          ",FFECHFIN=" + Serrores.FormatFechaHMS(tempRefParam14) +
                                          " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                    SqlCommand tempCommand_5 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                    tempCommand_5.ExecuteNonQuery();
                                }
                                else
                                {
                                    //se actualiza la sociedad cuando hay tomas en el periodo entre la fecha de inicio del movimiento y la fecha de fin del movimiento
                                    //pero no hay tomas entre FFINICIO y Fecha de Cambio
                                    object tempRefParam15 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
                                    sql = new StringBuilder("UPDATE DMOVFACT SET " +
                                          "GSOCIEDA = " + vCodSoc + "," +
                                          "NAFILIAC = '" + tbNafilia.Text.Trim() + "'," +
                                          "FFINICIO=" + Serrores.FormatFechaHMS(tempRefParam15));

                                    if (vstInspec != "")
                                    {
                                        sql.Append(",GINSPECC = '" + vstInspec + "'");
                                    }
                                    else
                                    {
                                        sql.Append(",GINSPECC = NULL ");
                                    }
                                    sql.Append(
                                               " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                    SqlCommand tempCommand_6 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                    tempCommand_6.ExecuteNonQuery();

                                }
                                
                                RrEditarA�adir.Close();
                            }
                            else
                            {

                                //''''''20/05/03''''''''''
                                //cuando la fecha de inicio sea mayor que la fecha de cambio,
                                //se actualiza la sociedad.
                                if (bSocFacFar)
                                {
                                    //Si pasa, se modifica
                                    sql = new StringBuilder("UPDATE DMOVFACT SET " +
                                          "GSOCIEDA = " + vCodSoc + "," +
                                          "NAFILIAC = '" + tbNafilia.Text.Trim() + "',");
                                    if (vstInspec != "")
                                    {
                                        sql.Append("GINSPECC = '" + vstInspec + "'");
                                    }
                                    else
                                    {
                                        sql.Append("GINSPECC = NULL ");
                                    }
                                    sql.Append(
                                               " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                    SqlCommand tempCommand_7 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                    tempCommand_7.ExecuteNonQuery();
                                    //''''''''''''''''
                                }
                                else
                                {
                                    //Si no pasa, se borra
                                    sql = new StringBuilder("DELETE DMOVFACT WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
                                    SqlCommand tempCommand_8 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                    tempCommand_8.ExecuteNonQuery();

                                    
                                    object tempRefParam16 = iteration_row["ffinicio"];
                                    object tempRefParam17 = iteration_row["FFECHFIN"];
                                    sql = new StringBuilder("update ETOMASFA set fpasfact = null where " + " ganoregi = " + Convert.ToString(iteration_row["GANOREGI"]).Trim() + " and gnumregi=" + Convert.ToString(iteration_row["GNUMREGI"]).Trim() + " and  " + " fpasfact is NOT null and " + " cdosimpu is not null and " + " gfarmaco = '" + Convert.ToString(iteration_row["GCONCFAC"]) + "' and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam16) + " and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam17) + " and " + " itiposer='H' ");
                                    SqlCommand tempCommand_9 = new SqlCommand(sql.ToString(), Conexion_Base_datos.RcAdmision);
                                    tempCommand_9.ExecuteNonQuery();
                                }
                            }

                            break;
                        case "DI":

                            //En este caso, no se modifica ya que se factura como privado 

                            break;
                    }
                }
            }
            RR.Close();
            //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            //PUEDE OCURRIR QUE HAYA UN PERIODO FACTURADO POR IFMS PERO QUE LA SOCIEDA ANTIGUA NO FACTURE F�RMACOS. POR ESO NO EXISTA
            //L�NEA EN DMOVFACT DE F�RMACO. ANTONCES, COMO DICE MI HIJA ANA, SI LA NUEVA SOCIEDAD LOS FACTURA, HABR� QUE METERLOS.
            //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            bool bNSocFacFar = false; //Nueva sociedad
            bool bASocFacFar = false; //Antigua sociedad
            object tempRefParam18 = sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
            sql = new StringBuilder("SELECT * FROM APERFACT WHERE GANOADME = " + PAnoRegi.ToString() + " and GNUMADME =" + PNumregi.ToString() + " and " +
                  " FULTFACT >" + Serrores.FormatFechaHMS(tempRefParam18));
            SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sql.ToString(), Conexion_Base_datos.RcAdmision);
            RR = new DataSet();
            tempAdapter_8.Fill(RR);
            if (RR.Tables[0].Rows.Count != 0)
            {
                //Comprobamos si la nueva sociedad factura los f�rmacos.
                bNSocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Double.Parse(vCodSoc)), vstInspec);
                //Comprobamos si la antigua sociedad facturaba los f�rmacos.
                bASocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Conversion.Val(vCod1Soc)), vstInspecOLD);
                if (bNSocFacFar && !bASocFacFar)
                {
                    result = ActualizamosMovimientos_De_Farmacos_En_Dmovfact_YAsiDeLargaHaQuedado_YOtroPocoMas(vCodSoc, vstInspec);
                }
                else
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            RR.Close();

            return result;
            
		}
        private bool ActualizamosMovimientos_De_Farmacos_En_Dmovfact_YAsiDeLargaHaQuedado_YOtroPocoMas(string stCodSoc, string stInspec)
        {
            string sql = String.Empty;
            DataSet RrEditarA�adir = null;
            string fFecha_factur = String.Empty;
            string fFecha_inicio = String.Empty;
            string fFecha_fin = String.Empty;
            float siFrascos = 0;
            bool NoActualizarDMOVFACT = false;
            int iFarmacia = 0;
            bool bNSocFacFar = false; //Nueva sociedad
            bool bASocFacFar = false; //Antigua sociedad
            bool bExisteFactIndiv = false;

            try
            {
                bNSocFacFar = true;
                bASocFacFar = true;
                bExisteFactIndiv = false;
                NoActualizarDMOVFACT = false;
                //Fecha de inicio del periodo en el que se cambia la sociedad
                System.DateTime TempDate = DateTime.FromOADate(0);
                fFecha_inicio = (DateTime.TryParse(sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : sdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;

                //Comprobamos si la nueva sociedad factura los f�rmacos.
                bNSocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Conversion.Val(stCodSoc)), stInspec);
                //Comprobamos si la antigua sociedad facturaba los f�rmacos.
                bASocFacFar = Conexion_Base_datos.FacturaFarmacosLaSociedad(Convert.ToInt32(Conversion.Val(vCod1Soc)), vstInspecOLD);

                //Si la constante global "CONTALMA" en el valfanu3 tiene valor "N" y el episodio
                //es de hospitalizacion no se graban los farmacos en DMOVFACT
                sql = "select valfanu3 from sconsglo where gconsglo='CONTALMA'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                RrEditarA�adir = new DataSet();
                tempAdapter.Fill(RrEditarA�adir);
                if (RrEditarA�adir.Tables[0].Rows.Count != 0)
                {
                    NoActualizarDMOVFACT = ((Convert.ToString(RrEditarA�adir.Tables[0].Rows[0]["valfanu3"]) + "").Trim().ToUpper() == "N");
                }
                RrEditarA�adir.Close();
                //Comprombamo si se ha facturado para ese paciente de forma individual.
                object tempRefParam = fFecha_inicio;
                sql = "SELECT Max(FULTFACT) FULTFACT ,max(FFINMOVI) FFINMOVI FROM AEPISADM A " +
                      "INNER JOIN APERFACT C ON A.GANOADME = C.GANOADME AND A.GNUMADME = C.GNUMADME " +
                      "INNER JOIN AMOVIFIN B ON A.GANOADME = B.GANOREGI AND A.GNUMADME = B.GNUMREGI AND 'H' = B.ITIPOSER " +
                      "where gidenpac='" + stCodPacE + "' and faltplan is null AND B.FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam) + " ";
                fFecha_inicio = Convert.ToString(tempRefParam);
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                RrEditarA�adir = new DataSet();
                tempAdapter_2.Fill(RrEditarA�adir);

                if (!Convert.IsDBNull(RrEditarA�adir.Tables[0].Rows[0]["FULTFACT"]))
                {
                    bExisteFactIndiv = true;
                    fFecha_factur = Convert.ToDateTime(RrEditarA�adir.Tables[0].Rows[0]["FULTFACT"]).ToString("dd/MM/yyyy HH:mm:ss");
                    //Puede suceder que la fecha hasta donde hay que grabar f�rmacos sea o la fecha del fin del periodo o la fecha de facturaci�n individual
                    if (Convert.IsDBNull(RrEditarA�adir.Tables[0].Rows[0]["FFINMOVI"]))
                    {
                        fFecha_fin = Convert.ToDateTime(RrEditarA�adir.Tables[0].Rows[0]["FULTFACT"]).ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    else
                    {   
                        fFecha_fin = Convert.ToDateTime((Convert.ToDateTime(RrEditarA�adir.Tables[0].Rows[0]["FFINMOVI"]) > Convert.ToDateTime(RrEditarA�adir.Tables[0].Rows[0]["FULTFACT"])) ? RrEditarA�adir.Tables[0].Rows[0]["FULTFACT"] : RrEditarA�adir.Tables[0].Rows[0]["FFINMOVI"]).ToString("dd/MM/yyyy HH:mm:ss");
                    }
                }

                //Si los factura y antes no se facturaba y si se ha realizado la facturaci�n parcial, comprobamos si queda alg�n f�rmaco por facturar en el periodo entre la fecha de inicio de movimiento y la fecha de facturaci�n
                if (bNSocFacFar && !bASocFacFar && bExisteFactIndiv && !NoActualizarDMOVFACT)
                {

                    sql = "select nnumeri1, nnumeri2 from sconsglo where gconsglo = 'SERVREAL'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RrEditarA�adir = new DataSet();
                    tempAdapter_3.Fill(RrEditarA�adir);
                    iFarmacia = Convert.ToInt32(RrEditarA�adir.Tables[0].Rows[0]["nnumeri1"]);
                    RrEditarA�adir.Close();


                    //'Comprobamos si existe alguna toma que se hab�a facturado en el periodo en cuesti�n pero que se elimin� en un cambio anterior y, con la nueva sociedad hay que incorporarlo.
                    object tempRefParam2 = fFecha_inicio;
                    object tempRefParam3 = fFecha_fin;
                    sql = "select distinct A.GANOADME,A.GNUMADME,B.gfarmaco, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " +
                          "FROM  AEPISADM A INNER JOIN ETOMASFA B ON A.GANOADME = B.GANOREGI AND A.GNUMADME = B.GNUMREGI AND 'H' = B.ITIPOSER " +
                          "INNER JOIN AMOVIFIN C ON B.GANOREGI = C.GANOREGI AND B.GNUMREGI = C.GNUMREGI AND B.ITIPOSER = C.ITIPOSER " +
                          "INNER JOIN AMOVIMIE D ON D.itiposer=C.itiposer and D.ganoregi=C.ganoregi and D.gnumregi=C.gnumregi " +
                          "inner join DCODFARMVT E on B.gfarmaco = E.gfarmaco " +
                          "where B.fpasfact is null and (B.iproppac is null or B.iproppac <> 'S') and cdosimpu is not null and gfarmadm is null and " +
                          "A.GIDENPAC = '" + stCodPacE + "' and A.FALTPLAN IS NULL  and C.FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam2) + " and " +
                          "B.FTOMASFA <= " + Serrores.FormatFechaHMS(tempRefParam3) + " and " +
                          "(C.fmovimie<=B.ftomasfa and (C.ffinmovi>=B.ftomasfa or C.ffinmovi is null)) and " +
                          "(D.fmovimie<=B.ftomasfa and (D.ffinmovi>=B.ftomasfa or D.ffinmovi is null))  " +
                          " group by B.gfarmaco, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior,A.GANOADME,A.GNUMADME ";
                    fFecha_fin = Convert.ToString(tempRefParam3);
                    fFecha_inicio = Convert.ToString(tempRefParam2);
                    sql = sql + "Union All ";

                    object tempRefParam4 = fFecha_inicio;
                    object tempRefParam5 = fFecha_fin;
                    sql = sql + "select distinct A.GANOADME,A.GNUMADME,B.gfarmadm, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " +
                          "FROM  AEPISADM A INNER JOIN ETOMASFA B ON A.GANOADME = B.GANOREGI AND A.GNUMADME = B.GNUMREGI AND 'H' = B.ITIPOSER " +
                          "INNER JOIN AMOVIFIN C ON B.GANOREGI = C.GANOREGI AND B.GNUMREGI = C.GNUMREGI AND B.ITIPOSER = C.ITIPOSER " +
                          "INNER JOIN AMOVIMIE D ON D.itiposer=C.itiposer and D.ganoregi=C.ganoregi and D.gnumregi=C.gnumregi " +
                          "inner join DCODFARMVT E on B.gfarmadm = E.gfarmaco  " +
                          "WHERE B.fpasfact is null and (B.iproppac is null or B.iproppac <> 'S') AND cdosimpu is not null and gfarmadm is NOT null and " +
                          "A.GIDENPAC = '" + stCodPacE + "' and A.FALTPLAN IS NULL  and C.FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam4) + " and " +
                          "B.FTOMASFA <= " + Serrores.FormatFechaHMS(tempRefParam5) + " and " +
                          "(C.fmovimie<=B.ftomasfa and (C.ffinmovi>=B.ftomasfa or C.ffinmovi is null)) and " +
                          "(D.fmovimie<=B.ftomasfa and (D.ffinmovi>=B.ftomasfa or D.ffinmovi is null))  " +
                          "group by B.gfarmadm, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior,A.GANOADME,A.GNUMADME ";
                    fFecha_fin = Convert.ToString(tempRefParam5);
                    fFecha_inicio = Convert.ToString(tempRefParam4);

                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RrEditarA�adir = new DataSet();
                    tempAdapter_4.Fill(RrEditarA�adir);

                    foreach (DataRow iteration_row in RrEditarA�adir.Tables[0].Rows)
                    {
                        //grabo una fila en DMOVFACT por cada f�rmaco pendiente de facturar
                        // primero paso la cantidad consumida a unidades de medida
                        if (!Convert.IsDBNull(iteration_row["nequival"]) && !Convert.IsDBNull(iteration_row["cporenva"]))
                        {
                            siFrascos = ((float)(Conversion.Val(Convert.ToString(iteration_row["cantidad"])) / Convert.ToDouble(iteration_row["nequival"])));
                        }
                        else
                        {
                            // si no est�n rellenos los campos en farmacia no le cobro porque no
                            // puedo calcular la cantidad consumida
                            siFrascos = 0;
                        }
                        if (siFrascos != 0)
                        {

                            //grabar registro en dmovfact
                            string tempRefParam6 = siFrascos.ToString().Trim();
                            string tempRefParam7 = Serrores.ObtenerSeparadorDecimalBD(Conexion_Base_datos.RcAdmision);
                            object tempRefParam8 = iteration_row["fechamin"];
                            object tempRefParam9 = iteration_row["fechamax"];
                            object tempRefParam10 = fFecha_factur;
                            sql = "insert into dmovfact (gidenpac,ITPACFAC,ganoregi,gnumregi,itiposer,itiporig,ganoorig,gnumorig,ncantida,gconcfac,gsocieda,nafiliac,ginspecc,gcodeven,ffinicio,ffechfin,fsistema,gserresp,gsersoli,gserreal,fperfact) values ( " +
                                  "'" + stCodPacE + "','I'," + Convert.ToString(iteration_row["GANOADME"]).Trim() + "," + Convert.ToString(iteration_row["GNUMADME"]).Trim() + ",'H','H'," + Convert.ToString(iteration_row["GANOADME"]).Trim() + "," + Convert.ToString(iteration_row["GNUMADME"]).Trim() + "," + Serrores.ConvertirDecimales(ref tempRefParam6, ref tempRefParam7, 2) + ",'" + Convert.ToString(iteration_row["gfarmaco"]) + "'," +
                                  Convert.ToString(iteration_row["gsocieda"]) + "," + ((Convert.IsDBNull(iteration_row["nafiliac"])) ? "Null" : "'" + Convert.ToString(iteration_row["nafiliac"]).Trim() + "'") + "," + ((Convert.IsDBNull(iteration_row["ginspecc"])) ? "Null" : "'" + Convert.ToString(iteration_row["ginspecc"]).Trim() + "'") + "," +
                                  "'FA'," + Serrores.FormatFechaHMS(tempRefParam8) + "," + Serrores.FormatFechaHMS(tempRefParam9) + ",getdate()," + fnBuscaSerResp(Convert.ToString(iteration_row["fechamin"]), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row["GANOADME"]))), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row["GNUMADME"])))).ToString() + ", " +
                                  fnBuscaSerResp(Convert.ToString(iteration_row["fechamin"]), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row["GANOADME"]))), Convert.ToInt32(Conversion.Val(Convert.ToString(iteration_row["GNUMADME"])))).ToString() + ", " + iFarmacia.ToString() + "," + Serrores.FormatFechaHMS(tempRefParam10) + ")";
                            fFecha_factur = Convert.ToString(tempRefParam10);
                            SqlCommand tempCommand = new SqlCommand(sql, Conexion_Base_datos.RcAdmision);
                            tempCommand.ExecuteNonQuery();

                            // pongo marca de pasado a facturaci�n de todos los acumulados
                            object tempRefParam11 = fFecha_factur;
                            object tempRefParam12 = iteration_row["fechamin"];
                            object tempRefParam13 = fFecha_fin;
                            sql = "update ETOMASFA set fpasfact = " + Serrores.FormatFechaHMS(tempRefParam11) + " where " + " ganoregi = " + Convert.ToString(iteration_row["GANOADME"]).Trim() + " and gnumregi=" + Convert.ToString(iteration_row["GNUMADME"]).Trim() + " and  " + " fpasfact is null and " + " cdosimpu is not null and " + " gfarmaco = '" + Convert.ToString(iteration_row["gfarmaco"]) + "' and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam12) + " and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam13) + " and " + " itiposer='H' ";
                            fFecha_fin = Convert.ToString(tempRefParam13);
                            fFecha_factur = Convert.ToString(tempRefParam11);
                            SqlCommand tempCommand_2 = new SqlCommand(sql, Conexion_Base_datos.RcAdmision);
                            tempCommand_2.ExecuteNonQuery();


                        } //fin de hay frascos

                    }
                    RrEditarA�adir.Close();
                }
                //Si no los factura y antes s� se facturaba, borrar�amos los f�rmacos que hubiera en ese periodo si hubiera habido facturaci�n individual y si hubiera que facturar f�rmacos.
                if (!bNSocFacFar && bASocFacFar && bExisteFactIndiv && !NoActualizarDMOVFACT)
                {
                    object tempRefParam14 = fFecha_inicio;
                    sql = "select * from AEPISADM A " +
                          "INNER JOIN AMOVIFIN B ON B.GANOREGI = A.GANOADME AND B.GNUMREGI = A.GNUMADME AND B.ITIPOSER = 'H' " +
                          "INNER JOIN DMOVFACT C ON B.GANOREGI = C.GANOREGI AND B.GNUMREGI = C.GNUMREGI AND B.ITIPOSER = C.ITIPOSER AND B.GSOCIEDA = C.GSOCIEDA " +
                          "where A.GIDENPAC = '" + stCodPacE + "'  and A.FALTPLAN IS NULL  and " +
                          "B.FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam14) + " AND itiporig <> 'Q' and C.ffinicio >=B.FMOVIMIE and " +
                          "(ffechfin<=FFINMOVI or FFINMOVI is null) and  gcodeven='FA'";
                    fFecha_inicio = Convert.ToString(tempRefParam14);
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RrEditarA�adir = new DataSet();
                    tempAdapter_5.Fill(RrEditarA�adir);
                    if (RrEditarA�adir.Tables[0].Rows.Count != 0)
                    {
                        RrEditarA�adir.MoveFirst();
                    }
                    foreach (DataRow iteration_row_2 in RrEditarA�adir.Tables[0].Rows)
                    {

                        // pongo marca de pasado a facturaci�n de todos los acumulados
                        object tempRefParam15 = iteration_row_2["ffinicio"];
                        object tempRefParam16 = fFecha_fin;
                        sql = "update ETOMASFA set fpasfact = NULL where " + " ganoregi = " + Convert.ToString(iteration_row_2["GANOADME"]).Trim() + " and gnumregi=" + Convert.ToString(iteration_row_2["GNUMADME"]).Trim() + " and  " + " fpasfact is NOT null and " + " cdosimpu is not null and " + " gfarmaco = '" + Convert.ToString(iteration_row_2["gconcfac"]).Trim() + "' and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam15) + " and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam16) + " and " + " itiposer='H' ";
                        fFecha_fin = Convert.ToString(tempRefParam16);

                        SqlCommand tempCommand_3 = new SqlCommand(sql, Conexion_Base_datos.RcAdmision);
                        tempCommand_3.ExecuteNonQuery();

                        object tempRefParam17 = fFecha_inicio;
                        sql = "delete dmovfact FROM  AEPISADM A " +
                              "INNER JOIN AMOVIFIN B ON B.GANOREGI = A.GANOADME AND B.GNUMREGI = A.GNUMADME AND B.ITIPOSER = 'H' " +
                              "INNER JOIN DMOVFACT C ON B.GANOREGI = C.GANOREGI AND B.GNUMREGI = C.GNUMREGI AND B.ITIPOSER = C.ITIPOSER AND B.GSOCIEDA = C.GSOCIEDA " +
                              "where A.GIDENPAC = '" + stCodPacE + "'  and A.FALTPLAN IS NULL  and " +
                              "B.FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam17) + " AND itiporig <> 'Q' and C.ffinicio >=B.FMOVIMIE and " +
                              "(ffechfin<=FFINMOVI or FFINMOVI is null) and  gcodeven='FA'";
                        fFecha_inicio = Convert.ToString(tempRefParam17);
                        SqlCommand tempCommand_4 = new SqlCommand(sql, Conexion_Base_datos.RcAdmision);
                        tempCommand_4.ExecuteNonQuery();

                    }
                    RrEditarA�adir.Close();

                }
                return true;
            }
            catch
            {
                return false;
            }
        }

            private double NumeroConvertido(string PNumero)
        {
            double result = 0;
            int CuantosDec = 0;
            int Constante = 0;

            if (PNumero == "")
            {
                return result;
            }

            int posdec = (PNumero.IndexOf('.') + 1);
            if (posdec == 0)
            {
                posdec = (PNumero.IndexOf(',') + 1);
            }
            if (posdec == 0)
            {
                //No hay decimales en el numero
                return Double.Parse(PNumero);
            }
            else
            {
                //Tratamiento de numero en dotacion decimal
                //Ej: 3,33 = (333 / 100)
                //     3.3 = (33  / 10)
                CuantosDec = PNumero.Substring(posdec).Length;
                Constante = Convert.ToInt32(Double.Parse("1" + new string('0', CuantosDec)));
                return Double.Parse(PNumero.Substring(0, Math.Min(posdec - 1, PNumero.Length)) + PNumero.Substring(posdec)) / Constante;
            }
            return result;
        }

		//UPGRADE_NOTE: (7001) The following declaration (ProCambiar_ConsultasIntermedias) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void ProCambiar_ConsultasIntermedias(DataSet RrCursor)
		//{
				//
				//funcion que busca las consultas relacionadas a las prestaciones y les cambia la sociedad
				////UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//object tempRefParam = RrCursor.Tables[0].Rows[0]["fmovimie"];
				//object tempRefParam2 = RrCursor.Tables[0].Rows[0]["ffinmovi"];
				//string tstSql = "select * from cconsult C inner join eprestac  E on  c.ganoregi=e.ganorela " + " and c.gnumregi=e.gnumrela where  itiprela='C' " + " and e.ganoregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["ganoregi"]) + " and e.gnumregi=" + Convert.ToString(RrCursor.Tables[0].Rows[0]["gnumregi"]) + " and e.itiposer='H' " + " and e.frealiza>=" + Serrores.FormatFechaHM(tempRefParam) + " and e.frealiza<=" + Serrores.FormatFechaHM(tempRefParam2);
				//
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, Conexion_Base_datos.RcAdmision);
				//DataSet tRr1 = new DataSet();
				//tempAdapter.Fill(tRr1);
				//
				//foreach (DataRow iteration_row in tRr1.Tables[0].Rows)
				//{
					////UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRr1.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//tRr1.Edit();
					//iteration_row["gsocieda"] = vCodSoc;
					//iteration_row["nafiliac"] = tbNafilia.Text.Trim();
					//if (vstInspec == "")
					//{
						////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//iteration_row["ginspecc"] = DBNull.Value;
					//}
					//else
					//{
						//iteration_row["ginspecc"] = vstInspec;
					//}
					//string tempQuery = tRr1.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					//SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					//tempAdapter_2.Update(tRr1, tRr1.Tables[0].TableName);
				//}
				////UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRr1.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//tRr1.Close();
				//
		//}

		public int fnBuscaSerResp(string ParaFecha, int ParaAno, int ParaNum)
		{
			//ParaFecha tiene formato "dd/mm/yyyy hh:nn:ss"
			int stServicio = 0;
			string sstemp = "select dservici.gservici from amovimie,dservici where ganoregi=" + ParaAno.ToString() + " and gnumregi=" + ParaNum.ToString() + " and fmovimie<=" + Serrores.FormatFechaHM(ParaFecha) + " and " + " (ffinmovi>=" + Serrores.FormatFechaHMS(ParaFecha) + " or ffinmovi is null) and amovimie.gservior=dservici.gservici";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sstemp, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{
				stServicio = Convert.ToInt32(rrtemp.Tables[0].Rows[0]["gservici"]);
			}
			rrtemp.Close();
			return stServicio;
		}

		//---------
	}
}