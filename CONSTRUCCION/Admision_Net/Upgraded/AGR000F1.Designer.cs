using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
    partial class menu_reservas
    {

        #region "Upgrade Support "
        private static menu_reservas m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static menu_reservas DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new menu_reservas();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "_Toolbar1_Button4", "_Toolbar1_Button5", "_Toolbar1_Button6", "_Toolbar1_Button7", "_Toolbar1_Button8", "_Toolbar1_Button9", "_Toolbar1_Button10", "Toolbar1", "_chkIngreso_2", "_chkIngreso_1", "_chkIngreso_0", "Frame3", "CbEntidad", "ChEntidad", "FrmEntidad", "tbBusqueda", "rbCama", "rbPaciente", "Frame2", "cbCerrar", "SprPacientes", "Frame1", "cmbIngresoHD", "cmbIngreso", "_Label1_2", "_Label1_1", "_Label1_0", "_Line1_2", "_Line1_1", "_Line1_0", "ImageList1", "Label1", "Line1", "chkIngreso", "ShapeContainer2", "SprPacientes_Sheet1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;

        private System.Windows.Forms.PictureBox pxLineas;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button2;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button4;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button5;

        private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button6;

        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button7;

        private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button8;

        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button9;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button10;
        public Telerik.WinControls.UI.RadCommandBar Toolbar1;
        private Telerik.WinControls.UI.RadCheckBox _chkIngreso_2;
        private Telerik.WinControls.UI.RadCheckBox _chkIngreso_1;
        private Telerik.WinControls.UI.RadCheckBox _chkIngreso_0;
        public Telerik.WinControls.UI.RadGroupBox Frame3;
        //public UpgradeHelpers.MSForms.MSCombobox CbEntidad;
        public Telerik.WinControls.UI.RadDropDownList CbEntidad;
        public Telerik.WinControls.UI.RadCheckBox ChEntidad;
        public Telerik.WinControls.UI.RadGroupBox FrmEntidad;
        public Telerik.WinControls.UI.RadTextBoxControl tbBusqueda;
        public Telerik.WinControls.UI.RadRadioButton rbCama;
        public Telerik.WinControls.UI.RadRadioButton rbPaciente;
        public Telerik.WinControls.UI.RadGroupBox Frame2;
        public Telerik.WinControls.UI.RadButton cbCerrar;
        public UpgradeHelpers.Spread.FpSpread SprPacientes;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
        //public AxMSForms.AxCommandButton cmbIngresoHD;
        //public AxMSForms.AxCommandButton cmbIngreso;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;


        public Telerik.WinControls.UI.RadButton cmbIngresoHD;
        public Telerik.WinControls.UI.RadButton cmbIngreso;
        private Telerik.WinControls.UI.RadLabel _Label1_2;
        private Telerik.WinControls.UI.RadLabel _Label1_1;
        private Telerik.WinControls.UI.RadLabel _Label1_0;
        private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_2;
        private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_1;
        private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_0;
        public System.Windows.Forms.ImageList ImageList1;
        public Telerik.WinControls.UI.RadLabel[] Label1 = new Telerik.WinControls.UI.RadLabel[3];
        public Microsoft.VisualBasic.PowerPacks.LineShape[] Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[3];
        public Telerik.WinControls.UI.RadCheckBox[] chkIngreso = new Telerik.WinControls.UI.RadCheckBox[3];

        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

        public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        //private FarPoint.Win.Spread.SheetView SprPacientes_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.   
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu_reservas));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();

            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();

            this.pxLineas = new System.Windows.Forms.PictureBox();
            this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button2 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button4 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button5 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button6 = new System.Windows.Forms.ToolStripSeparator();
            this._Toolbar1_Button7 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button8 = new System.Windows.Forms.ToolStripSeparator();
            this._Toolbar1_Button9 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button10 = new Telerik.WinControls.UI.CommandBarButton();
            this.FrmEntidad = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this._chkIngreso_2 = new Telerik.WinControls.UI.RadCheckBox();
            this._chkIngreso_1 = new Telerik.WinControls.UI.RadCheckBox();
            this._chkIngreso_0 = new Telerik.WinControls.UI.RadCheckBox();
            this.CbEntidad = new UpgradeHelpers.MSForms.MSCombobox();
            this.ChEntidad = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbBusqueda = new Telerik.WinControls.UI.RadTextBoxControl();
            this.rbCama = new Telerik.WinControls.UI.RadRadioButton();
            this.rbPaciente = new Telerik.WinControls.UI.RadRadioButton();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.SprPacientes = new UpgradeHelpers.Spread.FpSpread();
            //this.cmbIngresoHD = new AxMSForms.AxCommandButton();
            //this.cmbIngreso = new AxMSForms.AxCommandButton();

            this.cmbIngresoHD = new Telerik.WinControls.UI.RadButton();
            this.cmbIngreso = new Telerik.WinControls.UI.RadButton();
            this._Label1_2 = new Telerik.WinControls.UI.RadLabel();
            this._Label1_1 = new Telerik.WinControls.UI.RadLabel();
            this._Label1_0 = new Telerik.WinControls.UI.RadLabel();
            this._Line1_2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this._Line1_1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this._Line1_0 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ImageList1 = new System.Windows.Forms.ImageList();
            this.Toolbar1.SuspendLayout();
            this.FrmEntidad.SuspendLayout();
            this.Frame3.SuspendLayout();
            this.Frame2.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.SuspendLayout();
            this.KeyPreview = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(Menu_alta_PreviewKeyDown);
            //
            // pxLineas
            //
            this.pxLineas.Image = global::ADMISION.Properties.Resources.Lineas;
            this.pxLineas.Location = new System.Drawing.Point(5, 400);
            this.pxLineas.BackColor = System.Drawing.Color.Transparent;
            this.pxLineas.Name = "pxLineas";
            this.pxLineas.Size = new System.Drawing.Size(120, 50);
            this.pxLineas.TabIndex = 20;
            this.pxLineas.TabStop = false;
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer2.Size = new System.Drawing.Size(755, 483);
            this.ShapeContainer2.Shapes.Add(_Line1_2);
            this.ShapeContainer2.Shapes.Add(_Line1_1);
            this.ShapeContainer2.Shapes.Add(_Line1_0);
            // 
            // Toolbar1
            // 
            this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Toolbar1.ImageList = ImageList1;
            this.Toolbar1.Location = new System.Drawing.Point(0, 0);
            this.Toolbar1.Name = "Toolbar1";
            this.Toolbar1.ShowItemToolTips = true;
            this.Toolbar1.Size = new System.Drawing.Size(755, 28);
            this.Toolbar1.TabIndex = 1;            
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
this.radCommandBarLineElement1});

            // 
            // radCommandBarLineElement1 
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            //this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Size = new System.Drawing.Size(755, 28);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1 
            //              
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.StretchHorizontally = true;
            this.radCommandBarStripElement1.StretchVertically = true;
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
this._Toolbar1_Button1,
this._Toolbar1_Button2,
this._Toolbar1_Button3,
this._Toolbar1_Button4,
this._Toolbar1_Button5,
//this._Toolbar1_Button6,
this._Toolbar1_Button7,
//this._Toolbar1_Button8,
this._Toolbar1_Button9,
this._Toolbar1_Button10});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";


            // 
            // _Toolbar1_Button1
            // 
            //this._Toolbar1_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button1.ImageIndex = 2;
            //this._Toolbar1_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button1.Name = "";
            this._Toolbar1_Button1.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button1.Tag = "";
            this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button1.ToolTipText = "Nueva";
            this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button2
            // 
            //this._Toolbar1_Button2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button2.Enabled = false;
            this._Toolbar1_Button2.ImageIndex = 0;
            //this._Toolbar1_Button2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button2.Name = "";
            this._Toolbar1_Button2.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button2.Tag = "";
            this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button2.ToolTipText = "Documentos";
            this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button3
            // 
            //this._Toolbar1_Button3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button3.Enabled = false;
            this._Toolbar1_Button3.ImageIndex = 3;
            //this._Toolbar1_Button3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button3.Name = "";
            this._Toolbar1_Button3.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button3.Tag = "";
            this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button3.ToolTipText = "Anulaci�n";
            this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button4
            // 
            //this._Toolbar1_Button4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button4.Enabled = false;
            this._Toolbar1_Button4.ImageIndex = 4;
            //this._Toolbar1_Button4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button4.Name = "";
            this._Toolbar1_Button4.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button4.Tag = "";
            this._Toolbar1_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button4.ToolTipText = "Mantenimiento de datos de filiaci�n";
            this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button5
            // 
            //this._Toolbar1_Button5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button5.ImageIndex = 7;
            //this._Toolbar1_Button5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button5.Name = "";
            this._Toolbar1_Button5.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button5.Tag = "";
            this._Toolbar1_Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button5.ToolTipText = "Petici�n de Historia Cl�nica";
            this._Toolbar1_Button5.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button6
            // 
            this._Toolbar1_Button6.Name = "";
            this._Toolbar1_Button6.Size = new System.Drawing.Size(6, 22);
            this._Toolbar1_Button6.Tag = "";
            this._Toolbar1_Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button7
            // 
            //this._Toolbar1_Button7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button7.ImageIndex = 5;
            //this._Toolbar1_Button7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button7.Name = "";
            this._Toolbar1_Button7.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button7.Tag = "";
            this._Toolbar1_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button7.ToolTipText = "Listados de reservas pendientes de ingreso";
            this._Toolbar1_Button7.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button8
            // 
            this._Toolbar1_Button8.Name = "";
            this._Toolbar1_Button8.Size = new System.Drawing.Size(6, 22);
            this._Toolbar1_Button8.Tag = "";
            this._Toolbar1_Button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button8.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button9
            // 
            //this._Toolbar1_Button9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button9.ImageIndex = 6;
            //this._Toolbar1_Button9.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button9.Name = "";
            this._Toolbar1_Button9.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button9.Tag = "";
            this._Toolbar1_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button9.ToolTipText = "Listados de reservas anuladas";
            this._Toolbar1_Button9.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button10
            // 
            //this._Toolbar1_Button10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
            this._Toolbar1_Button10.ImageIndex = 8;
            //this._Toolbar1_Button10.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._Toolbar1_Button10.Enabled = false;
            this._Toolbar1_Button10.Name = "";
            this._Toolbar1_Button10.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button10.Tag = "";
            this._Toolbar1_Button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button10.ToolTipText = "IMDH-Open";
            this._Toolbar1_Button10.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // FrmEntidad
            // 
            //this.FrmEntidad.BackColor = System.Drawing.SystemColors.Control;
            this.FrmEntidad.Controls.Add(this.Frame3);
            this.FrmEntidad.Controls.Add(this.CbEntidad);
            this.FrmEntidad.Controls.Add(this.ChEntidad);
            this.FrmEntidad.Enabled = true;
            //this.FrmEntidad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FrmEntidad.Location = new System.Drawing.Point(6, 74);
            this.FrmEntidad.Name = "FrmEntidad";
            this.FrmEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmEntidad.Size = new System.Drawing.Size(533, 71);
            this.FrmEntidad.TabIndex = 8;
            this.FrmEntidad.Text = "Filtro por";
            this.FrmEntidad.Visible = true;
            // 
            // Frame3
            // 
            //this.Frame3.BackColor = System.Drawing.SystemColors.Control;
            this.Frame3.Controls.Add(this._chkIngreso_2);
            this.Frame3.Controls.Add(this._chkIngreso_1);
            this.Frame3.Controls.Add(this._chkIngreso_0);
            this.Frame3.Enabled = true;
            //this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame3.Location = new System.Drawing.Point(384, 0);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(149, 71);
            this.Frame3.TabIndex = 10;
            this.Frame3.Text = "Tipo Ingreso";
            this.Frame3.Visible = true;
            // 
            // _chkIngreso_2
            // 
            //this._chkIngreso_2.Appearance = System.Windows.Forms.Appearance.Normal;
            //this._chkIngreso_2.BackColor = System.Drawing.SystemColors.Control;
            this._chkIngreso_2.CausesValidation = true;
            //this._chkIngreso_2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._chkIngreso_2.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this._chkIngreso_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkIngreso_2.Enabled = true;
            //this._chkIngreso_2.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkIngreso_2.Location = new System.Drawing.Point(16, 50);
            this._chkIngreso_2.Name = "_chkIngreso_2";
            this._chkIngreso_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkIngreso_2.Size = new System.Drawing.Size(115, 13);
            this._chkIngreso_2.TabIndex = 13;
            this._chkIngreso_2.TabStop = true;
            this._chkIngreso_2.Text = "Programado";
            this._chkIngreso_2.Visible = true;
            this._chkIngreso_2.CheckStateChanged += new System.EventHandler(this.chkIngreso_CheckStateChanged);
            // 
            // _chkIngreso_1
            // 
            //this._chkIngreso_1.Appearance = System.Windows.Forms.Appearance.Normal;
            //this._chkIngreso_1.BackColor = System.Drawing.SystemColors.Control;
            this._chkIngreso_1.CausesValidation = true;
            //this._chkIngreso_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._chkIngreso_1.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this._chkIngreso_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkIngreso_1.Enabled = true;
            //this._chkIngreso_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkIngreso_1.Location = new System.Drawing.Point(16, 34);
            this._chkIngreso_1.Name = "_chkIngreso_1";
            this._chkIngreso_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkIngreso_1.Size = new System.Drawing.Size(115, 13);
            this._chkIngreso_1.TabIndex = 12;
            this._chkIngreso_1.TabStop = true;
            this._chkIngreso_1.Text = "Urgente";
            this._chkIngreso_1.Visible = true;
            this._chkIngreso_1.CheckStateChanged += new System.EventHandler(this.chkIngreso_CheckStateChanged);
            // 
            // _chkIngreso_0
            // 
            //this._chkIngreso_0.Appearance = System.Windows.Forms.Appearance.Normal;
            //this._chkIngreso_0.BackColor = System.Drawing.SystemColors.Control;
            this._chkIngreso_0.CausesValidation = true;
            //this._chkIngreso_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._chkIngreso_0.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this._chkIngreso_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkIngreso_0.Enabled = true;
            //this._chkIngreso_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkIngreso_0.Location = new System.Drawing.Point(16, 18);
            this._chkIngreso_0.Name = "_chkIngreso_0";
            this._chkIngreso_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkIngreso_0.Size = new System.Drawing.Size(115, 13);
            this._chkIngreso_0.TabIndex = 11;
            this._chkIngreso_0.TabStop = true;
            this._chkIngreso_0.Text = "Inmediato";
            this._chkIngreso_0.Visible = true;
            this._chkIngreso_0.CheckStateChanged += new System.EventHandler(this.chkIngreso_CheckStateChanged);
            // 
            // CbEntidad
            // 
            //this.CbEntidad.BackColor = System.Drawing.SystemColors.Window;
            this.CbEntidad.CausesValidation = true;
            this.CbEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbEntidad.DropDownStyle = Telerik.WinControls.UI.RadDropDownListStyle.DropDown;
            this.CbEntidad.Enabled = false;
            //this.CbEntidad.ForeColor = System.Drawing.SystemColors.WindowText;
            //this.CbEntidad.IntegralHeight = true;
            this.CbEntidad.Location = new System.Drawing.Point(76, 26);
            this.CbEntidad.Name = "CbEntidad";
            this.CbEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbEntidad.Size = new System.Drawing.Size(291, 21);
            //this.CbEntidad.Sorted = false;
            this.CbEntidad.TabIndex = 0;
            this.CbEntidad.TabStop = true;
            this.CbEntidad.Visible = true;
            CbEntidad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;

            this.CbEntidad.SelectedValueChanged += new System.EventHandler(this.CbEntidad_SelectionChangeCommitted);
            // 
            // ChEntidad
            // 
            //this.ChEntidad.Appearance = System.Windows.Forms.Appearance.Normal;
            //this.ChEntidad.BackColor = System.Drawing.SystemColors.Control;
            this.ChEntidad.CausesValidation = true;
            //this.ChEntidad.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ChEntidad.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.ChEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChEntidad.Enabled = true;
            //this.ChEntidad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChEntidad.Location = new System.Drawing.Point(10, 26);
            this.ChEntidad.Name = "ChEntidad";
            this.ChEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChEntidad.Size = new System.Drawing.Size(63, 21);
            this.ChEntidad.TabIndex = 9;
            this.ChEntidad.TabStop = true;
            this.ChEntidad.Text = "Entidad";
            this.ChEntidad.Visible = true;
            this.ChEntidad.CheckStateChanged += new System.EventHandler(this.ChEntidad_CheckStateChanged);
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.tbBusqueda);
            this.Frame2.Controls.Add(this.rbCama);
            this.Frame2.Controls.Add(this.rbPaciente);
            this.Frame2.Enabled = true;
            //this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(6, 32);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(533, 41);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Buscar por ..";
            this.Frame2.Visible = true;
            // 
            // tbBusqueda
            // 
            this.tbBusqueda.AcceptsReturn = true;
            //this.tbBusqueda.BackColor = System.Drawing.SystemColors.Window;
            //this.tbBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbBusqueda.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbBusqueda.Enabled = false;
            //this.tbBusqueda.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbBusqueda.Location = new System.Drawing.Point(160, 14);
            this.tbBusqueda.MaxLength = 60;
            this.tbBusqueda.Name = "tbBusqueda";
            this.tbBusqueda.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbBusqueda.Size = new System.Drawing.Size(353, 19);
            this.tbBusqueda.TabIndex = 14;
            this.tbBusqueda.Enter += new System.EventHandler(this.tbBusqueda_Enter);
            this.tbBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBusqueda_KeyPress);
            this.tbBusqueda.TextChanged += new System.EventHandler(this.tbBusqueda_TextChanged);
            // 
            // rbCama
            // 
            //this.rbCama.Appearance = System.Windows.Forms.Appearance.Normal;
            //this.rbCama.BackColor = System.Drawing.SystemColors.Control;
            this.rbCama.CausesValidation = true;
            //this.rbCama.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //this.rbCama.Checked = false;
            this.rbCama.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbCama.Enabled = true;
            //this.rbCama.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbCama.Location = new System.Drawing.Point(98, 16);
            this.rbCama.Name = "rbCama";
            this.rbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbCama.Size = new System.Drawing.Size(89, 17);
            this.rbCama.TabIndex = 7;
            this.rbCama.TabStop = true;
            this.rbCama.Text = "Cama ";
            //this.rbCama.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbCama.Visible = true;
            this.rbCama.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbCama_CheckedChanged);
            //this.rbCama.CheckedChanged += new System.EventHandler(this.rbCama_CheckedChanged);
            // 
            // rbPaciente
            // 
            //this.rbPaciente.Appearance = System.Windows.Forms.Appearance.Normal;
            //this.rbPaciente.BackColor = System.Drawing.SystemColors.Control;
            this.rbPaciente.CausesValidation = true;
            //this.rbPaciente.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //this.rbPaciente.Checked = false;
            this.rbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbPaciente.Enabled = true;
            //this.rbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbPaciente.Location = new System.Drawing.Point(12, 16);
            this.rbPaciente.Name = "rbPaciente";
            this.rbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbPaciente.Size = new System.Drawing.Size(81, 17);
            this.rbPaciente.TabIndex = 6;
            this.rbPaciente.TabStop = true;
            this.rbPaciente.Text = "Paciente";
            //this.rbPaciente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbPaciente.Visible = true;
            this.rbPaciente.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbPaciente_CheckedChanged);
            //this.rbPaciente.CheckedChanged += new System.EventHandler(this.rbPaciente_CheckedChanged);
            // 
            // cbCerrar
            // 
            //this.cbCerrar.BackColor = System.Drawing.SystemColors.Control;
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbCerrar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbCerrar.Location = new System.Drawing.Point(670, 446);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 29);
            this.cbCerrar.TabIndex = 4;
            this.cbCerrar.Text = "C&errar";
            //this.cbCerrar.UseVisualStyleBackColor = false;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.SprPacientes);
            this.Frame1.Enabled = true;
            //this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.Location = new System.Drawing.Point(2, 148);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(749, 290);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "Reservas pendientes de ingreso";
            this.Frame1.Visible = true;
            // 
            // SprPacientes
            // 
            gridViewTextBoxColumn1.HeaderText = "Paciente";
            gridViewTextBoxColumn1.Name = "Col_Paciente";
            gridViewTextBoxColumn1.Width = 180;
            gridViewTextBoxColumn2.HeaderText = "Fecha realizaci�n reserva";
            gridViewTextBoxColumn2.Name = "Col_FecRealiza";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.HeaderText = "Cama";
            gridViewTextBoxColumn3.Name = "Col_Cama";
            gridViewTextBoxColumn3.Width = 60;
            gridViewTextBoxColumn4.HeaderText = "Gidenpac";
            gridViewTextBoxColumn4.Name = "Col_IdPaciente";
            gridViewTextBoxColumn4.Width = 120;
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn5.HeaderText = "Fecha prevista de ingreso";
            gridViewTextBoxColumn5.Multiline = true;
            gridViewTextBoxColumn5.Name = "Col_FecIngreso";
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.HeaderText = "Edad";
            gridViewTextBoxColumn6.Name = "Col_Edad";
            gridViewTextBoxColumn6.Width = 60;
            gridViewTextBoxColumn7.HeaderText = "Financiadora";
            gridViewTextBoxColumn7.Name = "Col_Financia";
            gridViewTextBoxColumn7.Width = 170;
            gridViewTextBoxColumn8.HeaderText = "Servicio del ingreso";
            gridViewTextBoxColumn8.Name = "Col_ServiIngre";
            gridViewTextBoxColumn8.Width = 170;
            gridViewTextBoxColumn9.HeaderText = "Unidad de enfermeria";
            gridViewTextBoxColumn9.Name = "Col_UnidadEnf";
            gridViewTextBoxColumn9.Width = 170;
            gridViewTextBoxColumn10.HeaderText = "Fianza";
            gridViewTextBoxColumn10.Name = "Col_Fianza";
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.HeaderText = "Fecha de intervenci�n";
            gridViewTextBoxColumn11.Name = "Col_FecInterven";
            gridViewTextBoxColumn11.Width = 80;
            gridViewTextBoxColumn12.HeaderText = "Previsi�n de d�as de estancia";
            gridViewTextBoxColumn12.Name = "Col_DiasEstanci";
            gridViewTextBoxColumn12.Width = 120;
            gridViewTextBoxColumn13.HeaderText = "Prioridad";
            gridViewTextBoxColumn13.Name = "Col_Prioridad";
            gridViewTextBoxColumn13.Width = 80;
            gridViewTextBoxColumn14.HeaderText = "Tipo ingreso";
            gridViewTextBoxColumn14.Name = "Col_TipoIngre";
            gridViewTextBoxColumn14.Width = 80;
            gridViewTextBoxColumn15.HeaderText = "Ambito";
            gridViewTextBoxColumn15.Name = "Col_Ambito";
            gridViewTextBoxColumn15.Width = 80;
            gridViewTextBoxColumn16.HeaderText = "Centro Origen";
            gridViewTextBoxColumn16.Name = "Col_CentOrigen";
            gridViewTextBoxColumn16.Width = 170;
            gridViewTextBoxColumn17.HeaderText = "Servicio Solicitante";
            gridViewTextBoxColumn17.Name = "Col_SerSolici";
            gridViewTextBoxColumn17.Width = 250;
            gridViewTextBoxColumn18.HeaderText = "Medico Solicitante";
            gridViewTextBoxColumn18.Name = "Col_MedSolici";
            gridViewTextBoxColumn18.Width = 250;
            gridViewTextBoxColumn19.HeaderText = "Usuario";
            gridViewTextBoxColumn19.Name = "Col_Usuario";
            gridViewTextBoxColumn19.Width = 200;
            gridViewTextBoxColumn20.HeaderText = "Observaciones";
            gridViewTextBoxColumn20.Name = "Col_Observa";
            gridViewTextBoxColumn20.Width = 350;
            gridViewTextBoxColumn21.HeaderText = "Id. Reserva";
            gridViewTextBoxColumn21.Name = "Col_IDReserva";
            gridViewTextBoxColumn21.Width = 80;
            gridViewTextBoxColumn22.HeaderText = "Ord_Fec_Reali";
            gridViewTextBoxColumn22.Name = "Col_OrdenFecRealiza";
            gridViewTextBoxColumn22.Width = 80;
            gridViewTextBoxColumn23.HeaderText = "Ord_Fec_Ingreso";
            gridViewTextBoxColumn23.Name = "Col_OrdenFecIngreso";
            gridViewTextBoxColumn23.Width = 80;
            gridViewTextBoxColumn24.HeaderText = "Ord_Fec_Intervencion";
            gridViewTextBoxColumn24.Name = "Col_OrdenFecInterven";
            gridViewTextBoxColumn24.Width = 80;
            gridViewTextBoxColumn25.HeaderText = "Hospital Dia";
            gridViewTextBoxColumn25.Name = "Col_HospitalDia";
            gridViewTextBoxColumn25.Width = 80;
            gridViewTextBoxColumn26.HeaderText = "gservici";
            gridViewTextBoxColumn26.Name = "Col_gservici";
            gridViewTextBoxColumn26.Width = 80;
            gridViewTextBoxColumn27.HeaderText = "gpersona";
            gridViewTextBoxColumn27.Name = "Col_gpersona";
            gridViewTextBoxColumn27.Width = 80;
            gridViewTextBoxColumn28.HeaderText = "gsersoli";
            gridViewTextBoxColumn28.Name = "Col_gsersoli";
            gridViewTextBoxColumn28.Width = 80;
            gridViewTextBoxColumn29.HeaderText = "gpersoli";
            gridViewTextBoxColumn29.Name = "Col_gpersoli";
            gridViewTextBoxColumn29.Width = 80;
            gridViewTextBoxColumn30.HeaderText = "ganourge";
            gridViewTextBoxColumn30.Name = "Col_ganourge";
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn31.HeaderText = "gnumurge";
            gridViewTextBoxColumn31.Name = "Col_gnumurge";
            gridViewTextBoxColumn31.Width = 80;
            this.SprPacientes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            this.gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31});

            this.SprPacientes.Location = new System.Drawing.Point(6, 16);
            this.SprPacientes.Name = "SprPacientes";
            this.SprPacientes.Size = new System.Drawing.Size(735, 269);
            this.SprPacientes.TabIndex = 3;
            this.SprPacientes.AutoScroll = true;
            this.SprPacientes.AutoSizeRows = false;            
            this.SprPacientes.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.SprPacientes.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            this.SprPacientes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellClick);
            this.SprPacientes.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellDoubleClick);
            this.SprPacientes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprPacientes_MouseUp);
            this.SprPacientes.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.SprPacientes_RowFormatting);
            this.SprPacientes.TableElement.RowHeight = 40;            
            this.SprPacientes.TopLeftChange += new UpgradeHelpers.Spread.TopLeftChangeEventHandler(this.SprPacientes_TopLeftChange);
            // 
            // cmbIngresoHD
            // 
            this.cmbIngresoHD.Location = new System.Drawing.Point(624, 448);
            this.cmbIngresoHD.Name = "cmbIngresoHD";
            this.cmbIngresoHD.Size = new System.Drawing.Size(33, 29);
            this.cmbIngresoHD.TabIndex = 19;
            this.cmbIngresoHD.Visible = false;
            this.cmbIngresoHD.Click += new System.EventHandler(this.cmbIngresoHD_ClickEvent);
            this.cmbIngresoHD.Image = global::ADMISION.Properties.Resources.AGR000F1_000;
            this.cmbIngresoHD.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbIngreso
            // 
            this.cmbIngreso.Location = new System.Drawing.Point(624, 448);
            this.cmbIngreso.Name = "cmbIngreso";
            this.cmbIngreso.Size = new System.Drawing.Size(33, 29);
            this.cmbIngreso.TabIndex = 18;
            this.cmbIngreso.Visible = false;
            this.cmbIngreso.Click += new System.EventHandler(this.cmbIngreso_ClickEvent);
            this.cmbIngreso.Image = global::ADMISION.Properties.Resources.AGR000F1_001;
            this.cmbIngreso.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _Label1_2
            // 
            //this._Label1_2.BackColor = System.Drawing.SystemColors.Control;
            //this._Label1_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Label1_2.Cursor = System.Windows.Forms.Cursors.Default;
            //this._Label1_2.ForeColor = System.Drawing.Color.Red;
            this._Label1_2.Location = new System.Drawing.Point(48, 468);
            this._Label1_2.Name = "_Label1_2";
            this._Label1_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label1_2.Size = new System.Drawing.Size(117, 17);
            this._Label1_2.TabIndex = 17;
            this._Label1_2.Text = "Urgentes";
            // 
            // _Label1_1
            // 
            //this._Label1_1.BackColor = System.Drawing.SystemColors.Control;
            //this._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Label1_1.Cursor = System.Windows.Forms.Cursors.Default;
            //this._Label1_1.ForeColor = System.Drawing.SystemColors.WindowText;
            this._Label1_1.Location = new System.Drawing.Point(48, 436);
            this._Label1_1.Name = "_Label1_1";
            this._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label1_1.Size = new System.Drawing.Size(117, 15);
            this._Label1_1.TabIndex = 16;
            this._Label1_1.Text = "Programadas";
            // 
            // _Label1_0
            // 
            //this._Label1_0.BackColor = System.Drawing.SystemColors.Control;
            //this._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Label1_0.Cursor = System.Windows.Forms.Cursors.Default;
            //this._Label1_0.ForeColor = System.Drawing.Color.Blue;
            this._Label1_0.Location = new System.Drawing.Point(48, 452);
            this._Label1_0.Name = "_Label1_0";
            this._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label1_0.Size = new System.Drawing.Size(117, 19);
            this._Label1_0.TabIndex = 15;
            this._Label1_0.Text = "Inmediatas";
            // 
            // _Line1_2
            // 
            this._Line1_2.BorderColor = System.Drawing.Color.Red;
            this._Line1_2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._Line1_2.BorderWidth = 4;
            this._Line1_2.Enabled = false;
            this._Line1_2.Name = "_Line1_2";
            this._Line1_2.Visible = true;
            this._Line1_2.X1 = (int)14;
            this._Line1_2.X2 = (int)38;
            this._Line1_2.Y1 = (int)611;
            this._Line1_2.Y2 = (int)611;
            // 
            // _Line1_1
            // 
            this._Line1_1.BorderColor = System.Drawing.SystemColors.WindowText;
            this._Line1_1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._Line1_1.BorderWidth = 4;
            this._Line1_1.Enabled = false;
            this._Line1_1.Name = "_Line1_1";
            this._Line1_1.Visible = true;
            this._Line1_1.X1 = (int)14;
            this._Line1_1.X2 = (int)38;
            this._Line1_1.Y1 = (int)584;
            this._Line1_1.Y2 = (int)584;
            // 
            // _Line1_0
            // 
            this._Line1_0.BorderColor = System.Drawing.Color.Blue;
            this._Line1_0.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._Line1_0.BorderWidth = 4;
            this._Line1_0.Enabled = false;
            this._Line1_0.Name = "_Line1_0";
            this._Line1_0.Visible = true;
            this._Line1_0.X1 = (int)14;
            this._Line1_0.X2 = (int)38;
            this._Line1_0.Y1 = (int)598;
            this._Line1_0.Y2 = (int)598;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ImageList1.ImageStream");
            this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "");
            this.ImageList1.Images.SetKeyName(5, "");
            this.ImageList1.Images.SetKeyName(6, "");
            this.ImageList1.Images.SetKeyName(7, "");
            this.ImageList1.Images.SetKeyName(8, "");
            // 
            // menu_reservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            //this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(755, 483);
            this.Controls.Add(this.Toolbar1);
            this.Controls.Add(this.FrmEntidad);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cmbIngresoHD);
            this.Controls.Add(this.cmbIngreso);
            this.Controls.Add(this._Label1_2);
            this.Controls.Add(this._Label1_1);
            this.Controls.Add(this._Label1_0);            
            this.Controls.Add(this.pxLineas);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;            
            this.Location = new System.Drawing.Point(12, 103);            
            this.MaximizeBox = true;
            this.MinimizeBox = false;
            this.Name = "menu_reservas";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "RESERVAS - AGR000F1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ToolTipMain.SetToolTip(this.cmbIngresoHD, "Ingreso en hospital de d�a");
            this.ToolTipMain.SetToolTip(this.cmbIngreso, "Ingreso en hospitalizaci�n");            
            this.Activated += new System.EventHandler(this.menu_reservas_Activated);
            base.Closing += new System.ComponentModel.CancelEventHandler(this.menu_reservas_Closed);
            this.Load += new System.EventHandler(this.menu_reservas_Load);            
            this.Resize += new System.EventHandler(this.menu_reservas_Resize);
            this.Toolbar1.ResumeLayout(false);
            this.FrmEntidad.ResumeLayout(false);
            this.Frame3.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        void ReLoadForm(bool addEvents)
        {
            InitializechkIngreso();
            InitializeLine1();
            InitializeLabel1();
            //This form is an MDI child.
            //This code simulates the VB6 
            // functionality of automatically
            // loading and showing an MDI
            // child's parent.
            this.MdiParent = ADMISION.menu_admision.DefInstance;
            ADMISION.menu_admision.DefInstance.Show();
            //The MDI form in the VB6 project had its
            //AutoShowChildren property set to True
            //To simulate the VB6 behavior, we need to
            //automatically Show the form whenever it
            //is loaded.  If you do not want this behavior
            //then delete the following line of code
            //UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
            this.Show();
        }
        void InitializechkIngreso()
        {
            this.chkIngreso = new Telerik.WinControls.UI.RadCheckBox[3];
            this.chkIngreso[2] = _chkIngreso_2;
            this.chkIngreso[1] = _chkIngreso_1;
            this.chkIngreso[0] = _chkIngreso_0;
        }
        void InitializeLine1()
        {
            this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[3];
            this.Line1[2] = _Line1_2;
            this.Line1[1] = _Line1_1;
            this.Line1[0] = _Line1_0;
        }
        void InitializeLabel1()
        {
            this.Label1 = new Telerik.WinControls.UI.RadLabel[3];
            this.Label1[2] = _Label1_2;
            this.Label1[1] = _Label1_1;
            this.Label1[0] = _Label1_0;
        }
        #endregion
    }
}