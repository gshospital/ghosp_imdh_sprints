using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace ADMISION
{
	public partial class docu_reservas
        : Telerik.WinControls.UI.RadForm
    {

		public docu_reservas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		private void docu_reservas_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		ClaseReservasAdmision claseReserva = null;
        Crystal ListadoParticular = null; //OBJETO CRYSTAL REPORT

        DataSet vRrDOC = null;
		string vstSqDOC = String.Empty;
		int viIn = 0; //indice de las listas

		string NombrePaciente = String.Empty;
		string DniPaciente = String.Empty;
		string DireccionPaciente = String.Empty;



		string NombreDirector = String.Empty;
		string DniDirector = String.Empty;
		string CifEmpresa = String.Empty;
		string NombreProvincia = String.Empty;
		string PoblacionEmpresa = String.Empty;
		string NumeroRegistroMercantil = String.Empty;

		string NombreRepresentante = String.Empty;
		string DireccionRepresentante = String.Empty;
		string DniRepresentante = String.Empty;
		bool bPacienteEsResponsable = false; // si el paciente es el mismo responsable

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			ListadoParticular = Conexion_Base_datos.LISTADO_CRYSTAL;
			this.Cursor = Cursors.WaitCursor;


			//oscar 22/11/2004
			//proImprimirHoja
			if (mbAcceso.ObtenerCodigoSConsglo("DOCRESIN", "VALFANU1", Conexion_Base_datos.RcAdmision).Trim().ToUpper() == "S")
			{
				proImprimirHojaFormatoNuevo();
			}
			else
			{
				proImprimirHoja();
			}
            //---------------


            ListadoParticular = null;
            Activar_Imprimir();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
            ListadoParticular = null;
            this.Dispose();
            this.Close();
		}

		private void CbDelTodo_Click(Object eventSender, EventArgs eventArgs)
		{
            //quito todos de 2 y los pongo en 1

            if (LsbLista2.Items.Count > 0)
            {
                for (int i = 0; i < LsbLista2.Items.Count; i++)
                {
                    LsbLista1.Items.Add(LsbLista2.Items[i].Text);
                }
                LsbLista2.Items.Clear();
            }

		}

		private void CbDelUno_Click(Object eventSender, EventArgs eventArgs)
		{
			//pongo el deseleccionado de la 2 en la 1 y le quito de 2
			 if (LsbLista2.SelectedIndex != -1)
                {
				LsbLista1.Items.Add(LsbLista2.Items[LsbLista2.SelectedIndex].Text);
                //LsbLista1.SetItemData(LsbLista1.GetNewIndex(), LsbLista2.GetItemData(ListBoxHelper.GetSelectedIndex(LsbLista2)));
                LsbLista2.Items.Remove(LsbLista2.SelectedItem);
			}

		}

		private void CbInsTodo_Click(Object eventSender, EventArgs eventArgs)
		{
            //quito todos de 1 y los pongo en 2
            //indice de las listas
            if (LsbLista1.Items.Count > 0)
            {                                
                for (int i = 0;  i < LsbLista1.Items.Count; i++)                                 
                {
                    LsbLista2.Items.Add(LsbLista1.Items[i].Text);
                }
                LsbLista1.Items.Clear();
			}

		}

		private void CbInsUno_Click(Object eventSender, EventArgs eventArgs)
		{
			//pongo el seleccionado en la segunda lista y le quito de la primera

                if (LsbLista1.SelectedIndex != -1)
                {
                LsbLista2.Items.Add(LsbLista1.Items[LsbLista1.SelectedIndex].Text);
                //LsbLista2.SetItemData(LsbLista2.GetNewIndex(), LsbLista1.GetItemData(ListBoxHelper.GetSelectedIndex(LsbLista1)));
                LsbLista1.Items.Remove(LsbLista1.SelectedItem);
			}

		}

		private void docu_reservas_Load(Object eventSender, EventArgs eventArgs)
		{
			string NombrePaciente = String.Empty;
			SqlCommand tRqDOC = new SqlCommand();
			string LitPersona = String.Empty;

			try
			{

				//cargo la lista con los servicios
				vstSqDOC = "select ddocmate, gdocmate from adocmate order by ddocmate";
				tRqDOC.Connection = Conexion_Base_datos.RcAdmision;
				tRqDOC.CommandText = vstSqDOC;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tRqDOC);
				vRrDOC = new DataSet();
				tempAdapter.Fill(vRrDOC);
				foreach (DataRow iteration_row in vRrDOC.Tables[0].Rows)
				{
					if (!Convert.IsDBNull(iteration_row["ddocmate"]))
					{
						LsbLista1.Items.Add(Convert.ToString(iteration_row["ddocmate"]).Trim());
						//LsbLista1.SetItemData(LsbLista1.GetNewIndex(), Convert.ToInt32(iteration_row["gdocmate"]));
					}
				}

                vRrDOC.Close();
				tRqDOC = null;

				// guardamos en una variable local la informacion original a imprimir
				claseReserva = menu_reservas.DefInstance.ObjReservaAdmision;
				menu_reservas.DefInstance.SprPacientes.Col = 1;
				NombrePaciente = menu_reservas.DefInstance.SprPacientes.Text;
				lbPaciente.Text = NombrePaciente;

				LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
				LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
				Frame3.Text = LitPersona;
			}
			catch (Exception ex)
			{

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "docu_reservas:Form_load", ex);
			}

		}
		// imprime la hoja de informacion de la reserva en cuestion
		private void proImprimirHoja()
		{
			try
			{
				string SqlListado = String.Empty;
				StringBuilder ListadoDocumentos = new StringBuilder();
				PrintDialog VentanaImpresionPrint = null; //simula el commondialog
				DataSet Rrsql = null;


                //Rellena los documentos necesarios:
                //if (LsbLista2.GetListItem(0) == "")
                if (LsbLista2.SelectedItems.Count > 0)
                {
                    if (LsbLista2.Items[0].Value.ToString() == "")
                    {
                        ListadoDocumentos = new StringBuilder("Ninguno");
                    }
                    else
                    {
                        ListadoDocumentos = new StringBuilder("1.-  " + LsbLista2.Items[0].Value.ToString());
                        for (viIn = 1; viIn <= LsbLista2.Items.Count - 1; viIn++)
                        { // Bucle por la lista.
                          //ListadoDocumentos.Append("\" + Chr(10) + \"" + Conversion.Str(viIn + 1).Trim() + ".-  " + LsbLista2.GetListItem(viIn));
                            ListadoDocumentos.Append("\" + Chr(10) + \"" + Conversion.Str(viIn + 1).Trim() + ".-  " + LsbLista2.Items[viIn].Value.ToString());

                        }
                    }
                }
                else
                {
                    ListadoDocumentos = new StringBuilder("Ninguno");
                }

                //Selecciona los datos

                VentanaImpresionPrint = menu_admision.DefInstance.CommDiag_imprePrint;
				//VentanaImpresion.CancelError = true;
                VentanaImpresionPrint.ShowDialog();

				//OSCAR C ENE 2005
				//SqlListado = "SELECT ARESINGRVA.gplantas, ARESINGRVA.ghabitac, ARESINGRVA.gcamasbo, ARESINGRVA.iregaloj,ARESINGRVA.fpreving,ARESINGRVA.cfianza, " & _
				//" DPACIENT.dnombpac , DPACIENT.dape1pac, DPACIENT.dape2pac,DPERSONA.*, sconsglo.valfanu1 " & _
				//" From ARESINGRVA,DPACIENT,SUSUARIO,DPERSONA,sconsglo WHERE DPACIENT.GIDENPAC=ARESINGRVA.GIDENPAC" & _
				//" AND FPREVING=" & FormatFechaHM(claseReserva.FechaPrevistaIngreso) & "" & _
				//" AND ARESINGRVA.GUSUARIO=SUSUARIO.GUSUARIO AND SUSUARIO.GPERSONA=DPERSONA.GPERSONA" & _
				//" AND DPACIENT.GIDENPAC='" & claseReserva.IdentificadorPaciente & "' and gconsglo ='ilitresv'"
				object tempRefParam = claseReserva.FechaPrevistaIngreso;
				SqlListado = "SELECT ARESINGRVA.gplantas, ARESINGRVA.ghabitac, ARESINGRVA.gcamasbo, ARESINGRVA.iregaloj,ARESINGRVA.fpreving,ARESINGRVA.cfianza, " +
                             " DPACIENT.dnombpac , DPACIENT.dape1pac, DPACIENT.dape2pac,DPERSONA.dnompers,DPERSONA.dap1pers, ARESINGRVA.fatencion, sconsglo.valfanu1 " + 
				             " From ARESINGRVA,DPACIENT,SUSUARIO,DPERSONA,sconsglo " + 
				             " WHERE DPACIENT.GIDENPAC=ARESINGRVA.GIDENPAC" + 
				             "    AND FPREVING=" + Serrores.FormatFechaHM(tempRefParam) + "" + 
				             "    AND ARESINGRVA.GUSUARIO=SUSUARIO.GUSUARIO AND SUSUARIO.GPERSONA=DPERSONA.GPERSONA" + 
				             "    AND ganoregi = " + claseReserva.idReserva.Substring(0, Math.Min(claseReserva.idReserva.IndexOf('/'), claseReserva.idReserva.Length)).Trim() + 
				             "    AND gnumregi = " + claseReserva.idReserva.Substring(claseReserva.idReserva.IndexOf('/') + 1).Trim() + 
				             "    AND gconsglo ='ilitresv'";
				//------------------

				// hay que cambiar el destino del informe y la ubicacion
				if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
				{
					Conexion_Base_datos.proCabCrystal();
				}

                ListadoParticular.ReportFileName = Conexion_Base_datos.PathString + "\\RPT\\agr021r1_CR11.rpt";
                ListadoParticular.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                ListadoParticular.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                ListadoParticular.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                ListadoParticular.Formulas["{@FORM4}"] = "\"" + ListadoDocumentos.ToString() + "\"";
                //oscar D
                SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlListado, Conexion_Base_datos.RcAdmision);
				Rrsql = new DataSet();
				tempAdapter.Fill(Rrsql);
				if (Rrsql.Tables[0].Rows.Count != 0)
				{
					if (Convert.ToString(Rrsql.Tables[0].Rows[0]["iregaloj"]).Trim() == "C")
					{
                        ListadoParticular.Formulas["{@FORM5}"] = "\"Doble\"";
                    }
                    else
					{
                        ListadoParticular.Formulas["{@FORM5}"] = "\"Individual\"";
                    }

                    System.DateTime TempDate = DateTime.FromOADate(0);

                    ListadoParticular.Formulas["{@Fecha_hora}"] = "\"" + ((DateTime.TryParse(Convert.ToString(Rrsql.Tables[0].Rows[0]["fpreving"]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy  HH:mm") : Convert.ToString(Rrsql.Tables[0].Rows[0]["fpreving"]).Trim()) + "\"";

                    if (!Convert.IsDBNull(Rrsql.Tables[0].Rows[0]["cfianza"]))
					{
						if (Conversion.Val(Convert.ToString(Rrsql.Tables[0].Rows[0]["cfianza"])) > 0 && Convert.ToString(Rrsql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
						{
                            ListadoParticular.Formulas["{@Deposito}"] = "\"Deposito a realizar al ingreso : " + Convert.ToString(Rrsql.Tables[0].Rows[0]["cfianza"]) + " Euros." + "\"";
                        }
                    }
				}

				Rrsql.Close();
                //***********

                ////DIEGO - 07/03/2007 - Si hay logo se pinta una cabecera en el RPT, y si no la otra
                ListadoParticular.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";

                
                /* INIC SDSALAZAR_NOTODO_X_4*/
                string strDSN = String.Empty;
				string NOMBRE_DSN_ACCESS = String.Empty;

				Serrores.Datos_Conexion(Conexion_Base_datos.RcAdmision);

				Serrores.CrearCadenaDSN(ref strDSN, ref NOMBRE_DSN_ACCESS);

                ListadoParticular.SubreportToChange = "LogotipoIDC";
                ListadoParticular.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                ListadoParticular.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                ListadoParticular.SubreportToChange = "";
                ///DIEGO - 07/03/2007 - Si hay logo se pinta una cabecera en el RPT, y si no la otra


                //ListadoParticular.ReportFileName = PathString & "\RPT\agr021r1.rpt"
                //ListadoParticular.Destination = crptToWindow

                ListadoParticular.Destination = Crystal.DestinationConstants.crptToPrinter;


                ListadoParticular.WindowTitle = "Hoja de Reserva";
                ListadoParticular.CopiesToPrinter = (short)VentanaImpresionPrint.PrinterSettings.Copies;
                ListadoParticular.SQLQuery = SqlListado;
                ListadoParticular.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                //ListadoParticular.Connect = Conexion_Base_datos.RcAdmision.ConnectionString;

                ListadoParticular.Action = 1;
                ListadoParticular.Reset();
                VentanaImpresionPrint.PrinterSettings.Copies = 1;
                ListadoParticular = null;
                VentanaImpresionPrint = null;
                this.Cursor = Cursors.Default;
            }
            catch (SqlException e)
			{
                ListadoParticular.Reset();
                ListadoParticular = null;
                this.Cursor = Cursors.Default;
                if (e.Number == 32755 || e.Number == 20545 || e.Number == 28663)
				{ // error producido por pulsar cancelar en el commondialog
					// o al estar imprimiendo y error producido por no haber impresoras instaladas (28663)
					return;
				}
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "docu_reservas:proImprimirHoja", e);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void Activar_Imprimir()
		{
			//If chbHojaInformacion Or chbContrato Then
			cbAceptar.Enabled = true;
			//Else
			//    cbAceptar.Enabled = False
			//End If
		}



		private void LsbLista1_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsUno_Click(CbInsUno[0], new EventArgs());

		}

		private void LsbLista2_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelUno_Click(CbDelUno[0], new EventArgs());

		}


		//oscar 22/11/2004
		// imprime la hoja de informacion de la reserva en cuestion con nuevo formato
		private void proImprimirHojaFormatoNuevo()
		{
			try
			{
				string SqlListado = String.Empty;
				StringBuilder ListadoDocumentos = new StringBuilder();
				object VentanaImpresion = null; //simula el commondialog
				DataSet Rrsql = null;


				ListadoDocumentos = new StringBuilder("");
				//if (LsbLista2.GetListItem(0) == "")
                if(LsbLista2.SelectedItems.Count > 0)
                {
                     if (LsbLista2.Items[0].Text == "")
                     {
					    ListadoDocumentos = new StringBuilder("Ninguno");
				     }
				    else
				    {
					    //ListadoDocumentos = new StringBuilder("-  " + LsbLista2.GetListItem(0));
                        ListadoDocumentos = new StringBuilder("-  " + LsbLista2.Items[0].Value.ToString());
                        for (viIn = 1; viIn <= LsbLista2.Items.Count - 1; viIn++)
					    { // Bucle por la lista.
						    //ListadoDocumentos.Append("\" + Chr(10) + \"" + "-  " + LsbLista2.GetListItem(viIn));
                            ListadoDocumentos.Append("\" + Chr(10) + \"" + "-  " + LsbLista2.Items[viIn].Value.ToString());
                        }
				    }
                }
                else
                {
                    ListadoDocumentos = new StringBuilder("Ninguno");
                }

                //Set VentanaImpresion = menu_admision.CommDiag_impre
                //VentanaImpresion.CancelError = True
                //VentanaImpresion.ShowPrinter

                object tempRefParam = claseReserva.FechaPrevistaIngreso;
				SqlListado = "SELECT ARESINGRVA.fpreving, ARESINGRVA.gplantas, ARESINGRVA.ghabitac, ARESINGRVA.gcamasbo, " + 
				             "  ARESINGRVA.gdiaging, ARESINGRVA.odiaging," + 
				             "  ARESINGRVA.finterve, ARESINGRVA.ndiasest, ARESINGRVA.oobserva, isnull(ARESINGRVA.cfianza,0) as cfianza," + 
				             "  DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.fnacipac, DPACIENT.itipsexo, " + 
				             "  DSERVICI.dnomserv," + 
				             "  DPERSONA.dnompers, DPERSONA.dap1pers, DPERSONA.dap2pers," + 
				             "  HDOSSIER.ghistoria," + 
				             "  DSOCIEDA.dsocieda, DINSPECC.dinspecc, DENTIPAC.nafiliac, DDIAGNOS.dnombdia" + 
				             " FROM " + 
				             " ARESINGRVA " + 
				             " INNER JOIN DPACIENT ON ARESINGRVA.gidenpac = DPACIENT.gidenpac" + 
				             " INNER JOIN DSOCIEDA ON ARESINGRVA.gsocieda = DSOCIEDA.gsocieda" + 
				             " LEFT JOIN DINSPECC ON ARESINGRVA.ginspecc = DINSPECC.ginspecc" + 
				             " LEFT JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac" + 
				             " LEFT JOIN DSERVICI ON ARESINGRVA.gservici = DSERVICI.gservici" + 
				             " LEFT JOIN DPERSONA ON ARESINGRVA.gpersona = DPERSONA.gpersona" + 
				             " LEFT JOIN DDIAGNOS ON ARESINGRVA.gdiaging = DDIAGNOS.gcodidia" + 
				             " LEFT JOIN DENTIPAC ON ARESINGRVA.gsocieda = DENTIPAC.gsocieda" + 
				             "                   AND ARESINGRVA.gidenpac = DENTIPAC.gidenpac" + 
				             " WHERE " + 
				             " ARESINGRVA.FPREVING=" + Serrores.FormatFechaHM(tempRefParam) + " AND " + 
				             " ARESINGRVA.ganoregi=" + claseReserva.idReserva.Substring(0, Math.Min(claseReserva.idReserva.IndexOf('/'), claseReserva.idReserva.Length)).Trim() + " AND " + 
				             " ARESINGRVA.gnumregi=" + claseReserva.idReserva.Substring(claseReserva.idReserva.IndexOf('/') + 1).Trim();
				//" DPACIENT.GIDENPAC='" & claseReserva.IdentificadorPaciente & "'"

				if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
				{
					Conexion_Base_datos.proCabCrystal();
				}
                ListadoParticular.ReportFileName = Conexion_Base_datos.PathString + "\\RPT\\agr022r1_CR11.rpt";
                ListadoParticular.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                ListadoParticular.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                ListadoParticular.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlListado, Conexion_Base_datos.RcAdmision);
				Rrsql = new DataSet();
				tempAdapter.Fill(Rrsql);

                ListadoParticular.Formulas["{@cama}"] = "\"" + StringsHelper.Format(Rrsql.Tables[0].Rows[0]["gplantas"], "00") +
                                               StringsHelper.Format(Rrsql.Tables[0].Rows[0]["ghabitac"], "00") +
                                               Convert.ToString(Rrsql.Tables[0].Rows[0]["gcamasbo"]) + "\"";

                if (mbAcceso.ObtenerCodigoSConsglo("ILITRESV", "VALFANU1", Conexion_Base_datos.RcAdmision).Trim().ToUpper() == "S")
				{
					string tempRefParam2 = Convert.ToString(Rrsql.Tables[0].Rows[0]["cfianza"]);
					string tempRefParam3 = Serrores.ObtenerSeparadorDecimalBD(Conexion_Base_datos.RcAdmision);
					ListadoDocumentos.Append("\" + Chr(10) + \"" + "-  DEP�SITO: ..... " + Serrores.ConvertirDecimales(ref tempRefParam2, ref tempRefParam3, 2) + " Euros");
				}

                Rrsql.Close();

                ListadoParticular.Formulas["{@documentacion}"] = "\"" + ListadoDocumentos.ToString() + "\"";


                ////DIEGO - 07/03/2007 - Si hay logo se pinta una cabecera en el RPT, y si no la otra
                ListadoParticular.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";

                
                string strDSN = String.Empty;
				string NOMBRE_DSN_ACCESS = String.Empty;

				Serrores.Datos_Conexion(Conexion_Base_datos.RcAdmision);

				Serrores.CrearCadenaDSN(ref strDSN, ref NOMBRE_DSN_ACCESS);


                ListadoParticular.SubreportToChange = "LogotipoIDC";
                ListadoParticular.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                ListadoParticular.Connect = "" + strDSN + ";" + Serrores.VVstUsuarioBD + ";" + Serrores.VVstPasswordBD + "";
                ListadoParticular.SubreportToChange = "";


                // DIEGO - 07 / 03 / 2007 - Si hay logo se pinta una cabecera en el RPT, y si no la otra



                ListadoParticular.Destination = Crystal.DestinationConstants.crptToWindow;
                ListadoParticular.WindowState = Crystal.WindowStateConstants.crptMaximized;

                ListadoParticular.WindowTitle = "Hoja de Reserva";
                //ListadoParticular.CopiesToPrinter = VentanaImpresion.Copies
                ListadoParticular.SQLQuery = SqlListado;
                ListadoParticular.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a;

                ListadoParticular.Action = 1;
                ListadoParticular.Reset();
                //VentanaImpresion.Copies = 1

                ListadoParticular = null;
                VentanaImpresion = null;
                this.Cursor = Cursors.Default;

            }
            catch (SqlException e)
			{
                ListadoParticular.Reset();
                ListadoParticular = null;
                this.Cursor = Cursors.Default;

                if (e.Number == 32755 || e.Number == 20545 || e.Number == 28663)
				{
					return;
				}
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "docu_reservas:proImprimirHojaFormatoNuevo", e);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		private void docu_reservas_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}