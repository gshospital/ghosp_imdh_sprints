using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using System.Drawing;
using Telerik.WinControls;
using LlamarOperacionesFMS;
using AltaMedica;
using Notificaciones;
using CrystalWrapper;

namespace ADMISION
{
    public partial class Documentos_alta
        : Telerik.WinControls.UI.RadForm
    {

        string stGidenpac = String.Empty;
        string stNombre = String.Empty;
        string stApellido1 = String.Empty;
        string stApellido2 = String.Empty;
        string stCodigosoc = String.Empty;
        string StSociedad = String.Empty;
        string stNasegurado = String.Empty;
        string stHistoria = String.Empty;
        string stDNI = String.Empty;
        string stTemp = String.Empty;
        string stFecha_Llegada = String.Empty;
        string stFecha_ALTA = String.Empty;
        int iServicio_ALTA = 0;
        string stCalle = String.Empty;
        string stPoblacion = String.Empty;
        string stCodigo_Postal = String.Empty;
        string stProvincia = String.Empty;
        string tNombre = String.Empty;
        int vstAnoadme = 0;
        int vstNumadme = 0;

        Crystal.DestinationConstants Parasalida = (Crystal.DestinationConstants)0;      
        public Crystal cryInformes = new Crystal();
        int viform = 0;
        bool fEstaDadodeAlta = false;
        public Documentos_alta()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
            ReLoadForm(false);
        }

        public void pro_oficio()
        {
            object chbAsistencia = null;
            string sqlCrystal = String.Empty;

            DataSet rrAcompas = null;
            string nomprovi = String.Empty;
            DLLTextosSql.TextosSql oTextoSql = null;

            try
            {
                cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi580r1_CR11.rpt";

                sqlCrystal = " select dnomprov from sconsglo, dprovinc  where" + " sconsglo.gconsglo  = 'CODPROVI' and dprovinc.gprovinc = sconsglo.valfanu1";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCrystal, Conexion_Base_datos.RcAdmision);
                rrAcompas = new DataSet();
                tempAdapter.Fill(rrAcompas);
                if (rrAcompas.Tables[0].Rows.Count != 0)
                {
                    nomprovi = Convert.ToString(rrAcompas.Tables[0].Rows[0]["dnomprov"]).Trim();
                }
                else
                {
                    nomprovi = "";
                }

                //     sqlCrystal = " SELECT AEPISADM.fllegada, AEPISADM.faltplan, " _
                //'    & "DMOTALTA.dmotalta,DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac," _
                //'    & "DPOBLACI.DPOBLACI" _
                //'            & " From " _
                //'            & " AEPISADM, DPACIENT, dmotalta, DPOBLACI " _
                //'            & " Where " _
                //'            & " AEPISADM.gnumadme =" & vstNumadme & " AND " _
                //'            & " AEPISADM.ganoadme =" & vstAnoadme & " and " _
                //'            & " AEPISADM.gidenpac = DPACIENT.gidenpac and " _
                //'            & " aepisadm.gmotalta = dmotalta.gmotalta and " _
                //'            & " dpacient.gpobnaci *= dpoblaci.gpoblaci "

                object[] MatrizSql = new object[3];
                MatrizSql[1] = vstAnoadme;
                MatrizSql[2] = vstNumadme;
                oTextoSql = new DLLTextosSql.TextosSql();
                sqlCrystal = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, "ADMISION", "AGI220F1", "pro_oficio", 1, MatrizSql, Conexion_Base_datos.RcAdmision));
                oTextoSql = null;

                //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //*******************
                cryInformes.Formulas["{@FOR1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FOR2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@FOR3}"] = "\"" + nomprovi + "\"";
                cryInformes.Formulas["{@FOR4}"] = "\"" + nomprovi + "\"";
                cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";

                cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                cryInformes.SubreportToChange = "LogotipoIDC";
                cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                cryInformes.SubreportToChange = "";

                cryInformes.WindowTitle = "Oficio";
                cryInformes.SQLQuery = sqlCrystal;
                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                cryInformes.Destination = Parasalida;
                cryInformes.WindowShowPrintSetupBtn = true;
                cryInformes.Action = 1;
                cryInformes.PrinterStopPage = cryInformes.PageCount;
                cryInformes.Reset();
                Conexion_Base_datos.vacia_formulas(cryInformes, 6);
                Chboficio.Checked = false;
            }
            catch
            {
                //string[] tempRefParam7 = new string[] { chbAsistencia.Caption };
                string[] tempRefParam7 = new string[] { "" };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1100, Conexion_Base_datos.RcAdmision, tempRefParam7));
                //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
            }
        }

        private void proAlta_Medica()
        {
            mcAlta_Medica iAlt = new AltaMedica.mcAlta_Medica();
            iAlt.proAltaMedica(vstAnoadme, vstNumadme, cryInformes, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.PathString, Conexion_Base_datos.VstCrystal);
            iAlt = null;
            chbAltaMedica.CheckState = CheckState.Unchecked;
            cbImprimir.Enabled = false;
        }

        // ************* FUNCIONES DE VUELTA DE I-FMS ******
        public object RecogerDatos(object[] tab1, object[] tab2)
        {
            return null;
        }

        public object ActualizarConsulta(bool valor)
        {
            return null;
        }

        private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
        {
            object Clase = null;
            string StJustificanteIngreso = String.Empty;
            object Otros_Centros = null;
            object oParteIncap = null;
            DataSet Cursor_Renamed = null;
            string stSql = String.Empty;
            string stGinspecc = String.Empty;
            object instancia = null;
            string stConexion = String.Empty;
            bool bAcceso = false;

            try
            {
                bool bFirmaDigital = false;
                bFirmaDigital = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FIRMADIG", "VALFANU1") == "S";

                Conexion_Base_datos.Cuadro_Diag_imprePrint = menu_admision.DefInstance.CommDiag_imprePrint;

                bAcceso = false;

                cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;

                stConexion = "Description= BASE TEMPORAL EN ACCESS" + "\r" + "Oemtoansi=no" + "\r" + "server = (local)" + "\r" + "dbq=" + Conexion_Base_datos.StBaseTemporal + "";

                string stsqltemp = String.Empty;
                DataSet rrtemp = null;

                StJustificanteIngreso = "I";

                stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'IMPJUSIN' ";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    StJustificanteIngreso = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
                }
                rrtemp.Close();

                this.Cursor = Cursors.WaitCursor;
                if (ChbAlta.CheckState == CheckState.Checked || ChbAltavolun.CheckState == CheckState.Checked || chbFactura.CheckState == CheckState.Checked || ChbVolante.CheckState == CheckState.Checked || ChbMedico.CheckState == CheckState.Checked || chbFacturaSombra.CheckState == CheckState.Checked || chbAltaMedica.CheckState == CheckState.Checked || ChBHojaReferencia.CheckState == CheckState.Checked || Chboficio.CheckState == CheckState.Checked || chbHojaClinEsta.CheckState == CheckState.Checked || chbFactSombDocu.CheckState == CheckState.Checked || chbVolanteSociedad.CheckState == CheckState.Checked || (ChbHaceconstar.CheckState == CheckState.Checked && (!bFirmaDigital || !bsFirmaDigitalizada.fExistePDFCreator())) || ChbAltaInc.CheckState == CheckState.Checked || ChbFallecInc.CheckState == CheckState.Checked)
                {
                    Conexion_Base_datos.proDialogo_impre(cryInformes);
                }

                if (ChbJustificante.CheckState == CheckState.Checked && StJustificanteIngreso == "I")
                {
                    Conexion_Base_datos.proDialogo_impre(cryInformes);
                }

                if (ChbAlta.CheckState == CheckState.Checked)
                { // Han seleccinado imprimir el INFORME DE ALTA
                    proInforme_Alta();
                }

                if (ChbAltavolun.CheckState == CheckState.Checked)
                { // Han seleccinado imprimir el INFORME DE ALTA VOLUNTARIA
                    bAcceso = true;
                    proInforme_Alta_Volun();
                }
                if (chbFactura.CheckState == CheckState.Checked)
                {
                    // Han seleccionado imprimir la FACTURA
                    string tempRefParam = "FAPR1";
                    proFactura(tempRefParam);
                }
                if (ChbVolante.CheckState == CheckState.Checked)
                {
                    // Han seleccionado imprimir el VOLANTE DE AMBULANCIA
                    proVolante();
                }
                if (ChbMedico.CheckState == CheckState.Checked)
                {
                    // Han seleccionado imprimir el INFORME MEDICO AL ALTA
                    proMedico();
                }
                if (ChbJustificante.CheckState == CheckState.Checked)
                {
                    //Comprobar si se ha de autorizar a imprimir el justificante del SERMAS
                    stSql = "select valfanu1 from SCONSGLO where gconsglo ='JUSERMAS' and valfanu1='S'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    Cursor_Renamed = new DataSet();
                    tempAdapter_2.Fill(Cursor_Renamed);
                    if (Cursor_Renamed.Tables[0].Rows.Count == 1)
                    {
                        /*INDRA jproche TODO X_4
                        Clase = new DocumentoDLL.mcDocumento();
                        //UPGRADE_TODO: (1067) Member proConexion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                        */
                        if (StJustificanteIngreso == "I")
                        {
                            //                Clase.proJustificanteSERMAS stTGidenpac, "H", viGanoadme, viGnumadme, Cuadro_Diag_impre.Copies, "P", Me, VstCrystal, crptToPrinter
                            /*INDRA jproche TODO X_4
                           //UPGRADE_TODO: (1067) Member proJustificanteSERMAS is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                           Clase.proJustificanteSERMAS(stGidenpac, "H", vstAnoadme, vstNumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "P", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToPrinter);
                           */
                        }
                        else
                        {
                            //                Clase.proJustificanteSERMAS stTGidenpac, "H", viGanoadme, viGnumadme, Cuadro_Diag_impre.Copies, "P", Me, VstCrystal, crptToWindow
                            /*INDRA jproche TODO X_4
                           //UPGRADE_TODO: (1067) Member proJustificanteSERMAS is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                           Clase.proJustificanteSERMAS(stGidenpac, "H", vstAnoadme, vstNumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "P", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToWindow);
                           */
                        }

                        Clase = null;
                    }
                    else
                    {
                        if (StJustificanteIngreso == "I")
                        {
                            Conexion_Base_datos.proJustificante(cryInformes, Crystal.DestinationConstants.crptToPrinter, this, vstNumadme, vstAnoadme, stGidenpac, Conexion_Base_datos.Cuadro_Diag_imprePrint);
                        }
                        else
                        {
                            Conexion_Base_datos.proJustificante(cryInformes, Crystal.DestinationConstants.crptToWindow, this, vstNumadme, vstAnoadme, stGidenpac, Conexion_Base_datos.Cuadro_Diag_imprePrint);
                        }
                        //proJustificante cryInformes, Parasalida, Me, vstNumadme, vstAnoadme, stGidenpac
                    }
                    Cursor_Renamed = null;
                }

                if (ChbJusAcompa.CheckState == CheckState.Checked)
                { // Han seleccinado imprimir el JUSTIFICANTE DEL ACOMPA�ANTE
                    if (StJustificanteIngreso == "I")
                    {
                        //Conexion_Base_datos.Cuadro_Diag_imprePrint.CancelError = true;
                        Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
                        Conexion_Base_datos.Cuadro_Diag_imprePrint.ShowDialog();
                    }

                    //Comprobar si se ha de autorizar a imprimir el justificante del SERMAS
                    stSql = "select valfanu1 from SCONSGLO where gconsglo ='JUSERMAS' and valfanu1='S'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    Cursor_Renamed = new DataSet();
                    tempAdapter_3.Fill(Cursor_Renamed);
                    if (Cursor_Renamed.Tables[0].Rows.Count == 1)
                    {
                        /*INDRA jproche TODO X_4
                        Clase = new DocumentoDLL.mcDocumento();
                        //UPGRADE_TODO: (1067) Member proConexion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                        */
                        if (StJustificanteIngreso == "I")
                        {
                            /*INDRA jproche TODO X_4
                            //UPGRADE_TODO: (1067) Member proJustificanteSERMAS is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                            Clase.proJustificanteSERMAS(stGidenpac, "H", vstAnoadme, vstNumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "A", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToPrinter);
                            */
                        }
                        else
                        {
                            /*INDRA jproche TODO X_4
                            //UPGRADE_TODO: (1067) Member proJustificanteSERMAS is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                            Clase.proJustificanteSERMAS(stGidenpac, "H", vstAnoadme, vstNumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "A", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToWindow);
                            */
                        }
                        Clase = null;
                    }
                    else
                    {
                        if (StJustificanteIngreso == "I")
                        {
                            Conexion_Base_datos.proAcompa(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter, vstAnoadme, vstNumadme, cryInformes, this);
                        }
                        else
                        {
                            Conexion_Base_datos.proAcompa(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToWindow, vstAnoadme, vstNumadme, cryInformes, this);
                        }
                    }

                    Cursor_Renamed = null;
                    ChbJusAcompa.CheckState = CheckState.Unchecked;
                }

                if (this.chbFacturaSombra.CheckState == CheckState.Checked)
                {
                    string tempRefParam2 = "FAFS1";
                    proFactura(tempRefParam2);
                }

                if (this.chbAltaMedica.CheckState == CheckState.Checked)
                {
                    proAlta_Medica();
                }

                if (ChBHojaReferencia.CheckState == CheckState.Checked)
                {
                    //imprimir pacientes de traslado
                    bAcceso = true;
                    ReferenciaDLL.Referencia referenciadll = new ReferenciaDLL.Referencia();
                    referenciadll.proLlamada(this, referenciadll, "H", stGidenpac, vstAnoadme.ToString(), vstNumadme.ToString(), Conexion_Base_datos.RcAdmision, cryInformes, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ELEMENTOSCOMUNES\\RPT", Conexion_Base_datos.gUsuario, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo, 1, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a);
                    referenciadll = null;
                    ChBHojaReferencia.CheckState = CheckState.Unchecked;
                }

                if (Chboficio.IsChecked)
                {
                    pro_oficio();
                }

                if (chbHojaClinEsta.Visible)
                {
                    if (chbHojaClinEsta.CheckState == CheckState.Checked)
                    {
                        Conexion_Base_datos.proHojaClinEsta(Conexion_Base_datos.Cuadro_Diag_imprePrint, this, cryInformes, chbHojaClinEsta, vstAnoadme, vstNumadme, stGidenpac);
                    }
                }

                if (chbIncapacidad.CheckState == CheckState.Checked)
                { // Han seleccinado imprimir el PARTE DE INCAPACIDAD
                    bAcceso = true;
                    proIncapacidad(cryInformes, Conexion_Base_datos.Cuadro_Diag_imprePrint);
                }

                if (chbFactSombDocu.CheckState == CheckState.Checked)
                {
                    stSql = "select ginspecc from amovifin where itiposer='H' and ganoregi = " + vstAnoadme.ToString() + " and gnumregi = " + vstNumadme.ToString() + " order by fmovimie desc ";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    Cursor_Renamed = new DataSet();
                    tempAdapter_4.Fill(Cursor_Renamed);
                    if (Cursor_Renamed.Tables[0].Rows.Count != 0)
                    {
                        if (!Convert.IsDBNull(Cursor_Renamed.Tables[0].Rows[0]["ginspecc"]))
                        {
                            stGinspecc = Convert.ToString(Cursor_Renamed.Tables[0].Rows[0]["ginspecc"]);
                        }
                    }

                    Cursor_Renamed = null;
                    /*INDRA jproche TODO X_4
                    Otros_Centros = new FactSombDocuDll.FactSombClase();
                    //UPGRADE_TODO: (1067) Member proLlamada is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    Otros_Centros.proLlamada(this, stGidenpac, "H", vstAnoadme.ToString(), vstNumadme.ToString(), Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, stCodigosoc, stGinspecc);
                    */
                    Otros_Centros = null;
                    chbFactSombDocu.CheckState = CheckState.Unchecked;
                }

                //oscar 15/09/03
                if (chbVolanteSociedad.CheckState == CheckState.Checked)
                { //imprime Volante Sociedad
                    /*INDRA jproche TODO X_4
                    instancia = new DocumentoDLL.mcDocumento();
                    //UPGRADE_TODO: (1067) Member proConexion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    instancia.proConexion(this, Conexion_Base_datos.RcAdmision, cryInformes, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                    //UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control Cuadro_Diag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx
                    //UPGRADE_TODO: (1067) Member chbVolanteSociedad_ALTA is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    instancia.chbVolanteSociedad_ALTA(vstAnoadme, vstNumadme, stGidenpac, Conexion_Base_datos.Cuadro_Diag_impre, Conexion_Base_datos.DNICEDULA);
                    */
                    instancia = null;
                    chbVolanteSociedad.CheckState = CheckState.Unchecked;
                }

                if (ChbHaceconstar.CheckState == CheckState.Checked)
                { //imprime el Compromiso de pago
                    /*INDRA jproche TODO X_4
                    instancia = new DocumentoDLL.mcDocumento();
                    //UPGRADE_TODO: (1067) Member proConexion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    instancia.proConexion(this, Conexion_Base_datos.RcAdmision, cryInformes, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                    //UPGRADE_ISSUE: (2067) MSComDlg.CommonDialog control Cuadro_Diag_impre was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2067.aspx
                    //UPGRADE_TODO: (1067) Member proConstar_ALTA is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    instancia.proConstar_ALTA(vstAnoadme, vstNumadme, stGidenpac, Conexion_Base_datos.Cuadro_Diag_impre, Conexion_Base_datos.DNICEDULA.ToUpper());
                    */
                    instancia = null;
                    ChbHaceconstar.CheckState = CheckState.Unchecked;
                    if (bFirmaDigital)
                    {
                        if (Conexion_Base_datos.vImpre_defecto != "")
                        {
                            Serrores.proEstablecerImpresoraPredeterminada(Conexion_Base_datos.vImpre_defecto);
                        }
                        this.Show();
                    }
                }
                //---------------

                //oscar 22/03/04

                Notificaciones.classNotificaciones claseNotificaciones = null;
                if (ChbAltaInc.CheckState == CheckState.Checked || ChbFallecInc.CheckState == CheckState.Checked)
                {
                    claseNotificaciones = new Notificaciones.classNotificaciones();

                    claseNotificaciones.RecogerParametrosComunes(this, Conexion_Base_datos.RcAdmision, cryInformes, vstNumadme.ToString(), vstAnoadme.ToString(), stGidenpac, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ELEMENTOSCOMUNES\\RPT", Conexion_Base_datos.VstCrystal, Conexion_Base_datos.Cuadro_Diag_imprePrint);


                    if (ChbAltaInc.CheckState == CheckState.Checked)
                    {
                        //UPGRADE_TODO: (1067) Member proImprimirAltaIncapacitado is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        claseNotificaciones.proImprimirAltaIncapacitado(); // notificacion al juzgado del alta
                        ChbAltaInc.CheckState = CheckState.Unchecked;
                    }

                    if (ChbFallecInc.CheckState == CheckState.Checked)
                    {
                        //UPGRADE_TODO: (1067) Member proImprimirFallecimientoIncapacitado is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        claseNotificaciones.proImprimirFallecimientoIncapacitado();
                        ChbFallecInc.CheckState = CheckState.Unchecked;
                    }

                    claseNotificaciones = null;
                }
                //----------
                if (stGidenpac.Trim() != "" && bAcceso)
                {
                    Serrores.GrabarDCONSUHC(stGidenpac, "S", Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision);
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                this.Cursor = Cursors.Default;
                if (Information.Err().Number == 32755)
                {
                    //se ha pulsado cancelar en el cuadro de dialogo imprimir
                }
                else
                {
                    Otros_Centros = null;
                    RadMessageBox.Show(e.Message, Application.ProductName);
                }
            }
        }

        //********************************************************************************************************
        //*                                                                                                      *
        //*  Modificaciones:                                                                                     *
        //*                                                                                                      *
        //*      O.Frias 27/02/2007 Incorporacion de los campos (Cama, Diagn�stico, Fecha Ingreso y Sociedad)    *
        //*                                                                                                      *
        //*                                                                                                      *
        //********************************************************************************************************
        public void proInforme_Alta()
        {
            string sql = String.Empty;
            try
            {
                DataSet Cursor_Renamed = null;
                bool iExistAsegu = false;
                string LitPersona = String.Empty;
                string StrHospitalaria = String.Empty;
                string HospitalOCentro = String.Empty;
                string MaysculaOMinuscula = String.Empty;
                string FechaAlta = String.Empty;

                Serrores.Obtener_Multilenguaje(Conexion_Base_datos.RcAdmision, stGidenpac);
                Serrores.ObtenerTextosRPT(Conexion_Base_datos.RcAdmision, "ADMISION", "AGI221R1");
                Serrores.ObtenerMesesSemanas(Conexion_Base_datos.RcAdmision);

                MaysculaOMinuscula = "m";
                iExistAsegu = false;

                // PROCEDIMIENTO PARA OBTENER EL INFORME DE ALTA

                //**************Comprueba si tiene datos sobre las cabeceras******************
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //****************************************************************************

                tNombre = stNombre + " " + stApellido1 + " " + stApellido2;
                //proFormulas viform   'limpio las formulas

                var sqlInnerJoin = "";

                if (Serrores.ExisteAseguradora(Conexion_Base_datos.RcAdmision))
                {
                    sqlInnerJoin = @"
,DPACIENT.gidenpac,DPACIENT.dnombpac,DPACIENT.dape1pac,DPACIENT.dape2pac,DPACIENT.fnacipac,DPACIENT.itipsexo,DPACIENT.ndninifp,DPACIENT.ddirepac,DPACIENT.gcodipos,DPACIENT.gprovinc,DPACIENT.gpoblaci,DPACIENT.dpoblaci,DPACIENT.gpaisres,DPACIENT.ntelefo1,DPACIENT.ntelefo2,DPACIENT.gestaciv,DPACIENT.gcatasoc,DPACIENT.gsocieda,DPACIENT.nafiliac,DPACIENT.iexitusp,DPACIENT.ffalleci,DPACIENT.fultmovi,DPACIENT.nextesi1,DPACIENT.nextesi2,DPACIENT.ifilprov,DPACIENT.iaututil,DPACIENT.ialermed,DPACIENT.oalermed,DPACIENT.ialerali,DPACIENT.oalerali,DPACIENT.ntarjeta,DPACIENT.iotraler,DPACIENT.ootraler,DPACIENT.GTIPOVIA,DPACIENT.DDOMICIL,DPACIENT.NNUMEVIA,DPACIENT.DOTROVIA,DPACIENT.gpobnaci,DPACIENT.iautopsi,DPACIENT.igrupabo,DPACIENT.ifactorh,DPACIENT.gususang,DPACIENT.npasapor,DPACIENT.ddiemail,DPACIENT.ntelefo3,DPACIENT.nextesi3,DPACIENT.gusuario,DPACIENT.fsistema,DPACIENT.dempresa,DPACIENT.iextranj,DPACIENT.iidioma,DPACIENT.ibloqueo,DPACIENT.OOBSERVA,DPACIENT.ircpavan,DPACIENT.ntarjsae,DPACIENT.ntrabaja,DPACIENT.gparetra,DPACIENT.gcatespe,DPACIENT.ddireext,DPACIENT.ncipauto,DPACIENT.ienvisms,DPACIENT.ocipauto,DPACIENT.gpaisnac,DPACIENT.ifilbasi,DPACIENT.gmotifil,DPACIENT.omotifil,DPACIENT.gorigfil,DPACIENT.iacticib,DPACIENT.inecacom,DPACIENT.fdepg1n1,DPACIENT.fdepg1n2,DPACIENT.fdepg2n1,DPACIENT.fdepg2n2,DPACIENT.fdepg3n1,DPACIENT.fdepg3n2,DPACIENT.oobsgdep,DPACIENT.gindicad,DPACIENT.gsubindi,DPACIENT.gdtropos,DPACIENT.ghospscs,DPACIENT.gciudada,DPACIENT.gmotbaja,DPACIENT.ffecbaja,DPACIENT.fcaducid,DPACIENT.gtramoCB,DPACIENT.gtiviaCB,DPACIENT.ootrviCB,DPACIENT.npunkmCB,DPACIENT.iverobse,DPACIENT.gcanotic,
DSOCIEDA.gsocieda,DSOCIEDA.dsocieda,DSOCIEDA.ccamasco,DSOCIEDA.iregaloj,DSOCIEDA.gtipofin,DSOCIEDA.fborrado,DSOCIEDA.imensual,DSOCIEDA.gtipoter,DSOCIEDA.icobefar,DSOCIEDA.iteam,DSOCIEDA.iCitaWeb,DSOCIEDA.FDESACTI,DSOCIEDA.ifafarma,DSOCIEDA.ireferi,DSOCIEDA.itrafico,DSOCIEDA.iobldiag
";
                }



                    sql = @" SELECT 

AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.gidenpac,AEPISADM.fllegada,AEPISADM.gservici,AEPISADM.gmotingr,AEPISADM.gpersona,AEPISADM.faltaadm,AEPISADM.gserulti,AEPISADM.gperulti,AEPISADM.gsitalta,AEPISADM.gmotalta,AEPISADM.ghospita,AEPISADM.gdiaging,AEPISADM.odiaging,AEPISADM.iconfide,AEPISADM.iotrocen,AEPISADM.irevisio,AEPISADM.ireingre,AEPISADM.ienfment,AEPISADM.iambulan,AEPISADM.gcauambu,AEPISADM.iregaloj,AEPISADM.itipingr,AEPISADM.nanoadms,AEPISADM.nnumadms,AEPISADM.fingplan,AEPISADM.faltplan,AEPISADM.gdiagalt,AEPISADM.odiagalt,AEPISADM.gdiasec1,AEPISADM.odiasec1,AEPISADM.gdiasec2,AEPISADM.odiasec2,AEPISADM.finterve,AEPISADM.gcumplim,AEPISADM.gmotause,AEPISADM.iacompan,AEPISADM.ganrelur,AEPISADM.gnurelur,AEPISADM.gusuingr,AEPISADM.gusualta,AEPISADM.ghospori,AEPISADM.dmediori,AEPISADM.dserdest,AEPISADM.gmottrac,AEPISADM.dnomcond,AEPISADM.dplacamb,AEPISADM.gempream,AEPISADM.gdiagini,AEPISADM.odiagini,AEPISADM.diasabse,AEPISADM.horaabse,AEPISADM.itraslad,AEPISADM.iestudio,AEPISADM.gcoeconc,AEPISADM.iambproc,AEPISADM.gtiplaza,AEPISADM.gproducto,AEPISADM.ocinmexi,AEPISADM.ocpriexi,AEPISADM.faviso,AEPISADM.gjuzgad1,AEPISADM.nnumexp1,AEPISADM.fresolu1,AEPISADM.gjuzgad2,AEPISADM.nnumexp2,AEPISADM.fresolu2,AEPISADM.fnotifincap,AEPISADM.fprevalt,AEPISADM.gsofarma,AEPISADM.ndiasest,AEPISADM.ndiaotrc,AEPISADM.fultnotif,AEPISADM.gusuprea,AEPISADM.gtiproce,AEPISADM.ganotrat,AEPISADM.gnumtrat,AEPISADM.iautoinf,AEPISADM.iautojus,AEPISADM.gresping,AEPISADM.iguialta,
DPERSONA.gpersona,DPERSONA.dnompers,DPERSONA.dap1pers,DPERSONA.dap2pers,DPERSONA.ccamasco,DPERSONA.fborrado,DPERSONA.imedico,DPERSONA.ienfermeria,DPERSONA.iauxsanitario,DPERSONA.gempresa,DPERSONA.gempleado,DPERSONA.dnif,DPERSONA.gsigla,DPERSONA.dvia,DPERSONA.dnumero1,DPERSONA.dnumero2,DPERSONA.descalera,DPERSONA.dpiso,DPERSONA.dpuerta,DPERSONA.dletra,DPERSONA.dcodpostal,DPERSONA.gmunicipio,DPERSONA.dbarrio,DPERSONA.gprovincia,DPERSONA.gnacion,DPERSONA.dprefijotfno,DPERSONA.dtfno,DPERSONA.dmatricula,DPERSONA.gseccion,DPERSONA.gctrabajo,DPERSONA.gconvenio,DPERSONA.gcategoria,DPERSONA.gcontrato,DPERSONA.gsubcontrato,DPERSONA.iextranjero,DPERSONA.itipodocex,DPERSONA.dnumdocex,DPERSONA.gnacionex,DPERSONA.falta,DPERSONA.fumodif,DPERSONA.iborrado,DPERSONA.ncolegia,DPERSONA.gtipopro,DPERSONA.icomadro,DPERSONA.icamille,DPERSONA.iadjunto,DPERSONA.imonitor,
DSERVICI.gservici,DSERVICI.dnomserv,DSERVICI.dabrserv,DSERVICI.iurgenci,DSERVICI.ihospita,DSERVICI.iconsult,DSERVICI.icentral,DSERVICI.ggrupser,DSERVICI.itiurmed,DSERVICI.ghojaurg,DSERVICI.iticomed,DSERVICI.ghojacon,DSERVICI.itiqumed,DSERVICI.ghojaqui,DSERVICI.itiurenf,DSERVICI.ghoj1ure,DSERVICI.ghoj2ure,DSERVICI.itihoenf,DSERVICI.ghoj1hoe,DSERVICI.ghoj2hoe,DSERVICI.ggrufunc,DSERVICI.fborrado,DSERVICI.iquirofa,DSERVICI.itiquenf,DSERVICI.ghoj1que,DSERVICI.ghoj2que,DSERVICI.iservrad,DSERVICI.iservlab,DSERVICI.GCECOSTE,DSERVICI.cservofi,DSERVICI.imedquir,DSERVICI.itipsexo,DSERVICI.cedadmin,DSERVICI.cedadmax,DSERVICI.almacen,DSERVICI.ipediadu,DSERVICI.icentdia,DSERVICI.dnomservi1,DSERVICI.dnomservi2,DSERVICI.oblitras,DSERVICI.epigrafe,DSERVICI.iotrprof,DSERVICI.irinterc,DSERVICI.ioblmotp,DSERVICI.iapespe,DSERVICI.irecimen,DSERVICI.ipriourg,DSERVICI.ggrupweb,DSERVICI.ioblproc,DSERVICI.isolinrx,DSERVICI.cservof2,DSERVICI.cservof3,DSERVICI.dnoserec,DSERVICI.gtiparea,DSERVICI.iobstetr

                " + sqlInnerJoin + @"
                FROM  AEPISADM, DPERSONA, DSERVICI ";

                if (Serrores.ExisteAseguradora(Conexion_Base_datos.RcAdmision))
                {
                    iExistAsegu = true;
                    sql = sql + ", DPACIENT, DSOCIEDA ";
                }

                sql = sql + " WHERE AEPISADM.gidenpac ='" + stGidenpac + "' AND " + " AEPISADM.fllegada =" + Serrores.FormatFechaHMS(stFecha_Llegada) + " AND " + " AEPISADM.gperulti = DPERSONA.gpersona  AND " + " AEPISADM.gserulti = DSERVICI.gservici ";

                if (iExistAsegu)
                {
                    sql = sql + " AND AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " DPACIENT.gsocieda = DSOCIEDA.gsocieda ";
                    //        Set Cursor = RcAdmision.OpenResultset(sql, 1, 1)
                }

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                Cursor_Renamed = new DataSet();
                tempAdapter.Fill(Cursor_Renamed);

                //Se mueve esta linea a este punto porque se debe definir antes de la asignacion de formulas
                cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi221r1_CR11.rpt";

                cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                cryInformes.Formulas["{@FORM4}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstTelefoHo + "\"";
                cryInformes.Formulas["{@NOMBRE}"] = "\"" + tNombre + "\"";
                cryInformes.Formulas["{@HISTORIA}"] = "\"" + stHistoria + "\"";
                cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";

                if (iExistAsegu)
                {
                    //        cryInformes.Formulas(7) = "strFILIAC = 'N� ASEGURADO:'"
                    cryInformes.Formulas["{@strFILIAC}"] = "\"" + Serrores.VarRPT[5] + "\"";
                    cryInformes.Formulas["{@numFILIAC}"] = "\"" + Convert.ToString(Cursor_Renamed.Tables[0].Rows[0]["nafiliac"]).Trim() + "\"";
                    //cryInformes.set_Formulas(9, "strASEGUR = \"" + Serrores.VarRPT[6] + "\"");
                    cryInformes.Formulas["{@strASEGUR}"] = "\"" + Serrores.VarRPT[6] + "\"";
                    //cryInformes.set_Formulas(10, "desASEGUR = \"" + Convert.ToString(Cursor_Renamed.Tables[0].Rows[0]["dsocieda"]).Trim() + "\"");
                    cryInformes.Formulas["{@desASEGUR}"] = "\"" + Convert.ToString(Cursor_Renamed.Tables[0].Rows[0]["dsocieda"]).Trim() + "\"";
                    //Cursor.Close
                    iExistAsegu = false;
                }
                else
                {
                    cryInformes.Formulas["{@strFILIAC}"] = "\"" + "\"";
                    cryInformes.Formulas["{@numFILIAC}"] = "\"" + "\"";
                    cryInformes.Formulas["{@strASEGUR}"] = "\"" + "\"";
                    cryInformes.Formulas["{@desASEGUR}"] = "\"" + "\"";
                }
                cryInformes.Formulas["{@FechaHoy}"] = "\"" + Serrores.CapturarFechaHoy(MaysculaOMinuscula) + "\"";
                //JavierC

                System.DateTime TempDate = DateTime.FromOADate(0);
                cryInformes.Formulas["{@strFECHAALTA}"] = "\"" + ((DateTime.TryParse(stFecha_ALTA, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:NN") : stFecha_ALTA) + "\"";
                //***************** O.Frias 27/02/2007 *****************
                if (fNewCampos())
                {
                    cryInformes.Formulas["{@LitCama}"] = "\"" + Serrores.VarRPT[7] + "\"";
                    cryInformes.Formulas["{@NumCama}"] = "\"" + fCama(Convert.ToInt32(Cursor_Renamed.Tables[0].Rows[0]["Ganoadme"]), Convert.ToInt32(Cursor_Renamed.Tables[0].Rows[0]["Gnumadme"])) + "\"";
                    cryInformes.Formulas["{@Diagnostico}"] = "\"" + Serrores.VarRPT[8] + "\"";

                    if (!Convert.IsDBNull(Cursor_Renamed.Tables[0].Rows[0]["gdiagalt"]))
                    {
                        cryInformes.Formulas["{@TextDiagnos}"] = "\"" + fDiagnostico(Convert.ToString(Cursor_Renamed.Tables[0].Rows[0]["gdiagalt"]), Convert.ToDateTime(Cursor_Renamed.Tables[0].Rows[0]["faltplan"])) + "\"";
                    }
                    else
                    {
                        if (!Convert.IsDBNull(Cursor_Renamed.Tables[0].Rows[0]["odiagalt"]))
                        {
                            cryInformes.Formulas["{@TextDiagnos"] = "\"" + Convert.ToString(Cursor_Renamed.Tables[0].Rows[0]["odiagalt"]) + "\"";
                        }
                    }
                    cryInformes.Formulas["{@LitFecIngreso}"] = "\"" + Serrores.VarRPT[9] + "\"";

                    cryInformes.Formulas["{@FecIngreso}"] = "\"" + Convert.ToDateTime(Cursor_Renamed.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy") + "\"";

                    cryInformes.Formulas["{@LitSociedad}"] = "\"" + Serrores.VarRPT[10] + "\"";

                    cryInformes.Formulas["{@Sociedad}"] = "\"" + fSociedad(Convert.ToInt32(Cursor_Renamed.Tables[0].Rows[0]["Ganoadme"]), Convert.ToInt32(Cursor_Renamed.Tables[0].Rows[0]["Gnumadme"])) + "\"";

                    cryInformes.Formulas["{@NewDatos}"] = "\"" + "S" + "\"";
                }
                else
                {
                    cryInformes.Formulas["{@LitCama}"] = "\"" + "\"";
                    cryInformes.Formulas["{@NumCama}"] = "\"" + "\"";
                    cryInformes.Formulas["{@Diagnostico}"] = "\"" + "\"";
                    cryInformes.Formulas["{@TextDiagnos}"] = "\"" + "\"";
                    cryInformes.Formulas["{@LitFecIngreso}"] = "\"" + "\"";
                    cryInformes.Formulas["{@FecIngreso}"] = "\"" + "\"";
                    cryInformes.Formulas["{@LitSociedad}"] = "\"" + "\"";
                    cryInformes.Formulas["{@Sociedad}"] = "\"" + "\"";

                    cryInformes.Formulas["{@NewDatos}"] = "\"" + "N" + "\"";
                }

                Cursor_Renamed.Close();
                //***************** O.Frias 27/02/2007 *****************

                //O.Frias - 04/05/2009
                //Se incorpora la funcion de usuario dependiendo de las constantes IUSULARG y IUSUDOCU
                cryInformes.Formulas["{@Usuario}"] = "\"" + Serrores.fUsuarInfor(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision) + "\"";

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                //    LitPersona = "El " & LitPersona & ":"
                LitPersona = Serrores.VarRPT[4] + " " + LitPersona + ":";
                cryInformes.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";

                HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(Conexion_Base_datos.RcAdmision);
                if (HospitalOCentro.Trim() == "S")
                {
                    //        StrHospitalaria = "de Alta Hospitalaria por el Doctor encargado, habiendo cumplimentado todos los requisitos"
                    StrHospitalaria = Serrores.VarRPT[1];
                }
                else
                {
                    //        StrHospitalaria = "de Alta por el Doctor encargado, habiendo cumplimentado todos los requisitos"
                    StrHospitalaria = Serrores.VarRPT[2];
                }
                cryInformes.Formulas["{@Hospitalaria}"] = "\"" + StrHospitalaria + "\"";

                cryInformes.SQLQuery = sql;
                //    PathString = App.Path

                cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                cryInformes.SubreportToChange = "LogotipoIDC";
                cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                cryInformes.SubreportToChange = "";


                //    cryInformes.WindowTitle = "Informe de Alta"
                cryInformes.WindowTitle = Serrores.VarRPT[3];
                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                cryInformes.Destination = Crystal.DestinationConstants.crptToPrinter; //0 '
                cryInformes.WindowShowPrintSetupBtn = true;
                Serrores.LLenarFormulas(Conexion_Base_datos.RcAdmision, cryInformes, "AGI221R1", "AGI221R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT

                cryInformes.Action = 1;
                cryInformes.PrinterStopPage = cryInformes.PageCount;

                tNombre = "";
                viform = 5;
                Conexion_Base_datos.vacia_formulas(cryInformes, 14);
                ChbAlta.CheckState = CheckState.Unchecked;
                cbImprimir.Enabled = false;
                this.Cursor = Cursors.Default;
            }
            catch (System.Exception excep)
            {
                this.Cursor = Cursors.Default;
                RadMessageBox.Show(excep.Message, Application.ProductName);
                ChbAlta.CheckState = CheckState.Unchecked;
                cbImprimir.Enabled = false;
            }
        }

        public void proInforme_Alta_Volun()
        {
            string StrHospitalaria = String.Empty;

            string MaysculaOMinuscula = "m";

            Serrores.Obtener_Multilenguaje(Conexion_Base_datos.RcAdmision, stGidenpac);
            Serrores.ObtenerTextosRPT(Conexion_Base_datos.RcAdmision, "ADMISION", "AGI222R1");
            Serrores.ObtenerMesesSemanas(Conexion_Base_datos.RcAdmision);

            // PROCEDIMIENTO PARA OBTENER EL INFORME DE ALTA VOLUNTARIA
            string stsqltemp = "select valfanu1,valfanu1i1,valfanu1i2 from sconsglo where gconsglo= 'idenpers' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                switch (Serrores.CampoIdioma)
                {
                    case "IDIOMA0":
                        Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim() + "";
                        break;
                    case "IDIOMA1":
                        Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]).Trim() + "";
                        break;
                    case "IDIOMA2":
                        Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i2"]).Trim() + "";
                        break;
                }
                //--------
            }

            //**************Comprueba si tiene datos sobre las cabeceras******************
            if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
            {
                Conexion_Base_datos.proCabCrystal();
            }
            //****************************************************************************
            //proFormulas viform   'limpio las formulas

            tNombre = stNombre + " " + stApellido1 + " " + stApellido2;

            string[] MatrizSql = new string[] { String.Empty, String.Empty, String.Empty };
            MatrizSql[1] = stGidenpac;
            MatrizSql[2] = Serrores.FormatFechaHMS(stFecha_Llegada);
            DLLTextosSql.TextosSql oTextoSql = new DLLTextosSql.TextosSql();
            string sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, "ADMISION", "AGI220F1", "proInforme_Alta_Volun", 1, MatrizSql, Conexion_Base_datos.RcAdmision));
            oTextoSql = null;

            
            cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
            cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
            cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
            cryInformes.Formulas["{@NOMBRE}"] = "\"" + tNombre + "\"";
            cryInformes.Formulas["{@HORA}"] = "\"" + DateTimeHelper.Time.ToString("HH:mm") + "\"";
            cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + Conexion_Base_datos.DNICEDULA + "\"";
            cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";
            cryInformes.Formulas["{@FechaHoy}"] = "\"" + Serrores.CapturarFechaHoy(MaysculaOMinuscula) + "\"";
            //
            //
            string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(Conexion_Base_datos.RcAdmision);
            if (HospitalOCentro.Trim() == "S")
            {
                //        StrHospitalaria = "la hospitalizaci�n"
                StrHospitalaria = Serrores.VarRPT[1];
            }
            else
            {
                //        StrHospitalaria = "el ingreso"
                StrHospitalaria = Serrores.VarRPT[2];
            }

            cryInformes.Formulas["{@Hospitalaria}"] = "\"" + StrHospitalaria + "\"";

            cryInformes.SQLQuery = sql;

            cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            cryInformes.SubreportToChange = "LogotipoIDC";
            cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
            cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            cryInformes.SubreportToChange = "";

            //    cryInformes.WindowTitle = "Informe de Alta Voluntaria"
            cryInformes.WindowTitle = Serrores.VarRPT[3];
            cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
            tNombre = "";
            cryInformes.Destination = Parasalida;
            cryInformes.WindowShowPrintSetupBtn = true;
            Serrores.LLenarFormulas(Conexion_Base_datos.RcAdmision, cryInformes, "AGI222R1", "AGI222R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT
            cryInformes.Action = 1;
            cryInformes.PrinterStopPage = cryInformes.PageCount;
            viform = 10;
            ChbAltavolun.CheckState = CheckState.Unchecked;
            Conexion_Base_datos.vacia_formulas(cryInformes, 10);
            cbImprimir.Enabled = false;
        }

        // da error por un problema de indices dentro de su dll
        private void proFactura(string Operacion)
        {
            try
            {
                clsOperFMS ClasePedidos = null; //New LlamarOperacionesFMS.clsOperFMS
                object objTipoPrivado = null;

                ClasePedidos = new LlamarOperacionesFMS.clsOperFMS();

                /*En correo enviado por jfpena(22 / 04 / 2016 Asunto: L�mite tareas TFS) dicen que se retira dll LLamarFacturacion
                if (Operacion.ToUpper() == "FAPR1")
                { // factura a privados

                    // para que devuelve el tipo de operacion a realizar :
                    // 1. Privado o Privado de obra benefica
                    objTipoPrivado = new LLamarFacturacion.clsLLamadaFactur();
                    //UPGRADE_TODO: (1067) Member DevolverOperacionPrivados is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    objTipoPrivado.DevolverOperacionPrivados(Operacion, Conexion_Base_datos.RcAdmision, "A", objTipoPrivado);

                    //UPGRADE_TODO: (1067) Member OperacionPrivado is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    Operacion = Convert.ToString(objTipoPrivado.OperacionPrivado);

                    if (Operacion == "")
                    { // quiere decir que no he elegido tipo de privado
                        return;
                    }

                    //UPGRADE_TODO: (1067) Member EsPrivadoGenerico is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    if (Convert.ToBoolean(objTipoPrivado.EsPrivadoGenerico))
                    {
                        ClasePedidos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "FACTURA", stGidenpac, stApellido1, null, "H", vstAnoadme, vstNumadme, iServicio_ALTA);
                    }
                    else
                    {
                        ClasePedidos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "PRIVADOBENEFICA", stGidenpac, stApellido1, null, "H", vstAnoadme, vstNumadme, iServicio_ALTA);
                    }
                    objTipoPrivado = null;
                }
                else                
                {
                    // factura sombra
                    ClasePedidos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "FACTURASOMBRA", stGidenpac);
                }

                chbFactura.CheckState = CheckState.Unchecked;
                chbFacturaSombra.CheckState = CheckState.Unchecked;
                this.Show();*/
            }
            catch
            {
                RadMessageBox.Show("Error accediendo factura", Application.ProductName);
            }
        }

        public void proVolante()
        {
            string[] MatrizSql = null;

            string MaysculaOMinuscula = "m";

            Serrores.Obtener_Multilenguaje(Conexion_Base_datos.RcAdmision, stGidenpac);
            Serrores.ObtenerTextosRPT(Conexion_Base_datos.RcAdmision, "ADMISION", "AGI224R1");
            Serrores.ObtenerMesesSemanas(Conexion_Base_datos.RcAdmision);
            // PROCEDIMIENTO PARA OBTENER EL VOLANTE DE AMBULANCIA

            //**************Comprueba si tiene datos sobre las cabeceras******************
            if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
            {
                Conexion_Base_datos.proCabCrystal();
            }
            //****************************************************************************

            tNombre = stNombre + " " + stApellido1 + " " + stApellido2;
            //proFormulas viform   'limpio las formulas
            // Hacemos una consulta para sacar la direccion del paciente *****************

            MatrizSql = new string[] { String.Empty, String.Empty };
            MatrizSql[1] = stGidenpac;
            DLLTextosSql.TextosSql oTextoSql = new DLLTextosSql.TextosSql();
            string tempRefParam = "ADMISION";
            string tempRefParam2 = "AGI220F1";
            string tempRefParam3 = "proVolante";
            short tempRefParam4 = 1;
            string sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, MatrizSql, Conexion_Base_datos.RcAdmision));
            oTextoSql = null;

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
            DataSet tRr = new DataSet();
            tempAdapter.Fill(tRr);

            // Validaciones de los campos recibidos de la direccion del paciente
            if (Convert.IsDBNull(tRr.Tables[0].Rows[0]["ddirepac"]))
            {
                stCalle = "";
            }
            else
            {
                stCalle = Convert.ToString(tRr.Tables[0].Rows[0]["ddirepac"]);
            }
            if (Convert.IsDBNull(tRr.Tables[0].Rows[0]["gpoblaci"]))
            {
                stPoblacion = "";
            }
            else
            {
                stPoblacion = Convert.ToString(tRr.Tables[0].Rows[0]["poblacion"]);
            }
            if (Convert.IsDBNull(tRr.Tables[0].Rows[0]["dnomprov"]))
            {
                stProvincia = "";
            }
            else
            {
                stProvincia = Convert.ToString(tRr.Tables[0].Rows[0]["dnomprov"]);
            }
            if (Convert.IsDBNull(tRr.Tables[0].Rows[0]["gcodipos"]))
            {
                stCodigo_Postal = "";
            }
            else
            {
                stCodigo_Postal = Convert.ToString(tRr.Tables[0].Rows[0]["gcodipos"]);
            }
            tRr.Close();

            //'If stFORMFILI = "V" Then
            //'    ProvinEstado = "- Estado:"
            //'Else
            Conexion_Base_datos.ProvinEstado = "- Provincia:";
            Conexion_Base_datos.ProvinEstado = Serrores.VarRPT[4];
            //'End If

            MatrizSql = new string[] { String.Empty, String.Empty, String.Empty };
            MatrizSql[1] = stGidenpac;
            MatrizSql[2] = Serrores.FormatFechaHMS(stFecha_Llegada);
            oTextoSql = new DLLTextosSql.TextosSql();
            sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, "ADMISION", "AGI220F1", "proVolante", 2, MatrizSql, Conexion_Base_datos.RcAdmision));
            oTextoSql = null;

            //Se mueve esta linea aqu� porque se debe definir antes de la asignacion de formulas
            cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi224r1_CR11.rpt";

            cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
            cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
            cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
            cryInformes.Formulas["{@NOMBRE}"] = "\"" + tNombre + "\"";
            cryInformes.Formulas["{@FINANCIADORA}"] = "\"" + StSociedad + "\"";
            cryInformes.Formulas["{@NASEGURADO}"] = "\"" + stNasegurado + "\"";
            cryInformes.Formulas["{@CALLE}"] = "\"" + stCalle + "\"";
            cryInformes.Formulas["{@POBLACION}"] = "\"" + Documentos_ingreso.DefInstance.BuscarPoblacion(stGidenpac) + "\"";
            cryInformes.Formulas["{@CODIGO}"] = "\"" + stCodigo_Postal + "\"";
            cryInformes.Formulas["{@PROVINCIA}"] = "\"" + stProvincia + "\"";
            cryInformes.Formulas["{@TexProvincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";

            //'    If stFORMFILI = "V" Then
            //''        cryInformes.Formulas(11) = "FORM4= 'CONSTANCIA DE TRASLADO EN AMBULANCIA'"
            //'        cryInformes.Formulas(11) = "FORM4= """ & VarRPT(1) & """"
            //'    Else
            //        cryInformes.Formulas(11) = "FORM4= 'VOLANTE DE AMBULANCIA'"
			//'    End If

            cryInformes.Formulas["{@FORM4}"] = "\"" + Serrores.VarRPT[2] + "\"";
            //'    End If
            cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";
            cryInformes.Formulas["{@FechaHoy}"] = "\"" + Serrores.CapturarFechaHoy(MaysculaOMinuscula) + "\"";

            cryInformes.SQLQuery = sql;
            //    PathString = App.Path

            cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            cryInformes.SubreportToChange = "LogotipoIDC";
            cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
            cryInformes.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            cryInformes.SubreportToChange = "";
            //    cryInformes.WindowTitle = "Volante de Ambulancia"
            cryInformes.WindowTitle = Serrores.VarRPT[3];
            cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
            cryInformes.Destination = Parasalida;
            cryInformes.WindowShowPrintSetupBtn = true;
            Serrores.LLenarFormulas(Conexion_Base_datos.RcAdmision, cryInformes, "AGI224R1", "AGI224R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT
            cryInformes.Action = 1;
            cryInformes.PrinterStopPage = cryInformes.PageCount;
            tNombre = "";
            ChbVolante.CheckState = CheckState.Unchecked;
            cbImprimir.Enabled = false;
            Conexion_Base_datos.vacia_formulas(cryInformes, 12);

        }
        public void proMedico()
        {
            //cambio de la llamada de informe de alta a informes m�dicos
            InfMedSinEpisodio.MCInformeMedicos instancia = new InfMedSinEpisodio.MCInformeMedicos();
            instancia.LoadClase(Conexion_Base_datos.RcAdmision, stGidenpac, vstAnoadme, vstNumadme, stFecha_ALTA, iServicio_ALTA, Path.GetDirectoryName(Application.ExecutablePath), cryInformes, Conexion_Base_datos.stNombreDsnACCESS, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.BDtempo, false, false, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a);
            instancia = null;
            //    Set Instancia = CreateObject("inf_med_alta.mcinf_med_alta")
            //    Instancia.Load RcAdmision, App.Path, "A", Str(vstAnoadme), Str(vstNumadme), gUsuario, cryInformes, "GHOSP_ACCESS", VstCodUsua, BDtempo, fEstaDadodeAlta
            //    Set Instancia = Nothing
            //MsgBox ("HAY QUE REALIZAR LA CONEXION CON EL INFORME MEDICO AL ALTA EN PLANTA")
            ChbMedico.CheckState = CheckState.Unchecked;
            cbImprimir.Enabled = false;
        }
        private int fnAtNum(string cCaracter, string cCadena, double? nPosBus_optional, int? nComparar_optional)
        {
            double nPosBus = (nPosBus_optional == null) ? 0 : nPosBus_optional.Value;
            int nComparar = (nComparar_optional == null) ? 0 : nComparar_optional.Value;
            try
            {
                //********************************************************************************
                //Busca la N ocurrencia de un caracter en una cadena
                //Par�metros.......: - Cadena a buscar
                //                   - Cadena donde buscar
                //                   - N� de ocurrencia buscada (Opcional)
                //                   - Tipo de comparaci�n:- 0 = Comparaci�n binaria (por defecto)
                //                                         - 1 = Comparaci�n textual.
                //********************************************************************************

                int icont = 0;
                int iPosEnc = 0;
                int iPossig = 0;
                CompareMethod cComparar = (CompareMethod)0;

                if (nPosBus_optional == null)
                {
                    nPosBus = 1;
                }
                if (nPosBus > 0)
                {
                    if (nComparar_optional == null)
                    {
                        nComparar = 0;
                    }
                    iPosEnc = 1;
                    iPossig = 1;
                    icont = 1;

                    while (icont <= nPosBus && iPosEnc > 0)
                    {
                        iPosEnc = Strings.InStr(iPossig, cCadena, cCaracter, cComparar);
                        icont++;
                        iPossig = iPosEnc + 1;
                    };
                    return iPosEnc;
                }
                else
                {
                    return 0;
                }
            }
            finally
            {
                nPosBus_optional = nPosBus;
                nComparar_optional = nComparar;
            }
            return 0;
        }

        private int fnAtNum(string cCaracter, string cCadena, double? nPosBus_optional)
        {
            return fnAtNum(cCaracter, cCadena, nPosBus_optional, null);
        }

        private int fnAtNum(string cCaracter, string cCadena)
        {
            return fnAtNum(cCaracter, cCadena, null, null);
        }

        private int fnNumToken(string cCadena, object cCarSep_optional)
        {
            string cCarSep = (cCarSep_optional == Type.Missing) ? String.Empty : cCarSep_optional as string;
            try
            {

                //*****************************************************************************
                //Devuelve el n�mero de "token's" en una cadena
                //Par�metros......:- Cadena donde buscar
                //                 - Caracter de separaci�n de "token's", ";" por defecto.
                //*****************************************************************************

                int icont = 0;
                if (cCarSep_optional == Type.Missing)
                {
                    cCarSep = ";";
                }
                for (int iBucle = 1; iBucle <= cCadena.Length; iBucle++)
                {
                    if (String.Compare(cCadena.Substring(iBucle - 1, Math.Min(1, cCadena.Length - (iBucle - 1))), cCarSep) == 0)
                    {
                        icont++;
                    }
                }
                if (icont < 1 && cCadena.Length > 0)
                {
                    icont = 1;
                }
                else
                {
                    icont++;
                }
                return icont;
            }
            finally
            {
                cCarSep_optional = cCarSep;
            }

            return 0;
        }

        private int fnNumToken(string cCadena)
        {
            object tempRefParam4 = Type.Missing;
            return fnNumToken(cCadena, tempRefParam4);
        }

        private string fnGetToken(string cCadena, int nPosBus, object cCarSep_optional)
        {
            string cCarSep = (cCarSep_optional == Type.Missing) ? String.Empty : cCarSep_optional as string;
            try
            {

                //*****************************************************************************
                //Extrae un "token" (fragmento) de una cadena
                //Par�metros........: - Cadena donde buscar
                //                    - N� de ocurrencia a extraer
                //                    - Caracter de separaci�n de token's, ";" por defecto.
                //*****************************************************************************

                string stCarsep1 = String.Empty;
                int iPosEnc = 0;
                int iPossig = 0;


                string stCadFin = "";
                if (String.Compare(cCadena, "") != 0 && nPosBus > 0)
                {
                    if (cCarSep_optional == Type.Missing)
                    {
                        cCarSep = ";";
                    }
                    else
                    {
                        stCarsep1 = cCarSep;
                    }
                    if (nPosBus <= fnNumToken(cCadena, stCarsep1))
                    {
                        double tempRefParam = nPosBus - 1;
                        iPosEnc = fnAtNum(stCarsep1, cCadena, tempRefParam) + 1;
                        double tempRefParam2 = nPosBus;
                        iPossig = fnAtNum(stCarsep1, cCadena, tempRefParam2) - 1;
                        nPosBus = Convert.ToInt32(tempRefParam2);
                        if (iPossig < 0)
                        {
                            iPossig = cCadena.Length;
                        }
                        stCadFin = cCadena.Substring(iPosEnc - 1, Math.Min(iPossig - iPosEnc + 1, cCadena.Length - (iPosEnc - 1)));
                    }
                }
                return stCadFin;
            }
            finally
            {
                cCarSep_optional = cCarSep;
            }
            return System.String.Empty;
        }

        private string fnGetToken(string cCadena, int nPosBus)
        {
            object tempRefParam5 = Type.Missing;
            return fnGetToken(cCadena, nPosBus, tempRefParam5);
        }

        private void ChbAlta_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void chbAltaMedica_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbAltavolun_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void chbFactSombDocu_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void chbFactura_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void chbFacturaSombra_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void chbHojaClinEsta_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChBHojaReferencia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void chbIncapacidad_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbJusAcompa_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbJustificante_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbMedico_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void Chboficio_ClickEvent(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbVolante_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        //UPGRADE_NOTE: (7001) The following declaration (CheckBox1_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        //private void CheckBox1_Click()
        //{
        //activa_imprimir();
        //}

        //oscar 15/09/03
        private void chbVolanteSociedad_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbHaceconstar_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }
        //-------------

        private void Documentos_alta_Activated(Object eventSender, EventArgs eventArgs)
        {
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                string lCodPV = String.Empty;
                string lCodSS = String.Empty;
                string stCodSociedad = String.Empty;
                string stTexPV = String.Empty;
                string stTexSS = String.Empty;
                string vstSociedad = String.Empty;
                DataSet RR = null;
                DataSet rrtemp = null;
                string stSql = String.Empty;
                string stsqltemp = String.Empty;
                string[] MatrizSql = null;
                DLLTextosSql.TextosSql oTextoSql = null;

                cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;
                Parasalida = Crystal.DestinationConstants.crptToPrinter;
                

                string tstSS = String.Empty;
                int iTag = 0;
                if (Conexion_Base_datos.DescargaMenuAlta)
                {
                    this.Close();
                }
                else
                {

                    //oscar 17/05/04
                    menu_alta.DefInstance.Enabled = false;
                    //------

                    proCarga_Datos_Iniciales();

                    lbPaciente.Text = stNombre + " " + stApellido1 + " " + stApellido2;

                    stSql = " SELECT dsocieda" + " FROM DSOCIEDA " + " WHERE gsocieda = '" + stCodigosoc + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    RR = new DataSet();
                    tempAdapter.Fill(RR);

                    stsqltemp = " select valfanu1,nnumeri1 from sconsglo where gconsglo= 'segursoc' ";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_2.Fill(rrtemp);
                    lCodSS = Convert.ToString(rrtemp.Tables[0].Rows[0]["nnumeri1"]).Trim();
                    stTexSS = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();
                    rrtemp.Close();

                    stsqltemp = " select valfanu1,nnumeri1 from sconsglo where gconsglo= 'privado' ";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_3.Fill(rrtemp);
                    lCodPV = Convert.ToString(rrtemp.Tables[0].Rows[0]["nnumeri1"]).Trim();
                    stTexPV = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();
                    rrtemp.Close();

                    if (stCodSociedad == lCodSS)
                    {
                        StSociedad = stTexSS;
                    }
                    else if (stCodSociedad == lCodPV)
                    {
                        StSociedad = stTexPV;
                    }
                    else
                    {
                        if (RR.Tables[0].Rows.Count != 0)
                        {
                            StSociedad = Convert.ToString(RR.Tables[0].Rows[0]["dsocieda"]).Trim();
                        }
                    }

                    RR.Close();
                    //SI he seleccionado un paciente en concreto  saca los documentos de ese paciente
                    if (Conexion_Base_datos.Aepisodio != 0 && Conexion_Base_datos.Nepisodio != 0)
                    {
                        stSql = " SELECT fllegada,faltplan,gserulti, faltaadm " + " FROM AEPISADM " + " WHERE faltplan is not null and ganoadme = " + Conexion_Base_datos.Aepisodio.ToString() + " and " + " gnumadme = " + Conexion_Base_datos.Nepisodio.ToString() + " ";
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        RR = new DataSet();
                        tempAdapter_4.Fill(RR);
                        if (RR.Tables[0].Rows.Count == 0)
                        {
                            //MsgBox "Este paciente no ha estado ingresado, o no esta dado de alta", vbInformation
                            //OSCAR
                            RadMessageBox.Show("Este paciente esta ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                            //----
                            RR.Close();
                            this.Close();
                            return;
                        }
                        else
                        {
                            stFecha_Llegada = Convert.ToString(RR.Tables[0].Rows[0]["fllegada"]);
                            vstAnoadme = Conexion_Base_datos.Aepisodio;
                            vstNumadme = Conexion_Base_datos.Nepisodio;
                            //If IsNull(Rr("faltplan")) Then
                            //   fEstaDadodeAlta = False
                            //   Chboficio.Enabled = False
                            //Else
                            stFecha_ALTA = Convert.ToDateTime(RR.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm:ss");
                            iServicio_ALTA = Convert.ToInt32(RR.Tables[0].Rows[0]["gserulti"]);
                            fEstaDadodeAlta = true;
                            Chboficio.Enabled = true;
                            //End If
                        }
                        RR.Close();
                        stFecha_Llegada = stFecha_Llegada;

                    }
                    else
                    {
                        // No se ha elegido ningun paciente en el grid
                        stSql = " SELECT ganoadme,gnumadme,fllegada,faltplan,gserulti, faltaadm " + " FROM AEPISADM " + " WHERE faltplan is not null and " + " AEPISADM.gidenpac ='" + stGidenpac + "'";
                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        RR = new DataSet();
                        tempAdapter_5.Fill(RR);

                        if (RR.Tables[0].Rows.Count == 0)
                        { //EL paciente no est�  de alta o no ha estado ingresado
                            stSql = "select * from aepisadm where AEPISADM.gidenpac ='" + stGidenpac + "'";
                            SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                            RR = new DataSet();
                            tempAdapter_6.Fill(RR);
                            if (RR.Tables[0].Rows.Count == 0)
                            { //EL paciente no ha estado ingresado nunca
                                RadMessageBox.Show("Este paciente no ha estado ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                            }
                            else
                            {
                                //MsgBox "Este paciente no ha estado ingresado, o no esta dado de alta", vbInformation
                                //OSCAR
                                RadMessageBox.Show("Este paciente esta ingresado.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                            }

                            //---
                            this.Cursor = Cursors.WaitCursor;
                            RR.Close();
                            this.Close();
                            return;
                        }
                        else
                        {
                            if (RR.Tables[0].Rows.Count == 1)
                            {
                                stFecha_Llegada = Convert.ToString(RR.Tables[0].Rows[0]["fllegada"]);
                                vstAnoadme = Convert.ToInt32(RR.Tables[0].Rows[0]["ganoadme"]);
                                vstNumadme = Convert.ToInt32(RR.Tables[0].Rows[0]["gnumadme"]);
                                //        If IsNull(Rr("faltplan")) Then
                                //           fEstaDadodeAlta = False
                                //           Chboficio.Enabled = False
                                //       Else
                                stFecha_ALTA = Convert.ToDateTime(RR.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm:ss");
                                iServicio_ALTA = Convert.ToInt32(RR.Tables[0].Rows[0]["gserulti"]);
                                fEstaDadodeAlta = true;
                                Chboficio.Enabled = true;
                                //        End If
                                stFecha_Llegada = stFecha_Llegada;

                            }
                            else
                            {
                                //hay m�s de un paciente, muestra los episodios para sacar los documentos
                                //de un episodio en concreto                                
                                Seleccion_episodios.DefInstance.RecogerGidenpac(stGidenpac);
								Seleccion_episodios.DefInstance.ShowDialog();
                                
                                if (vstAnoadme == 0 && vstNumadme == 0)
                                {
                                    this.Close();
                                    return;
                                }
                            }
                        }
                    }

                    MatrizSql = new string[] { String.Empty, String.Empty, String.Empty };
                    MatrizSql[1] = stGidenpac;
                    MatrizSql[2] = Serrores.FormatFechaHMS(stFecha_Llegada);
                    oTextoSql = new DLLTextosSql.TextosSql();
                    stSql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, "ADMISION", "AGI220F1", "Form_Activate", 1, MatrizSql, Conexion_Base_datos.RcAdmision));
                    oTextoSql = null;
                    SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    RR = new DataSet();
                    tempAdapter_7.Fill(RR);

                    ChbVolante.Enabled = Convert.ToString(RR.Tables[0].Rows[0]["iambulan"]) == "S";
                    if (Convert.ToString(RR.Tables[0].Rows[0]["ivolunta"]) == "N")
                    {
                        ChbAltavolun.Enabled = false;
                    }

                    //oscar 22/03/04
                    // MODIFICACION AL 21/08/1998 EN FUENTES DE LA ANTIGUA ECASA:
                    // DEJAMOS EL DOC. DE FALLECIMIENTO AUNQUE NO TENGA GJUZGADO YA QUE
                    // SI NO LO TIENE SACAMOS EN EL REPORT "JUZGADO DECANO DEL REPARTO CIVIL"
                    ChbFallecInc.Enabled = Convert.ToString(RR.Tables[0].Rows[0]["icexitus"]) == "S";
                    ChbAltaInc.Enabled = !Convert.IsDBNull(RR.Tables[0].Rows[0]["gjuzgad1"]);
                    //-------------

                    if (Color.FromName(Documentos_alta.DefInstance.Tag.ToString()) == Conexion_Base_datos.Negro)
                    {
                        ChbAlta.Enabled = false;
                        ChbAltavolun.Enabled = false;
                        ChbVolante.Enabled = false;
                        chbFactura.Enabled = false;
                        //chbFacturaSombra.Enabled = False 'se controla en el load
                        ChbMedico.Enabled = false;
                    }
                    RR.Close();

                    stSql = "select aepisadm.diasabse, aepisadm.horaabse " +
                            " from aepisadm where AEPISADM.GANOADME  = " + vstAnoadme.ToString() + " AND " +
                            " AEPISADM.GNUMADME  = " + vstNumadme.ToString() + " ";

                    SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    RR = new DataSet();
                    tempAdapter_8.Fill(RR);
                    chbIncapacidad.Enabled = !Convert.IsDBNull(RR.Tables[0].Rows[0]["diasabse"]) || !Convert.IsDBNull(RR.Tables[0].Rows[0]["horaabse"]);
                    RR.Close();


                    //     stsql = "select aepisadm.diasabse, aepisadm.horaabse " & _
                    //'     " from aepisadm where AEPISADM.GANOADME  = " & Aepisodio & " AND " & _
                    //'     " AEPISADM.GNUMADME  = " & Nepisodio & " "
                    //
                    //      Set Rr = RcAdmision.OpenResultset(stsql, rdOpenKeyset)
                    //      If Not IsNull(Rr("diasabse")) Or Not IsNull(Rr("horaabse")) Then
                    //             chbIncapacidad.Enabled = True
                    //      Else
                    //             chbIncapacidad.Enabled = False
                    //      End If
                    //      Rr.Close
                    //    stsql = " SELECT ganoadme,gnumadme,fllegada,faltplan,gserulti, faltaadm " _
                    //'    & " FROM AEPISADM " _
                    //'    & " WHERE fllegada in (select max(fllegada) from aepisadm where " _
                    //'    & " AEPISADM.gidenpac ='" & stGidenpac & "') " _
                    //'    & " and AEPISADM.gidenpac ='" & stGidenpac & "'"
                    //Set Rr = RcAdmision.OpenResultset(stsql, rdOpenKeyset, 3, rdExecDirect)
                    //If Rr.RowCount = 0 Then
                    //    MsgBox "Este paciente no ha estado ingresado, o no esta dado de alta", vbInformation
                    //    Rr.Close
                    //    Unload Me
                    //Else
                    //            stFecha_Llegada = Rr("fllegada")
                    //            vstAnoadme = Rr("ganoadme")
                    //            vstNumadme = Rr("gnumadme")
                    //            If IsNull(Rr("faltplan")) Then
                    //                fEstaDadodeAlta = False
                    //                Chboficio.Enabled = False
                    //            Else
                    //                stFecha_ALTA = Format(Rr("faltplan"), "DD/MM/YYYY HH:mm:ss")
                    //                iServicio_ALTA = Rr("gserulti")
                    //                fEstaDadodeAlta = True
                    //                Chboficio.Enabled = True
                    //            End If
                    //            Rr.Close
                    //            stFecha_Llegada = stFecha_Llegada
                    //
                    //            ReDim MatrizSql(2)
                    //            MatrizSql(1) = stGidenpac
                    //            MatrizSql(2) = FormatFechaHMS(stFecha_Llegada)
                    //            Set oTextoSql = CreateObject("DLLTEXTOSSQL.TEXTOSSQL")
                    //            stsql = oTextoSql.devuelvetexto(vstTIPOBD, "ADMISION", "AGI220F1" _
                    //'            , "Form_Activate", 1, MatrizSql, RcAdmision)
                    //            Set oTextoSql = Nothing
                    //
                    //            Set Rr = RcAdmision.OpenResultset(stsql, rdOpenKeyset, 3, rdExecDirect)
                    //            If Rr("iambulan") = "S" Then
                    //                ChbVolante.Enabled = True
                    //            Else
                    //                ChbVolante.Enabled = False
                    //            End If
                    //   If Rr("ivolunta") = "N" Then
                    //        ChbAltavolun.Enabled = False
                    //    End If
                    //    If Documentos_alta.Tag = Negro Then
                    //        ChbAlta.Enabled = False
                    //        ChbAltavolun.Enabled = False
                    //        ChbVolante.Enabled = False
                    //        chbFactura.Enabled = False
                    //        chbFacturaSombra.Enabled = False
                    //        ChbMedico.Enabled = False
                    //    End If
                    //    Rr.Close
                    //End If

                    //oscar 15/09/03
                    //Si no es de sociedad no se imprime el Volante
                    tstSS = Conexion_Base_datos.fnSociedad(stGidenpac, vstAnoadme, vstNumadme);
                    iTag = (tstSS.IndexOf('/') + 1);
                    vstSociedad = tstSS.Substring(0, Math.Min(iTag - 1, tstSS.Length));
                    tstSS = tstSS.Substring(iTag);
                    chbVolanteSociedad.Enabled = tstSS != "PR";


                    stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'IFACSOMB' ";
                    SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_9.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count != 0)
                    {
                        if (Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
                        {
                            chbFacturaSombra.Enabled = Color.FromName(Documentos_alta.DefInstance.Tag.ToString()) != Conexion_Base_datos.Negro;
                        }
                        else
                        {
                            chbFacturaSombra.Enabled = false;
                        }
                    }
                    else
                    {
                        chbFacturaSombra.Enabled = Color.FromName(Documentos_alta.DefInstance.Tag.ToString()) != Conexion_Base_datos.Negro;
                    }
                    rrtemp.Close();
                    //---------------
                }


                chbFactSombDocu.Enabled = true; //si no es SS o esta facturado, no se activa.
                chbFactSombDocu.Enabled = mbAcceso.BuscarSociedadEpisodio(stGidenpac, vstAnoadme, vstNumadme, "H", Conexion_Base_datos.RcAdmision);

                // ***************************************************************************

            }
        }

        public void activa_imprimir()
        {
            bool stimprimir = false;
            if (ChbAlta.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbAltavolun.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (chbFactura.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbJustificante.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbMedico.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbVolante.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbJusAcompa.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (chbFacturaSombra.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (chbAltaMedica.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (chbHojaClinEsta.Visible)
            {
                if (chbHojaClinEsta.CheckState == CheckState.Checked)
                {
                    stimprimir = true;
                }
            }
            if (Chboficio.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChBHojaReferencia.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (chbIncapacidad.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (chbFactSombDocu.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }

            //oscar 15/09/03
            if (chbVolanteSociedad.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbHaceconstar.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            //-------------

            //oscar 22/03/2004
            if (ChbAltaInc.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            if (ChbFallecInc.CheckState == CheckState.Checked)
            {
                stimprimir = true;
            }
            //---------

            cbImprimir.Enabled = stimprimir;
        }

        //Public Sub proFormulas(idForm As Integer)
        //Dim tiForm As Integer
        //For tiForm = 0 To idForm
        //    cryInformes.Formulas(tiForm) = ""
        //Next tiForm
        //End Sub


        public void proCarga_Datos_Iniciales()
        {
            if (!Conexion_Base_datos.DescargaMenuAlta)
            {
                stTemp = Convert.ToString(menu_alta.DefInstance.Tag);
                stNombre = fnGetToken(stTemp, 1, "$");
                stApellido1 = fnGetToken(stTemp, 2, "$");
                stApellido2 = fnGetToken(stTemp, 3, "$");
                stNasegurado = fnGetToken(stTemp, 4, "$");
                stCodigosoc = fnGetToken(stTemp, 5, "$");
                stHistoria = fnGetToken(stTemp, 6, "$");
                stGidenpac = fnGetToken(stTemp, 7, "$");
                stDNI = fnGetToken(stTemp, 8, "$");
            }
        }

        private void Documentos_alta_Load(Object eventSender, EventArgs eventArgs)
        {
            bool STRHOJACLI = false;
            bool STROFICIO = false;
            bool STRINCAPACIDAD = false;
            int icont = 0;
            //  variables para comprobar si hay que visualizar los checkbox
            string stsqltemp = String.Empty;
            DataSet rrtemp = null;
            string LitPersona = String.Empty;
            try
            {

                //Left = 3; //(Screen.Width - Width) / 2
                //Top = 3; //(Screen.Height - Height) / 2

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
                Frame3.Text = LitPersona;

                //oscar 01/10/03 --> Se ha llevado a form_activate porque todavia no esta establecido
                //el tag y falla en el caso de que IFACSOMB=S
                //'    '******
                //'    StSqlTemp = "select valfanu1 from sconsglo where gconsglo= 'IFACSOMB' "
                //'    Set rrtemp = RcAdmision.OpenResultset(StSqlTemp, rdOpenKeyset, 1)
                //'    If Not rrtemp.EOF Then
                //'        If UCase(Trim(rrtemp("valfanu1"))) = "S" Then
                //'            If Documentos_alta.Tag = Negro Then
                //'                chbFacturaSombra.Enabled = False
                //'            Else
                //'                chbFacturaSombra.Enabled = True
                //'            End If
                //'        Else
                //'            chbFacturaSombra.Enabled = False
                //'        End If
                //'    Else
                //'        If Documentos_alta.Tag = Negro Then
                //'            chbFacturaSombra.Enabled = False
                //'        Else
                //'            chbFacturaSombra.Enabled = True
                //'        End If
                //'    End If
                //'    rrtemp.Close
                //'    '**************
                //---------------------------------------------------------------------------

                //Colocamos los objetos seg�n el n� de chequeos visibles
                icont = 0;
                PosicionarChequeo(Chboficio, "PSIQUIAT", ref icont, "S");
                PosicionarChequeo(chbHojaClinEsta, "HOCLIEST", ref icont, "SI");
                PosicionarChequeo(chbIncapacidad, "BAJALABO", ref icont, "S");
                if (Conexion_Base_datos.stFORMFILI == "E")
                {
                    PosicionarChequeo(chbFactSombDocu, "FACTSOMB", ref icont, "SI");
                }

                //oscar 15/09/03
                ChbHaceconstar.Enabled = true;
                chbVolanteSociedad.Enabled = true;
                icont++;
                chbVolanteSociedad.Top = (int)(ChBHojaReferencia.Top + icont * 300 / 15);
                icont++;
                ChbHaceconstar.Top = (int)(ChBHojaReferencia.Top + icont * 300 / 15);
                //-------------------


                //oscar 22/03/04
                PosicionarChequeo(ChbAltaInc, "NOJUZALT", ref icont, "SI");
                PosicionarChequeo(ChbFallecInc, "NOJUZALF", ref icont, "SI");
                //--------------



                Frame1.Height = (int)((3995 + icont * 300) / 15);
                cbImprimir.Top = (int)((4050 + icont * 300) / 15);
                cbCancelar.Top = (int)((4050 + icont * 300) / 15);
                this.Height = (int)((5000 + icont * 300) / 15);
                this.Width = Frame1.Width + 10;

                //**************
                //    If STRHOJACLI = False And STROFICIO = False And STRINCAPACIDAD = False Then
                //        Frame1.Height = 4695
                //        cbImprimir.Top = 4785
                //        cbCancelar.Top = 4785
                //        Me.Height = 5790
                //    End If
                //   If STRHOJACLI = True And STROFICIO = True And STRINCAPACIDAD = True Then
                //            chbHojaClinEsta.Visible = True
                //            Chboficio.Visible = True
                //            chbIncapacidad.Visible = True
                //            Frame1.Height = 5925
                //            cbImprimir.Top = 6000
                //            cbCancelar.Top = 6000
                //            Me.Height = 6850
                //    Else
                //        If STRHOJACLI = True And STROFICIO = False And STRINCAPACIDAD = True Then
                //           chbHojaClinEsta.Visible = True
                //           chbIncapacidad.Visible = True
                //           Frame1.Height = 5470
                //           chbHojaClinEsta.Top = 4725
                //           chbIncapacidad.Top = 5145
                //           cbImprimir.Top = 5552
                //           cbCancelar.Top = 5552
                //           Me.Height = 6475
                //        Else
                //           If STRHOJACLI = False And STROFICIO = True And STRINCAPACIDAD = True Then
                //               Chboficio.Visible = True
                //               chbIncapacidad.Visible = True
                //               Chboficio.Top = 4725
                //               chbIncapacidad.Top = 5145
                //               Frame1.Height = 5470
                //               cbImprimir.Top = 5552
                //               cbCancelar.Top = 5552
                //               Me.Height = 6475
                //           Else
                //                If STRHOJACLI = False And STROFICIO = False And STRINCAPACIDAD = True Then
                //                    chbIncapacidad.Visible = True
                //                    chbIncapacidad.Top = 4725
                //                    Frame1.Height = 5095
                //                    cbImprimir.Top = 5165
                //                    cbCancelar.Top = 5165
                //                    Me.Height = 6050
                //                Else
                //                    If STRHOJACLI = True And STROFICIO = True And STRINCAPACIDAD = False Then
                //                        Chboficio.Visible = True
                //                        chbHojaClinEsta.Visible = True
                //                        Chboficio.Top = 4725
                //                        chbHojaClinEsta.Top = 5145
                //                        Frame1.Height = 5470
                //                        cbImprimir.Top = 5552
                //                        cbCancelar.Top = 5552
                //                        Me.Height = 6475
                //                    End If
                //                End If
                //          End If
                //        End If
                //    End If

                //'    If stFORMFILI = "V" Then
                //'        ChbVolante.Caption = "Constancia de traslado en ambulancia"
                //'        chbIncapacidad.Caption = "Reposo"
                //'
                //'        'oscar 15/09/03
                //'        chbVolanteSociedad.Caption = "Constancia para la organizaci�n"
                //'        '--------------
                //'
                //'    End If
            }
            catch
            {
                rrtemp = null;
            }

        }
        public void PosicionarChequeo(RadCheckBox oCheq, string stConsGlo, ref int iContador, string stValor)
        {

            string stsqltemp = "select valfanu1 from sconsglo where gconsglo= '" + stConsGlo + "' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["valfanu1"]))
                {
                    if (Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == stValor.ToUpper())
                    {
                        iContador++;
                        oCheq.Visible = true;
                        oCheq.Top = (int)(ChBHojaReferencia.Top + iContador * 300 / 15);
                    }
                }
            }
            rrtemp.Close();
        }

        private void Documentos_alta_Closed(Object eventSender, EventArgs eventArgs)
        {
            //para no refrescar el menu_alta si lo han descargado
            if (!Conexion_Base_datos.DescargaMenuAlta)
            {
                //oscar 17/05/04
                menu_alta.DefInstance.Enabled = true;
                //------
                menu_alta.DefInstance.refresco();
                this.Cursor = Cursors.Default;
            }
            MemoryHelper.ReleaseMemory();
        }
        public void RecogerEpisodio(int A�oRegistro, int NumeroRegistro, string FechaAlta, int iServicio, string fllegada)
        {
            //en este procedimiento recojo los datos que manda la pantalla de seleccionar episodio
            vstAnoadme = A�oRegistro;
            vstNumadme = NumeroRegistro;
            stFecha_ALTA = FechaAlta;
            iServicio_ALTA = iServicio;
            stFecha_Llegada = fllegada;
        }


        public void proIncapacidad(Crystal PCryInformes, object PCuadro)
        {
            /*MCINCAPACIDAD oParteIncap = new INCAPACIDAD.MCINCAPACIDAD();
            oParteIncap.datos("H", vstAnoadme, vstNumadme, PCryInformes, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ELEMENTOSCOMUNES\\RPT", Conexion_Base_datos.RcAdmision, 1, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, PCuadro);
            oParteIncap = null;
            chbIncapacidad.CheckState = CheckState.Unchecked;*/
        }

        //oscar 22/03/2004
        private void ChbAltaInc_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }

        private void ChbFallecInc_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            activa_imprimir();
        }
        //----------

        private bool fNewCampos()
        {
            bool result = false;

            string strSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'ALTADMNF'";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, Conexion_Base_datos.RcAdmision);
            DataSet rdoTemp = new DataSet();
            tempAdapter.Fill(rdoTemp);

            if (rdoTemp.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu1"]))
                {
                    if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
                    {
                        result = true;
                    }
                }
            }
            rdoTemp.Close();

            return result;
        }

        private string fCama(int intAnoReg, int lngNumReg)
        {

            string result = String.Empty;
            result = "";

            string strSql = "SELECT gplantor, ghabitor, gcamaori " +
                            "FROM AMOVIMIE " +
                            "WHERE ganoregi = " + intAnoReg.ToString() + " AND " +
                            "gnumregi = " + lngNumReg.ToString();

            SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, Conexion_Base_datos.RcAdmision);
            DataSet rdoTemp = new DataSet();
            tempAdapter.Fill(rdoTemp);

            if (rdoTemp.Tables[0].Rows.Count != 0)
            {
                //result = StringsHelper.Format(rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count-1]["gplantor"], "0#") + StringsHelper.Format(rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count-1]["ghabitor"], "0#") + Convert.ToString(rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count-1]["gcamaori"]);
                result = Convert.ToString(int.Parse(rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count - 1]["gplantor"].ToString()).ToString("D2")) + Convert.ToString(int.Parse(rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count - 1]["ghabitor"].ToString()).ToString("D2")) + rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count - 1]["gcamaori"].ToString();
            }

            rdoTemp.Close();

            return result;
        }

        private string fDiagnostico(string strCodDiag, System.DateTime fFecAlta)
        {

            string result = String.Empty;
            result = "";

            string strSql = "SELECT dnombdia " +
                            "FROM DDIAGNOS " +
                            "WHERE gcodidia = '" + strCodDiag.Trim() + "' AND " +
                            "finvaldi <= " + Serrores.FormatFecha(fFecAlta) + " AND " +
                            "( ffivaldi is null or ffivaldi >= " + Serrores.FormatFecha(fFecAlta) + ")";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, Conexion_Base_datos.RcAdmision);
            DataSet rdoTemp = new DataSet();
            tempAdapter.Fill(rdoTemp);

            if (rdoTemp.Tables[0].Rows.Count != 0)
            {
                result = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombdia"]).Trim();
            }

            rdoTemp.Close();

            return result;
        }

        private string fSociedad(int intAnoReg, int lngNumReg)
        {

            string result = String.Empty;
            result = "";

            string strSql = "SELECT DSOCIEDA.dsocieda " +
                            "FROM AMOVIFIN " +
                            "INNER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " +
                            "WHERE ganoregi = " + intAnoReg.ToString() + " AND " +
                            "gnumregi = " + lngNumReg.ToString();

            SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, Conexion_Base_datos.RcAdmision);
            DataSet rdoTemp = new DataSet();
            tempAdapter.Fill(rdoTemp);

            if (rdoTemp.Tables[0].Rows.Count != 0)
            {
                result = Convert.ToString(rdoTemp.Tables[0].Rows[rdoTemp.Tables[0].Rows.Count - 1]["dsocieda"]).Trim();
            }

            rdoTemp.Close();
            return result;
        }
    }
}
