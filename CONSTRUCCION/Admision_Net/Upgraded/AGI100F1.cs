using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using UpgradeSupportHelper;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.VisualBasic.PowerPacks;
//using UpgradeHelpers.Spread;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Transactions;

namespace ADMISION
{
	public partial class menu_ingreso: Telerik.WinControls.UI.RadForm
    {
		int orden_columna = 0;
		//Dim classHist As classHistoria
		string vstHistoria = String.Empty;
        filiacionDLL.Filiacion vmcFiliacion = null;
        //Dim RrIngresos As rdoResultset
        object mcIngreso = null;
		//Dim stIngresos As String     'Select para abrir el RrIngresos
		MovimientosPac.MCMovimientosPac mcListadosPac = null; //para el listado de pacientes

		int iNuPlan = 0; //Variables de busqueda que salen de
		int iNuHab = 0; //proRecogerPac
		string stNuCama = String.Empty;
		string prototipo = String.Empty;

		string vstGidenpac = String.Empty; //Recojo el GIDENPAC de la Spread
		string stNombPaciente = String.Empty; //Recojo el nombre del paciente
		public int lRow = 0; //Fila seleccionada
		int viGanoadme = 0; //numero de registro
		int viGnumadme = 0; //numero de registro
		string vstGanoadme = String.Empty; //numero de LOS DOS registro para pasar en tag
		string vstIdPaciente = String.Empty; //recoge el gidenpac de la dll de filiaci�n
		string vstCodsoc = String.Empty; //codigo de la aseguradora de la dll de filiacion
		string vstAsegurado = String.Empty; //codigo del asegurado devuelto por dll de filiaci�n
		string vstIndAcPen = String.Empty; //Activo,pensionista  del asegurado devuelto por dll de filiaci�n
		string vstInspec = String.Empty; //Inspecci�n Seg. Soc.  del asegurado devuelto por dll de filiaci�n

		//Dim mCamMed() As Cambio_se_me_en   'Matriz de Cambio M�dico/Servicio
		int DimMtMed = 0;
		int iMed = 0; //contador cambio m�dico

        Cambio_entidad[] mCamEnt = null; //Matriz de Cambio Entidad
        int DimMtEnt = 0;
		int iEnt = 0; //contador cambio Entidad

        Anulacion_ingreso[] mAnul = null; //Matriz de anulacion
        int DimMtAnul = 0;
		int iAnul = 0; //contador anulacion
		bool vfAnul = false; //si es falso vuelve de anulaci�n y no debe buscar el paciente

		Cambio_R�gimen[] mCamRegimen = null; //Matriz de Cambio R�gimen
		int DimMtRegimen = 0;
		int iRegimen = 0; //contador cambio Regimen


		string sqlCrystal = String.Empty;
        CrystalWrapper.Crystal Crystal2 = null;

		bool vfNuevoPac = false; //si es falso no ha ingresado uno nuevo

		bool bOrdenacion = false; //lo utilizo para saber si debo ordenar o no
		int iColOrden = 0; //columna de ordenacion
		UpgradeHelpers.Spread.SortKeyOrderConstants iTipoOrden = (UpgradeHelpers.Spread.SortKeyOrderConstants) 0; //tipo de ordenacion 0,1,2
		bool bLoad = false; //es true cuando esta cargando la pantalla

		//OSCAR 15/03/04
		Mensajes.ClassMensajes clase_mensaje = null;
		bool bNoInfDePac = false;
		//---------
		public int TECLAFUNCION = 0;
		
		//********************************************************************************
		//*                                                                              *
		//*  Modificaciones:                                                             *
		//*                                                                              *
		//*      O.Frias 21/02/2007  Realizo la operativa para mostrar el tel�fono.      *
		//*                                                                              *
		//********************************************************************************
		public menu_ingreso()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}
        private void Menu_alta_PreviewKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
            int hotKey = IMDHOPEN.TeclaFuncionOpen(Conexion_Base_datos.RcAdmision);
            if (e.KeyCode == (Keys)Enum.Parse(typeof(Keys), hotKey.ToString()))
            {
                if (SprPacientes.ActiveRowIndex > 0)
                {
                    SprPacientes.Row = SprPacientes.ActiveRowIndex;
                    string Episodio = sValorColumna(SprPacientes, Conexion_Base_datos.col_Nosequecolumnaes);
                    string Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
                    string ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
                    Episodio = "H" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
                    string Paciente = sValorColumna(SprPacientes, Conexion_Base_datos.col_IdenPac);
                    string tempRefParam = this.Name;
                    string tempRefParam2 = "sprPacientes";
                    bool tempRefParam3 = true;
                    IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, tempRefParam, tempRefParam2, Paciente, Episodio, tempRefParam3, 0, 0);
                    IMDHOPEN = null;
                }
            }
        }
        public void carga_grid_pacientes(UpgradeHelpers.Spread.FpSpread rejilla)
		{
            
            int longi = 100;
			// SE A�ADE UNA COLUMNA NO OCULTA CON LOS DATOS DE LA FECHA PREVISTA DE ALTA
			// COLUMNA NUMERO 5 (LA 4 ES EL EPISODIO -> OCULTA)
			// COLUMNA NUMERO 6 sociedad

			// oscar 17/12/2003
			// COLUMNA NUMERO 8  --> SERVICIO ACTUAL
			// 30/03/04
			// COLUMNA NUMERO 9 --> FECHA DE AVISO
			//---------
			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();

			//************************ O.Frias 21/02/2007 ************************
			rejilla.MaxCols = 13; //9 '8 '7 oscar 30/10/2004
                                  //************************ O.Frias 21/02/2007 ************************
           // rejilla.Columns[1] = new GridViewImageColumn();
            rejilla.Row = 0;
            rejilla.Col = Conexion_Base_datos.col_Plaza;
            rejilla.Text = "Plaza";
            rejilla.SetColWidth(Conexion_Base_datos.col_Plaza, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 20);


            rejilla.Col = Conexion_Base_datos.col_AutoInfor;
            SprPacientes.MasterTemplate.Columns[1] = new GridViewImageColumn();
            rejilla.Text = " ";
            rejilla.SetColWidth(Conexion_Base_datos.col_AutoInfor, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 20);
            SprPacientes.SetColHidden(SprPacientes.Col, bNoInfDePac);

            rejilla.Col = Conexion_Base_datos.Col_Paciente;
            rejilla.Text = LitPersona;
            rejilla.SetColWidth(Conexion_Base_datos.Col_Paciente, CreateGraphics().MeasureString(rejilla.Text, Font).Width + longi + 60);
            rejilla.Row = 0;

            rejilla.Col = Conexion_Base_datos.col_Fingreso;
            rejilla.Text = "Fecha ingreso";
            rejilla.SetColWidth(Conexion_Base_datos.col_Fingreso, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 20);
            rejilla.Row = -1;           

            rejilla.Col = Conexion_Base_datos.col_Nosequecolumnaes;
            rejilla.Text = " ";
            rejilla.SetColHidden(rejilla.Col, true);
            rejilla.Row = 0;

            // FECHA PREVISTA DE ALTA PERO VISIBLE
            rejilla.Col = Conexion_Base_datos.col_Fprevalta;
            rejilla.Text = "Fecha prev.alta";
            rejilla.SetColWidth(Conexion_Base_datos.col_Fprevalta, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 25);
            rejilla.Row = -1;
            //rejilla.CellType = 0 'tipo date
            rejilla.Row = 0;


            //sociedad
            rejilla.Col = Conexion_Base_datos.col_Sociedad;
            rejilla.Text = "Sociedad";
            rejilla.SetColWidth(Conexion_Base_datos.col_Sociedad, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 150);
            rejilla.Row = 0;


            //Servicio
            rejilla.Col = Conexion_Base_datos.col_Servingr;
            rejilla.Text = "Servicio ingreso";
            rejilla.SetColWidth(Conexion_Base_datos.col_Servingr, CreateGraphics().MeasureString(rejilla.Text, Font).Width + longi);
            rejilla.Row = 0;

            //oscar
            //17/12/2003 Servicio Actual
            rejilla.Col = Conexion_Base_datos.col_ServActual;
            rejilla.Text = "Servicio Actual";
            rejilla.SetColWidth(Conexion_Base_datos.col_ServActual, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 80);
            rejilla.Row = 0;

            //************************ Jes�s m�dico responsable************************
            rejilla.Col = Conexion_Base_datos.col_MedResp;
            rejilla.Text = "M�dico responsable";
            rejilla.SetColWidth(Conexion_Base_datos.col_MedResp, CreateGraphics().MeasureString(rejilla.Text, Font).Width + longi);
            rejilla.Row = 0;

            //30/04/2004 Fecha de aviso
            rejilla.Col = Conexion_Base_datos.col_FAviso;
            rejilla.Text = "Fecha de Aviso";
            rejilla.SetColWidth(Conexion_Base_datos.col_FAviso, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 25);
            rejilla.Row = 0;
            //-------------

            //************************ O.Frias 21/02/2007 ************************
            rejilla.Col = Conexion_Base_datos.col_Telcama;
            rejilla.Text = "Tel�fono Habitaci�n";
            rejilla.SetColWidth(Conexion_Base_datos.col_Telcama, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 25);
            rejilla.Row = 0;

            //************************ O.Frias 21/02/2007 ************************
            rejilla.Col = Conexion_Base_datos.col_IdenPac;
            rejilla.Text = "Gidenpac";
            rejilla.SetColWidth(Conexion_Base_datos.col_IdenPac, CreateGraphics().MeasureString(rejilla.Text, Font).Width + 30);
            rejilla.Row = 0;
            rejilla.SetColHidden(rejilla.Col, true);


        }
		private void proOrdenGrid(int iCol, int iTipo, bool bActivate = false)
		{            
            SprPacientes.Row = 1;
			SprPacientes.Col = 1;
			SprPacientes.Row2 = SprPacientes.MaxRows;
			SprPacientes.Col2 = SprPacientes.MaxCols;

			SprPacientes.SortBy = UpgradeHelpers.Spread.SortByConstants.SortByRow; //Ordeno por columna
			SprPacientes.SetSortKey(1, iCol);
			if (!bActivate)
			{
				switch(iTipo)
				{
					case 1 : 
						SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending); 
						break;
					case 2 : case 0 : 
						SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending); 
						break;
				}

			}
			else
			{
				if (bLoad)
				{
					SprPacientes.SetSortKey(1, 1);
					SprPacientes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending);
				}
			}
			SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
            //SprPacientes.Rows[0].IsCurrent = true;


        }
		public void cbRefrescar_Click(Object eventSender, EventArgs eventArgs)
		{
			Llenar_sprGrid();			
			menu_ingreso_Activated(this, new EventArgs());
		}
		private void Command1_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
		private void menu_ingreso_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				if (Conexion_Base_datos.NoHacerActivate)
				{
					Conexion_Base_datos.NoHacerActivate = false;
					return;
				}
				int tlRow = 0;
				Application.DoEvents();
				int n = Application.OpenForms.Count;
				//Llenar_sprGrid
				SprPacientes.Row = 1;
				SprPacientes.Col = 0;
				SprPacientes.Col2 = 2;
				SprPacientes.Row2 = 1;
				SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
				Conexion_Base_datos.vstActivado_Ingreso = true;

				proVerMenus(true); //Activo los menus
				proActivar(true); // Activo  los botones
                
                proOrdenGrid(iColOrden, (int) iTipoOrden, true);
				if (vfAnul)
				{
					proSeleccionado();
				}
				else
				{
					vstGidenpac = fnRecogerPac(SprPacientes.CurrentRow.Index);
				}
				proPermisoAnulacion();
                _Toolbar1_Button7.Enabled = Conexion_Base_datos.fnHistoria(vstGidenpac) == "";                
				SprPacientes.Focus();
				bLoad = false;


                //OSCAR 15/03/04
                sdcPrevision.NullableValue = null;
				//sdcAviso.Value = DateTime.Parse("");
				tbHoraPrevision.Text = "  :  ";
				tbHoraAviso.Text = "  :  ";
				clase_mensaje = new Mensajes.ClassMensajes();
                _Toolbar1_Button15.Enabled = false;                
				menu_admision.DefInstance.mnu_adm011[15].Enabled = false;
                //----------
                //Oscar C Enero 2010
                //El boton de Conversion de Episodios de HD a Episodio de Hospitalizacion se habilitara cuando
                //se seleccione un episodio de hospital de dia y ademas este habilitada la opcion de ingresos en HD
                menu_admision.DefInstance.mnu_adm011[19].Enabled = ColorTranslator.ToOle(SprPacientes.Rows[SprPacientes.Row-1].Cells[SprPacientes.Col-1].Style.ForeColor) == ColorTranslator.ToOle(Line2.BorderColor) && _Toolbar1_Button12.Visibility == Telerik.WinControls.ElementVisibility.Visible;
                //----------------


                ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
				string tempRefParam = this.Name;
				IMDHOPEN.EstableceUltimoForm(tempRefParam, Conexion_Base_datos.VstCodUsua);
                _Toolbar1_Button22.Visibility = (IMDHOPEN.TeclaFuncionBotonVisible(Conexion_Base_datos.RcAdmision)) ? Telerik.WinControls.ElementVisibility.Visible : Telerik.WinControls.ElementVisibility.Collapsed;                
                IMDHOPEN = null;
                _Toolbar1_Button22.Enabled = true;                

			}
		}
		public void refreco_demantecamas()
		{
			Llenar_sprGrid();			
			menu_ingreso_Activated(this, new EventArgs());
		}		
		private void menu_ingreso_Load(Object eventSender, EventArgs eventArgs)
		{
			bLoad = true;
			orden_columna = 1;
            
			//Me.Top = 10
			//Oscar C Febrero 2010
			proRecolocaControles();           
            //-------------
            //Mostrar al usuario si el paciente deja o no informar a las personas que preguntan por �l.
            string stValTemp = mbAcceso.ObtenerCodigoSConsglo("INFOPACI", "valfanu1", Conexion_Base_datos.RcAdmision);
			bNoInfDePac = stValTemp != "S";

			SprPacientes.Col = Conexion_Base_datos.col_AutoInfor;
            //camaya todo_x_3
			//SprPacientes.SetColHidden(SprPacientes.Col, bNoInfDePac);
			pctAutoInfor.Visible = !bNoInfDePac;
			lblAutoInfor.Visible = !bNoInfDePac;

			if (!mbAcceso.AplicarControlAcceso(Conexion_Base_datos.VstCodUsua, "A", this, "AGI100F1", ref Conexion_Base_datos.astrControles, ref Conexion_Base_datos.astrEventos))
			{
				this.Close();
			}
			//oscar 11/12/2003
			//Se visualiza la leyenda "Plazas de D�a" si la constante gloval VERCAMAS tiene
			//en visualizacion de sillon ="S", es decir, existe hospital de dia
			Line2.Visible = Conexion_Base_datos.bGestionHD;
			Label3.Visible = Conexion_Base_datos.bGestionHD;            
            _Toolbar1_Button12.Visibility = (Conexion_Base_datos.bGestionHD) ? Telerik.WinControls.ElementVisibility.Visible : Telerik.WinControls.ElementVisibility.Collapsed;            
            Label4.Visible = Conexion_Base_datos.bGestionHD;
			Line3.Visible = Conexion_Base_datos.bGestionHD;
			//-------
			//oscar 30/03/2004
			//Se visualiza el boton "cambio de tipo de producto" si la constante global GESPRODUCT esta
			//establecida a "S"
            _Toolbar1_Button15.Visibility = (Conexion_Base_datos.bGestionProducto) ? Telerik.WinControls.ElementVisibility.Visible : Telerik.WinControls.ElementVisibility.Collapsed;
            
			//----
			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.ToUpper() + "S";
			Label2.Text = LitPersona + " INGRESADOS";

			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
			Label1.Text = LitPersona + " ausentes temporalmente";
            _Toolbar1_Button1.ToolTipText = "Ingreso de " + LitPersona.ToLower();
            _Toolbar1_Button8.ToolTipText = "Cambio identificaci�n del " + LitPersona.ToLower();
            _Toolbar1_Button9.ToolTipText = "Listados del " + LitPersona.ToLower();
            _Toolbar1_Button11.ToolTipText = "Consumos por " + LitPersona.ToLower();            

			//**********Deshabilito todo hasta la selecci�n*******
			proActivar(false);
			vfAnul = true;
			//***************************************************
			SprPacientes.Visible = true;
			carga_grid_pacientes(SprPacientes);
            //*********************conexion crystal***********************
            Crystal2 = Conexion_Base_datos.LISTADO_CRYSTAL;
			Llenar_sprGrid();            
            bOrdenacion = true;
            Label6.Text = "Fecha previsi�n" + Environment.NewLine + "de alta";

        }
		private void menu_ingreso_FormClosing(Object eventSender, FormClosingEventArgs eventArgs)
		{
			int Cancel = (eventArgs.Cancel) ? 1 : 0;
			int UnloadMode = (int) eventArgs.CloseReason;
			if (iEnt > 0)
			{
				Cancel = 69;
				RadMessageBox.Show("Debe cerrar la pantalla" + "\r" + "de cambio de Entidad", Application.ProductName);
                mCamEnt[iEnt].cbCancelar.Focus();
			}

			if (iMed > 0)
			{
				Cancel = 69;
                RadMessageBox.Show("Debe cerrar la pantalla" + "\r" + "de cambio de M�dico/Servicio", Application.ProductName);
                //camaya todo_x_3
                //mCamMed(iMed).CbCancelar.SetFocus
            }

            if (iRegimen > 0)
			{
				Cancel = 69;
				RadMessageBox.Show("Debe cerrar la pantalla" + "\r" + "de cambio de R�gimen", Application.ProductName);
				mCamRegimen[iRegimen].CbCancelar.Focus();
			}


			eventArgs.Cancel = Cancel != 0;
		}
		private void menu_ingreso_Closed(Object eventSender, EventArgs eventArgs)
		{
            Crystal2 = null;
            proVerMenus(false);            
            menu_admision.DefInstance.mnu_adm011[0].Visibility = Telerik.WinControls.ElementVisibility.Visible;
			Conexion_Base_datos.vstActivado_Ingreso = false;
		}        
		private void SprPacientes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            int Col = 0;
            int Row = 0;
                        	
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;                
            }
            else
            {
                Col = SprPacientes.Col;
                Row = SprPacientes.Row;
                
            }
            if (Row == 0)
			{ //Compruebo que es la cabecera
				if (bOrdenacion)
				{
					iColOrden = Col;
					//iTipoOrden = SprPacientes.GetSortKeyOrder(1);
					//proOrdenGrid(iColOrden, (int) iTipoOrden);
					lRow = 1;
					proSeleccionado();
				}
			}
			else
			{
				//Selecciono un paciente

				vstGidenpac = fnRecogerPac(Row); //Recojo los datos del paciente
				vfAnul = true;
                _Toolbar1_Button7.Enabled = Conexion_Base_datos.fnHistoria(vstGidenpac) == "";                  
				proPermisoAnulacion(); //puede o no anular el ingreso
				if (!fnComprobar_pac())
				{
					if (vstGidenpac.Trim() != "")
					{							
						short tempRefParam = 1200;
						string[] tempRefParam2 = new string[]{"El paciente", "dado de alta"};
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
					}
					Llenar_sprGrid();
					lRow = 0;
					proSeleccionado();
					return;
				}
				lRow = Row;
				//SprPacientes.OperationMode = OperationModeRow

				//OSCAR 15/03/04
				//Fecha prevista de alta
				SprPacientes.Col = Conexion_Base_datos.col_Fprevalta;
				if (Strings.Len(SprPacientes.Text) != 0)
				{
					sdcPrevision.Value = DateTime.Parse(DateTime.Parse(SprPacientes.Text).ToString("dd/MM/yyyy"));
					tbHoraPrevision.Text = DateTime.Parse(SprPacientes.Text).ToString("HH:mm");
					//buscar la fecha de aviso
				}
				else
				{
                    sdcPrevision.NullableValue = null;                    
					tbHoraPrevision.Text = "  :  ";
				}

				//oscar 30/03/04
				SprPacientes.Col = Conexion_Base_datos.col_FAviso;
				if (Strings.Len(SprPacientes.Text) != 0)
				{
					sdcAviso.Value = DateTime.Parse(DateTime.Parse(SprPacientes.Text).ToString("dd/MM/yyyy"));
					tbHoraAviso.Text = DateTime.Parse(SprPacientes.Text).ToString("HH:mm");
				}
				else
				{
                    sdcAviso.NullableValue = null;                    
					tbHoraAviso.Text = "  :  ";
				}

				//oscar 30/03/03
				//El boton de cambio de producto se habilita cuando exista gestion por producto y se
				//haya registrado producto en el ingreso
				if (Conexion_Base_datos.bGestionProducto)
				{
					if (TieneProducto())
					{
                    _Toolbar1_Button15.Enabled = true;
						menu_admision.DefInstance.mnu_adm011[15].Enabled = true;
					}
					else
					{
                        _Toolbar1_Button15.Enabled = false;                            
						menu_admision.DefInstance.mnu_adm011[15].Enabled = false;
					}
				}
                //------------

                //Oscar C Enero 2010
                //El boton de Conversion de Episodios de HD a Episodio de Hospitalizacion se habilitara cuando
                //se seleccione un episodio de hospital de dia y ademas este habilitada la opcion de ingresos en HD
                menu_admision.DefInstance.mnu_adm011[19].Enabled = ColorTranslator.ToOle(SprPacientes.Rows[SprPacientes.Row -1].Cells[SprPacientes.Col].Style.ForeColor) == ColorTranslator.ToOle(Line2.BorderColor) && _Toolbar1_Button12.Visibility == Telerik.WinControls.ElementVisibility.Visible;
                //----------------

                //menu_admision!mnu_adm011(21).Enabled = True
                _Toolbar1_Button23.Enabled = true;

			}
        }

        private void SprPacientes_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;
            if (Row != 0)
            {
                string tempRefParam = "";
				if (!mbAcceso.PermitirEvento(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.astrEventos, SprPacientes.Name, tempRefParam, "DblClick"))
				{
					return;
				}
                //********************RECOGER LOS DATOS DEL PACIENTE DEL SPREAD*************************                
                lRow = Row;
                vstGidenpac = fnRecogerPac(Row);
                //*************************futura DLL****************************************
                this.Cursor = Cursors.WaitCursor;

				//oscar 16/12/2003
				//Los episodios menores a 1900 corresponden a episodios de hospital de dia
				//por lo que debemos acceder a la correspondiente DLL
				//Set mcIngreso = CreateObject("IngresoDLL.Ingreso_clase")
				//mcIngreso.proCargaIngreso Me, mcIngreso, vstGidenpac, "C", "", "", "H", "", "", gEnfermeria, RcAdmision, Crystal2, App.Path, VstCrystal, gUsuario, gContrase�a, VstCodUsua
				//Set mcIngreso = Nothing
				SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
				if (Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(0, Math.Min(SprPacientes.Text.IndexOf('/'), SprPacientes.Text.Length)))) < 1900)
				{
                    //camaya todo_7_3
                    //mcIngreso = new IngresoHD.Ingreso_clase();
					
					//mcIngreso.proCargaIngreso(this, mcIngreso, vstGidenpac, "C", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
					mcIngreso = null;
				}
				else
				{
                    //camaya todo_7_3                    
                    //mcIngreso = new IngresoDLL.Ingreso_clase();
					
					//mcIngreso.proCargaIngreso(this, mcIngreso, vstGidenpac, "C", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
					mcIngreso = null;
				}
				//-------------
				this.Show();
				Llenar_sprGrid();
				proSeleccionado();
				this.Cursor = Cursors.Default;
			}
		}
		private void SprPacientes_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{			
            SprPacientes.SortDescriptors.Clear();
            /*SprPacientes.Row = SprPacientes.CurrentCell.RowIndex;
            SprPacientes.Col = SprPacientes.CurrentCell.ColumnIndex;*/
            SprPacientes_CellClick(SprPacientes,null);
		}
		private void SprPacientes_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float x = (float) (eventArgs.X * 15);
			float y = (float) (eventArgs.Y * 15);
			UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
			int i = 0;
			ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
			string Paciente = String.Empty;
			string Numero = String.Empty;
			string ano = String.Empty;
			string Episodio = String.Empty;
            if (eventArgs.Button == MouseButtons.Right)
            {
				if (SprPacientes.ActiveRowIndex > 0)
				{
					UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
					SprPacientes.Row = SprPacientes.ActiveRowIndex;
					Episodio = sValorColumna(SprPacientes, Conexion_Base_datos.col_Nosequecolumnaes);
					Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
					ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
					Episodio = "H" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
					Paciente = sValorColumna(SprPacientes, Conexion_Base_datos.col_IdenPac);
					UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
					IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
					string tempRefParam = this.Name;
					string tempRefParam2 = "sprPacientes";
					bool tempRefParam3 = false;
                    IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, tempRefParam, tempRefParam2, Paciente, Episodio, tempRefParam3, Mouse.X, Mouse.Y);
					IMDHOPEN = null;
				}
			}
		}
		private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
            CommandBarButton Button = (CommandBarButton) eventSender;
			object Clase = null;
			this.Cursor = Cursors.WaitCursor;
			UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
			int i = 0;
			string Paciente = String.Empty;
			string Numero = String.Empty;
			string ano = String.Empty;
			string Episodio = String.Empty;
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
			if (radCommandBarStripElement1.Items.IndexOf(Button) + 1 == 1 || radCommandBarStripElement1.Items.IndexOf(Button) + 1 == 12)
			{
				//cuando es el primer boton
				if (radCommandBarStripElement1.Items.IndexOf(Button) + 1 == 1)
				{
					proMnu_ing01(0);
				}
				else
				{
					proMnu_ing01(15);
				}
			}
			else
			{
				//comprobar que hay un registro seleccionado
				if (SprPacientes.ActiveRowIndex > 0)
				{
					switch(radCommandBarStripElement1.Items.IndexOf(Button) + 1)
					{
						case 1 :  //nuevo paciente 
							proMnu_ing01(0); 
							break;
						case 2 :  //datos filiacion 
							proMant_Datos(0); 
							break;
						case 3 :  //servio,m�dico 
							proMnu_ing01(3); 
							break;
						case 4 :  //cambio entidad 
							proMnu_ing01(4); 
							break;
						case 5 :  //pantalla anulacion 
							proMnu_ing01(5); 
							break;
						case 6 :  //documentos 
							proMenu_doc(); 
							break;
						case 7 :  //pedir historia 
							proMnu_ing01(8); 
							break;
						case 8 :  // cambiar identificacion 
							proMnu_ing01(9); 
							break;
						case 9 :  //presenta pantalla de documentos 
							proMnu_ing01(10); 
							break;
						case 10 : 
							proMnu_ing01(11); 
							break;
						case 11 : 
							proMnu_ing01(14); 
							 
							//oscar 18/03/04 
							break;
						case 13 :  //Registro de resolucion al juzgado 
							proMnu_ing01(16); 
							break;
						case 14 :  //Control de citas externas 
							proMnu_ing01(17); 
							break;
						case 15 :  //Cambio de producto 
							proMnu_ing01(18); 
							//------------ 
							 
							//oscar 16/12/04 
							break;
						case 16 :  //Pacientes hospitalizados							
							menu_admision.DefInstance.mnu_adm01401_Click(menu_admision.DefInstance.mnu_adm01401, new EventArgs()); 
							break;
						case 17 :  //Pacientes no hospitalizados 							
							menu_admision.DefInstance.mnu_adm01401_Click(menu_admision.DefInstance.mnu_adm01401, new EventArgs()); 
							break;
						case 18 :  //Consulta historia clinica 
							RegistrarDsnAccess(Conexion_Base_datos.BDtempo);
                            //camaya todo_x_3
                            //Clase = new HistoriaClinicaPaciente.MCHClinicaPac(); 
							if (vstGidenpac != "")
							{
                                
                                //camaya todo_x_3
                                //Clase.Llamada(this, Clase, Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.stNombreDsnACCESS, Crystal2, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.BDtempo, vstGidenpac);
							}
							else
							{
                                
                                //camaya todo_x_3
                                //Clase.Llamada(this, Clase, Conexion_Base_datos.RcAdmision, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.stNombreDsnACCESS, Crystal2, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.BDtempo);
							} 
							Clase = null; 
							break;
						case 19 :  // Sondeo de Episodios 
							RegistrarDsnAccess(Conexion_Base_datos.BDtempo);
                            Sondeo.ClassSondeo sondeo = new Sondeo.ClassSondeo();
                            sondeo.Load(sondeo, this, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo);
                            sondeo = null;
                            break;
						case 20 : 
							proMnu_ing01(19); 
							//------------- 
							break;
						case 21 : 
							proMnu_ing01(21);  //Presupuestos y dep�sitos. 
							break;
						case 22 : 
							Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI(); 
							if (SprPacientes.ActiveRowIndex > 0)
							{
								SprPacientes.Row = SprPacientes.ActiveRowIndex;
								Episodio = sValorColumna(SprPacientes, Conexion_Base_datos.col_Nosequecolumnaes);
								Numero = Episodio.Substring(Episodio.IndexOf('/') + 1);
								ano = Episodio.Substring(0, Math.Min(Episodio.IndexOf('/'), Episodio.Length));
								Episodio = "H" + Conversion.Str(ano).Trim() + Conversion.Str(Numero).Trim();
								Paciente = sValorColumna(SprPacientes, Conexion_Base_datos.col_IdenPac);
								UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
								IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();                       
                                IMDHOPEN.LlamadaSP(Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, this.Name, "OpenImdh", Paciente, Episodio, false, Mouse.X, Mouse.Y);
								IMDHOPEN = null;
							} 
							break;
						case 23 : 
							//' 04/04/2014 
							//' Llamada a la documentos aportados a HC 
							proMnu_ing01(23); 
							break;
					}

				}
				else
				{					
					short tempRefParam = 1270;
					string[] tempRefParam2 = new string[]{"un paciente"};
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				}
			}
			this.Cursor = Cursors.Default;
		}
		//****************************************************************************************
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*      O.Frias 21/02/2007  Incorporo la nueva operativa de tel�fono                    *
		//*                                                                                      *
		//****************************************************************************************
		public void Llenar_sprGrid()
		{
			//lleno el grid con los pacientes ingresados
			DataSet RrIngresos = null;
			string stIngresos = String.Empty; //Select para abrir el RrIngresos
			SqlCommand RqIngresos = new SqlCommand();
			Color vstColoringreso = new Color();
			int lacol = 0;
			int larow = 0;
			DLLTextosSql.TextosSql oTextoSql = null;
			//Para recorrer el Spread
			int iMaxFilas = 0; //Almacenar el m�ximo n�mero de filas
			string stcama = String.Empty; //Para rellenar las camas
                                          //********************
            SprPacientes.BeginUpdate();
			try
			{ //mensajes de error
                                
                SprPacientes.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				object[] MatrizSql = new object[1];
				oTextoSql = new DLLTextosSql.TextosSql();				
				string tempRefParam = "ADMISION";
				string tempRefParam2 = "AGI100F1";
				string tempRefParam3 = "Llenar_sprGrid";
				short tempRefParam4 = 1;
                stIngresos = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, MatrizSql, Conexion_Base_datos.RcAdmision));
				oTextoSql = null;

				RqIngresos.Connection = Conexion_Base_datos.RcAdmision;
				RqIngresos.CommandText = stIngresos;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(RqIngresos);
				RrIngresos = new DataSet();
				tempAdapter.Fill(RrIngresos);
				//*********************COMPROBAR				
                if (RrIngresos.Tables[0].Rows.Count != 0)
				{
                    SprPacientes.MaxRows = 0;
                    //iMaxFilas = RrIngresos.Tables[0].Rows.Count - 1; //Cuento cuantos registros hay y seran el n�mero de columnas
                    iMaxFilas = RrIngresos.Tables[0].Rows.Count;
                    //SprPacientes.MaxRows = iMaxFilas;
                    //***BUCLE FOR PARA LLENAR EL SPREAD CON LOS PACIENTES DEL HOSPITAL INGRESADOS******
                    for (int iFilas = 0; iFilas < iMaxFilas; iFilas++)
					{
                        SprPacientes.MaxRows++; 

                        SprPacientes.Row  = iFilas + 1 ;
						SprPacientes.Col = Conexion_Base_datos.col_Plaza;

						//oscar 11/12/2003
						//Pintamos de color diferenciativo a los ingresados en plazas de dia
						//Los episodios anteriores a 1900 son los nuevos episodios de hospital de
						//dia que se han realizado con un nuevo contador.
						//Ademas se deberan identificar los pacientes de plazas de dia ausentes con
						//un color propio
						//If Not IsNull(RrIngresos("gmotause")) Then
						//    vstColoringreso = Rojo
						//Else
						//    vstColoringreso = Negro
						//End If
						if (Convert.ToDouble(RrIngresos.Tables[0].Rows[iFilas]["ganoadme"]) < 1900)
						{
							if (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["gmotause"]))
							{
								vstColoringreso = Line3.BorderColor;
							}
							else
							{
								vstColoringreso = Line2.BorderColor;
							}
						}
						else
						{
							if (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["gmotause"]))
							{
								vstColoringreso = Conexion_Base_datos.Rojo;
							}
							else
							{
								vstColoringreso = Conexion_Base_datos.Negro;
							}
						}
						//---------------------------------                        
                        stcama = Convert.ToString(int.Parse(RrIngresos.Tables[0].Rows[iFilas]["Gplantas"].ToString()).ToString("D2")) + Convert.ToString(int.Parse(RrIngresos.Tables[0].Rows[iFilas]["GHABITAC"].ToString()).ToString("D2")) + Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["GCAMASBO"]).ToUpper();

                        SprPacientes.Text = stcama;
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;
						SprPacientes.TypeHAlign = ContentAlignment.MiddleCenter;

                        SprPacientes.Col = Conexion_Base_datos.col_AutoInfor;

                        SprPacientes.setTypePictCenter(true);						
                        SprPacientes.setTypePictStretch(true);
                        

                        if (!bNoInfDePac)
						{
							if ((Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["iautoinf"]) + "").Trim().ToUpper() == "N")
							{
                                SprPacientes.setTypePictPicture(pctAutoInfor.Image);
							}
						}
						SprPacientes.Lock = true;

						SprPacientes.Col = Conexion_Base_datos.Col_Paciente;
						SprPacientes.Text = Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["DAPE1PAC"]).Trim() + " " + Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["DAPE2PAC"]).Trim() + " , " + Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["DNOMBPAC"]).Trim();
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;

						SprPacientes.Col = Conexion_Base_datos.col_Fingreso;
						SprPacientes.Text = Convert.ToDateTime(RrIngresos.Tables[0].Rows[iFilas]["FLLEGADA"]).ToString("dd/MM/yyyy");
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;
                        SprPacientes.TypeHAlign = ContentAlignment.TopLeft;

						//Javier 13/02/2002 A�adido porque si no no deja introducir datos en la columna
						//***************************************************************************
						SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
						SprPacientes.Col = SprPacientes.Col;
						SprPacientes.Row = iFilas + 1;
						SprPacientes.Col2 = SprPacientes.Col;
						SprPacientes.Row2 = iFilas;
						SprPacientes.Lock = false;
						//***************************************************************************
						SprPacientes.Row = iFilas + 1;
						SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;						
						SprPacientes.Text = Conversion.Str(RrIngresos.Tables[0].Rows[iFilas]["ganoadme"]).Trim() + "/" + Conversion.Str(RrIngresos.Tables[0].Rows[iFilas]["gnumadme"]).Trim();
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;
						SprPacientes.Col = Conexion_Base_datos.col_Fprevalta;
						//oscar 01/03/2004
						//SprPacientes.Text = IIf(Not IsNull(RrIngresos("fprevalt")), RrIngresos("fprevalt"), "")
						SprPacientes.Text = (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["fprevalt"])) ? Convert.ToDateTime(RrIngresos.Tables[0].Rows[iFilas]["fprevalt"]).ToString("dd/MM/yyyy HH:mm") : "";
						//----------
						SprPacientes.Lock = true;
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.TypeVAlign = ContentAlignment.MiddleCenter;

						SprPacientes.Col = Conexion_Base_datos.col_Sociedad;
						SprPacientes.Text = Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["dsocieda"]).Trim();
						SprPacientes.Lock = true;
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Col = Conexion_Base_datos.col_Servingr;
						SprPacientes.Text = Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["dnomserv"]).Trim();
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;

						//oscar 11/12/2003
						SprPacientes.Col = Conexion_Base_datos.col_ServActual;
						SprPacientes.Text = Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["ServActual"]).Trim();
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;
						//---------------

						SprPacientes.Col = Conexion_Base_datos.col_MedResp;
						SprPacientes.Text = (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["dnommedi"])) ? Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["dnommedi"]) : "";
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;

						SprPacientes.Col = Conexion_Base_datos.col_FAviso;
						SprPacientes.Text = (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["faviso"])) ? Convert.ToDateTime(RrIngresos.Tables[0].Rows[iFilas]["faviso"]).ToString("dd/MM/yyyy HH:mm") : "";
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;
						SprPacientes.TypeHAlign = ContentAlignment.MiddleCenter;

						SprPacientes.Col = Conexion_Base_datos.col_Telcama;
						SprPacientes.Text = (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["ntelecam"])) ? Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["ntelecam"]) : "";
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;						

						SprPacientes.Col = Conexion_Base_datos.col_IdenPac;
						SprPacientes.Text = (!Convert.IsDBNull(RrIngresos.Tables[0].Rows[iFilas]["gidenpac"])) ? Convert.ToString(RrIngresos.Tables[0].Rows[iFilas]["gidenpac"]) : "";
						SprPacientes.foreColor = vstColoringreso;
						SprPacientes.Lock = true;										

					}


					lacol = orden_columna;
					larow = 0;
					bOrdenacion = false;                     
                    SprPacientes.Row = larow;
                    SprPacientes.Col = lacol;                
                    SprPacientes_CellClick(SprPacientes, null);
					bOrdenacion = true;
				}
				else
				{
				SprPacientes.MaxRows = 0;
				SprPacientes.BlockMode = true;
				//SprPacientes.MaxRows = 20
				SprPacientes.Col = 1;
				SprPacientes.Row = 1;
				SprPacientes.Col2 = 4;
				SprPacientes.Row2 = 20;
				SprPacientes.Lock = true;
				SprPacientes.BlockMode = false;
				}
				RrIngresos.Close();
				RqIngresos = null;
                //SprPacientes.OperationMode = OperationModeRow                     
                
                //SprPacientes.OperationMode = (UpgradeHelpers.Spread.OperationModeEnums) (((int)UpgradeHelpers.Spread.OperationModeEnums.ReadOnly) + ((int)UpgradeHelpers.Spread.OperationModeEnums.RowMode));
                SprPacientes.EndUpdate();
            }
			catch (SqlException ex)
			{
                Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		public void proActivar(bool activo)
		{
            _Toolbar1_Button2.Enabled = activo;
            _Toolbar1_Button3.Enabled = activo;
            _Toolbar1_Button4.Enabled = activo;
            _Toolbar1_Button8.Enabled = activo;            

			//oscar 18/03/3004
			//AHORA SE VE PA TO EL MUNDO
			//Toolbar1.Buttons(13).Enabled = False
			//menu_admision!mnu_adm011(13).Enabled = False
			//-----
		}
		public string fnRecogerPac(int proRow)
		{

			//********************RECOGER LOS DATOS DEL PACIENTE DEL SPREAD*************************
			string result = String.Empty;
			string stCamas = String.Empty; //Columna de las camas
            int iNuPlan = 0;
            string iNuHab = String.Empty;
			string stNuCama = String.Empty;
			string tstRecoger = String.Empty;
			DataSet tRrRecoger = null;
			int iGan = 0;
			//**********************
			try
			{                
				//****************Compruebo que hay datos
				SprPacientes.Col = Conexion_Base_datos.col_Plaza;
				SprPacientes.Row = proRow;
				if (SprPacientes.Text != "")
				{
					stCamas = SprPacientes.Text;
					iNuPlan = Convert.ToInt32(Double.Parse(stCamas.Substring(0, Math.Min(2, stCamas.Length)))); //planta
					iNuHab = (stCamas.Substring(2, Math.Min(2, stCamas.Length - 2))).ToString(); //habitacion
					stNuCama = stCamas.Substring(4); //cama
					SprPacientes.Col = Conexion_Base_datos.Col_Paciente;
					SprPacientes.Row = proRow;
					stNombPaciente = SprPacientes.Text;
					SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
					iGan = (SprPacientes.Text.IndexOf('/') + 1);
					viGanoadme = Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(0, Math.Min(iGan - 1, SprPacientes.Text.Length)))); //ganoadme,ganoregi
					viGnumadme = Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(iGan))); //gnumadme,gnumregi
					vstGanoadme = SprPacientes.Text.Trim();
					tstRecoger = "select gidenpac from aepisadm where ganoadme=" + viGanoadme.ToString() + " and gnumadme=" + viGnumadme.ToString();
					SqlDataAdapter tempAdapter = new SqlDataAdapter(tstRecoger, Conexion_Base_datos.RcAdmision);
					tRrRecoger = new DataSet();
					tempAdapter.Fill(tRrRecoger);
					if (tRrRecoger.Tables[0].Rows.Count > 0)
					{						
						result = Convert.ToString(tRrRecoger.Tables[0].Rows[0]["gidenpac"]); //("gidenpac")
					}
					else
					{
					}					
					tRrRecoger.Close();
				}
				return result;
			}
			catch
			{
				//*************************
				//MsgBox "fnRecogerPac"
				return result;
			}
		}

		public void Recoger_datos(string Nombre2, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Idpaciente, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen = "", string Inspec = "")
		{
			if (Idpaciente != "")
			{
				Llenar_sprGrid();
			}
			vstAsegurado = Asegurado;
			vstIdPaciente = Idpaciente;
			vstCodsoc = CodSOC;
			vstIndAcPen = IndActPen;
			vstInspec = Inspec;
			vstHistoria = Historia;
		}     
           
        public void proQuitarMatriz(string sCodigo_quitar, Cambio_R�gimen Form)
        {
            Form[] mCamMed = null;

            Form[] aTemporal = null; // LA ESTRUCTURA mtemp se asigna a mtemporal que realmente es aFormularios
            Form[] mTemp = null; // SE VAN A�ADIENDO TODOS LOS FORMULARIOS MENOS EL QUE LLAMA
            int iInTemp = 0;
            Llenar_sprGrid();
            // copiamos la que nos viene a aTemporal
            if (Form.Name.ToUpper() == "CAMBIO_R�GIMEN")
            {
                aTemporal = new Form[mCamRegimen.GetUpperBound(0) + 1];
                for (int i = 0; i < mCamRegimen.GetUpperBound(0) + 1; i++)
                {
                    aTemporal[i] = mCamRegimen[i];
                }
                if (mCamRegimen != null)
                {
                    Array.Clear(mCamRegimen, 0, mCamRegimen.Length);
                }
            }
            else
            {
                //    ReDim aTemporal(UBound(mCamMed))
                //    For i = 1 To UBound(mCamMed)
                //        Set aTemporal(i) = mCamMed(i)
                //    Next
                //    Erase mCamMed
            }

            DimMtMed = aTemporal.GetUpperBound(0) + 1;

            for (int iMTemp = 0; iMTemp < DimMtMed; iMTemp++)
            {
                if (sCodigo_quitar == Convert.ToString(aTemporal[iMTemp].Tag))
                {
                    //No hago nada
                }
                else
                {
                    iInTemp++;
                    Array.Resize(ref mTemp, iInTemp);
                    mTemp[iInTemp - 1] = aTemporal[iMTemp];
                }
            }
            //Erase aTemporal
            if (Form.Name.ToUpper() == "CAMBIO_SE_ME_EN")
            {   //jproche Matriz de anulacion
                if (DimMtMed != 1)
                { // SOLO HAY UN FORMULARIO CARGADO
                    mCamMed = new Form[mTemp.GetUpperBound(0) + 1];
                    for (int i = 0; i < mTemp.GetUpperBound(0) + 1; i++)
                    {
                        mCamMed[i] = mTemp[i];
                    }
                    iMed = mCamMed.GetUpperBound(0) + 1;
                    vfAnul = true;
                }
                else
                {
                    iMed = 0;
                }
            }
            else
            {
                if (DimMtMed != 1)
                { // SOLO HAY UN FORMULARIO CARGADO
                    mCamRegimen = new Cambio_R�gimen[mTemp.GetUpperBound(0) + 1];
                    for (int i = 0; i < mTemp.GetUpperBound(0) + 1; i++)
                    {
                        mCamRegimen[i] = (Cambio_R�gimen)mTemp[i];
                    }
                    iRegimen = mCamRegimen.GetUpperBound(0) + 1;
                }
                else
                {
                    iRegimen = 0;
                }
            }
            if (mTemp != null)
            {
                Array.Clear(mTemp, 0, mTemp.Length);
            }
            proGanoadme(sCodigo_quitar);
        }		

        public void proQuitar_entidad(string Codigo_quitar)
        {
            Cambio_entidad[] METemp = null;
            int iInETemp = 0;

            Llenar_sprGrid();
            DimMtEnt = mCamEnt.GetUpperBound(0) + 1;

            for (int iMETemp = 0; iMETemp < DimMtEnt; iMETemp++)
            {
                if (Codigo_quitar == Convert.ToString(mCamEnt[iMETemp].Tag))
                {   //No hago nada
                    vstGidenpac = Codigo_quitar;
                }
                else
                {
                    iInETemp++;
                    Array.Resize(ref METemp, iInETemp);
                    METemp[iInETemp - 1] = mCamEnt[iMETemp];
                }
            }
            if (mCamEnt != null)
            {
                Array.Clear(mCamEnt, 0, mCamEnt.Length);
            }

            mCamEnt = new Cambio_entidad[iInETemp];
            for (int iMETemp = 0; iMETemp < iInETemp; iMETemp++)
            {
                mCamEnt[iMETemp] = METemp[iMETemp];
            }

            if (METemp != null)
            {
                Array.Clear(METemp, 0, METemp.Length);
            }
            iEnt = mCamEnt.GetUpperBound(0) + 1;
            vfAnul = true;
            proGanoadme(Codigo_quitar);
        }


        //************************************************************************************
        //* O.Frias (18/06/2007) Modifico la llamada de ausencias.
        //************************************************************************************
        public void proMnu_ing01(int Index)
		{
            object CDAyuda = null;
			ConsumosPac.clsConsPac ClaseConsumos = null;
			System.DateTime fllegada = DateTime.FromOADate(0);
			object UniEnf = null;
			//****************MENU DE INGRESO
			string Respuesta = String.Empty;
            //Dim Cambio_med As Cambio_se_me_en   'Ventana cambio m�dico
            DLLPCambioID.MCCambioID vstcambio_pac = null;
			//********Cambio servicio/m�dico/entidad/anulacion
			int iEnt1 = 0;
			bool bExEnt = false;
			int iAnul1 = 0;
			bool bExAnul = false;
			//***************COMPROBACION NO ESTA INGRESADO
			string tstIngr = String.Empty;
			DataSet tRrIngr = null;
			//**** variable object de ausencias temporales
			Ausencias.Ausencia Ausencia_temporal = null;
			//*******para comprobar que no tiene ausencias
			string tstAusen = String.Empty;
			DataSet tRrAusen = null;
			dynamic instancia = null;
            string stModulo = "Admision";

            try
			{
                CambioServicio.clsCambioServicio oCambioServ = null;
				string strDSN = String.Empty;
				string NOMBRE_DSN_ACCESS = String.Empty;
				object ObjConsumos = null;
				string tstFil = String.Empty;
				DataSet tRrFil = null;
				string stSoci = String.Empty;
				switch(Index)
				{
					case 0 :  //M�dulo de filiaci�n 
						vfNuevoPac = false;  //inicializo la variable de paciente nuevo 
						vstIdPaciente = "";
                        //llamo a la dll de filiaci�n                         
                        vmcFiliacion = new filiacionDLL.Filiacion();                    
                        vmcFiliacion.Load(vmcFiliacion, this, "ALTA", "Admision", Conexion_Base_datos.RcAdmision);
                        //llamo a la pantalla de ingreso, le mando el gidenpac,si es nuevo paciente 
                        //si es activo/pensionista,inspecci�n de la SS,Urgencias/Admision,Conexi�n 
                        // 
                        vmcFiliacion = null; 
						this.Show(); 
						if (vstIdPaciente != "")
						{
							tstIngr = "select gidenpac from aepisadm where gidenpac='" + vstIdPaciente + "' and faltplan is null";
							SqlDataAdapter tempAdapter = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
							tRrIngr = new DataSet();
							tempAdapter.Fill(tRrIngr);
							if (tRrIngr.Tables[0].Rows.Count == 0)
							{
								tstIngr = "select gidenpac from uepisurg where gidenpac='" + vstIdPaciente + "' and faltaurg is null";
								SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
								tRrIngr = new DataSet();
								tempAdapter_2.Fill(tRrIngr);
								if (tRrIngr.Tables[0].Rows.Count == 0)
								{
                                    //camaya todo_7_3
                                    //mcIngreso = new IngresoDLL.Ingreso_clase();
									// formula.proReciboPac; vstIdPaciente, "NA", vstIndAcPen, vstInspec, "H", RcAdmision
									//Crystal2.Connect = ("DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "" == Conexion_Base_datos.VstCrystal).ToString();
									
									//mcIngreso.proCargaIngreso(this, mcIngreso, vstIdPaciente, "NA", vstIndAcPen, vstInspec, "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
									mcIngreso = null;
									//Me.Show
								}
								else
								{
									short tempRefParam = 1480;
									string[] tempRefParam2 = new string[]{"en Urgencias"};
									Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
								}
								Llenar_sprGrid();
							}
							else
							{							
								short tempRefParam3 = 1480;
                                string[] tempRefParam4 = new string[] { "" };
								Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4)); //"en Hospitalizaci�n"
							}
							menu_ingreso_Activated(this, new EventArgs());
							SprPacientes.Refresh();
							this.Show();
							
							tRrIngr.Close();
						} 
						break;
					case 3 :  //Cambio de servicio/m�dico 
						//Controlar que es la primera vez para este paciente 
						//se active si esta desactivada 
						if (vstGidenpac.Trim() != "")
						{
							//    For iMed1 = 1 To iMed
							//        If vstGidenpac = mCamMed(iMed1).Tag Then
							//            bExiste = True
							//            Exit For
							//        Else
							//            bExiste = False
							//        End If
							//    Next iMed1
							//
							//    If bExiste = True Then
							//        mCamMed(iMed1).CbbServicioA.SetFocus
							//    ElseIf bExiste = False Then
							//        iMed = iMed + 1
							//        ReDim Preserve mCamMed(iMed)
							//        Set mCamMed(iMed) = New Cambio_se_me_en
							//        mCamMed(iMed).Tag = vstGidenpac
							//        mCamMed(iMed).proRecPaciente vstGidenpac, stNombPaciente
							//        mCamMed(iMed).Show
							//    End If

							//OSCAR C MARZO 2006
							//EL CAMBIO DE SERVICIO SE HA SACADO A UNA DLL
							oCambioServ = new CambioServicio.clsCambioServicio();
							
							oCambioServ.proRecPacienteIngreso(Conexion_Base_datos.RcAdmision, menu_admision.DefInstance.crystalgeneral, vstGidenpac, stNombPaciente, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a);
                            oCambioServ = null;
							//-------------
							this.Show();
                            cbRefrescar.PerformClick();
                            SprPacientes.SortDescriptors.Clear();
                            iColOrden = 1;
                            SprPacientes.SetSortKey(1, iColOrden);
                            
                            proOrdenGrid(iColOrden, (int) iTipoOrden);
							proSeleccionado();

						} 
						break;
                    case 4:  //Cambio de entidad 
                        if (vstGidenpac.Trim() != "")
                        {
                            for (iEnt1 = 0; iEnt1 < iEnt; iEnt1++)
                            {
                                if (vstGidenpac == Convert.ToString(mCamEnt[iEnt1].Tag))
                                {
                                    bExEnt = true;
                                    break;
                                }
                                else
                                {
                                    bExEnt = false;
                                }
                            }

                            if (bExEnt)
                            {
                                mCamEnt[iEnt1].cbCancelar.Focus();
                            }
                            else if (!bExEnt)
                            {
                                iEnt++;
                                Array.Resize(ref mCamEnt, iEnt);
                                mCamEnt[iEnt - 1] = new Cambio_entidad();
                                mCamEnt[iEnt - 1].Tag = vstGidenpac;
                                mCamEnt[iEnt - 1].proRecPaciente(vstGidenpac, stNombPaciente);
                                mCamEnt[iEnt - 1].Show();
                            }
                        }
                        break;
                    case 5 :  //Pantalla anulaci�n 
						 
						for (iAnul1 = 1; iAnul1 <= iAnul; iAnul1++)
						{
                            if (vstGidenpac == Convert.ToString(mAnul[iAnul1].Tag))
                            {
                                bExAnul = true;
                                break;
                            }
                            else
                            {
                                bExAnul = false;
                            }
                        }
                        // 
                        if (bExAnul)
						{
                            mAnul[iAnul1].cbCancelar.Focus();
                        }
                        else if (!bExAnul)
						{ 
							//hay que validar que no tenga registros en la tabla de ausencias
							tstAusen = "select * from aausenci where ganoadme = " + viGanoadme.ToString() + " and gnumadme = " + viGnumadme.ToString() + " ";
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstAusen, Conexion_Base_datos.RcAdmision);
							tRrAusen = new DataSet();
							tempAdapter_3.Fill(tRrAusen);
							if (tRrAusen.Tables[0].Rows.Count == 0)
							{
                                mAnul = ArraysHelper.RedimPreserve(mAnul, new int[] { iAnul + 1 });
                                mAnul[iAnul] = new Anulacion_ingreso();
                                mAnul[iAnul].Tag = vstGidenpac;
                                mAnul[iAnul].proRecibo_Pac(vstGidenpac, stNombPaciente);
                                mAnul[iAnul].Show();
                                iAnul++;
                            }
                            else
							{								
								short tempRefParam5 = 1940;
                                string[] tempRefParam6 = new string[] { "anular", "ya existen apuntes" };
								Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
							}
						} 
						break;
					case 7 :  //pantalla de ausencias temporales 
						if (vstGidenpac.Trim() != "")
						{
							//Datos_Conexion RcAdmision
							Serrores.CrearCadenaDSN(ref strDSN, ref NOMBRE_DSN_ACCESS);
                            
                            Ausencia_temporal = new Ausencias.Ausencia();

                            //O.Frias 18/04/2006
                            //Incorporo ObjetoCrystal, DsnSQL,UsuCrystal, PassCrystal, RutaRpt

                            //*************** O.Frias 18/06/2007 ***************                            
                            Ausencia_temporal.Recibe_Paciente(Ausencia_temporal, this,ref Conexion_Base_datos.RcAdmision, vstGidenpac, stNombPaciente, vstGanoadme, menu_admision.DefInstance.crystalgeneral, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.BDtempo, Conexion_Base_datos.VstCodUsua);

							Ausencia_temporal = null;
							Llenar_sprGrid();							
							menu_ingreso_Activated(this, new EventArgs());
							SprPacientes.Refresh();
							this.Show();
						} 
						break;
					case 8 : 
						break;
					case 9 : 
						//cambio de identificacion del paciente 
						if (vstGidenpac.Trim() != "")
						{                            
                            vstcambio_pac = new DLLPCambioID.MCCambioID();
							vstcambio_pac.load(vstcambio_pac, this, ref Conexion_Base_datos.RcAdmision, vstGidenpac);
							vstcambio_pac = null;
							this.Show();
						} 
						break;
					case 10 : 
						//listados del paciente 
						//SE CONVIERTE EN DLL 
						if (vstGidenpac.Trim() != "")
						{
                            mcListadosPac = new MovimientosPac.MCMovimientosPac();
                            Conexion_Base_datos.LISTADO_CRYSTAL.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                            mcListadosPac.load(ref Conexion_Base_datos.RcAdmision, vstGidenpac, stNombPaciente, Crystal2, Conexion_Base_datos.PathString, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a);
                            
                            mcListadosPac = null;
                            this.Show();
                        } 
						break;					
                    case 11:
                        if (vstGidenpac.Trim() != "")
                        {
                            for (iEnt1 = 0; iEnt1 < iRegimen; iEnt1++)
                            {
                                if (vstGidenpac == Convert.ToString(mCamRegimen[iEnt1].Tag))
                                {
                                    bExEnt = true;
                                    break;
                                }
                                else
                                {
                                    bExEnt = false;
                                }
                            }

                            if (bExEnt)
                            {
                                mCamRegimen[iEnt1].CbCancelar.Focus();
                            }
                            else if (!bExEnt)
                            {
                                iRegimen++;
                                Array.Resize(ref mCamRegimen, iRegimen);
                                mCamRegimen[iRegimen - 1] = new Cambio_R�gimen();
                                mCamRegimen[iRegimen - 1].Tag = vstGidenpac;
                                mCamRegimen[iRegimen - 1].proRecPaciente(vstGidenpac, stNombPaciente, "RM");
                                mCamRegimen[iRegimen - 1].Show();
                            }
                        }
                        break;
					case 12 :  //ACCIDENTE LABORAL 
						if (vstGidenpac.Trim() != "")
						{
							SprPacientes.Col = Conexion_Base_datos.col_Fingreso;
                            //camaya todo_7_3
                            //instancia = new DLLAccLaboral.MCAccLaboral();
							//Crystal2.Connect = ("DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "" == Conexion_Base_datos.VstCrystal).ToString();
							
							//instancia.Load(Conexion_Base_datos.RcAdmision, vstGidenpac, viGanoadme.ToString(), viGnumadme.ToString(), SprPacientes.Text, stNombPaciente, "H", Crystal2, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.PathString + "\\RPT", Serrores.vNomAplicacion, Serrores.vAyuda, "AtenAltAcc");
							instancia = null;
						} 
						break;
					case 13 :  //ACCIDENTE TRAFICO 
						if (vstGidenpac.Trim() != "")
						{

							tstAusen = "select fllegada from aepisadm where ganoadme = " + viGanoadme.ToString() + " and gnumadme = " + viGnumadme.ToString() + " ";
							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tstAusen, Conexion_Base_datos.RcAdmision);
							tRrIngr = new DataSet();
							tempAdapter_4.Fill(tRrIngr);
							if (tRrIngr.Tables[0].Rows.Count != 0)
							{
								
								Respuesta = Convert.ToString(tRrIngr.Tables[0].Rows[0]["fllegada"]);
							}
                            //camaya todo_7_3
                            //instancia = new DLLAcctrafico.MCAccTrafico();
                            //Crystal2.Connect = ("DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "" == Conexion_Base_datos.VstCrystal).ToString();

                            
                            //camaya todo_x_3
                            //instancia.Load(Conexion_Base_datos.RcAdmision, vstGidenpac, viGanoadme.ToString(), viGnumadme.ToString(), Respuesta, stNombPaciente, "H", Crystal2, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.PathString + "\\RPT", Serrores.vNomAplicacion, Serrores.vAyuda);
							instancia = null;
						} 
						break;
					case 14 : 
						if (vstGidenpac.Trim() != "")
						{
                            
                            
                            CDAyuda = menu_admision.DefInstance.CommDiag_imprePrint;
							tstAusen = "select * from aepisadm,amovimie " + 
							           " where ganoadme = " + viGanoadme.ToString() + " and gnumadme = " + viGnumadme.ToString() + " AND " + 
							           " AMOVIMIE.GANOREGI = AEPISADM.ganoadme AND " + 
							           " AMOVIMIE.GNUMREGI = AEPISADM.gnumadme AND " + 
							           " AMOVIMIE.itiposer = 'H' and " + 
							           " AMOVIMIE.fmovimie = (select max(fmovimie) from amovimie where " + 
							           " ganoregi = " + viGanoadme.ToString() + " and gnumregi = " + viGnumadme.ToString() + ") and ffinmovi is null ";

							//  tstAusen = "select fingplan,fllegada from aepisadm where ganoadme = " & viGanoadme & " and gnumadme = " & viGnumadme & " "
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tstAusen, Conexion_Base_datos.RcAdmision);
							tRrIngr = new DataSet();
							tempAdapter_5.Fill(tRrIngr);
							if (tRrIngr.Tables[0].Rows.Count != 0)
							{								
								
								fllegada = Convert.ToDateTime((Convert.IsDBNull(tRrIngr.Tables[0].Rows[0]["fingplan"])) ? tRrIngr.Tables[0].Rows[0]["fllegada"] : tRrIngr.Tables[0].Rows[0]["fingplan"]);
								
								
								UniEnf = tRrIngr.Tables[0].Rows[0]["gunenfor"];
								
                                ClaseConsumos = new ConsumosPac.clsConsPac();								
								ClaseConsumos.ParamConsumos(vstGidenpac, "H", viGnumadme, viGanoadme, fllegada, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, CDAyuda, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), UniEnf);
								ClaseConsumos = null;

							}
						} 
						 
						//oscar 12/12/2003 
						//LLamada a la DLL de Ingreso en Plaza de Dia 
						break;
					case 15 : 
						vfNuevoPac = false;  //inicializo la variable de paciente nuevo 
						vstIdPaciente = "";
                        //llamo a la dll de filiaci�n                         
                        vmcFiliacion = new filiacionDLL.Filiacion();                         
                        vmcFiliacion.Load(vmcFiliacion, this, "ALTA", "Admision", Conexion_Base_datos.RcAdmision); 
                        //llamo a la pantalla de ingresoHD, le mando el gidenpac,si es nuevo paciente 
                        vmcFiliacion = null; 
						this.Show(); 
						if (vstIdPaciente != "")
						{
							tstIngr = "select gidenpac from aepisadm where gidenpac='" + vstIdPaciente + "' and faltplan is null";
							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
							tRrIngr = new DataSet();
							tempAdapter_6.Fill(tRrIngr);
							if (tRrIngr.Tables[0].Rows.Count == 0)
							{
								tstIngr = "select gidenpac from uepisurg where gidenpac='" + vstIdPaciente + "' and faltaurg is null";
								SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
								tRrIngr = new DataSet();
								tempAdapter_7.Fill(tRrIngr);
								if (tRrIngr.Tables[0].Rows.Count == 0)
								{
                                    //camaya todo_7_3
                                    //mcIngreso = new IngresoHD.Ingreso_clase();
                                    //Crystal2.Connect = ("DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "" == Conexion_Base_datos.VstCrystal).ToString();
									
									//mcIngreso.proCargaIngreso(this, mcIngreso, vstIdPaciente, "NA", vstIndAcPen, vstInspec, "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
									mcIngreso = null;
								}
								else
								{									
									short tempRefParam7 = 1480;
									string[] tempRefParam8 = new string[]{"en Urgencias"};
									Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
								}
								Llenar_sprGrid();
							}
							else
							{								
								short tempRefParam9 = 1480;
                                string[] tempRefParam10 = new string[] { "en Hospitalizaci�n"};
								Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
							}
							menu_ingreso_Activated(this, new EventArgs());
							SprPacientes.Refresh();
							this.Show();
							
							tRrIngr.Close();
						} 
						//oscar 18/03/04 
						break;
					case 16 :
                        //registro de resolucion al juzgado 
                        //OSCAR 06/09/05: se ha sacado a una dll para 
                        //que se pueda acceder desde el modulo de OTROS PROFESIONALES 
                        //resol_juzgado.Tag = vstGidenpac & "-" & vstGanoadme 
                        //resol_juzgado.Show vbModal 

                        DLLDatosJuzgado.clsDatos_juzgado datosJuzgados;
                        datosJuzgados = new DLLDatosJuzgado.clsDatos_juzgado();
                        datosJuzgados.proLoad(Conexion_Base_datos.RcAdmision, viGanoadme, viGnumadme, vstGidenpac, Conexion_Base_datos.gUsuario);
                        datosJuzgados = null; 
						//------------- 
						break;
					case 17 :  //Control de Citas externas 

                        instancia = new ECitaExterna.ECitasExternas(); 
						instancia.proload(vstGidenpac, viGanoadme, viGnumadme, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\ElementosComunes\\rpt", Conexion_Base_datos.VstCrystal); 
						instancia = null; 
						this.Show(); 
						break;
					case 18 :  //Cambio de producto 
						Cambio_producto tempLoadForm = Cambio_producto.DefInstance; 
						Cambio_producto.DefInstance.proRecibePaciente(vstGidenpac, viGanoadme, viGnumadme, stNombPaciente); 
						Cambio_producto.DefInstance.ShowDialog(); 
						break;
					case 19 :  //Cambio de Regimen de Alojamiento 
						if (vstGidenpac.Trim() != "")
						{
							for (iEnt1 = 0; iEnt1 < iRegimen; iEnt1++)
							{
								if (vstGidenpac == Convert.ToString(mCamRegimen[iEnt1].Tag))
								{
									bExEnt = true;
									break;
								}
								else
								{
									bExEnt = false;
								}
							}

							if (bExEnt)
							{
								mCamRegimen[iEnt1].CbCancelar.Focus();
							}
							else if (!bExEnt)
							{ 
								iRegimen++;
                                Array.Resize(ref mCamRegimen, iRegimen);                                
								mCamRegimen[iRegimen -1] = new Cambio_R�gimen();
								mCamRegimen[iRegimen -1].Tag = vstGidenpac;
								mCamRegimen[iRegimen -1].proRecPaciente(vstGidenpac, stNombPaciente, "RA");
								mCamRegimen[iRegimen -1].Show();
							}
						} 
						//------------------- 
						 
						break;
					case 20 : 
						//Oscar C 11/01/2010 
						//Conversion de Episodios de Hospital de Dia a Episodio de Hospitalizacion 
						//(hay que validar que no este facturado) 
						 
						tstAusen = "select * from dmovfact where " + " ganoregi=" + viGanoadme.ToString() + " and gnumregi=" + viGnumadme.ToString() + " and itiposer = 'H' and " + " gcodeven='ES' and ffactura is not null"; 
						 
						if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
						{
							string tempRefParam11 = "DMOVFACT";                            
                            tstAusen = tstAusen + " UNION ALL " + Serrores.proReplace( ref tstAusen, tempRefParam11, "DMOVFACH");
                        }

                        SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(tstAusen, Conexion_Base_datos.RcAdmision); 
						tRrAusen = new DataSet(); 
						tempAdapter_8.Fill(tRrAusen); 
						if (tRrAusen.Tables[0].Rows.Count == 0)
						{
                            ConversionHDaH tempLoadForm2 = ConversionHDaH.DefInstance;
                            ConversionHDaH.DefInstance.proRecibeEpisodio(viGanoadme, viGnumadme);
                            ConversionHDaH.DefInstance.ShowDialog();
                            cbRefrescar_Click(cbRefrescar, new EventArgs());
						}
						else
						{					
							short tempRefParam12 = 1420;
							string[] tempRefParam13 = new string[]{"El Episodio", "Facturado"};
							Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam12, Conexion_Base_datos.RcAdmision, tempRefParam13));
						}						
						tRrAusen.Close(); 
						//------------------ 
						break;
					case 21 : 
						if (mbAcceso.ObtenerCodigoSConsglo("PRESUDEP", "valfanu1", Conexion_Base_datos.RcAdmision) != "")
						{
							//cambio los datos en amovifin
							tstIngr = "select gsocieda from amovifin where ganoregi= " + viGanoadme.ToString() + " and gnumregi= " + viGnumadme.ToString() + " and  ffinmovi is null";
							SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
							tRrIngr = new DataSet();
							tempAdapter_9.Fill(tRrIngr);							
							stSoci = Convert.ToString(tRrIngr.Tables[0].Rows[0][0]).Trim();
							if (tRrIngr.Tables[0].Rows.Count == 1)
							{
								tstIngr = "select dape1pac,dape2pac from dpacient where gidenpac = '" + vstGidenpac + "'";
								SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
								tRrIngr = new DataSet();
								tempAdapter_10.Fill(tRrIngr);
                                //camaya todo_x_3
                                //ObjConsumos = new LlamarOperacionesFMS.clsOperFMS();
                                
                                //ObjConsumos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua.ToUpper(), Conexion_Base_datos.RcAdmision, this, "PRESUDEP", vstGidenpac, tRrIngr.Tables[0].Rows[0]["dape1pac"], tRrIngr.Tables[0].Rows[0]["dape2pac"], null, null, null, null, null, stSoci);
								ObjConsumos = null;
							}
							else
							{
								short tempRefParam14 = 4013;
								string[] tempRefParam15 = new string[]{"por estar el paciente dado de alta"};
								Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam14, Conexion_Base_datos.RcAdmision, tempRefParam15);
							}							
							tRrIngr.Close();
						}
						else
						{
							short tempRefParam16 = 4013;
							string[] tempRefParam17 = new string[]{"por no estar parametrizada"};
							Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam16, Conexion_Base_datos.RcAdmision, tempRefParam17);
						} 
						this.Show(); 
						break;
					case 23 :
                        //' 04/04/2014 
                        //' Llamada a la documentos aportados a HC 

                        frmListInformes tempLoadForm3 = frmListInformes.DefInstance; 
						frmListInformes.DefInstance.proRecibeDatos(stNombPaciente, viGanoadme, viGnumadme); 
						frmListInformes.DefInstance.ShowDialog();
						break;
				}
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		public void proMnu_ing21()
		{			
			short tempRefParam = 1040;
			string[] tempRefParam2 = new string[]{"Ayuda"};
			Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
		}
		public void proMenu_doc()
		{
			if (vstGidenpac.Trim() != "")
			{
				if (fnComprobar_pac())
				{
                    //**************Menu documento                                        
                    Documentos_ingreso.DefInstance.Tag = vstGidenpac + "/" + stNombPaciente + "/" + vstGanoadme;
					Documentos_ingreso.DefInstance.Show();
				}
				else
				{					
					short tempRefParam = 1420;
					string[] tempRefParam2 = new string[]{"El paciente", "dado de alta por otro usuario"};
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				}
			}
		}

		public void proMant_Datos(int Index)
		{
            //carga las ventanas de mantenimiento de datos de filiaci�n y de ingreso
            //On Error GoTo Etiqueta
            //RcAdmision.BeginTrans
            //*******MANTENIMIENTO DATOS

            string stModulo = "";
			switch(Index)
			{
				case 0 :  //FILIACION 
					this.Cursor = Cursors.WaitCursor;
                    vmcFiliacion = new filiacionDLL.Filiacion();                     
                    vmcFiliacion.Load(vmcFiliacion, this, "MODIFICACION", "Admision", Conexion_Base_datos.RcAdmision, vstGidenpac); 

                    vmcFiliacion = null; 
					this.Show(); 
					//proSeleccionado 
					this.Cursor = Cursors.Default; 
					//proOrdenGrid(iColOrden, (int) iTipoOrden); 
					//SprPacientes_Click 1, SprPacientes.ActiveRow 
					proSeleccionado(); 
					break;
				case 1 :  //INGRESO 
					if (vstGidenpac.Trim() != "")
					{
						//oscar 16/12/2003
						//Los episodios menores a 1900 corresponden a episodios de hospital de dia
						//por lo que debemos acceder a la correspondiente DLL
						//Set mcIngreso = CreateObject("IngresoDLL.Ingreso_clase")
						//mcIngreso.proCargaIngreso Me, mcIngreso, vstGidenpac, "C", "", "", "H", "", "", gEnfermeria, RcAdmision, Crystal2, App.Path, VstCrystal, gUsuario, gContrase�a, VstCodUsua
						//Set mcIngreso = Nothing
						SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
						if (Convert.ToInt32(Double.Parse(SprPacientes.Text.Substring(0, Math.Min(SprPacientes.Text.IndexOf('/'), SprPacientes.Text.Length)))) < 1900)
						{
                            //camaya todo_7_3
                            //mcIngreso = new IngresoHD.Ingreso_clase();
							
							//mcIngreso.proCargaIngreso(this, mcIngreso, vstGidenpac, "C", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
							mcIngreso = null;
						}
						else
						{
                            //camaya todo_7_3
                            //mcIngreso = new IngresoDLL.Ingreso_clase();
							
							//mcIngreso.proCargaIngreso(this, mcIngreso, vstGidenpac, "C", "", "", "H", "", "", Conexion_Base_datos.gEnfermeria, Conexion_Base_datos.RcAdmision, Crystal2, Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCrystal, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Conexion_Base_datos.VstCodUsua);
							mcIngreso = null;
						}
						//-------------

						this.Show();
					} 
					break;
				case 3 : 
					proMnu_ing01(9); 
					break;
			}
			return;
		}

		public void proVerMenus(bool bVer)
		{
			//activo o desactivo los men�s de la MDI
			menu_admision.DefInstance.mnu_adm011[0].Enabled = !bVer;
			menu_admision.DefInstance.mnu_adm011[1].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[2].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[3].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[4].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[5].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[6].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[7].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[8].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[9].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[10].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[11].Enabled = bVer;
			menu_admision.DefInstance.mnu_adm011[12].Enabled = bVer;
			//oscar 11/12/2003
			menu_admision.DefInstance.mnu_adm011[13].Enabled = bVer; //Resolucion del Juzgado
			//''menu_admision!mnu_adm011(14).Enabled = bVer 'consultas
			menu_admision.DefInstance.mnu_adm011[15].Enabled = bVer; //Cambio Producto
			menu_admision.DefInstance.mnu_adm011[18].Enabled = bVer; //Ingreso en plaza de dia
			menu_admision.DefInstance.mnu_adm011[16].Enabled = bVer; //Cambio de regimen de alojamiento
			//Oscar C Enero 2010
			menu_admision.DefInstance.mnu_adm011[19].Enabled = bVer; //Conversion de Episodio de Hospital de Dia a Hospitalizacion
			//----------------
			menu_admision.DefInstance.mnu_adm011[20].Enabled = bVer; //Presupuestos y dep�sitos
			menu_admision.DefInstance.mnu_adm011[21].Enabled = bVer;
		}

		public void proQuitar_MatAnul(string sACodigo_quitar)
		{
            Anulacion_ingreso[] MATemp = null;
			int iInATemp = 0;
			//compruebo que se ha anulado
			string tstAnul = sACodigo_quitar.Substring(13, Math.Min(1, sACodigo_quitar.Length - 13)); //si su valor es C no se ha anulado ,si es S si
			sACodigo_quitar = sACodigo_quitar.Substring(0, Math.Min(13, sACodigo_quitar.Length));
			Llenar_sprGrid();
            //*******************
            DimMtAnul = mAnul.GetUpperBound(0);

            for (int iMATemp = 1; iMATemp <= DimMtAnul; iMATemp++)
			{
                if (sACodigo_quitar == Convert.ToString(mAnul[iMATemp].Tag))
                {
                    //No hago nada
                }
                else
                {
                    iInATemp++;
                    MATemp = ArraysHelper.RedimPreserve(MATemp, new int[] { iInATemp + 1 });
                    MATemp[iInATemp] = mAnul[iMATemp];
                }
            }

            if (mAnul != null)
            {
                Array.Clear(mAnul, 0, mAnul.Length);
            }

            mAnul = new Anulacion_ingreso[iInATemp + 1];

            for (int iMATemp = 1; iMATemp <= iInATemp; iMATemp++)
            {
                mAnul[iInATemp] = MATemp[iInATemp];
            }

            if (MATemp != null)
            {
                Array.Clear(MATemp, 0, MATemp.Length);
            }
            iAnul = mAnul.GetUpperBound(0);
            switch (tstAnul)
			{
				case "S" : 
					vfAnul = false;  //no selecciono al paciente 
					break;
				case "C" : 
					vfAnul = true;  //no se le ha anulado y le dejo seleccionado 
					break;
			}
		}

		public bool fnComprobar_pac()
		{
			bool result = false;
			string tstIngreso = String.Empty;
			DataSet tRringreso = null;

			try
			{
				tstIngreso = "SELECT " + " GPLANTAS,GHABITAC,GCAMASBO,DAPE1PAC,DAPE2PAC,DNOMBPAC,FLLEGADA, " + " ganoadme,gnumadme " + "FROM " + " DCAMASBO, DPACIENT, AEPISADM " + "WHERE " + "DPACIENT.GIDENPAC=AEPISADM.GIDENPAC" + " and AEPISADM.GIDENPAC=DCAMASBO.GIDENPAC" + " and AEPISADM.FALTAADM is null " + " and aepisadm.FALTPLAN is null " + " AND DCAMASBO.IESTCAMA='O' " + "And AEPISADM.ganoadme = " + viGanoadme.ToString() + " AND  AEPISADM.gnumadme = " + viGnumadme.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstIngreso, Conexion_Base_datos.RcAdmision);
				tRringreso = new DataSet();
				tempAdapter.Fill(tRringreso);
				result = tRringreso.Tables[0].Rows.Count == 1;				
				tRringreso.Close();
				return result;
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
				return result;
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
                return result;
            }
        }

		public void proMant_Filiacion()
		{
			string tstFil = String.Empty;
			DataSet tRrFil = null;
			string tstServicio = String.Empty; //tipo de servicio
			//cambio los datos en amovifin
			try
			{
				//******************
				tstFil = "select * from amovifin where ganoregi= " + viGanoadme.ToString() + " and gnumregi= " + viGnumadme.ToString() + " and  ffinmovi is null";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstFil, Conexion_Base_datos.RcAdmision);
				tRrFil = new DataSet();
				tempAdapter.Fill(tRrFil);
				if (tRrFil.Tables[0].Rows.Count == 1)
				{
					if (Convert.ToString(tRrFil.Tables[0].Rows[0]["gsocieda"]) != vstCodsoc && vstCodsoc != "")
					{ //ha cambiado de sociedad
						
						tstServicio = Convert.ToString(tRrFil.Tables[0].Rows[0]["itiposer"]);                        												
						tRrFil.Tables[0].Rows[0]["ffinmovi"] = DateTime.Now;						
						string tempQuery = tRrFil.Tables[0].TableName;						
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                        tempAdapter.Update(tRrFil, tempQuery);
						//registro nuevo en amovifin
						tstFil = "select * from amovifin where 2=1";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstFil, Conexion_Base_datos.RcAdmision);
						tRrFil = new DataSet();
						tempAdapter_3.Fill(tRrFil);                                                
                        tRrFil.AddNew();						
						tRrFil.Tables[0].Rows[0]["ganoregi"] = viGanoadme;						
						tRrFil.Tables[0].Rows[0]["gnumregi"] = viGnumadme;						
						tRrFil.Tables[0].Rows[0]["itiposer"] = tstServicio;
						if (vstCodsoc != "")
						{ //codigo sociedad							
							tRrFil.Tables[0].Rows[0]["gsocieda"] = vstCodsoc;
						}
						if (vstAsegurado != "")
						{ //n�mero filiaci�n							
							tRrFil.Tables[0].Rows[0]["nafiliac"] = vstAsegurado;
						}
						if (vstIndAcPen != "")
						{ //activo/pensionista							
							tRrFil.Tables[0].Rows[0]["ginspecc"] = vstIndAcPen;
						}
						else
						{
							tRrFil.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
						}
						if (vstInspec != "")
						{ //inspecci�n
							tRrFil.Tables[0].Rows[0]["ginspecc"] = vstInspec;
						}
						else
						{
                            tRrFil.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
						}
						
						tRrFil.Tables[0].Rows[0]["FMOVIMIE"] = DateTime.Now;
						string tempQuery_2 = tRrFil.Tables[0].TableName;
						SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
                        tempAdapter_3.Update(tRrFil, tempQuery_2);
					}
					else
					{

					}
					tRrFil.Close();
				}
			}
			catch
			{
				//*********************************
				RadMessageBox.Show("Fallo en mantenimiento filiaci�n", Application.ProductName);
			}
		}

		public void proSeleccionado()
		{
			int tlRow = 0;
			int NumRow = 0;

			if (lRow == 0)
			{
				vstGidenpac = fnRecogerPac(1); //recojo los datos del primero
			}
			else
			{
				SprPacientes.Col = Conexion_Base_datos.col_Nosequecolumnaes;
				SprPacientes.Row = 0;
				// pongo esto porque cuando le anulas el ingreso se enbucla porque se ha eliminado de la lista

				while(!(SprPacientes.Text.Trim() == vstGanoadme || tlRow > SprPacientes.MaxRows))
				{
					SprPacientes.Row = tlRow;
					tlRow++;
				};
				if (tlRow > SprPacientes.MaxRows)
				{
					SprPacientes.Row = 1; // si no lo encuentra activo el primero
					//'Call SprPacientes_Click(0, 1)
				}
				//'Call SprPacientes_Click(0, tlRow)

				SprPacientes.Col = Conexion_Base_datos.col_IdenPac; // gidenpac
				//''    If Trim(SprPacientes.Text) = Trim(vstGidenpac) Then
				//''        SprPacientes.Action = 0   'activo el �ltimo seleccionado
				//''        Exit Sub
				//''    End If


				while(NumRow <= SprPacientes.MaxRows)
				{
					SprPacientes.Row = NumRow;
					NumRow++;
					if (SprPacientes.Text.Trim() == vstGidenpac.Trim())
					{
						SprPacientes.Col = 1;
						SprPacientes.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell; //activo el �ltimo seleccionado
						return;
					}
				};
				SprPacientes.Col = 1; // gidenpac
			}
		}

		public void proNuevoPac(string stGidenpac, int Ganoadme, int Gnumadme)
		{
			if (stGidenpac != "")
			{
				vfNuevoPac = true;
				vstGidenpac = stGidenpac;
				viGanoadme = Ganoadme;
				viGnumadme = Gnumadme;
				Llenar_sprGrid();
				lRow = 2;
				proGanoadme(stGidenpac);
				vstGidenpac = fnRecogerPac(SprPacientes.Row);
				//proSeleccionado
			}
		}

		public void proGanoadme(string Idpaciente)
		{
			string tstSql = "select ganoadme,gnumadme from aepisadm where gidenpac='" + Idpaciente + "' and faltplan is null";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, Conexion_Base_datos.RcAdmision);
			DataSet tRrpaciente = new DataSet();
			tempAdapter.Fill(tRrpaciente);
			if (tRrpaciente.Tables[0].Rows.Count == 1)
			{				
				vstGanoadme = Conversion.Str(tRrpaciente.Tables[0].Rows[0]["ganoadme"]).Trim() + "/" + Conversion.Str(tRrpaciente.Tables[0].Rows[0]["gnumadme"]).Trim();
				proSeleccionado();
			}
			else
			{

			}			
			tRrpaciente.Close();
		}

		public void Recoger_Datos2(string nuevoId)
		{
			Llenar_sprGrid();
		}

		public void proPermisoAnulacion()
		{
			string tstIngr = String.Empty;
			DataSet tRrIngr = null;
			DLLTextosSql.TextosSql oTextoSql = null;
			try
			{
				// Luis 17/08/2000 cambio sql para acceso a todo tipo de bd
				// tstIngr = "select (gidenpac) as NumMov,fingplan, fllegada from aepisadm,amovimie," & _
				//'           "amovifin,amovrega,aausenci,aregimen " _
				//'            & " where " _
				//'            & " aepisadm.ganoadme=" & viGanoadme _
				//'            & " and aepisadm.gnumadme=" & viGnumadme _
				//'            & " and aepisadm.ganoadme=amovimie.ganoregi and " _
				//'            & " aepisadm.gnumadme=amovimie.gnumregi and " _
				//'            & " aepisadm.ganoadme=amovifin.ganoregi and " _
				//'            & " aepisadm.gnumadme=amovifin.gnumregi and " _
				//'            & " aepisadm.ganoadme=amovrega.ganoregi and " _
				//'            & " aepisadm.gnumadme=amovrega.gnumregi and " _
				//'            & " aepisadm.ganoadme*=aausenci.ganoadme and " _
				//'            & " aepisadm.gnumadme*=aausenci.gnumadme and " _
				//'            & " aepisadm.ganoadme=aregimen.ganoadme and " _
				//'            & " aepisadm.gnumadme=aregimen.gnumadme and " _
				//'            & " gidenpac='" & vstGidenpac & "' and faltplan is null"

				object[] MatrizSql = new object[4];
				
				MatrizSql[1] = viGanoadme;
				
				MatrizSql[2] = viGnumadme;
				
				MatrizSql[3] = vstGidenpac;
				oTextoSql = new DLLTextosSql.TextosSql();
				
				string tempRefParam = "ADMISION";
				string tempRefParam2 = "AGI100F1";
				string tempRefParam3 = "PROPERMISOANULACION";
				short tempRefParam4 = 1;
				object[] tempRefParam5 = MatrizSql;
				tstIngr = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, Conexion_Base_datos.RcAdmision));
				MatrizSql = (object[]) tempRefParam5;
				oTextoSql = null;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstIngr, Conexion_Base_datos.RcAdmision);
				tRrIngr = new DataSet();
				tempAdapter.Fill(tRrIngr);
				if (tRrIngr.Tables[0].Rows.Count == 1)
				{
					//oscar 29/12/2003
					//If Me.SprPacientes.ForeColor = Line1.BorderColor Then  ' si est� ausente
					if (ColorTranslator.ToOle(this.SprPacientes.ForeColor) == ColorTranslator.ToOle(Line1.BorderColor) || ColorTranslator.ToOle(this.SprPacientes.ForeColor) == ColorTranslator.ToOle(Line3.BorderColor))
					{
                        //---------
                        _Toolbar1_Button5.Enabled = false;
						menu_admision.DefInstance.mnu_adm011[6].Enabled = false;
					}
					else
					{
                        // no est� ausente -> LLamar a FechaMinMaxApuntes
                        _Toolbar1_Button5.Enabled = true;
						menu_admision.DefInstance.mnu_adm011[6].Enabled = true;
					}
				}
				else
				{
                    _Toolbar1_Button5.Enabled = false;                    
					menu_admision.DefInstance.mnu_adm011[6].Enabled = false;
				}                    				
				tRrIngr.Close();
			}
			catch
			{
				RadMessageBox.Show("Error en comprobaci�n de anulaci�n del ingreso del paciente", Application.ProductName);
			}
		}

		//OSCAR 15/10/04
		private void cbAceptarPrevi_Click(Object eventSender, EventArgs eventArgs)
		{
            int puntero = 1;
            do {
                try
                {
                    string fecha = String.Empty;
                    fecha = DateTimeHelper.ToString(DateTime.Now);

                    //validar la fecha y la hora de prevision de alta
                    if (Convert.ToString(sdcPrevision.Value.Date) != "" || tbHoraPrevision.Text.Trim() != ":")
                    {
                        //if (tbHoraPrevision.Text.Trim() == ":" && Convert.ToString(sdcPrevision.Value.Date) != "")
                        if (tbHoraPrevision.Text.Trim() == ":" && Convert.ToString(sdcPrevision.Text) != "")
                        {
                            short tempRefParam = 1040;
                            string[] tempRefParam2 = new string[] { "hora de previsi�n" };
                            clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2);
                            tbHoraPrevision.Focus();
                            return;
                        }
                        else if (tbHoraPrevision.Text.Trim() != ":" && Convert.ToString(sdcPrevision.Text) == "") //(tbHoraPrevision.Text.Trim() != ":" && Convert.ToString(sdcPrevision.Value.Date) == "")
                        {
                            short tempRefParam3 = 1040;
                            string[] tempRefParam4 = new string[] { "fecha de previsi�n" };
                            clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4);
                            sdcPrevision.Focus();
                            return;
                        }
                        else if (!Information.IsDate(tbHoraPrevision.Text) && Convert.ToString(sdcPrevision.Text) != "") //(!Information.IsDate(tbHoraPrevision.Text) && Convert.ToString(sdcPrevision.Value) != "")
                        {
                            short tempRefParam5 = 1150;
                            string[] tempRefParam6 = new string[] { "hora de previsi�n" };
                            clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6);
                            tbHoraPrevision.Focus();
                            return;
                        }
                        //else if (!Information.IsDate(sdcPrevision.Text + " " + tbHoraPrevision.Text))
                        //{ 
                        //	short tempRefParam7 = 1210;
                        //  string[] tempRefParam8 = new string[] { "la fecha y/o la hora de previsi�n"};
                        //	clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8);
                        //	sdcPrevision.Focus();
                        //	return;
                        //}
                    }

                    //validar la fecha y la hora de aviso
                    if (Convert.ToString(sdcAviso.Value.Date) != "" || tbHoraAviso.Text.Trim() != ":")
                    {
                        //if (tbHoraAviso.Text.Trim() == ":" && Convert.ToString(sdcAviso.Value.Date) != "")
                        if (tbHoraAviso.Text.Trim() == ":" && Convert.ToString(sdcAviso.Text) != "")
                        {
                            short tempRefParam9 = 1040;
                            string[] tempRefParam10 = new string[] { "hora de Aviso" };
                            clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10);
                            tbHoraAviso.Focus();
                            return;
                        }
                        else if (tbHoraAviso.Text.Trim() != ":" && Convert.ToString(sdcAviso.Text) == "") //(tbHoraAviso.Text.Trim() != ":" && Convert.ToString(sdcAviso.Value.Date) == "")
                        {
                            short tempRefParam11 = 1040;
                            string[] tempRefParam12 = new string[] { "fecha de Aviso" };
                            clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12);
                            sdcAviso.Focus();
                            return;
                        }
                        else if (!Information.IsDate(tbHoraAviso.Text) && Convert.ToString(sdcAviso.Text) != "") //(!Information.IsDate(tbHoraAviso.Text) && Convert.ToString(sdcAviso.Value.Date) != "")
                        {
                            short tempRefParam13 = 1150;
                            string[] tempRefParam14 = new string[] { "hora de Aviso" };
                            clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, Conexion_Base_datos.RcAdmision, tempRefParam14);
                            tbHoraAviso.Focus();
                            return;
                        }
                        //else if (!Information.IsDate(sdcAviso.Text + " " + tbHoraAviso.Text))
                        //{ 
                        //	short tempRefParam15 = 1210;
                        //                   string[] tempRefParam16 = new string[] { "la fecha y/o la hora de Aviso"};
                        //	clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, Conexion_Base_datos.RcAdmision, tempRefParam16);
                        //	sdcAviso.Focus();
                        //	return;
                        //}
                    }

                    if (Convert.ToString(sdcPrevision.Value.Date) != "" && tbHoraPrevision.Text.Trim() != ":")
                    {
                        System.DateTime TempDate2 = DateTime.FromOADate(0);
                        System.DateTime TempDate = DateTime.FromOADate(0);
                        //if (DateTime.Parse((DateTime.TryParse(sdcPrevision.Value.Date + " " + tbHoraPrevision.Text, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : sdcPrevision.Value.Date + " " + tbHoraPrevision.Text) < DateTime.Parse((DateTime.TryParse(fecha, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : fecha))
                        if (DateTime.Parse((DateTime.TryParse(sdcPrevision.Value.ToShortDateString() + " " + tbHoraPrevision.Text, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : sdcPrevision.Value.ToShortDateString() + " " + tbHoraPrevision.Text) < DateTime.Parse((DateTime.TryParse(fecha, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : fecha))

                        {
                            Telerik.WinControls.RadMessageBox.Show("La fecha de previsi�n de alta debe ser mayor o igual a " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), Serrores.vNomAplicacion, MessageBoxButtons.OK, Telerik.WinControls.RadMessageIcon.Info);
                            //    MessageBox.Show("La fecha de previsi�n de alta debe ser mayor o igual a " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        //si ha tecleado fecha de prevision tiene que tener fecha de aviso
                        if (Convert.ToString(sdcAviso.Value.Date) != "" && tbHoraAviso.Text.Trim() != ":")
                        {
                            System.DateTime TempDate4 = DateTime.FromOADate(0);
                            System.DateTime TempDate3 = DateTime.FromOADate(0);
                            //if (DateTime.Parse((DateTime.TryParse(sdcAviso.Value.Date + " " + tbHoraAviso.Text, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:NN") : sdcAviso.Value.Date + " " + tbHoraAviso.Text) > DateTime.Parse((DateTime.TryParse(fecha, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:NN") : fecha))
                            if (DateTime.Parse((DateTime.TryParse(sdcAviso.Value.ToShortDateString() + " " + Convert.ToDateTime(tbHoraAviso.Text).ToString("HH:mm:ss"), out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm") : sdcAviso.Value.ToShortDateString() + " " + Convert.ToDateTime(tbHoraAviso.Text).ToString("HH:mm:ss")) > DateTime.Parse((DateTime.TryParse(fecha, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:mm") : fecha))
                            {
                                Telerik.WinControls.RadMessageBox.Show("La fecha de aviso debe ser menor o igual a " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), Serrores.vNomAplicacion, MessageBoxButtons.OK, Telerik.WinControls.RadMessageIcon.Info);
                                //MessageBox.Show("La fecha de aviso debe ser menor o igual a " + DateTime.Now.ToString("dd/MM/yyyy HH:NN"), Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            Grabar_prevision(sdcPrevision.Value.ToShortDateString() + " " + tbHoraPrevision.Text.Trim(), sdcAviso.Value.ToShortDateString() + " " + tbHoraAviso.Text.Trim());
                            Llenar_sprGrid();
                        }
                        else
                        {
                            short tempRefParam17 = 3200;
                            string[] tempRefParam18 = new string[] { "introduce Previsi�n de alta", "Fecha de aviso" };
                            Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam17, Conexion_Base_datos.RcAdmision, tempRefParam18);
                            return;
                        }
                    }
                    else
                    {
                        //si no ha tecleado fecha de prevision la fecha de aviso debe estar vacia
                        if (Convert.ToString(sdcAviso.Value.Date) != "")
                        {
                            short tempRefParam19 = 3200;
                            string[] tempRefParam20 = new string[] { "introduce Fecha de aviso", "Previsi�n de alta" };
                            Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam19, Conexion_Base_datos.RcAdmision, tempRefParam20);
                            return;
                        }
                        else
                        {
                            Grabar_prevision("NULL", "NULL");
                        }
                    }

                    if (!Information.IsDate(sdcAviso.Text + " " + tbHoraAviso.Text))
                    {
                        short tempRefParam15 = 1210;
                        string[] tempRefParam16 = new string[] { "la fecha y/o la hora de Aviso" };
                        clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, Conexion_Base_datos.RcAdmision, tempRefParam16);
                        sdcAviso.Focus();
                        return;
                    }

                    if (!Information.IsDate(sdcPrevision.Text + " " + tbHoraPrevision.Text))
                    {
                        short tempRefParam7 = 1210;
                        string[] tempRefParam8 = new string[] { "la fecha y/o la hora de previsi�n" };
                        clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8);
                        sdcPrevision.Focus();
                        return;
                    }
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "CbAceptarPrevi:menu_ingreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);
		}

		private void cbAceptarPrevi_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == ((int) Keys.Tab) && Shift == 1)
			{
				sdcPrevision.Focus();
			} // tabulador

		}

		private void cbAceptarPrevi_Leave(Object eventSender, EventArgs eventArgs)
		{
			cbAceptarPrevi.Enabled = false;
		}

		private void sdcAviso_Enter(Object eventSender, EventArgs eventArgs)
		{
			cbAceptarPrevi.Enabled = true;

		}

		private void sdcAviso_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == ((int) Keys.Tab))
			{
				cbAceptarPrevi.Focus();
			} // tabulador
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void sdcPrevision_Enter(Object eventSender, EventArgs eventArgs)
		{
			cbAceptarPrevi.Enabled = true;

		}

		private void sdcPrevision_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == ((int) Keys.Tab))
			{
				cbAceptarPrevi.Focus();
			} // tabulador
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbHoraPrevision_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbHoraPrevision.SelectionStart = 0;
			tbHoraPrevision.SelectionLength = Strings.Len(tbHoraPrevision.Text);
			cbAceptarPrevi.Enabled = true;
		}

		private void tbHoraPrevision_Leave(Object eventSender, EventArgs eventArgs)
		{
			int iHora = 0;
			int iMinutos = 0;

			if (tbHoraPrevision.Text.Trim() != "" && tbHoraPrevision.Text.Trim() != ":")
			{
				iHora = Convert.ToInt32(Conversion.Val(tbHoraPrevision.Text.Substring(0, Math.Min(tbHoraPrevision.Text.IndexOf(':'), tbHoraPrevision.Text.Length))));
				iMinutos = Convert.ToInt32(Conversion.Val(tbHoraPrevision.Text.Substring(tbHoraPrevision.Text.IndexOf(':') + 1, Math.Min(2, tbHoraPrevision.Text.Length - (tbHoraPrevision.Text.IndexOf(':') + 1)))));

				if (iHora < 0 || iHora > 23)
				{
					short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[] { "hora de previsi�n"};
					clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2);
					tbHoraPrevision.Focus();
					return;
				}
				if (iMinutos < 0 || iMinutos > 59)
				{
					short tempRefParam3 = 1150;
                    string[] tempRefParam4 = new string[] { "hora de previsi�n"};
					clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4);
					tbHoraPrevision.Focus();
					return;
				}
			}
		}

		private void tbHoraAviso_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbHoraAviso.SelectionStart = 0;
			tbHoraAviso.SelectionLength = Strings.Len(tbHoraAviso.Text);
			cbAceptarPrevi.Enabled = true;
		}

		private void tbHoraAviso_Leave(Object eventSender, EventArgs eventArgs)
		{
			int iHora = 0;
			int iMinutos = 0;

			if (tbHoraAviso.Text.Trim() != "" && tbHoraAviso.Text.Trim() != ":")
			{
				iHora = Convert.ToInt32(Conversion.Val(tbHoraAviso.Text.Substring(0, Math.Min(tbHoraAviso.Text.IndexOf(':'), tbHoraAviso.Text.Length))));
				iMinutos = Convert.ToInt32(Conversion.Val(tbHoraAviso.Text.Substring(tbHoraAviso.Text.IndexOf(':') + 1, Math.Min(2, tbHoraAviso.Text.Length - (tbHoraAviso.Text.IndexOf(':') + 1)))));

				if (iHora < 0 || iHora > 23)
				{
					short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[] { "hora de Aviso"};
					clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2);
					tbHoraAviso.Focus();
					return;
				}
				if (iMinutos < 0 || iMinutos > 59)
				{
					short tempRefParam3 = 1150;
                    string[] tempRefParam4 = new string[] { "hora de Aviso"};
					clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4);
					tbHoraAviso.Focus();
					return;
				}
			}
		}

		//************************************************************************************
		//*                                                                                  *
		//*  Modificaciones:                                                                 *
		//*                                                                                  *
		//*      O.Frias (06/02/2007) Incorporo la actualizaci�n de los campos gusuprea      *
		//*                                                                                  *
		//*                                                                                  *
		//************************************************************************************
		public void Grabar_prevision(string TextoFecha, string FechaAviso)
		{
			string sqlDatos = String.Empty, tstCama = String.Empty;
			int tstPlanta = 0, tstHabitacion = 0;
			DataSet RsPaciente = null;
            //se inicia una transaccion  

            //SqlTransaction transaction = Conexion_Base_datos.RcAdmision.BeginTrans();
            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                {
                    SprPacientes.Row = SprPacientes.ActiveRowIndex;
                    SprPacientes.Col = Conexion_Base_datos.col_Plaza;
                    tstPlanta = Convert.ToInt32(Conversion.Val(SprPacientes.Text.Substring(0, Math.Min(2, SprPacientes.Text.Length))));
                    tstHabitacion = Convert.ToInt32(Conversion.Val(SprPacientes.Text.Substring(2, Math.Min(2, SprPacientes.Text.Length - 2))));
                    tstCama = SprPacientes.Text.Substring(4);
                    SprPacientes.Col = Conexion_Base_datos.col_Fprevalta;
                    if (TextoFecha == "NULL")
                    {
                        //actualiza la columna del spread
                        SprPacientes.Text = "";
                    }
                    else
                    {
                        SprPacientes.Text = TextoFecha;
                    }
                    SprPacientes.Col = Conexion_Base_datos.col_FAviso;
                    if (TextoFecha == "NULL")
                    {
                        //actualiza la columna del spread
                        SprPacientes.Text = "";
                    }
                    else
                    {
                        SprPacientes.Text = FechaAviso;
                    }

                    //oscar 07/06/04 (Fprevalt se pasa a aepisadm)
                    //sqlDatos = "select fprevalt " _
                    //'   & "from DCAMASBO " _
                    //'   & "where gplantas = " & tstPlanta & " and ghabitac= " & tstHabitacion & " and gcamasbo = '" & tstCama & "'"
                    //
                    //Set RsPaciente = RcAdmision.OpenResultset(sqlDatos, rdOpenKeyset, 3, rdExecDirect)
                    //
                    //If Not (RsPaciente.EOF And RsPaciente.BOF) Then
                    //   RsPaciente.Edit
                    //   If TextoFecha = "NULL" Then
                    //      RsPaciente(0) = Null
                    //   Else
                    //      RsPaciente(0) = TextoFecha
                    //   End If
                    //   RsPaciente.Update
                    //End If
                    //RsPaciente.Close

                    sqlDatos = "select faviso, fprevalt, gusuprea from AEPISADM WHERE" +
                               " GANOADME=" + viGanoadme.ToString() + " AND " +
                               " GNUMADME=" + viGnumadme.ToString() + " AND " +
                               " GIDENPAC='" + vstGidenpac + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlDatos, Conexion_Base_datos.RcAdmision);
                    RsPaciente = new DataSet();
                    tempAdapter.Fill(RsPaciente);

                    if (!(RsPaciente.Tables[0].Rows.Count == 0))
                    {

                        if (FechaAviso == "NULL")
                        {
                            RsPaciente.Tables[0].Rows[0]["faviso"] = DBNull.Value;
                        }
                        else
                        {
                            RsPaciente.Tables[0].Rows[0]["faviso"] = FechaAviso;
                        }
                        if (TextoFecha == "NULL")
                        {
                            RsPaciente.Tables[0].Rows[0]["fprevalt"] = DBNull.Value;
                        }
                        else
                        {
                            RsPaciente.Tables[0].Rows[0]["fprevalt"] = TextoFecha;
                        }
                        //********************* O.Frias 06/07/2007 *********************					
                        RsPaciente.Tables[0].Rows[0]["gusuprea"] = Conexion_Base_datos.VstCodUsua;
                        //********************* O.Frias 06/07/2007 *********************

                        string tempQuery = RsPaciente.Tables[0].TableName;
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, Conexion_Base_datos.RcAdmision);
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                        string sql2 = "UPDATE AEPISADM SET faviso = " + "'" + Convert.ToDateTime(RsPaciente.Tables[0].Rows[0]["faviso"]).ToString("yyyy-MM-dd HH:mm") + "'" + "," +
                                                           "fprevalt = " + "'" + Convert.ToDateTime(RsPaciente.Tables[0].Rows[0]["fprevalt"]).ToString("yyyy-MM-dd HH:mm") + "'" + "," +
                                                           "gusuprea = " + "'" + Conexion_Base_datos.VstCodUsua + "'" +
                                                            "WHERE" + " GANOADME=" + viGanoadme.ToString() + " AND " +
                                                            " GNUMADME=" + viGnumadme.ToString() + " AND " +
                                                            " GIDENPAC='" + vstGidenpac + "'";
                        SqlCommand tempCommand = new SqlCommand(sql2, Conexion_Base_datos.RcAdmision);
                        tempCommand.ExecuteNonQuery();
                        // tempAdapter_2.Update(RsPaciente, RsPaciente.Tables[0].TableName);
                    }
                    RsPaciente.Close();
                    //Conexion_Base_datos.RcAdmision.CommitTrans(transaction);
                    scope.Complete();

                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Grabar_prevision:menu_ingreso", ex);
                    //Conexion_Base_datos.RcAdmision.RollbackTrans(transaction);
                }
                
            }
        }
        //oscar 18/03/04
        //El boton Cambio de producto se activara cuando se haya registrado producto en el ingreso y
        //exista gestion de producto.

        private bool TieneProducto()
		{
			bool result = false;
			vstGidenpac = fnRecogerPac(SprPacientes.Row);
			string sql = "SELECT GPRODUCTO FROM AEPISADM WHERE " + 
			             " GANOADME=" + viGanoadme.ToString() + " AND " + 
			             " GNUMADME=" + viGnumadme.ToString() + " AND " + 
			             " GIDENPAC='" + vstGidenpac + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);			
			if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
			{
				result = true;
			}
			return result;
		}
        //-----------------------------

        private object RegistrarDsnAccess(string base_de_datos)
		{
			string tstConexion = "Description=" + "Microsoft Access para Enfermeria" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "" + "\r" + "dbq=" + Path.GetDirectoryName(Application.ExecutablePath) + "\\" + base_de_datos.Trim() + "";
            
            //camaya todo_x_3
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(Conexion_Base_datos.stNombreDsnACCESS, "MicroSoft Access Driver (*.mdb)", true, tstConexion);
			return null;
		}
        //Oscar C Febrero 2010

        private void proRecolocaControles()
		{
            this.Top = (int)0;
            this.Left = (int)0;
            this.Height  = (int)menu_admision.DefInstance.ClientRectangle.Height - 55;
            this.Width = (int)menu_admision.DefInstance.ClientRectangle.Width - 5;
            Label2.Width = (int)(this.ClientRectangle.Width - 10);
            SprPacientes.Width = (int)(this.ClientRectangle.Width - 20);
            SprPacientes.Height = (int)(this.ClientRectangle.Height - 133);
            Frame3.Top = (int)(SprPacientes.Height + 60);
            Frame1.Top = (int)Frame3.Top;
            cbRefrescar.Top = (int)Frame3.Top;
            Command1.Top = (int)Frame3.Top;
            Command1.Left = (int)(this.ClientRectangle.Width - Command1.Width - 20);
            cbRefrescar.Left = (int)(Command1.Left - Command1.Width - 20);
            pctAutoInfor.Top = (int)(SprPacientes.Height + 60);
            lblAutoInfor.Top = (int)(SprPacientes.Height + 60);
        }
        //-------------        		

        private string sValorColumna(UpgradeHelpers.Spread.FpSpread sprRejilla, int lColumna)
		{
			string result = String.Empty;
			int lColRestaurar = sprRejilla.Col;
			sprRejilla.Col = lColumna;
			result = sprRejilla.Text.Trim();
			sprRejilla.Col = lColRestaurar;
			return result;
		}
	}
}