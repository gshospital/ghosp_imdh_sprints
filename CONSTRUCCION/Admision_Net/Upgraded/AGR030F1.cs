using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace ADMISION
{
	public partial class ReservasAnuladas
		: Telerik.WinControls.UI.RadForm
	{

		public ReservasAnuladas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		private void ReservasAnuladas_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		Crystal cryInformes = null;
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;

		private void cbImpresora_Click(Object eventSender, EventArgs eventArgs)
		{
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }
        
        private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
        {
            	proImprimir(Crystal.DestinationConstants.crptToWindow);
        }
        private void cbsalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {

            if (chbFecha[0].CheckState == CheckState.Unchecked && chbFecha[1].CheckState == CheckState.Unchecked)
            {
                short tempRefParam = 1270;
                string[] tempRefParam2 = new string[] { "una opci�n" };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                return;
            }

            if (sdcDesde.NullableValue == null || sdcHasta.NullableValue == null)
            {
                short tempRefParam3 = 1040;
                string[] tempRefParam4 = new string[] { "Fecha" };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                if (sdcDesde.NullableValue == null)
                {
                    sdcDesde.Focus();
                }
                else
                {
                    sdcHasta.Focus();
                }
                return;
            }
            if (sdcDesdeAnulacion.NullableValue == null || sdcHastaAnulacion.NullableValue == null)
            {
                short tempRefParam5 = 1040;
                string[] tempRefParam6 = new string[] { "Fecha" };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                if (sdcDesdeAnulacion.NullableValue == null)
                {
                    sdcDesdeAnulacion.Focus();
                }
                else
                {
                    sdcHastaAnulacion.Focus();
                }
                return;
            }
            if (sdcDesde.Value.Date > sdcHasta.Value.Date)
            {
                // Control del mensaje enviado
                stMensaje1 = "menor o igual";
                short tempRefParam7 = 1020;
                string[] tempRefParam8 = new string[] { lbDesde.Text, stMensaje1, lbHasta.Text };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
                // ***************************
                sdcDesde.Focus();
                return;
            }
            //la fecha prevista de ingreso puede ser mayor que la fecha actual
            //    If CDate(sdcHasta.Date) > CDate(Format(Date, "DD/MM/YYYY")) Then
            //        ' Control del mensaje enviado
            //        stMensaje1 = "menor o igual"
            //        stMensaje2 = "Fecha del Sistema"
            //        iResume = oClass.RespuestaMensaje(vNomAplicacion, vAyuda, 1020, _
            //'            RcAdmision, lbHasta.Caption, stMensaje1, stMensaje2)
            //        ' ***************************
            //        sdcHasta.SetFocus
            //        Exit Sub
            //    End If
            //la fecha de anulacion no puede ser mayor que la actual
            if (sdcDesdeAnulacion.Value.Date > sdcHastaAnulacion.Value.Date)
            {
                // Control del mensaje enviado
                stMensaje1 = "menor o igual";
                short tempRefParam9 = 1020;
                string[] tempRefParam10 = new string[] { lbDesde2.Text, stMensaje1, lbHasta.Text };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                // ***************************
                sdcDesdeAnulacion.Focus();
                return;
            }
            //la fecha de anulacion no puede ser mayor que la fecha actual
            if (sdcHastaAnulacion.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
            {
                // Control del mensaje enviado
                stMensaje1 = "menor o igual";
                stMensaje2 = "Fecha del Sistema";
                short tempRefParam11 = 1020;
                string[] tempRefParam12 = new string[] { lbHasta2.Text, stMensaje1, stMensaje2 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12));
                // ***************************
                sdcHastaAnulacion.Focus();
                return;
            }

            string sqlReservAnul = "select ";
            sqlReservAnul = sqlReservAnul + "dpacient.dnombpac,dpacient.dape1pac,dpacient.dape2pac,  ";
            sqlReservAnul = sqlReservAnul + "ARESANUL.fpreving, ARESANUL.fanulaci, ARESANUL.gplantas, ARESANUL.ghabitac, ARESANUL.gcamasbo, ARESANUL.odiaging, ";
            sqlReservAnul = sqlReservAnul + "a.dnompers as DPERSONA_RES$dnompers,a.dap1pers AS DPERSONA_RES$dap1pers,a.dap2pers AS DPERSONA_RES$dap2pers, ";
            sqlReservAnul = sqlReservAnul + "DPERSONA.dnompers as DPERSONA$dnompers,DPERSONA.dap1pers as DPERSONA$dap1pers,DPERSONA.dap2pers as DPERSONA$dap2pers, ";
            sqlReservAnul = sqlReservAnul + "dservici.dnomserv,ddiagnos.gcodidia , dnombdia, dunienfe.dunidenf, aresanul.fanulaci  ";
            sqlReservAnul = sqlReservAnul + "from aresanul ";
            sqlReservAnul = sqlReservAnul + "inner join susuario on susuario.gusuario = aresanul.gusualta ";
            sqlReservAnul = sqlReservAnul + "inner join dpacient on aresanul.gidenpac = dpacient.gidenpac ";
            sqlReservAnul = sqlReservAnul + "inner join DPERSONA  on susuario.gpersona = dpersona.gpersona ";
            sqlReservAnul = sqlReservAnul + "left join dpersona a on aresanul.gpersona = a.gpersona ";
            sqlReservAnul = sqlReservAnul + "left join dservici on aresanul.gservici = dservici.gservici ";
            sqlReservAnul = sqlReservAnul + "left join dunienfe on aresanul.gunienfe = dunienfe.gunidenf ";
            sqlReservAnul = sqlReservAnul + "left join ddiagnos on aresanul.gdiaging = ddiagnos.gcodidia and aresanul.finvaldi = ddiagnos.finvaldi  ";

            sqlReservAnul = sqlReservAnul + "Where ";
            if (chbFecha[0].CheckState == CheckState.Checked)
            {
                object tempRefParam13 = sdcDesde.Text + " 00:00:00";
                sqlReservAnul = sqlReservAnul + "aresanul.fpreving>= " + Serrores.FormatFechaHMS(tempRefParam13) + " and ";
                object tempRefParam14 = sdcHasta.Text + " 23:59:59";
                sqlReservAnul = sqlReservAnul + "aresanul.fpreving<= " + Serrores.FormatFechaHMS(tempRefParam14) + " ";
            }

            if (chbFecha[1].CheckState == CheckState.Checked)
            {
                if (chbFecha[0].CheckState == CheckState.Checked && chbFecha[1].CheckState == CheckState.Checked)
                {
                    sqlReservAnul = sqlReservAnul + " AND ";
                }
                object tempRefParam15 = sdcDesdeAnulacion.Text + " 00:00:00";
                sqlReservAnul = sqlReservAnul + "aresanul.fanulaci>= " + Serrores.FormatFechaHMS(tempRefParam15) + " and ";
                object tempRefParam16 = sdcHastaAnulacion.Text + " 23:59:59";
                sqlReservAnul = sqlReservAnul + "aresanul.fanulaci<= " + Serrores.FormatFechaHMS(tempRefParam16) + " ";
            }

            if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
            {
                Conexion_Base_datos.proCabCrystal();
            }
            cryInformes.ReportFileName = Conexion_Base_datos.PathString + "\\RPT\\agr030r1_CR11.rpt";
            cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
            cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
            cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
            string stFormulaCabecera = "";
            if (chbFecha[0].CheckState == CheckState.Checked)
            { //fecha prevista de ingreso
                stFormulaCabecera = "Fecha Prevista de Ingreso Desde: " + sdcDesde.Value.Date + " Hasta: " + sdcHasta.Value.Date + " ";
            }

            if (chbFecha[1].CheckState == CheckState.Checked)
            {
                if (stFormulaCabecera.Trim() != "")
                {
                    stFormulaCabecera = stFormulaCabecera + " y ";
                }
                stFormulaCabecera = stFormulaCabecera + "Fecha Anulaci�n Desde: " + sdcDesdeAnulacion.Value.Date + " Hasta: " + sdcHastaAnulacion.Value.Date + " ";
            }
            cryInformes.Formulas["{@Cabecera}"] = "\"" + stFormulaCabecera.Trim() + "\"";
            cryInformes.SQLQuery = sqlReservAnul;
            
            cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
            cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
            cryInformes.Destination = Parasalida;
            cryInformes.WindowShowPrintSetupBtn = true;
            if (cryInformes.Destination == Crystal.DestinationConstants.crptToPrinter)
            {
                Conexion_Base_datos.proDialogo_impre(cryInformes);
            }
            cryInformes.Action = 1;
            cryInformes.PrinterStopPage = cryInformes.PageCount;
            this.Cursor = Cursors.Default;
        }

        private void ReservasAnuladas_Load(Object eventSender, EventArgs eventArgs)
        {
            cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;
        }
        private void ReservasAnuladas_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}