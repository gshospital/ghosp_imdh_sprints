using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class frmListInformes
	{

		#region "Upgrade Support "
		private static frmListInformes m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmListInformes DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmListInformes();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cmbAceptar", "cmbCancelar", "sprDocumentos", "Frame3", "lblDatosEpisodio", "Label1", "lblDatosPaciente", "lblPaciente", "Frame2", "Frame1", "sprDocumentos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cmbAceptar;
		public Telerik.WinControls.UI.RadButton cmbCancelar;
		public UpgradeHelpers.Spread.FpSpread sprDocumentos;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadTextBox lblDatosEpisodio;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox lblDatosPaciente;
		public Telerik.WinControls.UI.RadLabel lblPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;

        private Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1;
        private Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1;
        private Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2;

        //	private FarPoint.Win.Spread.SheetView sprDocumentos_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmListInformes));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.cmbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cmbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.sprDocumentos = new UpgradeHelpers.Spread.FpSpread();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.lblDatosEpisodio = new Telerik.WinControls.UI.RadTextBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.lblDatosPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.lblPaciente = new Telerik.WinControls.UI.RadLabel();
			this.Frame1.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
            gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmbAceptar);
			this.Frame1.Controls.Add(this.cmbCancelar);
			this.Frame1.Controls.Add(this.Frame3);
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(4, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(609, 317);
			this.Frame1.TabIndex = 0;
			this.Frame1.Visible = true;
			// 
			// cmbAceptar
			// 
			this.cmbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmbAceptar.Location = new System.Drawing.Point(448, 276);
			this.cmbAceptar.Name = "cmbAceptar";
			this.cmbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmbAceptar.Size = new System.Drawing.Size(67, 29);
			this.cmbAceptar.TabIndex = 9;
			this.cmbAceptar.Text = "&Aceptar";
			this.cmbAceptar.Click += new System.EventHandler(this.cmbAceptar_Click);
			// 
			// cmbCancelar
			// 
			this.cmbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmbCancelar.Location = new System.Drawing.Point(532, 276);
			this.cmbCancelar.Name = "cmbCancelar";
			this.cmbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmbCancelar.Size = new System.Drawing.Size(67, 29);
			this.cmbCancelar.TabIndex = 8;
			this.cmbCancelar.Text = "&Cancelar";
			this.cmbCancelar.Click += new System.EventHandler(this.cmbCancelar_Click);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.sprDocumentos);
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(8, 56);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(591, 211);
			this.Frame3.TabIndex = 6;
			this.Frame3.Visible = true;
            // 
            // gridViewTextBoxColumn1
            //
            this.gridViewTextBoxColumn1.HeaderText = "Documento";
            this.gridViewTextBoxColumn1.Name = "Documento";
            this.gridViewTextBoxColumn1.Width = 400;
            this.gridViewTextBoxColumn1.AllowSort = false;
            this.gridViewTextBoxColumn1.ReadOnly = true;
            this.gridViewCheckBoxColumn1.HeaderText = "Agregado H.C.";
            this.gridViewCheckBoxColumn1.Name = "Agregado";
            this.gridViewCheckBoxColumn1.Width = 80;
            this.gridViewCheckBoxColumn1.AllowSort = false;
            this.gridViewTextBoxColumn2.HeaderText = "C�digo Documento";
            this.gridViewTextBoxColumn2.Name = "Codigo";
            this.gridViewTextBoxColumn2.Width = 120;
            // 
            // sprDocumentos
            // 
            this.sprDocumentos.Location = new System.Drawing.Point(8, 16);
			this.sprDocumentos.Name = "sprDocumentos";
			this.sprDocumentos.Size = new System.Drawing.Size(571, 183);
			this.sprDocumentos.TabIndex = 7;
            this.sprDocumentos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
                this.gridViewTextBoxColumn1,
                this.gridViewCheckBoxColumn1,
                this.gridViewTextBoxColumn2});
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.lblDatosEpisodio);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Controls.Add(this.lblDatosPaciente);
			this.Frame2.Controls.Add(this.lblPaciente);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(8, 10);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(591, 45);
			this.Frame2.TabIndex = 1;
			this.Frame2.Visible = true;
			// 
			// lblDatosEpisodio
			// 
			this.lblDatosEpisodio.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblDatosEpisodio.Location = new System.Drawing.Point(64, 14);
			this.lblDatosEpisodio.Name = "lblDatosEpisodio";
			this.lblDatosEpisodio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblDatosEpisodio.Size = new System.Drawing.Size(109, 19);
			this.lblDatosEpisodio.TabIndex = 5;
            this.lblDatosEpisodio.Enabled = false;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(12, 17);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(47, 13);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "Episodio:";
			// 
			// lblDatosPaciente
			// 
			this.lblDatosPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblDatosPaciente.Location = new System.Drawing.Point(236, 14);
			this.lblDatosPaciente.Name = "lblDatosPaciente";
			this.lblDatosPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblDatosPaciente.Size = new System.Drawing.Size(343, 19);
			this.lblDatosPaciente.TabIndex = 3;
            this.lblDatosPaciente.Enabled = false;
            // 
            // lblPaciente
            // 
            this.lblPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblPaciente.Location = new System.Drawing.Point(184, 17);
			this.lblPaciente.Name = "lblPaciente";
			this.lblPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblPaciente.Size = new System.Drawing.Size(47, 13);
			this.lblPaciente.TabIndex = 2;
			this.lblPaciente.Text = "Paciente:";
			// 
			// frmListInformes
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(618, 321);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 23);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmListInformes";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Documentaci�n agregada al sobre de Historia Cl�nica";
			this.Activated += new System.EventHandler(this.frmListInformes_Activated);
			this.Closed += new System.EventHandler(this.frmListInformes_Closed);
			this.Frame1.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}