using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class FiMailing
	{

		#region "Upgrade Support "
		private static FiMailing m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static FiMailing DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new FiMailing();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CommonDialog1Save", "cbCancelar", "cbAceptar", "SdcFechaIni", "SdcFechaFin", "Label2", "Label1", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public System.Windows.Forms.SaveFileDialog CommonDialog1Save;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaFin;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FiMailing));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.CommonDialog1Save = new System.Windows.Forms.SaveFileDialog();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
			this.SdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbCancelar
			// 
		
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
		
			this.cbCancelar.Location = new System.Drawing.Point(168, 112);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 4;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
		
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
		
			this.cbAceptar.Location = new System.Drawing.Point(80, 112);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 3;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// Frame1
			// 
	
			this.Frame1.Controls.Add(this.SdcFechaIni);
			this.Frame1.Controls.Add(this.SdcFechaFin);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(24, 8);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(225, 89);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Pacientes dados de alta";
			this.Frame1.Visible = true;
            // 
            // SdcFechaIni
            this.SdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaIni.Location = new System.Drawing.Point(72, 24);
			this.SdcFechaIni.Name = "SdcFechaIni";
			this.SdcFechaIni.Size = new System.Drawing.Size(105, 17);
			this.SdcFechaIni.TabIndex = 5;
			// 
			// SdcFechaFin
			// 
			//this.SdcFechaFin.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SdcFechaFin.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaFin.Location = new System.Drawing.Point(72, 56);
			this.SdcFechaFin.Name = "SdcFechaFin";
			this.SdcFechaFin.Size = new System.Drawing.Size(105, 17);
			this.SdcFechaFin.TabIndex = 6;
			// 
			// Label2
			// 
			
			
			this.Label2.Location = new System.Drawing.Point(24, 56);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(33, 17);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "Hasta:";
			// 
			// Label1
			// 
			
			this.Label1.Location = new System.Drawing.Point(24, 24);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(33, 17);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "Desde:";
			// 
			// FiMailing
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			
			this.ClientSize = new System.Drawing.Size(265, 150);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(186, 171);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FiMailing";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Fichero para mailing - AGI440F1";
			this.Closed += new System.EventHandler(this.FiMailing_Closed);
			this.Load += new System.EventHandler(this.FiMailing_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}