using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Movimientos_SS
	{

		#region "Upgrade Support "
		private static Movimientos_SS m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Movimientos_SS DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Movimientos_SS();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbImprimir", "CbCerrar", "CbPantalla", "ChBHosDia", "ChBHosGeneral", "FrmHospital", "_Tbdiagini_Button1", "Tbdiagini", "_TbDiagfin_Button1", "TbDiagfin", "tbCodiDiagini", "tbDiagnosini", "TbCodDiagfin", "TbDiagnosfin", "Lbdiagini", "Lbdiagfin", "Frame2", "CbDelUno", "CbDelTodo", "CbInsUno", "CbInsTodo", "LsbLista2", "LsbLista1", "FrInspecciones", "SdcFechaIni", "sdcFechaFin", "Label2", "Label1", "FrFecha", "chbSalidas", "chbEntradas", "Frtipomov", "FrPantalla", "IlIconos", "listBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		public Telerik.WinControls.UI.RadCheckBox ChBHosDia;
		public Telerik.WinControls.UI.RadCheckBox ChBHosGeneral;
		public Telerik.WinControls.UI.RadGroupBox FrmHospital;
		private Telerik.WinControls.UI.RadButton _Tbdiagini_Button1;
		private Telerik.WinControls.UI.RadButton _TbDiagfin_Button1;
		public Telerik.WinControls.UI.RadTextBoxControl tbCodiDiagini;
		public Telerik.WinControls.UI.RadTextBoxControl tbDiagnosini;
		public Telerik.WinControls.UI.RadTextBoxControl TbCodDiagfin;
		public Telerik.WinControls.UI.RadTextBoxControl TbDiagnosfin;
		public Telerik.WinControls.UI.RadLabel Lbdiagini;
		public Telerik.WinControls.UI.RadLabel Lbdiagfin;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton CbDelUno;
		public Telerik.WinControls.UI.RadButton CbDelTodo;
		public Telerik.WinControls.UI.RadButton CbInsUno;
		public Telerik.WinControls.UI.RadButton CbInsTodo;
        public Telerik.WinControls.UI.RadListControl LsbLista2;
		public Telerik.WinControls.UI.RadListControl LsbLista1;
		public Telerik.WinControls.UI.RadGroupBox FrInspecciones;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaFin;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox FrFecha;
		public Telerik.WinControls.UI.RadCheckBox chbSalidas;
		public Telerik.WinControls.UI.RadCheckBox chbEntradas;
		public Telerik.WinControls.UI.RadGroupBox Frtipomov;
		public Telerik.WinControls.UI.RadGroupBox FrPantalla;
		public System.Windows.Forms.ImageList IlIconos;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Movimientos_SS));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            this.FrPantalla = new Telerik.WinControls.UI.RadGroupBox();
            this.FrmHospital = new Telerik.WinControls.UI.RadGroupBox();
            this.ChBHosDia = new Telerik.WinControls.UI.RadCheckBox();
            this.ChBHosGeneral = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.TbDiagnosfin = new Telerik.WinControls.UI.RadTextBoxControl();
            this._Tbdiagini_Button1 = new Telerik.WinControls.UI.RadButton();
            this.IlIconos = new System.Windows.Forms.ImageList(this.components);
            this.TbCodDiagfin = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Lbdiagini = new Telerik.WinControls.UI.RadLabel();
            this._TbDiagfin_Button1 = new Telerik.WinControls.UI.RadButton();
            this.tbDiagnosini = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Lbdiagfin = new Telerik.WinControls.UI.RadLabel();
            this.tbCodiDiagini = new Telerik.WinControls.UI.RadTextBoxControl();
            this.FrInspecciones = new Telerik.WinControls.UI.RadGroupBox();
            this.CbDelUno = new Telerik.WinControls.UI.RadButton();
            this.CbDelTodo = new Telerik.WinControls.UI.RadButton();
            this.CbInsUno = new Telerik.WinControls.UI.RadButton();
            this.CbInsTodo = new Telerik.WinControls.UI.RadButton();
            this.LsbLista2 = new Telerik.WinControls.UI.RadListControl();
            this.LsbLista1 = new Telerik.WinControls.UI.RadListControl();
            this.FrFecha = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Frtipomov = new Telerik.WinControls.UI.RadGroupBox();
            this.chbSalidas = new Telerik.WinControls.UI.RadCheckBox();
            this.chbEntradas = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrPantalla)).BeginInit();
            this.FrPantalla.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).BeginInit();
            this.FrmHospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TbDiagnosfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Tbdiagini_Button1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbCodDiagfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbdiagini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TbDiagfin_Button1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDiagnosini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbdiagfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCodiDiagini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrInspecciones)).BeginInit();
            this.FrInspecciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrFecha)).BeginInit();
            this.FrFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frtipomov)).BeginInit();
            this.Frtipomov.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbSalidas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEntradas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.Location = new System.Drawing.Point(357, 385);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 12;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(532, 385);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 14;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.Location = new System.Drawing.Point(444, 385);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(80, 29);
            this.CbPantalla.TabIndex = 13;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // FrPantalla
            // 
            this.FrPantalla.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrPantalla.Controls.Add(this.FrmHospital);
            this.FrPantalla.Controls.Add(this.Frame2);
            this.FrPantalla.Controls.Add(this.FrInspecciones);
            this.FrPantalla.Controls.Add(this.FrFecha);
            this.FrPantalla.Controls.Add(this.Frtipomov);
            this.FrPantalla.HeaderText = "";
            this.FrPantalla.Location = new System.Drawing.Point(4, 0);
            this.FrPantalla.Name = "FrPantalla";
            this.FrPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrPantalla.Size = new System.Drawing.Size(610, 380);
            this.FrPantalla.TabIndex = 0;
            // 
            // FrmHospital
            // 
            this.FrmHospital.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmHospital.Controls.Add(this.ChBHosDia);
            this.FrmHospital.Controls.Add(this.ChBHosGeneral);
            this.FrmHospital.HeaderText = "Tipo de Ingreso";
            this.FrmHospital.Location = new System.Drawing.Point(8, 160);
            this.FrmHospital.Name = "FrmHospital";
            this.FrmHospital.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmHospital.Size = new System.Drawing.Size(591, 41);
            this.FrmHospital.TabIndex = 28;
            this.FrmHospital.Text = "Tipo de Ingreso";
            // 
            // ChBHosDia
            // 
            this.ChBHosDia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosDia.Location = new System.Drawing.Point(280, 16);
            this.ChBHosDia.Name = "ChBHosDia";
            this.ChBHosDia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosDia.Size = new System.Drawing.Size(97, 18);
            this.ChBHosDia.TabIndex = 19;
            this.ChBHosDia.Text = "Hospital de Día";
            this.ChBHosDia.CheckStateChanged += new System.EventHandler(this.ChBHosDia_CheckStateChanged);
            // 
            // ChBHosGeneral
            // 
            this.ChBHosGeneral.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosGeneral.Location = new System.Drawing.Point(48, 16);
            this.ChBHosGeneral.Name = "ChBHosGeneral";
            this.ChBHosGeneral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosGeneral.Size = new System.Drawing.Size(138, 18);
            this.ChBHosGeneral.TabIndex = 18;
            this.ChBHosGeneral.Text = "Hospitalización General";
            this.ChBHosGeneral.CheckStateChanged += new System.EventHandler(this.ChBHosGeneral_CheckStateChanged);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.TbDiagnosfin);
            this.Frame2.Controls.Add(this._Tbdiagini_Button1);
            this.Frame2.Controls.Add(this.TbCodDiagfin);
            this.Frame2.Controls.Add(this.Lbdiagini);
            this.Frame2.Controls.Add(this._TbDiagfin_Button1);
            this.Frame2.Controls.Add(this.tbDiagnosini);
            this.Frame2.Controls.Add(this.Lbdiagfin);
            this.Frame2.Controls.Add(this.tbCodiDiagini);
            this.Frame2.HeaderText = "Seleccione rango de diagnósticos";
            this.Frame2.Location = new System.Drawing.Point(10, 58);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(591, 96);
            this.Frame2.TabIndex = 19;
            this.Frame2.Text = "Seleccione rango de diagnósticos";
            // 
            // TbDiagnosfin
            // 
            this.TbDiagnosfin.AcceptsReturn = true;
            this.TbDiagnosfin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbDiagnosfin.Location = new System.Drawing.Point(79, 70);
            this.TbDiagnosfin.MaxLength = 0;
            this.TbDiagnosfin.Name = "TbDiagnosfin";
            this.TbDiagnosfin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TbDiagnosfin.Size = new System.Drawing.Size(475, 19);
            this.TbDiagnosfin.TabIndex = 15;
            this.TbDiagnosfin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbDiagnosfin_KeyPress);
            // 
            // _Tbdiagini_Button1
            // 
            this._Tbdiagini_Button1.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this._Tbdiagini_Button1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this._Tbdiagini_Button1.ImageIndex = 0;
            this._Tbdiagini_Button1.ImageList = this.IlIconos;
            this._Tbdiagini_Button1.Location = new System.Drawing.Point(559, 30);
            this._Tbdiagini_Button1.Name = "_Tbdiagini_Button1";
            this._Tbdiagini_Button1.Size = new System.Drawing.Size(27, 25);
            this._Tbdiagini_Button1.TabIndex = 3;
            this._Tbdiagini_Button1.Tag = "";
            this._Tbdiagini_Button1.Click += new System.EventHandler(this.Tbdiagini_ButtonClick);
            // 
            // IlIconos
            // 
            this.IlIconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IlIconos.ImageStream")));
            this.IlIconos.TransparentColor = System.Drawing.Color.Silver;
            this.IlIconos.Images.SetKeyName(0, "");
            // 
            // TbCodDiagfin
            // 
            this.TbCodDiagfin.AcceptsReturn = true;
            this.TbCodDiagfin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbCodDiagfin.Location = new System.Drawing.Point(7, 70);
            this.TbCodDiagfin.MaxLength = 0;
            this.TbCodDiagfin.Name = "TbCodDiagfin";
            this.TbCodDiagfin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TbCodDiagfin.Size = new System.Drawing.Size(57, 19);
            this.TbCodDiagfin.TabIndex = 5;
            this.TbCodDiagfin.TextChanged += new System.EventHandler(this.TbCodDiagfin_TextChanged);
            this.TbCodDiagfin.Enter += new System.EventHandler(this.TbCodDiagfin_Enter);
            this.TbCodDiagfin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbCodDiagfin_KeyPress);
            this.TbCodDiagfin.Leave += new System.EventHandler(this.TbCodDiagfin_Leave);
            // 
            // Lbdiagini
            // 
            this.Lbdiagini.Cursor = System.Windows.Forms.Cursors.Default;
            this.Lbdiagini.Location = new System.Drawing.Point(6, 16);
            this.Lbdiagini.Name = "Lbdiagini";
            this.Lbdiagini.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbdiagini.Size = new System.Drawing.Size(98, 18);
            this.Lbdiagini.TabIndex = 23;
            this.Lbdiagini.Text = "Diagnóstico Inicio:";
            // 
            // _TbDiagfin_Button1
            // 
            this._TbDiagfin_Button1.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this._TbDiagfin_Button1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this._TbDiagfin_Button1.ImageIndex = 0;
            this._TbDiagfin_Button1.ImageList = this.IlIconos;
            this._TbDiagfin_Button1.Location = new System.Drawing.Point(559, 67);
            this._TbDiagfin_Button1.Name = "_TbDiagfin_Button1";
            this._TbDiagfin_Button1.Size = new System.Drawing.Size(27, 25);
            this._TbDiagfin_Button1.TabIndex = 5;
            this._TbDiagfin_Button1.Tag = "";
            this._TbDiagfin_Button1.Click += new System.EventHandler(this.TbDiagfin_ButtonClick);
            // 
            // tbDiagnosini
            // 
            this.tbDiagnosini.AcceptsReturn = true;
            this.tbDiagnosini.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDiagnosini.Location = new System.Drawing.Point(78, 33);
            this.tbDiagnosini.MaxLength = 0;
            this.tbDiagnosini.Name = "tbDiagnosini";
            this.tbDiagnosini.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDiagnosini.Size = new System.Drawing.Size(475, 19);
            this.tbDiagnosini.TabIndex = 16;
            this.tbDiagnosini.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDiagnosini_KeyPress);
            // 
            // Lbdiagfin
            // 
            this.Lbdiagfin.Cursor = System.Windows.Forms.Cursors.Default;
            this.Lbdiagfin.Location = new System.Drawing.Point(7, 53);
            this.Lbdiagfin.Name = "Lbdiagfin";
            this.Lbdiagfin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbdiagfin.Size = new System.Drawing.Size(86, 18);
            this.Lbdiagfin.TabIndex = 22;
            this.Lbdiagfin.Text = "Diagnóstico Fin:";
            // 
            // tbCodiDiagini
            // 
            this.tbCodiDiagini.AcceptsReturn = true;
            this.tbCodiDiagini.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCodiDiagini.Location = new System.Drawing.Point(6, 33);
            this.tbCodiDiagini.MaxLength = 0;
            this.tbCodiDiagini.Name = "tbCodiDiagini";
            this.tbCodiDiagini.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCodiDiagini.Size = new System.Drawing.Size(57, 19);
            this.tbCodiDiagini.TabIndex = 4;
            this.tbCodiDiagini.TextChanged += new System.EventHandler(this.tbCodiDiagini_TextChanged);
            this.tbCodiDiagini.Enter += new System.EventHandler(this.tbCodiDiagini_Enter);
            this.tbCodiDiagini.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCodiDiagini_KeyPress);
            this.tbCodiDiagini.Leave += new System.EventHandler(this.tbCodiDiagini_Leave);
            // 
            // FrInspecciones
            // 
            this.FrInspecciones.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrInspecciones.Controls.Add(this.CbDelUno);
            this.FrInspecciones.Controls.Add(this.CbDelTodo);
            this.FrInspecciones.Controls.Add(this.CbInsUno);
            this.FrInspecciones.Controls.Add(this.CbInsTodo);
            this.FrInspecciones.Controls.Add(this.LsbLista2);
            this.FrInspecciones.Controls.Add(this.LsbLista1);
            this.FrInspecciones.HeaderText = "Seleccione Inspecciones";
            this.FrInspecciones.Location = new System.Drawing.Point(10, 202);
            this.FrInspecciones.Name = "FrInspecciones";
            this.FrInspecciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrInspecciones.Size = new System.Drawing.Size(591, 169);
            this.FrInspecciones.TabIndex = 18;
            this.FrInspecciones.Text = "Seleccione Inspecciones";
            // 
            // CbDelUno
            // 
            this.CbDelUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelUno.Location = new System.Drawing.Point(281, 142);
            this.CbDelUno.Name = "CbDelUno";
            this.CbDelUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelUno.Size = new System.Drawing.Size(33, 17);
            this.CbDelUno.TabIndex = 10;
            this.CbDelUno.Text = "<";
            this.CbDelUno.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // CbDelTodo
            // 
            this.CbDelTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelTodo.Location = new System.Drawing.Point(281, 100);
            this.CbDelTodo.Name = "CbDelTodo";
            this.CbDelTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelTodo.Size = new System.Drawing.Size(33, 17);
            this.CbDelTodo.TabIndex = 8;
            this.CbDelTodo.Text = "<<";
            this.CbDelTodo.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // CbInsUno
            // 
            this.CbInsUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsUno.Location = new System.Drawing.Point(281, 57);
            this.CbInsUno.Name = "CbInsUno";
            this.CbInsUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsUno.Size = new System.Drawing.Size(33, 17);
            this.CbInsUno.TabIndex = 7;
            this.CbInsUno.Text = ">";
            this.CbInsUno.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // CbInsTodo
            // 
            this.CbInsTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsTodo.Location = new System.Drawing.Point(281, 14);
            this.CbInsTodo.Name = "CbInsTodo";
            this.CbInsTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsTodo.Size = new System.Drawing.Size(33, 17);
            this.CbInsTodo.TabIndex = 6;
            this.CbInsTodo.Text = ">>";
            this.CbInsTodo.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // LsbLista2
            // 
            this.LsbLista2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista2.Location = new System.Drawing.Point(319, 14);
            this.LsbLista2.Name = "LsbLista2";
            this.LsbLista2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista2.Size = new System.Drawing.Size(262, 147);
            this.LsbLista2.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.LsbLista2.TabIndex = 9;
            this.LsbLista2.DoubleClick += new System.EventHandler(this.LsbLista2_DoubleClick);
            // 
            // LsbLista1
            // 
            this.LsbLista1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista1.Location = new System.Drawing.Point(9, 14);
            this.LsbLista1.Name = "LsbLista1";
            this.LsbLista1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista1.Size = new System.Drawing.Size(262, 147);
            this.LsbLista1.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.LsbLista1.TabIndex = 11;
            this.LsbLista1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.LsbLista1_SelectedIndexChanged);
            this.LsbLista1.DoubleClick += new System.EventHandler(this.LsbLista1_DoubleClick);
            // 
            // FrFecha
            // 
            this.FrFecha.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrFecha.Controls.Add(this.SdcFechaIni);
            this.FrFecha.Controls.Add(this.sdcFechaFin);
            this.FrFecha.Controls.Add(this.Label2);
            this.FrFecha.Controls.Add(this.Label1);
            this.FrFecha.HeaderText = "Seleccione rango de fechas del movimiento";
            this.FrFecha.Location = new System.Drawing.Point(203, 12);
            this.FrFecha.Name = "FrFecha";
            this.FrFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrFecha.Size = new System.Drawing.Size(391, 40);
            this.FrFecha.TabIndex = 16;
            this.FrFecha.Text = "Seleccione rango de fechas del movimiento";
            // 
            // SdcFechaIni
            // 
            this.SdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaIni.Location = new System.Drawing.Point(90, 14);
            this.SdcFechaIni.Name = "SdcFechaIni";
            this.SdcFechaIni.Size = new System.Drawing.Size(105, 20);
            this.SdcFechaIni.TabIndex = 3;
            this.SdcFechaIni.TabStop = false;
            this.SdcFechaIni.Text = "22/04/2016";
            this.SdcFechaIni.Value = new System.DateTime(2016, 4, 22, 18, 44, 28, 135);
            // 
            // sdcFechaFin
            // 
            this.sdcFechaFin.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaFin.Location = new System.Drawing.Point(276, 14);
            this.sdcFechaFin.Name = "sdcFechaFin";
            this.sdcFechaFin.Size = new System.Drawing.Size(105, 20);
            this.sdcFechaFin.TabIndex = 17;
            this.sdcFechaFin.TabStop = false;
            this.sdcFechaFin.Text = "22/04/2016";
            this.sdcFechaFin.Value = new System.DateTime(2016, 4, 22, 18, 44, 28, 143);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(220, 14);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(53, 18);
            this.Label2.TabIndex = 27;
            this.Label2.Text = "Fecha fin:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 14);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(67, 18);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "Fecha inicio:";
            // 
            // Frtipomov
            // 
            this.Frtipomov.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frtipomov.Controls.Add(this.chbSalidas);
            this.Frtipomov.Controls.Add(this.chbEntradas);
            this.Frtipomov.HeaderText = "Seleccione tipo de movimiento";
            this.Frtipomov.Location = new System.Drawing.Point(11, 12);
            this.Frtipomov.Name = "Frtipomov";
            this.Frtipomov.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frtipomov.Size = new System.Drawing.Size(183, 40);
            this.Frtipomov.TabIndex = 15;
            this.Frtipomov.Text = "Seleccione tipo de movimiento";
            // 
            // chbSalidas
            // 
            this.chbSalidas.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbSalidas.Location = new System.Drawing.Point(90, 18);
            this.chbSalidas.Name = "chbSalidas";
            this.chbSalidas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbSalidas.Size = new System.Drawing.Size(55, 18);
            this.chbSalidas.TabIndex = 2;
            this.chbSalidas.Text = "Salidas";
            this.chbSalidas.CheckStateChanged += new System.EventHandler(this.chbSalidas_CheckStateChanged);
            // 
            // chbEntradas
            // 
            this.chbEntradas.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbEntradas.Location = new System.Drawing.Point(6, 18);
            this.chbEntradas.Name = "chbEntradas";
            this.chbEntradas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbEntradas.Size = new System.Drawing.Size(63, 18);
            this.chbEntradas.TabIndex = 1;
            this.chbEntradas.Text = "Entradas";
            this.chbEntradas.CheckStateChanged += new System.EventHandler(this.chbEntradas_CheckStateChanged);
            // 
            // Movimientos_SS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(618, 420);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbPantalla);
            this.Controls.Add(this.FrPantalla);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Movimientos_SS";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Movimientos para la Seguridad Social (AGI4110)";
            this.Activated += new System.EventHandler(this.Movimientos_SS_Activated);
            this.Closed += new System.EventHandler(this.Movimientos_SS_Closed);
            this.Load += new System.EventHandler(this.Movimientos_SS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrPantalla)).EndInit();
            this.FrPantalla.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).EndInit();
            this.FrmHospital.ResumeLayout(false);
            this.FrmHospital.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TbDiagnosfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Tbdiagini_Button1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbCodDiagfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbdiagini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TbDiagfin_Button1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDiagnosini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbdiagfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCodiDiagini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrInspecciones)).EndInit();
            this.FrInspecciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CbDelUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrFecha)).EndInit();
            this.FrFecha.ResumeLayout(false);
            this.FrFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frtipomov)).EndInit();
            this.Frtipomov.ResumeLayout(false);
            this.Frtipomov.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbSalidas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEntradas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}