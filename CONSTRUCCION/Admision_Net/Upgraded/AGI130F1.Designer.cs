using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Anulacion_ingreso
	{

		#region "Upgrade Support "
		private static Anulacion_ingreso m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Anulacion_ingreso DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Anulacion_ingreso();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbMotAlta", "Label16", "Label15", "Label14", "Label13", "Label12", "lbPaciente", "lbHistoria", "lbEntFinancia", "lbNAsegurado", "Frame1", "cbAceptar", "cbCancelar", "sprDocumentos_Sheet1", "SprProductos_Sheet1", "SprPacientes_Sheet1", "SprEntidades_Sheet1", "SprEpisodios_Sheet1", "Sprcambios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadDropDownList cbbMotAlta;
		public Telerik.WinControls.UI.RadLabel Label16;
		public Telerik.WinControls.UI.RadLabel Label15;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadLabel Label13;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadTextBox lbEntFinancia;
		public Telerik.WinControls.UI.RadTextBox lbNAsegurado;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;

		//private FarPoint.Win.Spread.SheetView sprDocumentos_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprProductos_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprPacientes_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprEntidades_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprEpisodios_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView Sprcambios_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbMotAlta = new Telerik.WinControls.UI.RadDropDownList();
            this.Label16 = new Telerik.WinControls.UI.RadLabel();
            this.Label15 = new Telerik.WinControls.UI.RadLabel();
            this.Label14 = new Telerik.WinControls.UI.RadLabel();
            this.Label13 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.lbEntFinancia = new Telerik.WinControls.UI.RadTextBox();
            this.lbNAsegurado = new Telerik.WinControls.UI.RadTextBox();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMotAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbEntFinancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.cbbMotAlta);
            this.Frame1.Controls.Add(this.Label16);
            this.Frame1.Controls.Add(this.Label15);
            this.Frame1.Controls.Add(this.Label14);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.lbPaciente);
            this.Frame1.Controls.Add(this.lbHistoria);
            this.Frame1.Controls.Add(this.lbEntFinancia);
            this.Frame1.Controls.Add(this.lbNAsegurado);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(4, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(505, 111);
            this.Frame1.TabIndex = 2;
            // 
            // cbbMotAlta
            // 
            this.cbbMotAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMotAlta.Location = new System.Drawing.Point(165, 73);
            this.cbbMotAlta.Name = "cbbMotAlta";
            this.cbbMotAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbMotAlta.Size = new System.Drawing.Size(328, 20);
            this.cbbMotAlta.TabIndex = 3;
            this.cbbMotAlta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbMotAlta_KeyPress);
            this.cbbMotAlta.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbMotAlta_SelectionChangeCommitted);
            this.cbbMotAlta.Leave += new System.EventHandler(this.cbbMotAlta_Leave);
            // 
            // Label16
            // 
            this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label16.Location = new System.Drawing.Point(320, 16);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(81, 18);
            this.Label16.TabIndex = 12;
            this.Label16.Text = "Historia cl�nica:";
            // 
            // Label15
            // 
            this.Label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label15.Location = new System.Drawing.Point(288, 46);
            this.Label15.Name = "Label15";
            this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label15.Size = new System.Drawing.Size(78, 18);
            this.Label15.TabIndex = 11;
            this.Label15.Text = "N� asegurado:";
            // 
            // Label14
            // 
            this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label14.Location = new System.Drawing.Point(5, 16);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(51, 18);
            this.Label14.TabIndex = 10;
            this.Label14.Text = "Paciente:";
            // 
            // Label13
            // 
            this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label13.Location = new System.Drawing.Point(5, 46);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(94, 18);
            this.Label13.TabIndex = 9;
            this.Label13.Text = "Ent. Financiadora:";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(5, 75);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(154, 18);
            this.Label12.TabIndex = 8;
            this.Label12.Text = "Motivos de alta en Urgencias:";
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(64, 16);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.Enabled = false;
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(249, 20);
            this.lbPaciente.TabIndex = 7;
            // 
            // lbHistoria
            // 
            this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHistoria.Location = new System.Drawing.Point(400, 16);
            this.lbHistoria.Name = "lbHistoria";
            this.lbHistoria.Enabled = false;
            this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHistoria.Size = new System.Drawing.Size(97, 20);
            this.lbHistoria.TabIndex = 6;
            // 
            // lbEntFinancia
            // 
            this.lbEntFinancia.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbEntFinancia.Location = new System.Drawing.Point(104, 45);
            this.lbEntFinancia.Name = "lbEntFinancia";
            this.lbEntFinancia.Enabled = false;
            this.lbEntFinancia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbEntFinancia.Size = new System.Drawing.Size(177, 20);
            this.lbEntFinancia.TabIndex = 5;
            // 
            // lbNAsegurado
            // 
            this.lbNAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNAsegurado.Location = new System.Drawing.Point(368, 45);
            this.lbNAsegurado.Name = "lbNAsegurado";
            this.lbNAsegurado.Enabled = false;
            this.lbNAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNAsegurado.Size = new System.Drawing.Size(129, 20);
            this.lbNAsegurado.TabIndex = 4;
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(340, 121);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 1;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(428, 120);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 29);
            this.cbCancelar.TabIndex = 0;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // Anulacion_ingreso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(514, 153);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCancelar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Anulacion_ingreso";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Anulaci�n del Ingreso -AGI130F1";
            this.Closed += new System.EventHandler(this.Anulacion_ingreso_Closed);
            this.Load += new System.EventHandler(this.Anulacion_ingreso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMotAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbEntFinancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
		}
		#endregion
	}
}