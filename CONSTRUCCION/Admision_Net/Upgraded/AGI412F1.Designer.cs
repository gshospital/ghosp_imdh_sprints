using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
    partial class ingresos_por_entidades
    {

        #region "Upgrade Support "
        private static ingresos_por_entidades m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static ingresos_por_entidades DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new ingresos_por_entidades();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "RbAltas", "RbIngreso", "Frame5", "RbApellidos", "RbNingreso", "Frame4", "List1Servicios", "List2Servicios", "CbInsSerTodo", "CbInsSerUno", "CbDelSerTodo", "CbDelSerUno", "Frame6", "ChBHosGeneral", "ChBHosDia", "FrmHospital", "Dtfechaini", "Dtfechafin", "Label2", "Label1", "Frame2", "CbDelUno", "CbDelTodo", "CbInsUno", "CbInsTodo", "LsbLista2", "LsbLista1", "RbTodos", "RbOtros", "RbPrivado", "RbSs", "Frame3", "Frame1", "CbPantalla", "CbCerrar", "CbImprimir", "listBoxHelper1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadCheckBox RbAltas;
        public Telerik.WinControls.UI.RadCheckBox RbIngreso;
        public Telerik.WinControls.UI.RadGroupBox Frame5;
        public Telerik.WinControls.UI.RadRadioButton RbApellidos;
        public Telerik.WinControls.UI.RadRadioButton RbNingreso;
        public Telerik.WinControls.UI.RadGroupBox Frame4;        
        public Telerik.WinControls.UI.RadListControl List1Servicios;
        public Telerik.WinControls.UI.RadListControl List2Servicios;
        public Telerik.WinControls.UI.RadButton CbInsSerTodo;
        public Telerik.WinControls.UI.RadButton CbInsSerUno;
        public Telerik.WinControls.UI.RadButton CbDelSerTodo;
        public Telerik.WinControls.UI.RadButton CbDelSerUno;
        public Telerik.WinControls.UI.RadGroupBox Frame6;
        public Telerik.WinControls.UI.RadCheckBox ChBHosGeneral;
        public Telerik.WinControls.UI.RadCheckBox ChBHosDia;
        public Telerik.WinControls.UI.RadGroupBox FrmHospital;
        public Telerik.WinControls.UI.RadDateTimePicker Dtfechaini;
        public Telerik.WinControls.UI.RadDateTimePicker Dtfechafin;
        public Telerik.WinControls.UI.RadLabel Label2;
        public Telerik.WinControls.UI.RadLabel Label1;
        public Telerik.WinControls.UI.RadGroupBox Frame2;
        public Telerik.WinControls.UI.RadButton CbDelUno;
        public Telerik.WinControls.UI.RadButton CbDelTodo;
        public Telerik.WinControls.UI.RadButton CbInsUno;
        public Telerik.WinControls.UI.RadButton CbInsTodo;
        public Telerik.WinControls.UI.RadListControl LsbLista2;
        public Telerik.WinControls.UI.RadListControl LsbLista1;
        public Telerik.WinControls.UI.RadRadioButton RbTodos;
        public Telerik.WinControls.UI.RadRadioButton RbOtros;
        public Telerik.WinControls.UI.RadRadioButton RbPrivado;
        public Telerik.WinControls.UI.RadRadioButton RbSs;
        public Telerik.WinControls.UI.RadGroupBox Frame3;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
        public Telerik.WinControls.UI.RadButton CbPantalla;
        public Telerik.WinControls.UI.RadButton CbCerrar;
        public Telerik.WinControls.UI.RadButton CbImprimir;
        private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this.RbAltas = new Telerik.WinControls.UI.RadCheckBox();
            this.RbIngreso = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.RbApellidos = new Telerik.WinControls.UI.RadRadioButton();
            this.RbNingreso = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame6 = new Telerik.WinControls.UI.RadGroupBox();
            this.List1Servicios = new Telerik.WinControls.UI.RadListControl();
            this.List2Servicios = new Telerik.WinControls.UI.RadListControl();
            this.CbInsSerTodo = new Telerik.WinControls.UI.RadButton();
            this.CbInsSerUno = new Telerik.WinControls.UI.RadButton();
            this.CbDelSerTodo = new Telerik.WinControls.UI.RadButton();
            this.CbDelSerUno = new Telerik.WinControls.UI.RadButton();
            this.FrmHospital = new Telerik.WinControls.UI.RadGroupBox();
            this.ChBHosGeneral = new Telerik.WinControls.UI.RadCheckBox();
            this.ChBHosDia = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.Dtfechaini = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Dtfechafin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbDelUno = new Telerik.WinControls.UI.RadButton();
            this.CbDelTodo = new Telerik.WinControls.UI.RadButton();
            this.CbInsUno = new Telerik.WinControls.UI.RadButton();
            this.CbInsTodo = new Telerik.WinControls.UI.RadButton();
            this.LsbLista2 = new Telerik.WinControls.UI.RadListControl();
            this.LsbLista1 = new Telerik.WinControls.UI.RadListControl();
            this.RbTodos = new Telerik.WinControls.UI.RadRadioButton();
            this.RbOtros = new Telerik.WinControls.UI.RadRadioButton();
            this.RbPrivado = new Telerik.WinControls.UI.RadRadioButton();
            this.RbSs = new Telerik.WinControls.UI.RadRadioButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbAltas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbApellidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbNingreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.List1Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.List2Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsSerTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsSerUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelSerTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelSerUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).BeginInit();
            this.FrmHospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dtfechaini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dtfechafin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbTodos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbOtros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbPrivado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbSs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame5
            // 
            this.Frame5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame5.Controls.Add(this.RbAltas);
            this.Frame5.Controls.Add(this.RbIngreso);
            this.Frame5.HeaderText = "Seleccione tipo de movimiento";
            this.Frame5.Location = new System.Drawing.Point(363, 2);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(198, 39);
            this.Frame5.TabIndex = 25;
            this.Frame5.TabStop = false;
            this.Frame5.Text = "Seleccione tipo de movimiento";
            // 
            // RbAltas
            // 
            this.RbAltas.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbAltas.Location = new System.Drawing.Point(118, 16);
            this.RbAltas.Name = "RbAltas";
            this.RbAltas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbAltas.Size = new System.Drawing.Size(45, 18);
            this.RbAltas.TabIndex = 3;
            this.RbAltas.Text = "Altas";
            this.RbAltas.CheckStateChanged += new System.EventHandler(this.RbAltas_CheckStateChanged);
            // 
            // RbIngreso
            // 
            this.RbIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbIngreso.Location = new System.Drawing.Point(29, 15);
            this.RbIngreso.Name = "RbIngreso";
            this.RbIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbIngreso.Size = new System.Drawing.Size(62, 18);
            this.RbIngreso.TabIndex = 2;
            this.RbIngreso.Text = "Ingresos";
            this.RbIngreso.CheckStateChanged += new System.EventHandler(this.RbIngreso_CheckStateChanged);
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.RbApellidos);
            this.Frame4.Controls.Add(this.RbNingreso);
            this.Frame4.HeaderText = "Seleccione ordenaci�n del listado";
            this.Frame4.Location = new System.Drawing.Point(6, 2);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(351, 39);
            this.Frame4.TabIndex = 24;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "Seleccione ordenaci�n del listado";
            // 
            // RbApellidos
            // 
            this.RbApellidos.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbApellidos.Location = new System.Drawing.Point(203, 16);
            this.RbApellidos.Name = "RbApellidos";
            this.RbApellidos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbApellidos.Size = new System.Drawing.Size(111, 18);
            this.RbApellidos.TabIndex = 1;
            this.RbApellidos.Text = "Apellidos, nombre";
            this.RbApellidos.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbApellidos_CheckedChanged);
            // 
            // RbNingreso
            // 
            this.RbNingreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbNingreso.Location = new System.Drawing.Point(38, 16);
            this.RbNingreso.Name = "RbNingreso";
            this.RbNingreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbNingreso.Size = new System.Drawing.Size(117, 18);
            this.RbNingreso.TabIndex = 0;
            this.RbNingreso.Text = "N�mero de ingreso";
            this.RbNingreso.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbNingreso_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Frame6);
            this.Frame1.Controls.Add(this.FrmHospital);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(6, 40);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(555, 414);
            this.Frame1.TabIndex = 19;
            this.Frame1.TabStop = false;
            // 
            // Frame6
            // 
            this.Frame6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame6.Controls.Add(this.List1Servicios);
            this.Frame6.Controls.Add(this.List2Servicios);
            this.Frame6.Controls.Add(this.CbInsSerTodo);
            this.Frame6.Controls.Add(this.CbInsSerUno);
            this.Frame6.Controls.Add(this.CbDelSerTodo);
            this.Frame6.Controls.Add(this.CbDelSerUno);
            this.Frame6.HeaderText = "Seleccione los servicios";
            this.Frame6.Location = new System.Drawing.Point(8, 268);
            this.Frame6.Name = "Frame6";
            this.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame6.Size = new System.Drawing.Size(539, 137);
            this.Frame6.TabIndex = 29;
            this.Frame6.TabStop = false;
            this.Frame6.Text = "Seleccione los servicios";
            // 
            // List1Servicios
            // 
            this.List1Servicios.Cursor = System.Windows.Forms.Cursors.Default;
            this.List1Servicios.Location = new System.Drawing.Point(8, 20);
            this.List1Servicios.Name = "List1Servicios";
            this.List1Servicios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.List1Servicios.Size = new System.Drawing.Size(224, 108);
            this.List1Servicios.TabIndex = 35;
            this.List1Servicios.DoubleClick += new System.EventHandler(this.List1Servicios_DoubleClick);
            this.List1Servicios.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;

            // 
            // List2Servicios
            // 
            this.List2Servicios.Cursor = System.Windows.Forms.Cursors.Default;
            this.List2Servicios.Location = new System.Drawing.Point(298, 20);
            this.List2Servicios.Name = "List2Servicios";
            this.List2Servicios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.List2Servicios.Size = new System.Drawing.Size(232, 108);
            this.List2Servicios.TabIndex = 34;
            this.List2Servicios.DoubleClick += new System.EventHandler(this.List2Servicios_DoubleClick);
            this.List2Servicios.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            // 
            // CbInsSerTodo
            // 
            this.CbInsSerTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsSerTodo.Location = new System.Drawing.Point(249, 20);
            this.CbInsSerTodo.Name = "CbInsSerTodo";
            this.CbInsSerTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsSerTodo.Size = new System.Drawing.Size(33, 20);
            this.CbInsSerTodo.TabIndex = 33;
            this.CbInsSerTodo.Text = ">>";
            this.CbInsSerTodo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbInsSerTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbInsSerTodo.Click += new System.EventHandler(this.CbInsSerTodo_Click);
            // 
            // CbInsSerUno
            // 
            this.CbInsSerUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsSerUno.Location = new System.Drawing.Point(249, 48);
            this.CbInsSerUno.Name = "CbInsSerUno";
            this.CbInsSerUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsSerUno.Size = new System.Drawing.Size(33, 20);
            this.CbInsSerUno.TabIndex = 32;
            this.CbInsSerUno.Text = ">";
            this.CbInsSerUno.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbInsSerUno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbInsSerUno.Click += new System.EventHandler(this.CbInsSerUno_Click);
            // 
            // CbDelSerTodo
            // 
            this.CbDelSerTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelSerTodo.Location = new System.Drawing.Point(249, 76);
            this.CbDelSerTodo.Name = "CbDelSerTodo";
            this.CbDelSerTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelSerTodo.Size = new System.Drawing.Size(33, 20);
            this.CbDelSerTodo.TabIndex = 31;
            this.CbDelSerTodo.Text = "<<";
            this.CbDelSerTodo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbDelSerTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbDelSerTodo.Click += new System.EventHandler(this.CbDelSerTodo_Click);
            // 
            // CbDelSerUno
            // 
            this.CbDelSerUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelSerUno.Location = new System.Drawing.Point(249, 104);
            this.CbDelSerUno.Name = "CbDelSerUno";
            this.CbDelSerUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelSerUno.Size = new System.Drawing.Size(33, 20);
            this.CbDelSerUno.TabIndex = 30;
            this.CbDelSerUno.Text = "<";
            this.CbDelSerUno.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbDelSerUno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbDelSerUno.Click += new System.EventHandler(this.CbDelSerUno_Click);
            // 
            // FrmHospital
            // 
            this.FrmHospital.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmHospital.Controls.Add(this.ChBHosGeneral);
            this.FrmHospital.Controls.Add(this.ChBHosDia);
            this.FrmHospital.HeaderText = "Tipo de Ingreso";
            this.FrmHospital.Location = new System.Drawing.Point(8, 52);
            this.FrmHospital.Name = "FrmHospital";
            this.FrmHospital.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmHospital.Size = new System.Drawing.Size(539, 41);
            this.FrmHospital.TabIndex = 26;
            this.FrmHospital.TabStop = false;
            this.FrmHospital.Text = "Tipo de Ingreso";
            // 
            // ChBHosGeneral
            // 
            this.ChBHosGeneral.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosGeneral.Location = new System.Drawing.Point(48, 16);
            this.ChBHosGeneral.Name = "ChBHosGeneral";
            this.ChBHosGeneral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosGeneral.Size = new System.Drawing.Size(138, 18);
            this.ChBHosGeneral.TabIndex = 28;
            this.ChBHosGeneral.Text = "Hospitalizaci�n General";
            this.ChBHosGeneral.CheckStateChanged += new System.EventHandler(this.ChBHosGeneral_CheckStateChanged);
            // 
            // ChBHosDia
            // 
            this.ChBHosDia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChBHosDia.Location = new System.Drawing.Point(280, 16);
            this.ChBHosDia.Name = "ChBHosDia";
            this.ChBHosDia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosDia.Size = new System.Drawing.Size(97, 18);
            this.ChBHosDia.TabIndex = 27;
            this.ChBHosDia.Text = "Hospital de D�a";
            this.ChBHosDia.CheckStateChanged += new System.EventHandler(this.ChBHosDia_CheckStateChanged);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.Dtfechaini);
            this.Frame2.Controls.Add(this.Dtfechafin);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.HeaderText = "Seleccione intervalo de fechas del movimiento";
            this.Frame2.Location = new System.Drawing.Point(8, 9);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(539, 42);
            this.Frame2.TabIndex = 21;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "Seleccione intervalo de fechas del movimiento";
            // 
            // Dtfechaini
            // 
            this.Dtfechaini.CustomFormat = "dd/MM/yyyy";
            this.Dtfechaini.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtfechaini.Location = new System.Drawing.Point(125, 16);
            this.Dtfechaini.Name = "Dtfechaini";
            this.Dtfechaini.Size = new System.Drawing.Size(105, 20);
            this.Dtfechaini.TabIndex = 4;
            this.Dtfechaini.TabStop = false;          
            // 
            // Dtfechafin
            // 
            this.Dtfechafin.CustomFormat = "dd/MM/yyyy";
            this.Dtfechafin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtfechafin.Location = new System.Drawing.Point(378, 16);
            this.Dtfechafin.Name = "Dtfechafin";
            this.Dtfechafin.Size = new System.Drawing.Size(105, 20);
            this.Dtfechafin.TabIndex = 5;
            this.Dtfechafin.TabStop = false;           
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(315, 19);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(53, 18);
            this.Label2.TabIndex = 23;
            this.Label2.Text = "Fecha fin:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(45, 19);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(67, 18);
            this.Label1.TabIndex = 22;
            this.Label1.Text = "Fecha inicio:";

            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.CbDelUno);
            this.Frame3.Controls.Add(this.CbDelTodo);
            this.Frame3.Controls.Add(this.CbInsUno);
            this.Frame3.Controls.Add(this.CbInsTodo);
            this.Frame3.Controls.Add(this.LsbLista2);
            this.Frame3.Controls.Add(this.LsbLista1);
            this.Frame3.Controls.Add(this.RbTodos);
            this.Frame3.Controls.Add(this.RbOtros);
            this.Frame3.Controls.Add(this.RbPrivado);
            this.Frame3.Controls.Add(this.RbSs);
            this.Frame3.HeaderText = "Seleccione entidad financiadora";
            this.Frame3.Location = new System.Drawing.Point(8, 94);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(539, 171);
            this.Frame3.TabIndex = 20;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Seleccione entidad financiadora";
            // 
            // CbDelUno
            // 
            this.CbDelUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelUno.Location = new System.Drawing.Point(249, 136);
            this.CbDelUno.Name = "CbDelUno";
            this.CbDelUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelUno.Size = new System.Drawing.Size(33, 20);
            this.CbDelUno.TabIndex = 14;
            this.CbDelUno.Text = "<";
            this.CbDelUno.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbDelUno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbDelUno.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // CbDelTodo
            // 
            this.CbDelTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelTodo.Location = new System.Drawing.Point(249, 104);
            this.CbDelTodo.Name = "CbDelTodo";
            this.CbDelTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelTodo.Size = new System.Drawing.Size(33, 20);
            this.CbDelTodo.TabIndex = 13;
            this.CbDelTodo.Text = "<<";
            this.CbDelTodo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbDelTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbDelTodo.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // CbInsUno
            // 
            this.CbInsUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsUno.Location = new System.Drawing.Point(249, 72);
            this.CbInsUno.Name = "CbInsUno";
            this.CbInsUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsUno.Size = new System.Drawing.Size(33, 20);
            this.CbInsUno.TabIndex = 12;
            this.CbInsUno.Text = ">";
            this.CbInsUno.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbInsUno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbInsUno.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // CbInsTodo
            // 
            this.CbInsTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsTodo.Location = new System.Drawing.Point(249, 40);
            this.CbInsTodo.Name = "CbInsTodo";
            this.CbInsTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsTodo.Size = new System.Drawing.Size(33, 20);
            this.CbInsTodo.TabIndex = 11;
            this.CbInsTodo.Text = ">>";
            this.CbInsTodo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbInsTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbInsTodo.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // LsbLista2
            // 
            this.LsbLista2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista2.Location = new System.Drawing.Point(296, 40);
            this.LsbLista2.Name = "LsbLista2";
            this.LsbLista2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista2.Size = new System.Drawing.Size(232, 121);
            this.LsbLista2.TabIndex = 15;
            this.LsbLista2.DoubleClick += new System.EventHandler(this.LsbLista2_DoubleClick);
            // 
            // LsbLista1
            // 
            this.LsbLista1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista1.Location = new System.Drawing.Point(8, 40);
            this.LsbLista1.Name = "LsbLista1";
            this.LsbLista1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista1.Size = new System.Drawing.Size(224, 121);
            this.LsbLista1.TabIndex = 10;
            this.LsbLista1.DoubleClick += new System.EventHandler(this.LsbLista1_DoubleClick);
            this.LsbLista1.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            // 
            // RbTodos
            // 
            this.RbTodos.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbTodos.Location = new System.Drawing.Point(417, 16);
            this.RbTodos.Name = "RbTodos";
            this.RbTodos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbTodos.Size = new System.Drawing.Size(51, 18);
            this.RbTodos.TabIndex = 9;
            this.RbTodos.Text = "Todos";
            this.RbTodos.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbTodos_CheckedChanged);
            this.RbTodos.Leave += new System.EventHandler(this.RbTodos_Leave);
            // 
            // RbOtros
            // 
            this.RbOtros.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbOtros.Location = new System.Drawing.Point(283, 16);
            this.RbOtros.Name = "RbOtros";
            this.RbOtros.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbOtros.Size = new System.Drawing.Size(77, 18);
            this.RbOtros.TabIndex = 8;
            this.RbOtros.Text = "Sociedades";
            this.RbOtros.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbOtros_CheckedChanged);
            // 
            // RbPrivado
            // 
            this.RbPrivado.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbPrivado.Location = new System.Drawing.Point(160, 16);
            this.RbPrivado.Name = "RbPrivado";
            this.RbPrivado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbPrivado.Size = new System.Drawing.Size(58, 18);
            this.RbPrivado.TabIndex = 7;
            this.RbPrivado.Text = "Privado";
            this.RbPrivado.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbPrivado_CheckedChanged);
            // 
            // RbSs
            // 
            this.RbSs.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbSs.Location = new System.Drawing.Point(45, 16);
            this.RbSs.Name = "RbSs";
            this.RbSs.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbSs.Size = new System.Drawing.Size(61, 18);
            this.RbSs.TabIndex = 6;
            this.RbSs.Text = "S. Social";
            this.RbSs.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RbSs_CheckedChanged);
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.Location = new System.Drawing.Point(386, 459);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(81, 29);
            this.CbPantalla.TabIndex = 17;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(481, 459);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 18;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.Enabled = false;
            this.CbImprimir.Location = new System.Drawing.Point(293, 459);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 16;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // ingresos_por_entidades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(568, 494);
            this.Controls.Add(this.Frame5);
            this.Controls.Add(this.Frame4);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.CbPantalla);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbImprimir);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(39, 62);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ingresos_por_entidades";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Closed += new System.EventHandler(this.ingresos_por_entidades_Closed);
            this.Load += new System.EventHandler(this.ingresos_por_entidades_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbAltas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbApellidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbNingreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.List1Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.List2Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsSerTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsSerUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelSerTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelSerUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).EndInit();
            this.FrmHospital.ResumeLayout(false);
            this.FrmHospital.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChBHosDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dtfechaini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dtfechafin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbTodos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbOtros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbPrivado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbSs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }
        void ReLoadForm(bool addEvents)
        {
            //This form is an MDI child.
            //This code simulates the VB6 
            // functionality of automatically
            // loading and showing an MDI
            // child's parent.
            this.MdiParent = ADMISION.menu_admision.DefInstance;
            ADMISION.menu_admision.DefInstance.Show();
            //The MDI form in the VB6 project had its
            //AutoShowChildren property set to True
            //To simulate the VB6 behavior, we need to
            //automatically Show the form whenever it
            //is loaded.  If you do not want this behavior
            //then delete the following line of code
            //UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
            //this.Show();
        }
        #endregion
    }
}