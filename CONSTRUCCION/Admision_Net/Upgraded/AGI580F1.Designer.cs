using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Listado_ambulancias
	{

		#region "Upgrade Support "
		private static Listado_ambulancias m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Listado_ambulancias DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Listado_ambulancias();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbImprimir", "cbCerrar", "cbPantalla", "sdcFechHasta", "sdcFechDesde", "LbFechHasta", "LbFechDesde", "Frame2", "Label1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechHasta;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechDesde;
		public Telerik.WinControls.UI.RadLabel LbFechHasta;
		public Telerik.WinControls.UI.RadLabel LbFechDesde;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadLabel Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFechHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcFechDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.LbFechHasta = new Telerik.WinControls.UI.RadLabel();
            this.LbFechDesde = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbImprimir.Location = new System.Drawing.Point(208, 72);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(81, 29);
            this.cbImprimir.TabIndex = 8;
            this.cbImprimir.Text = "&Impresora";
            this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCerrar.Location = new System.Drawing.Point(396, 72);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 29);
            this.cbCerrar.TabIndex = 7;
            this.cbCerrar.Text = "&Cerrar";
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbPantalla.Location = new System.Drawing.Point(303, 72);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(81, 29);
            this.cbPantalla.TabIndex = 6;
            this.cbPantalla.Text = "&Pantalla";
            this.cbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.sdcFechHasta);
            this.Frame2.Controls.Add(this.sdcFechDesde);
            this.Frame2.Controls.Add(this.LbFechHasta);
            this.Frame2.Controls.Add(this.LbFechDesde);
            this.Frame2.HeaderText = "Intérvalo de fechas";
            this.Frame2.Location = new System.Drawing.Point(8, 8);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(473, 49);
            this.Frame2.TabIndex = 0;
            this.Frame2.Text = "Intérvalo de fechas";
            // 
            // sdcFechHasta
            // 
            this.sdcFechHasta.CustomFormat = "dd/MM/yyyy";
            this.sdcFechHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechHasta.Location = new System.Drawing.Point(328, 19);
            this.sdcFechHasta.Name = "sdcFechHasta";
            this.sdcFechHasta.Size = new System.Drawing.Size(105, 20);
            this.sdcFechHasta.TabIndex = 1;
            this.sdcFechHasta.TabStop = false;
            this.sdcFechHasta.Text = "25/04/2016";
            this.sdcFechHasta.Value = new System.DateTime(2016, 4, 25, 9, 31, 39, 116);
            // 
            // sdcFechDesde
            // 
            this.sdcFechDesde.CustomFormat = "dd/MM/yyyy";
            this.sdcFechDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechDesde.Location = new System.Drawing.Point(104, 19);
            this.sdcFechDesde.Name = "sdcFechDesde";
            this.sdcFechDesde.Size = new System.Drawing.Size(105, 20);
            this.sdcFechDesde.TabIndex = 2;
            this.sdcFechDesde.TabStop = false;
            this.sdcFechDesde.Text = "22/04/2016";
            this.sdcFechDesde.Value = new System.DateTime(2016, 4, 22, 0, 0, 0, 0);
            // 
            // LbFechHasta
            // 
            this.LbFechHasta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechHasta.Location = new System.Drawing.Point(248, 19);
            this.LbFechHasta.Name = "LbFechHasta";
            this.LbFechHasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechHasta.Size = new System.Drawing.Size(69, 18);
            this.LbFechHasta.TabIndex = 4;
            this.LbFechHasta.Text = "Fecha Hasta:";
            // 
            // LbFechDesde
            // 
            this.LbFechDesde.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechDesde.Location = new System.Drawing.Point(24, 19);
            this.LbFechDesde.Name = "LbFechDesde";
            this.LbFechDesde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechDesde.Size = new System.Drawing.Size(71, 18);
            this.LbFechDesde.TabIndex = 3;
            this.LbFechDesde.Text = "Fecha desde:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(16, 16);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(65, 18);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Feha desde:";
            // 
            // Listado_ambulancias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(488, 106);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Listado_ambulancias";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Listado de ambulancias - AGI580F1 ";
            this.Closed += new System.EventHandler(this.Listado_ambulancias_Closed);
            this.Load += new System.EventHandler(this.Listado_ambulancias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			//this.Show();
		}
		#endregion
	}
}