using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Microsoft.CSharp;

namespace ADMISION
{
	public partial class ConversionHDaH
		: RadForm,inf_conversionHD
	{
		int mGanoadme = 0;
		int mGnumadme = 0;
		string mGidenpac = String.Empty; // String con c�digo del paciente

		bool fCama = false;
		bool fCambiaCama = false;
		int inPlanta = 0;
		int inHabitac = 0;
		string stnCama = String.Empty;
		string StRegaloj = String.Empty;
		string stTipoIng = String.Empty;
		string stSexo = String.Empty;
		string stProc = String.Empty;
		string FILPROVI = String.Empty;
		bool Ferror_histo = false;
		public ConversionHDaH()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}

		public void proRecibeEpisodio(int ParaAno, int ParaNum)
		{
			mGanoadme = ParaAno;
			mGnumadme = ParaNum;
		}

        public int getQueryTimeout(SqlConnection instance)
        {
            int queryTimeout = 30;
            string tstTimeout = "select nnumeri1 from sconsglo where gconsglo='TTIMEOUT'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstTimeout, instance);
            DataSet tRrTimeout = new DataSet();
            tempAdapter.Fill(tRrTimeout);
            if (tRrTimeout.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToDouble(tRrTimeout.Tables[0].Rows[0]["nnumeri1"]) >= 30)
                {                    
                    queryTimeout = Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]);
                }
                else
                {                    
                    queryTimeout = 30;
                }
            }
            else
            {                
                queryTimeout = 30;
            }
            tRrTimeout.Close();
            return queryTimeout;
        }

        private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string sqlAux = String.Empty;
			DataSet RRAux = null;
			Numeracion.SIGUIENTE oSiguiente = null;
			string stTipoCama = String.Empty;
			string stUnienfe = String.Empty;
			bool bTransaccionIniciada = false;
			int gNewNumRegi = 0;
			int gNewAnoRegi = 0;
			int iTimeOut = 0;

			SqlCommand rdoQProceso = null;
            try
            {                
                iTimeOut = getQueryTimeout(Conexion_Base_datos.RcAdmision);

                bTransaccionIniciada = false;
                //Conversion de Episodios de Hospital de Dia a Episodio de Hospitalizacion
                //(hay que validar que no este facturado)

                //0. Validacion de que el paciente debe estar ingresado
                sqlAux = "select * from aepisadm where ganoadme=" + mGanoadme.ToString() + " and gnumadme=" + mGnumadme.ToString() + " and faltplan is not null";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
                RRAux = new DataSet();
                tempAdapter.Fill(RRAux);
                if (RRAux.Tables[0].Rows.Count > 0)
                {                    
                    short tempRefParam = 1420;
                    string[] tempRefParam2 = new string[] { "El Paciente", "de Alta" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    return;
                }
                
                RRAux.Close();

                //1. Validacion de Facturacion
                sqlAux = "select * from dmovfact where " + " ganoregi=" + mGanoadme.ToString() + " and gnumregi=" + mGnumadme.ToString() + " and itiposer = 'H' and " + " gcodeven='ES' and ffactura is not null";

                if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
                {
                    string tempRefParam3 = "DMOVFACT";
                    sqlAux = sqlAux + " UNION ALL " + Serrores.proReplace(ref sqlAux, tempRefParam3, "DMOVFACH");
                }

                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
                RRAux = new DataSet();
                tempAdapter_2.Fill(RRAux);
                if (RRAux.Tables[0].Rows.Count > 0)
                {                    
                    short tempRefParam4 = 1420;
                    string[] tempRefParam5 = new string[] { "El Episodio", "Facturado" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, Conexion_Base_datos.RcAdmision, tempRefParam5));
                    return;
                }
                
                RRAux.Close();

                //2.Validacion de la Cama
                sqlAux = "select * from DCAMASBO, DTIPOCAM " + "WHERE GPLANTAS = " + inPlanta.ToString() + " AND GHABITAC = " + inHabitac.ToString() + " AND GCAMASBO = '" + stnCama + "'" + " AND IESTCAMA = 'L'" + " AND DCAMASBO.gtipocam = DTIPOCAM.gtipocam " + " AND DTIPOCAM.isillon <>'S'";

                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
                RRAux = new DataSet();
                tempAdapter_3.Fill(RRAux);
                if (RRAux.Tables[0].Rows.Count == 0)
                {
                    //la cama no existe o no esta libre o es una plaza de dia                    
                    short tempRefParam6 = 1320;
                    string[] tempRefParam7 = new string[] { "ocupada , inexistente o Plaza de D�a" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, Conexion_Base_datos.RcAdmision, tempRefParam7));
                    return;
                }
                else
                {
                    //la cama existe
                    if (!fnComprobar_cama((rbIndividual.IsChecked) ? "I" : "C"))
                    {
                        return;
                    }
                }                
                RRAux.Close();

                this.Cursor = Cursors.WaitCursor;
                Conexion_Base_datos.RcAdmision.BeginTransScope();
                {                    
                    bTransaccionIniciada = true;

                    lbInfo.Text = "PROCESANDO....";
                    lbInfo.Visible = true;
                    this.Refresh();
                    Application.DoEvents();
                    
                    oSiguiente = new Numeracion.SIGUIENTE(); //New Numeracion.SIGUIENTE                    
                    gNewNumRegi = Convert.ToInt32(oSiguiente.SIG_NUMERO("NROEADM", Conexion_Base_datos.RcAdmision));                    
                    gNewAnoRegi = DateTime.Today.Year;
                    oSiguiente = null;

                    //4. Ejecutamos el procedimiento almacenado que realiza en si la conversion.                    

                    rdoQProceso = new SqlCommand();

                    rdoQProceso = Conexion_Base_datos.RcAdmision.CreateQuery("Llamada", "CONVERSION_HD_TO_H");
                    rdoQProceso.CommandType = CommandType.StoredProcedure;
                    rdoQProceso.CommandTimeout = iTimeOut;
                    
                    rdoQProceso.Parameters.Add("@ganoregiHD", SqlDbType.SmallInt).Size = 20;
                    rdoQProceso.Parameters.Add("@gnumregiHD", SqlDbType.Int).Size = 20;
                    rdoQProceso.Parameters.Add("@ganoregiH", SqlDbType.SmallInt).Size = 20;
                    rdoQProceso.Parameters.Add("@gnumregiH", SqlDbType.Int).Size = 20;
                    rdoQProceso.Parameters.Add("@derror", SqlDbType.VarChar).Size = 100;

                    rdoQProceso.Parameters[0].Direction = ParameterDirection.Input;
                    rdoQProceso.Parameters[1].Direction = ParameterDirection.Input;
                    rdoQProceso.Parameters[2].Direction = ParameterDirection.Input;
                    rdoQProceso.Parameters[3].Direction = ParameterDirection.Input;
                    rdoQProceso.Parameters[4].Direction = ParameterDirection.Output;
                    rdoQProceso.Parameters[0].Value = mGanoadme;
                    rdoQProceso.Parameters[1].Value = mGnumadme;
                    rdoQProceso.Parameters[2].Value = (int)gNewAnoRegi;
                    rdoQProceso.Parameters[3].Value = gNewNumRegi;
                    rdoQProceso.ExecuteNonQuery();
                                        
                    this.Refresh();
                    Application.DoEvents();

                    //5. Acciones especificas sobre tablas especificas
                    //a) DMOVFACT - Modificar el regimen de alojamiento y tipo de cama en las filas correspondientes al episodio antiguo de HD
                    //       (pero utilizamos las variables nuevas porque el procedimiento almacenado anterior ya se ha ejecutado)
                    sqlAux = "SELECT gtipocam, gcounien FROM DCAMASBOva " + " WHERE " + " DCAMASBOva.GPLANTAS = " + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)))).ToString() + " AND " + " DCAMASBOva.GHABITAC = " + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)))).ToString() + " AND " + " DCAMASBOva.gcamasbo ='" + tbCama.Text.Substring(4).Trim() + "'";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
                    RRAux = new DataSet();
                    tempAdapter_4.Fill(RRAux);

                    stTipoCama = Convert.ToString(RRAux.Tables[0].Rows[0]["gtipocam"]) + "";

                    stUnienfe = Convert.ToString(RRAux.Tables[0].Rows[0]["gcounien"]) + "";
                    //UPGRADE_ISSUE: (2064) RDO.rdoResultset method RRAux.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    RRAux.Close();
                    sqlAux = "UPDATE DMOVFACT SET iregaloj='" + StRegaloj + "', " + " gtipocam='" + stTipoCama + "' " +
                             " WHERE itiposer='H' AND ganoregi=" + ((int)gNewAnoRegi).ToString() + " AND gnumregi =" + gNewNumRegi.ToString() +
                             "   AND gcodeven in ('ES','AU')";
                    SqlCommand tempCommand = new SqlCommand(sqlAux, Conexion_Base_datos.RcAdmision);
                    tempCommand.ExecuteNonQuery();
                    //b) AEPISADM - Modificar el regimen de alojamiento,
                    //    tipo de ingreso (Inmediato),
                    //    tipo de proceso (M o Q dependiendo del tipo de plaza del episodio HD).
                    //   Ademas se vaciara el campo correspondiente al tipo de plaza
                    //       (pero utilizamos las variables nuevas porque el procedimiento almacenado anterior ya se ha ejecutado)
                    sqlAux = "UPDATE AEPISADM SET iregaloj='" + StRegaloj + "', " + " itipingr='" + stTipoIng + "', " +
                             " gtiproce='" + stProc + "', gtiplaza=null " +
                             " WHERE ganoadme=" + ((int)gNewAnoRegi).ToString() + " AND gnumadme =" + gNewNumRegi.ToString();
                    SqlCommand tempCommand_2 = new SqlCommand(sqlAux, Conexion_Base_datos.RcAdmision);
                    tempCommand_2.ExecuteNonQuery();
                    //c) AMOVIMIE - Modificar los datos de la plaza de dia y unidad de enfermeria por los datos de la nueva cama y su correspondiente unidad de enfermeria
                    //       (pero utilizamos las variables nuevas porque el procedimiento almacenado anterior ya se ha ejecutado)
                    sqlAux = "UPDATE AMOVIMIE SET gunenfor='" + stUnienfe + "', " +
                             " gplantor=" + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)))).ToString() + "," +
                             " ghabitor=" + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)))).ToString() + "," +
                             " gcamaori='" + tbCama.Text.Substring(4).Trim() + "'" +
                             " WHERE itiposer='H' AND ganoregi=" + ((int)gNewAnoRegi).ToString() + " AND gnumregi =" + gNewNumRegi.ToString() + " AND ffinmovi is null ";
                    SqlCommand tempCommand_3 = new SqlCommand(sqlAux, Conexion_Base_datos.RcAdmision);
                    tempCommand_3.ExecuteNonQuery();
                    sqlAux = "UPDATE AMOVIMIE SET gunenfor='" + stUnienfe + "', " +
                             " gplantor=" + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)))).ToString() + "," +
                             " ghabitor=" + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)))).ToString() + "," +
                             " gcamaori='" + tbCama.Text.Substring(4).Trim() + "'," +
                             " gunenfde='" + stUnienfe + "', " +
                             " gplantde=" + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)))).ToString() + "," +
                             " ghabitde=" + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)))).ToString() + "," +
                             " gcamades='" + tbCama.Text.Substring(4).Trim() + "'" +
                             " WHERE itiposer='H' AND ganoregi=" + ((int)gNewAnoRegi).ToString() + " AND gnumregi =" + gNewNumRegi.ToString() + " AND ffinmovi is not null ";
                    SqlCommand tempCommand_4 = new SqlCommand(sqlAux, Conexion_Base_datos.RcAdmision);
                    tempCommand_4.ExecuteNonQuery();
                    //d)Liberamos la plaza de dia
                    sqlAux = "UPDATE DCAMASBO SET gidenpac=null, itipsexo=null, iestcama='L'" +
                             " WHERE gidenpac='" + mGidenpac + "'";
                    SqlCommand tempCommand_5 = new SqlCommand(sqlAux, Conexion_Base_datos.RcAdmision);
                    tempCommand_5.ExecuteNonQuery();
                    //e)Ocupamos la nueva cama
                    sqlAux = "UPDATE DCAMASBO SET gidenpac='" + mGidenpac + "', itipsexo='" + stSexo + "', iestcama='O' " +
                             " WHERE GPLANTAS = " + inPlanta.ToString() +
                             " AND GHABITAC = " + inHabitac.ToString() +
                             " AND GCAMASBO = '" + stnCama + "'";
                    SqlCommand tempCommand_6 = new SqlCommand(sqlAux, Conexion_Base_datos.RcAdmision);
                    tempCommand_6.ExecuteNonQuery();

                    //6. Si el paceinte no tiene historia, se crea, y si tiene se pide
                    //dynamic mcHistoria = null;
                    PeticionHistoriaClinica.ClassHTC120F1 mcHistoria = null;
                    string letra = String.Empty;
                    string tstUniEnf = String.Empty;
                    int gSerulti = 0;
                    int gPerulti = 0;
                    System.DateTime fllegada = DateTime.FromOADate(0);
                    string gmotpeth = String.Empty;
                    int ndiasprest = 0;
                    int viGserarch = 0;
                    bool bSinHistoria = false;
                    Ferror_histo = false;

                    //Dependiendo de la constante global PEHISING, la peticion de historia al ingreso
                    //se hace a HMOVUNEF (unidad de enfermeria) o a Servicios (HMOVSERV)
                    letra = "E"; //Por defecto a UNIDAD DE ENFERMERIA
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter("SELECT ISNULL(VALFANU1,'E') FROM SCONSGLO WHERE GCONSGLO='PEHISING'", Conexion_Base_datos.RcAdmision);
                    RRAux = new DataSet();
                    tempAdapter_5.Fill(RRAux);
                    if (RRAux.Tables[0].Rows.Count != 0)
                    {

                        letra = Convert.ToString(RRAux.Tables[0].Rows[0][0]).Trim().ToUpper();
                    }                    
                    RRAux.Close();

                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter("SELECT gcounien FROM DCAMASBO WHERE GPLANTAS = " + inPlanta.ToString() + " AND GHABITAC = " + inHabitac.ToString() + " AND GCAMASBO = '" + stnCama + "'", Conexion_Base_datos.RcAdmision);
                    RRAux = new DataSet();
                    tempAdapter_6.Fill(RRAux);
                    if (RRAux.Tables[0].Rows.Count != 0)
                    {

                        tstUniEnf = Convert.ToString(RRAux.Tables[0].Rows[0][0]).Trim().ToUpper();
                    }
                    
                    RRAux.Close();

                    SqlDataAdapter tempAdapter_7 = new SqlDataAdapter("Select gperulti, gserulti , fllegada FROM AEPISADM where ganoadme = " + ((int)gNewAnoRegi).ToString() + " and gnumadme = " + gNewNumRegi.ToString(), Conexion_Base_datos.RcAdmision);
                    RRAux = new DataSet();
                    tempAdapter_7.Fill(RRAux);
                    if (RRAux.Tables[0].Rows.Count != 0)
                    {

                        gSerulti = Convert.ToInt32(RRAux.Tables[0].Rows[0]["gserulti"]);
                        gPerulti = Convert.ToInt32(RRAux.Tables[0].Rows[0]["gperulti"]);
                        fllegada = DateTime.Now; //RRAux("fllegada")
                    }
                    
                    RRAux.Close();

                    SqlDataAdapter tempAdapter_8 = new SqlDataAdapter("select gmotpeth,ndiasprest from dmotpeth where imothosp='S'", Conexion_Base_datos.RcAdmision);
                    RRAux = new DataSet();
                    tempAdapter_8.Fill(RRAux);
                    if (RRAux.Tables[0].Rows.Count != 0)
                    {

                        gmotpeth = Convert.ToString(RRAux.Tables[0].Rows[0]["gmotpeth"]);
                        ndiasprest = Convert.ToInt32(RRAux.Tables[0].Rows[0]["ndiasprest"]);
                    }
                    
                    RRAux.Close();

                    if (lbHistoria.Text.Trim() == "")
                    {
                        if (FILPROVI == "N")
                        {
                            //DGMORENOG_TODO_7_4 - DLL DLLHistoriaGeneral.dll - INICIO
                            /*mcHistoria = new DLLHistoria_General.classHistoria();
                            switch(letra)
                            {
                                case "E" :  
                                    //UPGRADE_TODO: (1067) Member proHistoria is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                                    mcHistoria.proHistoria(Path.GetDirectoryName(Application.ExecutablePath), "H", mGidenpac, Conexion_Base_datos.RcAdmision, this, gPerulti, gSerulti, "U", tstUniEnf, DateTime.Parse(fllegada.ToString("dd/MM/yyyy")), DateTime.Parse(fllegada.ToString("dd/MM/yyyy")), DateTime.Parse(fllegada.ToString("dd/MM/yyyy")).AddDays(ndiasprest).ToString("dd/MM/yyyy")); 
                                    break;
                                case "S" :  
                                    //UPGRADE_TODO: (1067) Member proHistoria is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
                                    mcHistoria.proHistoria(Path.GetDirectoryName(Application.ExecutablePath), "H", mGidenpac, Conexion_Base_datos.RcAdmision, this, gPerulti, gSerulti, "S", null, DateTime.Parse(fllegada.ToString("dd/MM/yyyy")), DateTime.Parse(fllegada.ToString("dd/MM/yyyy")), DateTime.Parse(fllegada.ToString("dd/MM/yyyy")).AddDays(ndiasprest).ToString("dd/MM/yyyy")); 
                                    break;
                            }
                            mcHistoria = null;*/
                            //DGMORENOG_TODO_7_4 - DLL DLLHistoriaGeneral.dll - FIN
                        }
                    }
                    else
                    {
                        sqlAux = "SELECT GSERARCH FROM HSERARCH, HDOSSIER " + " WHERE GHISTORIA = '" + lbHistoria.Text.Trim() + "' AND " + " GSERPROPIET = GSERARCH AND " + " ICENTRAL='S' and icontenido='H'";
                        SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sqlAux, Conexion_Base_datos.RcAdmision);
                        RRAux = new DataSet();
                        tempAdapter_9.Fill(RRAux);
                        if (RRAux.Tables[0].Rows.Count != 0)
                        {                            
                            viGserarch = Convert.ToInt32(RRAux.Tables[0].Rows[0]["Gserarch"]);
                        }
                        
                        RRAux.Close();
                        
                        
                        mcHistoria = new PeticionHistoriaClinica.ClassHTC120F1();                        
                        mcHistoria.ReferenciasClass(mcHistoria, this, Conexion_Base_datos.RcAdmision);

                        string StContenido = "H";
                        string VarNHistoria = lbHistoria.Text.ToString().Trim();
                        string VarIAutomatica = "S";
                        string VarCodServicioPeticionario = gSerulti.ToString();
                        string VarFechaPeticion = fllegada.ToString("dd/MM/yyyy");
                        string VarFechaDevol = fllegada.AddDays(ndiasprest).ToString("dd/MM/yyyy");
                        string VarEntidadExterna = "";
                        string VarSalaConsulta = "";
                        string VarUnidadEnfermeria = "";

                        switch (letra)
                        {
                            case "E" :                                  
                                mcHistoria.Recibir(Path.GetDirectoryName(Application.ExecutablePath), "H",ref viGserarch, ref StContenido, mGidenpac, ref VarNHistoria, ref VarIAutomatica, "U", ref VarCodServicioPeticionario, ref tstUniEnf,ref VarEntidadExterna, ref VarSalaConsulta, "",ref VarFechaPeticion , gmotpeth,gPerulti.ToString(), fllegada.ToString("dd/MM/yyyy"), ref VarFechaDevol , "");
                                break;
                            case "S" :                                                                  
                                mcHistoria.Recibir(Path.GetDirectoryName(Application.ExecutablePath), "H", ref viGserarch, ref StContenido, mGidenpac, ref VarNHistoria, ref VarIAutomatica, "S", ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, "", ref VarFechaPeticion, gmotpeth, gPerulti.ToString(), fllegada.ToString("dd/MM/yyyy"), ref VarFechaDevol, "");
                                
                                break;
                        }
                        mcHistoria = null;                        
                    }

                    SqlDataAdapter tempAdapter_10 = new SqlDataAdapter("select * from HDOSSIER where GIDENPAC ='" + mGidenpac.ToUpper() + "'", Conexion_Base_datos.RcAdmision);
                    RRAux = new DataSet();
                    tempAdapter_10.Fill(RRAux);
                    if (RRAux.Tables[0].Rows.Count == 0)
                    {
                        bSinHistoria = true;
                        Ferror_histo = true;
                    }
                    else
                    {
                        bSinHistoria = false;
                    }
                    
                    RRAux.Close();

                    //Grabamos en la tabla de hist�rico.
                    SqlCommand tempCommand_7 = new SqlCommand("INSERT INTO DCONVHDH values (" + mGanoadme.ToString() + "," + mGnumadme.ToString() + "," + ((int)gNewAnoRegi).ToString() + "," + gNewNumRegi.ToString() + ",'" + Conexion_Base_datos.VstCodUsua.Trim() + "',getdate())", Conexion_Base_datos.RcAdmision);
                    tempCommand_7.ExecuteNonQuery();

                    if (Ferror_histo)
                    {
                        Conexion_Base_datos.RcAdmision.RollbackTransScope();
                        this.Cursor = Cursors.Default;
                        lbInfo.Text = "Error en el m�dulo de historia";
                        lbInfo.BackColor = Color.FromArgb(255, 128, 128);
                        this.Refresh();
                        Application.DoEvents();
                        return;
                    }
                    else
                    {
                        Conexion_Base_datos.RcAdmision.CommitTransScope();
                        this.Cursor = Cursors.Default;
                        lbInfo.Text = "EPISODIO CONVERTIDO";
                        lbInfo.BackColor = Color.FromArgb(128, 255, 128);
                        this.Refresh();
                        Application.DoEvents();
                        this.Close();
                    }
                }                
            }
            catch (Exception ex)
            {
                if (bTransaccionIniciada)
                {
                    Conexion_Base_datos.RcAdmision.RollbackTransScope();
                }
                this.Cursor = Cursors.Default;
                lbInfo.BackColor = Color.FromArgb(255, 128, 128);
                if (Convert.ToString(rdoQProceso.Parameters[4].Value) + "" != "")
                {
                    lbInfo.Text = Convert.ToString(rdoQProceso.Parameters[4].Value);
                }
                this.Refresh();
                Application.DoEvents();
                RadMessageBox.Show(this,"Se ha producido un error Cr�tico: " + ex.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
            }
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void ConversionHDaH_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				string stSql = "SELECT " + " DPACIENT.DAPE1PAC, DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC, DPACIENT.ITIPSEXO, DPACIENT.IFILPROV," + " HDOSSIER.GHISTORIA," + " AEPISADM.gidenpac, AEPISADM.GSERULTI, DSERVICI.DNOMSERV," + " AEPISADM.GDIAGINI, AEPISADM.ODIAGINI, DDIAGNOS.DNOMBDIA," + " AEPISADM.GPERULTI, DPERSONA.DAP1PERS,DPERSONA.DAP2PERS, DPERSONA.DNOMPERS, " + " AEPISADM.FLLEGADA, AEPISADM.GTIPLAZA" + " FROM AEPISADM " + "  INNER JOIN DPACIENT ON DPACIENT.GIDENPAC = AEPISADM.GIDENPAC " + "  INNER JOIN DPERSONA ON DPERSONA.GPERSONA = AEPISADM.GPERULTI " + "  INNER JOIN DSERVICI ON DSERVICI.GSERVICI = AEPISADM.GSERULTI " + "  LEFT JOIN HDOSSIER ON AEPISADM.GIDENPAC = HDOSSIER.GIDENPAC AND HDOSSIER.icontenido='H' " + "  LEFT JOIN DDIAGNOS ON DDIAGNOS.GCODIDIA = AEPISADM.GDIAGINI AND ( AEPISADM.FLLEGADA BETWEEN FINVALDI AND FFIVALDI OR ( AEPISADM.FLLEGADA >= FINVALDI and ffivaldi IS  NULL))";
				stSql = stSql + " WHERE " + " AEPISADM.ganoadme= " + mGanoadme.ToString() + " AND gnumadme=" + mGnumadme.ToString() + " AND AEPISADM.FALTPLAN IS NULL and AEPISADM.FALtaadm is null";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
				DataSet RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{					
					FILPROVI = Convert.ToString(RrDatos.Tables[0].Rows[0]["IFILPROV"]);
					
					lbNombrePaciente.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAPE1PAC"]) + "").Trim() + " " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAPE2PAC"]) + "").Trim() + ", " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMBPAC"]) + "").Trim();
					
					mGidenpac = Convert.ToString(RrDatos.Tables[0].Rows[0]["gidenpac"]);
					
					switch(Convert.ToString(RrDatos.Tables[0].Rows[0]["ITIPSEXO"]))
					{
						case "H" : 
							RbHombre.IsChecked = true; 
							RbMujer.IsChecked = false; 
							stSexo = "H"; 
							break;
						case "M" : 
							RbHombre.IsChecked = false; 
							RbMujer.IsChecked = true; 
							stSexo = "M"; 
							break;
						default:
							RbHombre.IsChecked = false; 
							RbMujer.IsChecked = false; 
							stSexo = "I"; 
							break;
					}
					
					lbHistoria.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["ghistoria"]) + "").Trim();
					
					lbServicio.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMSERV"]) + "").Trim();
					
					if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["GDIAGINI"]))
					{						
						lbDiagnostico.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMBDIA"]) + "").Trim();
					}
					else if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ODIAGINI"]))
					{ 						
						lbDiagnostico.Text = Convert.ToString(RrDatos.Tables[0].Rows[0]["ODIAGINI"]) + "";
					}
					
					lbResponsable.Text = (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAP1PERS"]) + "").Trim() + " " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DAP2PERS"]) + "").Trim() + ", " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["DNOMPERS"]) + "").Trim();
					
					lbFechaIngreso.Text = Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm");
					
					switch(Convert.ToString(RrDatos.Tables[0].Rows[0]["gtiplaza"]))
					{
						case "Q" : 
							rbProceso[1].IsChecked = true; 
							stProc = "Q"; 
							break;
						default:
							rbProceso[0].IsChecked = true; 
							stProc = "M"; 
							break;
					}

					rbInmediato.IsChecked = true;
                    if(rbIndividual.IsChecked)
                        StRegaloj = "I";
                    stTipoIng = "I";
					
				}
				else
				{
					RadMessageBox.Show(this,"El Paciente ya ha sido dado de alta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
					this.Close();
				}				
				RrDatos.Close();
			}
		}

		private void ConversionHDaH_Load(Object eventSender, EventArgs eventArgs)
		{
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
            CbAceptar.Enabled = false;
		}

		private bool isInitializingComponent;
		private void rbInmediato_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				stTipoIng = "I";
			}
		}

		private void rbProceso_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (rbProceso[0].IsChecked)
				{
					stProc = "M";
				}
				if (rbProceso[1].IsChecked)
				{
					stProc = "Q";
				}
			}
		}

		private void tbCama_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			//si cambia la cama desde la ventana de camas se valida
			if (fCama)
			{
				fCambiaCama = true;
				inPlanta = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length))));
				inHabitac = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2))));
				stnCama = tbCama.Text.Substring(4).Trim();
			}
		}

		private void tbCama_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			fCambiaCama = true;
			fCama = false; //la cama es tecleada
			proAceptar();
		}

		private void tbCama_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			fCambiaCama = true;
			fCama = false; //la cama es tecleada
			proAceptar();
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void tbCama_Leave(Object eventSender, EventArgs eventArgs)
		{
			try
			{
				proAceptar();
				if (tbCama.Text != "")
				{
					fCama = false;
					tbCama.Text = tbCama.Text.ToUpper();
					inPlanta = Convert.ToInt32(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)));
					inHabitac = Convert.ToInt32(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)));
					stnCama = tbCama.Text.Substring(4).Trim();
				}
			}
			catch (Exception e)
			{				
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{"cama no v�lida"};
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				tbCama.Focus();
				return;
			}
		}

		private void proAceptar()
		{
			//compruebo que todos los campos estan rellenos antes de activar el bot�n de aceptar
			CbAceptar.Enabled = rbInmediato.IsChecked && (rbIndividual.IsChecked || rbCompartido.IsChecked) && (rbProceso[0].IsChecked || rbProceso[1].IsChecked) && tbCama.Text.Trim() != "";
		}
		private void CbSeleccion_Click(Object eventSender, EventArgs eventArgs)
		{
			mantecamas.manteclass Sel_cama = new mantecamas.manteclass(); //Para crear una instancia de la ventana de selecci�n de camas
			object tempRefParam = this;
			string tempRefParam2 = "H";
			string tempRefParam3 = "I";
			bool tempRefParam4 = false;
			bool tempRefParam5 = false;
			Sel_cama.proload(Sel_cama, tempRefParam, ref Conexion_Base_datos.RcAdmision, tempRefParam2, Conexion_Base_datos.VstCodUsua, tempRefParam3, stSexo, tempRefParam4, StRegaloj, tempRefParam5);
			Sel_cama = null;
			proAceptar();
		}

		private void rbCompartido_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				StRegaloj = "C";
				proAceptar();
			}
		}

		private void RbHombre_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				stSexo = "H";
			}
		}

		private void rbIndividual_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				StRegaloj = "I";
				proAceptar();
			}
		}

		private void RbMujer_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				stSexo = "M";
			}
		}
		public void proRecojo_Cama(string AcepCan, string Cama)
		{
			string sql = String.Empty;
			DataSet RR = null;
			if (AcepCan == "A")
			{
				fCama = true;
				tbCama.Text = Cama;
				// como al asignarle la cama que ha seleccionado Mantecamas no se dispara el
				// el change de la caja de texto TbCama -> lo simulo

				//Solo se permiten elegir tipos de cama con indicador de sillon a 'N'
				sql = "SELECT isillon FROM" + " DCAMASBOva ,DTIPOCAM " + " WHERE " + " DCAMASBOVA.gtipocam = DTIPOCAM.gtipocam AND " + " DCAMASBOva.GPLANTAS = " + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length)))).ToString() + " AND " + " DCAMASBOva.GHABITAC = " + Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2)))).ToString() + " AND " + " DCAMASBOva.gcamasbo ='" + tbCama.Text.Substring(4).Trim() + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{					
					if (Convert.ToString(RR.Tables[0].Rows[0]["isillon"]).Trim().ToUpper() != "N")
					{
						fCama = false;
						tbCama.Text = "";
						RadMessageBox.Show(this,"Debe seleccionar una cama de Hospitalizaci�n", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
					}
				}
				tbCama_TextChanged(tbCama, new EventArgs());
			}
			else if (AcepCan == "C")
			{ 
				fCama = false;
			}
		}

		public void proRespuestaCama(string stCamas)
		{
			if (stCamas != "")
			{
				fCama = true;
			}
		}

        private bool fnComprobar_cama(string regicama)
        {

            bool result = false;
            DataSet RrPacHab = null;
            string stPacHab = String.Empty;
            bool stRegAloj2 = false;
            bool cambio = false;

            //***********
            //On Error GoTo Etiqueta
            //*************
            result = true;

            bool bHayPacientesDistSexo = false;

            try
            {
                if (tbCama.Text != "")
                {
                    inPlanta = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(0, Math.Min(2, tbCama.Text.Length))));
                    inHabitac = Convert.ToInt32(Double.Parse(tbCama.Text.Substring(2, Math.Min(2, tbCama.Text.Length - 2))));
                    stnCama = tbCama.Text.Substring(4).Trim();


                    //oscar 16/12/2003
                    //No permitir realizar un igreso en una plaza de dia
                    stPacHab = "select * from DCAMASBO, DTIPOCAM " + "WHERE GPLANTAS = " + inPlanta.ToString() + " AND GHABITAC = " + inHabitac.ToString() + " AND GCAMASBO = '" + stnCama + "'" + " AND DCAMASBO.gtipocam = DTIPOCAM.gtipocam " + " AND DTIPOCAM.isillon ='S'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stPacHab, Conexion_Base_datos.RcAdmision);
                    RrPacHab = new DataSet();
                    tempAdapter.Fill(RrPacHab);
                    if (RrPacHab.Tables[0].Rows.Count != 0)
                    {
                        result = false;
                        //fAceptarMod = False                        
                        RrPacHab.Close();
                        RadMessageBox.Show(this,"No se puede realizar el ingreso en una plaza de D�a", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
                        //fCamaOcu = True  'no puede aceptar
                        CbSeleccion.Focus();
                        return result;
                    }
                    //-------------

                    //existe la cama
                    stPacHab = "SELECT " + " DCAMASBOva.iestcama,gidenpac" + " FROM " + " DCAMASBOva " + " WHERE " + " DCAMASBOva.GPLANTAS = " + inPlanta.ToString() + " AND " + " DCAMASBOva.GHABITAC = " + inHabitac.ToString() + " and dcamasbova.gcamasbo='" + stnCama + "'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stPacHab, Conexion_Base_datos.RcAdmision);
                    RrPacHab = new DataSet();
                    tempAdapter_2.Fill(RrPacHab);
                                        
                    if (RrPacHab.Tables[0].Rows.Count == 0)
                    {
                        result = false;                        
                        RrPacHab.Close();                        
                        short tempRefParam = 1100;
                        string[] tempRefParam2 = new string[] { "Cama " };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                        CbSeleccion.Focus();
                        return result;
                    }
                    else if (Convert.ToString(RrPacHab.Tables[0].Rows[0]["iestcama"]) == "O" || Convert.ToString(RrPacHab.Tables[0].Rows[0]["iestcama"]) == "B" || Convert.ToString(RrPacHab.Tables[0].Rows[0]["iestcama"]) == "I")
                    {                        
                        if (Convert.ToString(RrPacHab.Tables[0].Rows[0]["iestcama"]) == "I")
                        {
                            result = false;
                            
                            RrPacHab.Close();
                            
                            short tempRefParam3 = 1320;
                            string[] tempRefParam4 = new string[] { "Inhabilitada" };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                            CbSeleccion.Focus();
                            return result;
                        }
                        else
                        {                            
                            if (Convert.ToString(RrPacHab.Tables[0].Rows[0]["gidenpac"]) != mGidenpac)
                            {
                                result = false;
                                
                                RrPacHab.Close();
                                
                                short tempRefParam5 = 1320;
                                string[] tempRefParam6 = new string[] { "ocupada" };
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                                CbSeleccion.Focus();
                                return result;
                            }
                        }
                    }

                    // vemos paciente por paciente
                    stPacHab = "SELECT " + " DPACIENT.FNACIPAC,DPACIENT.ITIPSEXO,aepisadm.iregaloj," + " DCAMASBOva.IRESTSEX,dpacient.gidenpac" + " FROM " + " DPACIENT, DCAMASBOva,aepisadm " + " WHERE " + " DPACIENT.GIDENPAC = DCAMASBOva.GIDENPAC AND " + " Dcamasbova.GIDENPAC = aepisadm.GIDENPAC AND " + " aepisadm.faltplan is null AND " + " DCAMASBOva.GPLANTAS = " + inPlanta.ToString() + " AND " + " DCAMASBOva.GHABITAC = " + inHabitac.ToString();
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stPacHab, Conexion_Base_datos.RcAdmision);
                    RrPacHab = new DataSet();
                    tempAdapter_3.Fill(RrPacHab);

                    stRegAloj2 = false;
                    if (RrPacHab.Tables[0].Rows.Count > 0)
                    {
                        stRegAloj2 = true;                        
                        //Comprobamos Restricciones por sexo
                        foreach (DataRow iteration_row in RrPacHab.Tables[0].Rows)
                        {
                            if (Convert.ToString(iteration_row["irestsex"]) == "S" && stSexo != Convert.ToString(iteration_row["ITIPSEXO"]))
                            {
                                bHayPacientesDistSexo = true;
                                break;
                            }
                        }

                        if (bHayPacientesDistSexo)
                        {                            
                            short tempRefParam7 = 1410;
                            string[] tempRefParam8 = new string[] { "El sexo del paciente", "el de los pacientes de la habitaci�n" };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
                            if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.Yes)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                                CbSeleccion.Focus();
                                
                                RrPacHab.Close();
                                return result;
                            }
                        }                        

                        //Comprobamos Restricciones por Alojamiento
                        if (RrPacHab.Tables[0].Rows.Count == 1)
                        { //solo hay un paciente                          
                            if (Convert.ToString(RrPacHab.Tables[0].Rows[0]["gidenpac"]) != mGidenpac)
                            { //no es el mismo
                                if (StRegaloj == "I" && stRegAloj2)
                                {
                                    result = false;
                                    
                                    RrPacHab.Close();
                                    
                                    short tempRefParam9 = 1650;
                                    string[] tempRefParam10 = new string[] { };
                                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                                    if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.OK)
                                    {
                                        result = true;
                                    }
                                    return result;
                                }
                            }
                        }
                        else
                        {
                            //mas pacientes en la habitaci�n
                            if (StRegaloj == "I" && stRegAloj2)
                            {
                                result = false;
                                
                                RrPacHab.Close();
                                
                                short tempRefParam11 = 1650;
                                string[] tempRefParam12 = new string[] { };
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12));
                                if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.OK)
                                {
                                    result = true;
                                }
                                return result;
                            }
                            else
                            {
                                foreach (DataRow iteration_row_2 in RrPacHab.Tables[0].Rows)
                                { //comprobamos que los demas pacientes tienen r�gimen individual
                                    if (Convert.ToString(iteration_row_2["iregaloj"]) == "I")
                                    {                                        
                                        RrPacHab.Close();
                                        result = false;                                        
                                        short tempRefParam13 = 1650;
                                        string[] tempRefParam14 = new string[] { };
                                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, Conexion_Base_datos.RcAdmision, tempRefParam14));
                                        if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.OK)
                                        {
                                            result = true;
                                        }
                                        return result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!result)
                        {
                            if (regicama == "C")
                            {
                                CbSeleccion.Focus();
                            }
                            else
                            {
                                cambio = rbIndividual.IsChecked;
                                rbIndividual.IsChecked = rbCompartido.IsChecked;
                                rbCompartido.IsChecked = cambio;
                                if (cambio)
                                {
                                    rbCompartido.Focus();
                                }
                                else
                                {
                                    rbIndividual.Focus();
                                }
                            }
                        }
                    }
                    else
                    {
                        result = true;
                    }                    
                    RrPacHab.Close();
                }
                return result;
            }           
            catch (SqlException ex)
            {                                 
                Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
                return result;
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
                return result;
            }
        }
		
		public void recibir_historia(int error)
		{
			if (error != 0)
			{
				Ferror_histo = true;
			}
		}

		public void RecogerPeticion(int lError, object Prestado = null)
		{
			if (lError != 0)
			{
				Ferror_histo = true;
			}
		}

		private void ConversionHDaH_Closed(Object eventSender, EventArgs eventArgs)
		{
            DefInstance.Dispose();
            DefInstance = null;
        }
	}
}