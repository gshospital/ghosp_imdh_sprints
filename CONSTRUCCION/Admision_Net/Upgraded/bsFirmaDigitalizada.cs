using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;
using Telerik.WinControls;

namespace ADMISION
{
	internal static class bsFirmaDigitalizada
	{
       
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void Sleep(int dwMilliseconds);
		static bool bSoftWareFirmaDigital = false;
		static bool bCapturarFirmaPDF = false;
		static string stFicheroEJECUTABLE = String.Empty;
		static string stFicheroCONFIGURACION = String.Empty;
		static string stFicheroPLANTILLA = String.Empty;
		static string stFicheroCONFIGURACION_Consentimiento_pac = String.Empty;
		static string stFicheroCONFIGURACION_Consentimiento_med = String.Empty;
		static string stFicheroCONFIGURACION_CompromisoPago = String.Empty;
		static string stFicheroCONFIGURACION_LOPD = String.Empty;

		static string stDateLastModified = String.Empty;
		static string stFICHERO = String.Empty;
		static string varTitulo = String.Empty;
		static string varSubtitulo = String.Empty;
		static string varNombrePac = String.Empty;
		static string varAreaFirma = String.Empty;
		static string varLOPD = String.Empty;
		static string varCadena_a_Buscar = String.Empty;

		static string stPrinterName = String.Empty;
		//'    Dim stDestination As String
		//'    Dim stPrinterDriver As String
		//'    Dim StPrinterPort As String

		static IWshRuntimeLibrary.IFile Fl = null;
		static Scripting.FileSystemObject fs = null;
		static string sql = String.Empty;
		static DataSet RrDatos = null;



		internal static void proEmisionConsentimentoInformado_FirmaDigitalPaciente(SqlConnection vRcCon, dynamic vocrystal, string PGidenpac, string Pusuario, string PTipoServicio = "", int PAnoRegi = 0, int PNumregi = 0, int PNsesione = 0)
		{
            /*sdsalazar_notodo_x_3
			try
			{
				string loNombreReport = String.Empty;
				dynamic miObjPdf = null;
				object ObDocumentosPaciente = null;


				//'    stDestination = crptToPrinter 'vocrystal.Destination
				//'    stPrinterDriver = Printer.DriverName
				stPrinterName = PrinterHelper.Printer.DeviceName;
				//'    StPrinterPort = Printer.Port

				byte[] Binario = null;
				string iTipoDocumento = String.Empty;
				string DescTipoDocumento = String.Empty;
				if (fExistePDFCreator())
				{

					Pause(1);
					if (PrinterHelper.Printer.DeviceName == "PDFCreator")
					{
						proEsperaFinEjecucion("PDFCreator.exe");
					}

					loNombreReport = "CI" + PGidenpac + DateTime.Now.ToString("yyyyMMddHHmmss") + Serrores.fGET_HASH(5);
					stFICHERO = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + loNombreReport + ".pdf";

					//1. Generar el PDF
					vocrystal.Destination = (int) Crystal.DestinationConstants.crptToPrinter;
					if (Serrores.fPropiedadesPDF(ref miObjPdf))
					{
						miObjPdf.cOption["AutosaveDirectory"] = Path.GetDirectoryName(Application.ExecutablePath); //carpeta a generar
						miObjPdf.cOption["AutosaveFilename"] = loNombreReport; //Guarda el nombre
						vocrystal.Action = 1;

						while(FileSystem.Dir(stFICHERO, FileAttribute.Normal) == "")
						{
							Application.DoEvents();
						};
						miObjPdf.cclose();
						miObjPdf = null;
					}
					else
					{
						RadMessageBox.Show("No es posible generar el PDF en estos momentos.", Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
						return;
					}


					//3. Guardar el registro de documentacion entregada en la tabla de documentos entregados al paciente con estado
					// "Firmado por el Paciente" o "Pendiente de Firma",
					//  y el PDF generado en la tabla de archivos aportados en la tabla DDOCUPPDF.

					sql = "Select gtipdocu, dtipdocu From DTIPDOCU WHERE iconsinf ='S'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter.Fill(RrDatos);
					iTipoDocumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["gtipdocu"]);
					DescTipoDocumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["dtipdocu"]);
					RrDatos.Close();

					UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();

					//se a�ade el registro en la tabla DOCENPAC
					sql = "Select * From DOCENPAC Where 1 = 2";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);
					RrDatos.AddNew();
					RrDatos.Tables[0].Rows[0]["gidenpac"] = PGidenpac;
					RrDatos.Tables[0].Rows[0]["itiposer"] = PTipoServicio;
					RrDatos.Tables[0].Rows[0]["ganoregi"] = PAnoRegi;
					RrDatos.Tables[0].Rows[0]["gnumregi"] = PNumregi;
					RrDatos.Tables[0].Rows[0]["nsesione"] = PNsesione;
					if (iTipoDocumento != "")
					{
						RrDatos.Tables[0].Rows[0]["gtipdocu"] = iTipoDocumento;
					}
					RrDatos.Tables[0].Rows[0]["itipodoc"] = "I";
					RrDatos.Tables[0].Rows[0]["fentrega"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["gusuario"] = Pusuario;
					RrDatos.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["gnomdocu"] = loNombreReport;
					RrDatos.Tables[0].Rows[0]["oobserva"] = DescTipoDocumento;
					RrDatos.Tables[0].Rows[0]["ientapor"] = "E";
					string tempQuery = RrDatos.Tables[0].TableName;
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(RrDatos, RrDatos.Tables[0].TableName);
					RrDatos.Close();


					//Se a�ade el registro en la tabla DDOCUPDF
					Binario = (byte[]) Serrores.fPasaBinario(stFICHERO);
					sql = "Select * From DDOCUPDF Where 1 = 2";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter_4.Fill(RrDatos);
					RrDatos.AddNew();
					RrDatos.Tables[0].Rows[0]["gnombdoc"] = loNombreReport;
					RrDatos.Tables[0].Rows[0]["inumdocu"] = 1;
					RrDatos.Tables[0].Rows[0]["ddescrip"] = DescTipoDocumento;
					RrDatos.Tables[0].Rows[0]["fechalta"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["odocupdf"] = Binario;
					RrDatos.Tables[0].Rows[0]["dnombpdf"] = loNombreReport + ".pdf";
					RrDatos.Tables[0].Rows[0]["gusuario"] = Pusuario;
					string tempQuery_2 = RrDatos.Tables[0].TableName;
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_5);
					tempAdapter_5.Update(RrDatos, RrDatos.Tables[0].TableName);
					RrDatos.Close();

					UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();

					if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
					{
						File.Delete(stFICHERO);
					}

					//    MsgBox "PrinterDriver : " & stPrinterDriver & _
					//'             "PrinterName : " & stPrinterName & _
					//'             "PrinterPort : " & StPrinterPort

					//'        vocrystal.Destination = stDestination
					//'        vocrystal.PrinterDriver = stPrinterDriver
					//'        vocrystal.PrinterName = stPrinterName
					//'        vocrystal.PrinterPort = StPrinterPort
					//'        vocrystal.Action = 1
					//'
					//'    vocrystal.Reset

					if (stPrinterName != "")
					{
						Serrores.proEstablecerImpresoraPredeterminada(stPrinterName);
					}
					proMatarProceso("PDFCreator.exe");

					ObDocumentosPaciente = new DocumentosPaciente.MCDenpaciente();
					if (PTipoServicio != "")
					{
						ObDocumentosPaciente.proLlamadaEpisodioDirecto(vRcCon, Pusuario, PTipoServicio, PAnoRegi, PNumregi, PNsesione, loNombreReport);
					}
					else
					{
						ObDocumentosPaciente.proLLamadaSinEpisodio(vRcCon, Pusuario, PGidenpac, loNombreReport);
					}
					ObDocumentosPaciente = null;

				}
			}
			catch (Exception e)
			{

				UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
				if (stPrinterName != "")
				{
					Serrores.proEstablecerImpresoraPredeterminada(stPrinterName);
				}
				if (stFICHERO.Trim() != "")
				{
					if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
					{
						File.Delete(stFICHERO);
					}
				}
				//El error 20545 se produce al dar cancelar en la ventana de impresion
				if (Information.Err().Number != 20545)
				{
					RadMessageBox.Show("Error en proEmisionConsentimientoInformado_FirmaDigitalPaciente : " + e.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
            */

		}

		internal static void proEmisionCompromisoPago_FirmaDigitalPaciente(SqlConnection vRcCon, Crystal vocrystal, string PGidenpac, string Pusuario, string PTipoServicio = "", int PAnoRegi = 0, int PNumregi = 0, int PNsesione = 0)
		{
			try
			{
				string loNombreReport = String.Empty;
				dynamic miObjPdf = null;
                DocumentosPaciente.MCDenpaciente ObDocumentosPaciente = null;


				//    stDestination = crptToPrinter 'vocrystal.Destination
				//    stPrinterDriver = Printer.DriverName
				stPrinterName = PrinterHelper.Printer.DeviceName;
				//    StPrinterPort = Printer.Port

				byte[] Binario = null;
				string iTipoDocumento = String.Empty;
				string DescTipoDocumento = String.Empty;
				if (fExistePDFCreator())
				{

					Pause(1);
					if (PrinterHelper.Printer.DeviceName == "PDFCreator")
					{
						proEsperaFinEjecucion("PDFCreator.exe");
					}

					loNombreReport = "CP" + PGidenpac + DateTime.Now.ToString("yyyyMMddHHmmss") + Serrores.fGET_HASH(5);
					stFICHERO = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + loNombreReport + ".pdf";

					//1. Generar el PDF
					vocrystal.Destination = Crystal.DestinationConstants.crptToPrinter;
					if (Serrores.fPropiedadesPDF(ref miObjPdf))
					{
						miObjPdf.cOption["AutosaveDirectory"] = Path.GetDirectoryName(Application.ExecutablePath); //carpeta a generar
						miObjPdf.cOption["AutosaveFilename"] = loNombreReport; //Guarda el nombre
						vocrystal.Action = 1;

						while(FileSystem.Dir(stFICHERO, FileAttribute.Normal) == "")
						{
							Application.DoEvents();
						};
						miObjPdf.cclose();
						miObjPdf = null;
					}
					else
					{
						RadMessageBox.Show("No es posible generar el PDF en estos momentos.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
						return;
					}

					//3. Guardar el registro de documentacion entregada en la tabla de documentos entregados al paciente con estado
					// "Firmado por el Paciente" o "Pendiente de Firma",
					//  y el PDF generado en la tabla de archivos aportados en la tabla DDOCUPPDF.

					sql = "Select gtipdocu, dtipdocu From DTIPDOCU WHERE icompago ='S'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter.Fill(RrDatos);
					iTipoDocumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["gtipdocu"]);
					DescTipoDocumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["dtipdocu"]);
					RrDatos.Close();

                    vRcCon.BeginTransScope();

                    //se a�ade el registro en la tabla DOCENPAC
                    sql = "Select * From DOCENPAC Where 1 = 2";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);
					RrDatos.AddNew();
					RrDatos.Tables[0].Rows[0]["gidenpac"] = PGidenpac;
					RrDatos.Tables[0].Rows[0]["itiposer"] = PTipoServicio;
					RrDatos.Tables[0].Rows[0]["ganoregi"] = PAnoRegi;
					RrDatos.Tables[0].Rows[0]["gnumregi"] = PNumregi;
					RrDatos.Tables[0].Rows[0]["nsesione"] = PNsesione;
					if (iTipoDocumento != "")
					{
						RrDatos.Tables[0].Rows[0]["gtipdocu"] = iTipoDocumento;
					}
					RrDatos.Tables[0].Rows[0]["itipodoc"] = "I";
					RrDatos.Tables[0].Rows[0]["fentrega"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["gusuario"] = Pusuario;
					RrDatos.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["gnomdocu"] = loNombreReport;
					RrDatos.Tables[0].Rows[0]["oobserva"] = DescTipoDocumento;
					RrDatos.Tables[0].Rows[0]["ientapor"] = "E";
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					tempAdapter_2.Update(RrDatos, RrDatos.Tables[0].TableName);
					RrDatos.Close();


					//Se a�ade el registro en la tabla DDOCUPDF
					Binario = (byte[]) Serrores.fPasaBinario(stFICHERO);
					sql = "Select * From DDOCUPDF Where 1 = 2";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter_4.Fill(RrDatos);
					RrDatos.AddNew();
					RrDatos.Tables[0].Rows[0]["gnombdoc"] = loNombreReport;
					RrDatos.Tables[0].Rows[0]["inumdocu"] = 1;
					RrDatos.Tables[0].Rows[0]["ddescrip"] = DescTipoDocumento;
					RrDatos.Tables[0].Rows[0]["fechalta"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["odocupdf"] = Binario;
					RrDatos.Tables[0].Rows[0]["dnombpdf"] = loNombreReport + ".pdf";
					RrDatos.Tables[0].Rows[0]["gusuario"] = Pusuario;
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
					tempAdapter_4.Update(RrDatos, RrDatos.Tables[0].TableName);
					RrDatos.Close();

                    vRcCon.CommitTransScope();


                    if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
					{
						File.Delete(stFICHERO);
					}

					//       vocrystal.Destination = stDestination
					//       vocrystal.PrinterDriver = stPrinterDriver
					//       vocrystal.PrinterName = stPrinterName
					//       vocrystal.PrinterPort = StPrinterPort

					//       MsgBox "PrinterDriver : " & stPrinterDriver & _
					//'           "PrinterName : " & stPrinterName & _
					//'           "PrinterPort : " & StPrinterPort
					//       vocrystal.Action = 1
					//       vocrystal.Reset
					if (stPrinterName != "")
					{
						Serrores.proEstablecerImpresoraPredeterminada(stPrinterName);
					}
					proMatarProceso("PDFCreator.exe");

					ObDocumentosPaciente = new DocumentosPaciente.MCDenpaciente();
					if (PTipoServicio != "")
					{
						ObDocumentosPaciente.proLlamadaEpisodioDirecto(vRcCon, Pusuario, PTipoServicio, PAnoRegi, PNumregi, PNsesione, loNombreReport);
					}
					else
					{
						ObDocumentosPaciente.proLLamadaSinEpisodio(vRcCon, Pusuario, PGidenpac, loNombreReport);
					}
					ObDocumentosPaciente = null;

				}
			}
			catch (Exception e)
			{
                vRcCon.RollbackTransScope();

                if (stPrinterName != "")
				{
					Serrores.proEstablecerImpresoraPredeterminada(stPrinterName);
				}
				if (stFICHERO.Trim() != "")
				{
					if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
					{
						File.Delete(stFICHERO);
					}
				}
				//El error 20545 se produce al dar cancelar en la ventana de impresion
				if (Information.Err().Number != 20545)
				{
					RadMessageBox.Show("Error en proEmisionCompromisoPago_FirmaDigitalPaciente : " + e.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
				}
			}

		}

        /*sdsalazar_notodo_x_3
		internal static void proEmisionLOPD_FirmaDigitalPaciente(SqlConnection vRcCon, dynamic vocrystal, string PGidenpac, string Pusuario, string PTipoServicio = "", int PAnoRegi = 0, int PNumregi = 0, int PNsesione = 0)
		{

			try
			{
				string loNombreReport = String.Empty;
				dynamic miObjPdf = null;
				object ObDocumentosPaciente = null;

				//    stDestination = crptToPrinter 'vocrystal.Destination
				//    stPrinterDriver = Printer.DriverName
				stPrinterName = PrinterHelper.Printer.DeviceName;
				//    StPrinterPort = Printer.Port


				byte[] Binario = null;
				string iTipoDocumento = String.Empty;
				string DescTipoDocumento = String.Empty;
				if (fExistePDFCreator())
				{

					Pause(1);
					if (PrinterHelper.Printer.DeviceName == "PDFCreator")
					{
						proEsperaFinEjecucion("PDFCreator.exe");
					}

					loNombreReport = "LOPD" + PGidenpac + DateTime.Now.ToString("yyyyMMddHHmmss") + Serrores.fGET_HASH(5);
					stFICHERO = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + loNombreReport + ".pdf";

					//1. Generar el PDF
					vocrystal.Destination = (int) Crystal.DestinationConstants.crptToPrinter;
					if (Serrores.fPropiedadesPDF(ref miObjPdf))
					{
						miObjPdf.cOption["AutosaveDirectory"] = Path.GetDirectoryName(Application.ExecutablePath); //carpeta a generar
						miObjPdf.cOption["AutosaveFilename"] = loNombreReport; //Guarda el nombre
						vocrystal.Action = 1;

						while(FileSystem.Dir(stFICHERO, FileAttribute.Normal) == "")
						{
							Application.DoEvents();
						};
						miObjPdf.cclose();
						miObjPdf = null;
					}
					else
					{
						RadMessageBox.Show("No es posible generar el PDF en estos momentos.", Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
						return;
					}

					//3. Guardar el registro de documentacion entregada en la tabla de documentos entregados al paciente con estado
					// "Firmado por el Paciente" o "Pendiente de Firma",
					//  y el PDF generado en la tabla de archivos aportados en la tabla DDOCUPPDF.

					sql = "Select gtipdocu, dtipdocu From DTIPDOCU WHERE iacelopd ='S'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter.Fill(RrDatos);
					iTipoDocumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["gtipdocu"]);
					DescTipoDocumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["dtipdocu"]);
					RrDatos.Close();

					UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();

					//se a�ade el registro en la tabla DOCENPAC
					sql = "Select * From DOCENPAC Where 1 = 2";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);
					RrDatos.AddNew();
					RrDatos.Tables[0].Rows[0]["gidenpac"] = PGidenpac;
					RrDatos.Tables[0].Rows[0]["itiposer"] = PTipoServicio;
					RrDatos.Tables[0].Rows[0]["ganoregi"] = PAnoRegi;
					RrDatos.Tables[0].Rows[0]["gnumregi"] = PNumregi;
					RrDatos.Tables[0].Rows[0]["nsesione"] = PNsesione;
					if (iTipoDocumento != "")
					{
						RrDatos.Tables[0].Rows[0]["gtipdocu"] = iTipoDocumento;
					}
					RrDatos.Tables[0].Rows[0]["itipodoc"] = "I";
					RrDatos.Tables[0].Rows[0]["fentrega"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["gusuario"] = Pusuario;
					RrDatos.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["gnomdocu"] = loNombreReport;
					RrDatos.Tables[0].Rows[0]["oobserva"] = DescTipoDocumento;
					RrDatos.Tables[0].Rows[0]["ientapor"] = "E";
					string tempQuery = RrDatos.Tables[0].TableName;
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(RrDatos, RrDatos.Tables[0].TableName);
					RrDatos.Close();


					//Se a�ade el registro en la tabla DDOCUPDF
					Binario = (byte[]) Serrores.fPasaBinario(stFICHERO);
					sql = "Select * From DDOCUPDF Where 1 = 2";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, vRcCon);
					RrDatos = new DataSet();
					tempAdapter_4.Fill(RrDatos);
					RrDatos.AddNew();
					RrDatos.Tables[0].Rows[0]["gnombdoc"] = loNombreReport;
					RrDatos.Tables[0].Rows[0]["inumdocu"] = 1;
					RrDatos.Tables[0].Rows[0]["ddescrip"] = DescTipoDocumento;
					RrDatos.Tables[0].Rows[0]["fechalta"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					RrDatos.Tables[0].Rows[0]["odocupdf"] = Binario;
					RrDatos.Tables[0].Rows[0]["dnombpdf"] = loNombreReport + ".pdf";
					RrDatos.Tables[0].Rows[0]["gusuario"] = Pusuario;
					string tempQuery_2 = RrDatos.Tables[0].TableName;
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_5);
					tempAdapter_5.Update(RrDatos, RrDatos.Tables[0].TableName);
					RrDatos.Close();

					UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();

					if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
					{
						File.Delete(stFICHERO);
					}

					//    MsgBox "PrinterDriver : " & stPrinterDriver & _
					//'         "PrinterName : " & stPrinterName & _
					//'         "PrinterPort : " & StPrinterPort

					//    vocrystal.Destination = stDestination
					//    vocrystal.PrinterDriver = stPrinterDriver
					//    vocrystal.PrinterName = stPrinterName
					//    vocrystal.PrinterPort = StPrinterPort
					//    vocrystal.Action = 1
					//
					//    vocrystal.Reset
					if (stPrinterName != "")
					{
						Serrores.proEstablecerImpresoraPredeterminada(stPrinterName);
					}
					proMatarProceso("PDFCreator.exe");

					ObDocumentosPaciente = new DocumentosPaciente.MCDenpaciente();
					if (PTipoServicio != "")
					{
						ObDocumentosPaciente.proLlamadaEpisodioDirecto(vRcCon, Pusuario, PTipoServicio, PAnoRegi, PNumregi, PNsesione, loNombreReport);
					}
					else
					{
						ObDocumentosPaciente.proLLamadaSinEpisodio(vRcCon, Pusuario, PGidenpac, loNombreReport);
					}
					ObDocumentosPaciente = null;

				}
			}
			catch (Exception e)
			{

				if (stPrinterName != "")
				{
					Serrores.proEstablecerImpresoraPredeterminada(stPrinterName);
				}
				UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
				if (stFICHERO.Trim() != "")
				{
					if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
					{
						File.Delete(stFICHERO);
					}
				}
				//El error 20545 se produce al dar cancelar en la ventana de impresion
				if (Information.Err().Number != 20545)
				{
					RadMessageBox.Show("Error en proEmisionLOPD_FirmaDigitalPaciente : " + e.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}

		}
        fin sdsalazar_notodo_x_3*/

        /*sdsalazar_notodo_x_3
      internal static void ProCapturarFirmaDigitalPacienteResponsable(SqlConnection vRcCon, string Accion, string PNomDocumento, string PidPaciente, string PTipDocu, ref string Pfsistema, string Pientapor, string Pusuario)
      {
          string strSql = String.Empty;
          DataSet rdoTemp = null;
          byte[] bytFichero = null; //Variable a almacenar en Fichero
          int intFichero = 0;
          int iRespuesta = 0;
          bool iConsentimiento = false;
          bool iCompromisoPago = false;
          bool iLOPD = false;
          string stiTipoSer = String.Empty;
          int iAnoRegi = 0;
          int iNumregi = 0;
          try
          {
              stDateLastModified = "";
              stFICHERO = "";
              varTitulo = "";
              varSubtitulo = "";
              varNombrePac = "";
              varAreaFirma = "";
              varLOPD = "";
              varCadena_a_Buscar = "";

              iConsentimiento = false;
              iCompromisoPago = false;
              iLOPD = false;

              //    '****A BORRAR
              //    Dim sArchivo As String
              //    Dim CadenaArchivos As String
              //    CadenaArchivos = ""
              //    sql = "SELECT valfanu2,valfanu3 FROM SCONSGLO WHERE gconsglo='FIRMADIG'"
              //    Set RRDatos = vRcCon.OpenResultset(sql, 1, 1)
              //    If Not RRDatos.EOF Then
              //        CadenaArchivos = CadenaArchivos & "DIRECTORIO: " & Trim(RRDatos("valfanu2")) & Trim(RRDatos("valfanu3")) & vbCrLf
              //        CadenaArchivos = CadenaArchivos & "FICHEROS:"
              //        sArchivo = Dir(Trim(RRDatos("valfanu2")) & Trim(RRDatos("valfanu3")) & "\*.ini")
              //        Do While sArchivo <> ""
              //            CadenaArchivos = CadenaArchivos & vbCrLf & sArchivo
              //            sArchivo = Dir
              //        Loop
              //    End If
              //    RRDatos.Close
              //    MsgBox CadenaArchivos
              //    '-------------------

              bCapturarFirmaPDF = false;
              bSoftWareFirmaDigital = false;
              bSoftWareFirmaDigital = fSoftWareFirmaDigital(ref stFicheroEJECUTABLE);
              if (!bSoftWareFirmaDigital)
              {
                  //        MsgBox "Atenci�n: " & vbCrLf & _
                  //"No se encuentra el software ecoSignature_Tablet.exe", vbCritical, vNomAplicacion
                  return;
              }

              strSql = "SELECT ISNULL(iconsinf,'N') as iconsinf, " + 
                       "       ISNULL(icompago,'N') as icompago, " + 
                       "       ISNULL(iacelopd,'N') as iacelopd, dtipdocu FROM DTIPDOCU where gtipdocu=" + PTipDocu;
              SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, vRcCon);
              rdoTemp = new DataSet();
              tempAdapter.Fill(rdoTemp);
              if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["iconsinf"]) == "S")
              {
                  iConsentimiento = true;
                  if (!fFicheroConfiguracionConsentimientoPaciente(vRcCon, ref stFicheroCONFIGURACION_Consentimiento_pac))
                  {
                      //            MsgBox "Atenci�n: " & vbCrLf & _
                      //"No se encuentra el fichero de configuraci�n para firmar el " & rdoTemp("dtipdocu") & " para el paciente", vbCritical, vNomAplicacion
                      return;
                  }
                  else
                  {
                      stFicheroCONFIGURACION = stFicheroCONFIGURACION_Consentimiento_pac;
                  }
              }
              else if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["icompago"]) == "S")
              { 
                  iCompromisoPago = true;
                  if (!fFicheroConfiguracionCompromisoPago(vRcCon, ref stFicheroCONFIGURACION_CompromisoPago))
                  {
                      //            MsgBox "Atenci�n: " & vbCrLf & _
                      //"No se encuentra el fichero de configuraci�n para firmar el " & rdoTemp("dtipdocu") & " para el paciente", vbCritical, vNomAplicacion
                      return;
                  }
                  else
                  {
                      stFicheroCONFIGURACION = stFicheroCONFIGURACION_CompromisoPago;
                  }
              }
              else if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["iacelopd"]) == "S")
              { 
                  iLOPD = true;
                  if (!fFicheroConfiguracionLOPD(vRcCon, ref stFicheroCONFIGURACION_LOPD))
                  {
                      //            MsgBox "Atenci�n: " & vbCrLf & _
                      //"No se encuentra el fichero de configuraci�n para firmar el " & rdoTemp("dtipdocu") & " para el paciente", vbCritical, vNomAplicacion
                      return;
                  }
                  else
                  {
                      stFicheroCONFIGURACION = stFicheroCONFIGURACION_LOPD;
                  }
              }
              rdoTemp.Close();

              stFicheroPLANTILLA = stFicheroCONFIGURACION;
              string tempRefParam = ".ini";
              stFicheroPLANTILLA = Serrores.proReplace(ref stFicheroPLANTILLA, ref tempRefParam, ".ebp");

              strSql = "SELECT dnombpdf, odocupdf FROM DDOCUPDF WHERE gnombdoc = '" + PNomDocumento + "' and inumdocu=1";
              SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSql, vRcCon);
              rdoTemp = new DataSet();
              tempAdapter_2.Fill(rdoTemp);
              if (rdoTemp.Tables[0].Rows.Count == 0)
              {
                  RadMessageBox.Show("Atenci�n," + Environment.NewLine + 
                                  "No se ha encontrado el documento especificado en la tabla de Documentos PDF.", Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                  return;
              }
              else
              {
                  stFICHERO = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpdf"]);
                  bytFichero = (byte[]) rdoTemp.Tables[0].Rows[0]["odocupdf"];
              }
              rdoTemp.Close();

              proMatarProceso("ecoSignature_Tablet.exe");

              //1  Recuperar en un archivo PDF el documento almacenado en la tabla de documentos (DDOCUPDF)
              if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
              {
                  File.Delete(stFICHERO);
              }
              intFichero = FileSystem.FreeFile();
              FileSystem.FileOpen(intFichero, stFICHERO, OpenMode.Binary, OpenAccess.Write, OpenShare.Default, -1);
              FileSystem.FilePutObject(intFichero, bytFichero, 0);
              FileSystem.FileClose(intFichero);

              //2. Pasarselo al ejecutable de EDETALIA
              //SHELL %PROGRAMA% %FICHERO% %CONFIGURACION% %Directorio_Salida% %AutoSign% %Guardar% %cerrar%  %Titulo% %Subtitulo% %Nombre% %AreaFirma% %LOPD% %Cadena_a_Buscar% %plantilla%

              switch(Accion)
              {
                  case "PAC" : 
                      varCadena_a_Buscar = "FIRMA PACIENTE"; 
                      strSql = "SELECT dape1pac, dape2pac, dnombpac FROM DPACIENT where gidenpac='" + PidPaciente + "'"; 
                      SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSql, vRcCon); 
                      rdoTemp = new DataSet(); 
                      tempAdapter_3.Fill(rdoTemp); 
                      varNombrePac = "Paciente: " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]) + "").Trim() + " " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]) + "").Trim() + " " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]) + "").Trim(); 
                      rdoTemp.Close(); 
                      break;
                  case "RES" : 
                      varCadena_a_Buscar = "FIRMA RESPONSABLE"; 
                      varNombrePac = "Tutor / Responsable Legal"; 
                      break;
                  case "REV" : 
                      varCadena_a_Buscar = "FIRMA REVOCACION"; 
                      varNombrePac = "Paciente (o en su defecto Tutor / Responsable Legal)"; 
                      break;
              }

              fs = new Scripting.FileSystemObject();
              Fl = fs.GetFile(stFICHERO);
              stDateLastModified = DateTimeHelper.ToString(Fl.DateLastModified);

              iRespuesta = Convert.ToInt32(Process.Start(stFicheroEJECUTABLE + 
                           " " + "\"" + "-file='" + stFICHERO + "'" + "\"" + 
                           " " + "\"" + "-conf='" + stFicheroCONFIGURACION + "'" + "\"" + 
                           " " + "\"" + "-outdir='" + Path.GetDirectoryName(Application.ExecutablePath) + "'" + "\"" + 
                           " " + "\"" + "-AutoSign=1" + "\"" + 
                           " " + "\"" + "-autosave=1" + "\"" + 
                           " " + "\"" + "-autoclose" + "\"" + 
                           " " + "\"" + "-ini='[Modulo_Posicion_Widget_Str]Cadena_A_Buscar=" + varCadena_a_Buscar + "'" + "\"" + 
                           " " + "\"" + "-ini='[ecoBio_Tableta]Plantilla=" + stFicheroPLANTILLA + "'" + "\"" + 
                           " " + "\"" + "-var='Nombre=" + varNombrePac + "'" + "\"" + 
                           ((int) ProcessWindowStyle.Normal).ToString()).Id);

              proEsperaFinEjecucion("ecoSignature_Tablet.exe");

              //Si la fecha de ultima modificacion del PDF permanece inalterada despues de que EDETALIA haya finalizado,
              //signifiara que el PDF esta sin firmar digitalmente por lo que no se guardara en estado firmado.
              System.DateTime TempDate = DateTime.FromOADate(0);
              if (Fl.DateLastModified.ToString("yyyyMMddHHmmss") != ((DateTime.TryParse(stDateLastModified, out TempDate)) ? TempDate.ToString("yyyyMMddHHmmss") : stDateLastModified))
              {
                  bCapturarFirmaPDF = true;
              }
              Fl = null;
              fs = null;


              //3. Si se firma, se actualiza el registro en la tabla de documentacion entregada por el paciente (DOCENPAC)
              //   y se sustitye con el PDF nuevo generado icluyendo la firma en la tabla de archivos aportados (DOCUPPDF).
              if (bCapturarFirmaPDF)
              {

                  object tempRefParam2 = Pfsistema;
                  strSql = "Select * From DOCENPAC " + 
                           " WHERE gidenpac = '" + PidPaciente + "' AND " + 
                           "       gtipdocu = " + PTipDocu + " AND " + 
                           "       fsistema = " + Serrores.FormatFechaHMS(ref tempRefParam2) + " AND " + 
                           "       ientapor = '" + Pientapor + "'";
                  Pfsistema = Convert.ToString(tempRefParam2);
                  SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(strSql, vRcCon);
                  rdoTemp = new DataSet();
                  tempAdapter_4.Fill(rdoTemp);
                  if (rdoTemp.Tables[0].Rows.Count != 0)
                  {
                      stiTipoSer = Convert.ToString(rdoTemp.Tables[0].Rows[0]["itiposer"]) + "";
                      if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["ganoregi"]))
                      {
                          iAnoRegi = Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["ganoregi"]);
                      }
                      if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gnumregi"]))
                      {
                          iNumregi = Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["gnumregi"]);
                      }
                      rdoTemp.Edit();
                      rdoTemp.Tables[0].Rows[0]["gusuario"] = Pusuario;
                      rdoTemp.Tables[0].Rows[0]["ientapor"] = "A";
                      switch(Accion)
                      {
                          case "PAC" : 
                              rdoTemp.Tables[0].Rows[0]["ffirmpac"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"); 
                              break;
                          case "RES" : 
                              rdoTemp.Tables[0].Rows[0]["ffirmres"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"); 
                              break;
                          case "REV" : 
                              rdoTemp.Tables[0].Rows[0]["ffirmrev"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"); 
                              break;
                      }
                      string tempQuery = rdoTemp.Tables[0].TableName;
                      SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery, "");
                      SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_5);
                      tempAdapter_5.Update(rdoTemp, rdoTemp.Tables[0].TableName);
                  }
                  rdoTemp.Close();

                  bytFichero = (byte[]) Serrores.fPasaBinario(stFICHERO);
                  strSql = "Select * From DDOCUPDF WHERE gnombdoc='" + PNomDocumento + "' AND inumdocu=1";
                  SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(strSql, vRcCon);
                  rdoTemp = new DataSet();
                  tempAdapter_6.Fill(rdoTemp);
                  rdoTemp.Edit();
                  rdoTemp.Tables[0].Rows[0]["odocupdf"] = bytFichero;
                  rdoTemp.Tables[0].Rows[0]["gusuario"] = Pusuario;
                  string tempQuery_2 = rdoTemp.Tables[0].TableName;
                  SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tempQuery_2, "");
                  SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_7);
                  tempAdapter_7.Update(rdoTemp, rdoTemp.Tables[0].TableName);
                  rdoTemp.Close();

                  //Si se ha firmado un documento de aceptacion de LOPD, se marca en DPACIENT el check de auorizacion de utilizacion de daos
                  //pero si se caputura la firma de revocaci�n de firma del documento LOPD, se desmarca en DPACIENT dicho indicador.
                  if (iLOPD)
                  {
                      if (Accion != "REV")
                      {
                          strSql = " UPDATE DPACIENT SET iaututil='S' WHERE gidenpac ='" + PidPaciente + "'";
                      }
                      else
                      {
                          strSql = " UPDATE DPACIENT SET iaututil='N' WHERE gidenpac ='" + PidPaciente + "'";
                      }
                      SqlCommand tempCommand = new SqlCommand(strSql, vRcCon);
                      tempCommand.ExecuteNonQuery();
                  }

                  //Si se ha firmado un Consentimento Informado de una intervencion quirurgica y se encuentra activada la constante QCONSINF,
                  //se actualiza el campo iconsinf de la intervencion.
                  if (iConsentimiento && stiTipoSer == "Q" && Serrores.ObternerValor_CTEGLOBAL(vRcCon, "QCONSINF", "VALFANU1") == "S")
                  {
                      strSql = " UPDATE QINTQUIR SET iconsinf='S' WHERE ganoregi =" + iAnoRegi.ToString() + " AND gnumregi = " + iNumregi.ToString();
                      SqlCommand tempCommand_2 = new SqlCommand(strSql, vRcCon);
                      tempCommand_2.ExecuteNonQuery();
                  }

              }

              if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
              {
                  File.Delete(stFICHERO);
              }
          }
          catch (System.Exception excep)
          {

              if (stFICHERO.Trim() != "")
              {
                  if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
                  {
                      File.Delete(stFICHERO);
                  }
              }
              RadMessageBox.Show("Error en proCapturarFirmaPacienteResponsable : " + excep.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
      }
        fin sdsalazar_notodo_x_3*/

        
        internal static void ProCapturarFirmaDigitalMedico(SqlConnection vRcCon, string PNomDocumento, string PidPaciente, string PTipDocu, ref string Pfsistema, string Pientapor, string Pusuario)
        {
            string strSql = String.Empty;
            DataSet rdoTemp = null;
            byte[] bytFichero = null; //Variable a almacenar en Fichero
            int intFichero = 0;
            int iRespuesta = 0;

            try
            {

                stDateLastModified = "";
                stFICHERO = "";
                varTitulo = "";
                varSubtitulo = "";
                varNombrePac = "";
                varAreaFirma = "";
                varLOPD = "";
                varCadena_a_Buscar = "";

                bCapturarFirmaPDF = false;
                bSoftWareFirmaDigital = false;
                bSoftWareFirmaDigital = fSoftWareFirmaDigital(ref stFicheroEJECUTABLE);
                if (!bSoftWareFirmaDigital)
                {
                    //        MsgBox "Atenci�n: " & vbCrLf & _
                    //"No se encuentra el software ecoSignature_Tablet.exe", vbCritical, vNomAplicacion
                    return;
                }

                strSql = "SELECT ISNULL(iconsinf,'N') as iconsinf, dtipdocu FROM DTIPDOCU where gtipdocu=" + PTipDocu;
                SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, vRcCon);
                rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);
                if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["iconsinf"]) == "S")
                {
                    if (!fFicheroConfiguracionConsentimientoMedico(vRcCon, ref stFicheroCONFIGURACION_Consentimiento_med))
                    {
                        //MsgBox "Atenci�n: " & vbCrLf & _
                        //"No se encuentra el fichero de configuraci�n para firmar el " & rdoTemp("dtipdocu") & " para el m�dico", vbCritical, vNomAplicacion
                        return;
                    }
                    else
                    {
                        stFicheroCONFIGURACION = stFicheroCONFIGURACION_Consentimiento_med;
                    }
                }
                rdoTemp.Close();

                stFicheroPLANTILLA = stFicheroCONFIGURACION;
                string tempRefParam = ".ini";
                stFicheroPLANTILLA = Serrores.proReplace(ref stFicheroPLANTILLA, tempRefParam, ".ebp");

                strSql = "SELECT dnombpdf, odocupdf FROM DDOCUPDF WHERE gnombdoc = '" + PNomDocumento + "' and inumdocu=1";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSql, vRcCon);
                rdoTemp = new DataSet();
                tempAdapter_2.Fill(rdoTemp);
                if (rdoTemp.Tables[0].Rows.Count == 0)
                {
                    RadMessageBox.Show("Atenci�n," + Environment.NewLine + 
                                    "No se ha encontrado el documento especificado en la tabla de Documentos PDF.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
                else
                {
                    stFICHERO = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpdf"]);
                    bytFichero = (byte[]) rdoTemp.Tables[0].Rows[0]["odocupdf"];
                }
                rdoTemp.Close();

                proMatarProceso("ecoSignature_Tablet.exe");

                //1  Recuperar en un archivo PDF el documento almacenado en la tabla de documentos (DDOCUPDF)
                if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
                {
                    File.Delete(stFICHERO);
                }
                intFichero = FileSystem.FreeFile();
                FileSystem.FileOpen(intFichero, stFICHERO, OpenMode.Binary, OpenAccess.Write, OpenShare.Default, -1);
                FileSystem.FilePutObject(intFichero, bytFichero, 0);
                FileSystem.FileClose(intFichero);

                //2. Pasarselo al ejecutable de EDETALIA
                //SHELL %PROGRAMA% %FICHERO% %CONFIGURACION% %Directorio_Salida% %AutoSign% %Guardar% %cerrar%  %Titulo% %Subtitulo% %Nombre% %AreaFirma% %LOPD% %Cadena_a_Buscar% %plantilla%

                varCadena_a_Buscar = "FIRMA MEDICO";
                strSql = "SELECT dap1pers, dap2pers, dnompers, ncolegia FROM DPERSONA " + 
                         " INNER JOIN SUSUARIO ON SUSUARIO.gpersona = DPERSONA.gpersona " + 
                         " where gusuario='" + Pusuario + "'";
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSql, vRcCon);
                rdoTemp = new DataSet();
                tempAdapter_3.Fill(rdoTemp);
                varNombrePac = "M�dico: " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnompers"]) + "").Trim() + " " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap1pers"]) + "").Trim() + " " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap2pers"]) + "").Trim() + 
                               " - N� Col.: " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["ncolegia"]) + "").Trim();
                rdoTemp.Close();

                fs = new Scripting.FileSystemObject();
                /*sdsalazar todo_3_3
                Fl = fs.GetFile(stFICHERO);
                sdsalazar todo_3_3*/
                stDateLastModified = DateTimeHelper.ToString(Fl.DateLastModified);

                iRespuesta = Convert.ToInt32(Process.Start(stFicheroEJECUTABLE + 
                             " " + "\"" + "-file='" + stFICHERO + "'" + "\"" + 
                             " " + "\"" + "-conf='" + stFicheroCONFIGURACION + "'" + "\"" + 
                             " " + "\"" + "-outdir='" + Path.GetDirectoryName(Application.ExecutablePath) + "'" + "\"" + 
                             " " + "\"" + "-AutoSign=1" + "\"" + 
                             " " + "\"" + "-autosave=1" + "\"" + 
                             " " + "\"" + "-autoclose" + "\"" + 
                             " " + "\"" + "-ini='[Modulo_Posicion_Widget_Str]Cadena_A_Buscar=" + varCadena_a_Buscar + "'" + "\"" + 
                             " " + "\"" + "-ini='[ecoBio_Tableta]Plantilla=" + stFicheroPLANTILLA + "'" + "\"" + 
                             " " + "\"" + "-var='Nombre=" + varNombrePac + "'" + "\"" + 
                             ((int) ProcessWindowStyle.Normal).ToString()).Id);

                proEsperaFinEjecucion("ecoSignature_Tablet.exe");

                //Si la fecha de ultima modificacion del PDF permanece inalterada despues de que EDETALIA haya finalizado,
                //signifiara que el PDF esta sin firmar digitalmente por lo que no se guardara en estado firmado.
                System.DateTime TempDate = DateTime.FromOADate(0);
                if (Fl.DateLastModified.ToString("yyyyMMddHHmmss") != ((DateTime.TryParse(stDateLastModified, out TempDate)) ? TempDate.ToString("yyyyMMddHHmmss") : stDateLastModified))
                {
                    bCapturarFirmaPDF = true;
                }
                Fl = null;
                fs = null;


                //3. Si se firma, se actualiza el registro en la tabla de documentacion entregada por el paciente (DOCENPAC)
                //   y se sustitye con el PDF nuevo generado icluyendo la firma en la tabla de archivos aportados (DDOCUPPDF).
                if (bCapturarFirmaPDF)
                {

                    object tempRefParam2 = Pfsistema;
                    strSql = "Select * From DOCENPAC " + 
                             " WHERE gidenpac = '" + PidPaciente + "' AND " + 
                             "       gtipdocu = " + PTipDocu + " AND " + 
                             "       fsistema = " + Serrores.FormatFechaHMS(tempRefParam2) + " AND " + 
                             "       ientapor = '" + Pientapor + "'";
                    Pfsistema = Convert.ToString(tempRefParam2);
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(strSql, vRcCon);
                    rdoTemp = new DataSet();
                    tempAdapter_4.Fill(rdoTemp);
                    if (rdoTemp.Tables[0].Rows.Count != 0)
                    {
                        rdoTemp.Edit();
                        rdoTemp.Tables[0].Rows[0]["gusuario"] = Pusuario;
                        rdoTemp.Tables[0].Rows[0]["fsistema"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        rdoTemp.Tables[0].Rows[0]["ffirmmed"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        string tempQuery = rdoTemp.Tables[0].TableName;
                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery, "");
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_5);
                        tempAdapter_5.Update(rdoTemp, rdoTemp.Tables[0].TableName);
                    }
                    rdoTemp.Close();

                    bytFichero = (byte[]) Serrores.fPasaBinario(stFICHERO);
                    strSql = "Select * From DDOCUPDF WHERE gnombdoc='" + PNomDocumento + "' AND inumdocu=1";
                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(strSql, vRcCon);
                    rdoTemp = new DataSet();
                    tempAdapter_6.Fill(rdoTemp);
                    rdoTemp.Edit();
                    rdoTemp.Tables[0].Rows[0]["odocupdf"] = bytFichero;
                    rdoTemp.Tables[0].Rows[0]["gusuario"] = Pusuario;
                    string tempQuery_2 = rdoTemp.Tables[0].TableName;
                    SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tempQuery_2, "");
                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_7);
                    tempAdapter_7.Update(rdoTemp, rdoTemp.Tables[0].TableName);
                    rdoTemp.Close();

                }

                if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
                {
                    File.Delete(stFICHERO);
                }
            }
            catch (System.Exception excep)
            {

                if (stFICHERO.Trim() != "")
                {
                    if (FileSystem.Dir(stFICHERO, FileAttribute.Normal) != "")
                    {
                        File.Delete(stFICHERO);
                    }
                }
                RadMessageBox.Show("Error en proCapturarFirmaDigitalMedico : " + excep.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
            }
         
        }
        
        private static bool fFicheroConfiguracionLOPD(SqlConnection vRcCon, ref string CONFIGURCIONLOPD)
		{
			//--valfanu2 + valfanu3 --> Ruta del Directorio donde se encuentran los ficheros de configuracion
			//--valfanu2i2   --> Nombre del fichero de configuracion del documento LOPD
			bool result = false;
			string stConfiguracion = String.Empty;
			CONFIGURCIONLOPD = "";
			sql = "SELECT * FROM SCONSGLO WHERE gconsglo='FIRMADIG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
			RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);
			if (RrDatos.Tables[0].Rows.Count != 0)
			{
				stConfiguracion = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim() + "\\" + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2i2"]).Trim();
			}
			
            RrDatos.Close();
			if (FileSystem.Dir(stConfiguracion, FileAttribute.Normal) != "")
			{
				result = true;
				CONFIGURCIONLOPD = stConfiguracion;
			}
			else
			{
				RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
				                "No se encuentra el fichero de configuraci�n " + stConfiguracion, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
			return result;
		}
      
        private static bool fFicheroConfiguracionCompromisoPago(SqlConnection vRcCon, ref string CONFIGURCIONPAGO)
		{
			//--valfanu2 + valfanu3 --> Ruta del Directorio donde se encuentran los ficheros de configuracion
			//--valfanu2i1 --> Nombre del fichero de configuracion del compromiso de pago
			bool result = false;
			string stConfiguracion = String.Empty;
			CONFIGURCIONPAGO = "";
			sql = "SELECT * FROM SCONSGLO WHERE gconsglo='FIRMADIG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
			RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);
			if (RrDatos.Tables[0].Rows.Count != 0)
			{
				stConfiguracion = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim() + "\\" + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2i1"]).Trim();
			}
			RrDatos.Close();
			if (FileSystem.Dir(stConfiguracion, FileAttribute.Normal) != "")
			{
				result = true;
				CONFIGURCIONPAGO = stConfiguracion;
			}
			else
			{
				RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
				                "No se encuentra el fichero de configuraci�n " + stConfiguracion, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
			return result;
		}

		private static bool fFicheroConfiguracionConsentimientoPaciente(SqlConnection vRcCon, ref string CONFIGURACIONCONSENTIMIENTO_PAC)
		{
			//--valfanu2 + valfanu3 --> Ruta del Directorio donde se encuentran los ficheros de configuracion
			//--valfanu1i1   --> Nombre del fichero de configuracion del consenimiento informado del paciente
			bool result = false;
			string stConfiguracion = String.Empty;
			CONFIGURACIONCONSENTIMIENTO_PAC = "";
			sql = "SELECT * FROM SCONSGLO WHERE gconsglo='FIRMADIG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
			RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);
			if (RrDatos.Tables[0].Rows.Count != 0)
			{
				stConfiguracion = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim() + "\\" + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i1"]).Trim();
			}
			RrDatos.Close();

			if (FileSystem.Dir(stConfiguracion, FileAttribute.Normal) != "")
			{
				result = true;
				CONFIGURACIONCONSENTIMIENTO_PAC = stConfiguracion;
			}
			else
			{
				RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
				                "No se encuentra el fichero de configuraci�n " + stConfiguracion, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
			return result;
		}

		private static bool fFicheroConfiguracionConsentimientoMedico(SqlConnection vRcCon, ref string CONFIGURACIONCONSENTIMIENTO_MED)
		{
			//--valfanu2 + valfanu3 --> Ruta del Directorio donde se encuentran los ficheros de configuracion
			//--valfanu1i2 --> Nombre del fichero de configuracion del consenimiento informado del medico
			bool result = false;
			string stConfiguracion = String.Empty;
			CONFIGURACIONCONSENTIMIENTO_MED = "";
			sql = "SELECT * FROM SCONSGLO WHERE gconsglo='FIRMADIG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, vRcCon);
			RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);
			if (RrDatos.Tables[0].Rows.Count != 0)
			{
				stConfiguracion = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim() + "\\" + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i2"]).Trim();
			}
			
            RrDatos.Close();
			if (FileSystem.Dir(stConfiguracion, FileAttribute.Normal) != "")
			{
				result = true;
				CONFIGURACIONCONSENTIMIENTO_MED = stConfiguracion;
			}
			else
			{
				RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
				                "No se encuentra el fichero de configuraci�n " + stConfiguracion, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
			return result;
		}

		private static bool fSoftWareFirmaDigital(ref string PROGRAMA)
		{
			bool result = false;
			PROGRAMA = "";
			string stPrograma = Interaction.Environ("ProgramFiles") + "\\ecoSignature Tablet\\ecoSignature_Tablet.exe";
			if (FileSystem.Dir(stPrograma, FileAttribute.Normal) != "")
			{
				result = true;
				PROGRAMA = stPrograma;
			}
			else
			{
				RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
				                "No se encuentra el software " + stPrograma, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
			return result;
		}

		private static void proMatarProceso(string strNombreProceso)
		{
			object ListaProcesos = null;
			object ObjetoWMI = Interaction.GetObject ("winmgmts:", String.Empty);
			/*sdsalazar_notodo_x_3
			
            if (!Convert.IsDBNull(ObjetoWMI))
			{
				ListaProcesos = ObjetoWMI.InstancesOf("win32_process");
				foreach (object ProcesoACerrar in (IEnumerable) ListaProcesos)
				{
					if (Convert.ToString(ProcesoACerrar.Name).ToUpper() == strNombreProceso.ToUpper())
					{
						ProcesoACerrar.Terminate(0);
					}
				}
				ListaProcesos = null;
			}
            */
		}

		private static void proEsperaFinEjecucion(string strNombreProceso)
		{
			object ListaProcesos = null;
			object ObjetoWMI = null;
			bool bencotrado = false;
			do 
			{
				bencotrado = false;
				Application.DoEvents();
				ObjetoWMI = Interaction.GetObject ("winmgmts:", String.Empty);
				if (!Convert.IsDBNull(ObjetoWMI))
				{
                    /*sdsalazar_notodo_x_3
					ListaProcesos = ObjetoWMI.InstancesOf("win32_process");
					foreach (object ProcesoEnEjecucion in (IEnumerable) ListaProcesos)
					{
						//Debug.Print UCase(ProcesoEnEjecucion.Name)
						if (Convert.ToString(ProcesoEnEjecucion.Name).ToUpper() == strNombreProceso.ToUpper())
						{
							bencotrado = true;
							break;
						}
					}
					ListaProcesos = null;
					if (!bencotrado)
					{
						return;
					}
                    fin sdsalazar_notodo_x_3 */
                }
				ObjetoWMI = null;
				Pause(0.5f);
			}
			while(bencotrado);
		}

		private static void Pause(float SecsDelay)
		{
			float PrevTimer = (float) DateTime.Now.TimeOfDay.TotalSeconds;
			float TimeOut = PrevTimer + SecsDelay;

			while(PrevTimer < TimeOut)
			{
				UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(4); //-- Timer is only updated every 1/64 sec = 15.625 millisecs.
				Application.DoEvents();
				if (DateTime.Now.TimeOfDay.TotalSeconds < PrevTimer)
				{
					TimeOut -= 86400;
				} //-- pass midnight
				PrevTimer = (float) DateTime.Now.TimeOfDay.TotalSeconds;
			};
		}
        
        internal static bool fExistePDFCreator()
		{
			bool result = false;
			foreach (PrinterHelper loImpresora in PrinterHelper.Printers)
			{
				//if (PrinterHelper.Printer.DeviceName == "PDFCreator")
                    if (loImpresora.DeviceName  == "PDFCreator")
				{
					result = true;
					break;
				}
			}
			return result;
		}
        
    }
       
}