using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Morbilidad_hospitalaria
	{

		#region "Upgrade Support "
		private static Morbilidad_hospitalaria m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Morbilidad_hospitalaria DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Morbilidad_hospitalaria();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CommonDialog1Save", "CbDisquete", "cbSalida", "cbEntrada", "Frame2", "TxtTerminado", "SdcFechaIni", "SdcFechaFin", "LbTeminados", "Label1", "Label2", "Frame1", "CbImprimir", "CbCerrar", "CbPantalla"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public System.Windows.Forms.SaveFileDialog CommonDialog1Save;
		public Telerik.WinControls.UI.RadButton CbDisquete;
		public Telerik.WinControls.UI.RadRadioButton cbSalida;
		public Telerik.WinControls.UI.RadRadioButton cbEntrada;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadTextBoxControl TxtTerminado;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaFin;
		public Telerik.WinControls.UI.RadLabel LbTeminados;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.CommonDialog1Save = new System.Windows.Forms.SaveFileDialog();
            this.CbDisquete = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbSalida = new Telerik.WinControls.UI.RadRadioButton();
            this.cbEntrada = new Telerik.WinControls.UI.RadRadioButton();
            this.TxtTerminado = new Telerik.WinControls.UI.RadTextBoxControl();
            this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.LbTeminados = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.CbDisquete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEntrada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTerminado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTeminados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CbDisquete
            // 
            this.CbDisquete.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDisquete.Location = new System.Drawing.Point(10, 170);
            this.CbDisquete.Name = "CbDisquete";
            this.CbDisquete.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDisquete.Size = new System.Drawing.Size(82, 29);
            this.CbDisquete.TabIndex = 10;
            this.CbDisquete.Text = "&Generar Fichero";
            this.CbDisquete.TextWrap = true;
            this.CbDisquete.Click += new System.EventHandler(this.CbDisquete_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.TxtTerminado);
            this.Frame1.Controls.Add(this.SdcFechaIni);
            this.Frame1.Controls.Add(this.SdcFechaFin);
            this.Frame1.Controls.Add(this.LbTeminados);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.HeaderText = "Seleccione intervalo de fechas de alta";
            this.Frame1.Location = new System.Drawing.Point(7, 2);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(343, 158);
            this.Frame1.TabIndex = 3;
            this.Frame1.Text = "Seleccione intervalo de fechas de alta";
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.cbSalida);
            this.Frame2.Controls.Add(this.cbEntrada);
            this.Frame2.HeaderText = "Criterio de Ordenación";
            this.Frame2.Location = new System.Drawing.Point(14, 100);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(319, 53);
            this.Frame2.TabIndex = 11;
            this.Frame2.Text = "Criterio de Ordenación";
            // 
            // cbSalida
            // 
            this.cbSalida.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbSalida.Location = new System.Drawing.Point(162, 22);
            this.cbSalida.Name = "cbSalida";
            this.cbSalida.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbSalida.Size = new System.Drawing.Size(110, 18);
            this.cbSalida.TabIndex = 13;
            this.cbSalida.Text = "Numero de Salida";
            // 
            // cbEntrada
            // 
            this.cbEntrada.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbEntrada.Location = new System.Drawing.Point(12, 22);
            this.cbEntrada.Name = "cbEntrada";
            this.cbEntrada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbEntrada.Size = new System.Drawing.Size(118, 18);
            this.cbEntrada.TabIndex = 12;
            this.cbEntrada.Text = "Numero de entrada";
            // 
            // TxtTerminado
            // 
            this.TxtTerminado.AcceptsReturn = true;
            this.TxtTerminado.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtTerminado.Location = new System.Drawing.Point(115, 78);
            this.TxtTerminado.MaxLength = 1;
            this.TxtTerminado.Name = "TxtTerminado";
            this.TxtTerminado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtTerminado.Size = new System.Drawing.Size(22, 19);
            this.TxtTerminado.TabIndex = 9;
            this.TxtTerminado.TextChanged += new System.EventHandler(this.TxtTerminado_TextChanged);
            // 
            // SdcFechaIni
            // 
            this.SdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaIni.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaIni.Location = new System.Drawing.Point(32, 48);
            this.SdcFechaIni.Name = "SdcFechaIni";
            this.SdcFechaIni.Size = new System.Drawing.Size(105, 18);
            this.SdcFechaIni.TabIndex = 4;
            this.SdcFechaIni.TabStop = false;
            this.SdcFechaIni.Text = "22/04/16";
            this.SdcFechaIni.Value = new System.DateTime(2016, 4, 22, 14, 14, 1, 327);
            // 
            // SdcFechaFin
            // 
            this.SdcFechaFin.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaFin.Location = new System.Drawing.Point(192, 48);
            this.SdcFechaFin.Name = "SdcFechaFin";
            this.SdcFechaFin.Size = new System.Drawing.Size(105, 18);
            this.SdcFechaFin.TabIndex = 5;
            this.SdcFechaFin.TabStop = false;
            this.SdcFechaFin.Text = "22/04/16";
            this.SdcFechaFin.Value = new System.DateTime(2016, 4, 22, 14, 14, 1, 443);
            // 
            // LbTeminados
            // 
            this.LbTeminados.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTeminados.Location = new System.Drawing.Point(30, 79);
            this.LbTeminados.Name = "LbTeminados";
            this.LbTeminados.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTeminados.Size = new System.Drawing.Size(86, 18);
            this.LbTeminados.TabIndex = 8;
            this.LbTeminados.Text = "Terminados en :";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(32, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(67, 18);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "Fecha inicio:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(192, 24);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(53, 18);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "Fecha fin:";
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.Location = new System.Drawing.Point(97, 170);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(82, 29);
            this.CbImprimir.TabIndex = 2;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(273, 170);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(82, 29);
            this.CbCerrar.TabIndex = 1;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.Location = new System.Drawing.Point(185, 170);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(82, 29);
            this.CbPantalla.TabIndex = 0;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // Morbilidad_hospitalaria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(360, 210);
            this.Controls.Add(this.CbDisquete);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbPantalla);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Morbilidad_hospitalaria";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Encuesta de morbilidad hospitalaria - AGI418F1";
            this.Closed += new System.EventHandler(this.Morbilidad_hospitalaria_Closed);
            this.Load += new System.EventHandler(this.Morbilidad_hospitalaria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CbDisquete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEntrada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTerminado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTeminados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		#endregion
	}
}