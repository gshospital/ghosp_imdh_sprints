using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class MOVI_PACI_ENTI
	{

		#region "Upgrade Support "
		private static MOVI_PACI_ENTI m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static MOVI_PACI_ENTI DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new MOVI_PACI_ENTI();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "Sprcambios", "SprEpisodios", "cbCerrar", "LbNompac", "LbPaciente", "FrmPaciente", "SprEpisodios_Sheet1", "Sprcambios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread Sprcambios;
		public UpgradeHelpers.Spread.FpSpread SprEpisodios;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadTextBox LbNompac;
		public Telerik.WinControls.UI.RadLabel LbPaciente;
		public Telerik.WinControls.UI.RadGroupBox FrmPaciente;

		//private FarPoint.Win.Spread.SheetView SprEpisodios_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView Sprcambios_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MOVI_PACI_ENTI));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.Sprcambios = new UpgradeHelpers.Spread.FpSpread();
			this.SprEpisodios = new UpgradeHelpers.Spread.FpSpread();
			this.cbCerrar = new Telerik.WinControls.UI.RadButton();
			this.FrmPaciente = new Telerik.WinControls.UI.RadGroupBox();
			this.LbNompac = new Telerik.WinControls.UI.RadTextBox();
			this.LbPaciente = new Telerik.WinControls.UI.RadLabel();
			this.FrmPaciente.SuspendLayout();
			this.SuspendLayout();
			// 
			// Sprcambios
			// 
			this.Sprcambios.Location = new System.Drawing.Point(237, 56);
			this.Sprcambios.Name = "Sprcambios";
			this.Sprcambios.Size = new System.Drawing.Size(357, 201);
			this.Sprcambios.TabIndex = 4;
			// 
			// SprEpisodios
			// 
			this.SprEpisodios.Location = new System.Drawing.Point(4, 56);
			this.SprEpisodios.Name = "SprEpisodios";
			this.SprEpisodios.Size = new System.Drawing.Size(223, 201);
			this.SprEpisodios.TabIndex = 3;
			this.SprEpisodios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEpisodios_CellClick);
			// 
			// cbCerrar
			// 
			//this.cbCerrar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCerrar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCerrar.Location = new System.Drawing.Point(515, 264);
			this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;

            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCerrar.Size = new System.Drawing.Size(81, 29);
			this.cbCerrar.TabIndex = 5;
			this.cbCerrar.Text = "&Cerrar";
			this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCerrar.UseVisualStyleBackColor = false;
			this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
			// 
			// FrmPaciente
			// 
			//this.FrmPaciente.BackColor = System.Drawing.SystemColors.Control;
			this.FrmPaciente.Controls.Add(this.LbNompac);
			this.FrmPaciente.Controls.Add(this.LbPaciente);
			this.FrmPaciente.Enabled = true;
			//this.FrmPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FrmPaciente.Location = new System.Drawing.Point(3, 7);
			this.FrmPaciente.Name = "FrmPaciente";
			this.FrmPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmPaciente.Size = new System.Drawing.Size(594, 42);
			this.FrmPaciente.TabIndex = 0;
			this.FrmPaciente.Visible = true;
			// 
			// LbNompac
			// 
			//this.LbNompac.BackColor = System.Drawing.SystemColors.Control;
			//this.LbNompac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.LbNompac.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbNompac.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbNompac.Location = new System.Drawing.Point(79, 15);
			this.LbNompac.Name = "LbNompac";
			this.LbNompac.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbNompac.Size = new System.Drawing.Size(499, 18);
			this.LbNompac.TabIndex = 2;
            this.LbNompac.Enabled = false;
            // 
            // LbPaciente
            // 
            //this.LbPaciente.BackColor = System.Drawing.SystemColors.Control;
            //this.LbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbPaciente.Location = new System.Drawing.Point(5, 15);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(59, 19);
			this.LbPaciente.TabIndex = 1;
			this.LbPaciente.Text = "Paciente:";
			// 
			// MOVI_PACI_ENTI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(598, 295);
			this.Controls.Add(this.Sprcambios);
			this.Controls.Add(this.SprEpisodios);
			this.Controls.Add(this.cbCerrar);
			this.Controls.Add(this.FrmPaciente);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(21, 13);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MOVI_PACI_ENTI";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Consulta de cambios de entidades por episodios AGI419F1";
			this.Activated += new System.EventHandler(this.MOVI_PACI_ENTI_Activated);
			this.Closed += new System.EventHandler(this.MOVI_PACI_ENTI_Closed);
			this.Load += new System.EventHandler(this.MOVI_PACI_ENTI_Load);
			this.FrmPaciente.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			//this.Show();
		}
		#endregion
	}
}