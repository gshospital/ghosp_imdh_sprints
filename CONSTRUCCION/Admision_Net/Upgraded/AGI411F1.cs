using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace ADMISION
{
	public partial class general_movimiento
		: Telerik.WinControls.UI.RadForm
	{
		public general_movimiento()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}
		private void general_movimiento_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		string stMensaje3 = String.Empty;
		string stTipo = String.Empty;
		//Dim PathString As String
		string stSeleccion = String.Empty;
		string stIntervalo = String.Empty;
		string stSql = String.Empty;
		string stFllegadaI = String.Empty;
		string stFllegadaF = String.Empty;
	    Crystal CrystalReport1 = new Crystal();
		int viform = 0;
		bool vstcarga_combo = false;

		bool bGestionAutorizaciones = false; //OSCAR C ENE 2005
        DataSet Dbt1 = new DataSet();

        public void proImprimir(Crystal.DestinationConstants Parasalida)
        {
            DataSet Rr = null;
            string sql = String.Empty;
            try
            {
                if (SdcFecha.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                {
                    stMensaje1 = "Fecha Ingreso/Alta";
                    stMensaje2 = "menor o igual";
                    stMensaje3 = "Fecha del Sistema";
                    
                    short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { stMensaje1, stMensaje2, stMensaje3 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    SdcFecha.Focus();
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                //    PathString = App.Path   ' Cojo la ruta de acceso para buscar el informe
                // FORMATEO DE LA FECHA, para mandar en la consulta SQL un rango de fecha, en
                // nuestro caso el dia entero
                stFllegadaI = (SdcFecha.Text + " 00:00:00");
                stFllegadaF = (SdcFecha.Text + " 23:59:59");
                // **************************************************************************


                //OSCAR C ENE 2005
                bGestionAutorizaciones = false;
                sql = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='CTRLAUTO'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                Rr = new DataSet();
                tempAdapter.Fill(Rr);
                if (Rr.Tables[0].Rows.Count != 0)
                {                    
                    if (Convert.ToString(Rr.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
                    {
                        bGestionAutorizaciones = true;
                    }
                }
                //------------

                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                
                if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                {
                    Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                }
                
                if (chbIngreso.Checked)
                {
                    stSeleccion = "Tipo de Movimiento: Ingreso";
                    stIntervalo = "Fecha de Ingreso: " + SdcFecha.Text;
                    //Se mueve esta linea arriba porque se debe definir antes de la asignacion de formulas
                    CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi411r1_CR11.rpt";

                    //*******************
                    CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                    CrystalReport1.WindowTitle = "Listado General de Movimientos";
                    CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    //   CrystalReport1.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""
                    CrystalReport1.Destination = Parasalida;
                    CrystalReport1.WindowShowPrintSetupBtn = true;

                    CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
                    CrystalReport1.Formulas["{@SELECCION}"] = "\"" + stSeleccion + "\"";

                    if (Serrores.ExisteAseguradora(Conexion_Base_datos.RcAdmision))
                    {
                        CrystalReport1.Formulas["{@IASEGURA}"] = "'S'";

                    }
                    else
                    {
                        CrystalReport1.Formulas["{@IASEGURA}"] = "'N'";
                    }

                    proConsulta("I");

                    //OSCAR C ENE 2005
                    //CrystalReport1.SQLQuery = stsql & Chr(13) & Chr(10) & "order by AEPISADM.FLLEGADA "
                    if (bGestionAutorizaciones)
                    {
                        CrystalReport1.Formulas["{@VerAutoriz}"] = "'S'";
                    }
                    else
                    {
                        CrystalReport1.Formulas["{@VerAutoriz}"] = "'N'";
                    }

                    ProRellenaTablaTemp(Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, "I");

                    //CrystalReport1.set_DataFiles(0, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo);
                    CrystalReport1.DataFiles[0] = Dbt1.Tables["CR11_Agi411r1"];

                    CrystalReport1.SQLQuery = stSql;
                    CrystalReport1.Action = 1;
                    CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                    //----------

                }
                if (chbAlta.Checked)
                {
                    stSeleccion = "Tipo de Movimiento: Alta";
                    stIntervalo = "Fecha de Alta: " + SdcFecha.Text;
                    CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi411r2_CR11.rpt";
                    CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
                    CrystalReport1.Formulas["{@SELECCION}"] = "\"" + stSeleccion + "\"";

                    //*******************
                    CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                    CrystalReport1.WindowTitle = "Listado General de Movimientos";
                    CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    //   CrystalReport1.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""
                    CrystalReport1.Destination = Parasalida;
                    CrystalReport1.WindowShowPrintSetupBtn = true;

                    if (Serrores.ExisteAseguradora(Conexion_Base_datos.RcAdmision))
                    {
                        CrystalReport1.Formulas["{@IASEGURA}"] = "'S'";
                    }
                    else
                    {
                        CrystalReport1.Formulas["{@IASEGURA}"] = "'N'";
                    }

                    proConsulta("A");

                    //OSCAR C ENE 2005
                    //CrystalReport1.SQLQuery = stsql & Chr(13) & Chr(10) & "order by AEPISADM.FALTPLAN "
                    if (bGestionAutorizaciones)
                    {
                        CrystalReport1.Formulas["{@VerAutoriz}"] = "'S'";
                    }
                    else
                    {
                        CrystalReport1.Formulas["{@VerAutoriz}"] = "'N'";
                    }
                    ProRellenaTablaTemp(Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo, "A");
                    //CrystalReport1.set_DataFiles(0, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Conexion_Base_datos.BDtempo);
                    CrystalReport1.DataFiles[0] = Dbt1.Tables["CR11_Agi411r2"];
                    CrystalReport1.SQLQuery = stSql;
                    CrystalReport1.Action = 1;
                    CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                    //----------

                }

                CrystalReport1.Reset();
                CbPantalla.Enabled = false;
                CbImprimir.Enabled = false;
                
                chbIngreso.Checked = false;
                
                chbAlta.Checked = false;
                SdcFecha.Text = DateTime.Today.ToString("dd/MM/yyyy");
                if (!FrmHospital.Visible)
                {
                    ChBHosDia.CheckState = CheckState.Checked;
                    ChBHosGeneral.CheckState = CheckState.Checked;
                }
                else
                {
                    ChBHosDia.CheckState = CheckState.Unchecked;
                    ChBHosGeneral.CheckState = CheckState.Unchecked;
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {                
                if (Information.Err().Number == 32755)
                {
                    // cacelada la impresion
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "general_movimiento:proimprimir", e);
                    this.Cursor = Cursors.Default;
                }
            }
        }
        private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

        private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToPrinter);
        }


        private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
        {
            proImprimir(Crystal.DestinationConstants.crptToWindow);
        }

        private void chbAlta_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
           proAceptar();
		}

		private void ChBHosDia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}

		private void ChBHosGeneral_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}

		private void chbIngreso_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proAceptar();
		}
        		
		private void general_movimiento_Load(Object eventSender, EventArgs eventArgs)
		{
			bool MostrarHospitales = false;
			//mostrar o no el tipo de ingreso
			string stsqltemp = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='VERCAMAS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{				
				if (Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
				{
					MostrarHospitales = true;
				}
			}			
			rrtemp.Close();

			if (!MostrarHospitales)
			{
				this.Height = 230;
				this.Width = 286;
				Frame1.Height = 156;
				Frame1.Width = 273;
				FrmHospital.Visible = false;
				CbImprimir.Top = 171;
				CbPantalla.Top = 171;
				this.CbCerrar.Top = 171;
				this.ChBHosDia.CheckState = CheckState.Checked;
				this.ChBHosGeneral.CheckState = CheckState.Checked;
			}
			//cambiar los literales en el tipo de ingreso
			stsqltemp = "SELECT VALFANU1,VALFANU2 FROM SCONSGLO WHERE GCONSGLO='DETIPING'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
			rrtemp = new DataSet();
			tempAdapter_2.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{				
				ChBHosGeneral.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();				
				ChBHosDia.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2"]).Trim();
			}			
			rrtemp.Close();

            
			CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;
			CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
			CbPantalla.Enabled = false;
			CbImprimir.Enabled = false;
			stTipo = "";
			vstcarga_combo = false;
		}


		public void proAceptar()
		{
			bool bValor = (((Convert.ToBoolean(chbAlta.Checked == true || Convert.ToBoolean(chbIngreso.Checked)) ? -1 : 0) & (((int) this.ChBHosDia.CheckState) | ((int) this.ChBHosGeneral.CheckState))) != 0);
			CbPantalla.Enabled = bValor;
			CbImprimir.Enabled = bValor;
		}

		public void proConsulta(string tipo)
		{
			string[] MatrizSql = null;
			DLLTextosSql.TextosSql oTextoSql = null;
			try
			{

				switch(tipo)
				{
					case "I" : 
						MatrizSql = new string[]{String.Empty, String.Empty, String.Empty}; 	
                        					
						object tempRefParam = stFllegadaI; 
						MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam); 
						stFllegadaI = Convert.ToString(tempRefParam); 
                        						
						object tempRefParam2 = stFllegadaF; 
						MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam2); 
						stFllegadaF = Convert.ToString(tempRefParam2); 
						oTextoSql = new DLLTextosSql.TextosSql(); 
						
						string tempRefParam3 = "ADMISION"; 
						string tempRefParam4 = "AGI411F1"; 
						string tempRefParam5 = "proConsulta"; 
						short tempRefParam6 = 1; 
						stSql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, MatrizSql, Conexion_Base_datos.RcAdmision)); 
						oTextoSql = null; 
						break;
					case "A" : 
						MatrizSql = new string[]{String.Empty, String.Empty, String.Empty}; 
						
						object tempRefParam8 = stFllegadaI; 
						MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam8); 
						stFllegadaI = Convert.ToString(tempRefParam8); 
						
						object tempRefParam9 = stFllegadaF; 
						MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam9); 
						stFllegadaF = Convert.ToString(tempRefParam9); 
						oTextoSql = new DLLTextosSql.TextosSql(); 
						
						string tempRefParam10 = "ADMISION"; 
						string tempRefParam11 = "AGI411F1"; 
						string tempRefParam12 = "proConsulta"; 
						short tempRefParam13 = 2; 
						stSql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam10, tempRefParam11, tempRefParam12, tempRefParam13, MatrizSql, Conexion_Base_datos.RcAdmision)); 
						oTextoSql = null; 
						break;
				}

				if (this.ChBHosDia.CheckState == CheckState.Checked && this.ChBHosGeneral.CheckState == CheckState.Unchecked)
				{
					stSql = stSql + " and AEPISADM.ganoadme < 1900";
				}
				if (this.ChBHosDia.CheckState == CheckState.Unchecked && this.ChBHosGeneral.CheckState == CheckState.Checked)
				{
					stSql = stSql + " and AEPISADM.ganoadme > 1900";
				}
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "general_movimiento:proconsulta", ex);
			}


		}

		//OSCAR C ENE 2005
		private void ProRellenaTablaTemp(string paraBd, string paratipo)
		{
			object dbLangSpanish = null;
			object DBEngine = null;
						
			DataSet Rr = null;
			string stNombTabla = String.Empty;
            DataRow rrTabla = null;
            switch (paratipo)
			{
				case "I" :
                    //stNombTabla = "LIST_GRAL_MOV_ING"; 
                    stNombTabla = "CR11_Agi411r1";
                    break;
				case "A" :
                    //stNombTabla = "LIST_GRAL_MOV_ALT"; 
                    stNombTabla = "CR11_Agi411r2";
                    break;
			}

            //SI NO EXISTE LA BASE DE DATOS CREARLA
            /*bool bExiste_base = false;
						
            //Workspace Wk1 = (Workspace) DBEngine.Workspaces(0);
			if (FileSystem.Dir(paraBd, FileAttribute.Normal) == "")
			{
                //camaya todo_x_4
                //Dbt1 = (Database) Wk1.CreateDatabase(paraBd, dbLangSpanish);
			}
			else
			{
                //SI EXISTE LA TABLA BORRAR
                //camaya todo_x_4
                //Dbt1 = (Database) Wk1.OpenDatabase(paraBd);
				//foreach (TableDef dTabla in Dbt1.TableDefs)
				//{
				//	if (Convert.ToString(dTabla.Name) == stNombTabla)
				//	{
				//		bExiste_base = true;
				//		break;
				//	}
				//}
				if (bExiste_base)
				{
                    //camaya todo_x_4
                    //Dbt1.Execute("drop table " + stNombTabla);
				}
			}

			//crear tabla
			string sql = "CREATE TABLE " + stNombTabla + 
			             " (HORA CHAR(6)," + 
			             " NOMBREPAC CHAR(100), NASEGURADO CHAR(25)," + 
			             " HISTORIA CHAR(10), DIAGNOSTICO TEXT, " + 
			             " CAMA CHAR(10), SERVICIO CHAR(60), MEDICO CHAR(100), " + 
			             " ENTIDAD CHAR(50), INSPECCION CHAR(50)," + 
			             " ESTADO_AUTORIZACION CHAR(40)";

			if (paratipo == "I")
			{
				sql = sql + ", TIPOINGRESO CHAR(1), I INTEGER, P INTEGER, U INTEGER";
			}
			sql = sql + ");";
                        
            Dbt1.Execute(sql);*/

            Dbt1.CreateTable(stNombTabla, Conexion_Base_datos.PathString + "\\XSD\\" + stNombTabla + ".xsd");
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			//sql = "SELECT * FROM " + stNombTabla + " WHERE 2=1";
            //camaya todo_x_4
            //Recordset rrTabla =  Dbt1.OpenRecordset(sql);

			foreach (DataRow iteration_row in rrtemp.Tables[0].Rows)
			{
                //rrTabla.AddNew();
                rrTabla = Dbt1.Tables[stNombTabla].NewRow();

                rrTabla["hora"] = Convert.ToDateTime(iteration_row["fecha"]).ToString("HH:mm");
                rrTabla["NombrePAc"] = (Convert.ToString(iteration_row["NombrePAc"]) + "");
                rrTabla["nasegurado"] = (Convert.ToString(iteration_row["nafiliac"]) + "");
                rrTabla["Historia"] = (Convert.ToString(iteration_row["ghistoria"]).Trim() + "");
                if (Convert.IsDBNull(iteration_row["gdiag"]))
                {
                    rrTabla["diagnostico"] = (Convert.ToString(iteration_row["odiag"]) + "");
                }
                else
                {
                    rrTabla["diagnostico"] = (Convert.ToString(iteration_row["dnombdia"]) + "");
                }
                rrTabla["Cama"] = (StringsHelper.Format(iteration_row["gplan"], "00") + StringsHelper.Format(iteration_row["ghabi"], "00") + Convert.ToString(iteration_row["gcama"]));
                rrTabla["servicio"] = (Convert.ToString(iteration_row["dnomserv"]) + "");
                rrTabla["MEDICO"] = (Convert.ToString(iteration_row["NombreMedico"]) + "");
                rrTabla["entidad"] = (Convert.ToString(iteration_row["DSOCIEDA"]) + "");
                rrTabla["inspeccion"] = (Convert.ToString(iteration_row["DINSPECC"]) + "");
                if (paratipo == "I")
                {
                    rrTabla["TIPOINGRESO"] = (Convert.ToString(iteration_row["itipingr"]) + "");
                    switch (Convert.ToString(iteration_row["itipingr"]))
                    {
                        case "I":
                            rrTabla["i"] = 1;
                            break;
                        case "P":
                            rrTabla["p"] = 1;
                            break;
                        case "U":
                            rrTabla["u"] = 1;
                            break;
                    }
                }
                if (bGestionAutorizaciones)
                {
                    //Autorizacion
                   string sql = " select gestsoli from " +
                          " ifmsfa_activtau " +
                          " inner join ifmsfa_autoriza on ifmsfa_activtau.ganoauto = ifmsfa_autoriza.ganoauto and " +
                          " ifmsfa_activtau.gnumauto = ifmsfa_autoriza.gnumauto " +
                          " WHERE itipacti='R' AND itiposer='H' AND " +
                          "       ganoregi=" + Convert.ToString(iteration_row["Ganoadme"]) + " AND gnumregi=" + Convert.ToString(iteration_row["Gnumadme"]);
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    Rr = new DataSet();
                    tempAdapter_2.Fill(Rr);
                    if (Rr.Tables[0].Rows.Count != 0)
                    {
                        rrTabla["estado_autorizacion"] = (Convert.ToString(Rr.Tables[0].Rows[0]["gestsoli"]) + "");
                    }
                    Rr.Close();
                }
                //rrTabla.Update();
                Dbt1.Tables[stNombTabla].Rows.Add(rrTabla);
            }
            rrtemp.Close();            
            rrTabla = null;
            //Dbt1.Close();

            stSql = "SELECT * FROM " + stNombTabla + " ORDER BY HORA";

        }
		
		private void general_movimiento_Closed(Object eventSender, EventArgs eventArgs)
        {
            MemoryHelper.ReleaseMemory();
        }
    }
}