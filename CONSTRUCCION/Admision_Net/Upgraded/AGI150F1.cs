using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using CrystalWrapper;


namespace ADMISION
{
    public partial class Documentos_ingreso
        : RadForm
    {
        string sqlCrystal = String.Empty; //sql del crystal

        private Crystal cryInformes = null;
        private Crystal.DestinationConstants Parasalida = (Crystal.DestinationConstants)0;
        private Crystal.DestinationConstants ParaEtiquetas = (Crystal.DestinationConstants)0;


        string stTGidenpac = String.Empty; //gidenpac del paciente
        string stInforme = String.Empty;
        string stNPaciente = String.Empty;
        string vstHistoria = String.Empty; //n� de historia del paciente
        string vstSociedad = String.Empty; //sociedad

        int viGanoadme = 0; //ganoadme
        int viGnumadme = 0; //Maximo movimiento del paciente

        string GrupoHo = String.Empty; //Nombre y direcci�n Hospital
        string NombreHo = String.Empty;
        string DireccHo = String.Empty;

        DataSet Rr1 = null; //Resulset de ayuda para rellenar formulas
        DataSet Rr2 = null;
        DataSet Rr3 = null;
        DataSet Rr4 = null;
        DataSet Dbt1 = null;

        string sql1 = String.Empty; //sql de los resulset
        string sql2 = String.Empty;
        string sql3 = String.Empty;
        string sql4 = String.Empty;
        int viform = 0; //recojo las formulas que esten usandose y las pongo a Blancos

        object tiCopias = null; //numero de copias de las etiquetas devuelto por el ComDialog
        PrinterHelper vPrinter = null;
        string tstPrinter = String.Empty;
        public Documentos_ingreso()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            isInitializingComponent = true;
            InitializeComponent();
            isInitializingComponent = false;
            ReLoadForm(false);
        }

        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
        {
            Crystal LISTADO_PARTICULAR = null;

            string stOperacion = String.Empty;
            string stSql = String.Empty;
            DataSet rrContrato = null;
            DataSet rsTemp = null;
            bool bAcceso = false;
            object Clase = null;
            string StJustificanteIngreso = String.Empty;

            try
            {
                bool bFirmaDigital = false;
                bFirmaDigital = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FIRMADIG", "VALFANU1") == "S";

                string stsqltemp = String.Empty;
                DataSet rrtemp = null;

                StJustificanteIngreso = "I";

                stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'IMPJUSIN' ";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    StJustificanteIngreso = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
                }

                rrtemp.Close();

                //(maplaza)(18/03/2008)Necesario, por si el objeto Crystal se ha quedado (desde la �ltima impresi�n) con las dimensiones o medidas
                //que caracterizan a alguna de las impresoras con las que se imprimen etiquetas.

                cryInformes.Reset();
                //---------

                bAcceso = false;

                Conexion_Base_datos.Cuadro_Diag_imprePrint = menu_admision.DefInstance.CommDiag_imprePrint;
                //'' Esta propiedad dice si se deja como predeterminada la impresora seleccionada
                //'  Cuadro_Diag_impre.PrinterDefault = true/False
                //ChbJustificante.Value = 1 Or
                if (chbDatos.CheckState == CheckState.Checked || chbVolante.CheckState == CheckState.Checked || chbIngreso.CheckState == CheckState.Checked || Chbreligioso.CheckState == CheckState.Checked || Chbtrabajador.CheckState == CheckState.Checked || (ChbHaceconstar.CheckState == CheckState.Checked && (!bFirmaDigital || !bsFirmaDigitalizada.fExistePDFCreator())) || ChbAusencia.CheckState == CheckState.Checked || chbIntervencion.CheckState == CheckState.Checked || ChbOficio.CheckState == CheckState.Checked || (chbHojaClinEsta.Visible && chbHojaClinEsta.CheckState == CheckState.Checked) || (chbAutoDatos.CheckState == CheckState.Checked && !bFirmaDigital) || chbAutoEstudio.CheckState == CheckState.Checked || chbPa�ales.CheckState == CheckState.Checked || ChbInterJuzga.CheckState == CheckState.Checked || chbDuplicidad.CheckState == CheckState.Checked || ChbAutoEmision.CheckState == CheckState.Checked)
                {
                    //ChbJusAcompa.Value = 1 Or chbIntervencion.Value = 1 Or (chbHojaClinEsta.Visible And chbHojaClinEsta.Value = 1) Then                    

                    Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
                    Conexion_Base_datos.Cuadro_Diag_imprePrint.ShowDialog();
                }
                if (ChbJustificante.CheckState == CheckState.Checked && StJustificanteIngreso == "I")
                {
                    Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
                    Conexion_Base_datos.Cuadro_Diag_imprePrint.ShowDialog();
                }

                this.Cursor = Cursors.WaitCursor;
                //****************elegir informes*****************


                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";

                if (ChbJustificante.CheckState == CheckState.Checked)
                { //imprime justificante de asistencia
                  //Comprobar si se ha de autorizar a imprimir el justificante del SERMAS
                    stSql = "select valfanu1 from SCONSGLO where gconsglo ='JUSERMAS' and valfanu1='S'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    rrContrato = new DataSet();
                    tempAdapter_2.Fill(rrContrato);
                    if (rrContrato.Tables[0].Rows.Count == 1)
                    {
                        //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                        /*Clase = new DocumentoDLL.mcDocumento();						
						Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
						if (StJustificanteIngreso == "I")
						{
							Clase.proJustificanteSERMAS(stTGidenpac, "H", viGanoadme, viGnumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "P", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToPrinter);
						}
						else
						{
							Clase.proJustificanteSERMAS(stTGidenpac, "H", viGanoadme, viGnumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "P", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToWindow);
						}
						Clase = null;*/
                    }
                    else
                    {
                        if (StJustificanteIngreso == "I")
                        {
                            Conexion_Base_datos.proJustificante(cryInformes, Crystal.DestinationConstants.crptToPrinter, this, viGnumadme, viGanoadme, stTGidenpac, Conexion_Base_datos.Cuadro_Diag_imprePrint);
                        }
                        else
                        {
                            Conexion_Base_datos.proJustificante(cryInformes, Crystal.DestinationConstants.crptToWindow, this, viGnumadme, viGanoadme, stTGidenpac, Conexion_Base_datos.Cuadro_Diag_imprePrint);
                        }
                    }
                    rrContrato = null;
                }

                if (ChbJusAcompa.CheckState == CheckState.Checked)
                {

                    if (StJustificanteIngreso == "I")
                    {
                        Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
                        Conexion_Base_datos.Cuadro_Diag_imprePrint.ShowDialog();
                    }

                    //Comprobar si se ha de autorizar a imprimir el justificante del SERMAS
                    stSql = "select valfanu1 from SCONSGLO where gconsglo ='JUSERMAS' and valfanu1='S'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    rrContrato = new DataSet();
                    tempAdapter_3.Fill(rrContrato);
                    if (rrContrato.Tables[0].Rows.Count == 1)
                    {
                        //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                        /*Clase = new DocumentoDLL.mcDocumento();
						Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
						if (StJustificanteIngreso == "I")
						{
							Clase.proJustificanteSERMAS(stTGidenpac, "H", viGanoadme, viGnumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "A", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToPrinter);
						}
						else
						{
							Clase.proJustificanteSERMAS(stTGidenpac, "H", viGanoadme, viGnumadme, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies, "A", this, Conexion_Base_datos.VstCrystal, Crystal.DestinationConstants.crptToWindow);
						}
						Clase = null;*/
                    }
                    else
                    {
                        if (StJustificanteIngreso == "I")
                        {
                            Conexion_Base_datos.proAcompa(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter, viGanoadme, viGnumadme, cryInformes, this);
                        }
                        else
                        {
                            Conexion_Base_datos.proAcompa(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToWindow, viGanoadme, viGnumadme, cryInformes, this);
                        }
                    }
                    rrContrato = null;
                }

                if (chbDatos.CheckState == CheckState.Checked)
                { //imprime hoja de datos
                    proDatos(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (chbVolante.CheckState == CheckState.Checked)
                { //imprime Volante Sociedad
                  //llama a DocumentoDll
                    bAcceso = true;
                    proVolante(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (chbIngreso.CheckState == CheckState.Checked)
                { //imprime hoja de ingreso
                  //llama a DocumentoDll
                    bAcceso = true;
                    proIngreso(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (Chbreligioso.CheckState == CheckState.Checked)
                { //imprime hoja para el servicio religioso
                    proSerReligioso(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (Chbtrabajador.CheckState == CheckState.Checked)
                { //imprime hoja para el trabajador social
                    proTrabajador(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (ChbHaceconstar.CheckState == CheckState.Checked)
                { //imprime hoja de compromiso de pago(hace constar)
                    proConstar(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (ChbAusencia.CheckState == CheckState.Checked)
                { //imprime hoja de ausencia temporal
                    proAusencia(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (ChbOficio.CheckState == CheckState.Checked)
                {
                    proIOficio(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                if (chbIntervencion.CheckState == CheckState.Checked)
                { //imprime consentimiento
                  //llama a documentoDll
                    proIntervencion(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                }
                //lo ultimo que sacamos son las etiquetas
                LISTADO_PARTICULAR = Conexion_Base_datos.LISTADO_CRYSTAL;
                if (chbEtiqueta.CheckState == CheckState.Checked)
                {
                    //DIEGO - 30/03/2007 - No muestro la seleccion de impresora para Leon
                    //    If ObternerValor_CTEGLOBAL(RcAdmision, "TIPOETIQ", "VALFANU2") <> "S" Then 'nuevas etiquetas de Le�n
                    if (!Option1[5].IsChecked)
                    {
                        Seleccion_Impresoras.DefInstance.ShowDialog();

                        if (Conexion_Base_datos.VfCancelPrint)
                        {
                            chbEtiqueta.CheckState = CheckState.Unchecked;
                        }
                    }
                    //    Else
                    //        'DIEGO - 25/04/2007 - obtengo el n�mero de copias
                    //        stSql = "select nnumeri1 from sconsglo where gconsglo='nroetiad'"
                    //        Set rsTemp = RcAdmision.OpenResultset(stSql, 1, 1)
                    //        ViNumCopias = rsTemp("nnumeri1")
                    //        rsTemp.Close
                    //    End If
                }

                string stNumEtiquetas = String.Empty;
                if (chbEtiqueta.CheckState == CheckState.Checked)
                { //imprime etiquetas
                  //llama a DocumentoDll
                  //ViNumCopias es la variable que se llena en la pantalla seleccion_impresoras
                  //Impres1.CopiasDetalle = ViNumCopias
                  //proEtiquetas Impres1

                    //(maplaza)(04/01/2008)
                    if (Option1[4].IsChecked)
                    {
                        stNumEtiquetas = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "IMETAUAD", "NNUMERI1");
                        //La impresi�n de etiquetas de identificaci�n de pacientes se ha decidido realizarla con el objeto Printer.
                        //If (Trim(stNumEtiquetas) <> "") Then
                        //    LISTADO_PARTICULAR.DetailCopies = CInt(stNumEtiquetas)
                        //Else
                        //    'si no se encuentra el registro
                        //    LISTADO_PARTICULAR.DetailCopies = 1
                        //End If

                        if (stNumEtiquetas.Trim() != "")
                        {
                            proEtiquetas(LISTADO_PARTICULAR, Convert.ToInt32(Double.Parse(stNumEtiquetas)));
                        }
                    }
                    else
                    {
                        //-------
                        if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "TIPOETIQ", "VALFANU2") != "S")
                        { //nuevas etiquetas de Le�n
                            Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
                        }
                        LISTADO_PARTICULAR.DetailCopies = (short) Conexion_Base_datos.ViNumCopias;
                        proEtiquetas(LISTADO_PARTICULAR);
                    }
                }

                LISTADO_PARTICULAR = null;

                if (chbHojaClinEsta.Visible)
                {
                    if (chbHojaClinEsta.CheckState == CheckState.Checked)
                    {
                        Conexion_Base_datos.proHojaClinEsta(Conexion_Base_datos.Cuadro_Diag_imprePrint, this, cryInformes, chbHojaClinEsta, viGanoadme, viGnumadme, stTGidenpac);
                    }
                }

                //************* O.Frias - 29/09/2008  *************
                //NewProceso -- Aportaci�n Residente Pension
                if (chbDeposito.Visible)
                {
                    if (chbDeposito.CheckState == CheckState.Checked)
                    {
                        proDeposito();
                    }
                }

                if (chbFactSomb.Visible)
                {
                    if (chbFactSomb.CheckState == CheckState.Checked)
                    {
                        proFactSomb(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                    }
                }

                if (chbContrato.CheckState == CheckState.Checked)
                {
                    // Han seleccionado imprimir el contrato
                    //obtener nombre operacion
                    stSql = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'contrato' ";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    rrContrato = new DataSet();
                    tempAdapter_4.Fill(rrContrato);
                    if (rrContrato.Tables[0].Rows.Count != 0)
                    {
                        stOperacion = Convert.ToString(rrContrato.Tables[0].Rows[0]["valfanu2"]).Trim();
                    }
                    rrContrato.Close();
                    proContrato(stOperacion);
                }

                //Set Cuadro_Diag_impre = Nothing

                if (chbAutoDatos.CheckState == CheckState.Checked)
                {
                    proLOPD(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                    chbAutoDatos.CheckState = CheckState.Unchecked;
                }

                int loDestination = 0;
                int loCopiesToPrinter = 0;
                if (chbContratoIngreso.CheckState == CheckState.Checked)
                {
                    stsqltemp = "SELECT VALFANU2, VALFANU3 FROM SCONSGLO WHERE gconsglo='CONINGRE'";
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_5.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count != 0)
                    {
                        if ((Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU2"]) + "").Trim() == "I")
                        {
                            //            Cuadro_Diag_impre.CancelError = True
                            //            Cuadro_Diag_impre.Copies = 1
                            //            Cuadro_Diag_impre.ShowPrinter
                            loDestination = (int)Crystal.DestinationConstants.crptToPrinter;
                        }
                        else
                        {
                            loDestination = (int)Crystal.DestinationConstants.crptToWindow;
                        }
                        if (Conversion.Val(Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU3"]) + "") > 0)
                        {
                            loCopiesToPrinter = Convert.ToInt32(rrtemp.Tables[0].Rows[0]["VALFANU3"]);
                        }
                        else
                        {
                            loCopiesToPrinter = 1;
                        }
                    }
                    rrtemp.Close();
                    proContratoIngreso(Conexion_Base_datos.Cuadro_Diag_imprePrint, loDestination, loCopiesToPrinter);
                    chbContratoIngreso.CheckState = CheckState.Unchecked;
                }

                //oscar 15/03/04
                if (chbAutoEstudio.CheckState == CheckState.Checked)
                {
                    proEstudioClinico(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                    chbAutoEstudio.CheckState = CheckState.Unchecked;
                }
                if (chbPa�ales.CheckState == CheckState.Checked)
                {
                    proImprimirPa�ales(Conexion_Base_datos.Cuadro_Diag_imprePrint, Crystal.DestinationConstants.crptToPrinter);
                    chbPa�ales.CheckState = CheckState.Unchecked;
                }

                //16/03/04
                Notificaciones.classNotificaciones claseNotificaciones = null;
                string PathReport = String.Empty;
                PathReport = Conexion_Base_datos.PathString + "\\rpt";
                if (chbDuplicidad.CheckState == CheckState.Checked || ChbInterJuzga.CheckState == CheckState.Checked)
                {
                    claseNotificaciones = new Notificaciones.classNotificaciones();
                    claseNotificaciones.RecogerParametrosComunes(this, Conexion_Base_datos.RcAdmision, cryInformes, viGnumadme.ToString(), viGanoadme.ToString(), stTGidenpac, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, Path.GetDirectoryName(Application.ExecutablePath), PathReport, Conexion_Base_datos.VstCrystal, Conexion_Base_datos.Cuadro_Diag_imprePrint);

                    if (ChbInterJuzga.CheckState == CheckState.Checked)
                    {
                        claseNotificaciones.proImprimirInternamientoJudicial();
                        cryInformes.Reset();
                    }
                    if (chbDuplicidad.CheckState == CheckState.Checked)
                    {
                        claseNotificaciones.proImprimirDuplicidadExpediente();
                        cryInformes.Reset();
                    }

                    claseNotificaciones = null;
                }
                //----------

                //************* O.Frias - 29/09/2008  *************
                //NewProceso -- Aportaci�n Residente Pension
                if (chkAportaPension.Visible)
                {
                    if (chkAportaPension.CheckState == CheckState.Checked)
                    {
                        proAportaciones();
                    }
                }

                PersonasNoImpJust.MCPersonasNoImpJust objAutoJust = null;
                if (ChbAutoEmision.CheckState == CheckState.Checked)
                {
                    //comprobamos si se imprimen una detr�s de otra o en columna, las persona a las que no se les puede imprimir justificantes.
                    stSql = "select * from SCONSGLO where gconsglo ='JUSERMAS' and nnumeri1=1";
                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                    rrContrato = new DataSet();
                    tempAdapter_6.Fill(rrContrato);
                    Conexion.Obtener_conexion ClaseConexion = new Conexion.Obtener_conexion();

                    objAutoJust = new PersonasNoImpJust.MCPersonasNoImpJust();
                    objAutoJust.ImprimirConsentimiento(Conexion_Base_datos.RcAdmision, "H", viGanoadme, viGnumadme, stTGidenpac, Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Convert.ToString(ClaseConexion.NonMaquina()) + "Admision.mdb", Conexion_Base_datos.VstCrystal, Conexion_Base_datos.stNombreDsnACCESS, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Conexion_Base_datos.gUsuario, Conexion_Base_datos.gContrase�a, rrContrato.Tables[0].Rows.Count == 1, Conexion_Base_datos.Cuadro_Diag_imprePrint.PrinterSettings.Copies);
                    objAutoJust = null;
                    ClaseConexion = null;
                }

                if (stTGidenpac.Trim() != "" && bAcceso)
                {
                    Serrores.GrabarDCONSUHC(stTGidenpac, "S", Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision);
                }

                this.Cursor = Cursors.Default;
                proCheckBox();
                //Me.Show
            }
            catch (SqlException e)
            {

                this.Cursor = Cursors.Default;
                //cancela la impresi�n de etiquetas
                if (e.Number == 32755)
                {
                    proCheckBox();
                }
                else
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "Aceptar:DocumentosIngreso", e);

                }
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }


        public void proFactSomb(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            DataSet rrFactSomb1 = null;
            string stPoblacion = String.Empty;
            string vstGidenpac = String.Empty;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;

            //    Dim MaysculaOMinuscula As String
            string stSql = "Select gidenpac from AEPISADM Where Ganoadme = " + viGanoadme.ToString() + " And Gnumadme =  " + viGnumadme.ToString() + "";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            DataSet rrFactSomb = new DataSet();
            tempAdapter.Fill(rrFactSomb);
            if (rrFactSomb.Tables[0].Rows.Count != 0)
            {
                vstGidenpac = Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gidenpac"]);
            }

            rrFactSomb.Close();

            cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\Agi130r1_CR11.rpt";
            Serrores.Obtener_Multilenguaje(Conexion_Base_datos.RcAdmision, vstGidenpac);

            Serrores.ObtenerTextosRPT(Conexion_Base_datos.RcAdmision, "ADMISION", "AGI130R1");
            proTempo_FacturaSombra();
            //    ObtenerMesesSemanas RcAdmision
            //
            //    MaysculaOMinuscula = "m" 'para ver si la fecha va en mayusculas o minusculas

            //*********cabeceras
            if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
            {
                Conexion_Base_datos.proCabCrystal();
            }
            //*******************
            stSql = "select fllegada,faltplan,dnompers,dap1pers,dap2pers,dinspecc,amovifin.nafiliac, " +
                    "itituben , irespons, dnombper, dape1per, dape2per, ddireper,ddirepac, dparente , " +
                    "dperscon.gpoblaci as gpobpers, dperscon.dpoblaci as dpobpers,dperscon.gpaisres as gpaispers, dperscon.gprovinc as gprovres,  " +
                    "dpacient.gpoblaci as gpobpaci, dpacient.dpoblaci as dpobpaci, dpacient.gprovinc as gprovpac,dpacient.gpaisres gpaispaci " +
                    "from aepisadm inner join amovifin on aepisadm.ganoadme = amovifin.ganoregi and aepisadm.gnumadme = amovifin.gnumregi " +
                    "inner join dpersona on aepisadm.gperulti =dpersona.gpersona " +
                    "inner join dinspecc on amovifin.ginspecc =dinspecc.ginspecc " +
                    "inner join dentipac on dentipac.gidenpac = aepisadm.gidenpac and dentipac.gsocieda = amovifin.gsocieda " +
                    "left join dperscon on dperscon.gidenpac = aepisadm.gidenpac and irespons='S' " +
                    "inner join dpacient on dpacient.Gidenpac = aepisadm.Gidenpac " +
                    "Where Ganoadme = " + viGanoadme.ToString() + " And Gnumadme =  " + viGnumadme.ToString() + "";

            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            rrFactSomb = new DataSet();
            tempAdapter_2.Fill(rrFactSomb);
            if (rrFactSomb.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["itituben"]).Trim().ToUpper() == "T")
                {
                    cryInformes.Formulas["{@TITULAR}"] = "'X'";
                    cryInformes.Formulas["{@BENEFICIARIO}"] = "' '";
                }
                else
                {
                    cryInformes.Formulas["{@BENEFICIARIO}"] = "'X'";
                    cryInformes.Formulas["{@TITULAR}"] = "' '";
                }
                cryInformes.Formulas["{@PACIENTE}"] = "'" + stNPaciente + "'";
                cryInformes.Formulas["{@FINGRESO}"] = "'" + Convert.ToDateTime(rrFactSomb.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy") + "'";
                if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["faltplan"]))
                {
                    cryInformes.Formulas["{@FALTA}"] = "'" + Convert.ToDateTime(rrFactSomb.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy") + "'";
                }
                cryInformes.Formulas["{@MEDICO}"] = "'" + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dnompers"]) + "").Trim() + " " + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dap1pers"]) + "").Trim() + " " + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dap2pers"]) + "").Trim() + "'";
                cryInformes.Formulas["{@ENTIDAD}"] = "'" + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dinspecc"]) + "").Trim() + "'";
                cryInformes.Formulas["{@NAFILIACION}"] = "'" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["nafiliac"]).Trim() + "'";


                if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["irespons"]))
                {
                    if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["gpobpers"]))
                    { //DEPERSCON						
                        stSql = "SELECT * FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gpobpers"]).Trim() + "'";
                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrFactSomb1 = new DataSet();
                        tempAdapter_3.Fill(rrFactSomb1);
                        if (rrFactSomb1.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    stPoblacion = (Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["DPOBLACI"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["DPOBLACI"]).Trim();
                                    break;
                                case "IDIOMA1":
                                    stPoblacion = (Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII1"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII1"]).Trim();
                                    break;
                                case "IDIOMA2":
                                    stPoblacion = (Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII2"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII2"]).Trim();
                                    break;
                            }
                            //                         stPoblacion = Trim(rrFactSomb1("DPOBLACI"))
                        }
                        rrFactSomb1.Close();
                    }
                    else
                    {
                        if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["dpobpers"]))
                        {
                            stPoblacion = Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dpobpers"]).Trim() + " - ";
                        }
                    }

                    if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["gprovres"]))
                    {
                        stSql = "SELECT dnomprov,dnomprovi1,dnomprovi2 FROM dprovinc WHERE gprovinc = '" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gprovres"]).Trim() + "' ";
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrFactSomb1 = new DataSet();
                        tempAdapter_4.Fill(rrFactSomb1);
                        if (rrFactSomb1.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dnomprov"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dnomprov"]).Trim());
                                    break;
                                case "IDIOMA1":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dnomprovI1"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dnomprovI1"]).Trim());
                                    break;
                                case "IDIOMA2":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dnomprovI2"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dnomprovI2"]).Trim());
                                    break;
                            }
                            //                         stPoblacion = stPoblacion & " - " & Trim(rrFactSomb1("dnomprov"))
                        }
                        rrFactSomb1.Close();
                    }

                    if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["gpaispers"]))
                    {
                        stSql = "SELECT dpaisres,dpaisresi1,dpaisresi2 FROM dpaisres WHERE gpaisres = '" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gpaispers"]).Trim() + "' ";
                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrFactSomb1 = new DataSet();
                        tempAdapter_5.Fill(rrFactSomb1);
                        if (rrFactSomb1.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dpaisres"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dpaisres"]).Trim());
                                    break;
                                case "IDIOMA1":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dpaisresI1"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dpaisresI1"]).Trim());
                                    break;
                                case "IDIOMA2":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dpaisresI2"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dpaisresI2"]).Trim());
                                    break;
                            }
                            //                         stPoblacion = stPoblacion & "   " & Trim(rrFactSomb1("dpaisres"))
                        }
                        rrFactSomb1.Close();
                    }


                    cryInformes.Formulas["{@DIRECCION}"] = "'" + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["ddireper"]) + "").Trim() + ". " + stPoblacion + " '";
                    cryInformes.Formulas["{@PARENTESCO}"] = "'" + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dparente"]) + "").Trim() + "'";
                    cryInformes.Formulas["{@PACIENTEFIRMANTE}"] = "'" + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dnombper"]) + "").Trim() + " " + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dape1per"]) + "").Trim() + " " + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dape2per"]) + "").Trim() + "'";

                }
                else
                {
                    if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["gpobpaci"]))
                    { //DPACIENT						
                        stSql = "SELECT * FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gpobpaci"]).Trim() + "' ";
                        SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrFactSomb1 = new DataSet();
                        tempAdapter_6.Fill(rrFactSomb1);
                        if (rrFactSomb1.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    stPoblacion = (Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["DPOBLACI"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["DPOBLACI"]).Trim();
                                    break;
                                case "IDIOMA1":
                                    stPoblacion = (Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII1"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII1"]).Trim();
                                    break;
                                case "IDIOMA2":
                                    stPoblacion = (Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII2"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["DPOBLACII2"]).Trim();
                                    break;
                            }
                            //                         stPoblacion = Trim(rrFactSomb1("DPOBLACI"))
                        }
                        rrFactSomb1.Close();
                    }
                    else
                    {
                        if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["dpobpaci"]))
                        {
                            stPoblacion = Convert.ToString(rrFactSomb.Tables[0].Rows[0]["dpobpaci"]).Trim();
                        }
                    }

                    if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["gprovpac"]))
                    {
                        stSql = "SELECT dnomprov,dnomprovi1,dnomprovi2 FROM dprovinc WHERE gprovinc = '" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gprovpac"]).Trim() + "' ";
                        SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrFactSomb1 = new DataSet();
                        tempAdapter_7.Fill(rrFactSomb1);
                        if (rrFactSomb1.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dnomprov"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dnomprov"]).Trim());
                                    break;
                                case "IDIOMA1":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dnomprovI1"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dnomprovI1"]).Trim());
                                    break;
                                case "IDIOMA2":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dnomprovI2"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dnomprovI2"]).Trim());
                                    break;
                            }
                            //                         stPoblacion = stPoblacion & " - " & Trim(rrFactSomb1("dnomprov"))
                        }
                        rrFactSomb1.Close();
                    }

                    if (!Convert.IsDBNull(rrFactSomb.Tables[0].Rows[0]["gpaispaci"]))
                    {
                        stSql = "SELECT dpaisres,dpaisresi1,dpaisresi2 FROM dpaisres WHERE gpaisres = '" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["gpaispaci"]).Trim() + "'";
                        SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        rrFactSomb1 = new DataSet();
                        tempAdapter_8.Fill(rrFactSomb1);
                        if (rrFactSomb1.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dpaisres"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dpaisres"]).Trim());
                                    break;
                                case "IDIOMA1":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dpaisresI1"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dpaisresI1"]).Trim());
                                    break;
                                case "IDIOMA2":
                                    stPoblacion = stPoblacion + " - " + ((Convert.IsDBNull(rrFactSomb1.Tables[0].Rows[0]["dpaisresI2"])) ? "" : Convert.ToString(rrFactSomb1.Tables[0].Rows[0]["dpaisresI2"]).Trim());
                                    break;
                            }
                            //                         stPoblacion = stPoblacion & "   " & Trim(rrFactSomb1("dpaisres"))
                        }
                        rrFactSomb1.Close();
                    }

                    cryInformes.Formulas["{@DIRECCION}"] = "'" + (Convert.ToString(rrFactSomb.Tables[0].Rows[0]["ddirepac"]) + ". " + stPoblacion + "").Trim() + " '";
                    cryInformes.Formulas["{@PARENTESCO}"] = "'" + "" + "'";
                    cryInformes.Formulas["{@PACIENTEFIRMANTE}"] = "'" + stNPaciente + "'";

                }
            }

            rrFactSomb.Close();

            stSql = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
            SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            rrFactSomb = new DataSet();
            tempAdapter_9.Fill(rrFactSomb);
            if (rrFactSomb.Tables[0].Rows.Count != 0)
            {
                cryInformes.Formulas["{@POBLACIONFECHA}"] = "'" + Convert.ToString(rrFactSomb.Tables[0].Rows[0]["valfanu2"]).Trim() + "  " + DateTime.Today.ToString("dd/MM/yyyy") + "'";
            }
            rrFactSomb.Close();

            cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
            cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
            cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
            cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";

            string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision, Serrores.CampoIdioma);
            LitPersona = LitPersona.ToLower();

            cryInformes.Formulas["{@LitPersona}"] = "\"" + LitPersona + "\"";

            //O.Frias - 04/05/2009
            //Se incorpora la funcion de usuario dependiendo de las constantes IUSULARG y IUSUDOCU
            cryInformes.Formulas["{@Usuario}"] = "\"" + Serrores.fUsuarInfor(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision) + "\"";
            //cryInformes.WindowTitle = "Factura Sombra"
            cryInformes.WindowTitle = Serrores.VarRPT[1];
            //cryInformes.SQLQuery = "select * from FacturaSombra";
            cryInformes.DataFiles[0] = Dbt1.Tables[0];
            cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
            cryInformes.Destination = Crystal.DestinationConstants.crptToWindow; // destino

            cryInformes.WindowShowPrintSetupBtn = true;
            cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
            //cryInformes.Connect = "dsn=" + Conexion_Base_datos.stNombreDsnACCESS;

            cryInformes.SubreportToChange = "LogotipoIDC";
            cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
            cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
            cryInformes.SubreportToChange = "";
            Serrores.LLenarFormulas(Conexion_Base_datos.RcAdmision, cryInformes, "AGI130R1", "AGI130R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT

            cryInformes.Action = 1;
            cryInformes.PrinterStopPage = cryInformes.PageCount;
            Conexion_Base_datos.vacia_formulas(cryInformes, 14);
            cryInformes.Reset();
            ChbJusAcompa.CheckState = CheckState.Unchecked;
        }

        public void proTempo_FacturaSombra()
        {
            
            string stCodDiag = String.Empty;
            string stDesDiag = String.Empty;

            Dbt1 = new DataSet();

            DataSet RrTemp2 = null;

            string stNomtabla = "CR11_AGI130R1"; //"FacturaSombra";
            //SI NO EXISTE LA BASE DE DATOS CREARLA
            bool bExiste_base = false;

            Dbt1.CreateTable(stNomtabla, Conexion_Base_datos.PathString + "\\rpt\\CR11_Agi130r1.xsd");

            //Obtenemos el diagn�stico al ingreso
            var stSql = "SELECT GDIAGING,ODIAGING,FLLEGADA From AEPISADM Where ganoadme = " + viGanoadme.ToString() + " AND gnumadme = " + viGnumadme.ToString();
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);

            if (!Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["GDIAGING"]))
            {
                stCodDiag = Convert.ToString(rrtemp.Tables[0].Rows[0]["GDIAGING"]).Trim();
                object tempRefParam = rrtemp.Tables[0].Rows[0]["FLLEGADA"];
                object tempRefParam2 = rrtemp.Tables[0].Rows[0]["FLLEGADA"];
                stSql = "SELECT * FROM DDIAGNOS WHERE GCODIDIA='" + Convert.ToString(rrtemp.Tables[0].Rows[0]["GDIAGING"]) + "' AND" + " FINVALDI<=" + Serrores.FormatFechaHMS(tempRefParam) + " AND (FFIVALDI>=" + Serrores.FormatFechaHMS(tempRefParam2) + " OR FFIVALDI IS NULL)";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                RrTemp2 = new DataSet();
                tempAdapter_2.Fill(RrTemp2);
                if (RrTemp2.Tables[0].Rows.Count != 0)
                {
                    //           stDesDiag = Trim(RrTemp2("DNOMBDIA"))
                    if (Serrores.CampoIdioma == "IDIOMA0")
                    {
                        stDesDiag = (Convert.IsDBNull(RrTemp2.Tables[0].Rows[0]["DNOMBDIA"])) ? "" : Convert.ToString(RrTemp2.Tables[0].Rows[0]["DNOMBDIA"]).Trim();
                    }
                    else if (Serrores.CampoIdioma == "IDIOMA1")
                    {
                        stDesDiag = (Convert.IsDBNull(RrTemp2.Tables[0].Rows[0]["DNOMBDIAI1"])) ? "" : Convert.ToString(RrTemp2.Tables[0].Rows[0]["DNOMBDIAI1"]).Trim();
                    }
                    else if (Serrores.CampoIdioma == "IDIOMA2")
                    {
                        stDesDiag = (Convert.IsDBNull(RrTemp2.Tables[0].Rows[0]["DNOMBDIAI2"])) ? "" : Convert.ToString(RrTemp2.Tables[0].Rows[0]["DNOMBDIAI2"]).Trim();
                    }
                }
                RrTemp2.Close();
            }
            else
            {
                stDesDiag = Convert.ToString(rrtemp.Tables[0].Rows[0]["ODIAGING"]).Trim();
                stCodDiag = "";
            }

            rrtemp.Close();
            //RELLENAR LA TABLA CON EL DIAGN�STICO Y LAS PRESTACIONES
            //stsql = "SELECT * From EPRESTAC Where (iestsoli='P' or iestsoli='R') and ganoregi = " & viGanoadme & " AND gnumregi = " & viGnumadme

            stSql = "SELECT * From EPRESTAC inner join dcodpres on eprestac.gprestac=dcodpres.gprestac " +
                    "and finivali<= fpeticio and (ffinvali>=fpeticio or ffinvali is null) inner join " +
                    "dservici on dcodpres.gservici=dservici.gservici and iquirofa='S' " +
                    "Where (iestsoli='P' or iestsoli='R') and ganoregi = " + viGanoadme.ToString() + " AND gnumregi = " + viGnumadme.ToString();
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            rrtemp = new DataSet();
            tempAdapter_3.Fill(rrtemp);
           
            DataTable table = Dbt1.Tables[stNomtabla];
            DataRow rrTabla = null;
            //Si tenemos alguna prestaci�n o un diagn�stico, lo registramos.
            if (rrtemp.Tables[0].Rows.Count != 0 || stDesDiag != "" || stCodDiag != "")
			{
				do 
				{
                    rrTabla = table.NewRow();
					
					rrTabla["CodDiag"] = stCodDiag;
					
					rrTabla["DesDiag"] = stDesDiag;
					if (rrtemp.Tables[0].Rows.Count != 0)
					{						
						rrTabla["CodPres"] = rrtemp.Tables[0].Rows[0]["gprestac"];
						
						rrtemp.MoveNext();
					}

                    table.Rows.Add(rrTabla);
				}
				while(rrtemp.Tables[0].Rows.Count != 0);
			}
			rrtemp.Close();
        }

        private void proAportaciones()
        {
            string stSql = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'APENRESI'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            DataSet rdoTemp = new DataSet();
            tempAdapter.Fill(rdoTemp);
            object ObjDeposito = null;
            if (rdoTemp.Tables[0].Rows.Count != 0)
            {
                if (!(Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu2"])))
                {
                    if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]).Trim() != "")
                    {
                        //DGMORENOG TODO_4_4 - PENDIENTE IMPLEMENTAR
                        /*ObjDeposito = new LlamarOperacionesFMS.clsOperFMS();						
						ObjDeposito.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]).Trim(), stTGidenpac, null, null, "H", viGanoadme, viGnumadme, "");
						ObjDeposito = null;*/
                        chkAportaPension.CheckState = CheckState.Unchecked;
                        return;
                    }
                }
            }
            chkAportaPension.CheckState = CheckState.Unchecked;
            RadMessageBox.Show("Error en el valfanu2 de la constante APENRESI.", "Error: Documentos Ingreso", MessageBoxButtons.OK, RadMessageIcon.Error);
        }

        public void proDeposito()
        {
            int lCodServicio = 0;

            string stSql = "SELECT gserulti From AEPISADM Where ganoadme = " + viGanoadme.ToString() + " AND gnumadme = " + viGnumadme.ToString();
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
            DataSet rrDeposito = new DataSet();
            tempAdapter.Fill(rrDeposito);
            if (rrDeposito.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(rrDeposito.Tables[0].Rows[0]["gserulti"]))
                {
                    lCodServicio = Convert.ToInt32(rrDeposito.Tables[0].Rows[0]["gserulti"]);
                }
            }
            //DGMORENOG TODO_4_4 - PENDIENTE IMPLEMENTAR
            /*object ObjDeposito = new LlamarOperacionesFMS.clsOperFMS();			
			ObjDeposito.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "DEPOSITO", stTGidenpac, null, null, "H", viGanoadme, viGnumadme, lCodServicio);
			ObjDeposito = null;*/

            chbDeposito.CheckState = CheckState.Unchecked;
        }

        //private bool SoloEtiquetas()
        //{
        //bool result = false;
        //if (chbEtiqueta.CheckState == CheckState.Checked )
        //{
        //result = true;
        //}
        //if (ChbJustificante.CheckState == CheckState.Checked )
        //{
        //result = false;
        //}
        //if (chbDatos.CheckState == CheckState.Checked )
        //{ //imprime hoja de datos
        //result = false;
        //}
        //if (chbVolante.CheckState == CheckState.Checked )
        //{ //imprime Volante Sociedad
        //result = false;
        //}
        //if (chbIngreso.CheckState == CheckState.Checked )
        //{ //imprime hoja de ingreso
        //result = false;
        //}
        //if (Chbreligioso.CheckState == CheckState.Checked )
        //{ //imprime hoja para el servicio religioso
        //result = false;
        //}
        //if (Chbtrabajador.CheckState == CheckState.Checked )
        //{ //imprime hoja para el trabajador social
        //result = false;
        //}
        //if (ChbHaceconstar.CheckState == CheckState.Checked )
        //{ //imprime hoja de hace constar
        //result = false;
        //}
        //if (ChbAusencia.CheckState == CheckState.Checked )
        //{ //imprime hoja de ausencia temporal
        //result = false;
        //}
        //if (ChbJusAcompa.CheckState == CheckState.Checked )
        //{
        //result = false;
        //}
        //if (chbIntervencion.CheckState == CheckState.Checked )
        //{ //imprime consentimiento
        //result = false;
        //}
        //if (ChbOficio.CheckState == CheckState.Checked )
        //{ //imprime carta
        //result = false;
        //}
        //
        //oscar 15/03/04
        //if (chbAutoDatos.CheckState == CheckState.Checked )
        //{ //imprime autorizacion de datos
        //result = false;
        //}
        //if (chbAutoEstudio.CheckState == CheckState.Checked )
        //{ //imprime autorizacion de estudio clinico
        //result = false;
        //}
        //--------
        //
        //return result;
        //}

        private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        //private void chbAsistencia_Click()
        //{
        //proSeleccion_Doc2();
        //}

        private void ChbAusencia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void ChbAutoEmision_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbContrato_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbContratoIngreso_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbDatos_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbDeposito_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbEtiqueta_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbFactSomb_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void ChbHaceconstar_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbHojaClinEsta_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbIngreso_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbIntervencion_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void ChbJusAcompa_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void ChbJustificante_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void ChbOficio_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void Chbreligioso_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void Chbtrabajador_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbVolante_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chkAportaPension_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void Documentos_ingreso_Activated(Object eventSender, EventArgs eventArgs)
        {
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;

                string stComp = String.Empty, tstSS = String.Empty;
                int iTag = 0;
                int iTag2 = 0;
                string stSql = String.Empty;
                bool stPermitirImpJust = false;
                //******
                try
                {
                    //************
                    //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
                    /*Parasalida = Crystal.DestinationConstants.crptToPrinter; //crptToWindow*/
                    //ParaEtiquetas = crptToWindow
                    stComp = "/";
                    iTag = (Convert.ToString(this.Tag).IndexOf(stComp, StringComparison.CurrentCultureIgnoreCase) + 1);
                    iTag2 = Strings.InStr(iTag + 1, Convert.ToString(this.Tag), stComp, CompareMethod.Text);
                    stTGidenpac = Convert.ToString(this.Tag).Substring(0, Math.Min(iTag - 1, Convert.ToString(this.Tag).Length));
                    stNPaciente = Convert.ToString(this.Tag).Substring(iTag, Math.Min(iTag2 - iTag - 1, Convert.ToString(this.Tag).Length - iTag));
                    iTag = Strings.InStr(iTag2 + 1, Convert.ToString(this.Tag), stComp, CompareMethod.Text);
                    viGanoadme = Convert.ToInt32(Double.Parse(Convert.ToString(this.Tag).Substring(iTag2, Math.Min(iTag - iTag2 - 1, Convert.ToString(this.Tag).Length - iTag2))));
                    viGnumadme = Convert.ToInt32(Double.Parse(Convert.ToString(this.Tag).Substring(iTag)));

                    lbPaciente.Text = stNPaciente;
                    Frame2.Enabled = false;
                    cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                    //*******historia, si no tiene no puede seleccionar Hoja de Datos
                    //Capio 18/12/08,    Ahora se puede seleccionar siempre
                    vstHistoria = Conexion_Base_datos.fnHistoria(stTGidenpac);
                    //''    If vstHistoria <> "" Then
                    //''        chbDatos.Enabled = True
                    //''    Else
                    //''        chbDatos.Enabled = False
                    //''    End If
                    //********Si no es de sociedad no se imprime el Volante
                    //********Si no es de SS no se imprime la factura sombra

                    tstSS = Conexion_Base_datos.fnSociedad(stTGidenpac, viGanoadme, viGnumadme);
                    iTag = (tstSS.IndexOf(stComp, StringComparison.CurrentCultureIgnoreCase) + 1);
                    vstSociedad = tstSS.Substring(0, Math.Min(iTag - 1, tstSS.Length));
                    tstSS = tstSS.Substring(iTag);
                    chbFactSomb.Enabled = tstSS == "SS";
                    chbVolante.Enabled = tstSS != "PR";
                    //****ausencia temporal
                    sqlCrystal = "SELECT gunenfor,  AAUSENCI.fausenci, AAUSENCI.fprevuel," + "DPACIENT.dnombpac, DPACIENT.dape1pac,DPACIENT.dape2pac " + " From AAUSENCI,AEPISADM,amovimie, DPACIENT " + " Where " + " (AAUSENCI.ganoadme = AEPISADM.ganoadme AND AAUSENCI.gnumadme = AEPISADM.gnumadme " + " AND AAUSENCI.gmotause = AEPISADM.gmotause AND AEPISADM.gidenpac = DPACIENT.gidenpac " + " AND  AEPISADM.ganoadme =amovimie.ganoregi " + " AND  AEPISADM.gnumadme =amovimie.gnumregi " + " AND  AEPISADM.ganoadme = " + viGanoadme.ToString() + " AND  AEPISADM.gnumadme = " + viGnumadme.ToString() + " AND  AEPISADM.gidenpac ='" + stTGidenpac + "')" + " order by AAUSENCI.fausenci";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCrystal, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count == 0)
                    {
                        ChbAusencia.Enabled = false;
                    }
                    Rr1.Close();


                    stPermitirImpJust = true;
                    //oscar 16/03/04
                    string sql = String.Empty;
                    sql = "Select iautojus,gjuzgad1,gjuzgad2 from aepisadm where " +
                          " Ganoadme = " + viGanoadme.ToString() +
                          " AND gnumadme = " + viGnumadme.ToString() +
                          " AND gidenpac ='" + stTGidenpac + "'";

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_2.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count != 0)
                    {
                        ChbInterJuzga.Enabled = !Convert.IsDBNull(Rr1.Tables[0].Rows[0]["gjuzgad1"]);
                        chbDuplicidad.Enabled = !Convert.IsDBNull(Rr1.Tables[0].Rows[0]["gjuzgad2"]);
                        stPermitirImpJust = (Convert.ToString(Rr1.Tables[0].Rows[0]["iautojus"]) + "" != "N");
                    }
                    Rr1.Close();
                    //--------
                    //Si est� activa la constante y el paciente ha dejado imprimir el justificante o no se ha podido saber su deseo(es nulo el campo)
                    sql = "select * from SCONSGLO where gconsglo ='JUSERMAS' and valfanu1='S'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_3.Fill(Rr1);
                    ChbAutoEmision.Enabled = ((Rr1.Tables[0].Rows.Count == 1) && stPermitirImpJust);
                    return;
                }
                catch (SqlException ex)
                {
                    //********
                    Serrores.GestionOtrosErrores(ex.Number, Conexion_Base_datos.RcAdmision);
                }
                catch (Exception exception)
                {
                    System.Console.Write(exception.Message);
                }
            }
        }

        public void proSeleccion_Doc2()
        {
            //Habilito cbaceptar
            if (ChbJustificante.CheckState == CheckState.Checked || chbIntervencion.CheckState == CheckState.Checked || chbDatos.CheckState == CheckState.Checked || chbIngreso.CheckState == CheckState.Checked || chbVolante.CheckState == CheckState.Checked || chbEtiqueta.CheckState == CheckState.Checked || Chbreligioso.CheckState == CheckState.Checked || Chbtrabajador.CheckState == CheckState.Checked || ChbHaceconstar.CheckState == CheckState.Checked || ChbAusencia.CheckState == CheckState.Checked || ChbJusAcompa.CheckState == CheckState.Checked || chbHojaClinEsta.CheckState == CheckState.Checked || ChbOficio.CheckState == CheckState.Checked || chbDeposito.CheckState == CheckState.Checked || chbFactSomb.CheckState == CheckState.Checked || chbContrato.CheckState == CheckState.Checked || chbAutoDatos.CheckState == CheckState.Checked || chbAutoEstudio.CheckState == CheckState.Checked || chbPa�ales.CheckState == CheckState.Checked || ChbInterJuzga.CheckState == CheckState.Checked || chbDuplicidad.CheckState == CheckState.Checked || chkAportaPension.CheckState == CheckState.Checked || ChbAutoEmision.CheckState == CheckState.Checked || chbContratoIngreso.CheckState == CheckState.Checked)
            {
                if (chbEtiqueta.CheckState == CheckState.Checked)
                {
                    Frame2.Enabled = true;
                    cbAceptar.Enabled = Option1[0].IsChecked || Option1[1].IsChecked || Option1[2].IsChecked || Option1[3].IsChecked || Option1[4].IsChecked || Option1[5].IsChecked;
                }
                else
                {
                    cbAceptar.Enabled = true;
                }
            }
            if (chbEtiqueta.CheckState == CheckState.Unchecked)
            {
                Frame2.Enabled = false;
                Option1[0].IsChecked = false;
                Option1[1].IsChecked = false;
                Option1[2].IsChecked = false;
                Option1[3].IsChecked = false;
                //(maplaza)(04/01/2008)
                Option1[4].IsChecked = false;
                //-------
                Option1[5].IsChecked = false;
            }
            if (ChbJustificante.CheckState == CheckState.Unchecked && chbIntervencion.CheckState == CheckState.Unchecked && chbDatos.CheckState == CheckState.Unchecked && chbIngreso.CheckState == CheckState.Unchecked && chbVolante.CheckState == CheckState.Unchecked && chbEtiqueta.CheckState == CheckState.Unchecked && Chbreligioso.CheckState == CheckState.Unchecked && Chbtrabajador.CheckState == CheckState.Unchecked && ChbHaceconstar.CheckState == CheckState.Unchecked && ChbAusencia.CheckState == CheckState.Unchecked && ChbJusAcompa.CheckState == CheckState.Unchecked && chbHojaClinEsta.CheckState == CheckState.Unchecked && ChbOficio.CheckState == CheckState.Unchecked && chbDeposito.CheckState == CheckState.Unchecked && chbFactSomb.CheckState == CheckState.Unchecked && chbContrato.CheckState == CheckState.Unchecked && chbAutoDatos.CheckState == CheckState.Unchecked && chbAutoEstudio.CheckState == CheckState.Unchecked && chbPa�ales.CheckState == CheckState.Unchecked && ChbInterJuzga.CheckState == CheckState.Unchecked && chbDuplicidad.CheckState == CheckState.Unchecked && chkAportaPension.CheckState == CheckState.Unchecked && ChbAutoEmision.CheckState == CheckState.Unchecked && chbContratoIngreso.CheckState == CheckState.Unchecked)
            {
                cbAceptar.Enabled = false;
            }
        }

        //(maplaza)(12/03/2008)Se a�ade un par�metro opcional (iNumCopias), pues si la impresi�n se realiza a trav�s
        //del objeto Printer, se necesita esta informaci�n.

        public void proEtiquetas(Crystal Dialogo_impresora, int iNumCopias = 0)
        {
            //debe llamar al la dll
            object Clase = null;
            string stTipoEtiqueta = String.Empty;
            string stImpresora = String.Empty;
            int puntero = 0;
            do
            {
                try
                {
                    //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                    /*Clase = new DocumentoDLL.mcDocumento();				
                    Clse.proConexion(this, Conexion_Base_datos.RcAdmision, Dialogo_impresora, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword);*/

                    if (Option1[0].IsChecked)
                    { // Simple
                        stTipoEtiqueta = "SIMPLE";
                    }
                    else if (Option1[1].IsChecked)
                    {  //Completa
                        stTipoEtiqueta = "COMPLETA";
                    }
                    else if (Option1[2].IsChecked)
                    {  //Correo
                        stTipoEtiqueta = "CORREO";
                    }
                    else if (Option1[3].IsChecked)
                    {  //m�s etiquetas
                        stTipoEtiqueta = "MAS";
                        //(maplaza)(04/01/2008)Etiquetas de identificaci�n del paciente
                    }
                    else if (Option1[4].IsChecked)
                    {  //m�s etiquetas
                        stTipoEtiqueta = "IDENTIFICACION";
                        //-------------
                    }
                    else if (Option1[5].IsChecked)
                    {
                        stTipoEtiqueta = "BRAZALETE";
                    }

                    //(maplaza)(04/01/2008)
                    string stFech = String.Empty;
                    if (stTipoEtiqueta == "IDENTIFICACION")
                    {
                        if (PrinterHelper.Printer.DeviceName.Trim() != "")
                        {
                            stImpresora = PrinterHelper.Printer.DeviceName;
                        }
                        if (Serrores.FnSelecciona_ImpresoraCrystal(Dialogo_impresora, "DATAMAX", Conexion_Base_datos.RcAdmision))
                        {
                            //(maplaza)(12/03/2008)Las etiquetas de identificaci�n se imprimen a trav�s del objeto Printer.
                            //Clase.proEtiquetas stTipoEtiqueta, stTGidenpac, viGanoadme, viGnumadme, Dialogo_impresora						
                            //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                            //Clase.proEtiquetasIdentificacion(PrinterHelper.Printer.DeviceName, stTGidenpac, iNumCopias);
                        }
                        //se restaura en el objeto Printer la impresora que estaba antes
                        foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
                        {
                            if (PrinterHelper.Printer.DeviceName.ToUpper() == stImpresora.ToUpper())
                            {
                                PrinterHelper.Printer = vimpresora;
                                break;
                            }
                        }
                    }
                    else if ((stTipoEtiqueta == "BRAZALETE"))
                    {
                        stFech = DateTimeHelper.ToString(DateTime.Now);

                        while (DateTime.Parse(stFech).AddSeconds(2) > DateTime.Now)
                        {
                        };
                        if (PrinterHelper.Printer.DeviceName.Trim() != "")
                        {
                            stImpresora = PrinterHelper.Printer.DeviceName;
                        }
                        if (Serrores.FnSelecciona_Brazalete(PrinterHelper.Printer, "ZEBRA2824", Conexion_Base_datos.RcAdmision))
                        {
                            //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                            //Clase.proEtiquetasBrazalete(PrinterHelper.Printer.DeviceName, stTGidenpac);
                        }
                        else
                        {
                            RadMessageBox.Show("Impresora de brazaletes no definida para este equipo", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                        //se restaura en el objeto Printer la impresora que estaba antes
                        foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
                        {
                            if (PrinterHelper.Printer.DeviceName.ToUpper() == stImpresora.ToUpper())
                            {
                                PrinterHelper.Printer = vimpresora;
                                break;
                            }
                        }
                    }
                    else if ((stTipoEtiqueta == "COMPLETA") && Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "TIPOETIQ", "VALFANU2") == "S")
                    {
                        //Si se trata de etiquetas completas y est� activa la constante de Le�n......
                        if (PrinterHelper.Printer.DeviceName.Trim() != "")
                        {
                            stImpresora = PrinterHelper.Printer.DeviceName;
                        }
                        PrinterHelper.Printer = Conexion_Base_datos.prImpre;

                        //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
                        /*Dialogo_impresora.PrinterName = Conexion_Base_datos.VstImpresora2;
                        Dialogo_impresora.PrinterDriver = PrinterHelper.Printer.DeviceName;
                        Dialogo_impresora.PrinterPort = PrinterHelper.Printer.Port;*/

                        //(Las etiquetas se imprimen a trav�s del objeto Printer.
                        //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                        //Clase.proEtiquetasCompletas(PrinterHelper.Printer.DeviceName, stTGidenpac, Conexion_Base_datos.ViNumCopias, Image1, Image2);
                        //se restaura en el objeto Printer la impresora que estaba antes
                        foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
                        {
                            if (PrinterHelper.Printer.DeviceName.ToUpper() == stImpresora.ToUpper())
                            {
                                PrinterHelper.Printer = vimpresora;
                                break;
                            }
                        }
                    }
                    else
                    {
                        //------------
                        //sintaxis pcristal
                        //Dialogo_impresora.Impresora = VstImpresora2
                        //Dialogo_impresora.Puerto = VstPortPrint2
                        //Dialogo_impresora.Driver = VstDriverPrint2
                        //-------
                        //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
                        /*Dialogo_impresora.PrinterName = Conexion_Base_datos.VstImpresora2;
                        Dialogo_impresora.PrinterPort = Conexion_Base_datos.VstPortPrint2;
                        Dialogo_impresora.PrinterDriver = Conexion_Base_datos.VstDriverPrint2;*/

                        //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                        //Clase.proEtiquetas(stTipoEtiqueta, stTGidenpac, viGanoadme, viGnumadme, Dialogo_impresora);
                    }

                    Clase = null;
                }
                catch (Exception ex)
                {
                    //*********
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proEtiquetas:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);
        }


        public void proDatos(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            string tstProv1 = String.Empty;
            string tstProv2 = String.Empty;
            string tstPobla2 = String.Empty;
            string tstDiag = String.Empty;
            string[] MatrizSql = null;
            DLLTextosSql.TextosSql oTextoSql = null;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;
            int puntero = 0;
            do
            {
                try
                {
                    string LitPersona = String.Empty;
                    cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi121r1_CR11.rpt";

                    //   sqlCrystal = "SELECT * From (((((((AMOVIFIN inner join AEPISADM  on " _
                    //'              & " (AMOVIFIN.GANOREGI= AEPISADM.GANOADME) and (AMOVIFIN.GNUMREGI= AEPISADM.GNUMADME)) " _
                    //'              & "  inner join dcamasbo on " _
                    //'              & " (AEPISADM.gidenpac = dcamasbo.gidenpac)) " _
                    //'              & " inner join DPACIENT on " _
                    //'              & " (aepisadm.gidenpac=dpacient.gidenpac)) " _
                    //'              & " inner join DSERVICI on " _
                    //'              & " (AEPISADM.gservici =DSERVICI.gservici)) " _
                    //'              & " left join DPRIVADO on" _
                    //'              & " (AEPISADM.gidenpac = dpRIVADO.gidenpac)) " _
                    //'              & " left join DSOCIEDA on" _
                    //'              & " (AMOVIFIN.gsocieda =  DSOCIEDA.gsocieda)) " _
                    //'              & " left join DENTIPAC on " _
                    //'              & " (AEPISADM.gidenpac = DENTIPAC.gidenpac) and (AMOVIFIN.GSOCIEDA = DENTIPAC.GSOCIEDA) AND (AMOVIFIN.NAFILIAC = DENTIPAC.NAFILIAC)) " _
                    //'              & " Where " _
                    //'              & " AEPISADM.ganoadme = " & viGanoadme _
                    //'              & " AND  AEPISADM.gnumadme = " & viGnumadme _
                    //'              & " AND  AEPISADM.gidenpac ='" & stTGidenpac & "' and " _
                    //'              & " DCAMASBO.IESTCAMA='O'  AND " _
                    //'              & " AMOVIFIN.FFINMOVI IS NULL"
                    MatrizSql = new string[] { String.Empty, String.Empty, String.Empty, String.Empty };
                    MatrizSql[1] = viGanoadme.ToString();
                    MatrizSql[2] = viGnumadme.ToString();
                    MatrizSql[3] = stTGidenpac;
                    oTextoSql = new DLLTextosSql.TextosSql();

                    string tempRefParam = "ADMISION";
                    string tempRefParam2 = "AGI150F1";
                    string tempRefParam3 = "proDatos";
                    short tempRefParam4 = 1;
                    sqlCrystal = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, MatrizSql, Conexion_Base_datos.RcAdmision));
                    oTextoSql = null;
                    //**********DIAGNOSTICO
                    sql1 = "select GDIAGING,ODIAGING,fllegada From AEPISADM where FALTPLAN IS NULL AND FALTAADM IS NULL and gidenpac='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter.Fill(Rr1);

                    if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["GDIAGING"]))
                    {
                        object tempRefParam6 = Rr1.Tables[0].Rows[0]["FLLEGADA"];
                        object tempRefParam7 = Rr1.Tables[0].Rows[0]["FLLEGADA"];
                        sql2 = "SELECT * FROM DDIAGNOS where GCODIDIA= '" + Convert.ToString(Rr1.Tables[0].Rows[0]["GDIAGING"]) + "' AND (" + Serrores.FormatFecha(tempRefParam6) + " BETWEEN FINVALDI AND FFIVALDI " + " OR " + " (" + Serrores.FormatFecha(tempRefParam7) + ">=FINVALDI and ffivaldi IS  NULL))";
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql2, Conexion_Base_datos.RcAdmision);
                        Rr2 = new DataSet();
                        tempAdapter_2.Fill(Rr2);
                        if (Rr2.Tables[0].Rows.Count == 1)
                        {
                            tstDiag = Convert.ToString(Rr2.Tables[0].Rows[0]["DNOMBDIA"]);
                        }
                        Rr2.Close();
                    }
                    else if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["ODIAGING"]))
                    {
                        tstDiag = Convert.ToString(Rr1.Tables[0].Rows[0]["ODIAGING"]);
                    }
                    Rr1.Close();

                    //**************provincia
                    sql1 = "select gprovinc,gpoblaci from dpacient where gidenpac='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_3.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count != 0)
                    {
                        if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["gprovinc"]))
                        {
                            sql2 = "select dnomprov from dprovinc where gprovinc='" + Convert.ToString(Rr1.Tables[0].Rows[0]["gprovinc"]) + "'";
                            SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql2, Conexion_Base_datos.RcAdmision);
                            Rr2 = new DataSet();
                            tempAdapter_4.Fill(Rr2);
                            if (Rr2.Tables[0].Rows.Count != 0)
                            {
                                if (!Convert.IsDBNull(Rr2.Tables[0].Rows[0]["dnomprov"]))
                                {
                                    tstProv1 = Convert.ToString(Rr2.Tables[0].Rows[0]["dnomprov"]).Trim();
                                }
                            }
                            Rr2.Close();
                        }
                        //''    If Not IsNull(Rr1("gpoblaci")) Then
                        //''        sql2 = "select dpoblaci from dpoblaci where gpoblaci='" & Rr1("gpoblaci") & "'"
                        //''        Set Rr2 = RcAdmision.OpenResultset(sql2, 1, 1)
                        //''        If Not Rr2.EOF Then
                        //''            If Not IsNull(Rr2("dpoblaci")) Then
                        //''                tstPobla1 = Trim(Rr2("dpoblaci"))
                        //''            End If
                        //''        End If
                        //''        Rr2.Close
                        //''    End If
                    }
                    Rr1.Close();

                    //**************provincia de persona de contacto
                    sql1 = "select gprovinc,gpoblaci from dperscon " +
                           "where gidenpac='" + stTGidenpac + "'";
                    sql1 = sql1 + " order by nnumorde ";
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_5.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count != 0)
                    {
                        if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["gprovinc"]))
                        {
                            sql2 = "select dnomprov from dprovinc where gprovinc='" + Convert.ToString(Rr1.Tables[0].Rows[0]["gprovinc"]) + "'";
                            SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql2, Conexion_Base_datos.RcAdmision);
                            Rr2 = new DataSet();
                            tempAdapter_6.Fill(Rr2);
                            if (Rr2.Tables[0].Rows.Count != 0)
                            {
                                if (!Convert.IsDBNull(Rr2.Tables[0].Rows[0]["dnomprov"]))
                                {
                                    tstProv2 = Convert.ToString(Rr2.Tables[0].Rows[0]["dnomprov"]).Trim();
                                }
                            }
                            Rr2.Close();
                        }
                        //''    If Not IsNull(Rr1("gpoblaci")) Then
                        //''        sql2 = "select dpoblaci from dpoblaci where gpoblaci='" & Rr1("gpoblaci") & "'"
                        //''        Set Rr2 = RcAdmision.OpenResultset(sql2, 1, 1)
                        //''        If Not Rr2.EOF Then
                        //''            If Not IsNull(Rr2("dpoblaci")) Then
                        //''                tstPobla2 = Trim(Rr2("dpoblaci"))
                        //''            End If
                        //''        End If
                        //''        Rr2.Close
                        //''    End If
                    }
                    Rr1.Close();
                    //*********cabeceras
                    if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                    {
                        Conexion_Base_datos.proCabCrystal();
                    }
                    //*******************

                    //'If stFORMFILI = "V" Then
                    //'    ProvinEstado = "Estado:"
                    //'Else
                    Conexion_Base_datos.ProvinEstado = "Provincia:";
                    //'End If

                    cryInformes.Formulas["{@Grup_Hosp}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@Nombre_hospital}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@Dir_hosp}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                    cryInformes.Formulas["{@Historia}"] = "\"" + vstHistoria + "\"";
                    cryInformes.Formulas["{@Nob_Prov}"] = "\"" + tstProv1 + "\"";
                    cryInformes.Formulas["{@Pac_poblaci}"] = "\"" + BuscarPoblacion(stTGidenpac) + "\"";
                    cryInformes.Formulas["{@Diagnostico}"] = "\"" + tstDiag + "\"";
                    cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + Conexion_Base_datos.DNICEDULA + ":" + "\"";

                    //sql1 = "select dnombper, dape1per, dape2per, ddireper, gcodipos, ntelefo1, dparente, dpoblaci.dpoblaci, dnomprov from (dperscon left join dpoblaci on " & _
                    //'          "(dperscon.gpoblaci = dpoblaci.gpoblaci )) left join dprovinc on (dperscon.gprovinc = dprovinc.gprovinc ) where  gidenpac ='" & stTGidenpac & "'"
                    MatrizSql = new string[] { String.Empty, String.Empty };
                    MatrizSql[1] = stTGidenpac;
                    oTextoSql = new DLLTextosSql.TextosSql();

                    string tempRefParam8 = "ADMISION";
                    string tempRefParam9 = "AGI150F1";
                    string tempRefParam10 = "proDatos";
                    short tempRefParam11 = 2;
                    sql1 = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam8, tempRefParam9, tempRefParam10, tempRefParam11, MatrizSql, Conexion_Base_datos.RcAdmision));
                    oTextoSql = null;

                    SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_7.Fill(Rr1);

                    if (Rr1.Tables[0].Rows.Count > 0)
                    {
                        cryInformes.Formulas["{@pers_nombre}"] = "\"" + (Convert.ToString(Rr1.Tables[0].Rows[0]["dape1per"]) + "").Trim() + " " + (Convert.ToString(Rr1.Tables[0].Rows[0]["dape2per"]) + "").Trim() + ", " + (Convert.ToString(Rr1.Tables[0].Rows[0]["dnombper"]) + "").Trim() + "\"";
                        cryInformes.Formulas["{@pers_relacion}"] = "\"" + (Convert.ToString(Rr1.Tables[0].Rows[0]["dparente"]) + "").Trim() + "\"";
                        cryInformes.Formulas["{@pers_direccion}"] = "\"" + (Convert.ToString(Rr1.Tables[0].Rows[0]["ddireper"]) + "").Trim() + "\"";
                        cryInformes.Formulas["{@pers_prov}"] = "\"" + (Convert.ToString(Rr1.Tables[0].Rows[0]["dnomprov"]) + "").Trim() + "\"";
                        cryInformes.Formulas["{@pers_poblaci}"] = "\"" + BuscarPoblacionContacto(stTGidenpac) + "\"";
                        cryInformes.Formulas["{@pers_codpost}"] = "\"" + (Convert.ToString(Rr1.Tables[0].Rows[0]["gcodipos"]) + "").Trim() + "\"";
                        cryInformes.Formulas["{@pers_telefono}"] = "\"" + (Convert.ToString(Rr1.Tables[0].Rows[0]["ntelefo1"]) + "").Trim() + "\"";

                    }

                    Rr1.Close();

                    LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                    LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1) + ":";

                    cryInformes.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";
                    LitPersona = "Relaci�n con el " + LitPersona.ToLower();
                    cryInformes.Formulas["{@LitPers1}"] = "\"" + LitPersona + "\"";
                    cryInformes.Formulas["{@TexProvincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";

                    cryInformes.WindowTitle = "Hoja de Datos";
                    cryInformes.SQLQuery = sqlCrystal;
                    cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    cryInformes.Destination = destino;
                    cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                    cryInformes.WindowShowPrintSetupBtn = true;
                    cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                    cryInformes.Action = 1;
                    cryInformes.PrinterStopPage = cryInformes.PageCount;
                    Conexion_Base_datos.vacia_formulas(cryInformes, 15);
                    cryInformes.Reset();
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proDatos:DocumentosIngreso", ex);
                   
                }
            }
            while (puntero >= 1);
        }


        public void proIngreso(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            //hay que llamar a la dll
            object Clase = null;
            int puntero = 0;
            do
            {
                try
                {
                    //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                    /*Clase = new DocumentoDLL.mcDocumento();
                    if (destino == Crystal.DestinationConstants.crptToPrinter)
                    {					
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                    }
                    else
                    {					
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToWindow, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                    }				
                    Clase.proIngreso(viGanoadme, viGnumadme, stTGidenpac, Dialogo_impresora);

                    Clase = null;*/
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proIngreso:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);
        }

        public void PosicionarChequeo(RadCheckBox oCheq, string stConsGlo, ref int iContador)
        {
            string stsqltemp = "select valfanu1, valfanu1i1 from sconsglo where gconsglo= '" + stConsGlo + "' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
            DataSet rrtemp = new DataSet();
            tempAdapter.Fill(rrtemp);
            if (rrtemp.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "SI" || Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
                {
                    iContador++;
                    oCheq.Visible = true;
                    if ((Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]) + "").Trim() != "")
                    {
                        oCheq.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]).Trim();
                    }
                    oCheq.Top = (int)(ChbAutoEmision.Top + iContador * 300 / 15);
                }
            }
            rrtemp.Close();
        }

        private void Documentos_ingreso_Load(Object eventSender, EventArgs eventArgs)
        {
            int icont = 0; //Indica el n� de chequeos visibles.
            string LitPersona = String.Empty;

            try
            {
                Left = 3; //(Screen.Width - Width) / 2
                Top = 3; //(Screen.Height - Height) / 2

                //'    If stFORMFILI = "V" Then
                //'        ChbVolante.Caption = "Constancia para la organizaci�n"
                //'        ChbJustificante.Caption = "Constancia de hospitalizaci�n"
                //'    End If
                //Colocamos los objetos seg�n el n� de chequeos visibles
                icont = 0;
                PosicionarChequeo(ChbOficio, "PSIQUIAT", ref icont);
                PosicionarChequeo(chbHojaClinEsta, "HOCLIEST", ref icont);
                PosicionarChequeo(chbDeposito, "DEPOSITO", ref icont);
                if (Conexion_Base_datos.stFORMFILI == "E")
                {
                    PosicionarChequeo(chbFactSomb, "FACTSOMB", ref icont);
                }
                PosicionarChequeo(chbContrato, "CONTRATO", ref icont);

                //oscar 15/03/04
                //Nuevos infomes de ECASA
                PosicionarChequeo(chbAutoDatos, "AUTDATOS", ref icont);
                PosicionarChequeo(chbAutoEstudio, "AUTESTCL", ref icont);
                PosicionarChequeo(chbPa�ales, "INFPANAL", ref icont);

                PosicionarChequeo(ChbInterJuzga, "NOJUZING", ref icont);
                PosicionarChequeo(chbDuplicidad, "NODUPEXP", ref icont);
                //-----------------

                //************* O.Frias - 29/09/2008  *************
                //NewProceso -- Aportaci�n Residente Pension
                PosicionarChequeo(chkAportaPension, "APENRESI", ref icont);
                PosicionarChequeo(chbContratoIngreso, "CONINGRE", ref icont);

                //'    stSql = "SELECT ISNULL(valfanu1,'N') as valfanu1 FROM SCONSGLO WHERE gconsglo = 'APENRESI' "
                //'    Set Rr1 = RcAdmision.OpenResultset(stSql, rdOpenKeyset, 1)
                //'If Not Rr1.EOF Then
                //'    If Trim(Rr1("valfanu1")) = "S" Then
                //'        chkAportaPension.Visible = True
                //'    End If
                //'End If

                Frame1.Height = (int)((4500 + icont * 300) / 15);
                cbAceptar.Top = (int)((4500 + icont * 320) / 15);
                cbCancelar.Top = (int)((4500 + icont * 320) / 15);
                this.Height = (int)((5400 + icont * 330) / 15);

                cryInformes = Conexion_Base_datos.LISTADO_CRYSTAL;

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
                Frame3.Text = LitPersona;
            }
            catch
            {
            }
        }

        private void Documentos_ingreso_Closed(Object eventSender, EventArgs eventArgs)
        {
            cryInformes = null;
            menu_ingreso.DefInstance.Llenar_sprGrid();
        }

        private bool isInitializingComponent;

        private void Option1_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (((RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                proSeleccion_Doc2();
            }
        }


        public void proVolante(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            //debe llamar a la dll
            object Clase = null;
            int puntero = 0;
            do
            {

                try
                {
                    //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                    /*Clase = new DocumentoDLL.mcDocumento();
                    if (destino == Crystal.DestinationConstants.crptToPrinter)
                    {
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, cryInformes, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                    }
                    else
                    {
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, cryInformes, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToWindow, mbAcceso.gstUsuario, mbAcceso.gstPassword, Conexion_Base_datos.VstCodUsua);
                    }
                    Clase.proVolante(viGanoadme, viGnumadme, stTGidenpac, Dialogo_impresora, Conexion_Base_datos.DNICEDULA);

                    Clase = null;*/
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proVolante:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);
        }

        public void proFormulas(int idForm)
        {
            int tiForm = 0;
            foreach (var key in cryInformes.Formulas.Keys)
            {
                if (tiForm <= idForm)
                    cryInformes.Formulas[key] = "";
            }
        }


        public void proSerReligioso(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            DLLTextosSql.TextosSql oTextoSql = null;
            string LitPersona = String.Empty;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;
            int puntero = 0;
            do
            {

                try
                {
                    cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi127r1_CR11.rpt";
                    string[] MatrizSql = new string[] { String.Empty, String.Empty, String.Empty, String.Empty };
                    MatrizSql[1] = stTGidenpac;
                    MatrizSql[2] = viGanoadme.ToString();
                    MatrizSql[3] = viGnumadme.ToString();
                    oTextoSql = new DLLTextosSql.TextosSql();
                    string tempRefParam = "ADMISION";
                    string tempRefParam2 = "AGI150F1";
                    string tempRefParam3 = "ProSerReligioso";
                    short tempRefParam4 = 1;
                    object[] tempRefParam5 = MatrizSql;
                    sqlCrystal = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, Conexion_Base_datos.RcAdmision));
                    MatrizSql = (string[])tempRefParam5;
                    oTextoSql = null;

                    //*********cabeceras
                    if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                    {
                        Conexion_Base_datos.proCabCrystal();
                    }
                    //*******************
                    //buscar la poblacion y si es panama el distrito
                    //BuscarPoblacion stTGidenpac

                    //'If stFORMFILI = "V" Then
                    //'    ProvinEstado = "Estado:"
                    //'Else
                    Conexion_Base_datos.ProvinEstado = "Provincia:";
                    //'End If

                    cryInformes.Formulas["{@Cade_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@Nom_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@Dir_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                    cryInformes.Formulas["{@Historia}"] = "\"" + vstHistoria + "\"";
                    cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + Conexion_Base_datos.DNICEDULA + ":" + "\"";
                    cryInformes.Formulas["{@POBLACION}"] = "\"" + BuscarPoblacion(stTGidenpac) + "\"";
                    cryInformes.Formulas["{@Provincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";

                    LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                    LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1) + ":";
                    cryInformes.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";

                    //'If stFORMFILI = "P" Then
                    //'    sql1 = "select * from DPACIENT Where DPACIENT.GIDENPAC ='" & stTGidenpac & "'"
                    //'    Set Rr1 = RcAdmision.OpenResultset(sql1, 1, 1)
                    //'    If Rr1.RowCount = 1 Then
                    //'        cryInformes.Formulas(5) = "lbReligio='Religi�n:'"
                    //'        If Not IsNull(Rr1("greligio")) Then cryInformes.Formulas(6) = "tbReligio= """ & ObtenerDescripcion("dreligio", "greligio", Rr1("greligio"), "dreligio") & """"
                    //'    End If
                    //'    Rr1.Close
                    //'End If
                    cryInformes.Formulas["{@PonerNaseguradora}"] = "\"N\"";
                    if (Serrores.ExisteAseguradora(Conexion_Base_datos.RcAdmision))
                    {
                        cryInformes.Formulas["{@stAseguradora}"] = "'N� Asegurado:'";
                        cryInformes.Formulas["{@PonerNaseguradora}"] = "\"S\"";
                    }
                    cryInformes.WindowTitle = "Hoja de Servicio Religioso";
                    cryInformes.SQLQuery = sqlCrystal;
                    cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    cryInformes.Destination = destino;
                    cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                    cryInformes.WindowShowPrintSetupBtn = true;
                    cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                    cryInformes.Action = 1;
                    cryInformes.PrinterStopPage = cryInformes.PageCount;
                    Conexion_Base_datos.vacia_formulas(cryInformes, 5);
                    cryInformes.Reset();

                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proSerReligioso:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);

        }


        public void proTrabajador(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            string tstFllegada = String.Empty, tstDiag = String.Empty, tstCatadem = String.Empty;
            string tstInspeccion = String.Empty;
            DLLTextosSql.TextosSql oTextoSql = null;
            string LitPersona = String.Empty;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;
            int puntero = 0;


            do
            {

                try
                {
                    cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi128r1_CR11.rpt";
                    string[] MatrizSql = new string[] { String.Empty, String.Empty, String.Empty, String.Empty };
                    MatrizSql[1] = viGanoadme.ToString();
                    MatrizSql[2] = viGnumadme.ToString();
                    MatrizSql[3] = stTGidenpac;
                    oTextoSql = new DLLTextosSql.TextosSql();
                    string tempRefParam = "ADMISION";
                    string tempRefParam2 = "AGI150F1";
                    string tempRefParam3 = "ProTrabajador";
                    short tempRefParam4 = 1;
                    object[] tempRefParam5 = MatrizSql;
                    sqlCrystal = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, Conexion_Base_datos.RcAdmision));
                    MatrizSql = (string[])tempRefParam5;
                    oTextoSql = null;
                    //**********DIAGNOSTICO
                    sql1 = "select GDIAGINI,ODIAGINI,fllegada From AEPISADM where FALTPLAN IS NULL AND FALTAADM IS NULL and gidenpac='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count != 0)
                    {
                        tstFllegada = Convert.ToDateTime(Rr1.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm");

                        if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["GDIAGINI"]))
                        {
                            object tempRefParam6 = Rr1.Tables[0].Rows[0]["FLLEGADA"];
                            object tempRefParam7 = Rr1.Tables[0].Rows[0]["FLLEGADA"];
                            sql2 = "SELECT * FROM DDIAGNOS where GCODIDIA= '" + Convert.ToString(Rr1.Tables[0].Rows[0]["GDIAGINI"]) + "' AND (" + Serrores.FormatFecha(tempRefParam6) + " BETWEEN FINVALDI AND FFIVALDI " + " OR " + " (" + Serrores.FormatFecha(tempRefParam7) + ">=FINVALDI and ffivaldi IS  NULL))";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql2, Conexion_Base_datos.RcAdmision);
                            Rr2 = new DataSet();
                            tempAdapter_2.Fill(Rr2);
                            if (Rr2.Tables[0].Rows.Count == 1)
                            {
                                tstDiag = Convert.ToString(Rr2.Tables[0].Rows[0]["DNOMBDIA"]).Trim();
                            }

                            Rr2.Close();
                        }
                        else if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["ODIAGINI"]))
                        {
                            tstDiag = Convert.ToString(Rr1.Tables[0].Rows[0]["ODIAGINI"]).Trim();
                        }
                    }
                    Rr1.Close();
                    //*******catalogaci�n demografica
                    sql1 = "select DCATADEM.DCATADEM FROM DCATADEM,DPOBLACI,DPACIENT " + " Where  DCATADEM.GCATADEM=DPOBLACI.GCATADEM AND " + " DPOBLACI.GPOBLACI=DPACIENT.GPOBLACI AND DPACIENT.GIDENPAC='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_3.Fill(Rr1);

                    if (Rr1.Tables[0].Rows.Count == 1 && !Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DCATADEM"]))
                    {
                        tstCatadem = Convert.ToString(Rr1.Tables[0].Rows[0]["DCATADEM"]).Trim().ToUpper();
                    }

                    Rr1.Close();
                    //**********inspecci�n
                    sql1 = "select DINSPECC From DPACIENT, DENTIPAC, DINSPECC " + " Where DPACIENT.GIDENPAC =DENTIPAC.GIDENPAC AND " + " DPACIENT.nafiliac = DENTIPAC.nafiliac AND " + " DPACIENT.GSOCIEDA = DENTIPAC.GSOCIEDA AND " + " DENTIPAC.ginspecc = DINSPECC.ginspecc " + " AND DPACIENT.GIDENPAC ='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_4.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count == 1)
                    {
                        tstInspeccion = Convert.ToString(Rr1.Tables[0].Rows[0]["DINSPECC"]);
                    }

                    Rr1.Close();

                    //'If stFORMFILI = "V" Then
                    //'    ProvinEstado = "Estado:"
                    //'Else
                    Conexion_Base_datos.ProvinEstado = "Provincia:";
                    //'End If

                    //*********cabeceras
                    if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                    {
                        Conexion_Base_datos.proCabCrystal();
                    }
                    //******************

                    cryInformes.Formulas["{@Grup_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@Nom_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@Dir_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                    cryInformes.Formulas["{@Historia}"] = "\"" + vstHistoria + "\"";
                    cryInformes.Formulas["{@Diagnostico}"] = "\"" + tstDiag + "\"";
                    cryInformes.Formulas["{@Fllegada}"] = "\"" + tstFllegada + "\"";
                    cryInformes.Formulas["{@Cata_Dem}"] = "\"" + tstCatadem + "\"";
                    cryInformes.Formulas["{@INSPECCION}"] = "\"" + tstInspeccion + "\"";
                    //'If stFORMFILI = "P" Then
                    //'    sql1 = "select * from DPACIENT Where DPACIENT.GIDENPAC ='" & stTGidenpac & "'"
                    //'    Set Rr1 = RcAdmision.OpenResultset(sql1, 1, 1)
                    //'    If Rr1.RowCount = 1 Then
                    //'        cryInformes.Formulas(8) = "lbdiretra='Direcci�n Transitoria:'"
                    //'        If Not IsNull(Rr1("ddiretra")) Then cryInformes.Formulas(9) = "tbdiretra= """ & Trim(Rr1("ddiretra")) & """"
                    //'        cryInformes.Formulas(10) = "lblugtrab='Lugar de trabajo:'"
                    //'        If Not IsNull(Rr1("glugtrab")) Then cryInformes.Formulas(11) = "tblugtrab= """ & ObtenerDescripcion("dlugtrab", "glugtrab", Rr1("glugtrab"), "dlugtrab") & """"
                    //'        cryInformes.Formulas(12) = "lbGruetni='Grupo �tnico:'"
                    //'        If Not IsNull(Rr1("glugtrab")) Then cryInformes.Formulas(13) = "tbGruetni= """ & ObtenerDescripcion("dgruetni", "ggruetni", Rr1("ggruetni"), "dgruetni") & """"
                    //'        cryInformes.Formulas(14) = "lbescolar='Nivel de escolaridad:'"
                    //'        If Not IsNull(Rr1("gescolar")) Then cryInformes.Formulas(15) = "tbescolar= """ & ObtenerDescripcion("descolar", "gescolar", Rr1("gescolar"), "descolar") & """"
                    //'        cryInformes.Formulas(16) = "lbncurapr='N� de cursos aprobados:'"
                    //'        If Not IsNull(Rr1("ncurapro")) Then cryInformes.Formulas(17) = "tbncurapr= """ & Trim(Rr1("ncurapro")) & """"
                    //'    cryInformes.Formulas(18) = "lbCatasoc='Ocupaci�n:'"
                    //'    End If
                    //'    Rr1.Close
                    //'Else
                    cryInformes.Formulas["{@lbCatasoc}"] = "'Catalogaci�n social:'";
                    //'End If
                    //Set Rr1 = RcAdmision.OpenResultset("SELECT * FROM SCONSGLO WHERE GCONSGLO ='IDENPERS'", rdOpenForwardOnly)
                    cryInformes.Formulas["{@tbdni}"] = "\"" + Conexion_Base_datos.DNICEDULA + ":" + "\"";
                    //Rr1.Close
                    cryInformes.Formulas["{@poblacion}"] = "\"" + BuscarPoblacion(stTGidenpac) + "\"";
                    cryInformes.Formulas["{@Provincia}"] = "\"" + Conexion_Base_datos.ProvinEstado + "\"";

                    LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                    LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1) + ":";
                    cryInformes.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";

                    cryInformes.WindowTitle = "Hoja de Trabajador Social";
                    cryInformes.SQLQuery = sqlCrystal;
                    cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    cryInformes.Destination = destino;
                    cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                    cryInformes.WindowShowPrintSetupBtn = true;
                    cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                    cryInformes.Action = 1;
                    cryInformes.PrinterStopPage = cryInformes.PageCount;
                    Conexion_Base_datos.vacia_formulas(cryInformes, 22);
                    cryInformes.Reset();
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proTrabajador:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);

        }

        public void proAusencia(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            string tstNomPac = String.Empty;
            string UnidadEnfermeria = String.Empty;
            string stsqltemp = String.Empty;
            DataSet rrtemp = null;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;
            int puntero = 0;

            do
            {
                try
                {
                    Serrores.Obtener_Multilenguaje(Conexion_Base_datos.RcAdmision, stTGidenpac);
                    cryInformes.ReportFileName = Convert.ToString("" + Conexion_Base_datos.PathString + "\\rpt\\agi12ar1_CR11.rpt");
                    Serrores.ObtenerTextosRPT(Conexion_Base_datos.RcAdmision, "ADMISION", "AGI12AR1");

                    sqlCrystal = "SELECT  dunidenf,dunidenfi1,dunidenfi2,  AAUSENCI.fausenci, AAUSENCI.fprevuel," + "DPACIENT.dnombpac, DPACIENT.dape1pac,DPACIENT.dape2pac,DPACIENT.ndninifp " + " From AAUSENCI,AEPISADM,amovimie, DPACIENT,DUNIENFE " + " Where " + " (AAUSENCI.ganoadme = AEPISADM.ganoadme AND AAUSENCI.gnumadme = AEPISADM.gnumadme " + " AND AAUSENCI.gmotause = AEPISADM.gmotause AND AEPISADM.gidenpac = DPACIENT.gidenpac " + " AND  AEPISADM.ganoadme =amovimie.ganoregi " + " AND  AEPISADM.gnumadme =amovimie.gnumregi  AND" + " AMOVIMIE.GUNENFOR=DUNIENFE.gunidenf " + " AND  AEPISADM.ganoadme = " + viGanoadme.ToString() + " AND  AEPISADM.gnumadme = " + viGnumadme.ToString() + " AND  AEPISADM.gidenpac ='" + stTGidenpac + "')" + " order by AAUSENCI.fausenci";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCrystal, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter.Fill(Rr1);
                    string LitPersona = String.Empty;
                    if (Rr1.Tables[0].Rows.Count > 0)
                    { //ha solicitado alguna ausencia

                        Rr1.MoveLast(null);

                        if (Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE1PAC"]) != "")
                        {
                            tstNomPac = Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE1PAC"]).Trim();
                        }

                        if (Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE2PAC"]) != "")
                        {
                            tstNomPac = tstNomPac + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
                        }

                        if (Convert.ToString(Rr1.Tables[0].Rows[0]["DNOMBPAC"]) != "")
                        {
                            tstNomPac = tstNomPac + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["DNOMBPAC"]).Trim();
                        }
                        //*********cabeceras
                        if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                        {
                            Conexion_Base_datos.proCabCrystal();
                        }
                        //*******************
                        if (Serrores.CampoIdioma == "IDIOMA0")
                        {
                            UnidadEnfermeria = (Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DUNIDENF"])) ? "" : Convert.ToString(Rr1.Tables[0].Rows[0]["DUNIDENF"]);
                        }
                        else if (Serrores.CampoIdioma == "IDIOMA1")
                        {
                            UnidadEnfermeria = (Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DUNIDENFI1"])) ? "" : Convert.ToString(Rr1.Tables[0].Rows[0]["DUNIDENFI1"]);
                        }
                        else if (Serrores.CampoIdioma == "IDIOMA2")
                        {
                            UnidadEnfermeria = (Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DUNIDENFI2"])) ? "" : Convert.ToString(Rr1.Tables[0].Rows[0]["DUNIDENFI2"]);
                        }

                        cryInformes.Formulas["{@Grup_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                        cryInformes.Formulas["{@Nom_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                        cryInformes.Formulas["{@Dir_hos}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                        cryInformes.Formulas["{@Nom_Pac}"] = "\"" + tstNomPac + "\"";
                        cryInformes.Formulas["{@Fec_aus}"] = "\"" + Convert.ToDateTime(Rr1.Tables[0].Rows[0]["fausenci"]).ToString("dd/MM/yyyy") + "\"";
                        cryInformes.Formulas["{@Fec_vuel}"] = "\"" + Convert.ToDateTime(Rr1.Tables[0].Rows[0]["fprevuel"]).ToString("dd/MM/yyyy") + "\"";
                        cryInformes.Formulas["{@Uni_hos}"] = "\"" + UnidadEnfermeria + "\"";
                        cryInformes.Formulas["{@DNI}"] = "'" + ((!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["NDNINIFP"])) ? Convert.ToString(Rr1.Tables[0].Rows[0]["NDNINIFP"]) : "") + "'";


                        stsqltemp = "select valfanu1,valfanu1i1,valfanu1i2 from sconsglo where gconsglo= 'idenpers' ";
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                        rrtemp = new DataSet();
                        tempAdapter_2.Fill(rrtemp);
                        if (rrtemp.Tables[0].Rows.Count != 0)
                        {
                            switch (Serrores.CampoIdioma)
                            {
                                case "IDIOMA0":
                                    Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim() + "";
                                    break;
                                case "IDIOMA1":
                                    Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i1"]).Trim() + "";
                                    break;
                                case "IDIOMA2":
                                    Conexion_Base_datos.DNICEDULA = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1i2"]).Trim() + "";
                                    break;
                            }
                            //--------
                        }
                        rrtemp.Close();

                        Conexion_Base_datos.DNICEDULA = Conexion_Base_datos.DNICEDULA + ": ";
                        cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + Conexion_Base_datos.DNICEDULA + "\"";
                        cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";
                        LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision, Serrores.CampoIdioma);
                        LitPersona = LitPersona + ":";
                        cryInformes.Formulas["{@LitPers}"] = "\"" + LitPersona + "\"";

                        //'    cryInformes.WindowTitle = "Solicitud de Ausencia Temporal"
                        cryInformes.WindowTitle = Serrores.VarRPT[1];
                        cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                        cryInformes.Destination = Crystal.DestinationConstants.crptToWindow; //destino
                        cryInformes.WindowShowPrintSetupBtn = true;
                        cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                        cryInformes.SubreportToChange = "LogotipoIDC";
                        cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                        cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                        cryInformes.SubreportToChange = "";
                        Serrores.LLenarFormulas(Conexion_Base_datos.RcAdmision, cryInformes, "AGI12AR1", "AGI12AR1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT

                        cryInformes.Action = 1;
                        cryInformes.PrinterStopPage = cryInformes.PageCount;
                        Conexion_Base_datos.vacia_formulas(cryInformes, 10);
                        cryInformes.Reset();

                        Rr1.Close();

                    }
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proAusencia:DocumentosIngreso", ex);
                    
                }
            }
            while (puntero >= 1);

        }

        public void proCheckBox()
        {
            chbEtiqueta.CheckState = CheckState.Unchecked;
            chbIngreso.CheckState = CheckState.Unchecked;
            chbIntervencion.CheckState = CheckState.Unchecked;
            chbDatos.CheckState = CheckState.Unchecked;
            chbVolante.CheckState = CheckState.Unchecked;
            Chbreligioso.CheckState = CheckState.Unchecked;
            Chbtrabajador.CheckState = CheckState.Unchecked;
            ChbHaceconstar.CheckState = CheckState.Unchecked;
            ChbAusencia.CheckState = CheckState.Unchecked;
            ChbJustificante.CheckState = CheckState.Unchecked;
            ChbJusAcompa.CheckState = CheckState.Unchecked;
            ChbAutoEmision.CheckState = CheckState.Unchecked;
            ChbOficio.CheckState = CheckState.Unchecked;
            chbHojaClinEsta.CheckState = CheckState.Unchecked;
            chbDeposito.CheckState = CheckState.Unchecked;
            chbFactSomb.CheckState = CheckState.Unchecked;
            chbContrato.CheckState = CheckState.Unchecked;
            chbAutoDatos.CheckState = CheckState.Unchecked;
            chbAutoEstudio.CheckState = CheckState.Unchecked;
            chbPa�ales.CheckState = CheckState.Unchecked;
            ChbInterJuzga.CheckState = CheckState.Unchecked;
            chbDuplicidad.CheckState = CheckState.Unchecked;
            chkAportaPension.CheckState = CheckState.Unchecked;
            chbContratoIngreso.CheckState = CheckState.Unchecked;
        }

        public void proConstar(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            int CODIGO_PROVINCIA = 0;
            DataSet rrCrystal = null;
            string sqlCrystal = String.Empty;
            string strPrivado = String.Empty;
            string sql = String.Empty;
            DataSet RR = null;
            bool iExtranjero = false;
            string litperso = String.Empty;
            string LitPersona = String.Empty;
            string MaysculaOMinuscula = String.Empty;
            string vstGidenpac = String.Empty;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;

            //Dim MatrizSql() As Variant
            //Dim oTextoSql As Object
            try
            {
                iExtranjero = false;

                cryInformes.Reset();

                bool bFirmaDigital = false;
                bFirmaDigital = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FIRMADIG", "VALFANU1") == "S";


                MaysculaOMinuscula = "m";

                sql = " SELECT gidenpac From  AEPISADM " +
                      " Where ganoadme = " + viGanoadme.ToString() + " and " +
                      " gnumadme = " + viGnumadme.ToString() + " ";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                RR = new DataSet();
                tempAdapter.Fill(RR);
                if (RR.Tables[0].Rows.Count != 0)
                {
                    vstGidenpac = Convert.ToString(RR.Tables[0].Rows[0]["gidenpac"]);
                }

                RR.Close();

                Serrores.Obtener_Multilenguaje(Conexion_Base_datos.RcAdmision, vstGidenpac);
                Serrores.ObtenerTextosRPT(Conexion_Base_datos.RcAdmision, "ADMISION", "AGI129R1");
                Serrores.ObtenerMesesSemanas(Conexion_Base_datos.RcAdmision);

                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }

                sql = "select valfanu1,valfanu1i1,valfanu1i2 from sconsglo where gconsglo= 'idenpers' ";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                RR = new DataSet();
                tempAdapter_2.Fill(RR);
                if (RR.Tables[0].Rows.Count != 0)
                {
                    switch (Serrores.CampoIdioma)
                    {
                        case "IDIOMA0":
                            Conexion_Base_datos.DniCedulaMay = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() + "";  //en mayusculas 
                            break;
                        case "IDIOMA1":
                            Conexion_Base_datos.DniCedulaMay = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1i1"]).Trim().ToUpper() + "";  //en mayusculas 
                            break;
                        case "IDIOMA2":
                            Conexion_Base_datos.DniCedulaMay = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1i2"]).Trim().ToUpper() + "";  //en mayusculas 
                            break;
                    }
                }

                RR.Close();


                cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi129r1_CR11.rpt";

                cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@DNI/CEDULA}"] = "\"" + Conexion_Base_datos.DniCedulaMay + ":" + "\"";

                sqlCrystal = "select amovifin.* from amovifin,sconsglo " +
                             " where amovifin.ganoregi =  " + viGanoadme.ToString() + " and " +
                             " amovifin.gnumregi = " + viGnumadme.ToString() + " and " +
                             " amovifin.ffinmovi is null and " +
                             " amovifin.itiposer = 'H' and " +
                             " amovifin.gsocieda = nnumeri1 and " +
                             " gconsglo = 'PRIVADO'";

                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlCrystal, Conexion_Base_datos.RcAdmision);
                rrCrystal = new DataSet();
                tempAdapter_3.Fill(rrCrystal);
                strPrivado = (rrCrystal.Tables[0].Rows.Count == 0) ? "DPERSCON" : "DPRIVADO";
                if (strPrivado == "DPRIVADO")
                {
                    //
                    sql1 = "SELECT DPRIVADO.dape1pri, DPRIVADO.dape2pri, DPRIVADO.dnombpri, ";
                    sql1 = sql1 + "DPRIVADO.nnifpriv, DPRIVADO.ddirepri, DPRIVADO.gcodipos, ";
                    sql1 = sql1 + "DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, ";
                    sql1 = sql1 + "DPACIENT.ndninifp ";
                    sql1 = sql1 + "From aepisadm   ";
                    sql1 = sql1 + "INNER JOIN DPACIENT on aepisadm.gidenpac = DPACIENT.gidenpac  ";
                    sql1 = sql1 + "LEFT JOIN DPRIVADO on aepisadm.gidenpac = DPRIVADO.gidenpac ";
                    sql1 = sql1 + " Where aepisadm.Ganoadme =" + viGanoadme.ToString() + " and ";
                    sql1 = sql1 + " aepisadm.Gnumadme = " + viGnumadme.ToString() + " ";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_4.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count != 0)
                    {
                        cryInformes.Formulas["{@Nombre_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["dape1pri"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dape2pri"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dnombpri"]).Trim() + " \"";
                        cryInformes.Formulas["{@DNI_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["nnifpriv"]).Trim() + "\"";
                        cryInformes.Formulas["{@Direccion_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["ddirepri"]).Trim() + "\"";
                        cryInformes.Formulas["{@CP_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["gcodipos"]).Trim() + " \"";
                        cryInformes.Formulas["{@telf_privado}"] = "";
                    }

                    sql = " SELECT DPRIVADO.gpoblaci,DPRIVADO.gpaisres " +
                          " From aepisadm, DPRIVADO " +
                          " Where aepisadm.ganoadme = " + viGanoadme.ToString() + " and " +
                          " aepisadm.gnumadme = " + viGnumadme.ToString() + " and " +
                          " aepisadm.gidenpac = DPRIVADO.gidenpac ";
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RR = new DataSet();
                    tempAdapter_5.Fill(RR);
                    if (RR.Tables[0].Rows.Count != 0)
                    {
                        if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["gpoblaci"]))
                        {
                            CODIGO_PROVINCIA = Convert.ToInt32(RR.Tables[0].Rows[0]["gpoblaci"]);
                        }
                        else
                        {
                            if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["gpaisres"]))
                            {
                                CODIGO_PROVINCIA = Convert.ToInt32(RR.Tables[0].Rows[0]["gpaisres"]);
                                iExtranjero = true;
                            }
                        }
                        RR.Close();
                    }
                }
                else
                {
                    sql1 = "SELECT DPERSCON.dape1per, DPERSCON.dape2per, DPERSCON.dnombper, ";
                    sql1 = sql1 + "DPERSCON.ddireper, DPERSCON.gcodipos as gcodiposDPERSCON, DPERSCON.ntelefo1 TelfContac, ";
                    sql1 = sql1 + "DPERSCON.irespons, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, ";
                    sql1 = sql1 + "DPACIENT.ndninifp, DPACIENT.ddirepac, DPACIENT.gcodipos as gcodiposDPACIENT, ";
                    sql1 = sql1 + "DPACIENT.ntelefo1 TelfPaciente, ";
                    sql1 = sql1 + "DPERSCON.ndninifp as DniContacto ";
                    sql1 = sql1 + "From aepisadm  ";
                    sql1 = sql1 + "INNER JOIN DPACIENT on aepisadm.gidenpac =  DPACIENT.gidenpac ";
                    sql1 = sql1 + "LEFT JOIN DPERSCON ON aepisadm.gidenpac = DPERSCON.gidenpac AND IRESPONS='S'   ";
                    sql1 = sql1 + "Where aepisadm.Ganoadme =" + viGanoadme.ToString() + " and ";
                    sql1 = sql1 + "aepisadm.Gnumadme = " + viGnumadme.ToString() + "  ";
                    sql1 = sql1 + "  ";
                    sql1 = sql1 + " ";
                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                    Rr1 = new DataSet();
                    tempAdapter_6.Fill(Rr1);
                    if (Rr1.Tables[0].Rows.Count != 0)
                    {
                        if (Convert.IsDBNull(Rr1.Tables[0].Rows[0]["irespons"]))
                        {
                            cryInformes.Formulas["{@Nombre_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dape2pac"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dnombpac"]).Trim() + " \"";
                            cryInformes.Formulas["{@DNI_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["ndninifp"]).Trim() + "\"";
                            cryInformes.Formulas["{@Direccion_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["ddirepac"]).Trim() + " \"";
                            cryInformes.Formulas["{@CP_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["gcodiposDPACIENT"]).Trim() + " \"";
                            cryInformes.Formulas["{@telf_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["TelfPaciente"]).Trim() + " \"";

                            sql = " SELECT DPACIENT.gpoblaci,DPACIENT.GPAISRES " +
                                  " From aepisadm, DPACIENT " +
                                  " Where aepisadm.ganoadme = " + viGanoadme.ToString() + " and " +
                                  " aepisadm.gnumadme = " + viGnumadme.ToString() + " and " +
                                  " aepisadm.gidenpac = DPACIENT.gidenpac ";
                            SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                            RR = new DataSet();
                            tempAdapter_7.Fill(RR);
                            if (RR.Tables[0].Rows.Count != 0)
                            {
                                if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["gpoblaci"]))
                                {
                                    CODIGO_PROVINCIA = Convert.ToInt32(RR.Tables[0].Rows[0]["gpoblaci"]);
                                }
                                else
                                {
                                    if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["gpaisres"]))
                                    {
                                        CODIGO_PROVINCIA = (Convert.ToInt32(RR.Tables[0].Rows[0]["gpaisres"]));
                                        iExtranjero = true;
                                    }
                                }
                            }
                            RR.Close();
                        }
                        else
                        {
                            sql = " SELECT DPERSCON.gpoblaci ,dperscon.gpaisres From aepisadm, DPERSCON " +
                                  " Where aepisadm.ganoadme = " + viGanoadme.ToString() + " and " +
                                  " aepisadm.gnumadme  = " + viGnumadme.ToString() + " and " +
                                  " aepisadm.gidenpac = DPERSCON.gidenpac and " +
                                  " dperscon.IRESPONS='S'";
                            SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                            RR = new DataSet();
                            tempAdapter_8.Fill(RR);
                            if (RR.Tables[0].Rows.Count != 0)
                            {
                                if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["gpoblaci"]))
                                {
                                    CODIGO_PROVINCIA = Convert.ToInt32(RR.Tables[0].Rows[0]["gpoblaci"]);
                                }
                                else
                                {
                                    if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["gpaisres"]))
                                    {
                                        CODIGO_PROVINCIA = (Convert.ToInt32(RR.Tables[0].Rows[0]["gpaisres"]));
                                        iExtranjero = true;
                                    }
                                }
                                RR.Close();
                            }

                            cryInformes.Formulas["{@Nombre_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["dape1per"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dape2per"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dnombper"]).Trim() + " \"";
                            cryInformes.Formulas["{@DNI_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["DniContacto"]).Trim() + "\"";
                            cryInformes.Formulas["{@Direccion_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["ddireper"]).Trim() + " \"";
                            cryInformes.Formulas["{@CP_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["gcodiposDPERSCON"]).Trim() + " \"";
                            cryInformes.Formulas["{@telf_privado}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["TelfContac"]).Trim() + " \"";

                        }
                    }
                }
                if (CODIGO_PROVINCIA != 0 && !iExtranjero)
                {
                    sql = " SELECT dpoblaci.dpoblaci From dpoblaci " +
                          " Where dpoblaci.gpoblaci = '" + StringsHelper.Format(CODIGO_PROVINCIA, "000000") + "' ";
                    SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                    RR = new DataSet();
                    tempAdapter_9.Fill(RR);
                    if (RR.Tables[0].Rows.Count != 0)
                    {
                        cryInformes.Formulas["{@Poblacion_privado}"] = "\"" + Convert.ToString(RR.Tables[0].Rows[0]["dpoblaci"]).Trim() + "\"";
                    }
                    RR.Close();
                }
                else
                {
                    if (CODIGO_PROVINCIA != 0 && iExtranjero)
                    {
                        sql = " SELECT dpaisres.dpaisres From dpaisres " +
                              " Where dpaisres.gpaisres =  '" + CODIGO_PROVINCIA.ToString() + "'  ";
                        SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                        RR = new DataSet();
                        tempAdapter_10.Fill(RR);
                        if (RR.Tables[0].Rows.Count != 0)
                        {
                            cryInformes.Formulas["{@Poblacion_privado}"] = "\"" + Convert.ToString(RR.Tables[0].Rows[0]["dpaisres"]).Trim() + "\"";
                        }
                        RR.Close();
                    }
                }

                cryInformes.Formulas["{@Nombre_paciente}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dape2pac"]).Trim() + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["dnombpac"]).Trim() + " \"";
                cryInformes.Formulas["{@Paciente_DNI}"] = "\"" + Convert.ToString(Rr1.Tables[0].Rows[0]["ndninifp"]).Trim() + " \"";
                cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";
                cryInformes.Formulas["{@FechaHoy}"] = "\"" + Serrores.CapturarFechaHoy(MaysculaOMinuscula) + "\"";

                LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision, Serrores.CampoIdioma);
                //litperso = "Como responsable del " & LitPersona & "."
                litperso = Serrores.VarRPT[1] + " " + LitPersona + ".";

                cryInformes.Formulas["{@LitPers}"] = "\"" + litperso + "\"";

                //O.Frias - 04/05/2009
                //Se incorpora la funcion de usuario dependiendo de las constantes IUSULARG y IUSUDOCU
                cryInformes.Formulas["{@Usuario}"] = "\"" + Serrores.fUsuarInfor(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision) + "\"";

                //Oscar C 26 de Agosto de 2010
                sql = "select dsocieda from dsocieda " +
                      "  inner join amovifin on amovifin.gsocieda = dsocieda.gsocieda " +
                      "  where amovifin.ganoregi =  " + viGanoadme.ToString() + " and " +
                      "  amovifin.gnumregi = " + viGnumadme.ToString() + " and " +
                      "  amovifin.ffinmovi is null and " +
                      "  amovifin.itiposer = 'H' ";
                SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
                RR = new DataSet();
                tempAdapter_11.Fill(RR);
                if (RR.Tables[0].Rows.Count != 0)
                {
                    cryInformes.Formulas["{@Sociedad}"] = "\"" + Convert.ToString(RR.Tables[0].Rows[0]["dsocieda"]).Trim() + "\"";
                }

                RR.Close();
                cryInformes.Formulas["{@PIELEON}"] = "\"" + Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "PIECOMPA", "VALFANU1") + "\"";
                //-----------------
                cryInformes.Formulas["{@PanelFirma}"] = "\"" + Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FIRMADIG", "VALFANU1") + "\"";

                Rr1.Close();

                cryInformes.SQLQuery = "select fila_id,slogodoc,stextovert,sfirmadoc from SLOGODOC";
                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                cryInformes.Destination = destino; //0 'destino
                cryInformes.WindowShowPrintSetupBtn = true;
                cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                cryInformes.SubreportToChange = "LogotipoIDC";
                cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                cryInformes.SubreportToChange = "";
                Serrores.LLenarFormulas(Conexion_Base_datos.RcAdmision, cryInformes, "AGI129R1", "AGI129R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT

                if (!bFirmaDigital || !bsFirmaDigitalizada.fExistePDFCreator())
                {
                    cryInformes.Action = 1;
                }

                cryInformes.PrinterStopPage = cryInformes.PageCount;
                Application.DoEvents();

                //INTEGRACION CON LA FIRMA DIGITAL MANUSCRITA
                //********************************************
                if (bFirmaDigital)
                {
                    //bsFirmaDigitalizada.proEmisionCompromisoPago_FirmaDigitalPaciente(Conexion_Base_datos.RcAdmision, cryInformes, vstGidenpac, Conexion_Base_datos.VstCodUsua, "H", viGanoadme, viGnumadme);
                    if (Conexion_Base_datos.vImpre_defecto != "")
                    {
                        Serrores.proEstablecerImpresoraPredeterminada(Conexion_Base_datos.vImpre_defecto);
                    }
                    this.Show();
                }

                Conexion_Base_datos.vacia_formulas(cryInformes, 13);
                cryInformes.Reset();

            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proConstar:DocumentosIngreso", ex);
                //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
            }
        }

        private void proContratoIngreso(object Dialogo_impresora, int destino, int loNumCopias)
        {
            object Clase = null;
            int puntero = 0;
            do
            {
                try
                {
                    //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                    /*Clase = new DocumentoDLL.mcDocumento();
                    if (destino == ((int) Crystal.DestinationConstants.crptToPrinter))
                    {
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword);
                    }
                    else
                    {
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToWindow, mbAcceso.gstUsuario, mbAcceso.gstPassword);
                    }
                    Clase.proContratoIngreo(Dialogo_impresora, "H", viGanoadme, viGnumadme, loNumCopias);
                    Clase = null;*/
                    puntero = 0;
                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proContratoIngreso:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);
        }

        public void proLOPD(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            object Clase = null;
            int puntero = 0;
            do
            {
                try
                {
                    bool bFirmaDigital = false;
                    bFirmaDigital = Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FIRMADIG", "VALFANU1") == "S";

                    //DGMORENOG TODO_7_4 - DLL DocumentoDLL
                    /*Clase = new DocumentoDLL.mcDocumento();
                    if (destino == Crystal.DestinationConstants.crptToPrinter)
                    {					
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword);
                    }
                    else
                    {					
                        Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToWindow, mbAcceso.gstUsuario, mbAcceso.gstPassword);
                    }				
                    Clase.proLOPD(stTGidenpac, Dialogo_impresora, Conexion_Base_datos.StBaseTemporal, Conexion_Base_datos.VstCodUsua.ToUpper(), "H", viGanoadme, viGnumadme, null, true);*/

                    if (bFirmaDigital)
                    {
                        if (Conexion_Base_datos.vImpre_defecto != "")
                        {
                            Serrores.proEstablecerImpresoraPredeterminada(Conexion_Base_datos.vImpre_defecto);
                        }
                        this.Show();
                    }
                    Clase = null;
                    puntero = 0;

                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proLOPD:DocumentosIngreso", ex);
                }
            }
            while (puntero >= 1);
        }

        public void proIntervencion(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            object Clase = null;

            try
            {
                //DGMORENOG TODO_7_4 - DLL DocumentoDLL - INICIO
                /*Clase = new DocumentoDLL.mcDocumento();
				if (destino == Crystal.DestinationConstants.crptToPrinter)
				{
					Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToPrinter, mbAcceso.gstUsuario, mbAcceso.gstPassword);
				}
				else
				{
					Clase.proConexion(this, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.LISTADO_CRYSTAL, Conexion_Base_datos.PathString, Crystal.DestinationConstants.crptToWindow, mbAcceso.gstUsuario, mbAcceso.gstPassword);
				}
				//(maplaza)(14/11/2006)Cuando se imprime el Consentimiento Informado, es necesario pasar el usuario. Y tambi�n se
				//pasa el tipo de servicio (que ser� "H") y los datos del episodio.
				//Clase.proConsentimiento stTGidenpac, Dialogo_impresora, StBaseTemporal, stNombreDsnACCESS
				Clase.proConsentimiento(stTGidenpac, Dialogo_impresora, Conexion_Base_datos.StBaseTemporal, Conexion_Base_datos.stNombreDsnACCESS, Conexion_Base_datos.VstCodUsua.ToUpper(), "H", viGanoadme, viGnumadme);*/
                //DGMORENOG TODO_7_4 - DLL DocumentoDLL - FIN
                //-----
                if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "FIRMADIG", "VALFANU1") == "S")
                {
                    if (Conexion_Base_datos.vImpre_defecto != "")
                    {
                        Serrores.proEstablecerImpresoraPredeterminada(Conexion_Base_datos.vImpre_defecto);
                    }
                    this.Show();
                }
                Clase = null;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proIntervencion:DocumentosIngreso", ex);
                //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
            }
        }

        public void proIOficio(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            string stsqltemp = String.Empty;
            string fregreso = String.Empty, txtingreso = String.Empty;
            DataSet rrtemp = null;
            string nomprovi = String.Empty;
            string edad = String.Empty;
            DLLTextosSql.TextosSql oTextoSql = null;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;
            int puntero = 0;
            do
            {
                try
                {
                    stsqltemp = "select ireingre from aepisadm where AEPISADM.ganoadme = " + viGanoadme.ToString() + " AND  AEPISADM.gnumadme = " + viGnumadme.ToString() + " AND  AEPISADM.gidenpac ='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToString(rrtemp.Tables[0].Rows[0]["ireingre"]).Trim().ToUpper() == "S" || Convert.ToString(rrtemp.Tables[0].Rows[0]["ireingre"]).Trim().ToUpper() == DBNull.Value.ToString())
                        {
                            // ha reingresado
                            txtingreso = " reingresado ";
                        }
                        else
                        {
                            stsqltemp = " select max(fausenci), freavuel from aausenci where AAUSENCI.ganoadme = " + viGanoadme.ToString() + " AND  AAUSENCI.gnumadme = " + viGnumadme.ToString() + " group by freavuel ORDER BY freavuel";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                            rrtemp = new DataSet();
                            tempAdapter_2.Fill(rrtemp);
                            if (rrtemp.Tables[0].Rows.Count != 0)
                            {
                                if (!Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["freavuel"]))
                                {
                                    fregreso = Convert.ToDateTime(rrtemp.Tables[0].Rows[0]["freavuel"]).ToString("dd/MM/yyyy HH:mm");
                                    txtingreso = " reingresado ";
                                }
                                else
                                {
                                    txtingreso = " ingresado ";
                                }
                            }
                            else
                            {
                                txtingreso = " ingresado ";
                            }
                        }
                    }

                    rrtemp.Close();
                    stsqltemp = "select fnacipac  from dpacient where dpacient.gidenpac ='" + stTGidenpac + "'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_3.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count > 0)
                    {
                        if (!Convert.IsDBNull(rrtemp.Tables[0].Rows[0]["fnacipac"]))
                        {
                            edad = ((int)DateAndTime.DateDiff("yyyy", Convert.ToDateTime(rrtemp.Tables[0].Rows[0]["fnacipac"]), DateTime.Now, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, Microsoft.VisualBasic.FirstWeekOfYear.Jan1)).ToString();
                        }
                        else
                        {
                            edad = "      ";
                        }
                    }
                    rrtemp.Close();

                    stsqltemp = " select dnomprov from sconsglo, dprovinc  where" + " sconsglo.gconsglo  = 'CODPROVI' and dprovinc.gprovinc = sconsglo.valfanu1";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
                    rrtemp = new DataSet();
                    tempAdapter_4.Fill(rrtemp);
                    if (rrtemp.Tables[0].Rows.Count != 0)
                    {
                        nomprovi = Convert.ToString(rrtemp.Tables[0].Rows[0]["dnomprov"]).Trim();
                    }
                    else
                    {
                        nomprovi = "";
                    }
                    rrtemp.Close();

                    //*********cabeceras
                    if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                    {
                        Conexion_Base_datos.proCabCrystal();
                    }
                    //*******************

                    cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi581r1_CR11.rpt";

                    //    cryInformes.SQLQuery = " SELECT DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " & _
                    //'    " DPOBLACI.dpoblaci, destaciv.destaciv, DPOBLACI2.dpoblaci " & _
                    //'    " from ((DPACIENT left JOIN DESTACIV ON DPACIENT.gestaciv = DESTACIV.gestaciv) " & _
                    //'    " LEFT JOIN DPOBLACI DPOBLACI2 ON DPACIENT.gpoblaci = DPOBLACI2.gpoblaci) " & _
                    //'    " LEFT JOIN DPOBLACI ON DPACIENT.gpobnaci = DPOBLACI.gpoblaci where " & _
                    //'    " dpacient.gidenpac ='" & stTGidenpac & "'"

                    string[] MatrizSql = new string[] { String.Empty, String.Empty };
                    MatrizSql[1] = stTGidenpac;
                    oTextoSql = new DLLTextosSql.TextosSql();

                    string tempRefParam = "ADMISION";
                    string tempRefParam2 = "AGI150F1";
                    string tempRefParam3 = "ProIOficio";
                    short tempRefParam4 = 1;
                    object[] tempRefParam5 = MatrizSql;


                    cryInformes.SQLQuery = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, Conexion_Base_datos.RcAdmision));
                    MatrizSql = (string[])tempRefParam5;
                    oTextoSql = null;

                    cryInformes.Formulas["{@FOR1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    cryInformes.Formulas["{@FOR2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    cryInformes.Formulas["{@FOR3}"] = "\"" + nomprovi + "\"";
                    cryInformes.Formulas["{@dprovincia}"] = "\"" + nomprovi + "\"";
                    cryInformes.Formulas["{@edad}"] = "\"" + edad + "\"";
                    cryInformes.Formulas["{@txtingreso2}"] = "\"" + txtingreso + "\"";


                    cryInformes.WindowTitle = "Oficio";
                    cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                    cryInformes.Destination = destino;
                    cryInformes.WindowShowPrintSetupBtn = true;
                    cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                    cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                    cryInformes.Action = 1;
                    cryInformes.PrinterStopPage = cryInformes.PageCount;
                    Conexion_Base_datos.vacia_formulas(cryInformes, 7);
                    cryInformes.Reset();
                    puntero = 0;

                }
                catch (Exception ex)
                {
                    Serrores.AnalizaError("Admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.gUsuario, "proAusencia:DocumentosIngreso", ex);
                    puntero = 1;
                }
            }
            while (puntero >= 1);

        }

        //private string ObtenerDescripcion(string Tabla, string Campocod, string Codigo, string Descripcion)
        //{
        //string result = String.Empty;
        //result = "";
        //string sql = "select " + Tabla + " from " + Tabla + " where " + Campocod + " = " + Conversion.Val(Codigo).ToString();
        //SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion_Base_datos.RcAdmision);
        //DataSet RrSql = new DataSet();
        //tempAdapter.Fill(RrSql);
        //if (RrSql.Tables[0].Rows.Count != 0)
        //{
        //result = Convert.ToString(RrSql.Tables[0].Rows[0]["& Descripcion & "]).Trim();
        //}
        //RrSql.Close();
        //return result;
        //}

        public string BuscarPoblacion(string gidenpac)
        {
            string result = String.Empty;

            result = "";

            string sqlPob = "select DPOBLACI.dpoblaci,DPOBLACI.gpoblaci, " +
                            "DPacient.dpoblaci poblacion, " +
                            "dpaisres.dpaisres " +
                            "from dpacient " +
                            "left join dpoblaci on dpacient.gpoblaci = dpoblaci.gpoblaci " +
                            "left join dpaisres on dpacient.gpaisres = dpaisres.gpaisres " +
                            "where dpacient.gidenpac ='" + gidenpac + "'";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPob, Conexion_Base_datos.RcAdmision);
            DataSet RrPob = new DataSet();
            tempAdapter.Fill(RrPob);
            if (RrPob.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaci"]))
                {
                    result = Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaci"]);
                    //'        If stFORMFILI = "P" Then
                    //'            Dim CodPobla As String
                    //'            CodPobla = RrPob("gpoblaci")
                    //'            SqlDis = "select * from DDISTRIT where gprovinc = '" & Mid(CodPobla, 1, 2) & "' and gdistrit = '" & Mid(CodPobla, 3, 2) & "'"
                    //'            Set RrDis = RcAdmision.OpenResultset(SqlDis)
                    //'            If Not RrDis.EOF Then
                    //'                BuscarPoblacion = BuscarPoblacion & " - " & RrDis("ddistrit")
                    //'            End If
                    //'            RrDis.Close
                    //'        End If
                }
                else
                {
                    result = (!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["poblacion"])) ? Convert.ToString(RrPob.Tables[0].Rows[0]["poblacion"]) + " - " : "";
                    result = result + ((!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpaisres"])) ? Convert.ToString(RrPob.Tables[0].Rows[0]["dpaisres"]) : "");
                }
            }
            RrPob.Close();

            return result;
        }

        public string BuscarPoblacionContacto(string gidenpac)
        {
            string result = String.Empty;

            result = "";

            string sqlPob = "select DPOBLACI.dpoblaci,DPOBLACI.gpoblaci, " +
                            "DPERSCON.DPOBLACI poblacion, " +
                            "dpaisres.dpaisres " +
                            "from DPERSCON " +
                            "left join dpoblaci on DPERSCON.gpoblaci = dpoblaci.gpoblaci " +
                            "left join dpaisres on DPERSCON.gpaisres = dpaisres.gpaisres " +
                            "where DPERSCON.gidenpac ='" + gidenpac + "'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPob, Conexion_Base_datos.RcAdmision);
            DataSet RrPob = new DataSet();
            tempAdapter.Fill(RrPob);
            if (RrPob.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaci"]))
                {
                    result = Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaci"]);
                    //'        If stFORMFILI = "P" Then
                    //'            Dim CodPobla As String
                    //'            CodPobla = RrPob("gpoblaci")
                    //'            SqlDis = "select * from DDISTRIT where gprovinc = '" & Mid(CodPobla, 1, 2) & "' and gdistrit = '" & Mid(CodPobla, 3, 2) & "'"
                    //'            Set RrDis = RcAdmision.OpenResultset(SqlDis)
                    //'            If Not RrDis.EOF Then
                    //'                BuscarPoblacionContacto = BuscarPoblacionContacto & " - " & RrDis("ddistrit")
                    //'            End If
                    //'            RrDis.Close
                    //'        End If
                }
                else
                {
                    result = (!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["poblacion"])) ? Convert.ToString(RrPob.Tables[0].Rows[0]["poblacion"]) + " - " : "";
                    result = result + ((!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpaisres"])) ? Convert.ToString(RrPob.Tables[0].Rows[0]["dpaisres"]) : "");
                }
            }
            RrPob.Close();

            return result;
        }

        private void proContrato(string Operacion)
        {
            try
            {
                LlamarOperacionesFMS.clsOperFMS ClasePedidos = null; //New LlamarOperacionesFMS.clsOperFMS
                object objTipoPrivado = null;
                string APE1 = String.Empty;
                string APE2 = String.Empty;
                string stSql = String.Empty;
                DataSet RrSql = null;

                stSql = "select DAPE1PAC, DAPE2PAC from DPACIENT where GIDENPAC  = '" + stTGidenpac + "'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                RrSql = new DataSet();
                tempAdapter.Fill(RrSql);
                if (RrSql.Tables[0].Rows.Count != 0)
                {
                    if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["DAPE1PAC"]))
                    {
                        APE1 = Convert.ToString(RrSql.Tables[0].Rows[0]["DAPE1PAC"]).Trim().ToUpper();
                    }

                    if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["DAPE2PAC"]))
                    {
                        APE2 = Convert.ToString(RrSql.Tables[0].Rows[0]["DAPE2PAC"]).Trim().ToUpper();
                    }
                }

                RrSql.Close();

                ClasePedidos = new LlamarOperacionesFMS.clsOperFMS();				
				ClasePedidos.LlamarOperacionesFMS(Conexion_Base_datos.VstCodUsua, Conexion_Base_datos.RcAdmision, this, "CONTRATO", stTGidenpac, APE1, APE2);
                this.Show();
            }
            catch
            {
                RadMessageBox.Show("Error accediendo contrato", Application.ProductName);
            }
        }

        //oscar 15/03/04
        private void chbAutoDatos_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbAutoEstudio_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbPa�ales_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }


        public void proEstudioClinico(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            string SqlRespon = String.Empty;
            string DniRespon = String.Empty;
            PrintDialog impresora = (PrintDialog)Dialogo_impresora;


            try
            {

                cryInformes.Reset();
                this.Cursor = Cursors.WaitCursor;
                cryInformes.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi12Dr1_CR11.rpt";
                //*********cabeceras
                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }
                //*******************

                sql1 = "SELECT dnombpac,dape1pac,dape2pac,ndninifp FROM DPACIENT WHERE GIDENPAC='" + stTGidenpac + "'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql1, Conexion_Base_datos.RcAdmision);
                Rr1 = new DataSet();
                tempAdapter.Fill(Rr1);
                sql1 = "";

                if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DNOMBPAC"]))
                {
                    sql1 = Convert.ToString(Rr1.Tables[0].Rows[0]["DNOMBPAC"]).Trim().ToUpper();
                }

                if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DAPE1PAC"]))
                {
                    sql1 = sql1 + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE1PAC"]).Trim().ToUpper();
                }

                if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DAPE2PAC"]))
                {
                    sql1 = sql1 + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE2PAC"]).Trim().ToUpper();
                }

                sql2 = "";

                if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["NDNINIFP"]))
                {
                    sql2 = Convert.ToString(Rr1.Tables[0].Rows[0]["NDNINIFP"]);
                }

                Rr1.Close();

                SqlRespon = "SELECT dnombper,dape1per,dape2per,ndninifp FROM DPERSCON " + " WHERE GIDENPAC='" + stTGidenpac + "' AND IRESPONS='S'";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SqlRespon, Conexion_Base_datos.RcAdmision);
                Rr1 = new DataSet();
                tempAdapter_2.Fill(Rr1);
                SqlRespon = "";
                if (Rr1.Tables[0].Rows.Count != 0)
                {
                    if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DNOMBPER"]))
                    {
                        SqlRespon = Convert.ToString(Rr1.Tables[0].Rows[0]["DNOMBPER"]).Trim().ToUpper();
                    }

                    if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DAPE1PER"]))
                    {
                        SqlRespon = SqlRespon + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE1PER"]).Trim().ToUpper();
                    }

                    if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["DAPE2PER"]))
                    {
                        SqlRespon = SqlRespon + " " + Convert.ToString(Rr1.Tables[0].Rows[0]["DAPE2PER"]).Trim().ToUpper();
                    }

                    if (!Convert.IsDBNull(Rr1.Tables[0].Rows[0]["ndninifp"]))
                    {
                        DniRespon = Convert.ToString(Rr1.Tables[0].Rows[0]["ndninifp"]);
                    }
                }
                else
                {
                    SqlRespon = sql1;
                }

                Rr1.Close();


                cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                cryInformes.Formulas["{@PACIENTE}"] = "\"" + sql1 + "\"";

                if (DniRespon != "")
                {
                    cryInformes.Formulas["{@DNI}"] = "\"" + DniRespon + "\"";
                    cryInformes.Formulas["{@reponsable}"] = "\"" + SqlRespon + "\"";
                }
                else
                {
                    cryInformes.Formulas["{@DNI}"] = "\"" + sql2 + "\"";
                    cryInformes.Formulas["{@reponsable}"] = "\"" + sql1 + "\"";
                }

                if (sql1.Trim().ToUpper() == SqlRespon.Trim().ToUpper())
                {
                    cryInformes.Formulas["{@RESIDRESPON}"] = "\"" + Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision).ToUpper() + ":" + "\"";
                }
                else
                {
                    cryInformes.Formulas["{@RESIDRESPON"] = "'RESPONSABLE:'";
                }

                cryInformes.Formulas["{@DNICEDULA}"] = "\"" + Conexion_Base_datos.DNICEDULA + ":" + "\"";
                cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";
                cryInformes.Formulas["{@RESPONSABLEFIRMA"] = "";
                cryInformes.WindowTitle = "Autorizaci�n utilizaci�n Datos Cl�nicos";
                cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;
                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";
                cryInformes.SQLQuery = "select ganoadme, gnumadme, gidenpac,fllegada,gservici,gmotingr,gpersona,faltaadm,gserulti,gperulti,gsitalta,gmotalta,ghospita,gdiaging,odiaging,iconfide,iotrocen,irevisio,ireingre,ienfment,iambulan,gcauambu,iregaloj,itipingr,nanoadms,nnumadms,fingplan,faltplan,gdiagalt,odiagalt,gdiasec1,odiasec1,gdiasec2,odiasec2,finterve,gcumplim,gmotause,iacompan,ganrelur,gnurelur,gusuingr,gusualta,ghospori,dmediori ,dserdest,gmottrac,dnomcond,dplacamb,gempresa,gdiagini,odiagini,diasabse,horaabse,itraslad,iestudio,gcoeconc,iambproc,gtiplaza from aepisadm";// where 1=2";               
                cryInformes.SubreportToChange = "LogotipoIDC";
                cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + "" + "";
                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                cryInformes.Destination = destino;
                cryInformes.WindowShowPrintSetupBtn = true;                

                cryInformes.Action = 1;
                cryInformes.PrinterStopPage = cryInformes.PageCount;
                cryInformes.Reset();

            }
            catch (Exception ex)
            {

                if (Information.Err().Number != 20000)
                {
                    Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "documentos_ingreso:proEstudioClinico", ex);
                }

                cryInformes.PrinterStopPage = cryInformes.PageCount;
                cryInformes.Reset();
            }
        }


        private void proImprimirPa�ales(object Dialogo_impresora, Crystal.DestinationConstants destino)
        {
            try
            {
                string NombreDirector = String.Empty;
                string NombreRepresentante = String.Empty;

                DataSet RrConstantes = null;
                string sqlConstantes = String.Empty;
                PrintDialog impresora = (PrintDialog)Dialogo_impresora;

                if (chbContrato.CheckState != CheckState.Checked)
                {
                    sqlConstantes = "select valfanu1 from sconsglo where gconsglo='NDNIDIRE'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlConstantes, Conexion_Base_datos.RcAdmision);
                    RrConstantes = new DataSet();
                    tempAdapter.Fill(RrConstantes);
                    if (RrConstantes.Tables[0].Rows.Count == 1)
                    {
                        NombreDirector = Convert.ToString(RrConstantes.Tables[0].Rows[0]["valfanu1"]).Trim();
                    }

                    RrConstantes.Close();

                    //DATOS DEL RESPONSABLE
                    sqlConstantes = "select rtrim(dnombper)+' ' + rtrim(dape1per)+' '+ rtrim(isnull(dape2per,'')) as nombre" +
                                    " from dperscon" +
                                    " where gidenpac='" + stTGidenpac + "' and irespons='S'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlConstantes, Conexion_Base_datos.RcAdmision);
                    RrConstantes = new DataSet();
                    tempAdapter_2.Fill(RrConstantes);
                    if (RrConstantes.Tables[0].Rows.Count == 1)
                    { // SI TIENE DATOS DE LA PERSONA DE CONTACTO                      
                        NombreRepresentante = Convert.ToString(RrConstantes.Tables[0].Rows[0]["nombre"]).Trim();
                    }

                    RrConstantes.Close();
                }

                if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                {
                    Conexion_Base_datos.proCabCrystal();
                }

                cryInformes.ReportFileName = Conexion_Base_datos.PathString + "\\RPT\\agi131r1_CR11.rpt";

                cryInformes.Formulas["{@NOMBREDIRECTOR}"] = "\"" + NombreDirector + "\"";
                cryInformes.Formulas["{@NOMBREPACIENTE}"] = "\"" + lbPaciente.Text + "\"";
                cryInformes.Formulas["{@NOMBREREPRESENTANTE}"] = "\"" + NombreRepresentante + "\"";

                cryInformes.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                cryInformes.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                cryInformes.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";

                cryInformes.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(Conexion_Base_datos.RcAdmision) + "\"";
                cryInformes.Formulas["{@LITPERSONA}"] = "\"" + Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision) + "\"";

                cryInformes.WindowTitle = "Informaci�n de pa�ales";

                cryInformes.SQLQuery = "SELECT * FROM dmotingr where 1=2";
                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a;

                cryInformes.SubreportToChange = "LogotipoIDC";
                cryInformes.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                cryInformes.Connect = "" + Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";" + "";

                cryInformes.WindowState = Crystal.WindowStateConstants.crptMaximized;
                cryInformes.Destination = destino;
                cryInformes.WindowShowPrintSetupBtn = true;
                cryInformes.CopiesToPrinter = (short)impresora.PrinterSettings.Copies;

                cryInformes.Action = 1;
                cryInformes.PrinterStopPage = cryInformes.PageCount;
                cryInformes.Reset();

            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "documentos_ingreso:proImprimirPa�ales", ex);
            }

        }
        ////---------

        //oscar 16/03/2004
        private void ChbInterJuzga_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }

        private void chbDuplicidad_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            proSeleccion_Doc2();
        }
        //------
    }
}