using Microsoft.VisualBasic;
using System;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.VB;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace ADMISION
{
	public partial class Seleccion_Impresoras
        : RadForm
    {

		bool fCombImp = false;
		bool fCancel = false;
		public Seleccion_Impresoras()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}







		private void cbbImpresoras_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			fCombImp = true;
			if (cbbImpresoras.SelectedIndex != -1 && cbbImpresoras.Text != "" && tbCopias.Text != "")
			{
                //SDSALAZAR CORREGIR SPRINT 4
                //Conexion_Base_datos.VstImpresora2 = cbbImpresoras.GetListItem(cbbImpresoras.SelectedIndex);
                Conexion_Base_datos.VstImpresora2 = cbbImpresoras.SelectedText;
                foreach (PrinterHelper prImpre2 in PrinterHelper.Printers)
				{
					Conexion_Base_datos.prImpre = prImpre2;
					if (PrinterHelper.Printer.DriverName == Conexion_Base_datos.VstImpresora2)
					{
						Conexion_Base_datos.VstPortPrint2 = PrinterHelper.Printer.Port;
						Conexion_Base_datos.VstDriverPrint2 = PrinterHelper.Printer.DriverName;
						break;
					}
					Conexion_Base_datos.prImpre = null;
				}

				cbImprimir.Enabled = true;
			}
			else
			{
				cbImprimir.Enabled = false;
			}

		}


		private void cbbImpresoras_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			fCancel = false;
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{

			Conexion_Base_datos.ViNumCopias = Convert.ToInt32(Double.Parse(tbCopias.Text));
            fCancel = true; 
            this.Close();
		}

		private void Seleccion_Impresoras_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				Application.DoEvents();
				int tin = Application.OpenForms.Count;
			}
		}

		private void Seleccion_Impresoras_Load(Object eventSender, EventArgs eventArgs)
		{
			try
			{
                //cbbImpresoras.Items.Clear();
                //relleno el combo de impresoras y cojo el nombre de la predeterminada
               
                Conexion_Base_datos.VfCancelPrint = false;
				fCancel = false;
				//VstImpresora = Printer.DeviceName
				//VstPortPrint = Printer.Port
				//VstDriverPrint = Printer.DriverName
				//ViOrientacion = Printer.Orientation
				Conexion_Base_datos.VstImpresora2 = PrinterHelper.Printer.DeviceName;
				Conexion_Base_datos.VstPortPrint2 = PrinterHelper.Printer.Port;
				Conexion_Base_datos.VstDriverPrint2 = PrinterHelper.Printer.DeviceName;
				Conexion_Base_datos.ViOrientacion2 = PrinterHelper.Printer.Orientation;
				fCombImp = false;
				foreach (PrinterHelper prImpre2 in PrinterHelper.Printers)
				{
					Conexion_Base_datos.prImpre = prImpre2;
					//cbbImpresoras.Items.Add(PrinterHelper.Printer.DeviceName);
                    cbbImpresoras.Items.Add(prImpre2.DeviceName);
                    Conexion_Base_datos.prImpre = null;
				}

				tbCopias.Text = Convert.ToString(Conexion_Base_datos.fnNEtiqueta());
			}
			catch (System.Exception excep)
			{
				MessageBox.Show(excep.Message, Application.ProductName);
				fCancel = true;
				this.Close();
			}
		}

		private void Seleccion_Impresoras_Closed(Object eventSender, EventArgs eventArgs)
		{
			if (!fCancel)
			{ //ha cancelado
				Conexion_Base_datos.VfCancelPrint = true;
				Conexion_Base_datos.VstImpresora = PrinterHelper.Printer.DeviceName;
				Conexion_Base_datos.VstPortPrint = PrinterHelper.Printer.Port;
				Conexion_Base_datos.VstDriverPrint = PrinterHelper.Printer.DeviceName;
				Conexion_Base_datos.ViOrientacion = PrinterHelper.Printer.Orientation;
			}

            this.Dispose();
		}

		private void tbCopias_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;

			tbCopias.IsReadOnly = !((KeyCode >= 96 && KeyCode <= 105) || (KeyCode >= 48 && KeyCode <= 57) || (KeyCode == 12) || (KeyCode == 46) || (KeyCode == 8)); //no lo son

		}

		private void tbCopias_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (tbCopias.Text != "" && !fCombImp)
			{
				cbImprimir.Enabled = true;
			}
			else if (cbbImpresoras.SelectedIndex != -1 && cbbImpresoras.Text != "" && fCombImp && tbCopias.Text != "")
			{ 
				cbImprimir.Enabled = true;
			}
			else
			{
				cbImprimir.Enabled = false;
			}
		}
	}
}