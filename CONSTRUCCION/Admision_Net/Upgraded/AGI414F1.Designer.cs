using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class Anulacion_ingresos_alta
	{

		#region "Upgrade Support "
		private static Anulacion_ingresos_alta m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Anulacion_ingresos_alta DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Anulacion_ingresos_alta();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbPantalla", "CbCerrar", "CbImprimir", "SdcFechaFin", "SdcFechaIni", "Label2", "Label1", "Frame2", "RbIngreso", "RbAlta", "Frame3", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaFin;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadRadioButton RbIngreso;
		public Telerik.WinControls.UI.RadRadioButton RbAlta;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.RbIngreso = new Telerik.WinControls.UI.RadRadioButton();
            this.RbAlta = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.Location = new System.Drawing.Point(129, 176);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(81, 29);
            this.CbPantalla.TabIndex = 5;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(217, 176);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 6;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.Location = new System.Drawing.Point(41, 176);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 9;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(300, 170);
            this.Frame1.TabIndex = 0;
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.SdcFechaFin);
            this.Frame2.Controls.Add(this.SdcFechaIni);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.HeaderText = "Seleccione intervalo de fechas de anulación";
            this.Frame2.Location = new System.Drawing.Point(8, 16);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(281, 73);
            this.Frame2.TabIndex = 4;
            this.Frame2.Text = "Seleccione intervalo de fechas de anulación";
            // 
            // SdcFechaFin
            // 
            this.SdcFechaFin.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaFin.Location = new System.Drawing.Point(160, 40);
            this.SdcFechaFin.Name = "SdcFechaFin";
            this.SdcFechaFin.Size = new System.Drawing.Size(113, 20);
            this.SdcFechaFin.TabIndex = 10;
            this.SdcFechaFin.TabStop = false;           
            this.SdcFechaFin.Value = new System.DateTime(2016, 4, 27, 10, 7, 4, 213);
            // 
            // SdcFechaIni
            // 
            this.SdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaIni.Location = new System.Drawing.Point(16, 40);
            this.SdcFechaIni.Name = "SdcFechaIni";
            this.SdcFechaIni.Size = new System.Drawing.Size(113, 20);
            this.SdcFechaIni.TabIndex = 11;
            this.SdcFechaIni.TabStop = false;            
            this.SdcFechaIni.Value = new System.DateTime(2016, 4, 27, 10, 7, 4, 242);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(160, 16);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(53, 18);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Fecha fin:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(16, 16);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(67, 18);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "Fecha inicio:";
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.RbIngreso);
            this.Frame3.Controls.Add(this.RbAlta);
            this.Frame3.HeaderText = "Seleccione tipo de movimiento";
            this.Frame3.Location = new System.Drawing.Point(8, 96);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(281, 57);
            this.Frame3.TabIndex = 1;
            this.Frame3.Text = "Seleccione tipo de movimiento";
            // 
            // RbIngreso
            // 
            this.RbIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbIngreso.Location = new System.Drawing.Point(16, 24);
            this.RbIngreso.Name = "RbIngreso";
            this.RbIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbIngreso.Size = new System.Drawing.Size(58, 18);
            this.RbIngreso.TabIndex = 3;
            this.RbIngreso.Text = "Ingreso";
            this.RbIngreso.IsChecked = true;
            this.RbIngreso.IsChecked = false;
            this.RbIngreso.CheckStateChanged += new System.EventHandler(this.RbIngreso_CheckedChanged);
            // 
            // RbAlta
            // 
            this.RbAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbAlta.Location = new System.Drawing.Point(168, 24);
            this.RbAlta.Name = "RbAlta";
            this.RbAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbAlta.Size = new System.Drawing.Size(40, 18);
            this.RbAlta.TabIndex = 2;
            this.RbAlta.Text = "Alta";
            this.RbAlta.CheckStateChanged += new System.EventHandler(this.RbAlta_CheckedChanged);
            // 
            // Anulacion_ingresos_alta
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(301, 206);
            this.Controls.Add(this.CbPantalla);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Anulacion_ingresos_alta";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Anulación de ingresos/altas AGI414F1";
            this.Closed += new System.EventHandler(this.Anulacion_ingresos_alta_Closed);
            this.Load += new System.EventHandler(this.Anulacion_ingresos_alta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
        
        void ReLoadForm(bool addEvents)
		{
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
            //The MDI form in the VB6 project had its
            //AutoShowChildren property set to True
            //To simulate the VB6 behavior, we need to
            //automatically Show the form whenever it
            //is loaded.  If you do not want this behavior
            //then delete the following line of code
                      
		}
		#endregion
	}
}