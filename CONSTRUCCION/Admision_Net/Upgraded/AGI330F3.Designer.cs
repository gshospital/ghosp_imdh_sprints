using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class List_traslados
	{

		#region "Upgrade Support "
		private static List_traslados m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static List_traslados DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new List_traslados();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_CbInsTodo_1", "_CbInsUno_1", "_CbDelTodo_1", "_CbDelUno_1", "LsbLista4", "LsbLista3", "FrmDestino", "LsbLista2", "_CbDelUno_0", "_CbDelTodo_0", "_CbInsUno_0", "_CbInsTodo_0", "LsbLista1", "FrmOrigen", "Frame3", "sdcFechfin", "sdcFechini", "Label1", "Label2", "Frame2", "cbPantalla", "cbCerrar", "cbImprimir", "CbDelTodo", "CbDelUno", "CbInsTodo", "CbInsUno", "listBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadButton _CbInsTodo_1;
		private Telerik.WinControls.UI.RadButton _CbInsUno_1;
		private Telerik.WinControls.UI.RadButton _CbDelTodo_1;
		private Telerik.WinControls.UI.RadButton _CbDelUno_1;
		public Telerik.WinControls.UI.RadListControl LsbLista4;
		public Telerik.WinControls.UI.RadListControl LsbLista3;
		public Telerik.WinControls.UI.RadGroupBox FrmDestino;
		public Telerik.WinControls.UI.RadListControl LsbLista2;
		private Telerik.WinControls.UI.RadButton _CbDelUno_0;
		private Telerik.WinControls.UI.RadButton _CbDelTodo_0;
		private Telerik.WinControls.UI.RadButton _CbInsUno_0;
		private Telerik.WinControls.UI.RadButton _CbInsTodo_0;
		public Telerik.WinControls.UI.RadListControl LsbLista1;
		public Telerik.WinControls.UI.RadGroupBox FrmOrigen;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechfin;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechini;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadButton[] CbDelTodo = new Telerik.WinControls.UI.RadButton[2];
		public Telerik.WinControls.UI.RadButton[] CbDelUno = new Telerik.WinControls.UI.RadButton[2];
		public Telerik.WinControls.UI.RadButton[] CbInsTodo = new Telerik.WinControls.UI.RadButton[2];
		public Telerik.WinControls.UI.RadButton[] CbInsUno = new Telerik.WinControls.UI.RadButton[2];
		private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.FrmDestino = new Telerik.WinControls.UI.RadGroupBox();
            this._CbInsTodo_1 = new Telerik.WinControls.UI.RadButton();
            this._CbInsUno_1 = new Telerik.WinControls.UI.RadButton();
            this._CbDelTodo_1 = new Telerik.WinControls.UI.RadButton();
            this._CbDelUno_1 = new Telerik.WinControls.UI.RadButton();
            this.LsbLista4 = new Telerik.WinControls.UI.RadListControl();
            this.LsbLista3 = new Telerik.WinControls.UI.RadListControl();
            this.FrmOrigen = new Telerik.WinControls.UI.RadGroupBox();
            this.LsbLista2 = new Telerik.WinControls.UI.RadListControl();
            this._CbDelUno_0 = new Telerik.WinControls.UI.RadButton();
            this._CbDelTodo_0 = new Telerik.WinControls.UI.RadButton();
            this._CbInsUno_0 = new Telerik.WinControls.UI.RadButton();
            this._CbInsTodo_0 = new Telerik.WinControls.UI.RadButton();
            this.LsbLista1 = new Telerik.WinControls.UI.RadListControl();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFechfin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcFechini = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDestino)).BeginInit();
            this.FrmDestino.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsTodo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsUno_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelTodo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelUno_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmOrigen)).BeginInit();
            this.FrmOrigen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelUno_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelTodo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsUno_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsTodo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.FrmDestino);
            this.Frame3.Controls.Add(this.FrmOrigen);
            this.Frame3.HeaderText = "Unidades de Enfermerķa";
            this.Frame3.Location = new System.Drawing.Point(8, 31);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(593, 289);
            this.Frame3.TabIndex = 8;
            this.Frame3.Text = "Unidades de Enfermerķa";
            // 
            // FrmDestino
            // 
            this.FrmDestino.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmDestino.Controls.Add(this._CbInsTodo_1);
            this.FrmDestino.Controls.Add(this._CbInsUno_1);
            this.FrmDestino.Controls.Add(this._CbDelTodo_1);
            this.FrmDestino.Controls.Add(this._CbDelUno_1);
            this.FrmDestino.Controls.Add(this.LsbLista4);
            this.FrmDestino.Controls.Add(this.LsbLista3);
            this.FrmDestino.HeaderText = "Destino";
            this.FrmDestino.Location = new System.Drawing.Point(8, 152);
            this.FrmDestino.Name = "FrmDestino";
            this.FrmDestino.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmDestino.Size = new System.Drawing.Size(577, 130);
            this.FrmDestino.TabIndex = 15;
            this.FrmDestino.Text = "Destino";
            // 
            // _CbInsTodo_1
            // 
            this._CbInsTodo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbInsTodo_1.Location = new System.Drawing.Point(272, 13);
            this._CbInsTodo_1.Name = "_CbInsTodo_1";
            this._CbInsTodo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbInsTodo_1.Size = new System.Drawing.Size(33, 21);
            this._CbInsTodo_1.TabIndex = 21;
            this._CbInsTodo_1.Text = ">>";
            this._CbInsTodo_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbInsTodo_1.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // _CbInsUno_1
            // 
            this._CbInsUno_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbInsUno_1.Location = new System.Drawing.Point(272, 45);
            this._CbInsUno_1.Name = "_CbInsUno_1";
            this._CbInsUno_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbInsUno_1.Size = new System.Drawing.Size(33, 21);
            this._CbInsUno_1.TabIndex = 20;
            this._CbInsUno_1.Text = ">";
            this._CbInsUno_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbInsUno_1.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // _CbDelTodo_1
            // 
            this._CbDelTodo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbDelTodo_1.Location = new System.Drawing.Point(272, 77);
            this._CbDelTodo_1.Name = "_CbDelTodo_1";
            this._CbDelTodo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbDelTodo_1.Size = new System.Drawing.Size(33, 21);
            this._CbDelTodo_1.TabIndex = 19;
            this._CbDelTodo_1.Text = "<<";
            this._CbDelTodo_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbDelTodo_1.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // _CbDelUno_1
            // 
            this._CbDelUno_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbDelUno_1.Location = new System.Drawing.Point(272, 107);
            this._CbDelUno_1.Name = "_CbDelUno_1";
            this._CbDelUno_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbDelUno_1.Size = new System.Drawing.Size(33, 21);
            this._CbDelUno_1.TabIndex = 18;
            this._CbDelUno_1.Text = "<";
            this._CbDelUno_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbDelUno_1.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // LsbLista4
            // 
            this.LsbLista4.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.LsbLista4.Location = new System.Drawing.Point(328, 13);
            this.LsbLista4.Name = "LsbLista4";
            this.LsbLista4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista4.Size = new System.Drawing.Size(233, 112);
            this.LsbLista4.TabIndex = 17;
            this.LsbLista4.TabStop = false;
            this.LsbLista4.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.LsbLista4_SelectedIndexChanged);
            this.LsbLista4.DoubleClick += new System.EventHandler(this.LsbLista4_DoubleClick);
            // 
            // LsbLista3
            // 
            this.LsbLista3.BackColor = System.Drawing.SystemColors.Window;
            this.LsbLista3.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.LsbLista3.Location = new System.Drawing.Point(16, 13);
            this.LsbLista3.Name = "LsbLista3";
            this.LsbLista3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista3.Size = new System.Drawing.Size(233, 112);
            this.LsbLista3.TabIndex = 16;
            this.LsbLista3.TabStop = false;
            this.LsbLista3.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.LsbLista3_SelectedIndexChanged);
            this.LsbLista3.DoubleClick += new System.EventHandler(this.LsbLista3_DoubleClick);
            // 
            // FrmOrigen
            // 
            this.FrmOrigen.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmOrigen.Controls.Add(this.LsbLista2);
            this.FrmOrigen.Controls.Add(this._CbDelUno_0);
            this.FrmOrigen.Controls.Add(this._CbDelTodo_0);
            this.FrmOrigen.Controls.Add(this._CbInsUno_0);
            this.FrmOrigen.Controls.Add(this._CbInsTodo_0);
            this.FrmOrigen.Controls.Add(this.LsbLista1);
            this.FrmOrigen.HeaderText = "Origen";
            this.FrmOrigen.Location = new System.Drawing.Point(8, 16);
            this.FrmOrigen.Name = "FrmOrigen";
            this.FrmOrigen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmOrigen.Size = new System.Drawing.Size(577, 137);
            this.FrmOrigen.TabIndex = 9;
            this.FrmOrigen.Text = "Origen";
            // 
            // LsbLista2
            // 
            this.LsbLista2.BackColor = System.Drawing.SystemColors.Window;
            this.LsbLista2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.LsbLista2.Location = new System.Drawing.Point(328, 13);
            this.LsbLista2.Name = "LsbLista2";
            this.LsbLista2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista2.Size = new System.Drawing.Size(233, 117);
            this.LsbLista2.TabIndex = 22;
            this.LsbLista2.TabStop = false;
            this.LsbLista2.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.LsbLista2_SelectedIndexChanged);
            this.LsbLista2.DoubleClick += new System.EventHandler(this.LsbLista2_DoubleClick);
            // 
            // _CbDelUno_0
            // 
            this._CbDelUno_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbDelUno_0.Location = new System.Drawing.Point(272, 109);
            this._CbDelUno_0.Name = "_CbDelUno_0";
            this._CbDelUno_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbDelUno_0.Size = new System.Drawing.Size(33, 21);
            this._CbDelUno_0.TabIndex = 14;
            this._CbDelUno_0.Text = "<";
            this._CbDelUno_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbDelUno_0.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // _CbDelTodo_0
            // 
            this._CbDelTodo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbDelTodo_0.Location = new System.Drawing.Point(272, 77);
            this._CbDelTodo_0.Name = "_CbDelTodo_0";
            this._CbDelTodo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbDelTodo_0.Size = new System.Drawing.Size(33, 21);
            this._CbDelTodo_0.TabIndex = 13;
            this._CbDelTodo_0.Text = "<<";
            this._CbDelTodo_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbDelTodo_0.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // _CbInsUno_0
            // 
            this._CbInsUno_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbInsUno_0.Location = new System.Drawing.Point(272, 45);
            this._CbInsUno_0.Name = "_CbInsUno_0";
            this._CbInsUno_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbInsUno_0.Size = new System.Drawing.Size(33, 21);
            this._CbInsUno_0.TabIndex = 12;
            this._CbInsUno_0.Text = ">";
            this._CbInsUno_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbInsUno_0.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // _CbInsTodo_0
            // 
            this._CbInsTodo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._CbInsTodo_0.Location = new System.Drawing.Point(272, 13);
            this._CbInsTodo_0.Name = "_CbInsTodo_0";
            this._CbInsTodo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CbInsTodo_0.Size = new System.Drawing.Size(33, 21);
            this._CbInsTodo_0.TabIndex = 11;
            this._CbInsTodo_0.Text = ">>";
            this._CbInsTodo_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._CbInsTodo_0.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // LsbLista1
            // 
            this.LsbLista1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.LsbLista1.Location = new System.Drawing.Point(16, 13);
            this.LsbLista1.Name = "LsbLista1";
            this.LsbLista1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista1.Size = new System.Drawing.Size(233, 117);
            this.LsbLista1.TabIndex = 10;
            this.LsbLista1.TabStop = false;
            this.LsbLista1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.LsbLista1_SelectedIndexChanged);
            this.LsbLista1.DoubleClick += new System.EventHandler(this.LsbLista1_DoubleClick);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.sdcFechfin);
            this.Frame2.Controls.Add(this.sdcFechini);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.HeaderText = "Fechas";
            this.Frame2.Location = new System.Drawing.Point(8, -2);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(593, 33);
            this.Frame2.TabIndex = 3;
            this.Frame2.Text = "Fechas";
            // 
            // sdcFechfin
            // 
            this.sdcFechfin.CustomFormat = "dd/MM/yyyy";
            this.sdcFechfin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechfin.Location = new System.Drawing.Point(400, 10);
            this.sdcFechfin.Name = "sdcFechfin";
            this.sdcFechfin.Size = new System.Drawing.Size(105, 20);
            this.sdcFechfin.TabIndex = 4;
            this.sdcFechfin.TabStop = false;
            this.sdcFechfin.Text = "25/04/2016";
            this.sdcFechfin.Value = new System.DateTime(2016, 4, 25, 11, 14, 5, 391);
            // 
            // sdcFechini
            // 
            this.sdcFechini.CustomFormat = "dd/MM/yyyy";
            this.sdcFechini.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechini.Location = new System.Drawing.Point(112, 10);
            this.sdcFechini.Name = "sdcFechini";
            this.sdcFechini.Size = new System.Drawing.Size(105, 20);
            this.sdcFechini.TabIndex = 5;
            this.sdcFechini.TabStop = false;
            this.sdcFechini.Text = "25/04/2016";
            this.sdcFechini.Value = new System.DateTime(2016, 4, 25, 11, 14, 5, 410);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(72, 11);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(35, 18);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "Inicio:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(368, 11);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(23, 18);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "Fin:";
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbPantalla.Location = new System.Drawing.Point(435, 322);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(81, 29);
            this.cbPantalla.TabIndex = 2;
            this.cbPantalla.Text = "&Pantalla";
            this.cbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbCerrar.Location = new System.Drawing.Point(520, 322);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 29);
            this.cbCerrar.TabIndex = 1;
            this.cbCerrar.Text = "&Cerrar";
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbImprimir.Location = new System.Drawing.Point(348, 322);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(81, 29);
            this.cbImprimir.TabIndex = 0;
            this.cbImprimir.Text = "&Impresora";
            this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // List_traslados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(605, 353);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.cbImprimir);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 30);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "List_traslados";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Listado de los traslados realizados - AGI330F3";
            this.Closed += new System.EventHandler(this.List_traslados_Closed);
            this.Load += new System.EventHandler(this.List_traslados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrmDestino)).EndInit();
            this.FrmDestino.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._CbInsTodo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsUno_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelTodo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelUno_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmOrigen)).EndInit();
            this.FrmOrigen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelUno_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbDelTodo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsUno_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CbInsTodo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeCbInsUno();
			InitializeCbInsTodo();
			InitializeCbDelUno();
			InitializeCbDelTodo();
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		void InitializeCbInsUno()
		{
			this.CbInsUno = new Telerik.WinControls.UI.RadButton[2];
			this.CbInsUno[1] = _CbInsUno_1;
			this.CbInsUno[0] = _CbInsUno_0;
		}
		void InitializeCbInsTodo()
		{
			this.CbInsTodo = new Telerik.WinControls.UI.RadButton[2];
			this.CbInsTodo[1] = _CbInsTodo_1;
			this.CbInsTodo[0] = _CbInsTodo_0;
		}
		void InitializeCbDelUno()
		{
			this.CbDelUno = new Telerik.WinControls.UI.RadButton[2];
			this.CbDelUno[1] = _CbDelUno_1;
			this.CbDelUno[0] = _CbDelUno_0;
		}
		void InitializeCbDelTodo()
		{
			this.CbDelTodo = new Telerik.WinControls.UI.RadButton[2];
			this.CbDelTodo[1] = _CbDelTodo_1;
			this.CbDelTodo[0] = _CbDelTodo_0;
		}
		#endregion
	}
}