using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ADMISION
{
	partial class prevision_alta
	{

		#region "Upgrade Support "
		private static prevision_alta m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static prevision_alta DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new prevision_alta();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbImprimir", "CbCerrar", "CbPantalla", "_optCriterioOrdenacion_1", "_optCriterioOrdenacion_0", "frameCriterioOrdenacion", "ChBHosGeneral", "ChBHosDia", "FrmHospital", "SdcFechaIni", "SdcFechaFin", "Label2", "Label1", "Frame2", "CbInsTodo", "CbInsUno", "CbDelTodo", "CbDelUno", "LsbLista1", "LsbLista2", "Frame3", "Frame1", "optCriterioOrdenacion", "listBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton CbPantalla;
		private Telerik.WinControls.UI.RadRadioButton _optCriterioOrdenacion_1;
		private Telerik.WinControls.UI.RadRadioButton _optCriterioOrdenacion_0;
		public Telerik.WinControls.UI.RadGroupBox frameCriterioOrdenacion;
		public Telerik.WinControls.UI.RadCheckBox ChBHosGeneral;
		public Telerik.WinControls.UI.RadCheckBox ChBHosDia;
		public Telerik.WinControls.UI.RadGroupBox FrmHospital;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaIni;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaFin;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton CbInsTodo;
		public Telerik.WinControls.UI.RadButton CbInsUno;
		public Telerik.WinControls.UI.RadButton CbDelTodo;
		public Telerik.WinControls.UI.RadButton CbDelUno;
		public Telerik.WinControls.UI.RadListControl LsbLista1;
		public Telerik.WinControls.UI.RadListControl LsbLista2;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadRadioButton[] optCriterioOrdenacion = new Telerik.WinControls.UI.RadRadioButton[2];
		private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.CbImprimir = new Telerik.WinControls.UI.RadButton();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbPantalla = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.frameCriterioOrdenacion = new Telerik.WinControls.UI.RadGroupBox();
            this._optCriterioOrdenacion_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._optCriterioOrdenacion_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.FrmHospital = new Telerik.WinControls.UI.RadGroupBox();
            this.ChBHosGeneral = new Telerik.WinControls.UI.RadCheckBox();
            this.ChBHosDia = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcFechaIni = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbInsTodo = new Telerik.WinControls.UI.RadButton();
            this.CbInsUno = new Telerik.WinControls.UI.RadButton();
            this.CbDelTodo = new Telerik.WinControls.UI.RadButton();
            this.CbDelUno = new Telerik.WinControls.UI.RadButton();
            this.LsbLista1 = new Telerik.WinControls.UI.RadListControl();
            this.LsbLista2 = new Telerik.WinControls.UI.RadListControl();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frameCriterioOrdenacion)).BeginInit();
            this.frameCriterioOrdenacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._optCriterioOrdenacion_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._optCriterioOrdenacion_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).BeginInit();
            this.FrmHospital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CbImprimir
            // 
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbImprimir.Location = new System.Drawing.Point(275, 368);
            this.CbImprimir.Name = "CbImprimir";
            this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbImprimir.Size = new System.Drawing.Size(81, 29);
            this.CbImprimir.TabIndex = 6;
            this.CbImprimir.Text = "&Impresora";
            this.CbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbCerrar.Location = new System.Drawing.Point(449, 368);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 5;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbPantalla
            // 
            this.CbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbPantalla.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.CbPantalla.Location = new System.Drawing.Point(362, 368);
            this.CbPantalla.Name = "CbPantalla";
            this.CbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbPantalla.Size = new System.Drawing.Size(81, 29);
            this.CbPantalla.TabIndex = 4;
            this.CbPantalla.Text = "&Pantalla";
            this.CbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbPantalla.Click += new System.EventHandler(this.CbPantalla_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.frameCriterioOrdenacion);
            this.Frame1.Controls.Add(this.FrmHospital);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(529, 361);
            this.Frame1.TabIndex = 0;
            // 
            // frameCriterioOrdenacion
            // 
            this.frameCriterioOrdenacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frameCriterioOrdenacion.Controls.Add(this._optCriterioOrdenacion_1);
            this.frameCriterioOrdenacion.Controls.Add(this._optCriterioOrdenacion_0);
            this.frameCriterioOrdenacion.HeaderText = "Criterio de ordenaci�n";
            this.frameCriterioOrdenacion.Location = new System.Drawing.Point(8, 296);
            this.frameCriterioOrdenacion.Name = "frameCriterioOrdenacion";
            this.frameCriterioOrdenacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frameCriterioOrdenacion.Size = new System.Drawing.Size(511, 49);
            this.frameCriterioOrdenacion.TabIndex = 19;
            this.frameCriterioOrdenacion.Text = "Criterio de ordenaci�n";
            // 
            // _optCriterioOrdenacion_1
            // 
            this._optCriterioOrdenacion_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._optCriterioOrdenacion_1.Location = new System.Drawing.Point(296, 20);
            this._optCriterioOrdenacion_1.Name = "_optCriterioOrdenacion_1";
            this._optCriterioOrdenacion_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optCriterioOrdenacion_1.Size = new System.Drawing.Size(166, 18);
            this._optCriterioOrdenacion_1.TabIndex = 21;
            this._optCriterioOrdenacion_1.Text = "Unidad de enfermer�a / cama";
            // 
            // _optCriterioOrdenacion_0
            // 
            this._optCriterioOrdenacion_0.CheckState = System.Windows.Forms.CheckState.Checked;
            this._optCriterioOrdenacion_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._optCriterioOrdenacion_0.Location = new System.Drawing.Point(32, 16);
            this._optCriterioOrdenacion_0.Name = "_optCriterioOrdenacion_0";
            this._optCriterioOrdenacion_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optCriterioOrdenacion_0.Size = new System.Drawing.Size(175, 18);
            this._optCriterioOrdenacion_0.TabIndex = 20;
            this._optCriterioOrdenacion_0.Text = "Servicio / fecha prevista de alta";
            this._optCriterioOrdenacion_0.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // FrmHospital
            // 
            this.FrmHospital.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmHospital.Controls.Add(this.ChBHosGeneral);
            this.FrmHospital.Controls.Add(this.ChBHosDia);
            this.FrmHospital.HeaderText = "Tipo de Ingreso";
            this.FrmHospital.Location = new System.Drawing.Point(8, 248);
            this.FrmHospital.Name = "FrmHospital";
            this.FrmHospital.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmHospital.Size = new System.Drawing.Size(511, 41);
            this.FrmHospital.TabIndex = 16;
            this.FrmHospital.Text = "Tipo de Ingreso";
            // 
            // ChBHosGeneral
            // 
            //this.ChBHosGeneral.BackColor = System.Drawing.SystemColors.Control;
            this.ChBHosGeneral.Cursor = System.Windows.Forms.Cursors.Default;
            //this.ChBHosGeneral.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChBHosGeneral.Location = new System.Drawing.Point(48, 16);
            this.ChBHosGeneral.Name = "ChBHosGeneral";
            this.ChBHosGeneral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosGeneral.Size = new System.Drawing.Size(145, 17);
            this.ChBHosGeneral.TabIndex = 18;
            this.ChBHosGeneral.Text = "Hospitalizaci�n General";
            //this.ChBHosGeneral.UseVisualStyleBackColor = false;
            this.ChBHosGeneral.CheckStateChanged += new System.EventHandler(this.ChBHosGeneral_CheckStateChanged);
            // 
            // ChBHosDia
            // 
            //this.ChBHosDia.BackColor = System.Drawing.SystemColors.Control;
            this.ChBHosDia.Cursor = System.Windows.Forms.Cursors.Default;
            //this.ChBHosDia.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChBHosDia.Location = new System.Drawing.Point(312, 16);
            this.ChBHosDia.Name = "ChBHosDia";
            this.ChBHosDia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChBHosDia.Size = new System.Drawing.Size(105, 17);
            this.ChBHosDia.TabIndex = 17;
            this.ChBHosDia.Text = "Hospital de D�a";
            //this.ChBHosDia.UseVisualStyleBackColor = false;
            this.ChBHosDia.CheckStateChanged += new System.EventHandler(this.ChBHosDia_CheckStateChanged);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.SdcFechaIni);
            this.Frame2.Controls.Add(this.SdcFechaFin);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.HeaderText = "Seleccione fecha prevista de alta";
            this.Frame2.Location = new System.Drawing.Point(8, 16);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(513, 65);
            this.Frame2.TabIndex = 2;
            this.Frame2.Text = "Seleccione fecha prevista de alta";
            // 
            // SdcFechaIni
            // 
            this.SdcFechaIni.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaIni.Location = new System.Drawing.Point(48, 32);
            this.SdcFechaIni.Name = "SdcFechaIni";
            this.SdcFechaIni.Size = new System.Drawing.Size(105, 20);
            this.SdcFechaIni.TabIndex = 3;
            this.SdcFechaIni.TabStop = false;                       
            // 
            // SdcFechaFin
            // 
            this.SdcFechaFin.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaFin.Location = new System.Drawing.Point(312, 32);
            this.SdcFechaFin.Name = "SdcFechaFin";
            this.SdcFechaFin.Size = new System.Drawing.Size(105, 20);
            this.SdcFechaFin.TabIndex = 7;
            this.SdcFechaFin.TabStop = false;            
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(48, 16);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(67, 18);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "Fecha inicio:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(312, 16);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(53, 18);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "Fecha fin:";
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.CbInsTodo);
            this.Frame3.Controls.Add(this.CbInsUno);
            this.Frame3.Controls.Add(this.CbDelTodo);
            this.Frame3.Controls.Add(this.CbDelUno);
            this.Frame3.Controls.Add(this.LsbLista1);
            this.Frame3.Controls.Add(this.LsbLista2);
            this.Frame3.HeaderText = "Seleccione servicio";
            this.Frame3.Location = new System.Drawing.Point(8, 88);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(513, 153);
            this.Frame3.TabIndex = 1;
            this.Frame3.Text = "Seleccione servicio";
            // 
            // CbInsTodo
            // 
            //this.CbInsTodo.BackColor = System.Drawing.SystemColors.Control;
            this.CbInsTodo.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbInsTodo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CbInsTodo.Location = new System.Drawing.Point(240, 24);
            this.CbInsTodo.Name = "CbInsTodo";
            this.CbInsTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsTodo.Size = new System.Drawing.Size(33, 17);
            this.CbInsTodo.TabIndex = 15;
            this.CbInsTodo.Text = ">>";
            this.CbInsTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.CbInsTodo.UseVisualStyleBackColor = false;
            this.CbInsTodo.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // CbInsUno
            // 
            //this.CbInsUno.BackColor = System.Drawing.SystemColors.Control;
            this.CbInsUno.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbInsUno.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CbInsUno.Location = new System.Drawing.Point(240, 56);
            this.CbInsUno.Name = "CbInsUno";
            this.CbInsUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsUno.Size = new System.Drawing.Size(33, 17);
            this.CbInsUno.TabIndex = 14;
            this.CbInsUno.Text = ">";
            this.CbInsUno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.CbInsUno.UseVisualStyleBackColor = false;
            this.CbInsUno.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // CbDelTodo
            // 
            //this.CbDelTodo.BackColor = System.Drawing.SystemColors.Control;
            this.CbDelTodo.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbDelTodo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CbDelTodo.Location = new System.Drawing.Point(240, 88);
            this.CbDelTodo.Name = "CbDelTodo";
            this.CbDelTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelTodo.Size = new System.Drawing.Size(33, 17);
            this.CbDelTodo.TabIndex = 13;
            this.CbDelTodo.Text = "<<";
            this.CbDelTodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.CbDelTodo.UseVisualStyleBackColor = false;
            this.CbDelTodo.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // CbDelUno
            // 
            //this.CbDelUno.BackColor = System.Drawing.SystemColors.Control;
            this.CbDelUno.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbDelUno.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CbDelUno.Location = new System.Drawing.Point(240, 120);
            this.CbDelUno.Name = "CbDelUno";
            this.CbDelUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelUno.Size = new System.Drawing.Size(33, 17);
            this.CbDelUno.TabIndex = 12;
            this.CbDelUno.Text = "<";
            this.CbDelUno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.CbDelUno.UseVisualStyleBackColor = false;
            this.CbDelUno.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // LsbLista1
            // 
            this.LsbLista1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista1.Location = new System.Drawing.Point(8, 24);
            this.LsbLista1.Name = "LsbLista1";
            this.LsbLista1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista1.Size = new System.Drawing.Size(209, 124);
            this.LsbLista1.TabIndex = 11;
            this.LsbLista1.DoubleClick += new System.EventHandler(this.LsbLista1_DoubleClick);
            // 
            // LsbLista2
            // 
            this.LsbLista2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista2.Location = new System.Drawing.Point(296, 24);
            this.LsbLista2.Name = "LsbLista2";
            this.LsbLista2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista2.Size = new System.Drawing.Size(209, 124);
            this.LsbLista2.TabIndex = 10;
            this.LsbLista2.DoubleClick += new System.EventHandler(this.LsbLista2_DoubleClick);
            // 
            // prevision_alta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(532, 403);
            this.Controls.Add(this.CbImprimir);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbPantalla);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.Name = "prevision_alta";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Previsi�n del alta - AGI413F1";
            this.Closed += new System.EventHandler(this.prevision_alta_Closed);
            this.Load += new System.EventHandler(this.prevision_alta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frameCriterioOrdenacion)).EndInit();
            this.frameCriterioOrdenacion.ResumeLayout(false);
            this.frameCriterioOrdenacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._optCriterioOrdenacion_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._optCriterioOrdenacion_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmHospital)).EndInit();
            this.FrmHospital.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaIni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeoptCriterioOrdenacion();
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = ADMISION.menu_admision.DefInstance;
			ADMISION.menu_admision.DefInstance.Show();
			//The MDI form in the VB6 project had its
			//AutoShowChildren property set to True
			//To simulate the VB6 behavior, we need to
			//automatically Show the form whenever it
			//is loaded.  If you do not want this behavior
			//then delete the following line of code
			//UPGRADE_TODO: (2018) Remove the next line of code to stop form from automatically showing. More Information: http://www.vbtonet.com/ewis/ewi2018.aspx
			this.Show();
		}
		void InitializeoptCriterioOrdenacion()
		{
			this.optCriterioOrdenacion = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optCriterioOrdenacion[1] = _optCriterioOrdenacion_1;
			this.optCriterioOrdenacion[0] = _optCriterioOrdenacion_0;
		}
		#endregion
	}
}