using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ADMISION
{
	public partial class frmListInformes
		: Telerik.WinControls.UI.RadForm
	{

		int intAnoRegi = 0;
		int lngNumRegi = 0;
		public frmListInformes()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public void proRecibeDatos(string datosPaciente, int anoEpisodio, int numeroEpisodio)
		{
			intAnoRegi = anoEpisodio;
			lngNumRegi = numeroEpisodio;
			lblDatosEpisodio.Text = Conversion.Str(anoEpisodio).Trim() + "/" + Conversion.Str(numeroEpisodio).Trim();
			lblDatosPaciente.Text = datosPaciente;
			//    Form_Activate
		}

		private void cmbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string strCadena = String.Empty;

			try
			{


				SqlCommand tempCommand = new SqlCommand("DELETE FROM ADOCUMHC WHERE ganoregi = " + Conversion.Str(intAnoRegi).Trim() + " AND gnumregi = " + Conversion.Str(lngNumRegi).Trim(), Conexion_Base_datos.RcAdmision);
				tempCommand.ExecuteNonQuery();

				int tempForVar = sprDocumentos.MaxRows;
				for (int contador = 1; contador <= tempForVar; contador++)
				{
					sprDocumentos.Row = contador;
					sprDocumentos.Col = 2;
					if (Convert.ToBoolean(sprDocumentos.Rows[sprDocumentos.Row - 1].Cells[sprDocumentos.Col - 1].Value))
					{
						sprDocumentos.Col = 3;
						SqlCommand tempCommand_2 = new SqlCommand("INSERT INTO ADOCUMHC ( ganoregi , gnumregi, gtipdocu, gusuario ) VALUES( " + Conversion.Str(intAnoRegi).Trim() + ", " + Conversion.Str(lngNumRegi).Trim() + ", " + sprDocumentos.Text + ", '" + Conexion_Base_datos.VstCodUsua + "')", Conexion_Base_datos.RcAdmision);
						tempCommand_2.ExecuteNonQuery();
					}

				}
				this.Close();
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show(excep.Message, "Error: Actualizar registros.", MessageBoxButtons.OK, RadMessageIcon.Error);
			}
		}

		private void cmbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void frmListInformes_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				proRellenaGrid();
			}
		}

		private void proRellenaGrid()
		{
			string sqltemp = String.Empty;
			DataSet rdoTemp = null;
			int iColumna = 0;
			int iFila = 0;
			try
			{
				sqltemp = "SELECT ADOCUMHC.gtipdocu, DTIPDOCU.dtipdocu, 'S' as agregado " + 
				          "FROM ADOCUMHC " + 
				          "INNER JOIN DTIPDOCU ON ADOCUMHC.gtipdocu = DTIPDOCU.gtipdocu " + 
				          "WHERE ganoregi = " + Conversion.Str(intAnoRegi).Trim() + " AND gnumregi = " + Conversion.Str(lngNumRegi).Trim() + " " + 
				          "UNION ALL " + 
				          "SELECT DTIPDOCU.gtipdocu, DTIPDOCU.dtipdocu, 'N' " + 
				          "FROM DTIPDOCU WHERE DTIPDOCU.gtipdocu NOT IN ( SELECT ADOCUMHC.gtipdocu FROM ADOCUMHC WHERE ganoregi = " + Conversion.Str(intAnoRegi).Trim() + " AND gnumregi = " + Conversion.Str(lngNumRegi).Trim() + " ) AND DTIPDOCU.fborrado is null order by DTIPDOCU.dtipdocu";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, Conexion_Base_datos.RcAdmision);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count > 0)
				{
					sprDocumentos.MaxRows = rdoTemp.Tables[0].Rows.Count;
					iFila = 0;
					foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
					{
						iFila++;
						iColumna = 0;
						sprDocumentos.Row = iFila;
						iColumna++;
						sprDocumentos.Col = iColumna;
						sprDocumentos.Text = Convert.ToString(iteration_row["dtipdocu"]);
						sprDocumentos.Lock = true;
						iColumna++;
						sprDocumentos.Col = iColumna;

                        //'sprDocumentos.CellType = SS_CELL_TYPE_CHECKBOX
                        sprDocumentos.Rows[iFila - 1].Cells[iColumna - 1].Value = ((Convert.ToString(iteration_row["agregado"]) == "S")) ? true : false;
                        sprDocumentos.Lock = false;

                        iColumna++;
						sprDocumentos.Col = iColumna;
						sprDocumentos.Text = Convert.ToString(iteration_row["gtipdocu"]);
						sprDocumentos.Lock = true;
						sprDocumentos.SetColHidden(sprDocumentos.Col, true);

					}
				}
				else
				{
					sprDocumentos.MaxRows = 0;
				}
				rdoTemp.Close();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, this.Text + ":proRellenaGrid", ex);
			}
		}
		private void frmListInformes_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}