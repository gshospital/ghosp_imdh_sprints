using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Transactions;

namespace ADMISION
{
	public partial class Cambio_R�gimen
        : Telerik.WinControls.UI.RadForm
	{

        string stTipoRegimen = String.Empty; //RM (Regimen del Medico) / RA (regimen de Alojamiento)

		//Const REGIMEN_CERRADO = "Cerrado"
		//Const REGIMEN_ABIERTO = "Abierto"

		string REGIMEN_CERRADO = String.Empty;
		string REGIMEN_ABIERTO = String.Empty;

		string REGIMEN_INDIVIDUAL = String.Empty;
		string REGIMEN_COMPARTIDO = String.Empty;

		string IRegGrabar = String.Empty;

		string stFechaMaxMov = String.Empty;

		string vstfecha_hora_sys = String.Empty;
		string vstfecha = String.Empty;
		string vsthora = String.Empty;

		string vstSegundos = String.Empty; // segundos del sistema
		int sthorasystem = 0; // horas del sistema
		int stminutossystem = 0; //minutos del sistema
		string stSeleccion = String.Empty; //se utiliza para el report

		//Dim CrystalReport1 As Crystal.CrystalReport

		string vstAccion = String.Empty; //se guarda la accion que se ha seleccionado
		string stFechaIngreso = String.Empty; //lleva la fecha de llegada de AEPISADM

		int lRegistroSeleccionado = 0; //registro marcado en el spread
		bool fFilaSeleccionada = false; //guarda si hay una fila marcada o no

		string stCodpac = String.Empty; //Gidenpac del paciente
		int iA�oRegistro = 0; //ganoregi del paciente
		int iNumRegistro = 0; //gnumregi del paciente




		//datos destino (parece que es algo que sobra debido a una horrible copi-paste)
		//Dim stFfinmovi     As String 'ffinmovi de la fila borrada
		//Dim stUnidadDes    As String
		//Dim iPlantaDes     As Integer
		//Dim iHabitaDes     As Integer
		//Dim stCamaDes      As String
		//Dim viCodMedicoDes  As Long 'c�digo m�dico de
		//Dim viServicioDeS   As Integer 'c�digo servicio de
		//Dim stTipotra      As String
		//Dim iMotitras      As Integer
		public Cambio_R�gimen()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            ReLoadForm(false);
		}


		//Dim stIrotaci      As String

		public void proRecPaciente(string stCodPaciente, string stNombPaciente, string pTiporegimen)
		{
			//por donde entra desde el menu de ingresos
			//antes de salir de la pantalla del menu entra en este procedimiento
			string stSqCambio = String.Empty;
			DataSet RrMaximo = null;

			try
			{

				LbPaciente.Text = stNombPaciente;
				stCodpac = stCodPaciente;

				//RM (regimen del medico) / RA (regimen de alojamiento)
				stTipoRegimen = pTiporegimen;
				//Jes�s 07/04/2006 Para diferenciar la ayuda si viene por cambio de regimen m�dico o de alojamiento
				if (stTipoRegimen == "RA")
				{
					//UPGRADE_ISSUE: (2064) Form property Cambio_R�gimen.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//this.setHelpContextID(1251);
				}
				else
				{
					//UPGRADE_ISSUE: (2064) Form property Cambio_R�gimen.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//this.setHelpContextID(125);
				}

				switch(stTipoRegimen)
				{
					case "RM" :  //Regimen de medico (abierto / asociado) 
						stSqCambio = "SELECT MAX(AREGIMEN.fmovimie) FECHAMAXIMAMVTO,AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada " + " FROM AEPISADM, AREGIMEN " + " WHERE " + " AEPISADM.GNUMADME = AREGIMEN.GNUMADME AND " + " AEPISADM.GANOADME = AREGIMEN.GANOADME AND " + " AEPISADM.GIDENPAC = '" + stCodpac + "' and faltplan is null " + " GROUP BY AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada ORDER BY AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada  "; 
						break;
					case "RA" : 
						stSqCambio = "SELECT MAX(AMOVREGA.fmovimie) FECHAMAXIMAMVTO,AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada " + " FROM AEPISADM, AMOVREGA " + " WHERE " + " AEPISADM.GNUMADME = AMOVREGA.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVREGA.GANOREGI AND " + " AMOVREGA.itiposer='H' AND " + " AEPISADM.GIDENPAC = '" + stCodpac + "' and faltplan is null " + " GROUP BY AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada ORDER BY AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada  "; 
						break;
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, Conexion_Base_datos.RcAdmision);
				RrMaximo = new DataSet();
				tempAdapter.Fill(RrMaximo);
				if (RrMaximo.Tables[0].Rows.Count != 0)
				{
                    stFechaMaxMov = Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["FECHAMAXIMAMVTO"]).ToString("dd/MM/yyyy HH:mm:ss");
					stFechaIngreso = Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm:ss");
					//en el mindate ponemos la fecha de llegada del paciente
					SdcFecha.MinDate = DateTime.Parse(Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy"));
					iA�oRegistro = Convert.ToInt32(RrMaximo.Tables[0].Rows[0]["ganoadme"]);
					iNumRegistro = Convert.ToInt32(RrMaximo.Tables[0].Rows[0]["gnumadme"]);
				}
				else
				{
					RadMessageBox.Show("no hay movimientos del paciente ", Application.ProductName);
				}
				RrMaximo.Close();

				//limpiamos los campos
			}
			catch (Exception ex)
			{
				//**********
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_R�gimen:proRecPaciente", ex);
			}
		}

		public bool MasPacientesenHabitacion()
		{
			//Jes�s 10/04/2006 Para comprobar si hay otros pacientes en la habitaci�n y se va a cambiar el r�gimen a Individual
			bool result = false;
			int iPlanta = 0;
			int iHabitac = 0;
			string stCama = String.Empty;

			string StSqHabitac = "SELECT GPLANTAS,GHABITAC,GCAMASBO FROM DCAMASBOVA WHERE IESTCAMA = 'O' and GIDENPAC = '" + stCodpac + "'";
            SqlCommand command = new SqlCommand(StSqHabitac, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.RcAdmision.getCurrentTransaction());
            SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
			DataSet rrHabitac = new DataSet();
			tempAdapter.Fill(rrHabitac);
			if (rrHabitac.Tables[0].Rows.Count != 0)
			{
				iPlanta = Convert.ToInt32(rrHabitac.Tables[0].Rows[0]["gplantas"]);
				iHabitac = Convert.ToInt32(rrHabitac.Tables[0].Rows[0]["ghabitac"]);
				stCama = Convert.ToString(rrHabitac.Tables[0].Rows[0]["gcamasbo"]);
			}
			rrHabitac.Close();
			StSqHabitac = "SELECT GPLANTAS,GHABITAC,GCAMASBO FROM DCAMASBOVA WHERE GPLANTAS = " + Convert.ToString(iPlanta) + 
			              " AND GHABITAC = " + iHabitac.ToString() + " AND GCAMASBO <> '" + stCama + "' and IESTCAMA in ('O','B')";
            command = new SqlCommand(StSqHabitac, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.RcAdmision.getCurrentTransaction());
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command);
			rrHabitac = new DataSet();
			tempAdapter_2.Fill(rrHabitac);
			result = rrHabitac.Tables[0].Rows.Count != 0;
			rrHabitac.Close();

			return result;
		}

		public void rellena_grid()
		{
			string StSqlPlanta = String.Empty;
			//Dim iServicioAux    As Integer
			//Dim lMedicoaux      As Long

			int lcol = 0;
			int lfil = 1;

			//iServicioAux = -1
			//lMedicoaux = -1
			SprCambios.MaxRows = 0;

			//StSqlPlanta = "select DSERVICI.dnomserv,DPERSONA.dap1pers,DPERSONA.dap2pers,DPERSONA.dnomperS,AMOVIMIE.* " _
			//'    & "from AMOVIMIE,DSERVICI,DPERSONA Where AMOVIMIE.itiposer = 'H' " _
			//'    & "and AMOVIMIE.ganoregi = " & iA�oRegistro & " And AMOVIMIE.gnumregi = " & iNumRegistro & " " _
			//'    & "and AMOVIMIE.gservior = DSERVICI.gservici AND AMOVIMIE.gmedicor = DPERSONA.gpersona "

			switch(stTipoRegimen)
			{
				case "RM" : 
					StSqlPlanta = "select AREGIMEN.* " + " from AREGIMEN Where " + " AREGIMEN.ganoadme = " + iA�oRegistro.ToString() + " And AREGIMEN.gnumadme = " + iNumRegistro.ToString() + " order by fmovimie"; 
					break;
				case "RA" : 
					StSqlPlanta = "select AMOVREGA.* " + " from AMOVREGA Where " + " AMOVREGA.ganoregi = " + iA�oRegistro.ToString() + " And AMOVREGA.gnumregi = " + iNumRegistro.ToString() + " AND " + " AMOVREGA.itiposer ='H' " + " order by fmovimie"; 
					break;
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqlPlanta, Conexion_Base_datos.RcAdmision);
			DataSet RrPlanta = new DataSet();
			tempAdapter.Fill(RrPlanta);
			if (RrPlanta.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in RrPlanta.Tables[0].Rows)
				{
					//el primer registro tiene que salir
					//guardamos el servicio y el medico del primero
					//If iServicioAux <> RrPlanta("gservior") Or lMedicoAux <> RrPlanta("gmedicor") Then
					SprCambios.MaxRows++;
					lfil++;
					SprCambios.Row = lfil;
					lcol++;
					SprCambios.Col = lcol;
					SprCambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm");
					lcol++;
					SprCambios.Col = lcol;
					switch(stTipoRegimen)
					{
						case "RM" :  
							SprCambios.Text = (Convert.ToString(iteration_row["iregimen"]) == "A") ? REGIMEN_ABIERTO : REGIMEN_CERRADO; 
							break;
						case "RA" :  
							SprCambios.Text = (Convert.ToString(iteration_row["iregaloj"]) == "I") ? REGIMEN_INDIVIDUAL : REGIMEN_COMPARTIDO; 
							break;
					}
					lcol++;
					SprCambios.Col = lcol;
					SprCambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
					//        End If
					lcol = 0;
				}
			}
			else
			{
				//no se recupera ningun movimiento
			}

			RrPlanta.Close();
		}

		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
            SqlCommand command; 

			//para select auxiliares
			string stTemp = String.Empty;
			DataSet rrtemp = null;
			//para las select de la transaccion
			string StSqAceptar = String.Empty;
			DataSet RrAceptar = null;
			string stFechaMovimiento = String.Empty;
			string stFechaSiguiente = String.Empty;

			//(parece que es algo que sobra debido a una horrible copi-paste)
			//Dim sEnf As String, sCama As String
			//Dim iSer As Integer, iPlan As Integer, ihab As Integer
			//Dim iMed As Long
			//Dim ServicioCambio As Integer
			//Dim MedicoCambio   As Long
			//Dim ServicioAnterior As Integer
			//Dim MedicoAnterior   As Long
			//Dim ServicioPosterior As Integer
			//Dim MedicoPosterior   As Long

			//****************************
            try
            {
                //*******************************
                this.Cursor = Cursors.WaitCursor;
                //la hora tiene que estar rellena
                if (tbHoraCambio.Text == "  :  ")
                {
                    this.Cursor = Cursors.Default;
                    short tempRefParam = 1040;
                    string[] tempRefParam2 = new string[] { Label19.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                    tbHoraCambio.Focus();
                    return;
                }

                //VALIDAR QUE LA FECHA SEA POSTERIOR AL �LTIMO MOVIMIENTO
                //NO PERMITIMOS CAMBIAR LA FECHA A CUALQUIER DIA POR QUE
                //SE DESCABALA EL ORDEN CRONOLOGICO DE LOS APUNTES CERRANDO UN APUNTE
                //CON FECHA ANTERIOR A LA DEL INICIO

                //se valida que la fecha/hora sea posterior a el �ltimo movimiento
                switch (stTipoRegimen)
                {
                    case "RM":
                        stTemp = " select max(fmovimie) FECHAMAXIMA from aregimen" + " where ganoadme=" + iA�oRegistro.ToString() + " and gnumadme=" + iNumRegistro.ToString() + " and ffinmovi is null";
                        break;
                    case "RA":
                        stTemp = " select max(fmovimie) FECHAMAXIMA from AMOVREGA" + " where ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " AND " + "       itiposer='H' AND ffinmovi is null";
                        break;
                }

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stTemp, Conexion_Base_datos.RcAdmision);
                rrtemp = new DataSet();
                tempAdapter.Fill(rrtemp);
                if (rrtemp.Tables[0].Rows.Count != 0)
                {
                    stFechaMaxMov = Convert.ToDateTime(rrtemp.Tables[0].Rows[0]["FECHAMAXIMA"]).ToString("dd/MM/yyyy HH:mm:ss");
                    if (Convert.ToDateTime(rrtemp.Tables[0].Rows[0]["FECHAMAXIMA"]) > DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
                    {
                        this.Cursor = Cursors.Default;
                        short tempRefParam3 = 1020;
                        string[] tempRefParam4 = new string[] { "la fecha del cambio", "mayor", "la fecha del �ltimo cambio " + Convert.ToDateTime(rrtemp.Tables[0].Rows[0]["FECHAMAXIMA"]).ToString("dd/MM/yyyy HH:mm:ss") };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                        rrtemp.Close();
                        SdcFecha.Focus();
                        return;
                    }
                }
                rrtemp.Close();

                //la fecha no puede ser menor que la fecha del ingreso
                if (DateTime.Parse(stFechaIngreso) > DateTime.Parse(SdcFecha.Value.Date.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
                {
                    this.Cursor = Cursors.Default;
                    System.DateTime TempDate = DateTime.FromOADate(0);
                    short tempRefParam5 = 1020;
                    string[] tempRefParam6 = new string[] { "la fecha del movimiento", "mayor", "la fecha de ingreso " + ((DateTime.TryParse(stFechaIngreso, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : stFechaIngreso) };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                    SdcFecha.Focus();
                    return;
                }

                using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                {
                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                        Conexion_Base_datos.RcAdmision.Close();
                    Conexion_Base_datos.RcAdmision.Open();
                    switch (stTipoRegimen)
                    {
                        case "RM":
                            StSqAceptar = "SELECT * from AREGIMEN where " + " GNUMADME = " + iNumRegistro.ToString() + " AND GANOADME = " + iA�oRegistro.ToString() + " and " + " FMOVIMIE =" + Serrores.FormatFechaHMS(stFechaMaxMov) + "";
                            break;
                        case "RA":
                            StSqAceptar = "SELECT * from AMOVREGA where " + " GNUMREGI = " + iNumRegistro.ToString() + " AND GANOREGI = " + iA�oRegistro.ToString() + " and ITIPOSER = 'H' AND " + " FMOVIMIE =" + Serrores.FormatFechaHMS(stFechaMaxMov) + "";
                            break;
                    }

                    command = new SqlCommand(StSqAceptar, Conexion_Base_datos.RcAdmision);
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command);
                    RrAceptar = new DataSet();
                    tempAdapter_2.Fill(RrAceptar);
                    if (RrAceptar.Tables[0].Rows.Count != 0)
                    {

                        //    SprCambios.Row = lRegistroSeleccionado
                        //    SprCambios.Col = 2
                        switch (stTipoRegimen)
                        {
                            case "RM":
                                IRegGrabar = (Convert.ToString(RrAceptar.Tables[0].Rows[0]["iregimen"]).Trim() == "A") ? "C" : "A";
                                break;
                            case "RA":
                                IRegGrabar = (Convert.ToString(RrAceptar.Tables[0].Rows[0]["iregaloj"]).Trim() == "I") ? "C" : "I";
                                break;
                        }

                        //Jes�s 10/04/2006 Indicar que se va a pasar a Individual cuando hay m�s camas en la habitaci�n
                        if (stTipoRegimen == "RA" && IRegGrabar == "I")
                        {
                            if (MasPacientesenHabitacion())
                            {
                                short tempRefParam9 = 1650;
                                string[] tempRefParam10 = new string[] { };
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                                if (Serrores.iresume != (int)System.Windows.Forms.DialogResult.OK)
                                {
                                    RrAceptar.Close();
                                    //indra jproche Aqui estaba ubicado un rollback que ya no  se necesita al usar TransactionScope 
                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                            }
                        }
                        //completo el �ltimo registro de amovimie
                        RrAceptar.Tables[0].Rows[0]["ffinmovi"] = DateTime.Parse(SdcFecha.Value.Date.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                        tempAdapter_2.Update(RrAceptar, RrAceptar.Tables[0].TableName);

                        RrAceptar.Close();
                        //**********grabo el nuevo registro en amovimie/amovrega
                        switch (stTipoRegimen)
                        {
                            case "RM":
                                StSqAceptar = "SELECT * FROM AREGIMEN where 2=1 ";
                                break;
                            case "RA":
                                StSqAceptar = "SELECT * FROM AMOVREGA WHERE 2=1";
                                break;
                        }
                        command = new SqlCommand(StSqAceptar, Conexion_Base_datos.RcAdmision);
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command);

                        RrAceptar = new DataSet();
                        tempAdapter_4.Fill(RrAceptar);

                        RrAceptar.AddNew();
                        RrAceptar.Tables[0].Rows[0]["FMOVIMIE"] = DateTime.Parse(SdcFecha.Value.Date.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
                        switch (stTipoRegimen)
                        {
                            case "RM":
                                RrAceptar.Tables[0].Rows[0]["GANOADME"] = iA�oRegistro;
                                RrAceptar.Tables[0].Rows[0]["GNUMADME"] = iNumRegistro;
                                RrAceptar.Tables[0].Rows[0]["IREGIMEN"] = IRegGrabar;
                                break;
                            case "RA":
                                RrAceptar.Tables[0].Rows[0]["GANOREGI"] = iA�oRegistro;
                                RrAceptar.Tables[0].Rows[0]["GNUMREGI"] = iNumRegistro;
                                RrAceptar.Tables[0].Rows[0]["ITIPOSER"] = "H";
                                RrAceptar.Tables[0].Rows[0]["IREGALOJ"] = IRegGrabar;
                                break;
                        }
                        RrAceptar.Tables[0].Rows[0]["GUSUARIO"] = Conexion_Base_datos.VstCodUsua;
                        RrAceptar.Tables[0].Rows[0]["FSISTEMA"] = vstfecha_hora_sys;

                        tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
                        tempAdapter_4.Update(RrAceptar, RrAceptar.Tables[0].TableName);
                        RrAceptar.Close();

                        //Jes�s 10/04/2006 Actualizo el registro en aepisadm cuando registro un nuevo regimen de alojamiento
                        if (stTipoRegimen == "RA")
                        {
                            ActualizarAEPISADM(IRegGrabar);
                        }
                        scope.Complete();
                    }

                    IniciarPantalla();
                    rellena_grid();

                    this.Cursor = Cursors.Default;
                }
            }
            catch (SqlException ex)
            {
                //indra jproche Aqui estaba ubicado un rollback que ya no  se necesita al usar TransactionScope 
                this.Cursor = Cursors.Default;
                //deberia haber un select case que diferencie casos de update o de lectura
                Serrores.GestionErrorUpdate(ex.Number, Conexion_Base_datos.RcAdmision);
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
            finally
            {
                SprCambios.CurrentRow = null;
            }
		}

		private void cbBorrar_Click(Object eventSender, EventArgs eventArgs)
		{
           
            SqlCommand command; 

         	//solo esta activo si se selecciona el ultimo movimiento y no es el primero
			//Hay que buscar en dmovfact para comprobar si hay movimientos facturados
			string stFechaMovimiento = String.Empty; //fecha del movimiento seleccionado
			string stSql = String.Empty;
			DataSet RrSql = null;
			DataSet RRSQL2 = null;

			try
			{
				this.Cursor = Cursors.WaitCursor;

				if (lRegistroSeleccionado > 0)
				{
                    if (lRegistroSeleccionado == SprCambios.MaxRows && lRegistroSeleccionado > 1)
                    { //si estamos en el ultimo registro
                        SprCambios.Row = lRegistroSeleccionado;
                        SprCambios.Col = 3;
                        stFechaMovimiento = SprCambios.Text;
                        //buscar si hay movimientos en dmovfact
                        stSql = "select * from DMOVFACT where itiposer = 'H' and " + " ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " ffinicio >=" + Serrores.FormatFechaHMS(stFechaMovimiento) + "";

                        if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
                        {
                            string tempRefParam2 = "DMOVFACT";
                            stSql = stSql + " UNION ALL " + Serrores.proReplace(ref stSql, tempRefParam2, "DMOVFACH");
                        }

                        SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
                        RrSql = new DataSet();
                        tempAdapter.Fill(RrSql);
                        if (RrSql.Tables[0].Rows.Count != 0)
                        {
                            //si hay filas en DMOVFACT no se le permite eliminar los registros
                            this.Cursor = Cursors.Default;
                            RrSql.Close();
                            short tempRefParam3 = 1940;
                            string[] tempRefParam4 = new string[] { "eliminar", "ya ha sido facturado" };
                            Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
                            return;
                        }
                        this.Cursor = Cursors.Default;

                        short tempRefParam5 = 1490;
                        string[] tempRefParam6 = new string[] { "los datos seleccionados" };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                        if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.No)
                        {
                            //si no se quiere eliminar							
                            RrSql.Close();
                            return;
                        }

                        this.Cursor = Cursors.WaitCursor;
                        //no hay filas en dmovfact se puede borrar
                        //Buscamos en amovimie si hay movimientos posteriores al seleccionado  

                        using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                        {
                            //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                            if (Conexion_Base_datos.RcAdmision.State == ConnectionState.Open)
                                Conexion_Base_datos.RcAdmision.Close();
                            Conexion_Base_datos.RcAdmision.Open();

                            //se borra la fila seleccionada
                            switch (stTipoRegimen)
                            {
                                case "RM":
                                    stSql = "SELECT * FROM AREGIMEN where " + " ganoadme=" + iA�oRegistro.ToString() + " and gnumadme=" + iNumRegistro.ToString() + " and " + " fmovimie = " + Serrores.FormatFechaHMS(stFechaMovimiento) + "";
                                    break;
                                case "RA":
                                    stSql = "SELECT * FROM AMOVREGA where " + " ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " itiposer='H' AND " + " fmovimie = " + Serrores.FormatFechaHMS(stFechaMovimiento) + "";
                                    break;
                            }
                            command = new SqlCommand(stSql, Conexion_Base_datos.RcAdmision);
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command);
                            RrSql = new DataSet();
                            tempAdapter_2.Fill(RrSql);
                            if (RrSql.Tables[0].Rows.Count == 1)
                            {
                                //Jes�s 10/04/2006 Si va a borrar es que hay mas de un registro y si el que va a borrar es de tipo C (Compartido) quiere decir
                                //que el que se va a quedar va a ser I (Individual)
                                //Indicar que se va a pasar a Individual cuando hay m�s camas en la habitaci�n
                                if (stTipoRegimen == "RA")
                                {
                                    if (Convert.ToString(RrSql.Tables[0].Rows[0]["iregaloj"]) == "C" && MasPacientesenHabitacion())
                                    {
                                        short tempRefParam9 = 1650;
                                        string[] tempRefParam10 = new string[] { };
                                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                                        if (Serrores.iresume != (int)System.Windows.Forms.DialogResult.OK)
                                        {
                                            RrSql.Close();
                                            //indra jproche Aqui estaba ubicado un rollback que ya no  se necesita al usar TransactionScope 
                                            this.Cursor = Cursors.Default;
                                            return;
                                        }
                                    }
                                }
                                //fin Jes�s
                                RrSql.Delete(tempAdapter_2);
                            }
                            RrSql.Close();

                            switch (stTipoRegimen)
                            {
                                case "RM":
                                    stSql = "select ffinmovi, ganoadme, gnumadme, fmovimie from aregimen where  ganoadme=" + iA�oRegistro.ToString() + " and gnumadme=" + iNumRegistro.ToString() +
                                            " and ffinmovi is not null and fmovimie = (select max(fmovimie) from aregimen where " +
                                            " ganoadme=" + iA�oRegistro.ToString() + " and gnumadme=" + iNumRegistro.ToString() + ")";
                                    break;
                                case "RA":
                                    stSql = "select ffinmovi, iregaloj, itiposer, ganoregi, gnumregi, fmovimie from AMOVREGA where  ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() +
                                            " and ffinmovi is not null and itiposer='H' AND " +
                                            " fmovimie = (select max(fmovimie) from AMOVREGA where " +
                                            "             ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and itiposer='H')";
                                    break;
                            }

                            command = new SqlCommand(stSql, Conexion_Base_datos.RcAdmision);
                            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(command);

                            RRSQL2 = new DataSet();
                            tempAdapter_3.Fill(RRSQL2);
                            if (RRSQL2.Tables[0].Rows.Count == 1)
                            {
                                RRSQL2.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
                                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                                tempAdapter_3.Update(RRSQL2, RRSQL2.Tables[0].TableName);
                            }
                            //Jes�s 10/04/2006 Actualizo el registro en aepisadm al �ltimo regimen de alojamiento del episodio
                            if (stTipoRegimen == "RA")
                            {
                                ActualizarAEPISADM(Convert.ToString(RRSQL2.Tables[0].Rows[0]["iregaloj"]));
                            }
                            RRSQL2.Close();
                            scope.Complete();

                            IniciarPantalla();
                            rellena_grid();

                        }
                    }
                    else
                    {
                        //si es una fila diferente a la ultima
                    }
				}
				else
				{
					//si no hay ninguno seleccionado
				}

				this.Cursor = Cursors.Default;
			}
            
			catch (Exception ex)
			{
                //indra jproche Aqui estaba ubicado un rollback que ya no  se necesita al usar TransactionScope 
				this.Cursor = Cursors.Default;
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_R�gimen:cbBorrar", ex);
			}            

		}

		private void cbCambio_Click(Object eventSender, EventArgs eventArgs)
		{
			string stFechaMovimiento = String.Empty;
			string StSqAceptar = String.Empty;
			DataSet RrAceptar = null;

			try
			{

				//modifica el registro seleccionado
				if (lRegistroSeleccionado > 0)
				{
					vstAccion = "CAMBIO";
					//tiene que existir un registro seleccionado
					SprCambios.Row = lRegistroSeleccionado;
					SprCambios.Col = 3;
					stFechaMovimiento = SprCambios.Text;

					//buscar si hay movimientos en dmovfact
                    StSqAceptar = "select * from DMOVFACT where itiposer = 'H' and " + " ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " ffinicio >=" + Serrores.FormatFechaHMS(stFechaMovimiento) + "";
					
					if (Serrores.proExisteTabla("DMOVFACH", Conexion_Base_datos.RcAdmision))
					{
                        StSqAceptar = StSqAceptar + " UNION ALL " + Serrores.proReplace(ref StSqAceptar, "DMOVFACT", "DMOVFACH");
					}

					SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqAceptar, Conexion_Base_datos.RcAdmision);
					RrAceptar = new DataSet();
					tempAdapter.Fill(RrAceptar);
					if (RrAceptar.Tables[0].Rows.Count != 0)
					{
						//si hay filas en DMOVFACT no se le permite modificar los registros						
						RrAceptar.Close();
						this.Cursor = Cursors.Default;
						short tempRefParam3 = 1940;
                        string[] tempRefParam4 = new string[] { "modificar", "ya ha sido facturado" };
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
						return;
					}
					RrAceptar.Close();

					SprCambios.Row = lRegistroSeleccionado;
					SprCambios.Col = 2;

					switch(stTipoRegimen)
					{
						case "RM" : 
							IRegGrabar = (SprCambios.Text.Trim() == REGIMEN_ABIERTO) ? "C" : "A"; 
                            SqlCommand tempCommand = new SqlCommand("UPDATE aregimen SET iregimen='" + IRegGrabar + "',gusuario='" + Conexion_Base_datos.VstCodUsua + "',fsistema =" + Serrores.FormatFechaHMS(vstfecha_hora_sys) + "" + " Where ganoadme=" + iA�oRegistro.ToString() + " and " + " gnumadme=" + iNumRegistro.ToString() + " and " + " fmovimie=" + Serrores.FormatFechaHMS(stFechaMovimiento) + "", Conexion_Base_datos.RcAdmision); 
							tempCommand.ExecuteNonQuery(); 
							break;
						case "RA" : 
							IRegGrabar = (SprCambios.Text.Trim() == REGIMEN_INDIVIDUAL) ? "C" : "I"; 
							//Jes�s 10/04/2006 Indicar que se va a pasar a Individual cuando hay m�s camas en la habitaci�n 
							if (IRegGrabar == "I" && MasPacientesenHabitacion())
							{
								short tempRefParam7 = 1650;
                                string[] tempRefParam8 = new string[] { };
								Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
								if (Serrores.iresume != (int)System.Windows.Forms.DialogResult.OK)
								{
									this.Cursor = Cursors.Default;
									return;
								}
							} 
                            SqlCommand tempCommand_2 = new SqlCommand("UPDATE AMOVREGA SET iregaloj='" + IRegGrabar + "',gusuario='" + Conexion_Base_datos.VstCodUsua + "',fsistema =" + Serrores.FormatFechaHMS(vstfecha_hora_sys) + "" + " Where ganoregi=" + iA�oRegistro.ToString() + " and " + " gnumregi=" + iNumRegistro.ToString() + " and " + " itiposer='H' and " + " fmovimie=" + Serrores.FormatFechaHMS(stFechaMovimiento) + "", Conexion_Base_datos.RcAdmision); 
							tempCommand_2.ExecuteNonQuery(); 
							//Jes�s 10/04/2006 Actualizo el registro en aepisadm al �ltimo regimen de alojamiento del episodio 
							ActualizarAEPISADM(IRegGrabar); 
							break;
					}

					IniciarPantalla();
					rellena_grid();
                    SprCambios.CurrentRow = null;


					// hay que activar
					//   Frame1.Enabled = True 'servicio
					Frame3.Enabled = false; //fecha/hora del cambio
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;
					//   Frame4.Enabled = True  'medico
				}
				else
				{
					//MsgBox "debe seleccionar un registro para modificar"
					short tempRefParam11 = 1270;
                    string[] tempRefParam12 = new string[] { "un registro" };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, Conexion_Base_datos.RcAdmision, tempRefParam12));
					//    Frame1.Enabled = False 'servicio
					Frame3.Enabled = false; //fecha/hora del cambio
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;
					//    Frame4.Enabled = False   'medico
				}
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_R�gimen:cbCambio", ex);
			}

		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		//(parece que es algo que sobra debido a una horrible copi-paste)
		//Private Sub cbImprimir_Click()
		//Dim LitPersona As String
		//Dim LitPersona1 As String
		//
		//    On Error GoTo Etiqueta
		//    Set CrystalReport1 = LISTADO_CRYSTAL
		//    Me.MousePointer = 11
		//    pro_query
		//
		//    '*********cabeceras
		//    If VstrCrystal.VfCabecera = False Then
		//        proCabCrystal
		//    End If
		//    '*******************
		//    CrystalReport1.Formulas(0) = "FORM1= """ & VstrCrystal.VstGrupoHo & """"
		//    CrystalReport1.Formulas(1) = "FORM2= """ & VstrCrystal.VstNombreHo & """"
		//    CrystalReport1.Formulas(2) = "FORM3= """ & VstrCrystal.VstDireccHo & """"
		//    LitPersona = ObtenerLiteralPersona(RcAdmision)
		//    LitPersona = UCase(LitPersona)
		//    CrystalReport1.Formulas(3) = "Litpers= """ & LitPersona & """"
		//    LitPersona1 = "CAMBIOS DE SERVICIO Y M�DICO DE " & UCase(LitPersona) & "S"
		//    CrystalReport1.Formulas(4) = "LitpersTitulo= """ & LitPersona1 & """"
		//
		//    CrystalReport1.ReportFileName = "" & PathString & "\rpt\agi415r1.rpt"
		//    CrystalReport1.WindowTitle = "Listado de Movimientos"
		//    CrystalReport1.WindowState = crptMaximized
		//    CrystalReport1.Destination = crptToWindow
		//    CrystalReport1.Connect = "DSN=" & VstCrystal & ";UID=" & gUsuario & ";PWD=" & gContrase�a & ""
		//    CrystalReport1.Action = 1
		//    Me.MousePointer = 0
		//
		//    vacia_formulas CrystalReport1, 5
		//    stSeleccion = ""
		//    Exit Sub
		//Etiqueta:
		//    MsgBox "Error: " & Err.Number & " " & Err.Description
		//    Me.MousePointer = 0
		//End Sub

		private void cbNuevo_Click(Object eventSender, EventArgs eventArgs)
		{
			//se a�ade un nuevo registro al spread
			try
			{
				vstAccion = "NUEVO";
				//si hay alguna fila seleccionada se tendra que deseleccionar                
                SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				fFilaSeleccionada = false;
				lRegistroSeleccionado = 0;
				//se tendran que activar los campos de entrada de datos
				//Frame1.Enabled = True
				Frame3.Enabled = true;
				Label21.Enabled = true;
				SdcFecha.Enabled = true;
				tbHoraCambio.Enabled = true;
				Label19.Enabled = true;
				//Frame4.Enabled = True

				SdcFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
				tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
				vstSegundos = DateTime.Now.ToString("ss");
				CbAceptar.Enabled = true;
				//(parece que es algo que sobra debido a una horrible copi-paste)
				//cargo en medico y servicio gserulti y gperulti de AEPISADM
				//proMedicoDe   'Pongo el m�dico
				//proServicio   'Lleno el  Servicio
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Conexion_Base_datos.VstCodUsua, "Cambio_R�gimen:cbNuevo", ex);
			}

		}

		private void Cambio_R�gimen_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				//Frame1.Refresh
				Frame2.Refresh();
				Frame3.Refresh();
				//Frame4.Refresh
				Frame5.Refresh();

				rellena_grid();

				IniciarPantalla();

				//Frame1.Enabled = False
				//Frame3.Enabled = False
				//Frame4.Enabled = False
				//cbBorrar.Enabled = False
				//cbCambio.Enabled = False
				//cbNuevo.Enabled = True

			}
		}

		//(parece que es algo que sobra debido a una horrible copi-paste)
		//Sub pro_query2()
		//    StSqlPanta = " SELECT amovimie.fmovimie as fechaini,dservici.dnomserv as serviori,a.dnomserv as servides, " _
		//'        & " dpersona.dap1pers as ape1ori,dpersona.dap2pers as ape2ori,dpersona.dnomperS as nombori," _
		//'        & " B.dap1pers as ape1DES,B.dap2pers as ape2DES,B.dnomperS as nombDES," _
		//'        & " amovimie.ffinmovi as fechacam,amovimie.ganoregi,amovimie.gnumregi " _
		//'        & " FROM AMOVIMIE, AEPISADM, DSERVICI, DPERSONA, " _
		//'        & " DSERVICI AS A, DPERSONA AS B " _
		//'        & " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " _
		//'        & " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " _
		//'        & " AEPISADM.faltplan IS NULL AND " _
		//'        & " (AMOVIMIE.gservior <> AMOVIMIE.gservide or " _
		//'        & " AMOVIMIE.gmedicor <> AMOVIMIE.gmedicde) " _
		//'        & " AND " _
		//'        & " AMOVIMIE.gservior = DSERVICI.gservici AND " _
		//'        & " AMOVIMIE.gservide = A.gservici AND " _
		//'        & " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " _
		//'        & " AMOVIMIE.gmedicde = B.gpersona AND " _
		//'        & " AEPISADM.GIDENPAC = '" & stCodpac & "' and AEPISADM.faltplan is null"
		//Exit Sub
		//Etiqueta:
		//     AnalizaError "admision", App.Path, VstCodUsua, "Cambio_se_me_en:pro_query2", RcAdmision
		//End Sub

		//Sub pro_query()
		//Dim stsql  As String
		//
		//On Error GoTo Etiqueta
		//
		//    stSeleccion = "Movimientos del paciente"
		//    stsql = " SELECT * " _
		//'        & " FROM AMOVIMIE, AEPISADM, DPACIENT, DSERVICI, DPERSONA, " _
		//'        & " DSERVICI AS A, DPERSONA AS B " _
		//'        & " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " _
		//'        & " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " _
		//'        & " AEPISADM.faltplan IS NULL AND " _
		//'        & " AEPISADM.gidenpac = DPACIENT.gidenpac AND " _
		//'        & " (AMOVIMIE.gservior <> AMOVIMIE.gservide or " _
		//'        & " AMOVIMIE.gmedicor <> AMOVIMIE.gmedicde) " _
		//'        & " AND " _
		//'        & " AMOVIMIE.gservior = DSERVICI.gservici AND " _
		//'        & " AMOVIMIE.gservide = A.gservici AND " _
		//'        & " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " _
		//'        & " AMOVIMIE.gmedicde = B.gpersona AND " _
		//'        & " AEPISADM.GIDENPAC = '" & stCodpac & "' and AEPISADM.faltplan is null"
		//
		//        '& " ORDER BY DAPE1PAC,DAPE2PAC,DNOMBPAC,ffinmovi ASC "
		//    CrystalReport1.SortFields(0) = "+{DPACIENT.DAPE1PAC}"
		//    CrystalReport1.SortFields(1) = "+{DPACIENT.DAPE2PAC}"
		//    CrystalReport1.SortFields(2) = "+{DPACIENT.DNOMBPAC}"
		//    CrystalReport1.SortFields(3) = "+{AMOVIMIE.FFINMOVI}"
		//
		//    CrystalReport1.Formulas(3) = "SELECCION= """ & stSeleccion & """"
		//    CrystalReport1.SQLQuery = stsql
		//Exit Sub
		//Etiqueta:
		//     AnalizaError "admision", App.Path, VstCodUsua, "Cambio_se_me_en:pro_query", RcAdmision
		//End Sub

        private void Cambio_R�gimen_Load(Object eventSender, EventArgs eventArgs)
		{
			Application.DoEvents();
			int n = Application.OpenForms.Count;
			this.Top = 1;
			this.Left = (int) 0;
			//Me.Height = 4215
			//Me.Width = 6540
			tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
			vstSegundos = DateTime.Now.ToString("ss");
			CbAceptar.Enabled = false;
			SdcFecha.MaxDate = DateTime.Now;

			//oscar
			string stSql = "SELECT valfanu1, valfanu2 FROM SCONSGLO WHERE gconsglo='REGFACUL'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion_Base_datos.RcAdmision);
			DataSet Rr = new DataSet();
			tempAdapter.Fill(Rr);
			if (Rr.Tables[0].Rows.Count != 0)
			{
				//DIEGO 19-09-2006 - Controlamos si VALFANU1 o VALFANU2 son NULL
				REGIMEN_CERRADO = (Convert.IsDBNull(Rr.Tables[0].Rows[0]["VALFANU1"])) ? "Cerrado" : Convert.ToString(Rr.Tables[0].Rows[0]["VALFANU1"]).Trim();
				REGIMEN_ABIERTO = (Convert.IsDBNull(Rr.Tables[0].Rows[0]["VALFANU2"])) ? "Abierto" : Convert.ToString(Rr.Tables[0].Rows[0]["VALFANU2"]).Trim();
				///DIEGO 19-09-2006
				//UPGRADE_ISSUE: (2064) Form property Cambio_R�gimen.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//this.setHelpContextID(125);
			}
			else
			{
				REGIMEN_CERRADO = "Cerrado";
				REGIMEN_ABIERTO = "Abierto";
				//UPGRADE_ISSUE: (2064) Form property Cambio_R�gimen.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//this.setHelpContextID(1251);
			}			
			Rr.Close();

			REGIMEN_INDIVIDUAL = "Individual";
			REGIMEN_COMPARTIDO = "Compartido";

			string LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label28.Text = LitPersona;

			//-----
		}

		private void Cambio_R�gimen_Closed(Object eventSender, EventArgs eventArgs)
		{
			menu_ingreso.DefInstance.proQuitarMatriz(stCodpac, this);
		}
       
        private void SprCambios_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;
            //cuando se selecciona un registro tendriamos que activar un boton u otro
            //dependiendo del registro seleccionado
            SprCambios.Row = Row;
            //se se selecciona el registro de cabecera o no se selecciona ninguno
            if (SprCambios.Row <= 0)
            {
                return;
            }

            if (lRegistroSeleccionado == Row)
            {
                //si el seleccionado coincide con el seleccionado anterior
                if (fFilaSeleccionada)
                {
                    SprCambios.CurrentRow = null;
                    fFilaSeleccionada = false;
                }
                else
                {
                    fFilaSeleccionada = true;
                }
            }
            else
            {                
                fFilaSeleccionada = true;
            }

            if (fFilaSeleccionada)
            {
                lRegistroSeleccionado = Row;
                //habria que cargar los datos del registro seleccionado en la pantalla
                //se pone la fecha del registro seleccionado
                SprCambios.Row = lRegistroSeleccionado;
                SprCambios.Col = 3;
                SdcFecha.Text = DateTime.Parse(SprCambios.Text).ToString("dd/MM/yyyy");
                tbHoraCambio.Text = DateTime.Parse(SprCambios.Text).ToString("HH:mm");
                vstSegundos = DateTime.Parse(SprCambios.Text).ToString("ss");
                //se cargan los datos del seleccionado

                //se protegen los campos de la pantalla
                Frame3.Enabled = false;
                Label21.Enabled = false;
                SdcFecha.Enabled = false;
                tbHoraCambio.Enabled = false;
                Label19.Enabled = false;
                //    Frame4.Enabled = False

                cbBorrar.Enabled = (lRegistroSeleccionado == SprCambios.MaxRows && lRegistroSeleccionado > 1);

                cbCambio.Enabled = (lRegistroSeleccionado == 1 && SprCambios.MaxRows == 1);

                cbNuevo.Enabled = false;
            }
            else
            {
                //se trata de la cabecera
                lRegistroSeleccionado = 0;
                //hay que limpiar los campos de la pantalla
                IniciarPantalla();
                cbBorrar.Enabled = false;
                cbCambio.Enabled = false;
                cbNuevo.Enabled = true;
            }
        }

		private void tbHoraCambio_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			sthorasystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("HH")));
			stminutossystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("mm")));

		}

		private void tbHoraCambio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbHoraCambio.SelectionStart = 0;
			tbHoraCambio.SelectionLength = Strings.Len(tbHoraCambio.Text);
		}

		private void tbHoraCambio_Leave(Object eventSender, EventArgs eventArgs)
		{
			int tiI = 0;
			int sthora = 0;
			int stminutos = 0;
			string stfecha = String.Empty;
			//*****************
			try
			{
				//******************
				if (this.ActiveControl.Name == CbCancelar.Name)
				{
					CbCancelar_Click(CbCancelar, new EventArgs());
					return;
				}
				tiI = (tbHoraCambio.Text.IndexOf(':') + 1);
				if (tbHoraCambio.Text != "  :  ")
				{
					if (Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(0, Math.Min(tiI - 1, tbHoraCambio.Text.Length)))) > 24 || Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(tiI))) > 60)
					{
						//MsgBox "Hora Incorrecta", vbOKOnly + vbInformation, "Admisi�n"
						short tempRefParam = 1150;
                        string[] tempRefParam2 = new string[] { "Hora" };
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
						tbHoraCambio.Focus();
						return;
					}
					sthora = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("HH")));
					stminutos = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("mm")));
					stfecha = DateTime.Now.ToString("dd/MM/yyyy");
					if (stfecha == SdcFecha.Text)
					{
						if (sthora == sthorasystem)
						{
							if (stminutos > stminutossystem)
							{
								//ERROR MINUTOS MAYOR QUE LOS DEL SYSTEMA
								short tempRefParam3 = 1020;
                                string[] tempRefParam4 = new string[] { "La hora no", "mayor", "la del sistema" };
								Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
								tbHoraCambio.Focus();
								return;
							}
						}
						else
						{
							if (sthora > sthorasystem)
							{
								//ERROR HORA MAYOR QUE LA DEL SYSTEMA
								short tempRefParam5 = 1020;
                                string[] tempRefParam6 = new string[] { "La hora no", "mayor", "la del sistema" };
								Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
								tbHoraCambio.Focus();
								return;
							}
						}
					}
					CbAceptar.Enabled = true;
					tbHoraCambio.Text = DateTime.Parse(tbHoraCambio.Text).ToString("HH") + ":" + DateTime.Parse(tbHoraCambio.Text).ToString("mm");
				}
				else
				{
					CbAceptar.Enabled = false;
				}

				vstSegundos = DateTime.Now.ToString("ss");
			}
			//*******************
			catch (InvalidCastException e)
			{
				short tempRefParam7 = 1150;
                string[] tempRefParam8 = new string[] { "Hora" };
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
				tbHoraCambio.Focus();
				return;
			}
			//ojo error 13 si deja en blanco
		}

		private void IniciarPantalla()
		{
            SprCambios.MaxCols = 3;
            SprCambios.Row = 0;
            SprCambios.Col = 1;
            SprCambios.Text = "Fecha Cambio";
            SprCambios.Col = 2;
            SprCambios.Text = "R�gimen";
            SprCambios.Col = 3;
            SprCambios.Text = "FechaFin";
            SprCambios.SetColHidden(SprCambios.Col, true);
            SprCambios.MasterTemplate.EnableSorting = false;
            SprCambios.ReadOnly = true;
            

			//se pone la fecha actual
			SdcFecha.MaxDate = DateTime.Now;
			vstfecha = DateTimeHelper.ToString(DateTime.Today);
			//el mindate tiene que ser la fecha del ingreso
			System.DateTime TempDate = DateTime.FromOADate(0);
			SdcFecha.MinDate = DateTime.Parse((DateTime.TryParse(stFechaIngreso, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : stFechaIngreso);

			SdcFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
			tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
			vstSegundos = DateTime.Now.ToString("ss");
			vstfecha_hora_sys = DateTimeHelper.ToString(DateTime.Now);

			switch(stTipoRegimen)
			{
				case "RM" :  
					this.Text = "Cambio de R�gimen M�dico - AGI125F1"; 
					break;
				case "RA" :  
					this.Text = "Cambio de R�gimen de Alojamiento - AGI125F1"; 
					break;
			}

			//se limpian los campos de la pantalla


			//se protegen los campos de la pantalla
			Frame3.Enabled = false;
			Label21.Enabled = false;
			SdcFecha.Enabled = false;
			tbHoraCambio.Enabled = false;
			Label19.Enabled = false;


			cbBorrar.Enabled = false;
			cbCambio.Enabled = false;
			cbNuevo.Enabled = true;
			CbAceptar.Enabled = false;

			vstAccion = "";

            //se desmarca el spread
            SprCambios.CurrentRow = null;
            fFilaSeleccionada = false;

		}

		private void ActualizarAEPISADM(string iRegAlojam)
		{
			//rutina que actualiza AEPISADM con los datos que le mandemos

			string sql = "SELECT * FROM AEPISADM WHERE " + " AEPISADM.ganoadme = " + iA�oRegistro.ToString() + " AND AEPISADM.gnumadme =  " + iNumRegistro.ToString();
            SqlCommand command = new SqlCommand(sql, Conexion_Base_datos.RcAdmision, Conexion_Base_datos.RcAdmision.getCurrentTransaction());
            SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
			DataSet RrActualizar = new DataSet();
			tempAdapter.Fill(RrActualizar);
			if (RrActualizar.Tables[0].Rows.Count != 0)
			{				
				RrActualizar.Tables[0].Rows[0]["IREGALOJ"] = iRegAlojam;
                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrActualizar, RrActualizar.Tables[0].TableName);                
			}
			RrActualizar.Close();
		}
	}
}