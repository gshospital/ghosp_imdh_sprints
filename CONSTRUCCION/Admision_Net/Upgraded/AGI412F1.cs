using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using CrystalWrapper;

namespace ADMISION
{
	public partial class ingresos_por_entidades: Telerik.WinControls.UI.RadForm
    {

		public ingresos_por_entidades()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}


		private void ingresos_por_entidades_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		DataSet Rr1 = null;
		DataSet Rr2 = null;
		DataSet Rr3 = null;
		DataSet Rr4 = null;

		string sql = String.Empty;
		string sql2 = String.Empty;
		string sql3 = String.Empty;
		string sql4 = String.Empty;
		string stNombreHo = String.Empty;
		string stGrupoHo = String.Empty;
		string stDireccHo = String.Empty;
		string[] Carga_Enti = null;
		string[] Carga_Servi = null;
		string stFechaini = String.Empty;
		string stFechafin = String.Empty;
		string stIntervalo = String.Empty;
		string stEntidad = String.Empty;
		string stMensaje1 = String.Empty;
		string stMensaje2 = String.Empty;
		// Cuando se cambie el n�mero m�ximo de entidades a selecionar hay que cambiar
		// el indice de al variable matricial mtemp(n) y dar el mismo valor a la variable
		// iNumero que se carga en el load del form
		string[] mTemp = new string[]{String.Empty, String.Empty, String.Empty, String.Empty, String.Empty};
		string stOrden = String.Empty;
		int iNumero = 0;
		int viform = 0;
		int iNumero_Lista1 = 0;
		int iNumero_Lista2 = 0;
		int iNumero_Lista1_Ser = 0;
		int iNumero_Lista2_Ser = 0;
		bool tIndicador = false;
		int StPrivado = 0;
		int StSeguSoc = 0;
        
		Crystal CrystalReport1 = new Crystal();

		public void proCarga_Lista1_Todos()
		{
			int i = 0;
			string sqlLista = String.Empty;
			try
			{
				sqlLista = " SELECT * FROM DSOCIEDA";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["dsocieda"]) != "" && Convert.ToDouble(iteration_row["gsocieda"]) != StPrivado && Convert.ToDouble(iteration_row["gsocieda"]) != StSeguSoc)
					{
						LsbLista1.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["dsocieda"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row["gsocieda"])));
						Carga_Enti = ArraysHelper.RedimPreserve(Carga_Enti, new int[]{i + 1});
						Carga_Enti[i] = Convert.ToString(iteration_row["dsocieda"]).Trim();						
						i++;
					}
				}
				
				Rr1.Close();
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		public void proCarga_Lista_Servicios()
		{
			int i = 0;
			string sqlLista = String.Empty;
			try
			{
				sqlLista = " SELECT * FROM DSERVICI WHERE FBORRADO IS NULL and ihospita = 'S' order by dnomserv";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					List1Servicios.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["dnomserv"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row["gservici"])));
					Carga_Servi = ArraysHelper.RedimPreserve(Carga_Servi, new int[]{i + 1});
					Carga_Servi[i] = Convert.ToString(iteration_row["dnomserv"]).Trim();					
					i++;
				}
				
				Rr1.Close();
			}
			catch (SqlException ex)
			{
				
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		public void proCarga_Lista2_Servicios()
		{
			int i = 0;
			string sqlLista = String.Empty;
			try
			{
				sqlLista = " SELECT * FROM DSERVICI WHERE FBORRADO IS NULL and ihospita = 'S' order by dnomserv";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
                {                                            
                    List2Servicios.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["dnomserv"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row["gservici"])));
					Carga_Servi = ArraysHelper.RedimPreserve(Carga_Servi, new int[]{i + 1});
					Carga_Servi[i] = Convert.ToString(iteration_row["dnomserv"]).Trim();                                        
					i++;
				}
				
				Rr1.Close();
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		public void prolistado_query2()
		{
			string Vst_Tipomov = String.Empty;
			string Constante = String.Empty;
			DLLTextosSql.TextosSql oTextoSql = null;
			object[] MatrizSql = null;
			int[] MatrizAux = null;
			// Formateo las variables de fecha al formato SQL, y las variables a cero
			try
			{

				stFechaini = Dtfechaini.Text + " " + "00:00:00";
				stFechafin = Dtfechafin.Text + " " + "23:59:59";
				stEntidad = "";
				stOrden = "";				
				if (mTemp != null)
				{
					Array.Clear(mTemp, 0, mTemp.Length);
				}
				if (RbNingreso.IsChecked)
				{
					stOrden = "Ordenado por: N�mero de Ingreso";
				}
				else
				{
					stOrden = "Ordenado por: Apellidos, Nombre";
				}


				if (RbIngreso.CheckState == CheckState.Checked)
				{
					//para que salga la situcion en la que esta el paciente
					Vst_Tipomov = "Ingresos de ";
				}
				else
				{
					Vst_Tipomov = "Altas de ";
				}
				if (Dtfechaini.Value.Date <= Dtfechafin.Value.Date)
				{
					if (RbSs.IsChecked || RbPrivado.IsChecked)
					{
						if (RbSs.IsChecked)
						{
							stEntidad = Vst_Tipomov + "Entidad Financiadora: Seguridad Social";
							Constante = "SEGURSOC";
						}
						else
						{
							stEntidad = Vst_Tipomov + "Entidad Financiadora: Privado";
							Constante = "PRIVADO";
						}
						if (RbIngreso.CheckState == CheckState.Checked)
						{
							//''ReDim MatrizSql(3)'''MatrizSql(1) = FormatFechaHMS(stFechaini)'''MatrizSql(2) = FormatFechaHMS(stFechafin)'''MatrizSql(3) = Constante'''Set oTextoSql = CreateObject("DLLTEXTOSSQL.TEXTOSSQL")'''sql = oTextoSql.devuelvetexto(VstTipoBD, "ADMISION", "AGI412F1" _''', "prolistado_query2", 1, MatrizSql, RcAdmision)'''Set oTextoSql = Nothing
							//incluir en todos los sql las entidades
							MatrizSql = new object[5];
							
							object tempRefParam = stFechaini;
							MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam);
							stFechaini = Convert.ToString(tempRefParam);							
							
							object tempRefParam2 = stFechafin;
							MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam2);
							stFechafin = Convert.ToString(tempRefParam2);
							MatrizSql[3] = Constante;
							MatrizAux = new int[1];
                            MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { List2Servicios.Items.Count });
                            for (int i = 0; i <= List2Servicios.Items.Count - 1; i++)
							{								
                                MatrizAux[i] = Convert.ToInt32(List2Servicios.Items[i].Value);
                            }
							MatrizSql[4] = ArraysHelper.DeepCopy(MatrizAux);

							oTextoSql = new DLLTextosSql.TextosSql();							
							string tempRefParam3 = "ADMISION";
							string tempRefParam4 = "AGI412F1";
							string tempRefParam5 = "prolistado_query2";
							short tempRefParam6 = 1;
							sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, MatrizSql, Conexion_Base_datos.RcAdmision));
							oTextoSql = null;

						}
						else
						{
							//''            ReDim MatrizSql(3)'''            MatrizSql(1) = FormatFechaHMS(stFechaini)'''            MatrizSql(2) = FormatFechaHMS(stFechafin)'''            MatrizSql(3) = Constante'''            Set oTextoSql = CreateObject("DLLTEXTOSSQL.TEXTOSSQL")'''            sql = oTextoSql.devuelvetexto(VstTipoBD, "ADMISION", "AGI412F1" _'''               , "prolistado_query2", 2, MatrizSql, RcAdmision)'''            Set oTextoSql = Nothing
							MatrizSql = new object[5];						
							object tempRefParam8 = stFechaini;
							MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam8);
							stFechaini = Convert.ToString(tempRefParam8);				
							
							object tempRefParam9 = stFechafin;
							MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam9);
							stFechafin = Convert.ToString(tempRefParam9);
							MatrizSql[3] = Constante;
							MatrizAux = new int[1];
                            MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { List2Servicios.Items.Count });
                            for (int i = 0; i <= List2Servicios.Items.Count - 1; i++)
							{								
                                MatrizAux[i] = Convert.ToInt32(List2Servicios.Items[i].Value);
                            }
							MatrizSql[4] = ArraysHelper.DeepCopy(MatrizAux);
							oTextoSql = new DLLTextosSql.TextosSql();							
							string tempRefParam10 = "ADMISION";
							string tempRefParam11 = "AGI412F1";
							string tempRefParam12 = "prolistado_query2";
							short tempRefParam13 = 2;
							sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam10, tempRefParam11, tempRefParam12, tempRefParam13, MatrizSql, Conexion_Base_datos.RcAdmision));
							oTextoSql = null;
						}
						return;
					}
					if (RbOtros.IsChecked)
					{
						//        Dim i As Integer
						if (LsbLista2.Items.Count > iNumero && LsbLista2.Items.Count != iNumero_Lista1)
						{
							stMensaje1 = "" + iNumero.ToString();
							stMensaje2 = "Entidades";							
							short tempRefParam15 = 1280;
                            string[] tempRefParam16 = new string[] { stMensaje1, stMensaje2 };
							Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, Conexion_Base_datos.RcAdmision, tempRefParam16));
							tIndicador = true;
							this.Cursor = Cursors.Default;
							LsbLista2.Items.Clear();
							proCarga_Lista1_Todos();
							return;
						}
						if (LsbLista2.Items.Count == iNumero_Lista1)
						{
							//SELECCIONA TODAS LAS ENTIDADES QUE NO SON SS NI PRIVADO
							stEntidad = Vst_Tipomov + "Entidades Financiadoras";
							//''            ReDim MatrizSql(2)'''            MatrizSql(1) = FormatFechaHMS(stFechaini)'''            MatrizSql(2) = FormatFechaHMS(stFechafin)
							MatrizSql = new object[4];							
							object tempRefParam17 = stFechaini;
							MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam17);
							stFechaini = Convert.ToString(tempRefParam17);
							
							object tempRefParam18 = stFechafin;
							MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam18);
							stFechafin = Convert.ToString(tempRefParam18);
							MatrizAux = new int[1];
                            MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { List2Servicios.Items.Count });
                            for (int i = 0; i <= List2Servicios.Items.Count - 1; i++)
							{								
                                MatrizAux[i] = Convert.ToInt32(List2Servicios.Items[i].Value);
                            }
							MatrizSql[3] = ArraysHelper.DeepCopy(MatrizAux);

							if (RbIngreso.CheckState == CheckState.Checked)
							{
								oTextoSql = new DLLTextosSql.TextosSql();								
								string tempRefParam19 = "ADMISION";
								string tempRefParam20 = "AGI412F1";
								string tempRefParam21 = "prolistado_query2";
								short tempRefParam22 = 3;
								sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam19, tempRefParam20, tempRefParam21, tempRefParam22, MatrizSql, Conexion_Base_datos.RcAdmision));
							}
							else
							{
								oTextoSql = new DLLTextosSql.TextosSql();								
								string tempRefParam24 = "ADMISION";
								string tempRefParam25 = "AGI412F1";
								string tempRefParam26 = "prolistado_query2";
								short tempRefParam27 = 4;
								sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam24, tempRefParam25, tempRefParam26, tempRefParam27, MatrizSql, Conexion_Base_datos.RcAdmision));
							}
							oTextoSql = null;
							return;
						}
						iNumero_Lista2 = LsbLista2.Items.Count;
						stEntidad = Vst_Tipomov + "Entidades Financiadoras";
						//''        ReDim MatrizSql(3)'''        MatrizSql(1) = FormatFechaHMS(stFechaini)'''        MatrizSql(2) = FormatFechaHMS(stFechafin)'''        ReDim MatrizAux(0)'''        For i = 0 To LsbLista2.ListCount - 1'''               ReDim Preserve MatrizAux(UBound(MatrizAux) + 1)'''               MatrizAux(UBound(MatrizAux)) = LsbLista2.ItemData(i)'''        Next'''        MatrizSql(3) = MatrizAux
						MatrizSql = new object[5];						
						object tempRefParam29 = stFechaini;
						MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam29);
						stFechaini = Convert.ToString(tempRefParam29);
                        						
						object tempRefParam30 = stFechafin;
						MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam30);
						stFechafin = Convert.ToString(tempRefParam30);
						MatrizAux = new int[1];
                        
                        MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { LsbLista2.Items.Count });
                        for (int i = 0; i <= LsbLista2.Items.Count - 1; i++)
						{                            
                            MatrizAux[i] = Convert.ToInt32(LsbLista2.Items[i].Value);
                        }						
						MatrizSql[3] = ArraysHelper.DeepCopy(MatrizAux);
						//los servicios
						MatrizAux = new int[1];
                        //dgmorenog
                        MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { List2Servicios.Items.Count});
                        for (int i = 0; i <= List2Servicios.Items.Count - 1; i++)
						{							
                            MatrizAux[i] = Convert.ToInt32(List2Servicios.Items[i].Value);
                        }
						MatrizSql[4] = ArraysHelper.DeepCopy(MatrizAux);

						oTextoSql = new DLLTextosSql.TextosSql();
						if (RbIngreso.CheckState == CheckState.Checked)
						{							
							string tempRefParam31 = "ADMISION";
							string tempRefParam32 = "AGI412F1";
							string tempRefParam33 = "prolistado_query2";
							short tempRefParam34 = 5;
							sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam31, tempRefParam32, tempRefParam33, tempRefParam34, MatrizSql, Conexion_Base_datos.RcAdmision));
						}
						else
						{
							string tempRefParam36 = "ADMISION";
							string tempRefParam37 = "AGI412F1";
							string tempRefParam38 = "prolistado_query2";
							short tempRefParam39 = 6;
							sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam36, tempRefParam37, tempRefParam38, tempRefParam39, MatrizSql, Conexion_Base_datos.RcAdmision));
						}
						oTextoSql = null;
						return;
					}
					if (RbTodos.IsChecked)
					{
						//TODAS LAS ENTIDADES MENOS SS Y PRIVADO
						//SCONSGLO AS SCONSGLOA, SCONSGLO AS SCONSGLOB, le he quitado esto
						stEntidad = Vst_Tipomov + "Entidades Financiadoras";
						//''        ReDim MatrizSql(2)'''        MatrizSql(1) = FormatFechaHMS(stFechaini)'''        MatrizSql(2) = FormatFechaHMS(stFechafin)
						MatrizSql = new object[4];
						
						object tempRefParam41 = stFechaini;
						MatrizSql[1] = Serrores.FormatFechaHMS(tempRefParam41);
						stFechaini = Convert.ToString(tempRefParam41);
						
						object tempRefParam42 = stFechafin;
						MatrizSql[2] = Serrores.FormatFechaHMS(tempRefParam42);
						stFechafin = Convert.ToString(tempRefParam42);
						MatrizAux = new int[1];
                        MatrizAux = ArraysHelper.RedimPreserve(MatrizAux, new int[] { List2Servicios.Items.Count });
                        for (int i = 0; i <= List2Servicios.Items.Count - 1; i++)
						{							
                            MatrizAux[i] = Convert.ToInt32(List2Servicios.Items[i].Value);
                        }
						MatrizSql[3] = MatrizAux;
						oTextoSql = new DLLTextosSql.TextosSql();
						if (RbIngreso.CheckState == CheckState.Checked)
						{							
							string tempRefParam43 = "ADMISION";
							string tempRefParam44 = "AGI412F1";
							string tempRefParam45 = "prolistado_query2";
							short tempRefParam46 = 7;
							sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam43, tempRefParam44, tempRefParam45, tempRefParam46, MatrizSql, Conexion_Base_datos.RcAdmision));
						}
						else
						{							
							string tempRefParam48 = "ADMISION";
							string tempRefParam49 = "AGI412F1";
							string tempRefParam50 = "prolistado_query2";
							short tempRefParam51 = 8;
							sql = Convert.ToString(oTextoSql.DevuelveTexto(Serrores.VstTipoBD, tempRefParam48, tempRefParam49, tempRefParam50, tempRefParam51, MatrizSql, Conexion_Base_datos.RcAdmision));
						}
						oTextoSql = null;
						return;
					}
					else
					{
						// No ha seleccionado nada, le mado la sql vacia
						sql = "";
						return;
					}
				}
			}
			catch (System.Exception excep)
			{				
				MessageBox.Show("ERROR: " + Information.Err().Number.ToString() + excep.Message, Application.ProductName);
				//GestionOtrosErrores Err.Number, RcAdmision
			}
		}
		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
            ingresos_por_entidades.DefInstance.Close();            
        }
        private void CbDelSerTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			List1Servicios.Items.Clear();
			List2Servicios.Items.Clear();
			proCarga_Lista_Servicios();
			CbDelSerTodo.Enabled = false;
			CbDelSerUno.Enabled = false;
			CbInsSerTodo.Enabled = true;
			CbInsSerUno.Enabled = true;
			CbImprimir.Enabled = false;
			CbPantalla.Enabled = false;

		}
		private void CbDelSerUno_Click(Object eventSender, EventArgs eventArgs)
		{           
            if (this.List2Servicios.SelectedIndex == -1)
            {
            //MsgBox ("Debe haber seleccionado alguna entidad")
            stMensaje1 = "un servicio";				
				short tempRefParam = 1270;
                string[] tempRefParam2 = new string[] { stMensaje1 };
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				return;
			}
            List1Servicios.Items.Add(List2Servicios.SelectedItem);            
            //List1Servicios.SetItemData(List1Servicios.GetNewIndex(), List2Servicios.GetItemData(ListBoxHelper.GetSelectedIndex(List2Servicios)));            
            List2Servicios.Refresh();
            List2Servicios.SelectedIndex = -1;

            if (List2Servicios.Items.Count == 0)
			{
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
			}

		}
		private void CbDelTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			LsbLista2.Items.Clear();
			LsbLista1.Items.Clear();
			proCarga_Lista1_Todos();
			CbDelTodo.Enabled = false;
			CbDelUno.Enabled = false;
			CbInsTodo.Enabled = true;
			CbInsUno.Enabled = true;
			CbImprimir.Enabled = false;
			CbPantalla.Enabled = false;

		}
		private void CbDelUno_Click(Object eventSender, EventArgs eventArgs)
		{
			
            if (LsbLista2.SelectedIndex == -1)
            {
                //MsgBox ("Debe haber seleccionado alguna entidad")
                stMensaje1 = "una Entidad Financiadora";
                short tempRefParam = 1270;
                string[] tempRefParam2 = new string[] { stMensaje1 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                return;
            }
            LsbLista1.Items.Add(LsbLista2.SelectedItem);            
            //LsbLista1.SetItemData(LsbLista1.GetNewIndex(), LsbLista2.GetItemData(ListBoxHelper.GetSelectedIndex(LsbLista2)));            
			LsbLista2.Refresh();
            LsbLista2.SelectedIndex = -1;

            if (LsbLista2.Items.Count == 0)
			{
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
			}

		}
		private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{

			if (RbIngreso.CheckState == CheckState.Checked && RbAltas.CheckState == CheckState.Checked)
			{
				//desabilitar altas
				RbAltas.CheckState = CheckState.Unchecked;
                
				PEmite_Listado(Crystal.DestinationConstants.crptToPrinter);
				//desabilitar ingresos
				RbIngreso.CheckState = CheckState.Unchecked;
                
                PEmite_Listado(Crystal.DestinationConstants.crptToPrinter);
				LsbLista1.Items.Clear();
				LsbLista2.Items.Clear();
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
				this.Cursor = Cursors.Default;
				RbSs.IsChecked = false;
				RbPrivado.IsChecked = false;
				RbApellidos.IsChecked = false;
				RbOtros.IsChecked = false;
				RbTodos.IsChecked = false;
				RbNingreso.IsChecked = false;
				RbIngreso.CheckState = CheckState.Unchecked;
				RbAltas.CheckState = CheckState.Unchecked;
				Dtfechafin.Text = DateTime.Today.ToString("dd/MM/yyyy");
				Dtfechaini.Text = DateTime.Today.ToString("dd/MM/yyyy");
				CbDelTodo.Enabled = false;
				CbDelUno.Enabled = false;
				iNumero_Lista2 = 0;
			}
			else
			{
				if (RbIngreso.CheckState == CheckState.Unchecked && this.RbAltas.CheckState == CheckState.Unchecked)
				{					
					short tempRefParam = 1270;
                    string[] tempRefParam2 = new string[] { "un tipo de movimiento" };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				}
				else
				{                    
                    PEmite_Listado(Crystal.DestinationConstants.crptToPrinter);
					LsbLista1.Items.Clear();
					LsbLista2.Items.Clear();
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
					this.Cursor = Cursors.Default;
					RbSs.IsChecked = false;
					RbPrivado.IsChecked = false;
					RbApellidos.IsChecked = false;
					RbOtros.IsChecked = false;
					RbTodos.IsChecked = false;
					RbNingreso.IsChecked = false;
					RbIngreso.CheckState = CheckState.Unchecked;
					RbAltas.CheckState = CheckState.Unchecked;
					Dtfechafin.Text = DateTime.Today.ToString("dd/MM/yyyy");
					Dtfechaini.Text = DateTime.Today.ToString("dd/MM/yyyy");
					CbDelTodo.Enabled = false;
					CbDelUno.Enabled = false;
					iNumero_Lista2 = 0;
				}
			}
			if (!FrmHospital.Visible)
			{
				ChBHosDia.CheckState = CheckState.Checked;
				ChBHosGeneral.CheckState = CheckState.Checked;
			}
			else
			{
				ChBHosDia.CheckState = CheckState.Unchecked;
				ChBHosGeneral.CheckState = CheckState.Unchecked;
			}
			CbDelSerTodo_Click(CbDelSerTodo, new EventArgs());
		}
		private void CbInsSerTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			List1Servicios.Items.Clear();
			List2Servicios.Items.Clear();
			proCarga_Lista2_Servicios();
			CbInsSerTodo.Enabled = false;
			CbInsSerUno.Enabled = false;
			CbDelSerTodo.Enabled = true;
			CbDelSerUno.Enabled = true;
			if ((((RbNingreso.IsChecked || RbApellidos.IsChecked) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
			{
				CbImprimir.Enabled = true;
				CbPantalla.Enabled = true;
			}
			iNumero_Lista2_Ser = List2Servicios.Items.Count;

		}
		private void CbInsSerUno_Click(Object eventSender, EventArgs eventArgs)
		{
            //camaya todo_x_4
            if (List1Servicios.SelectedIndex == -1)
            {
                //MsgBox("Debe haber seleccionado alguna entidad")
                stMensaje1 = "un servicio";
                short tempRefParam = 1270;
                string[] tempRefParam2 = new string[] { stMensaje1 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                return;
            }
            List2Servicios.Items.Add(List1Servicios.SelectedItem);            
            //List2Servicios.SetItemData(List2Servicios.GetNewIndex(), List1Servicios.GetItemData(ListBoxHelper.GetSelectedIndex(List1Servicios)));            
            List1Servicios.Refresh();
            List1Servicios.SelectedIndex = -1;
            CbDelSerUno.Enabled = true;
			CbDelSerTodo.Enabled = true;
			if ((((RbNingreso.IsChecked || RbApellidos.IsChecked) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
			{
				if (RbOtros.IsChecked && this.LsbLista2.Items.Count > 0)
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
				else if (!RbOtros.IsChecked)
				{ 
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
			}

		}
		private void CbInsTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			//    iNumero_Lista1 = LsbLista1.ListCount
			LsbLista1.Items.Clear();
			LsbLista2.Items.Clear();
			proCarga_Lista2_Todos();
			CbInsTodo.Enabled = false;
			CbInsUno.Enabled = false;
			CbDelTodo.Enabled = true;
			CbDelUno.Enabled = true;
			if ((((RbNingreso.IsChecked || RbApellidos.IsChecked) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
			{
				if (RbOtros.IsChecked && this.LsbLista2.Items.Count > 0)
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
				else if (!RbOtros.IsChecked)
				{ 
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
			}
			iNumero_Lista2 = LsbLista2.Items.Count;
		}
		private void CbInsUno_Click(Object eventSender, EventArgs eventArgs)
		{
			if (LsbLista1.SelectedIndex == -1)
			{
				//MsgBox ("Debe haber seleccionado alguna entidad")
				stMensaje1 = "una Entidad Financiadora";				
				short tempRefParam = 1270;
                string[] tempRefParam2 = new string[] {stMensaje1};
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				return;
			}
			if (LsbLista2.Items.Count == iNumero)
			{
				//MsgBox ("No se pueden seleccionar mas de " & iNumero & " Entidades")
				stMensaje1 = "" + iNumero.ToString();
				stMensaje2 = "Entidades";				
				short tempRefParam3 = 1280;
                string[] tempRefParam4 = new string[] { stMensaje1, stMensaje2 };
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));
				return;
			}

			LsbLista2.Items.Add(LsbLista1.SelectedItem);           
			//LsbLista2.SetItemData(LsbLista2.GetNewIndex(), LsbLista1.GetItemData(ListBoxHelper.GetSelectedIndex(LsbLista1)));			
			LsbLista1.Refresh();
			CbDelUno.Enabled = true;
			CbDelTodo.Enabled = true;
            LsbLista1.SelectedIndex = -1;
			if ((((RbNingreso.IsChecked || RbApellidos.IsChecked) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
			{
				if (RbOtros.IsChecked && LsbLista2.Items.Count > 0)
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
				else if (!RbOtros.IsChecked)
				{ 
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
			}
		}
		private void CbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
			if (RbIngreso.CheckState == CheckState.Checked && RbAltas.CheckState == CheckState.Checked)
			{
				//desabilitar altas
				RbAltas.CheckState = CheckState.Unchecked;
                
                PEmite_Listado(Crystal.DestinationConstants.crptToWindow);
				//desabilitar ingresos
				RbIngreso.CheckState = CheckState.Unchecked;
                
                PEmite_Listado(Crystal.DestinationConstants.crptToWindow);
				LsbLista1.Items.Clear();
				LsbLista2.Items.Clear();
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
				this.Cursor = Cursors.Default;
				RbSs.IsChecked = false;
				RbPrivado.IsChecked = false;
				RbApellidos.IsChecked = false;
				RbOtros.IsChecked = false;
				RbTodos.IsChecked = false;
				RbNingreso.IsChecked = false;
				RbIngreso.CheckState = CheckState.Unchecked;
				RbAltas.CheckState = CheckState.Unchecked;
				Dtfechafin.Text = DateTime.Today.ToString("dd/MM/yyyy");
				Dtfechaini.Text = DateTime.Today.ToString("dd/MM/yyyy");
				CbDelTodo.Enabled = false;
				CbDelUno.Enabled = false;
				iNumero_Lista2 = 0;
			}
			else
			{
				if (RbIngreso.CheckState == CheckState.Unchecked && this.RbAltas.CheckState == CheckState.Unchecked)
				{					
					short tempRefParam = 1270;
                    string[] tempRefParam2 = new string[] { "un tipo de movimiento" };
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
				}
				else
				{                    
                    PEmite_Listado(Crystal.DestinationConstants.crptToWindow);
					LsbLista1.Items.Clear();
					LsbLista2.Items.Clear();
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
					this.Cursor = Cursors.Default;
					RbSs.IsChecked = false;
					RbPrivado.IsChecked = false;
					RbApellidos.IsChecked = false;
					RbOtros.IsChecked = false;
					RbTodos.IsChecked = false;
					RbNingreso.IsChecked = false;
					RbIngreso.CheckState = CheckState.Unchecked;
					RbAltas.CheckState = CheckState.Unchecked;
					Dtfechafin.Text = DateTime.Today.ToString("dd/MM/yyyy");
					Dtfechaini.Text = DateTime.Today.ToString("dd/MM/yyyy");
					CbDelTodo.Enabled = false;
					CbDelUno.Enabled = false;
					iNumero_Lista2 = 0;
				}
			}
			if (!FrmHospital.Visible)
			{
				ChBHosDia.CheckState = CheckState.Checked;
				ChBHosGeneral.CheckState = CheckState.Checked;
			}
			else
			{
				ChBHosDia.CheckState = CheckState.Unchecked;
				ChBHosGeneral.CheckState = CheckState.Unchecked;
			}
			CbDelSerTodo_Click(CbDelSerTodo, new EventArgs());
		}

        public void PEmite_Listado(Crystal.DestinationConstants Ptipo)
        {
            string LitPersona = String.Empty;
            string stMensaje3 = String.Empty;

            int tiOrder = 0;
            string stTipoMov = String.Empty;
            // Realizamos las validaciones de las fechas introducidas *******************
            if (Dtfechaini.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
            {
                stMensaje2 = "menor o igual";
                stMensaje3 = "Fecha del Sistema";
                int tempRefParam = 1020;
                string[] tempRefParam2 = new string[] { Label1.Text, stMensaje2, stMensaje3 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion_Base_datos.RcAdmision, tempRefParam2));
                
                Dtfechaini.Focus();
                return;
            }
            if (Dtfechafin.Value.Date > DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
            {
                stMensaje2 = "menor o igual";
                stMensaje3 = "Fecha del Sistema";
                short tempRefParam3 = 1020;
                string[] tempRefParam4 = new string[] { Label2.Text, stMensaje2, stMensaje3 };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion_Base_datos.RcAdmision, tempRefParam4));

                Dtfechafin.Focus();
                return;
            }
            if (Dtfechafin.Value.Date < Dtfechaini.Value.Date)
            {
                stMensaje2 = "mayor o igual";
                short tempRefParam5 = 1020;
                string[] tempRefParam6 = new string[] { Label2.Text, stMensaje2, Label1.Text };
                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion_Base_datos.RcAdmision, tempRefParam6));
                Dtfechafin.Focus();
                return;
            }
            // **************************************************************************
            try
            {
                this.Cursor = Cursors.WaitCursor;
                tIndicador = false;
                prolistado_query2();
                if (tIndicador)
                {
                    return;
                }

                if (this.ChBHosDia.CheckState == CheckState.Checked && this.ChBHosGeneral.CheckState == CheckState.Unchecked)
                {
                    sql = sql + " and AEPISADM.ganoadme < 1900";
                }
                if (this.ChBHosDia.CheckState == CheckState.Unchecked && this.ChBHosGeneral.CheckState == CheckState.Checked)
                {
                    sql = sql + " and AEPISADM.ganoadme > 1900";
                }


                if (sql != "")
                {
                    //        PathString = App.Path
                    //proFormulas viform   'limpio las formulas
                    //*********cabeceras
                    if (!Conexion_Base_datos.VstrCrystal.VfCabecera)
                    {
                        Conexion_Base_datos.proCabCrystal();
                    }


                    //Se movio aqui porque debe estar antes de la asignacion de formulas
                    // Para seleccionar que informe cojo
                    if (RbSs.IsChecked || RbPrivado.IsChecked)
                    {
                        // Cuando Seleccionan SS o Privado
                        CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi412r1_CR11.rpt";
                    }
                    else if (RbTodos.IsChecked)
                    {
                        // Seleccionan todas
                        CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi412r2_CR11.rpt";
                    }
                    else if (RbOtros.IsChecked)
                    {
                        if (iNumero_Lista2 == iNumero_Lista1)
                        {
                            // Seleccionan todas en la lista
                            CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi412r2_CR11.rpt";
                        }
                        else if ((iNumero_Lista2 != 0) && (iNumero_Lista2 != iNumero_Lista1))
                        {
                            // Selecciona algunas de la lista1
                            CrystalReport1.ReportFileName = "" + Conexion_Base_datos.PathString + "\\rpt\\agi412r2_CR11.rpt";
                        }
                    }

                    //*******************
                    stIntervalo = "Desde: " + Dtfechaini.Text + "  Hasta: " + Dtfechafin.Text;
                    CrystalReport1.Formulas["{@FORM1}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstGrupoHo + "\"";
                    CrystalReport1.Formulas["{@FORM2}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstNombreHo + "\"";
                    CrystalReport1.Formulas["{@FORM3}"] = "\"" + Conexion_Base_datos.VstrCrystal.VstDireccHo + "\"";
                    CrystalReport1.Formulas["{@INTERVALO}"] = "\"" + stIntervalo + "\"";
                    CrystalReport1.Formulas["{@ENTIDAD}"] = "\"" + stEntidad + "\"";
                    CrystalReport1.Formulas["{@ORDEN}"] = "\"" + stOrden + "\"";

                    LitPersona = Serrores.ObtenerLiteralPersona(Conexion_Base_datos.RcAdmision);
                    LitPersona = LitPersona.ToUpper();

                    CrystalReport1.Formulas["{@LitPers}"] ="\"" + LitPersona + "\"";

                    //        If RbIngreso.Value = 1 Then
                    //            stTipoMov = "I"
                    //            CrystalReport1.WindowTitle = "Listado de Ingresos por Entidades"
                    //        Else
                    //            stTipoMov = "A"
                    //            CrystalReport1.WindowTitle = "Listado de Altas por Entidades"
                    //        End If
                    //        CrystalReport1.Formulas(6) = "TIPOMOVI= """ & stTipoMov & """"

                    //'        If stFORMFILI <> "V" Then
                    if (RbIngreso.CheckState == CheckState.Checked)
                    {
                        stTipoMov = "I";
                        CrystalReport1.WindowTitle = "Listado de Ingresos por Entidades";
                    }
                    else
                    {
                        stTipoMov = "A";
                        CrystalReport1.WindowTitle = "Listado de Altas por Entidades";
                    }
                    CrystalReport1.Formulas["{@TIPOMOVI}"] = "\"" + stTipoMov + "\"";
                    CrystalReport1.Formulas["{@FORM4}"] = "'LISTADO DE MOVIMIENTOS POR ENTIDADES'";
                    //'        Else
                    //'           If RbIngreso.Value = 1 Then
                    //'               stTipoMov = "I"
                    //'               CrystalReport1.WindowTitle = "Listado de Ingresos por Organizaci�n"
                    //'           Else
                    //'               stTipoMov = "A"
                    //'               CrystalReport1.WindowTitle = "Listado de Altas por Organizaci�n"
                    //'           End If
                    //'           CrystalReport1.Formulas(6) = "TIPOMOVI= """ & stTipoMov & """"
                    //'           CrystalReport1.Formulas(7) = "FORM4 ='LISTADO DE MOVIMIENTOS DEL CENTRO'"
                    //'        End If
                    CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
                   
                    //CrystalReport1.Connect = "DSN=" + Conexion_Base_datos.VstCrystal + ";UID=" + Conexion_Base_datos.gUsuario + ";PWD=" + Conexion_Base_datos.gContrase�a + "";
                    CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
                    if (RbOtros.IsChecked || RbTodos.IsChecked)
                    {
                        sql = sql + "\r" + "\n" + " order by dsocieda.dsocieda";
                        //CrystalReport1.SortFields(0) = "+{dsocieda.dsocieda}"
                        tiOrder = 1;
                    }
                    else
                    {
                        tiOrder = 0;
                    }
                    if (!RbApellidos.IsChecked)
                    {
                        if (tiOrder == 0)
                        {
                            sql = sql + "\r" + "\n" + " order by ";
                        }
                        else
                        {
                            sql = sql + ",";
                        }
                        sql = sql + "aepisadm.ganoadme,aepisadm.gnumadme";
                        //            CrystalReport1.SortFields("" & tiOrder & "") = "+{AEPISADM.ganoadme}"
                        //            CrystalReport1.SortFields("" & tiOrder + 1 & "") = "+{AEPISADM.gnumadme}"

                    }
                    else
                    {
                        if (tiOrder == 0)
                        {
                            sql = sql + "\r" + "\n" + " order by ";
                        }
                        else
                        {
                            sql = sql + ",";
                        }
                        sql = sql + "DPACIENT.dape1pac,DPACIENT.dape2pac,DPACIENT.dnombpac";
                        //            CrystalReport1.SortFields("" & tiOrder & "") = "+{DPACIENT.dape1pac}"
                        //            CrystalReport1.SortFields("" & tiOrder + 1 & "") = "+{DPACIENT.dape2pac}"
                        //            CrystalReport1.SortFields("" & tiOrder + 2 & "") = "+{DPACIENT.dnombpac}"
                    }
                    if (RbOtros.IsChecked || RbTodos.IsChecked)
                    {
                        if ((LsbLista2.Items.Count > 1) || (RbTodos.IsChecked))
                        {
                            paginacion(); //para poder paginar el infoerme por Entidad
                        }
                    }
                    CrystalReport1.SQLQuery = sql;
                    CrystalReport1.Destination = Ptipo;
                    CrystalReport1.WindowShowPrintSetupBtn = true;
                    if (CrystalReport1.Destination == Crystal.DestinationConstants.crptToPrinter)
                    {
                        Conexion_Base_datos.proDialogo_impre(CrystalReport1);
                    }
                    CrystalReport1.Action = 1;
                    CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
                    viform = 7;                    
                    Conexion_Base_datos.vacia_formulas(CrystalReport1, viform);
                }
                else
                {
                    stMensaje1 = "una Entidad";
                    short tempRefParam7 = 1270;
                    string[] tempRefParam8 = new string[] { stMensaje1 };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, Conexion_Base_datos.RcAdmision, tempRefParam8));
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            catch (Exception e)
            {
                this.Cursor = Cursors.Default;
                CbImprimir.Enabled = false;
                CbPantalla.Enabled = false;
                RbNingreso.IsChecked = false;
                RbApellidos.IsChecked = false;
                if (Information.Err().Number == 32755)
                {
                    //CANCELADA IMPRESION
                }
                else
                {
                    //Seg�n constante global que de un mensaje de error m�s descriptivo o uno gen�rico
                    if (Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "NMOVENTI", "VALFANU1") == "S")
                    {
                        RadMessageBox.Show(e.Message, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                    else
                    {
                        short tempRefParam9 = 1100;
                        string[] tempRefParam10 = new string[] { "Informe" };
                        Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, Conexion_Base_datos.RcAdmision, tempRefParam10));
                    }
                }
            }
        }

        private void ChBHosDia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaaccion();
		}
		private void ChBHosGeneral_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaaccion();
		}		
		private void ingresos_por_entidades_Load(Object eventSender, EventArgs eventArgs)
		{

			bool MostrarHospitales = false;

			string stsqltemp = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='VERCAMAS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
			DataSet rrtemp = new DataSet();
			tempAdapter.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{				
				if (Convert.ToString(rrtemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
				{
					MostrarHospitales = true;
				}
			}			
			rrtemp.Close();

			if (!MostrarHospitales)
			{
				//    Me.Height = 5265
				//    Me.Width = 8610
				//    Frame3.Top = 780
				//    Frame3.Height = 2655
				//    Frame1.Height = 3570
				FrmHospital.Visible = false;
				//    CbImprimir.Top = 4305
				//    CbPantalla.Top = 4305
				//    Me.CbCerrar.Top = 4305
				this.ChBHosDia.CheckState = CheckState.Checked;
				this.ChBHosGeneral.CheckState = CheckState.Checked;
			}

			//cambiar los literales en el tipo de ingreso
			stsqltemp = "SELECT VALFANU1,VALFANU2 FROM SCONSGLO WHERE GCONSGLO='DETIPING'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, Conexion_Base_datos.RcAdmision);
			rrtemp = new DataSet();
			tempAdapter_2.Fill(rrtemp);
			if (rrtemp.Tables[0].Rows.Count != 0)
			{				
				ChBHosGeneral.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu1"]).Trim();				
				ChBHosDia.Text = Convert.ToString(rrtemp.Tables[0].Rows[0]["valfanu2"]).Trim();
			}			
			rrtemp.Close();


			//'    If stFORMFILI <> "V" Then
			ingresos_por_entidades.DefInstance.Text = "Movimientos por entidades - AGI412F1";
            //'    Else
            //'        ingresos_por_entidades.Caption = "Movimientos del centro - AGI412F1"
            //'        RbSs.Enabled = False
            //'        RbOtros.Caption = "Sociedad"
            //'        RbPrivado.Left = 600
            //'        RbOtros.Left = 3500
            //'    End If
            
            CrystalReport1 = Conexion_Base_datos.LISTADO_CRYSTAL;
            //Me.Height = 5190
            //Me.Width = 7995
            
            CrystalReport1.Connect = Conexion_Base_datos.VstCrystal + ";" + Conexion_Base_datos.gUsuario + ";" + Conexion_Base_datos.gContrase�a + ";";
			iNumero = Convert.ToInt32(Double.Parse(Serrores.ObternerValor_CTEGLOBAL(Conexion_Base_datos.RcAdmision, "NMOVENTI", "NNUMERI1")));
            RbNingreso.IsChecked = true;
            CbImprimir.Enabled = false;
			CbPantalla.Enabled = false;
			CbInsTodo.Enabled = false;
			CbInsUno.Enabled = false;
			CbDelTodo.Enabled = false;
			CbDelUno.Enabled = false;
			proSegu_priva();
			proCarga_Lista_Servicios();
		}
		public void proSegu_priva()
		{
			string sqlcon = String.Empty;
			DataSet rrcon = null;
			try
			{
				sqlcon = "select * from sconsglo where gconsglo='PRIVADO' or gconsglo='SEGURSOC'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcon, Conexion_Base_datos.RcAdmision);
				rrcon = new DataSet();
				tempAdapter.Fill(rrcon);
				StPrivado = 0;
				StSeguSoc = 0;
				if (rrcon.Tables[0].Rows.Count != 0)
				{
					rrcon.MoveFirst();
					foreach (DataRow iteration_row in rrcon.Tables[0].Rows)
					{
						if (Convert.ToString(iteration_row["gconsglo"]).Trim() == "PRIVADO")
						{
							StPrivado = Convert.ToInt32(iteration_row["nnumeri1"]);
						}
						else
						{
							StSeguSoc = Convert.ToInt32(iteration_row["nnumeri1"]);
						}
					}
				}				
				rrcon.Close();
			}
			catch
			{
				StPrivado = 0;
				StSeguSoc = 0;
				return;
			}
		}
		private void List1Servicios_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsSerUno_Click(CbInsSerUno, new EventArgs());
		}
		private void List2Servicios_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelSerUno_Click(CbDelSerUno, new EventArgs());
		}
		private void LsbLista1_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsUno_Click(CbInsUno, new EventArgs());
		}
		private void LsbLista2_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelUno_Click(CbDelUno, new EventArgs());
		}
		private void RbAltas_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaccion2();
		}

		private bool isInitializingComponent;
		private void RbApellidos_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
				if (isInitializingComponent)
				{
					return;
				}
				proActivaaccion();
			}
		}
		public void proActivaccion2()
		{
			if ((RbSs.IsChecked || RbPrivado.IsChecked || RbTodos.IsChecked || RbOtros.IsChecked) && (RbNingreso.IsChecked || RbApellidos.IsChecked) && List2Servicios.Items.Count > 0)
			{
				if (RbOtros.IsChecked)
				{
					if (LsbLista2.Items.Count == 0)
					{
						CbImprimir.Enabled = false;
						CbPantalla.Enabled = false;
					}
				}
				else
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
			}
			else
			{
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
			}
			RbSs.Enabled = true;
			RbPrivado.Enabled = true;
			RbOtros.Enabled = true;
			RbTodos.Enabled = true;
			Dtfechaini.Enabled = true;
			Dtfechafin.Enabled = true;

		}
		private void RbIngreso_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaccion2();
		}
		private void RbNingreso_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
				if (isInitializingComponent)
				{
					return;
				}
				proActivaaccion();

			}
		}
		public void proActivaaccion()
		{
			if (((((RbSs.IsChecked || RbPrivado.IsChecked || RbTodos.IsChecked || RbOtros.IsChecked) && (RbAltas.CheckState == CheckState.Checked || RbIngreso.CheckState == CheckState.Checked)) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
			{
				if (RbOtros.IsChecked)
				{
					if (LsbLista2.Items.Count == 0)
					{
						CbImprimir.Enabled = false;
						CbPantalla.Enabled = false;
					}
					else
					{
						CbImprimir.Enabled = true;
						CbPantalla.Enabled = true;
					}
				}
				else
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
			}
			else
			{
				CbImprimir.Enabled = false;
				CbPantalla.Enabled = false;
			}
			RbSs.Enabled = true;
			RbPrivado.Enabled = true;
			RbOtros.Enabled = true;
			RbTodos.Enabled = true;
			Dtfechaini.Enabled = true;
			Dtfechafin.Enabled = true;

		}
		private void RbOtros_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
				if (isInitializingComponent)
				{
					return;
				}
				LsbLista2.Enabled = true;
				LsbLista1.Enabled = true;
				CbInsUno.Enabled = true;
				CbInsTodo.Enabled = true;
				if (((((RbApellidos.IsChecked || RbNingreso.IsChecked) && (RbIngreso.CheckState == CheckState.Checked || RbAltas.CheckState == CheckState.Checked)) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
				{
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
				}
				else
				{
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
				}
				LsbLista2.Items.Clear();
				LsbLista1.Items.Clear();
				proCarga_Lista1_Todos();
				iNumero_Lista1 = LsbLista1.Items.Count;
				CbInsTodo.Enabled = true;
				CbInsUno.Enabled = true;
				CbDelTodo.Enabled = false;
				CbDelUno.Enabled = false;
			}
		}
		private void RbPrivado_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
				if (isInitializingComponent)
				{
					return;
				}
				if (((((RbApellidos.IsChecked || RbNingreso.IsChecked) && (RbIngreso.CheckState == CheckState.Checked || RbAltas.CheckState == CheckState.Checked)) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
				else
				{
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
				}
				LsbLista2.Items.Clear();
				CbInsTodo.Enabled = false;
				CbInsUno.Enabled = false;
				CbDelTodo.Enabled = false;
				CbDelUno.Enabled = false;
				LsbLista1.Items.Clear();

			}
		}
		private void RbSs_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
				if (isInitializingComponent)
				{
					return;
				}
				if (((((RbApellidos.IsChecked || RbNingreso.IsChecked) && (RbIngreso.CheckState == CheckState.Checked || RbAltas.CheckState == CheckState.Checked)) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
				else
				{
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
				}
				LsbLista2.Items.Clear();
				CbInsTodo.Enabled = false;
				CbInsUno.Enabled = false;
				CbDelTodo.Enabled = false;
				CbDelUno.Enabled = false;
				LsbLista1.Items.Clear();

			}
		}
		private void RbTodos_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
				if (isInitializingComponent)
				{
					return;
				}
				if (((((RbApellidos.IsChecked || RbNingreso.IsChecked) && (RbIngreso.CheckState == CheckState.Checked || RbAltas.CheckState == CheckState.Checked)) ? -1 : 0) & (((int) ChBHosDia.CheckState) | ((int) ChBHosGeneral.CheckState)) & ((List2Servicios.Items.Count > 0) ? -1 : 0)) != 0)
				{
					CbImprimir.Enabled = true;
					CbPantalla.Enabled = true;
				}
				else
				{
					CbImprimir.Enabled = false;
					CbPantalla.Enabled = false;
				}
				LsbLista2.Items.Clear();
				LsbLista1.Items.Clear();
				CbDelUno.Enabled = false;
				CbDelTodo.Enabled = false;
				LsbLista2.Enabled = false;
				LsbLista1.Enabled = false;
				CbInsUno.Enabled = false;
				CbInsTodo.Enabled = false;
			}
		}
		public void proCarga_Lista2_Todos()
		{
			int i = 0;
			string sqlLista = String.Empty;
			try
			{

				sqlLista = " SELECT * FROM DSOCIEDA";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlLista, Conexion_Base_datos.RcAdmision);
				Rr1 = new DataSet();
				tempAdapter.Fill(Rr1);
				foreach (DataRow iteration_row in Rr1.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["dsocieda"]) != "" && Convert.ToDouble(iteration_row["gsocieda"]) != StPrivado && Convert.ToDouble(iteration_row["gsocieda"]) != StSeguSoc)
					{
						LsbLista2.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dsocieda"]).ToUpper(), Convert.ToInt32(iteration_row["gsocieda"])));
						Carga_Enti = ArraysHelper.RedimPreserve(Carga_Enti, new int[]{i + 1});
						Carga_Enti[i] = Convert.ToString(iteration_row["dsocieda"]).Trim();                        
                        //LsbLista2.SetItemData(LsbLista2.GetNewIndex(), Convert.ToInt32(iteration_row["gsocieda"]));
                        i++;
					}
				}                				
				Rr1.Close();
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, Conexion_Base_datos.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		private void RbTodos_Leave(Object eventSender, EventArgs eventArgs)
		{
			LsbLista2.Items.Clear();
		}
		public void proFormulas(int idForm)
		{
            int tiForm = 0;
            foreach (var key in CrystalReport1.Formulas.Keys)
            {
                if (tiForm <= idForm)
                    CrystalReport1.Formulas[key] = "\"" + "" + "\"";

                ++tiForm;
            }


        }
        private void paginacion()
		{
			string pagina = String.Empty;
			string texto = "Entidad";

			DialogResult iRespuesta = RadMessageBox.Show("Desea que muestre una p�gina por cada " + texto, Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button1);
			if (iRespuesta == System.Windows.Forms.DialogResult.Yes)
			{
				pagina = "s";
			}
			if (pagina == "s")
			{                
                CrystalReport1.SectionFormat[0] = "GH1;X;T;X;X;X;X;X;X";
			}
			else
			{                
                CrystalReport1.SectionFormat[0] = "GH1;X;F;X;X;X;X;X;X";
			}
		}
		private void ingresos_por_entidades_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}