using Microsoft.VisualBasic;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using System.Management;

namespace Principal
{
	public partial class IMDH00F2
         : Telerik.WinControls.UI.RadForm
	{
		//Creamos un array de estructura para guardar el manejador de cada m�dulo
		//y su nombre
		private struct Modulo
		{


			public int lIdentificador;
			public int lManejador;
			public string StModulo;
			public string StTituloAplicacion;
			public static Modulo CreateInstance()
			{
					Modulo result = new Modulo();
					result.StModulo = String.Empty;
					result.StTituloAplicacion = String.Empty;
					return result;
			}
		}

		IMDH00F2.Modulo[] aModulo = null;
		int Posx = 0;
		int Posy = 0;
		System.DateTime Tiempo = DateTime.FromOADate(0);
		int resaltado = 0;
		int noresaltado = 0;
		string[] etiquetas = null;
		bool NuevaImagen = false;
		string vstUsuarioActual = String.Empty;


		Mensajeria.MCMensajeria objGestMen = null;
		bool bMensajeriaPrivada = false;
		public IMDH00F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
            //This call is required by the Windows Form Designer.
            isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
            ReLoadForm(false);
            #region menu
            mbPrincipal.FormularioMDI.MainMenu1.SuspendLayout();
            if (mbPrincipal.FormularioMDI.MainMenu1.Items.Count > 0)
            {
                mbPrincipal.FormularioMDI.Controls.RemoveByKey(mbPrincipal.FormularioMDI.MainMenu1.Name);
                mbPrincipal.FormularioMDI.MainMenu1.Items.Clear();
                mbPrincipal.FormularioMDI.Controls.Add(mbPrincipal.FormularioMDI.MainMenu1);
            }

            mbPrincipal.FormularioMDI.MainMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuArchivo,
            //this.mnuEdicion,
            //this.mnuVer,
            this.mnuOpciones,
            this.mnuSesion,
            this.mnuVentana,
            this.mnu_otrasopciones,
            this.mnuAyuda
            });

            mbPrincipal.FormularioMDI.MainMenu1.ResumeLayout(false);
            mbPrincipal.FormularioMDI.MainMenu1.PerformLayout();
            #endregion

            #region StatusBar
            if (mbPrincipal.FormularioMDI.Controls.OfType<Control>().Any(x => x.Name == this.stBar.Name))
                mbPrincipal.FormularioMDI.Controls.RemoveByKey(this.stBar.Name);
            mbPrincipal.FormularioMDI.Controls.Add(this.stBar);
            #endregion
        }



		private void CargaVentanas()
		{
            mnuVentana.Items.Clear();
            mnuTemporal[0].Text = "&Organizar Iconos";
            mnuTemporal[0].Visibility = (!NuevaImagen)
                    ? Telerik.WinControls.ElementVisibility.Visible
                    : Telerik.WinControls.ElementVisibility.Collapsed;
			mnuTemporal[2].Text = "&1 " + this.Text;
            if (mnuTemporal[2].GetType() != typeof(Telerik.WinControls.UI.RadMenuSeparatorItem))
            {
                ((Telerik.WinControls.UI.RadMenuItem)mnuTemporal[2]).IsChecked = true;
            }

			for (int X = 3; X <= mnuTemporal.Length - 1; X++)
			{
                ControlArrayHelper.UnloadControl(this, "mnuTemporal", X);
            }
			int Y = 3;
			for (int X = 0; X <= aModulo.GetUpperBound(0); X++)
			{
				if (EstaActivo(aModulo[X].lManejador))
				{
					ControlArrayHelper.LoadControl(this, "mnuTemporal", Y);
					mnuTemporal[Y].Text = "&" + Conversion.Str(Y - 1).Trim() + " " + aModulo[X].StModulo;
                    mnuTemporal[Y].Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    mnuTemporal[Y].Click += mnuTemporal_Click;
                    ((Telerik.WinControls.UI.RadMenuItem)mnuTemporal[Y]).IsChecked = false;
                    Y++;
				}
			}
            mnuVentana.Items.AddRange(new RadItemCollection(mnuTemporal.Where(x => x != null).ToArray()));
		}
		private void CargarIconos()
		{
			//Mro si cada ejecutable tiene su icono asociado,
			//y si no lo tiene, le pongo el que tiene por defecto
			int IPosi = 0;
			string StEjecutableSinEXE = String.Empty;

			if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Iconos\\Principal.ico", FileAttribute.Normal) != "")
			{
				mbPrincipal.FormularioMDI.Icon = new Icon(Path.GetDirectoryName(Application.ExecutablePath) + "\\Iconos\\Principal.ico");
			}




			if (!NuevaImagen)
			{
				if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.bmp", FileAttribute.Normal) != "")
				{
					mbPrincipal.ControlPantalla.BackgroundImage = Image.FromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.bmp"); //Fondo
                    mbPrincipal.ControlPantalla.BackgroundImageLayout = ImageLayout.Stretch;
                }
			}
			else
			{
                if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal4.bmp", FileAttribute.Normal) != "")
				{
                    mbPrincipal.ControlPantalla.BackgroundImage = Image.FromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal4.bmp"); //Fondo
                    mbPrincipal.ControlPantalla.BackgroundImageLayout = ImageLayout.None;    
                }
			}


			string StComando = "Select VEJECUTABLE,NORDENMODULO from SMODULOS " + "ORDER BY NORDENMODULO ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, mbPrincipal.RcPrincipal);
			DataSet RsIconos = new DataSet();
			tempAdapter.Fill(RsIconos);

			foreach (DataRow iteration_row in RsIconos.Tables[0].Rows)
			{
				IPosi = (Convert.ToString(iteration_row["VEJECUTABLE"]).IndexOf(".exe", StringComparison.CurrentCultureIgnoreCase) + 1);

				if (IPosi != 0)
				{
					StEjecutableSinEXE = Convert.ToString(iteration_row["VEJECUTABLE"]).Substring(0, Math.Min(IPosi - 1, Convert.ToString(iteration_row["VEJECUTABLE"]).Length));
				}
				else
				{
					StEjecutableSinEXE = Convert.ToString(iteration_row["VEJECUTABLE"]);
				}
				if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Iconos\\" + StEjecutableSinEXE + ".ico", FileAttribute.Normal) != "")
				{
					imgIconos[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Image = Image.FromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Iconos\\" + StEjecutableSinEXE + ".ico");
				}
				else
				{
					imgIconos[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Image = imgIMDH.Image;
				}
			}
			RsIconos.Close();
		}

        /// <summary>
        /// INDRA - ralopezn - 13/03/2016
        /// Este m�todo permite consultar los diferntes modulos que tiene el aplicativo para
        /// cargarlos en el men� Opciones.
        /// </summary>
		private void CargarMenuModulos()
		{
            mnuOpciones.Items.Clear();

            string StComando = "Select DMODULO,VLETRAMODULO,NORDENMODULO from SMODULOS " + "order by NORDENMODULO";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, mbPrincipal.RcPrincipal);
            DataSet RsMenu = new DataSet();
            tempAdapter.Fill(RsMenu);
            foreach (DataRow iteration_row in RsMenu.Tables[0].Rows)
            {
                if (Convert.ToDouble(iteration_row["NORDENMODULO"]) != 1)
                {
                    ControlArrayHelper.LoadControl(this, "mnuOpcion", Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1));
                    ControlArrayHelper.LoadControl(this, "imgIconos", Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1));
                }
                mnuOpcion[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Text = PonerUnpersand(Convert.ToString(iteration_row["DMODULO"]).Trim(), Convert.ToString(iteration_row["VLETRAMODULO"]).Trim());
                mnuOpcion[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Name = "_mnuOpcion_" + Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1);
                mnuOpcion[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Click += mnuOpcion_Click;

                ToolTipMain.SetToolTip(imgIconos[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)], Convert.ToString(iteration_row["DMODULO"]).Trim());
                imgIconos[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Visible = true;
                imgIconos[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Name = "_imgIconos_" + Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1);
                LbEtiqueta[Convert.ToInt32(Convert.ToDouble(iteration_row["NORDENMODULO"]) - 1)].Text = Convert.ToString(iteration_row["DMODULO"]).Trim();
            }
            RsMenu.Close();
            mnuOpciones.Items.AddRange(new RadItemCollection(mnuOpcion.Where(x => x != null).ToArray()));
        }

		private void Ejecutar(int IIndice)
		{

			string StEjecutableSinEXE = String.Empty;
			int I = 0;
			bool fexiste = false;
			int vIden = 0;
			int vman = 0;
			//Busco en la tabla de m�dulos el nombre del ejecutable
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
					Environment.Exit(0);
				}
			}
			string StComando = "Select GMODULO, VEJECUTABLE from SMODULOS " + "where DMODULO=" + "'" + ToolTipMain.GetToolTip(imgIconos[IIndice]).Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, mbPrincipal.RcPrincipal);
			mbPrincipal.RsPrincipal = new DataSet();
			tempAdapter.Fill(mbPrincipal.RsPrincipal);

			string StEjecutable = Convert.ToString(mbPrincipal.RsPrincipal.Tables[0].Rows[0]["VEJECUTABLE"]).Trim();
			string strModulo = Convert.ToString(mbPrincipal.RsPrincipal.Tables[0].Rows[0]["GMODULO"]).Trim();
			mbPrincipal.RsPrincipal.Close();


			if (StEjecutable == "ServiciosMedicos.exe")
			{
				StEjecutable = "EstacionMedica.exe";
				strModulo = "MEDICOS";
			}

			if (StEjecutable == "")
			{
				return;
			}

			//Busco la posici�n en la que se encuentra el ".exe" para crearme dos
			//variables: ejecutable con .exe y sin .exe
			int IPosi = (StEjecutable.IndexOf(".exe", StringComparison.CurrentCultureIgnoreCase) + 1);

			if (IPosi != 0)
			{
				StEjecutableSinEXE = StEjecutable.Substring(0, Math.Min(IPosi - 1, StEjecutable.Length));
			}
			else
			{
				StEjecutableSinEXE = StEjecutable;
			}

            StEjecutable = "NET_" + StEjecutable;

            //Busco el ejecutable y si no est� saco un mensaje de error
            if (FileSystem.Dir(mbPrincipal.vPathAplicacion + StEjecutableSinEXE + "\\" + StEjecutable, FileAttribute.Normal) != "")
			{

				//Miro si ya se ha ejecutado anteriormente el m�dulo para poder sacar
				//su manejador y saber si se est� ejecutando en este momento
				if (aModulo.GetUpperBound(0) != 0)
				{
					for (I = 0; I <= aModulo.GetUpperBound(0) - 1; I++)
					{
						if (aModulo[I].StModulo.Trim() == ToolTipMain.GetToolTip(imgIconos[IIndice]).Trim())
						{
							fexiste = true;
							break;
						}
					}
				}

				//Si ya se ha ejecutado anteriormente
				if (fexiste)
				{
					//Miro si se est� ejecutando en este momento el m�dulo

					//Si no est� activo, lo ejecuto y grabo el nuevo manejador encima
					//del que ten�a anteriormente
					if (!EstaActivo(aModulo[I].lManejador))
					{

						//Oscar: Incidencias de acceso
						S01000F1.DefInstance.GrabaIncidencia(mbPrincipal.vstUsuario, strModulo, 0);
						S01000F1.DefInstance.GrabaAcceso(mbPrincipal.vstUsuario);


                        //vIden = Shell(vPathAplicacion & StEjecutableSinEXE & "\" & StEjecutable & " " & vstUsuario & ",Principal@", 1)
                        ProcessStartInfo startInfo = new ProcessStartInfo(mbPrincipal.vPathAplicacion + StEjecutableSinEXE + "\\" + StEjecutable,
                                                  mbPrincipal.vstUsuario + ",Principal@*" + mbAcceso.gstServerActual + "#" +
                                                  mbAcceso.gstDriver + "#" + mbAcceso.gstBasedatosActual + "#" + mbAcceso.gstUsuarioBD + "#" +
                                                  mbAcceso.gstPasswordBD);
                        startInfo.WindowStyle = ProcessWindowStyle.Normal;
						vIden = Convert.ToInt32(Process.Start(startInfo).Id);
                        //-----

                        vman = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.OpenProcess(mbPrincipal.SYNCHRONIZE, 0, vIden);
                        if (vman != 0)
						{
							aModulo[I].lIdentificador = vIden;
							aModulo[I].lManejador = vman;
							aModulo[I].StModulo = ToolTipMain.GetToolTip(imgIconos[IIndice]).Trim();
                            try
                            {
                                Interaction.AppActivate(aModulo[I].lIdentificador);
                            } catch (ArgumentException aex) { }

                        }
						//Si est� activo, saco un mensaje de error
					}
					else
					{
						//Ya existe una copia activo de "MODULO"
						short tempRefParam = 2030;
						string[] tempRefParam2 = new string[]{aModulo[I].StModulo};
						mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
					}

					//Si no se ha ejecutado anteriormente, lo ejecuto y grabo
					//el modulo con su manejador
				}
				else
				{

					//Oscar: Incidencias de acceso
					S01000F1.DefInstance.GrabaIncidencia(mbPrincipal.vstUsuario, strModulo, 0);
					S01000F1.DefInstance.GrabaAcceso(mbPrincipal.vstUsuario);

					//vIden = Shell(vPathAplicacion & StEjecutableSinEXE & "\" & StEjecutable & " " & vstUsuario & ",Principal@", 1)
					ProcessStartInfo startInfo2 = new ProcessStartInfo(mbPrincipal.vPathAplicacion + StEjecutableSinEXE + "\\" + StEjecutable, 
					                              mbPrincipal.vstUsuario + ",Principal@*" + mbAcceso.gstServerActual + "#" + 
					                              mbAcceso.gstDriver + "#" + mbAcceso.gstBasedatosActual + "#" + mbAcceso.gstUsuarioBD + "#" + 
					                              mbAcceso.gstPasswordBD);
					startInfo2.WindowStyle = ProcessWindowStyle.Normal;
                    vIden = Convert.ToInt32(Process.Start(startInfo2).Id);
                    //-----


                    vman = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.OpenProcess(mbPrincipal.SYNCHRONIZE, 0, vIden);
                    if (vman != 0)
					{
						aModulo = ArraysHelper.RedimPreserve(aModulo, new int[]{aModulo.GetUpperBound(0) + 2});
						aModulo[aModulo.GetUpperBound(0) - 1].lIdentificador = vIden;
						aModulo[aModulo.GetUpperBound(0) - 1].lManejador = vman;
						aModulo[aModulo.GetUpperBound(0) - 1].StModulo = ToolTipMain.GetToolTip(imgIconos[IIndice]).Trim();
                        try
                        {
                            Interaction.AppActivate(aModulo[aModulo.GetUpperBound(0) - 1].lIdentificador);
                        } catch (ArgumentException aex) { }
					}
				}
			}
			else
			{
				//Oscar: Incidencias de acceso
				S01000F1.DefInstance.GrabaIncidencia(mbPrincipal.vstUsuario, strModulo, 4);
				//---------------------------

				//"MODULO" no disponible actualmente
				short tempRefParam3 = 1760;
				string[] tempRefParam4 = new string[]{ToolTipMain.GetToolTip(imgIconos[IIndice])};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
			}
			mbPrincipal.CERRARCONEXION();
		}

		private bool EstaActivo(int lManejador)
		{
            int lResul = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.WaitForSingleObject(lManejador, 0);
			if (lResul != mbPrincipal.wait_timeout)
			{
				return false;
			}
			else
			{
				return true;
			}
		}


		private string PonerUnpersand(string StModulo, string stLetra)
		{
			int IPosi1 = (StModulo.IndexOf(stLetra, StringComparison.CurrentCultureIgnoreCase) + 1);
			if (IPosi1 != 0)
			{
				return (StModulo.Substring(0, Math.Min(IPosi1 - 1, StModulo.Length)) + "&" + StModulo.Substring(IPosi1 - 1)).Trim();
			}
			else
			{
				return StModulo;
			}
		}

		//UPGRADE_NOTE: (7001) The following declaration (QuitarUnpersand) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string QuitarUnpersand(string stTexto)
		//{
				//int IPosi1 = (stTexto.IndexOf('&') + 1);
				//if (IPosi1 != 0)
				//{
					//return (stTexto.Substring(0, Math.Min(IPosi1 - 1, stTexto.Length)) + stTexto.Substring(IPosi1)).Trim();
				//}
				//else
				//{
					//return stTexto;
				//}
				//
		//}






		private void SinResalte()
		{
			this.Cursor = Cursors.Default;
			int tempForVar = LbEtiqueta.GetUpperBound(0);
			for (int X = 0; X <= tempForVar; X++)
			{
				LbEtiqueta[X].ForeColor = ColorTranslator.FromOle(noresaltado);
			}
		}

		private void IMDH00F2_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				// no comentar

				CargaVentanas();
				Tiempo = DateTime.Now;
				resaltado = 0xFF80FF;
				noresaltado = 0xC00000;
				//Me.WindowState = vbMaximized
				//stBar.Align = 2
				stBar.Items[0].AutoSize = true;
				stBar.Items[1].AutoSize = true;
				stBar.Items[2].AutoSize = true;
				stBar.Items[3].AutoSize = true;
				stBar.Refresh();

				//'''Oscar: redireccionamiento de BD.
				//''Dim RR As rdoResultset
				//''Dim sql As String
				//''Dim x As Integer
				//''If PrimerAcceso = True Then
				//''    sql = "SELECT SERVIDOR,BASEDEDATOS FROM SUSUBASE WHERE gusuario='" & vstUsuario & "'"
				//''    Set RR = RcPrincipal.OpenResultset(sql, 1, 1)
				//''    If Not RR.EOF Then
				//''        Me.Visible = False
				//''        If RR("SERVIDOR") <> "" And RR("BASEDEDATOS") <> "" Then
				//''            Load S03000F1
				//''            S03000F1.Hide
				//''            DoEvents
				//''            For x = 1 To UBound(Tabla_BD, 2)
				//''                'Debug.Print UCase(Tabla_BD(1, x)) & " " & UCase(Tabla_BD(2, x))
				//''                If UCase(Tabla_BD(1, x)) = UCase(RR("BASEDEDATOS")) And UCase(Tabla_BD(2, x)) = UCase(RR("SERVIDOR")) Then
				//''                    S03000F1.CmbBD.ListIndex = x - 1
				//''                    Call S03000F1.cbAceptar_Click
				//''                    Exit For
				//''                End If
				//''            Next
				//''        Else
				//''            Me.Visible = True
				//''        End If
				//''    Else
				//''        Me.Visible = True
				//''    End If
				//''    RR.Close
				//''    PrimerAcceso = False
				//''End If
				//'''------------------------

				if (bMensajeriaPrivada)
                {
                    objGestMen = new Mensajeria.MCMensajeria();
                    objGestMen.proLoadMensajeria(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD, mbPrincipal.vstUsuario);
                    objGestMen.proCompruebaMensajeria();
				}

			}
		}

       private void IMDH00F2_Load(Object eventSender, EventArgs eventArgs)
		{
			bool Actualizacion = false;
			int Respuesta = 0;
			string sql = String.Empty;
			DataSet cursor = null;
			string sSave = String.Empty;
			int ret = 0;
			string stUnidadSistema = String.Empty;
			string paraTexto = String.Empty;
			string TEXTEJECUTABLES = String.Empty;
			this.Left = (int) 0;
			this.Top = (int) 0;
           
            try
			    {         
				    sSave = new String(' ', 255);
				    //Get the system directory
				    ret = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetSystemDirectory(ref sSave, 255);
				    //Remove all unnecessary chr$(0)'s
				    sSave = sSave.Substring(0, Math.Min(ret, sSave.Length));
				    stUnidadSistema = sSave.Substring(0, Math.Min(sSave.IndexOf(':'), sSave.Length));

				    Serrores.CrearCadenaDSN(ref mbPrincipal.DSN_SQLserver, ref mbPrincipal.dsn_sqlaccess);

				    vstUsuarioActual = UsuarioActual();

				    sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='NUEVAIMG'";
				    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
				    cursor = new DataSet();
				    tempAdapter.Fill(cursor);
				    if (cursor.Tables[0].Rows.Count == 0)
				    {
					    NuevaImagen = false;
				    }
				    else
				    {
					    NuevaImagen = Convert.ToString(cursor.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S";
				    }
				    if (NuevaImagen)
				    {
					    Label2.Visible = true;
					    Label1.Visible = true;
				    }
				    //verificar que no necesita una actualizacion
				    if (mbPrincipal.NecesitaActualizar() && mbPrincipal.ObligaAactualizar)
				    {

					    ProcessStartInfo startInfo = new ProcessStartInfo("\\\\" + mbPrincipal.Servidor + "\\" + mbPrincipal.Directorio + "\\ActualizacionAutomatica.exe");
					    startInfo.WindowStyle = ProcessWindowStyle.Normal;
					    Respuesta = Convert.ToInt32(Process.Start(startInfo).Id);

					    //FINALIZAR PRINCIPAL
					    mbPrincipal.SalirActualizacion = true;
					    if (Respuesta == 0)
					    {
						    RadMessageBox.Show("Se ha producido un error al intentar Actualizar I-MDH a la nueva versi�n " + "\n" + "\r" + "Pongase en contacto con inform�tica", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
						    string tempRefParam = ("No se ha podido ejecutar el fichero ActualizacionAutomatica.exe en el servidor");
						    mbPrincipal.Grabarerror(ref tempRefParam);
					    }
					    mnuSalirItem_Click(mnuSalirItem, new EventArgs());
				    }


				    sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='IUSEREXI'";
				    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
				    cursor = new DataSet();
				    tempAdapter_2.Fill(cursor);
				    if (cursor.Tables[0].Rows.Count != 0)
				    {
					    if (Convert.ToString(cursor.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
					    {
                            try
                            {
                                if (FileSystem.Dir("\\\\" + mbPrincipal.Servidor + "\\" + mbPrincipal.Directorio + "\\IMDHuserexit.exe ", FileAttribute.Normal) != "")
                                {
                                    ProcessStartInfo startInfo2 = new ProcessStartInfo("\\\\" + mbPrincipal.Servidor + "\\" + mbPrincipal.Directorio + "\\IMDHuserexit.exe " + mbPrincipal.vstUsuario + "," + mbAcceso.gstServerActual + "," + mbAcceso.gstBasedatosActual);
                                    startInfo2.WindowStyle = ProcessWindowStyle.Normal;
                                    Respuesta = Convert.ToInt32(Process.Start(startInfo2).Id);
                                }
                            }
                            catch (Exception ex) {
                                if (Information.Err().Number == Convert.ToInt32(5))
                                {
                                    //POR SI da un error al ejecutar IMDHuserexit.exe, se contin�a la ejecuci�n
                                } else {
                                    RadMessageBox.Show(ex.Message);
                                }
                            }
					    }
				    }
				    cursor.Close();


				    Serrores.AjustarRelojCliente(mbPrincipal.RcPrincipal);
				    CargarMenuModulos();
				    CargarIconos();

				    //Cargamos los ToolTipText de los iconos.
				    //Como el �ndice de cada icono se corresponde con el �ndice
				    //de su opci�n de men� correspondiente, pondremos en el ToolTipText
				    //de cada icono el caption de la opci�n de men� correspondiente
				    //For i = 0 To 12
				    //    imgIconos(i).ToolTipText = QuitarUnpersand(mnuOpcion(i).Caption)
				    //Next i

				    mbPrincipal.FormularioMDI.Text = Serrores.vNomAplicacion + " - " + "IMDH00F1";
				    this.Text = Serrores.vNomAplicacion + " - " + "IMDH00F1";
				    mbPrincipal.ControlPantalla.mnuAcercaDeItem.Text = mbPrincipal.ControlPantalla.mnuAcercaDeItem.Text + Serrores.vNomAplicacion;


				    bMensajeriaPrivada = false;
				    sql = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO='MENSAPRI'";
				    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
				    cursor = new DataSet();
				    tempAdapter_3.Fill(cursor);
				    if (cursor.Tables[0].Rows.Count != 0)
				    {
					    if (Convert.ToString(cursor.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
					    {
						    bMensajeriaPrivada = true;
					    }
				    }
				
                    cursor.Close();
				    mnu_LeerMensajesPrivados.Visibility = (bMensajeriaPrivada)
                    ? Telerik.WinControls.ElementVisibility.Visible
                    : Telerik.WinControls.ElementVisibility.Collapsed;


				    if (mbPrincipal.vstUsuario == mbAcceso.vstMetaUsuario)
				    {
					    mnuCambiarContrase�a.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
					    if (!mbAcceso.AplicarControlAcceso(mbPrincipal.vstUsuario, "P", this.ParentForm, "IMDH00F2", ref mbPrincipal.astrControles, ref mbPrincipal.astrEventos))
					    {
						    Environment.Exit(0);
					    }

				    }
				    else
				    {
					    mnuCambiarContrase�a.Visibility = Telerik.WinControls.ElementVisibility.Visible;
					    if (!mbAcceso.AplicarControlAcceso(mbPrincipal.vstUsuario, "P", this.ParentForm, "IMDH00F2", ref mbPrincipal.astrControles, ref mbPrincipal.astrEventos))
					    {
						    Environment.Exit(0);
					    }

				    }


				    if (NuevaImagen)
				    {
					    int tempForVar = imgIconos.GetUpperBound(0);
					    for (int I = 0; I <= tempForVar; I++)
					    {
						    LbEtiqueta[I].Visible = imgIconos[I].Visible;
						    imgIconos[I].Visible = false;
					    }
				    }
				    else
				    {
					    OrdenarIconos();
				    }
				    aModulo = new IMDH00F2.Modulo[1];


				    mbPrincipal.vPathAplicacion = Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\";

                    //Di�logo de la Ayuda
                    string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;
                    
				  
                    CargaVentanas();
				    ExistenMensajes();

				    if (mbPrincipal.bLargo)
				    {
					    stBar.Items[1].Text = mbPrincipal.vstUsuarioLargo;
				    }
				    else
				    {
					    stBar.Items[1].Text = mbPrincipal.vstUsuario;
				    }

				    //stBar.Panels(2).Text = vstUsuario
				    //stBar.Panels(4).Text = VstBaseInicial & " en el servidor " & VstServidorInicial

				    //Oscar: Incidencias de acceso
				    //stBar.Panels(4).Text = fnQueryRegistro("BASEDATOS") & " en el servidor " & fnQueryRegistro("SERVIDOR")
				    stBar.Items[3].Text = mbAcceso.gstBasedatosActual + " en el servidor " + mbAcceso.gstServerActual;
				    //Call S01000F1.GrabaIncidencia(vstUsuario, "PRINCIPAL", 0)
				    //---------------------------

				    this.Top = (int) 0;
				    this.Left = (int) 0;
				    stBar.Dock = DockStyle.Bottom;
				    this.Width = (int) mbPrincipal.FormularioMDI.ClientRectangle.Width;
				    this.Height = (int) mbPrincipal.FormularioMDI.ClientRectangle.Height;



				    if (mbPrincipal.objBloqueo == null && mbPrincipal.ActivarBloqueo)
				    {

                        mbPrincipal.objBloqueo = new BloqueoSistema.clsBloqueo();

                        mbPrincipal.objBloqueo.Iniciar(this, ref mbPrincipal.vstUsuario, ref mbAcceso.gstServerActual, ref mbAcceso.gstDriver, ref mbAcceso.gstBasedatosActual, ref mbAcceso.gstUsuarioBD, ref mbAcceso.gstPasswordBD);

				    }
				    else if (mbPrincipal.ActivarBloqueo)
				    { 

					    mbPrincipal.objBloqueo.CambioUsuario(mbPrincipal.vstUsuario, this);

				    }
				    //RcPrincipal.Close
				    //fConexion = False
			    }
               catch (Exception e)
			    {
					    RadMessageBox.Show(e.Message, Application.ProductName);
			    }
		}
       
		private void ExistenMensajes()
		{
			string sql = String.Empty;
			DataSet cursor = null;
			object[, ] matriz = null;
			int X = 0;
			try
			{
				//oscar 04/12/2003
				object TextMensaje = null;
				TextMensaje = DBNull.Value;
				//-----
				X = 0;
				if (!mbPrincipal.fConexion)
				{
					//If EstablecerConexion = False Then
					if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
					{
						// MsgBox ("Error de conexi�n")
					}

				}
                LeerCorreoDLL.MCLeerCorreo objLeerCor = null;
				if (mbPrincipal.fConexion)
				{
					//oscar 04/12/2003 : a�adimos el campo a�o
					//RcPrincipal.BeginTrans
					//SQL = "Select * from smensusu,stexmens where smensusu.gusuario='" & vstUsuario & "' and fechlect is null " & _
					//" and stexmens.nmensaje=smensusu.nmensaje and irayosmen <> 'S'"
					sql = "Select * from smensusu,stexmens " + 
					      " where smensusu.gusuario='" + mbPrincipal.vstUsuario + "' and fechlect is null " + 
					      " and stexmens.amensaje=smensusu.amensaje " + 
					      " and stexmens.nmensaje=smensusu.nmensaje and irayosmen <> 'S'";
					//-----
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
					cursor = new DataSet();
					tempAdapter.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{
						X = 1;

						foreach (DataRow iteration_row in cursor.Tables[0].Rows)
						{
							matriz = ArraysHelper.RedimPreserve<object[, ]>(matriz, new int[]{3, X + 1});
							matriz[1, X] = iteration_row["asunto"];

							//oscar 04/12/2003
							//matriz(2, x) = cursor("textomen")
							TextMensaje = iteration_row["textomen"];
							if (!Convert.IsDBNull(TextMensaje))
							{
								matriz[2, X] = TextMensaje;
							}
							else
							{
								matriz[2, X] = "";
							}
							//------------
                            object fecha = DateTime.Now;
                            string sql2 = "update smensusu set FECHLECT = " + Serrores.FormatFechaHMS(fecha) + " where gusuario = '" + iteration_row["gusuario"].ToString().Trim() + "' and nmensaje = '" + iteration_row["nmensaje"].ToString().Trim() +"' and amensaje = '" + iteration_row["amensaje"].ToString().Trim() +"'" ;
                            SqlCommand tempCommand = new SqlCommand(sql2, mbPrincipal.RcPrincipal);
                            tempCommand.ExecuteNonQuery();
                            X++;
						}
					}
					cursor.Close();

					if (X > 0)
					{
						for (X = 1; X <= matriz.GetUpperBound(1); X++)
						{
							objLeerCor = new LeerCorreoDLL.MCLeerCorreo();
							//oscar 04/12/2003
							//objLeerCor.Load RcPrincipal, "G", vstUsuario, True
							 objLeerCor.Load(mbPrincipal.RcPrincipal, "G", mbPrincipal.vstUsuario, true, Convert.ToString(matriz[1, X]), Convert.ToString(matriz[2, X]));
							//-------
							objLeerCor = null;
						}
					}
					if (!Timer1.Enabled)
					{
						sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='TILECMEN'";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
						cursor = new DataSet();
						tempAdapter_3.Fill(cursor);
						if (cursor.Tables[0].Rows.Count != 0)
						{
							if (Convert.ToDouble(cursor.Tables[0].Rows[0]["NNUMERI1"]) != 0)
							{
								if (Convert.ToInt32(Convert.ToDouble(cursor.Tables[0].Rows[0]["NNUMERI1"]) * 1000) == 0)
								{
									Timer1.Enabled = false;
								}
								else
								{
									Timer1.Interval = Convert.ToInt32(Convert.ToDouble(cursor.Tables[0].Rows[0]["NNUMERI1"]) * 1000);
									Timer1.Enabled = true;
								}
								Timer1.Enabled = true;
							}
						}
						cursor.Close();
					}
					//RcPrincipal.CommitTrans
				}
			}
			catch (System.Exception excep)
			{
				//    RcPrincipal.RollbackTrans
			 	RadMessageBox.Show(excep.Message, Application.ProductName);
			}

		}

		private void IMDH00F2_MouseMove(Object eventSender, MouseEventArgs eventArgs)
		{
			SinResalte();
			CargaVentanas();
		}

		private bool isInitializingComponent;
		private void IMDH00F2_Resize(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			this.Refresh();
		}

		private void IMDH00F2_Closed(Object eventSender, CancelEventArgs eventArgs)
		{
			//Miro si se est� ejecutando alg�n m�dulo y avisar de que no se puede
			//Cerrar la aplicaci�n hasta que no se cierren todos los m�dulos
			if (mbPrincipal.SalirActualizacion)
			{
				//SetValue "SOFTWARE\INDRA\GHOSP", "BASEDATOS", VstBaseInicial, 1
				//SetValue "SOFTWARE\INDRA\GHOSP", "SERVIDOR", VstServidorInicial, 1
				Environment.Exit(0);
			}
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}

			}
			if (mbPrincipal.fConexion)
			{
				if (proEstacionMedicaActiva())
				{
					short tempRefParam = 2900;
					string[] tempRefParam2 = new string[]{"Cerrar la Aplicaci�n", "Estaci�n M�dica"};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
					eventArgs.Cancel = true;
					mbPrincipal.CERRARCONEXION();
					return;
				}

				if (aModulo.GetUpperBound(0) != 0)
				{
					for (int I = 0; I <= aModulo.GetUpperBound(0) - 1; I++)
					{
						if (EstaActivo(aModulo[I].lManejador))
						{
							//Antes de "cerrar la aplicaci�n" debe cerrar el m�dulo de "MODULO"
							short tempRefParam3 = 2900;
							string[] tempRefParam4 = new string[]{"Cerrar la Aplicaci�n", aModulo[I].StModulo};
							mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
							eventArgs.Cancel = true;
							mbPrincipal.CERRARCONEXION();
							return;
						}
					}
				}
				if (mbPrincipal.gbSalir)
				{
					//SetValue "SOFTWARE\INDRA\GHOSP", "BASEDATOS", VstBaseInicial, 1
					//SetValue "SOFTWARE\INDRA\GHOSP", "SERVIDOR", VstServidorInicial, 1
					//SetValue "SOFTWARE\INDRA\GHOSP", "USUARIO", "", 1
					Environment.Exit(0);
				}
				Serrores.Borrar_DSN(mbPrincipal.DSN_SQLserver, mbPrincipal.dsn_sqlaccess);
			}
            objGestMen = null;
            this.Dispose();
            IMDH00F2.DefInstance = null;            
		}

		private void imgIconos_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.imgIconos, eventSender);

			string tempRefParam = Index.ToString();
			if (!mbAcceso.PermitirEvento(mbPrincipal.RcPrincipal, mbPrincipal.astrEventos, "imgIconos", tempRefParam, "DblClick"))
			{
				return;
			}

			Ejecutar(Index);

		}

        private void imgIconos_MouseDown(Object eventSender, MouseEventArgs e)
        {
            Posx = MousePosition.X;
            Posy = MousePosition.Y;
            if (e.Clicks == 2)
            {
                //imgIconos_DoubleClick(eventSender, e);
            }
            else
            {

                mbPrincipal.Cual = Array.IndexOf(this.imgIconos, eventSender);
                if (mbPrincipal.Cual >= 0)
                {
                    imgIconos[mbPrincipal.Cual].DoDragDrop(imgIconos[mbPrincipal.Cual], DragDropEffects.Move);
                }
            }

        }

        private void imgIconos_GiveFeedback(object sender, System.Windows.Forms.GiveFeedbackEventArgs e)
        {
            if (e.Effect == DragDropEffects.Move)
            {
                e.UseDefaultCursors = false;
                Bitmap icon = new Bitmap((sender as PictureBox).Image);
                Cursor.Current = new Cursor(icon.GetHicon());
            } else
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void imgIconos_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
            //mbPrincipal.Cual = Array.IndexOf(this.imgIconos, eventSender);
            //if (mbPrincipal.Cual >= 0)
            //{
            //    imgIconos[mbPrincipal.Cual].DoDragDrop(imgIconos[mbPrincipal.Cual], DragDropEffects.Move);
            //}
		}

		private void LbEtiqueta_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.LbEtiqueta, eventSender);

			UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(500);
			if (!mbPrincipal.fConexion)
			{
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}
			}
			if (mbPrincipal.fConexion)
			{
				string tempRefParam = Index.ToString();
				if (!mbAcceso.PermitirEvento(mbPrincipal.RcPrincipal, mbPrincipal.astrEventos, "imgIconos", tempRefParam, "DblClick"))
				{
					mbPrincipal.CERRARCONEXION();
					return;
				}
				Ejecutar(Index);
			}

		}

		private void LbEtiqueta_MouseMove(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float X = (float) (eventArgs.X * 15);
			float Y = (float) (eventArgs.Y * 15);
			int Index = Array.IndexOf(this.LbEtiqueta, eventSender);
			SinResalte();
            LbEtiqueta[Index].Cursor = Cursors.Hand;
			
			LbEtiqueta[Index].ForeColor = ColorTranslator.FromOle(resaltado);
		}

		public void mnu_accesootras_Click(Object eventSender, EventArgs eventArgs)
		{            
			//'    *************** CURIAE: A REVISAR POR EL EQUIPO DE MIGRACION ********************
			//'              NUEVA FORMA DE CONEXION AL MODULO DE IFMS
			//'    *************************************************************************
			//'    Dim ifms As Object
			//'    Dim X As Integer
			//'    Dim CodDpto As String
			//'    Dim CodEmpresa As String
			//'    If fConexion = False Then
			//'        If EstablecerConexion(gstServerActual, _
			//''                              gstDriver, _
			//''                              gstBasedatosActual, _
			//''                              gstUsuarioBD, _
			//''                              gstPasswordBD) = False Then
			//'            MsgBox ("Error de conexi�n")
			//'         End If
			//'    End If
			//'    If fConexion = True Then
			//'        Set ifms = CreateObject("Dll_Menu.Menu")
			//'        Me.MousePointer = 11
			//'        CodEmpresa = ObtenerCodigoSConsglo("FOPEMENU", "valfanu1", RcPrincipal)
			//'        CodDpto = ObtenerCodigoSConsglo("FOPEMENU", "valfanu2", RcPrincipal)
			//'        X = ifms.cargar_m(vstUsuario, CodEmpresa, CodDpto, "SQLSERVER", RcPrincipal, "Principal", "", Me)
			//'        CERRARCONEXION
			//'        Me.MousePointer = 0
			//'        If X <> 0 Then
			//'            MsgBox ("Error al abrir men� n�mero :" & Trim(Str(X)))
			//'        End If
			//'    End If
		}

		public void mnu_LeerCorreo_Click(Object eventSender, EventArgs eventArgs)
		{
            LeerCorreoDLL.MCLeerCorreo objLeerCor = null;
            if (!mbPrincipal.fConexion)
            {
                //If EstablecerConexion = False Then
                if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
                {
                    RadMessageBox.Show("Error de conexi�n", Application.ProductName);
                }

            }
            if (mbPrincipal.fConexion)
            {
                objLeerCor = new LeerCorreoDLL.MCLeerCorreo();
                string tempRefParam = "G";
                bool tempRefParam2 = false;
                string tempRefParam3 = String.Empty;
                string tempRefParam4 = String.Empty;
                objLeerCor.Load(mbPrincipal.RcPrincipal, tempRefParam, mbPrincipal.vstUsuario, tempRefParam2, tempRefParam3, tempRefParam4);
                objLeerCor = null;
            }
            //Load S06000F1
            //S06000F1.Show 1
            mbPrincipal.CERRARCONEXION();
		}

		public void mnu_LeerMensajesPrivados_Click(Object eventSender, EventArgs eventArgs)
		{
            if (bMensajeriaPrivada)
            {
                if (objGestMen == null)
                {
                    objGestMen = new Mensajeria.MCMensajeria();
                    objGestMen.proLoadMensajeria(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD, mbPrincipal.vstUsuario);
                }
                objGestMen.proVerMenuMensajeria();
            }
		}

		public void mnuAcercaDeItem_Click(Object eventSender, EventArgs eventArgs)
		{

			frmAcerca.DefInstance.Show();

		}



		public void mnuCambiarBD_Click(Object eventSender, EventArgs eventArgs)
		{
			//Miro si se est� ejecutando alg�n m�dulo y avisar de que no se puede
			//Cerrar la aplicaci�n hasta que no se cierren todos los m�dulos
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}

			}
			if (mbPrincipal.fConexion)
			{
				if (proEstacionMedicaActiva())
				{
					short tempRefParam = 2900;
					string[] tempRefParam2 = new string[]{"Cambiar de Usuario", "Estaci�n M�dica"};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
					mbPrincipal.CERRARCONEXION();
					return;
				}

				if (aModulo.GetUpperBound(0) != 0)
				{
					for (int I = 0; I <= aModulo.GetUpperBound(0) - 1; I++)
					{
						if (EstaActivo(aModulo[I].lManejador))
						{
							//Antes de "cambiar de usuario" debe cerrar el m�dulo de "MODULO"
							short tempRefParam3 = 2900;
							string[] tempRefParam4 = new string[]{"Cambiar de B.D.", aModulo[I].StModulo};
							mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
							mbPrincipal.CERRARCONEXION();
							return;
						}
					}
				}

				mbPrincipal.iContador = 0;
				S03000F1.DefInstance.ShowDialog();
			}
		}

		public void mnuCambiarContrase�a_Click(Object eventSender, EventArgs eventArgs)
		{
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}

			}
			if (mbPrincipal.fConexion)
			{
				S02000F1.DefInstance.ShowDialog();
			}

		}

		public void mnuCambiarUsuario_Click(Object eventSender, EventArgs eventArgs)
		{
			//Miro si se est� ejecutando alg�n m�dulo y avisar de que no se puede
			//Cerrar la aplicaci�n hasta que no se cierren todos los m�dulos
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}

			}
			if (mbPrincipal.fConexion)
			{
				if (proEstacionMedicaActiva())
				{
					short tempRefParam = 2900;
					string[] tempRefParam2 = new string[]{"Cambiar de Usuario", "Estaci�n M�dica"};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
					return;
				}

				if (aModulo.GetUpperBound(0) != 0)
				{
					for (int I = 0; I <= aModulo.GetUpperBound(0) - 1; I++)
					{
						if (EstaActivo(aModulo[I].lManejador))
						{
							//Antes de "cambiar de usuario" debe cerrar el m�dulo de "MODULO"
							short tempRefParam3 = 2900;
							string[] tempRefParam4 = new string[]{"Cambiar de Usuario", aModulo[I].StModulo};
							mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
							return;
						}
					}
				}

				mbPrincipal.iContador = 0;
				S01000F1.DefInstance.ShowDialog();
			}
		}


		public void mnuContenido_Click(Object eventSender, EventArgs eventArgs)
		{
                    
            //Contenido de ayuda
            string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;
           
                try
                {
                    Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
                }
                catch
                {
                    string PATHAYUDA_ = Path.GetDirectoryName(Application.ExecutablePath);
                    string FICHEROAYUDAPRINCIPAL_ = "Ayuda_Principal.hlp";
                    string strHelpA = PATHAYUDA_ + @"\" + FICHEROAYUDAPRINCIPAL_;
                    DialogResult Result = RadMessageBox.Show("No se puede hallar el archivo " + strHelpA + "." + " �Desea Intentar hallarlo usted mismo?", "Ayuda de Windows", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                    if (Result == System.Windows.Forms.DialogResult.Yes)
                    {
                        OpenFileDialog openFileDialog1 = new OpenFileDialog();
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            strHelp = openFileDialog1.FileName;
                            Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
                            Constantes.DefInstance.PATHAYUDA = Path.GetDirectoryName(strHelp);
                            Constantes.DefInstance.FICHEROAYUDAPRINCIPAL = openFileDialog1.SafeFileName;

                        }
                    }
                }
           
		}

		public void mnuIndiceItem_Click(Object eventSender, EventArgs eventArgs)
		{
            //Indice de ayuda
            string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;
              try
                {
                    Help.ShowHelp(this, strHelp, HelpNavigator.Index);
                }
                catch
                {
                    string PATHAYUDA_ = Path.GetDirectoryName(Application.ExecutablePath);
                    string FICHEROAYUDAPRINCIPAL_ = "Ayuda_Principal.hlp";
                    string strHelpA = PATHAYUDA_ + @"\" + FICHEROAYUDAPRINCIPAL_;
                    DialogResult Result = RadMessageBox.Show("No se puede hallar el archivo " + strHelpA + "." + " �Desea Intentar hallarlo usted mismo?", "Ayuda de Windows", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                    if (Result == System.Windows.Forms.DialogResult.Yes)
                    {
                        OpenFileDialog openFileDialog1 = new OpenFileDialog();
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            strHelp = openFileDialog1.FileName;
                            Help.ShowHelp(this, strHelp, HelpNavigator.Index);
                            Constantes.DefInstance.PATHAYUDA = Path.GetDirectoryName(strHelp);
                            Constantes.DefInstance.FICHEROAYUDAPRINCIPAL = openFileDialog1.SafeFileName;

                        }
                    }
                }
		}

		public void mnuOpcion_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnuOpcion, eventSender);

			string tempRefParam = Index.ToString();
			if (!mbAcceso.PermitirEvento(mbPrincipal.RcPrincipal, mbPrincipal.astrEventos, "mnuOpcion", tempRefParam, "Click"))
			{
				return;
			}

			Ejecutar(Index);

		}


		//UPGRADE_NOTE: (7001) The following declaration (mnuOrganizarIconos_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void mnuOrganizarIconos_Click()
		//{
				//OrdenarIconos();
		//}

		public void mnuSalirItem_Click(Object eventSender, EventArgs eventArgs)
		{
			mbPrincipal.gbSalir = true;
			this.Close();
		}

		public void OrdenarIconos()
		{

            int iIconos = 0;

            mbPrincipal.viNuevoTop = 531; //360
            mbPrincipal.vinuevoLeft = 805; //360

            foreach (Control MiControl in ContainerHelper.Controls(this))
            {
                if (MiControl.Name == imgIconos[iIconos].Name)
                {
                    if (ControlArrayHelper.GetControlIndex(MiControl) > -1)
                    {
                        if (imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Visible)
                        {
                            if (((float)(this.Width * 15)) > mbPrincipal.vinuevoLeft + ((float)(imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Width * 15)))
                            {
                                imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Top = (int)(mbPrincipal.viNuevoTop / 15);
                                imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Left = (int)(mbPrincipal.vinuevoLeft / 15);
                                mbPrincipal.vinuevoLeft = Convert.ToInt32(mbPrincipal.vinuevoLeft + ((float)(imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Width * 15)) + mbPrincipal.COiLateral);
                                iIconos++;
                            }
                            else
                            {
                                mbPrincipal.viNuevoTop = Convert.ToInt32(mbPrincipal.viNuevoTop + ((float)(imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Height * 15)) + mbPrincipal.COiSuperior);
                                mbPrincipal.vinuevoLeft = mbPrincipal.COiLateral;
                                imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Top = (int)(mbPrincipal.viNuevoTop / 15);
                                imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Left = (int)(mbPrincipal.vinuevoLeft / 15);
                                mbPrincipal.vinuevoLeft = Convert.ToInt32(mbPrincipal.vinuevoLeft + ((float)(imgIconos[ControlArrayHelper.GetControlIndex(MiControl)].Width * 15)) + mbPrincipal.COiLateral);
                                iIconos++;
                            }
                        }
                        else
                        {
                            iIconos++;
                        }
                    }
                }
            }
		}

		public void mnuSincronizar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (!mbPrincipal.fConexion)
			{
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}
			}
			if (mbPrincipal.fConexion)
			{
				Serrores.AjustarRelojCliente(mbPrincipal.RcPrincipal);
			}
			mbPrincipal.CERRARCONEXION();
		}


		public void mnuTemasAyuda_Click(Object eventSender, EventArgs eventArgs)
		{
            string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;

            try
            {
                Help.ShowHelp(this, strHelp, HelpNavigator.Index);
            }
            catch
            {
                string PATHAYUDA_ = Path.GetDirectoryName(Application.ExecutablePath);
                string FICHEROAYUDAPRINCIPAL_ = "Ayuda_Principal.hlp";
                string strHelpA = PATHAYUDA_ + @"\" + FICHEROAYUDAPRINCIPAL_;
                DialogResult Result = RadMessageBox.Show("No se puede hallar el archivo " + strHelpA + "." + " �Desea Intentar hallarlo usted mismo?", "Ayuda de Windows", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                if (Result == System.Windows.Forms.DialogResult.Yes)
                {
                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        strHelp = openFileDialog1.FileName;
                        Help.ShowHelp(this, strHelp, HelpNavigator.Index);
                        Constantes.DefInstance.PATHAYUDA = Path.GetDirectoryName(strHelp);
                        Constantes.DefInstance.FICHEROAYUDAPRINCIPAL = openFileDialog1.SafeFileName;

                    }
                }
            }


		}


		public void mnuTemporal_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mnuTemporal, eventSender);
			int I = 0;
			int retorno = 0;
			string cadena = String.Empty;
			try
			{
				if (Index == 0)
				{
					OrdenarIconos();
				}
				else
				{
					I = (mnuTemporal[Index].Text.IndexOf(' ') + 1);
					cadena = mnuTemporal[Index].Text.Substring(I);
					for (int X = 0; X <= aModulo.GetUpperBound(0) - 1; X++)
					{
						if (aModulo[X].StModulo == cadena)
						{
							Interaction.AppActivate(aModulo[X].lIdentificador);
							break;
						}
					}
				}
			}
			catch
			{
				CargaVentanas();
			}
		}

		public void mnuUsarAyuda_Click(Object eventSender, EventArgs eventArgs)
		{
            //Como usar la ayuda
            string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;
              try
                {
                    Help.ShowHelp(this, strHelp, HelpNavigator.Index);
                }
                catch
                {
                    string PATHAYUDA_ = Path.GetDirectoryName(Application.ExecutablePath);
                    string FICHEROAYUDAPRINCIPAL_ = "Ayuda_Principal.hlp";
                    string strHelpA = PATHAYUDA_ + @"\" + FICHEROAYUDAPRINCIPAL_;
                    DialogResult Result = RadMessageBox.Show("No se puede hallar el archivo " + strHelpA + "." + " �Desea Intentar hallarlo usted mismo?", "Ayuda de Windows", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                    if (Result == System.Windows.Forms.DialogResult.Yes)
                    {
                        OpenFileDialog openFileDialog1 = new OpenFileDialog();
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            strHelp = openFileDialog1.FileName;
                            Help.ShowHelp(this, strHelp, HelpNavigator.Index);
                            Constantes.DefInstance.PATHAYUDA = Path.GetDirectoryName(strHelp);
                            Constantes.DefInstance.FICHEROAYUDAPRINCIPAL = openFileDialog1.SafeFileName;

                        }
                    }
                }
            
		}


		private void Timer1_Tick(Object eventSender, EventArgs eventArgs)
		{
			ExistenMensajes();
			mbPrincipal.CERRARCONEXION();
		}

		private void Timer2_Tick(Object eventSender, EventArgs eventArgs)
		{
			Inactivo();
		}
		private void Inactivo()
		{
			UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI w = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
			int X = 0;
			string CAP = String.Empty;
			int incremento = 5;
			int res = UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref w);
			if (w.X != Posx || w.Y != Posy)
			{
				Tiempo = DateTime.Now;
				Posx = w.X;
				Posy = w.Y;
			}
			else
			{
				if (Tiempo.AddSeconds(incremento) <= DateTime.Now)
				{
					//        MsgBox ("fin de la aplicaci�n")
					CAP = "Radiodiagnostico";
					for (X = 0; X <= aModulo.GetUpperBound(0) - 1; X++)
					{
						string tempRefParam = null;
						res = UpgradeSupportHelper.PInvoke.SafeNative.user32.FindWindow(ref tempRefParam, ref CAP);
						int tempRefParam2 = 0;
						res = UpgradeSupportHelper.PInvoke.SafeNative.user32.SendMessage(res, mbPrincipal.WM_SYSCOMMAND, mbPrincipal.SC_CLOSE, ref tempRefParam2);
					}
				}
			}
		}


		public void proReiniciaSesion(string usuarioNuevo, string usuarioNuevoLargo)
		{

			mbPrincipal.vstUsuario = usuarioNuevo;
			mbPrincipal.vstUsuarioLargo = usuarioNuevoLargo; //vstUsuario

			mbPrincipal.gbSalir = false;
			mbPrincipal.ControlPantalla.Close();
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					//            MsgBox ("Error de conexi�n")
				}
			}
			//IMDH00F3 tempLoadForm = mbPrincipal.ControlPantalla;
            if (mbPrincipal.ControlPantalla.Name == "IMDH00F2")
                mbPrincipal.ControlPantalla = new IMDH00F2();
            else if (mbPrincipal.ControlPantalla.Name == "IMDH00F3")
                mbPrincipal.ControlPantalla = new IMDH00F3();

			mbPrincipal.ControlPantalla.Show();
			mbPrincipal.gbSalir = true;
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					//            MsgBox ("Error de conexi�n")
				}
			}
			if (mbPrincipal.fConexion)
			{
				if (mbPrincipal.objBloqueo == null && mbPrincipal.ActivarBloqueo)
				{
                    mbPrincipal.objBloqueo = new BloqueoSistema.clsBloqueo();

                    mbPrincipal.objBloqueo.Iniciar(this, ref mbPrincipal.vstUsuario, ref mbAcceso.gstServerActual, ref mbAcceso.gstDriver, ref mbAcceso.gstBasedatosActual, ref mbAcceso.gstUsuarioBD, ref mbAcceso.gstPasswordBD);
                }
				mbPrincipal.CERRARCONEXION();
			}

		}

		public int[] proProcesosActivos()
		{

			int[] oProcesos = null;

			if (aModulo.GetUpperBound(0) > 0)
			{
				oProcesos = new int[aModulo.GetUpperBound(0) + 1];

				for (int I = 0; I <= aModulo.GetUpperBound(0) - 1; I++)
				{
					oProcesos[I] = aModulo[I].lIdentificador;
				}
			}
			else
			{
				oProcesos = new int[1];
			}

			return ArraysHelper.CastArray<int[]>(oProcesos);

		}


		private bool proEstacionMedicaActiva()
		{

            bool result = false;
            string strNameOfUser = String.Empty;
            string strDomainOfUser = String.Empty;

            ManagementObjectSearcher colProcesses = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Process where name = 'EstacionMedica.exe'");

            string[] argList = new string[] { string.Empty, string.Empty };

            try
            {

                foreach (ManagementObject objProcess in colProcesses.Get())
                {
                    int rtn = Convert.ToInt32(objProcess.InvokeMethod("GetOwner", argList));

                    if (rtn == 0)
                    {
                        if (vstUsuarioActual.Trim().ToUpper() == argList[0].ToString().ToUpper())
                        {
                            result = true;
                            break;
                        }
                    }
                }

                return result;
            }
            catch
            {

                return false;
            }
            return false;
		}

		//Esta funci�n devuelve el nombre del Usuario
		private string UsuarioActual()
		{
			string sUsuario = String.Empty;

			string sBuffer = new String(' ', 260);
			int lSize = sBuffer.Length;
			UpgradeSupportHelper.PInvoke.SafeNative.advapi32.GetUserName(ref sBuffer, ref lSize);
			if (lSize > 0)
			{
				sUsuario = sBuffer.Substring(0, Math.Min(lSize, sBuffer.Length));
				//Quitarle el CHR$(0) del final...
				lSize = (sUsuario.IndexOf(Strings.Chr(0).ToString()) + 1);
				if (lSize != 0)
				{
					sUsuario = sUsuario.Substring(0, Math.Min(lSize - 1, sUsuario.Length));
				}
			}
			else
			{
				sUsuario = "";
			}
			return sUsuario;
		}

        private void IMDH00F2_DragDrop(object sender, DragEventArgs e)
        {

            Control icono = (Control)imgIconos[mbPrincipal.Cual];
            if (Posx != e.X && Posy != e.Y)
            {
                int x = this.PointToClient(new Point(e.X, e.Y)).X;
                int y = this.PointToClient(new Point(e.Y, e.Y)).Y;
                icono.SetBounds(x, y, icono.Width, icono.Height);

            }


        }

        private void IMDH00F2_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
        
        //public void LoadForm()
        //{
        //    InitializeComponent();
        //    //mbPrincipal.ControlPantalla.ReLoadForm(false);
        //    InitializemnuTemporal();
        //    InitializemnuOpcion();
        //    InitializeimgIconos();
        //    InitializeLbEtiqueta();            
        //    IMDH00F2_Load(null, null);
        //    IMDH00F2_Activated(null, null);
        //    InitLayout();            
        //}
	}
}