using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class S01000F1
	{

		#region "Upgrade Support "
		private static S01000F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static S01000F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new S01000F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbAyuda", "cbAceptar", "tbContraseña", "tbUsuario", "cbCancelar", "lbContraseña", "lbUsuario", "imgIcono", "lbDescripcion"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbAyuda;
		public Telerik.WinControls.UI.RadButton cbAceptar;
        public Telerik.WinControls.UI.RadTextBoxControl tbContraseña;
		public Telerik.WinControls.UI.RadTextBoxControl tbUsuario;
		public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadLabel lbContraseña;
		public Telerik.WinControls.UI.RadLabel lbUsuario;
		public System.Windows.Forms.PictureBox imgIcono;
		public Telerik.WinControls.UI.RadLabel lbDescripcion;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S01000F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbAyuda = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.tbContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbUsuario = new Telerik.WinControls.UI.RadTextBoxControl();
            this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
            this.CDAyudaFont = new System.Windows.Forms.FontDialog();
            this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
            this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.lbContraseña = new Telerik.WinControls.UI.RadLabel();
            this.lbUsuario = new Telerik.WinControls.UI.RadLabel();
            this.imgIcono = new System.Windows.Forms.PictureBox();
            this.lbDescripcion = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbAyuda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbAyuda
            // 
            this.cbAyuda.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAyuda.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAyuda.Location = new System.Drawing.Point(280, 152);
            this.cbAyuda.Name = "cbAyuda";
            this.cbAyuda.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAyuda.Size = new System.Drawing.Size(81, 32);
            this.cbAyuda.TabIndex = 5;
            this.cbAyuda.Text = "A&yuda";
            this.cbAyuda.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAyuda.Click += new System.EventHandler(this.cbAyuda_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.Location = new System.Drawing.Point(106, 152);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 3;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // tbContraseña
            // 
            this.tbContraseña.AcceptsReturn = true;
            this.tbContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbContraseña.Location = new System.Drawing.Point(196, 100);
            this.tbContraseña.MaxLength = 8;
            this.tbContraseña.Name = "tbContraseña";
            this.tbContraseña.PasswordChar = '*';
            this.tbContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbContraseña.Size = new System.Drawing.Size(145, 20);
            this.tbContraseña.TabIndex = 1;
            // 
            // tbUsuario
            // 
            this.tbUsuario.AcceptsReturn = true;
            this.tbUsuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbUsuario.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbUsuario.Location = new System.Drawing.Point(196, 72);
            this.tbUsuario.MaxLength = 8;
            this.tbUsuario.Name = "tbUsuario";
            this.tbUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbUsuario.Size = new System.Drawing.Size(145, 20);
            this.tbUsuario.TabIndex = 0;
            this.tbUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbUsuario_KeyPress);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(192, 152);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 4;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // lbContraseña
            // 
            this.lbContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbContraseña.Location = new System.Drawing.Point(114, 104);
            this.lbContraseña.Name = "lbContraseña";
            this.lbContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbContraseña.Size = new System.Drawing.Size(68, 18);
            this.lbContraseña.TabIndex = 7;
            this.lbContraseña.Text = "Contraseña :";
            // 
            // lbUsuario
            // 
            this.lbUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUsuario.Location = new System.Drawing.Point(114, 76);
            this.lbUsuario.Name = "lbUsuario";
            this.lbUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUsuario.Size = new System.Drawing.Size(50, 18);
            this.lbUsuario.TabIndex = 6;
            this.lbUsuario.Text = "Usuario :";
            // 
            // imgIcono
            // 
            this.imgIcono.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgIcono.Image = ((System.Drawing.Image)(resources.GetObject("imgIcono.Image")));
            this.imgIcono.Location = new System.Drawing.Point(14, 12);
            this.imgIcono.Name = "imgIcono";
            this.imgIcono.Size = new System.Drawing.Size(70, 66);
            this.imgIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgIcono.TabIndex = 8;
            this.imgIcono.TabStop = false;
            // 
            // lbDescripcion
            // 
            this.lbDescripcion.AutoSize = false;
            this.lbDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDescripcion.Location = new System.Drawing.Point(116, 10);
            this.lbDescripcion.Name = "lbDescripcion";
            this.lbDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDescripcion.Size = new System.Drawing.Size(193, 35);
            this.lbDescripcion.TabIndex = 2;
            this.lbDescripcion.Text = "Escriba un código de Usuario y una Contraseña válidos para este Sistema ";
            // 
            // S01000F1
            // 
            this.AcceptButton = this.cbAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 186);
            this.ControlBox = false;
            this.Controls.Add(this.cbAyuda);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.tbContraseña);
            this.Controls.Add(this.tbUsuario);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.lbContraseña);
            this.Controls.Add(this.lbUsuario);
            this.Controls.Add(this.imgIcono);
            this.Controls.Add(this.lbDescripcion);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "S01000F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CONTROL DE ACCESO - S01000F1";
            this.Closed += new System.EventHandler(this.S01000F1_Closed);
            this.Load += new System.EventHandler(this.S01000F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbAyuda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

    }
}