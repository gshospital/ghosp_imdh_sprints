using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace Principal
{
	internal partial class S02000F1
        : Telerik.WinControls.UI.RadForm
	{

		public S02000F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void S02000F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					// MsgBox ("Error de conexión")
				}
			}

			DataSet RR = null;
			bool blnPassCambiada = false; //diego 01-06-2006
			if (mbPrincipal.fConexion)
			{
				//Comprobamos que el campo contraseña no sea nulo.
				if (tbContraseña.Text == "")
				{
					//Campo "CONTRASEÑA" requerido
					short tempRefParam = 1040;
					string[] tempRefParam2 = new string[]{S02000F1.DefInstance.lbContraseña.Text};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
					tbContraseña.Focus();
					return;
				}

				//Comprobamos que el campo no sea nulo.
				if (tbNuevaContraseña.Text == "")
				{
					//Campo "NUEVA CONTRASEÑA" requerido
					short tempRefParam3 = 1040;
					string[] tempRefParam4 = new string[]{S02000F1.DefInstance.lbNContraseña.Text};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
					tbNuevaContraseña.Focus();
					return;
				}

				//03/06/2004
				//oscar
				//Comprobamos que el campo nueva contraseña tenga como minimo el numero de caracteres especificado
				//en la constante global CONTLMIN.

				SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT ISNULL(NNUMERI1,0) FROM SCONSGLO WHERE GCONSGLO='CONTLMIN'", mbPrincipal.RcPrincipal);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{
					if (Strings.Len(tbNuevaContraseña.Text) < Convert.ToInt32(RR.Tables[0].Rows[0][0]))
					{
						short tempRefParam5 = 1020;
						string[] tempRefParam6 = new string[]{"Longitud de contraseña", "mayor o igual", Convert.ToString(RR.Tables[0].Rows[0][0])};
						mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, mbPrincipal.RcPrincipal, tempRefParam6);
						tbNuevaContraseña.Focus();
						return;
					}
				}
				
				RR.Close();
				
				//Comprobamos que el campo no es nulo.
				if (tbRepetNContraseña.Text == "")
				{
					//Campo "REPETIR NUEVA CONTRASEÑA" requerido
					short tempRefParam7 = 1040;
					string[] tempRefParam8 = new string[]{S02000F1.DefInstance.lbRepNContraseña.Text};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, mbPrincipal.RcPrincipal, tempRefParam8);
					tbRepetNContraseña.Focus();
					return;
				}

				//Comprobamos que los campos sean iguales.
				if (tbNuevaContraseña.Text != tbRepetNContraseña.Text)
				{
					//Contraseña no confirmada.
					//La contraseña de confirmación debe coincidir con la nueva
					//contraseña
					short tempRefParam9 = 1250;
					string[] tempRefParam10 = new string[]{};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, mbPrincipal.RcPrincipal, tempRefParam10);
					tbContraseña.Text = "";
					tbNuevaContraseña.Text = "";
					tbRepetNContraseña.Text = "";
					tbContraseña.Focus();
					return;
				}

				//CCR 09/2003 comprobar que la contraseña tecleada es la misma del usuario
				blnPassCambiada = false;

				string tempRefParam11 = tbContraseña.Text.Trim();
				if (ValidarContraseñaActual(ref tempRefParam11))
				{
					string tempRefParam12 = tbNuevaContraseña.Text;
					blnPassCambiada = CambiarContraseña(ref tempRefParam12);

					if (blnPassCambiada)
					{
						//MENSAJE: El Administrador del Sistema debe conocer
						//su contraseña.
						mbPrincipal.VerificarModoConexion();

						if (mbPrincipal.vstModo.Value == "N")
						{
							//El Administrador del Sistema debe conocer su nueva Contraseña.
							short tempRefParam13 = 1260;
							string[] tempRefParam14 = new string[]{};
							mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, mbPrincipal.RcPrincipal, tempRefParam14);
						}
						//''        CERRARCONEXION
						this.Close();
					}
				}
				else
				{
					short tempRefParam15 = 3900;
					string[] tempRefParam16 = new string[]{"la contraseña", "la del usuario"};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, mbPrincipal.RcPrincipal, tempRefParam16);
					tbContraseña.Text = "";
					tbNuevaContraseña.Text = "";
					tbRepetNContraseña.Text = "";
					tbContraseña.Focus();
				}
			}
			else
			{
                RadMessageBox.Show("Error al establecer la conexión con la Base de Datos ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
			}
		}

		private void cbAyuda_Click(Object eventSender, EventArgs eventArgs)
		{            
           ////Constantes Constante = new Constantes();
            string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;
         
            if (Constantes.DefInstance.FICHEROAYUDAPRINCIPAL == "")
            {
                try
                {
                    Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
                }
                catch
                {
                    DialogResult Result = RadMessageBox.Show("No se puede hallar el archivo " + strHelp + "." + " ¿Desea Intentar hallarlo usted mismo?", "Ayuda de Windows", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                    if (Result == System.Windows.Forms.DialogResult.Yes)
                    {
                        OpenFileDialog openFileDialog1 = new OpenFileDialog();
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            strHelp = openFileDialog1.FileName;
                            Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
                            Constantes.DefInstance.PATHAYUDA = Path.GetDirectoryName(strHelp);
                            Constantes.DefInstance.FICHEROAYUDAPRINCIPAL = openFileDialog1.SafeFileName;
                        }
                    }
                }
            }
            else
            {
                Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
            }
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (mbPrincipal.fConexion)
			{
				mbPrincipal.CERRARCONEXION();
			}
			this.Close();

		}

		private void S02000F1_Load(Object eventSender, EventArgs eventArgs)
		{
			if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico", FileAttribute.Normal) != "")
			{
				this.imgIcono.Image = Image.FromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico");
				this.Icon = new Icon(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico");
			}

			//    If ComprobarMetaUsuario(vstUsuario, vstContraseña) = True Then
			//        Mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1240, RcPrincipal
			//        Unload Me
			//    Else

			//lbNombreUsuario.Caption = vstUsuario
			lbNombreUsuario.Text = mbPrincipal.vstUsuarioLargo;
			//    End If

		}

		//DIEGO 01-06-2006 Cambio el procedimiento por una función.
		public bool CambiarContraseña(ref string stContraseña)
		{
			//Rutina para modificar la contraseña del usuario de la aplicación
			//encriptándola.
			bool result = false;
			string sql = String.Empty;

			stContraseña = mbPrincipal.EncriptarPassword(ref stContraseña);

			string stModificar = "SELECT * FROM susuario ";
			stModificar = stModificar + "WHERE gusuario='" + mbPrincipal.vstUsuario + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stModificar, mbPrincipal.RcPrincipal);
			mbPrincipal.RsPrincipal = new DataSet();
			tempAdapter.Fill(mbPrincipal.RsPrincipal);

			//oscar
			//No se permite cambiar la contraseña por la misma que tuviera anteriormente
			if (Convert.ToString(mbPrincipal.RsPrincipal.Tables[0].Rows[0]["vpassword"]).Trim() != stContraseña.Trim())
			{
				//OSCAR C Julio 2007
				if (!ValidaPassAnteriores(mbPrincipal.vstUsuario, stContraseña))
				{
					return false;
				}
				//------

				mbPrincipal.RsPrincipal.Tables[0].Rows[0]["vpassword"] = stContraseña;
				//Oscar: Caducidad password
				mbPrincipal.RsPrincipal.Tables[0].Rows[0]["FCONTCAD"] = DateTime.Today;
				//-----
				                
                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);                
                tempAdapter.Update(mbPrincipal.RsPrincipal);

				//oscar 03/06/2004
				sql = "INSERT INTO SUSUCCON (FCAMBIO, GUSUARIO, NPASSWORD) " + 
				      " VALUES (GETDATE(), '" + mbPrincipal.vstUsuario + "','" + stContraseña + "')";
				SqlCommand tempCommand = new SqlCommand(sql, mbPrincipal.RcPrincipal);
				tempCommand.ExecuteNonQuery();
				result = true;
				//-----
			}
			else
			{
                RadMessageBox.Show("ATENCION:" + Environment.NewLine + 
				                "NO se puede establecer como contraseña de usuario la contraseña anterior.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				return false;
			}
			
			mbPrincipal.RsPrincipal.Close();

			//oscar. Si se redirecciona al usuario a otra base de datos de forma
			//automatica, cuando éste cambie de contraseña, la fecha de modificacion de la
			//contraseña se cambiara en ambas bases de datos: la principal y la destino.
			string Base_Actual = String.Empty;
			if (mbPrincipal.bUsuarioRedireccionado)
			{
				//If EstablecerConexion()=False then
				if (!mbPrincipal.EstablecerConexion(mbPrincipal.gstServerInicial, mbAcceso.gstDriver, mbPrincipal.gstBasedatosInicial, mbPrincipal.gstUsuarioBDInicial, mbPrincipal.gstpasswordBDInicial))
				{
					RadMessageBox.Show("Error al establecer la conexión con la Base de Datos : " + mbPrincipal.VstBaseInicial + "\n" + "\r" + "Se reestablecerá la conexión con la Base de Datos : " + Base_Actual.Trim(), Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);

					//If EstablecerConexion()=False then
					if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
					{
                        RadMessageBox.Show("Error al reestablecer la conexión inicial. Contacte con informática.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
						Environment.Exit(0);
					}
				}
				else
				{
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stModificar, mbPrincipal.RcPrincipal);
					mbPrincipal.RsPrincipal = new DataSet();
					tempAdapter_3.Fill(mbPrincipal.RsPrincipal);
					mbPrincipal.RsPrincipal.Tables[0].Rows[0]["vpassword"] = stContraseña;
					mbPrincipal.RsPrincipal.Tables[0].Rows[0]["FCONTCAD"] = DateTime.Today;					
                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);                    
                    tempAdapter_3.Update(mbPrincipal.RsPrincipal, mbPrincipal.RsPrincipal.Tables[0].TableName);
					
					mbPrincipal.RsPrincipal.Close();

					//oscar 03/06/2004
					sql = "INSERT INTO SUSUCCON (FCAMBIO, GUSUARIO, NPASSWORD) " + 
					      " VALUES (GETDATE(), '" + mbPrincipal.vstUsuario + "','" + stContraseña + "')";
					SqlCommand tempCommand_2 = new SqlCommand(sql, mbPrincipal.RcPrincipal);
					tempCommand_2.ExecuteNonQuery();
					//-----
					//If EstablecerConexion()=False then
					if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
					{
                        RadMessageBox.Show("Error al reestablecer la conexión inicial. Contacte con informática.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
						Environment.Exit(0);
					}
				}
			}
			//--------------------------------
			return result;
		}

		public bool ComprobarCaracteres(ref string stTexto)
		{
			int[] entrada = new int[stTexto.Length + 1];
			stTexto = stTexto.ToUpper();
			for (int iI = 1; iI <= stTexto.Length; iI++)
			{
				entrada[iI] = Strings.Asc(stTexto.Substring(iI - 1, Math.Min(1, stTexto.Length - (iI - 1)))[0]);
				if (!(entrada[iI] == 32 || (entrada[iI] >= 48 && entrada[iI] <= 57) || (entrada[iI] >= 65 && entrada[iI] <= 90) || entrada[iI] == 209))
				{
					return false;
				}
			}
			return true;

		}

		private void S02000F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			// CERRARCONEXION
            S02000F1.DefInstance.Dispose();
		}

		private void tbContraseña_Leave(Object eventSender, EventArgs eventArgs)
		{            
			//Si pulsa cancelar no ejecuta la rutina
			if (ActiveControl.Name == cbCancelar.Name)
			{
				return;
			}

			//Comprobamos que el campo no sea nulo.
			if (tbContraseña.Text == "")
			{
				//Campo "CONTRASEÑA" requerido
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{S02000F1.DefInstance.lbContraseña.Text};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
				return;
			}
		}

		private void tbNuevaContraseña_Leave(Object eventSender, EventArgs eventArgs)
		{
			//Si pulsa cancelar no ejecuta la rutina
			if (ActiveControl.Name == cbCancelar.Name)
			{
				return;
			}
			//Comprobamos que el campo no sea nulo.
			if (tbNuevaContraseña.Text == "")
			{
				//Campo "NUEVA CONTRASEÑA" requerido
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{S02000F1.DefInstance.lbNContraseña.Text};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
				return;
			}

			//Comprobamos si los caracteres son válidos.
			string tempRefParam3 = tbNuevaContraseña.Text;
			if (!ComprobarCaracteres(ref tempRefParam3))
			{
				//Formato de "NUEVA CONTRASEÑA" erroneo, debe indicar "letras y números"
				short tempRefParam4 = 1230;
				string[] tempRefParam5 = new string[]{S01000F1.DefInstance.lbContraseña.Text, "letras y números"};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, mbPrincipal.RcPrincipal, tempRefParam5);
				tbNuevaContraseña.Focus();
				tbNuevaContraseña.SelectionStart = 0;
				tbNuevaContraseña.SelectionLength = Strings.Len(tbNuevaContraseña.Text);
				//SendKeys "{Home}+{End}"
			}
		}

		private void tbRepetNContraseña_Leave(Object eventSender, EventArgs eventArgs)
		{
			//Si pulsa cancelar no ejecuta la rutina
			if (ActiveControl.Name == cbCancelar.Name)
			{
				return;
			}

			//Comprobamos que el campo no es nulo.
			if (tbRepetNContraseña.Text == "")
			{
				//Campo "REPETIR NUEVA CONTRASEÑA" requerido
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{S02000F1.DefInstance.lbRepNContraseña.Text};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
				return;
			}

			//Comprobamos si los caracteres son válidos.
			string tempRefParam3 = tbRepetNContraseña.Text;
			if (!ComprobarCaracteres(ref tempRefParam3))
			{
				//Formato de "NUEVA CONTRASEÑA" erroneo, debe indicar "LETRAS Y NUMEROS"
				short tempRefParam4 = 1230;
				string[] tempRefParam5 = new string[]{S01000F1.DefInstance.lbContraseña.Text, "letras y números"};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, mbPrincipal.RcPrincipal, tempRefParam5);
				tbRepetNContraseña.Focus();
				tbRepetNContraseña.SelectionStart = 0;
				tbRepetNContraseña.SelectionLength = Strings.Len(tbRepetNContraseña.Text);
				//SendKeys "{Home}+{End}"
			}
		}

		private bool ValidarContraseñaActual(ref string stContraseña)
		{
			bool result = false;

			stContraseña = mbPrincipal.EncriptarPassword(ref stContraseña);

			string StSql = "SELECT * FROM susuario ";
			StSql = StSql + "WHERE gusuario='" + mbPrincipal.vstUsuario + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, mbPrincipal.RcPrincipal);
			mbPrincipal.RsPrincipal = new DataSet();
			tempAdapter.Fill(mbPrincipal.RsPrincipal);
			if (mbPrincipal.RsPrincipal.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(mbPrincipal.RsPrincipal.Tables[0].Rows[0]["vpassword"]).Trim() == stContraseña.Trim())
				{
					//se puede continuar con el proceso
					result = true;
				}
				else
				{
					//si la password que hay no es la misma que se ha tecleado
					//no se puede permitir el cambio
					result = false;
				}
			}
			
			mbPrincipal.RsPrincipal.Close();
			return result;
		}

		private bool ValidaPassAnteriores(string sUsuario, string Password)
		{
			bool result = false;
			string sql = String.Empty;
			DataSet rc = null;
			int I = 0;
			result = true;
			string X = Serrores.ObternerValor_CTEGLOBAL(mbPrincipal.RcPrincipal, "CONTREP", "NNUMERI1");
			if (X != "" && Conversion.Val(X) > 0)
			{
				sql = "select * FROM SUSUCCON " + 
				      " where gusuario ='" + sUsuario + "'" + 
				      " order by fcambio desc ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
				rc = new DataSet();
				tempAdapter.Fill(rc);
				if (rc.Tables[0].Rows.Count != 0 )
				{
					I = 1;

                   
                    //    foreach (DataRow iteration_row in rc.Tables[0].Rows)
                    //    {
                    //        if (Password.Trim().ToUpper() == Convert.ToString(iteration_row["npassword"]).Trim().ToUpper())
                    //        {
                    //            RadMessageBox.Show("ATENCION:" + Environment.NewLine +
                    //                            "NO se puede establecer como contraseña de usuario una de las " + X + " contraseñas  anteriores.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);

                    //            return false;
                    //        }
                    //        I++;
                    //    }   
                   
                      for(int i = 0; i < Convert.ToInt32(X); i++)
                        {
                        if (Password.Trim().ToUpper() == rc.Tables[0].Rows[i]["npassword"].ToString().Trim().ToUpper())
                        {
                            RadMessageBox.Show("ATENCION:" + Environment.NewLine +
                                            "NO se puede establecer como contraseña de usuario una de las " + X + " contraseñas  anteriores.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);

                            return false;
                        }
                    }
                }	
                			
				rc.Close();
			}
			return result;
		}
	}
}