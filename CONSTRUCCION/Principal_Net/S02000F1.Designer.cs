using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class S02000F1
	{

		#region "Upgrade Support "
		private static S02000F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static S02000F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new S02000F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbContraseña", "cbAyuda", "tbRepetNContraseña", "cbCancelar", "cbAceptar", "tbNuevaContraseña", "lbContraseña", "lbDescripcion", "imgIcono", "lbNombreUsuario", "lbUsuario", "lbRepNContraseña", "lbNContraseña"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadTextBoxControl tbContraseña;
		public Telerik.WinControls.UI.RadButton cbAyuda;
		public Telerik.WinControls.UI.RadTextBoxControl tbRepetNContraseña;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadTextBoxControl tbNuevaContraseña;
		public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		public Telerik.WinControls.UI.RadLabel lbContraseña;
		public Telerik.WinControls.UI.RadLabel lbDescripcion;
		public System.Windows.Forms.PictureBox imgIcono;
		public Telerik.WinControls.UI.RadLabel lbNombreUsuario;
		public Telerik.WinControls.UI.RadLabel lbUsuario;
		public Telerik.WinControls.UI.RadLabel lbRepNContraseña;
		public Telerik.WinControls.UI.RadLabel lbNContraseña;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S02000F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.tbContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbAyuda = new Telerik.WinControls.UI.RadButton();
            this.tbRepetNContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.tbNuevaContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
            this.CDAyudaFont = new System.Windows.Forms.FontDialog();
            this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
            this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
            this.lbContraseña = new Telerik.WinControls.UI.RadLabel();
            this.lbDescripcion = new Telerik.WinControls.UI.RadLabel();
            this.imgIcono = new System.Windows.Forms.PictureBox();
            this.lbNombreUsuario = new Telerik.WinControls.UI.RadLabel();
            this.lbUsuario = new Telerik.WinControls.UI.RadLabel();
            this.lbRepNContraseña = new Telerik.WinControls.UI.RadLabel();
            this.lbNContraseña = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAyuda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRepetNContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNuevaContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombreUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRepNContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tbContraseña
            // 
            this.tbContraseña.AcceptsReturn = true;
            this.tbContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbContraseña.Location = new System.Drawing.Point(288, 82);
            this.tbContraseña.MaxLength = 8;
            this.tbContraseña.Name = "tbContraseña";
            this.tbContraseña.PasswordChar = '*';
            this.tbContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbContraseña.Size = new System.Drawing.Size(67, 20);
            this.tbContraseña.TabIndex = 1;
            this.tbContraseña.Leave += new System.EventHandler(this.tbContraseña_Leave);
            // 
            // cbAyuda
            // 
            this.cbAyuda.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAyuda.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAyuda.Location = new System.Drawing.Point(274, 168);
            this.cbAyuda.Name = "cbAyuda";
            this.cbAyuda.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAyuda.Size = new System.Drawing.Size(81, 31);
            this.cbAyuda.TabIndex = 6;
            this.cbAyuda.Text = "A&yuda";
            this.cbAyuda.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAyuda.Click += new System.EventHandler(this.cbAyuda_Click);
            // 
            // tbRepetNContraseña
            // 
            this.tbRepetNContraseña.AcceptsReturn = true;
            this.tbRepetNContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRepetNContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbRepetNContraseña.Location = new System.Drawing.Point(288, 132);
            this.tbRepetNContraseña.MaxLength = 8;
            this.tbRepetNContraseña.Name = "tbRepetNContraseña";
            this.tbRepetNContraseña.PasswordChar = '*';
            this.tbRepetNContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRepetNContraseña.Size = new System.Drawing.Size(67, 20);
            this.tbRepetNContraseña.TabIndex = 3;
            this.tbRepetNContraseña.Leave += new System.EventHandler(this.tbRepetNContraseña_Leave);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(188, 168);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 31);
            this.cbCancelar.TabIndex = 5;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.Location = new System.Drawing.Point(102, 168);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 31);
            this.cbAceptar.TabIndex = 4;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // tbNuevaContraseña
            // 
            this.tbNuevaContraseña.AcceptsReturn = true;
            this.tbNuevaContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNuevaContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbNuevaContraseña.Location = new System.Drawing.Point(288, 106);
            this.tbNuevaContraseña.MaxLength = 8;
            this.tbNuevaContraseña.Name = "tbNuevaContraseña";
            this.tbNuevaContraseña.PasswordChar = '*';
            this.tbNuevaContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNuevaContraseña.Size = new System.Drawing.Size(67, 20);
            this.tbNuevaContraseña.TabIndex = 2;
            this.tbNuevaContraseña.Leave += new System.EventHandler(this.tbNuevaContraseña_Leave);
            // 
            // lbContraseña
            // 
            this.lbContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbContraseña.Location = new System.Drawing.Point(150, 86);
            this.lbContraseña.Name = "lbContraseña";
            this.lbContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbContraseña.Size = new System.Drawing.Size(65, 18);
            this.lbContraseña.TabIndex = 11;
            this.lbContraseña.Text = "Contraseña:";
            // 
            // lbDescripcion
            // 
            this.lbDescripcion.AutoSize = false;
            this.lbDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDescripcion.Location = new System.Drawing.Point(150, 12);
            this.lbDescripcion.Name = "lbDescripcion";
            this.lbDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDescripcion.Size = new System.Drawing.Size(205, 31);
            this.lbDescripcion.TabIndex = 10;
            this.lbDescripcion.Text = "Escriba la Nueva Contraseña y confírmela tecleándola de nuevo";
            // 
            // imgIcono
            // 
            this.imgIcono.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgIcono.Image = ((System.Drawing.Image)(resources.GetObject("imgIcono.Image")));
            this.imgIcono.Location = new System.Drawing.Point(16, 16);
            this.imgIcono.Name = "imgIcono";
            this.imgIcono.Size = new System.Drawing.Size(91, 91);
            this.imgIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgIcono.TabIndex = 12;
            this.imgIcono.TabStop = false;
            // 
            // lbNombreUsuario
            // 
            this.lbNombreUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNombreUsuario.Location = new System.Drawing.Point(288, 64);
            this.lbNombreUsuario.Name = "lbNombreUsuario";
            this.lbNombreUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNombreUsuario.Size = new System.Drawing.Size(2, 2);
            this.lbNombreUsuario.TabIndex = 9;
            // 
            // lbUsuario
            // 
            this.lbUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUsuario.Location = new System.Drawing.Point(150, 64);
            this.lbUsuario.Name = "lbUsuario";
            this.lbUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUsuario.Size = new System.Drawing.Size(47, 18);
            this.lbUsuario.TabIndex = 8;
            this.lbUsuario.Text = "Usuario:";
            // 
            // lbRepNContraseña
            // 
            this.lbRepNContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRepNContraseña.Location = new System.Drawing.Point(150, 134);
            this.lbRepNContraseña.Name = "lbRepNContraseña";
            this.lbRepNContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRepNContraseña.Size = new System.Drawing.Size(139, 18);
            this.lbRepNContraseña.TabIndex = 7;
            this.lbRepNContraseña.Text = "Repetir Nueva Contraseña:";
            // 
            // lbNContraseña
            // 
            this.lbNContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNContraseña.Location = new System.Drawing.Point(150, 108);
            this.lbNContraseña.Name = "lbNContraseña";
            this.lbNContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNContraseña.Size = new System.Drawing.Size(100, 18);
            this.lbNContraseña.TabIndex = 0;
            this.lbNContraseña.Text = "Nueva Contraseña:";
            // 
            // S02000F1
            // 
            this.AcceptButton = this.cbAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 211);
            this.ControlBox = false;
            this.Controls.Add(this.tbContraseña);
            this.Controls.Add(this.cbAyuda);
            this.Controls.Add(this.tbRepetNContraseña);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.tbNuevaContraseña);
            this.Controls.Add(this.lbContraseña);
            this.Controls.Add(this.lbDescripcion);
            this.Controls.Add(this.imgIcono);
            this.Controls.Add(this.lbNombreUsuario);
            this.Controls.Add(this.lbUsuario);
            this.Controls.Add(this.lbRepNContraseña);
            this.Controls.Add(this.lbNContraseña);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "S02000F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAMBIO DE CONTRASEÑA - S02000F1";
            this.Closed += new System.EventHandler(this.S02000F1_Closed);
            this.Load += new System.EventHandler(this.S02000F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAyuda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRepetNContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNuevaContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombreUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRepNContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}