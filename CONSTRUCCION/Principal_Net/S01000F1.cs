using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using ElementosCompartidos;
using System.DirectoryServices.AccountManagement;
using Microsoft.CSharp;

namespace Principal
{
	internal partial class S01000F1
        : Telerik.WinControls.UI.RadForm
	{
		public S01000F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            ThemeResolutionService.ApplicationThemeName = "Office2007Black";
		}


		private void S01000F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		public bool fLoginSucceeded = false;
		public bool fOK = false;
		public bool fTarjeta = false;
		int iNumeroIntentos = 0;
		string stPasswEncriptada = String.Empty;

		public void Usuario_largo()
		{
			//**indicador de si se usa usuario largo

			//**
			string tstLargo = "select * from sconsglo where gconsglo='IUSULARG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstLargo, mbPrincipal.RcPrincipal);
			DataSet tRrLargo = new DataSet();
			tempAdapter.Fill(tRrLargo);
			if (tRrLargo.Tables[0].Rows.Count == 0)
			{
				mbPrincipal.bLargo = false;
			}
			else
			{
				mbPrincipal.bLargo = Convert.ToString(tRrLargo.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S";
			}
		}

		public void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//oscar
			mbPrincipal.bUsuarioRedireccionado = false;
			//------
			//Comprobamos que todav�a no tenemos conexion establecida y
			//todav�a no hemos establecido la variable "Mensaje" para enviar
			//los posibles mensajes al usuario.

			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
                    RadMessageBox.Show("Error de conexi�n", Application.ProductName);
					Environment.Exit(0);
				}
				mbPrincipal.Mensaje = new Mensajes.ClassMensajes();
			}

			//Comprobar que los campos no est�n vac�os
			if (tbUsuario.Text == "")
			{
				//Campo "USUARIO" requerido
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{lbUsuario.Text};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
				return;
			}
			else if (tbContrase�a.Text == "")
			{ 
				//Campo "CONTRASE�A" requerido
				short tempRefParam3 = 1040;
				string[] tempRefParam4 = new string[]{lbContrase�a.Text};
				mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
				return;
			}

			//Comprobamos primero la password porque si viene vac�a, al
			//comprobar el Metausuario, s�lo con el nombre nos valdr�a,
			//porque la rutina de comprobaci�n permite que la contrase�a
			//vaya vac�a para los Subsistemas.

			mbPrincipal.vstUsuarioLargo = tbUsuario.Text.Trim();

			if (mbDirectorioActivo.vstTipoAutenticacion == "M")
			{
				mbDirectorioActivo.proAutenticacionUsuario(mbPrincipal.vstUsuarioLargo, mbPrincipal.RcPrincipal, mbPrincipal.bLargo);
			}
			else
			{
				mbDirectorioActivo.vstAutenticacionUsuario = mbDirectorioActivo.vstTipoAutenticacion;
			}

			if (tbContrase�a.Text == mbAcceso.vstMetaPassword && mbDirectorioActivo.vstTipoAutenticacion == "I")
			{
				mbAcceso.fMetaUsuario = mbAcceso.ComprobarMetaUsuario(tbUsuario.Text.Trim(), tbContrase�a.Text);

				//Si es el metausuario le dejamos pasar
				if (mbAcceso.fMetaUsuario)
				{
					mbPrincipal.vstUsuario = tbUsuario.Text.Trim();
					mbPrincipal.vstContrase�a = tbContrase�a.Text;
					fLoginSucceeded = true;
					fOK = true;

					//OSCAR: Caducidad de la pasword
					//Si es el metausaurio, la contrase�a no caduca
					GrabaIncidencia(tbUsuario.Text.Trim().Substring(0, Math.Min(8, tbUsuario.Text.Trim().Length)), "PRINCIPAL", 0);
					GrabaAcceso(tbUsuario.Text.Trim().Substring(0, Math.Min(8, tbUsuario.Text.Trim().Length)));
					//------------

					if (!mbPrincipal.fFormCargado)
					{                        		                      
						this.Close();
						mbPrincipal.ControlPantalla.Show();
						if (fTarjeta)
						{
							mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
							mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;
						}
                       
						mbPrincipal.fFormCargado = true;
					}
					else
					{
						mbPrincipal.gbSalir = false;
                      
						mbPrincipal.ControlPantalla.Close();
						this.Close();
						if (fTarjeta)
						{
							mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
							mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;
						}
						mbPrincipal.ControlPantalla.Show();                        
					}
				}
				else
				{
					//Si est� mal el nombre de MetaUsuario
					//Usuario no autorizado.
					short tempRefParam5 = 1240;
					string[] tempRefParam6 = new string[]{};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, mbPrincipal.RcPrincipal, tempRefParam6);

					//Oscar : Incidencias de acceso
					GrabaIncidencia(tbUsuario.Text.Trim().Substring(0, Math.Min(8, tbUsuario.Text.Trim().Length)), "PRINCIPAL", 3);
					//----------

					iNumeroIntentos = mbPrincipal.ObtenerNIntentos();
					if (!fTarjeta)
					{
						tbUsuario.Focus();
						tbUsuario.SelectionStart = 0;
						tbUsuario.SelectionLength = Strings.Len(tbUsuario.Text);
						//SendKeys "{Home}+{End}"
						tbContrase�a.Text = "";
						mbPrincipal.iContador++;
					}
					return;
				}
			}
			else
			{
				//Si no corresponde la contrase�a a la del MetaUsuario le
				//tratamos como un Usuario normal.

				//'''''''''''''''If vstUsuario = "use" Or vstUsuario = "Eusebio" Then
				//'''''''''''''''    'Encriptamos la contrase�a
				if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
				{
					string tempRefParam7 = tbContrase�a.Text;
					stPasswEncriptada = mbPrincipal.EncriptarPassword(ref tempRefParam7);
				}
				else
				{
					stPasswEncriptada = tbContrase�a.Text;
				}
				//'''''''''''''''Else
				//'''''''''''''''    'De momento no encriptamos
				//'''''''''''''''    stPasswEncriptada = vstContrase�a
				//'''''''''''''''End If

				//Consulta con nombre de usuario y la contrase�a encriptada
				//y comparar. Si est� bien le dejamos; si NO -> intentos n veces.

				if (mbPrincipal.iContador == 0)
				{
					iNumeroIntentos = mbPrincipal.ObtenerNIntentos();

					ComprobarUsuario(tbUsuario.Text.Trim(), stPasswEncriptada);

					if (mbPrincipal.RsPrincipal.Tables[0].Rows.Count < 1)
					{
						//Usuario no autorizado.
						short tempRefParam8 = 1240;
						string[] tempRefParam9 = new string[]{};
						mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam8, mbPrincipal.RcPrincipal, tempRefParam9);

						//Oscar : Incidencias de acceso
						GrabaIncidencia(tbUsuario.Text.Trim().Substring(0, Math.Min(8, tbUsuario.Text.Trim().Length)), "PRINCIPAL", 3);
						//----------

						if (!fTarjeta)
						{
							tbUsuario.Focus();
							tbUsuario.SelectionStart = 0;
							tbUsuario.SelectionLength = Strings.Len(tbUsuario.Text);
							//SendKeys "{Home}+{End}"
							tbContrase�a.Text = "";
							mbPrincipal.iContador++;
						}
						
						mbPrincipal.RsPrincipal.Close();
						return;
					}
					else
					{
						if (!mbPrincipal.bLargo)
						{
							mbPrincipal.vstUsuario = tbUsuario.Text.Trim();
						}
						else
						{
							mbPrincipal.vstUsuario = Convert.ToString(mbPrincipal.RsPrincipal.Tables[0].Rows[0]["gusuario"]).Trim();
						}
						mbPrincipal.vstContrase�a = tbContrase�a.Text;
						mbPrincipal.iContador = 0; // inicializamos el contador cada vez que entre
						// correctamente
						fLoginSucceeded = true;
						fOK = true;

						//OSCAR: Caducidad de la pasword
						//David: S�lo si estamos validando por I-MDH
						if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
						{
							CaducaPassword(mbPrincipal.bLargo, tbUsuario.Text.Trim(), stPasswEncriptada);
						}
						//------------

						if (!mbPrincipal.fFormCargado)
						{                            
							this.Close();

							//Oscar: Incidencias de acceso
							GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 0);
							GrabaAcceso(mbPrincipal.vstUsuario);
							//--------------------------
							if (mbPrincipal.PrimerAcceso)
							{
								//mbPrincipal.ControlPantalla.Hide();
								Redirecciona_BD();
								if (!mbPrincipal.PrimerAcceso)
								{
                                    mbPrincipal.ControlPantalla.Show();
								}
							}
							else
							{
                                mbPrincipal.ControlPantalla.Show();
							}
							//--------------------------

							if (fTarjeta)
							{
                                mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
                                mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;
							}
							                             
                             mbPrincipal.fFormCargado = true;
						}
						else
						{
                            mbPrincipal.gbSalir = false;

                            mbPrincipal.ControlPantalla.Close();
                           
                            this.Close();

							//Oscar: Incidencias de acceso
							GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 0);
							GrabaAcceso(mbPrincipal.vstUsuario);
							//---------------------------
							if (mbPrincipal.PrimerAcceso)
							{
								mbPrincipal.ControlPantalla.Hide();
								Redirecciona_BD();
								if (!mbPrincipal.PrimerAcceso)
								{
									mbPrincipal.ControlPantalla.Show();
								}
							}
							else
							{                                
                                if (mbPrincipal.ControlPantalla.Name == "IMDH00F2")
                                    mbPrincipal.ControlPantalla = new IMDH00F2();
                                else if (mbPrincipal.ControlPantalla.Name == "IMDH00F3")
                                    mbPrincipal.ControlPantalla = new IMDH00F3();

                                mbPrincipal.ControlPantalla.Show();                                
							}
							//---------------------------
							mbPrincipal.gbSalir = true;
							if (fTarjeta)
							{      
                         
								mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
								mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;                              
							}
							//ControlPantalla.Show
						}
					}
				}
				else
				{
					//de n�mero de intentos > que 0
					ComprobarUsuario(tbUsuario.Text.Trim(), stPasswEncriptada);

					if (mbPrincipal.RsPrincipal.Tables[0].Rows.Count < 1)
					{
						if (mbPrincipal.iContador < iNumeroIntentos - 1)
						{
							//Usuario no autorizado.
							short tempRefParam10 = 1240;
							string[] tempRefParam11 = new string[]{};
							mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam10, mbPrincipal.RcPrincipal, tempRefParam11);

							//Oscar : Incidencias de acceso
							GrabaIncidencia(tbUsuario.Text.Trim().Substring(0, Math.Min(8, tbUsuario.Text.Trim().Length)), "PRINCIPAL", 3);
							//---------------------------

							if (!fTarjeta)
							{
								tbUsuario.Focus();
								tbUsuario.SelectionStart = 0;
								tbUsuario.SelectionLength = Strings.Len(tbUsuario.Text);
								//SendKeys "{Home}+{End}"
								tbContrase�a.Text = "";
								mbPrincipal.iContador++;
							}							
							mbPrincipal.RsPrincipal.Close();
							return;
						}
						else
						{
							//Oscar : Incidencias de acceso
							GrabaIncidencia(tbUsuario.Text.Trim().Substring(0, Math.Min(8, tbUsuario.Text.Trim().Length)), "PRINCIPAL", 3);
							//----------

							//establece la variable global a false
							//para indicar un fallo en el inicio de sesi�n

							//Conexi�n fallida despu�s de "V1" intentos.
							//La Aplicaci�n "V2" se va a cerrar.
							short tempRefParam12 = 1180;
							string[] tempRefParam13 = new string[]{iNumeroIntentos.ToString(), Serrores.vNomAplicacion};
							mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam12, mbPrincipal.RcPrincipal, tempRefParam13);
							fLoginSucceeded = false;
							fOK = false;
							this.Hide();
							//OSCAR C Julio 2007
							Desactivar_Usuario(tbUsuario.Text.Trim());
							//----
							Environment.Exit(0);
						}
					}
					else
					{
						if (!mbPrincipal.bLargo)
						{
							mbPrincipal.vstUsuario = tbUsuario.Text.Trim();
						}
						else
						{
							mbPrincipal.vstUsuario = Convert.ToString(mbPrincipal.RsPrincipal.Tables[0].Rows[0]["gusuario"]).Trim();
						}
						mbPrincipal.vstContrase�a = tbContrase�a.Text;
						mbPrincipal.iContador = 0; // inicializamos el contador cada vez que entre
						// correctamente
						fLoginSucceeded = true;
						fOK = true;

						//OSCAR: Caducidad de la pasword
						if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
						{
							CaducaPassword(mbPrincipal.bLargo, tbUsuario.Text.Trim(), stPasswEncriptada);
						}
						//---------------------------

						if (!mbPrincipal.fFormCargado)
						{                           
							this.Close();
							//Oscar: Incidencias de acceso
							GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 0);
							GrabaAcceso(mbPrincipal.vstUsuario);
							//---------------------------
							if (mbPrincipal.PrimerAcceso)
							{
								mbPrincipal.ControlPantalla.Hide();
								Redirecciona_BD();
								if (!mbPrincipal.PrimerAcceso)
								{
									mbPrincipal.ControlPantalla.Show();
								}
							}
							else
							{
								mbPrincipal.ControlPantalla.Show();
							}
							//---------------------------

							if (fTarjeta)
							{
								mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
								mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;
							}
							//ControlPantalla.Show
                             mbPrincipal.fFormCargado = true;                        
                        }
						else
						{
							mbPrincipal.gbSalir = false;
            
							mbPrincipal.ControlPantalla.Close();
                            
							this.Close();

							//Oscar: Incidencias de acceso
							GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 0);
							GrabaAcceso(mbPrincipal.vstUsuario);
							//---------------------------
							if (mbPrincipal.PrimerAcceso)
							{
								mbPrincipal.ControlPantalla.Hide();
								Redirecciona_BD();
								if (!mbPrincipal.PrimerAcceso)
								{
									mbPrincipal.ControlPantalla.Show();
								}
							}
							else
							{
								mbPrincipal.ControlPantalla.Show();
							}
							//---------------------------

							if (fTarjeta)
							{
								mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
								mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;
							}
							//ControlPantalla.Show                          
						}
					}
				}
				//    RsPrincipal.Close

			}
            this.Close();
            mbPrincipal.ControlPantalla.Dock = DockStyle.Fill;
            mbPrincipal.FormularioMDI.MainMenu1.Visible = true;

            bool vCambiaContrase�a = (mbDirectorioActivo.vstTipoAutenticacion == "I") || (mbDirectorioActivo.vstTipoAutenticacion == "M" && mbDirectorioActivo.vstAutenticacionUsuario == "I");

            if (vCambiaContrase�a)
                mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            else
                mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;

            //SetValue "SOFTWARE\INDRA\GHOSP", "USUARIO", vstUsuario, 1

            mbPrincipal.CERRARCONEXION();
		}

		public void cbAyuda_Click(Object eventSender, EventArgs eventArgs)
		{
            string strHelp = Constantes.DefInstance.PATHAYUDA + @"\" + Constantes.DefInstance.FICHEROAYUDAPRINCIPAL;
            if (Constantes.DefInstance.FICHEROAYUDAPRINCIPAL != "")
            {
                try
                {
                    Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
                }
                catch
                {
                    DialogResult Result = RadMessageBox.Show("No se puede hallar el archivo " + strHelp + "." + " �Desea Intentar hallarlo usted mismo?", "Ayuda de Windows", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                    if (Result == System.Windows.Forms.DialogResult.Yes)
                    {
                        OpenFileDialog openFileDialog1 = new OpenFileDialog();
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            strHelp = openFileDialog1.FileName;
                            Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
                            Constantes.DefInstance.PATHAYUDA = Path.GetDirectoryName(strHelp);
                            Constantes.DefInstance.FICHEROAYUDAPRINCIPAL = openFileDialog1.SafeFileName;
                        }
                    }
                }
            }
            else
            {
                Help.ShowHelp(this, strHelp, HelpNavigator.TableOfContents);
            }
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			//establece la variable global a false
			//para indicar un fallo en el inicio de sesi�n

			//oscar
			//��OJO!!
			if (!mbPrincipal.PrimerAcceso)
			{
				if (!mbPrincipal.fFormCargado)
				{
					fLoginSucceeded = false;
					fOK = false;
					this.Hide();
				}
				else
				{
					//volvemos a dejar la conexion con la base de datos antes del cambio
					//si accedemos a esta pantalla via cambio de base de datos
					if (mbPrincipal.AccedoLoginMedianteCambioBD)
					{
						if (mbPrincipal.EstablecerConexion(mbPrincipal.gstServerAC, mbAcceso.gstDriver, mbPrincipal.gstBasedatosAC, mbPrincipal.gstUsuarioBDAC, mbPrincipal.gstpasswordBDAC))
						{

							mbAcceso.gstBasedatosActual = mbPrincipal.gstBasedatosAC;
							mbAcceso.gstServerActual = mbPrincipal.gstServerAC;
							mbAcceso.gstUsuarioBD = mbPrincipal.gstUsuarioBDAC;
							mbAcceso.gstPasswordBD = mbPrincipal.gstpasswordBDAC;

							mbDirectorioActivo.vstTipoAutenticacion = mbDirectorioActivo.gstTipoAutenticacion_Actual;
							mbDirectorioActivo.vstDominioAutenticacion = mbDirectorioActivo.gstDominio_Actual;
							mbDirectorioActivo.vstAutenticacionUsuario = mbDirectorioActivo.gstAutenticacionUsuario_Actual;

							mbPrincipal.AccedoLoginMedianteCambioBD = false;
						}
						else
						{
                            RadMessageBox.Show("Se ha producido un error al tratar de reestablecer la conexi�n." + Environment.NewLine + 
							                "Consulte con Inform�tica.", "Error de conexi�n", MessageBoxButtons.OK, RadMessageIcon.Error);
							Environment.Exit(0);
						}
					}
					//----------
					mbPrincipal.CERRARCONEXION();
					this.Close();
				}
			}
			else
			{
				Environment.Exit(0);
			}

		}

		private void S01000F1_Load(Object eventSender, EventArgs eventArgs)
		{
			if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico", FileAttribute.Normal) != "")
			{
				this.imgIcono.Image = Image.FromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico");
				this.Icon = new Icon(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico");
			}

			//Me.Height = 3195
			//Me.Width = 4800

			string stBuffer = new String(' ', 8);
			int lSize = stBuffer.Length;
			UpgradeSupportHelper.PInvoke.SafeNative.advapi32.GetUserName(ref stBuffer, ref lSize);

			int LOCALE_STIME = 0x1E;
			int LOCALE_USER_DEFAULT = 0x400;
			string tempRefParam = ":";
            /* ralopezn_NOTODO_X_1: CF003
			UpgradeSupportHelper.PInvoke.SafeNative.kernel32.SetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STIME, ref tempRefParam);
            */

            Usuario_largo();
			if (!mbPrincipal.bLargo)
			{
				tbUsuario.MaxLength = 8;
			}
			else
			{
				tbUsuario.MaxLength = 20;
			}

			if (mbPrincipal.vstUsuario == "")
			{
				if (lSize > 0)
				{
					//oscar
					//vstUsuario_NT = Left$(stBuffer, lSize)
					//tbUsuario.Text = Left$(stBuffer, lSize)
					mbPrincipal.vstUsuario_NT = Serrores.obtener_usuario();
					tbUsuario.Text = Serrores.obtener_usuario();
					//----
					tbUsuario.Text = tbUsuario.Text.Trim();
					tbUsuario.SelectionStart = 0;
					tbUsuario.SelectionLength = Strings.Len(tbUsuario.Text);
					//SendKeys "{Home}+{End}"
				}
				else
				{
					mbPrincipal.vstUsuario_NT = null;
					tbUsuario.Text = null;
				}
			}
			else
			{
				tbUsuario.Text = mbPrincipal.vstUsuarioLargo;
				tbUsuario.Text = tbUsuario.Text.Trim();
				tbUsuario.SelectionStart = 0;
				tbUsuario.SelectionLength = Strings.Len(tbUsuario.Text);
				//SendKeys "{Home}+{End}"
			}

			// Comprobamos el tipo de autenticaci�n a realizar

			mbDirectorioActivo.proCompruebaAutenticacion(mbPrincipal.RcPrincipal);

			tbContrase�a.MaxLength = Convert.ToInt16(mbDirectorioActivo.viTama�oPassword);

			fTarjeta = fTarjeta && ((mbDirectorioActivo.vstTipoAutenticacion == "I") || (mbDirectorioActivo.vstTipoAutenticacion == "M" && mbDirectorioActivo.vstAutenticacionUsuario == "I"));
		}

		public object ComprobarUsuario(string stUsuario, string stContrase�a)
		{
			string stConsulta = String.Empty;

			if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
			{
				// Si la validaci�n es por I-MDH, seguimos igual

				if (mbPrincipal.bLargo)
				{
					stConsulta = "select gusuario,vpassword,gusularg from susuario ";
					stConsulta = stConsulta + "Where gusularg ='" + stUsuario + "'";
				}
				else
				{
					stConsulta = "select gusuario,vpassword from susuario ";
					stConsulta = stConsulta + "Where gusuario ='" + stUsuario + "'";
				}

				stConsulta = stConsulta + " and vpassword='" + stContrase�a + "' and fborrado is null";
				object tempRefParam = DateTime.Now;
				stConsulta = stConsulta + " AND (FVALIDEZ IS NULL OR FVALIDEZ >=" + Serrores.FormatFechaHMS(tempRefParam) + ")";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stConsulta, mbPrincipal.RcPrincipal);
				mbPrincipal.RsPrincipal = new DataSet();
				tempAdapter.Fill(mbPrincipal.RsPrincipal);
			}
			else
			{
				// La validaci�n es por Dominio. Intentamos validar

				//If AuthenticateUser(vstDominioAutenticacion, stUsuario, stContrase�a) Then
				if (AuthCheck(stUsuario, stContrase�a))
				{
					// Validaci�n correcta. Avanzamos, comprobando siempre con el usuario largo

					object tempRefParam2 = DateTime.Now;
					stConsulta = "select gusuario,vpassword,gusularg from susuario where " + 
					             "gusularg = '" + stUsuario + "' and " + 
					             "fborrado is null and " + 
					             "(FVALIDEZ IS NULL OR FVALIDEZ >=" + Serrores.FormatFechaHMS(tempRefParam2) + ")";
				}
				else
				{
					// No se devolver� ning�n resultado

					stConsulta = "select * from SUSUARIO where 1 = 2";
				}

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stConsulta, mbPrincipal.RcPrincipal);
				mbPrincipal.RsPrincipal = new DataSet();
				tempAdapter_2.Fill(mbPrincipal.RsPrincipal);
			}
			return null;
		}

		private bool AuthCheck(string sUser, string sPassword)
		{
			bool result = false;

			try
			{
                using (PrincipalContext context = new PrincipalContext(ContextType.Domain))
                {
                    result = context.ValidateCredentials(sUser, sPassword);
                }

				return result;
			}
			catch
			{

				return false;
			}
		}

		//OSCAR:
		//Caducidad Password
		public void CaducaPassword(bool PbLargo, string PstUsuario, string PstContrase�a)
		{
			string sql = String.Empty;
			double lDias_para_Caducar = 0;
			double lDias_desde_avisar = 0;
			System.DateTime FechaCaducidad = DateTime.FromOADate(0);

			Serrores.AjustarRelojCliente(mbPrincipal.RcPrincipal);

			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT NNUMERI1,NNUMERI2 FROM SCONSGLO WHERE GCONSGLO='CONTCADU'", mbPrincipal.RcPrincipal);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				lDias_para_Caducar = (Convert.IsDBNull(RR.Tables[0].Rows[0]["NNUMERI1"])) ? 0 : Convert.ToDouble(RR.Tables[0].Rows[0]["NNUMERI1"]);
				lDias_desde_avisar = (Convert.IsDBNull(RR.Tables[0].Rows[0]["NNUMERI2"])) ? 0 : Convert.ToDouble(RR.Tables[0].Rows[0]["NNUMERI2"]);
			}
			
			RR.Close();

			int RESP = 0;
			System.DateTime fechaActual = DateTime.FromOADate(0); //DIEGO 01-06-2006
			if (lDias_para_Caducar != 0 && lDias_desde_avisar != 0)
			{
				sql = "select FCONTCAD, GETDATE() AS FECHA_ACTUAL from susuario WHERE";
				if (PbLargo)
				{
					sql = sql + " gusularg ='" + PstUsuario + "'";
				}
				else
				{
					sql = sql + " gusuario ='" + PstUsuario + "'";
				}
				sql = sql + " and vpassword='" + PstContrase�a + "'";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
				RR = new DataSet();
				tempAdapter_2.Fill(RR);

				//comprobamos si la clave ha caducado.
				//En este caso, no se le permite continuar hasta que cambie la password
				fechaActual = DateTime.FromOADate(0);
				fechaActual = Convert.ToDateTime(RR.Tables[0].Rows[0]["FECHA_ACTUAL"]); //DIEGO 01-06-2006
				FechaCaducidad = Convert.ToDateTime(RR.Tables[0].Rows[0]["FCONTCAD"]).AddDays(lDias_para_Caducar - 1);
				if (FechaCaducidad < fechaActual)
				{ //Date 'DIEGO 01-06-2006
                    RESP = (int)RadMessageBox.Show("Atenci�n: " + Environment.NewLine +
                           "La contrase�a del usuario " + PstUsuario + " ha caducado." + Environment.NewLine + Environment.NewLine +
                           "� Desea cambiarla ?.", "Contrase�a Caducada", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button1);

					if (RESP == ((int) System.Windows.Forms.DialogResult.Yes))
					{
						S02000F1 tempLoadForm = S02000F1.DefInstance;
						S02000F1.DefInstance.cbCancelar.Visible = false;
						S02000F1.DefInstance.lbNombreUsuario.Text = PstUsuario;
						S02000F1.DefInstance.ShowDialog();
						return;
					}
					else
					{
						GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 2);
						Environment.Exit(0);
					}
				}
				//--------

				//Comprobamos que quedan NNUMERI2 o menos dias para que caduque la password
				// y avisamos al usuario el numerode dias restantes para que caudque.

				//If DateDiff("d", Date, FechaCaducidad) + 1 <= lDias_desde_avisar Then
               
                if (Math.Ceiling((FechaCaducidad - fechaActual).TotalDays) +1 <= lDias_desde_avisar)
                {
					//If DateDiff("d", Date, FechaCaducidad) = 0 Then
					if (Math.Ceiling((FechaCaducidad - fechaActual).TotalDays) == 0)
                    {
                        RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
						                "La contrase�a del usuario " + PstUsuario + " caduca HOY." + Environment.NewLine + Environment.NewLine + 
						                "Recuerde que tiene que cambiar la contrase�a antes de que finalize el d�a.", "Cambio de Contrase�a", MessageBoxButtons.OK, RadMessageIcon.Error);
					}
					else
					{
                        RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
						                "La contrase�a del usuario " + PstUsuario + " caduca en " + ((int) Math.Ceiling((FechaCaducidad - fechaActual).TotalDays)).ToString() + " D�as." + Environment.NewLine + Environment.NewLine + 
						                "Recuerde que tiene que cambiar la contrase�a antes de que �sta caduque el pr�ximo " + FechaCaducidad.ToString("dd/MM/yyyy"), "Cambio de Contrase�a", MessageBoxButtons.OK, RadMessageIcon.Error);
					}
				}
				//---------				
				RR.Close();
			}
		}

		//Incidencias de acceso
		public void GrabaIncidencia(string pUsuario, string PModulo, int PIncidencia)
		{
			string strMotivo = String.Empty;
			DataSet RR = null;

			string strPermiso = "N";
			switch(PIncidencia)
			{
				case 0 :  // No incidencia 
					strMotivo = ""; 
					strPermiso = "S"; 
					//Case 1 'Contrase�a no v�lida 
					//    strMotivo = "CONTRASE�A NO V�LIDA" 
					break;
				case 2 :  //Contrase�a caducada 
					strMotivo = "CONTRASE�A CADUCADA"; 
					break;
				case 3 :  //Usuario no autorizado 
					strMotivo = "USUARIO NO AUTORIZADO"; 
					break;
				case 4 :  //Modulo no disponible 
					strMotivo = "MODULO NO DISPONIBLE"; 
					break;
			}

			if (!mbPrincipal.fConexion)
			{
				//If EstablecerConexion = False Then
				if (!mbPrincipal.EstablecerConexion(mbAcceso.gstServerActual, mbAcceso.gstDriver, mbAcceso.gstBasedatosActual, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
                    RadMessageBox.Show("Error de conexi�n", Application.ProductName);
				}

			}
			if (mbPrincipal.fConexion)
			{
				SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM SUSUACCE WHERE 1=2", mbPrincipal.RcPrincipal);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				//select vacia
				RR.AddNew();
				RR.Tables[0].Rows[0]["GUSUARIO"] = pUsuario;
				RR.Tables[0].Rows[0]["FACCESO"] = DateTime.Now;
				RR.Tables[0].Rows[0]["GMODULO"] = PModulo;
				RR.Tables[0].Rows[0]["IPERMISO"] = strPermiso;
				RR.Tables[0].Rows[0]["DDENEGAD"] = strMotivo;
				string tempQuery = RR.Tables[0].TableName;                
                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);                
                tempAdapter.Update(RR, tempQuery);
				RR.Close();
			}
		}
		//--------------------------

		//Oscar: redireccionamiento de BD la 1� vez que se entre a la aplicacion.
		public object Redirecciona_BD()
		{
            DataSet RR = null;
            string sql = String.Empty;

            //On Error Resume Next
            try
            {
                sql = "SELECT SERVIDOR,BASEDEDATOS FROM SUSUBASE WHERE gusuario='" + mbPrincipal.vstUsuario + "'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
                RR = new DataSet();
                tempAdapter.Fill(RR);

                if (RR.Tables[0].Rows.Count != 0)
                {
                    if (Convert.ToString(RR.Tables[0].Rows[0]["SERVIDOR"]) != "" && Convert.ToString(RR.Tables[0].Rows[0]["BASEDEDATOS"]) != "")
                    {
                        S03000F1 tempLoadForm = new S03000F1();
                        tempLoadForm.Show();
                        tempLoadForm.Hide();
                        Application.DoEvents();
                                                                        
                        for (int X = 1; X <= mbPrincipal.Tabla_BD.GetUpperBound(1); X++)
                        {
                            //Debug.Print UCase(Tabla_BD(1, x)) & " " & UCase(Tabla_BD(2, x))
                            if (mbPrincipal.Tabla_BD[1, X].ToUpper() == Convert.ToString(RR.Tables[0].Rows[0]["BASEDEDATOS"]).Trim().ToUpper() && mbPrincipal.Tabla_BD[2, X].ToUpper() == Convert.ToString(RR.Tables[0].Rows[0]["SERVIDOR"]).Trim().ToUpper())
                            {
                                tempLoadForm.CmbBD.SelectedIndex = X - 1;                                
                                tempLoadForm.cbAceptar_Click(tempLoadForm.cbAceptar, new EventArgs());
                                break;
                            }
                        }
                        tempLoadForm.Close();
                    }
                }                
                RR.Close();
                mbPrincipal.PrimerAcceso = false;
            }
            catch (SqlException ex)
            {
                // INDRA - ralopezn - 15/03/2016
                // se aprovecha de la clase RDO_rdoErros para implementar el tratamiento de errores.
                var errors = new RDO_rdoErrors(ex);
                if (errors.getCount() != 0)
                {
                    if (errors.Item(1).Number == 208)
                    {
                        mbPrincipal.PrimerAcceso = false;
                        return null;
                    }
                }
            }
			return null;
		}


		public void GrabaAcceso(string pUsuario)
		{
			//Solo se grabara en esta tabla cuando se logre conectar a la B.D sin incidencias.

			int sid = 0;

			string usuarioNT = Serrores.obtener_usuario();

			Conexion.Obtener_conexion Instancia = new Conexion.Obtener_conexion();

			int pid = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetCurrentProcessId();

			Exception ex = null;
			int ret = 0;
			string tstNombreMaq = String.Empty;
			string sql = String.Empty;
			SqlDataAdapter tempAdapter = null;
			DataSet RR = null;
			string tempQuery = String.Empty;
			//SqlDataAdapter tempAdapter_2 = null;
			SqlCommandBuilder tempCommandBuilder = null;
			ErrorHandlingHelper.ResumeNext(out ex, 
				() => {ret = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.ProcessIdToSessionId(pid, ref sid);});

			if (ErrorHandlingHelper.ResumeNextExpr<bool>(() => {return Information.Err().Number == 453;}))
				{
					sid = 0;
				}
				ErrorHandlingHelper.ResumeNext(out ex, 
						//Nota: Este error ocurriria si nuestro Kernel32.dll no tiene la funcion
						//      ProcessIdToSessionId para gestionar funciones Terminal Server

					() => {tstNombreMaq = Instancia.NonMaquina().Trim();}, 
					() => {Instancia = null;});

				if (tstNombreMaq == "")
				{
					ErrorHandlingHelper.ResumeNext(out ex, 
						() => {tstNombreMaq = pUsuario;});
				}
				ErrorHandlingHelper.ResumeNext(out ex, 

					() => {sql = "SELECT * FROM SSESUSUA WHERE " + 
					             "IDCONEXI=" + sid.ToString() + 
					             " AND DUSUARNT='" + usuarioNT + "' " + 
					             " AND PUESTOPC='" + tstNombreMaq + "'" + 
					             " and gusuario = '" + pUsuario + "'";}, 

					() => {tempAdapter = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);}, 
					() => {RR = new DataSet();}, 
					() => {tempAdapter.Fill(RR);});

				if (RR.Tables[0].Rows.Count == 0)
				{
					ErrorHandlingHelper.ResumeNext(out ex, 
						() => {RR.AddNew();});
				}
				else
				{
					ErrorHandlingHelper.ResumeNext(out ex, 
							() => {RR.Edit();});
				}
				ErrorHandlingHelper.ResumeNext(out ex, 
					() => {RR.Tables[0].Rows[0]["IDCONEXI"] = sid;}, 
					() => {RR.Tables[0].Rows[0]["GUSUARIO"] = pUsuario;}, 
					() => {RR.Tables[0].Rows[0]["BASEDATO"] = mbAcceso.gstBasedatosActual;}, 
					() => {RR.Tables[0].Rows[0]["SERVIDOR"] = mbAcceso.gstServerActual;}, 
					() => {RR.Tables[0].Rows[0]["USUARIBD"] = mbAcceso.gstUsuarioBD;}, 
					() => {RR.Tables[0].Rows[0]["PWBASEDA"] = mbPrincipal.EncriptarPassword(ref mbAcceso.gstPasswordBD);}, 
					() => {RR.Tables[0].Rows[0]["DUSUARNT"] = usuarioNT;}, 
					() => {RR.Tables[0].Rows[0]["PUESTOPC"] = tstNombreMaq;},  //Instancia.NonMaquina
					() => {RR.Tables[0].Rows[0]["DDRIVER"] = mbAcceso.gstDriver;}, 
					() => {tempQuery = RR.Tables[0].TableName;},
                    () => { tempCommandBuilder = new SqlCommandBuilder(tempAdapter); },
                    () => { tempAdapter.Update(RR, tempQuery); }, 					
					() => {RR.Close();});

				return;
				if (ex != null)
				{
					ErrorHandlingHelper.ResumeNext(

						() => {RadMessageBox.Show("ERROR en Acceso. " + Environment.NewLine + 
						 "Consulte con su Administrador", "Gesti�n Asistencial y Departamental", MessageBoxButtons.OK, RadMessageIcon.Error);}, 
						() => {Environment.Exit(0);});
				}
			}

			private void tbUsuario_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
			{
					int KeyAscii = Strings.Asc(eventArgs.KeyChar);
					if (KeyAscii == 39)
					{
						KeyAscii = Serrores.SustituirComilla();
					}
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					eventArgs.KeyChar = Convert.ToChar(KeyAscii);
			}

			//Oscar C Julio 2007
			//En funcion de la constante global NROINTEN, se desactivara al usuario cuando se falle mas de x
			//veces el intento de login.
			private void Desactivar_Usuario(string pUsuario)
			{
					string sql = String.Empty;
					if (Serrores.ObternerValor_CTEGLOBAL(mbPrincipal.RcPrincipal, "NROINTEN", "VALFANU1") == "S")
					{
						sql = "UPDATE SUSUARIO set fvalidez=getdate() where ";
						if (mbPrincipal.bLargo)
						{
							sql = sql + " gusularg ='" + pUsuario + "'";
						}
						else
						{
							sql = sql + " gusuario ='" + pUsuario + "'";
						}
						SqlCommand tempCommand = new SqlCommand(sql, mbPrincipal.RcPrincipal);
						tempCommand.ExecuteNonQuery();
						short tempRefParam = 1420;
						string[] tempRefParam2 = new string[]{"El usuario " + pUsuario, " desactivado. Contacte con el Administrador del Sistema"};
						mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbPrincipal.RcPrincipal, tempRefParam2);
					}
			}
			
			private void S01000F1_Closed(Object eventSender, EventArgs eventArgs)
			{
                S01000F1.DefInstance.Dispose();
			}
		}
}