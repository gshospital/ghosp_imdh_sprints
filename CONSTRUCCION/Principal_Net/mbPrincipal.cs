using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Configuration;
using Telerik.WinControls;
using ElementosCompartidos;

namespace Principal
{
	internal static class mbPrincipal
	{
        //DGMORENOG
        private const string KEY = "IMHHOSPITAL_INDRAC0L0MB1@";
        private static readonly byte[] Salt = Encoding.ASCII.GetBytes("?pt1$8f]l4g80");
        private const Int32 ITERATIONS = 1042;
        public static AesManaged _MyAes = new AesManaged();
       
        public static AesManaged MyAes
        {
            get
            {
                return _MyAes;
            }
            set
            {
                _MyAes = value;
            }
        }
        //DGMORENOG

        //oscar
        public static bool PrimerAcceso = false;
		public static string[, ] Tabla_BD = null;
		public static bool bUsuarioRedireccionado = false;
		public static dynamic ControlPantalla = null;

		public static string gstBasedatosInicial = String.Empty;
		public static string gstServerInicial = String.Empty;
		public static string gstUsuarioBDInicial = String.Empty;
		public static string gstpasswordBDInicial = String.Empty;

		//variables para saber cual es la base de datos activa justo antes
		//de realizar el cambio de base de datos, para poder volver a ella
		//en la pantalla de verificacion de usuario y contrase�a en el caso
		//de que se pulsara el boton cancelar, porque una vez que se llega a esta
		//pantalla desde el cambio de base de datos, la conexion se cambia a la nueva
		//para verificar el usuario.
		public static bool AccedoLoginMedianteCambioBD = false;
		public static string gstBasedatosAC = String.Empty;
		public static string gstServerAC = String.Empty;
		public static string gstUsuarioBDAC = String.Empty;
		public static string gstpasswordBDAC = String.Empty;
		//-----

		public static string DSN_SQLserver = String.Empty;
		public static string dsn_sqlaccess = String.Empty;
		public static bool VstError = false;
		public static string VstBaseInicial = String.Empty;
		public static string VstServidorInicial = String.Empty;
		public static bool ObligaAactualizar = false;
		public static bool SalirActualizacion = false;
		
		public static object RePrincipal = null;
		public static SqlConnection RcPrincipal = null;
		public static DataSet RsPrincipal = null;
		
		public static bool ActivarBloqueo = false;

		public static string vstUsuario = String.Empty;
		public static string vstContrase�a = String.Empty;

		public static bool fFormCargado = false;
		public static bool fConexion = false;
		public static string vstUsuario_NT = String.Empty;

		public static IMDH00F1 FormularioMDI = null;

		public static mbAcceso.strControlOculto[] astrControles = null;
		public static mbAcceso.strEventoNulo[] astrEventos = null;

		public static string vstServer = String.Empty;
		public static string vstDriver = String.Empty;
		public static string vstBase = String.Empty;

		public static Mensajes.ClassMensajes Mensaje = null;
		public static int Cual = 0;
		public static FixedLengthString vstModo = new FixedLengthString(1);
		public static bool gbSalir = false;
				
		public static int iContador = 0;
		public static int viNuevoTop = 0;
		public static int vinuevoLeft = 0;

		public const int COiSuperior = 531; //360
		public const int COiLateral = 805; //1440

		
		public const int SYNCHRONIZE = 0x100000;
		public const int WM_CLOSE = 0x10;
		public const int WM_SYSCOMMAND = 0x112;
		public const int SC_CLOSE = 0xF060;
		public const int wait_timeout = 0x102;
		public static double UltimaVersion = 0;
		public static double NuevaVersion = 0;
		public static string Directorio = String.Empty;
		public static string Fichero = String.Empty;
		public static string Servidor = String.Empty;
		public static string vPathAplicacion = String.Empty;

		//****indicador de usuario largo
		public static bool bLargo = false;
		public static string vstUsuarioLargo = String.Empty;
		
		public const int PROCESS_QUERY_INFORMATION = 0x400;
		public const int STILL_ACTIVE = 0x103;
		public const int PROCESS_CREATE_THREAD = 0x2;
		public const int PROCESS_VM_OPERATION = 0x8;
		public const int PROCESS_VM_READ = 0x10;
		public const int PROCESS_VM_WRITE = 0x20;
		public const int PROCESS_DUP_HANDLE = 0x40;
		public const int PROCESS_CREATE_PROCESS = 0x80;
		public const int PROCESS_SET_QUOTA = 0x100;
		public const int PROCESS_SET_INFORMATION = 0x200;
		public const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
		public static readonly int PROCESS_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0xFFF;

		public static dynamic objBloqueo = null;

		internal static void CrearInicial()
		{
			//'If fnQueryRegistro2("SERVIDOR2") = "Mal leido" Then
			//'    'crear claves
			//'    VstServidorInicial = Trim(fnQueryRegistro("SERVIDOR"))
			//'    VstBaseInicial = Trim(fnQueryRegistro("BASEDATOS"))
			//'    SetValue "SOFTWARE\INDRA\GHOSP", "BASEDATOS2", VstBaseInicial, 1
			//'    SetValue "SOFTWARE\INDRA\GHOSP", "SERVIDOR2", VstServidorInicial, 1
			//'Else
			//'    'establecer inicial
			//'    VstServidorInicial = Trim(fnQueryRegistro("SERVIDOR2"))
			//'    VstBaseInicial = Trim(fnQueryRegistro("BASEDATOS2"))
			//'    SetValue "SOFTWARE\INDRA\GHOSP", "BASEDATOS", VstBaseInicial, 1
			//'    SetValue "SOFTWARE\INDRA\GHOSP", "SERVIDOR", VstServidorInicial, 1
			//'End If

			//compruebo que existen las entradas para formato de fecha
			if (fnQueryRegistro2("TIPOBD") == "Mal leido")
			{
				SetValue("SOFTWARE\\INDRA\\GHOSP", "TIPOBD", "SQLSERVER", 1);
			}
			//comprobar que existe la entrada del servidor de aplicaiones
			if (fnQueryRegistro2("SERVIDORAPLICACION") == "Mal leido")
			{
				SetValue("SOFTWARE\\INDRA\\GHOSP", "SERVIDORAPLICACION", VstServidorInicial, 1);
			}

			//oscar
			if (fnQueryRegistro2("INICIO") == "Mal leido")
			{
				SetValue("SOFTWARE\\INDRA\\GHOSP", "INICIO", "GHOSP", 1);
			}
			if (fnQueryRegistro2("INICIO_PWD") == "Mal leido")
			{
				string tempRefParam = "INDRA";
				SetValue("SOFTWARE\\INDRA\\GHOSP", "INICIO_PWD", EncriptarPassword(ref tempRefParam), 1);
			}
			//-------

            ///DGMORENOG-VARIABLES FALTANTES EN EL REGISTRO            
            if (fnQueryRegistro2("DRIVER") == "Mal leido")                      
                SetValue("", "DRIVER", "{SQL SERVER}", 1);
                        
            if (fnQueryRegistro2("BASEDATOS") == "Mal leido")            
                SetValue("", "BASEDATOS", "SIA_TEST", 1);
            
            if (fnQueryRegistro2("SERVIDOR") == "Mal leido")            
                SetValue("", "SERVIDOR", "CURIAE.cloudapp.net", 1);
            
            if (fnQueryRegistro2("FICHERO") == "Mal leido")                            
                SetValue("", "FICHERO", "RUTA FICHERO", 1);            
            // FIN DGMORENOG
		}

		internal static string LeerNuevaVersion(string Fichero)
		{
			string result = String.Empty;
			StringBuilder caracter = new StringBuilder();
			string tmpChar = String.Empty;
			string FicheroVersion = String.Empty;
			try
			{
				FicheroVersion = "\\\\" + Servidor + "\\" + Directorio + "\\" + Fichero + ".log";
				caracter = new StringBuilder("");
				FileSystem.FileOpen(1, FicheroVersion, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);

				while(!FileSystem.EOF(1))
				{
					tmpChar = FileSystem.InputString(1, 1);
					if ((String.CompareOrdinal(tmpChar, "0") >= 0 && String.CompareOrdinal(tmpChar, "9") <= 0) || tmpChar == ".")
					{
						caracter.Append(tmpChar);
					}
					//    caracter = caracter & Input(1, #1)
				};
				FileSystem.FileClose(1);
				//LeerNuevaVersion = Val(caracter)
				return caracter.ToString();
			}
			catch (System.Exception excep)
			{
				result = "";
				VstError = true;
				string tempRefParam = (excep.Message);
				Grabarerror(ref tempRefParam);
				return result;
			}
		}

		private static string ServAplicacion()
		{
            /* ralopezn_NOTODO_X_1: CF004

			//devuelve cual es el servidor de aplicaciones
			string result = String.Empty;
			string miIp = String.Empty;
			StringBuilder cadenaIp = new StringBuilder();
			int digitos = 0;
			string tmpCadena = String.Empty;
			string sql = String.Empty;


			string tst = "select valfanu1 from sconsglo where gconsglo='SERAPLIC'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tst, RcPrincipal);
			DataSet rsTemp = new DataSet();
			tempAdapter.Fill(rsTemp);
			if (rsTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(rsTemp.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper();
			}

			
			rsTemp.Close();

			if (result == "NO ACTUALIZAR")
			{
				return result;
			}

			object miObj = null;
			if (Serrores.ObtenerValor_CTEGLOBAL(RcPrincipal, "IACTUAIP", "valfanu1") == "S")
			{
                
				miObj = new ProcesoIP.clsProcesoIP();

				//UPGRADE_TODO: (1067) Member RecuperarIP is not defined in type object. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				miIp = Convert.ToString(miObj.RecuperarIP());
				if (miIp != "")
				{
					digitos = 0;
					tmpCadena = "";
					for (int contadorFor = 1; contadorFor <= miIp.Length; contadorFor++)
					{
						if (miIp.Substring(contadorFor - 1, Math.Min(1, miIp.Length - (contadorFor - 1))) == ".")
						{
							if (digitos == 3)
							{
								cadenaIp.Append(tmpCadena + miIp.Substring(contadorFor - 1, Math.Min(1, miIp.Length - (contadorFor - 1))));
							}
							else
							{
								cadenaIp.Append(StringsHelper.Format(tmpCadena, "000") + miIp.Substring(contadorFor - 1, Math.Min(1, miIp.Length - (contadorFor - 1))));
							}
							tmpCadena = "";
							digitos = 0;
						}
						else if (miIp.Length == contadorFor)
						{ 
							tmpCadena = tmpCadena + miIp.Substring(contadorFor - 1, Math.Min(1, miIp.Length - (contadorFor - 1)));
							cadenaIp.Append(StringsHelper.Format(tmpCadena, "000"));
						}
						else
						{
							tmpCadena = tmpCadena + miIp.Substring(contadorFor - 1, Math.Min(1, miIp.Length - (contadorFor - 1)));
							//'cadenaIp = Mid(miIp, contadorFor, 1)
							digitos++;
						}
					}
					sql = "SELECT ISNULL(dpathins, '') as dpathins FROM sdatomaq WHERE gipinici <= '" + cadenaIp.ToString() + "' AND gipfinal >= '" + cadenaIp.ToString() + "'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcPrincipal);
					rsTemp = new DataSet();
					tempAdapter_2.Fill(rsTemp);
					if (rsTemp.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(rsTemp.Tables[0].Rows[0]["dpathins"]) != "")
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							result = Convert.ToString(rsTemp.Tables[0].Rows[0]["dpathins"]);
							SetValue("SOFTWARE\\INDRA\\GHOSP", "SERVIDORAPLICACION", result, 1);
						}
					}
					
					rsTemp.Close();
				}
			}

			return result;
            */
            return "NO ACTUALIZAR";
		}

		internal static void VerificarModoConexion()
		{
			string sql = "select valfanu1 from SCONSGLO where gconsglo='MODCONEX'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcPrincipal);
			RsPrincipal = new DataSet();
			tempAdapter.Fill(RsPrincipal);			
			vstModo.Value = Convert.ToString(RsPrincipal.Tables[0].Rows[0][0]);			
			RsPrincipal.Close();
		}

		internal static string DesencriptarPassword(string stContrase�a)
		{
			int[] arraydesp = new int[9];
			int[] entrada = new int[9];
			StringBuilder stDevolucion = new StringBuilder();

			arraydesp[1] = 10;
			arraydesp[2] = 13;
			arraydesp[3] = 31;
			arraydesp[4] = 14;
			arraydesp[5] = 1;
			arraydesp[6] = 22;
			arraydesp[7] = 34;
			arraydesp[8] = 8;

			//A PARTIR DE AQUI, LA DESENCRIPTACION
			for (int I = 1; I <= 8; I++)
			{
				entrada[I] = Strings.Asc(stContrase�a.Substring(I - 1, Math.Min(1, stContrase�a.Length - (I - 1)))[0]);
				if (entrada[I] == 32)
				{
					entrada[I] = 1;
				}
				else
				{
					if (entrada[I] >= 48 && entrada[I] <= 57)
					{
						entrada[I] -= 46;
					}
					else
					{
						if (entrada[I] >= 65 && entrada[I] <= 90)
						{
							entrada[I] -= 53;
						}
						else
						{
							entrada[I] = 38;
						}
					}
				}
				entrada[I] -= arraydesp[I];
				if (entrada[I] < 0)
				{
					entrada[I] += 38;
				}
				if (entrada[I] == 1)
				{
					entrada[I] = 32;
				}
				else
				{
					if (entrada[I] >= 2 && entrada[I] <= 11)
					{
						entrada[I] += 46;
					}
					else
					{
						if (entrada[I] >= 12 && entrada[I] <= 37)
						{
							entrada[I] += 53;
						}
						else
						{
							entrada[I] = 209;
						}
					}
				}
				stDevolucion.Append(Strings.Chr(entrada[I]).ToString());

			}

			return stDevolucion.ToString();

		}
		
		[STAThread]
		public static void Main()
		{
			//oscar
			PrimerAcceso = true;
			bUsuarioRedireccionado = false;
            
			//-----
			S01000F1 fLogin = new S01000F1();
			bool esRana = false;
			mbAcceso.vstMetaUsuario = "metausua";
			mbAcceso.vstMetaPassword = "metapass";
			SalirActualizacion = false;


            FormularioMDI = new IMDH00F1();
            
			//*******
			//obtener la base de datos y el servidor de inicio
			//******modificacion acceso con tarjeta 27/11/1998
			//leer la inicial o crearla
			CrearInicial();

			///********************************************/
			//PARA OBTENER LOS DATOS DE LA CONEXI�N DEL PRINCIPAL PODRA HACERSE DE DOS FORMAS
			//SI EN LA L�NEA DE COMANDO DEL PRINCIPAL VIENE ALGO HAR� LA CONEXI�N CON LO QUE VENGA
			//EN ESE COMANDO:
			//   DOS POSIBILIDADES DEL COMANDO:
			//   -TODOS LOS PARAMETROS EN LA L�NEA DE COMANDO:
			//       TENDRA UNA CADENA DE ESTE TIPO
			//           SERVIDOR#DRIVER#BASEDEDATOS#USUARIO#PWD_ENCRIPTADA POR IFMS (sin espcio en blanco)
			//   -RUTA DE ARCHIVO:
			//       TENDRA UNA CADENA DE ESTE TIPO
			//           F
			//ADEMAS SI NO SE PROPORCIONA NINGUNA FORMA, SIEMPRE EXESTIRA lA OPCION DE SACAR LOS
			//PARAMETROS DEL REGISTRO DE WINDOWS.

			//oscar y carlos
			//si tiene algo en el comand saco los valores de el
			string RutaFichero = String.Empty;
			int NroLinea = 0;
			int X = 0;
			string strCadenaParametros = String.Empty;
			string strUP = String.Empty;
			string strSB = String.Empty;

            //INDRA - ralopezn - 13/03/2016
            // Secci�n donde se obtienen los datos de cadena de conexi�n y demas variables para que la aplicaci�n
            // pueda conectarse o validarse contra base de datos.
            #region Lectura de comandos desde los argumentos
            string RecogeCommand = Interaction.Command();


			if (RecogeCommand != "")
			{

				if (RecogeCommand.ToUpper() == "F")
				{
					//Los valores de la conexion se deberan coger de un fichero codificado
					X = 1;
					strCadenaParametros = "";
					RutaFichero = mbDatosRegistro.fnQueryRegistro_I("RutaFichero").Substring(0, Math.Min(mbDatosRegistro.fnQueryRegistro_I("RutaFichero").IndexOf(';'), mbDatosRegistro.fnQueryRegistro_I("RutaFichero").Length));
					NroLinea = Convert.ToInt32(Double.Parse(mbDatosRegistro.fnQueryRegistro_I("RutaFichero").Substring(Math.Max(mbDatosRegistro.fnQueryRegistro_I("RutaFichero").Length - 1, 0))));

					if (FileSystem.Dir(RutaFichero, FileAttribute.Normal) != "")
					{

						FileSystem.FileOpen(1, RutaFichero, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);

						while(!FileSystem.EOF(1))
						{
							strCadenaParametros = FileSystem.LineInput(1);
							if (X == NroLinea)
							{
								break;
							}
							X++;
						};
						FileSystem.FileClose(1);
						strCadenaParametros = Serrores.Decodificar_frase(ref strCadenaParametros);
						strCadenaParametros = strCadenaParametros.Substring(strCadenaParametros.IndexOf(';') + 1);

						strUP = strCadenaParametros.Substring(0, Math.Min(strCadenaParametros.IndexOf(';'), strCadenaParametros.Length));
						X = (strCadenaParametros.IndexOf(';') + 1);

						strSB = strCadenaParametros.Substring(X, Math.Min(Strings.InStr(X + 1, strCadenaParametros, ";", CompareMethod.Binary) - X - 1, strCadenaParametros.Length - X));
						X = Strings.InStr(X + 1, strCadenaParametros, ";", CompareMethod.Binary);

						mbAcceso.gstDriver = strCadenaParametros.Substring(X, Math.Min(Strings.InStr(X + 1, strCadenaParametros, ";", CompareMethod.Binary) - X - 1, strCadenaParametros.Length - X));
						mbAcceso.gstUsuarioBD = strUP.Substring(0, Math.Min(strUP.IndexOf('/'), strUP.Length));
						mbAcceso.gstPasswordBD = strUP.Substring(strUP.IndexOf('/') + 1);
						mbAcceso.gstBasedatosActual = strSB.Substring(strSB.IndexOf('/') + 1);
						mbAcceso.gstServerActual = strSB.Substring(0, Math.Min(strSB.IndexOf('/'), strSB.Length));


					}
					else
					{
						RadMessageBox.Show("ATENCION:" + Environment.NewLine + 
						                "No se ha encontrado el fichero " + RutaFichero + "." + Environment.NewLine + 
						                "Aseg�rese que el fichero se encuentra en la ruta especificada.", "Error de acceso al fichero de configuraci�n", MessageBoxButtons.OK, RadMessageIcon.Error);
						Environment.Exit(0);
					}
				}
				else
				{

					//StComa = InStr(1, RecogeCommand, ",")
					//COMENTO ESTA LINEA 30/10/2002 CARLOS
					//vstUsuario = Mid(RecogeCommand, 1, StComa - 1)

					//oscar
					//staster = InStr(1, RecogeCommand, "*")
					//TroceoDSN_PRINCIPAL Mid(RecogeCommand, staster + 1)
					string tempRefParam = RecogeCommand.Trim();
					TroceoDSN_PRINCIPAL(ref tempRefParam);

				}
			}
			else
			{
				mbAcceso.gstUsuarioBD = mbDatosRegistro.fnQueryRegistro("INICIO");
				mbAcceso.gstPasswordBD = DesencriptarPassword(mbDatosRegistro.fnQueryRegistro("INICIO_PWD").Trim()).Trim();
				//Encriptacion IFMS:
				//gstPasswordBD = trim(Decodificar_frase(Trim(fnQueryRegistro("INICIO_PWD")) & " "))
				mbAcceso.gstDriver = mbDatosRegistro.fnQueryRegistro("DRIVER");
				mbAcceso.gstBasedatosActual = mbDatosRegistro.fnQueryRegistro("BASEDATOS");
				mbAcceso.gstServerActual = mbDatosRegistro.fnQueryRegistro("SERVIDOR");
            }
            #endregion

            gstBasedatosInicial = mbAcceso.gstBasedatosActual;
			gstServerInicial = mbAcceso.gstServerActual;
			gstUsuarioBDInicial = mbAcceso.gstUsuarioBD;
			gstpasswordBDInicial = mbAcceso.gstPasswordBD;
			//----

			//OJO: Estas variables globales empleadas en S03000F1.frm
			//     se cargaban con los valores del registro en la funcion
			//     CrearInicial, independientemente
			//     de la forma de conexion: fichero, command, o registro
			VstServidorInicial = gstServerInicial;
			VstBaseInicial = gstBasedatosInicial;
			//----

			//If EstablecerConexion = False Then
			if (!EstablecerConexion(gstServerInicial, mbAcceso.gstDriver, gstBasedatosInicial, gstUsuarioBDInicial, gstpasswordBDInicial))
			{
				RadMessageBox.Show("Error en conexi�n ", Application.ProductName);
				Environment.Exit(0);
			}


			bool nImagen = false;
			string stSql2 = "SELECT * FROM SCONSGLO WHERE GCONSGLO='NUEVAIMG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql2, RcPrincipal);
			DataSet cursor = new DataSet();
			tempAdapter.Fill(cursor);
			if (cursor.Tables[0].Rows.Count == 0)
			{
				nImagen = false;
				esRana = false;
			}
			else
			{
				nImagen = Convert.ToString(cursor.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S";
				esRana = Convert.ToString(cursor.Tables[0].Rows[0]["VALFANU2"]).Trim().ToUpper() == "F";
			}
			
			cursor.Close();

			if ((Screen.PrimaryScreen.Bounds.Height * 15 / 15) > 600 && (Screen.PrimaryScreen.Bounds.Width * 15 / 15) > 800)
			{
				if (nImagen && esRana)
				{
                    ControlPantalla = new IMDH00F3();
				}
				else
				{
					ControlPantalla = new IMDH00F2();
				}
			}
			else
			{
                ControlPantalla = new IMDH00F2();
			}
            

			//oscar
			//gstBasedatosActual = fnQueryRegistro("BASEDATOS")
			//gstServerActual = fnQueryRegistro("SERVIDOR")
			//-----

			// Vemos si es necesario bloquear


			stSql2 = "select isNull(valfanu1, 'N') bloqueo from SCONSGLO where " + 
			         "gconsglo = 'IBLOQUEO'";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql2, RcPrincipal);
			DataSet RrDatos = new DataSet();
			tempAdapter_2.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count == 0)
			{
				ActivarBloqueo = false;
			}
			else
			{				
				ActivarBloqueo = Convert.ToString(RrDatos.Tables[0].Rows[0]["bloqueo"]).Trim().ToUpper() == "S";
			}

			
			RrDatos.Close();

			RrDatos = null;


			ObligaAactualizar = true;
			Mensaje = new Mensajes.ClassMensajes();
			fConexion = true;

			fLogin.fTarjeta = false;
            fLogin.ShowDialog(FormularioMDI);

			if (!fLogin.fOK)
			{
				//Error al iniciar, la aplicaci�n terminar�
				//        RcPrincipal.Close
				//        RePrincipal.Close
				Environment.Exit(0);
			}
            Application.Run(FormularioMDI);
		}

		private static void TroceoDSN_PRINCIPAL(ref string manejador)
		{
			//Nota:
			//El manejador debera de ser del tipo:
			//SRVSAN2000#{SQL SERVER}#IDC_TOLEDO#GHOSP#INDRA

			//gstServerActual = Mid(manejador, 1, InStr(1, manejador, "#") - 1)
			//ti = InStr(1, manejador, "#")
			//gstDriver = Mid(manejador, ti + 1, InStr(ti + 1, manejador, "#") - ti - 1)
			//ti = InStr(ti + 1, manejador, "#")
			//gstBasedatosActual = Trim(Mid(manejador, ti + 1))
			//gstUsuarioBD = "GHOSP"
			//gstPasswordBD = "INDRA"

			mbAcceso.gstServerActual = manejador.Substring(0, Math.Min(manejador.IndexOf('#'), manejador.Length));
			int ti = (manejador.IndexOf('#') + 1);
			mbAcceso.gstDriver = manejador.Substring(ti, Math.Min(Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary) - ti - 1, manejador.Length - ti));
			ti = Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary);
			mbAcceso.gstBasedatosActual = manejador.Substring(ti, Math.Min(Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary) - ti - 1, manejador.Length - ti));
			ti = Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary);
			mbAcceso.gstUsuarioBD = manejador.Substring(ti, Math.Min(Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary) - ti - 1, manejador.Length - ti));
			ti = Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary);
			mbAcceso.gstPasswordBD = manejador.Substring(ti).Trim();
			string tempRefParam = mbAcceso.gstPasswordBD + " ";
			mbAcceso.gstPasswordBD = Serrores.Decodificar_frase(ref tempRefParam).Trim();

		}

        #region Registro AppConfig

        static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("Salt Is Not A Password");

        public static string EncryptString(System.Security.SecureString input)
        {
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                entropy,
                System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        public static SecureString DecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy,
                    System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        public static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        public static string ToInsecureString(SecureString input)
        {
            string returnValue = string.Empty;
            IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }

        public static void KeyGenerator()
        {
            var key = new Rfc2898DeriveBytes(KEY, Salt, ITERATIONS);
            MyAes.Key = key.GetBytes(MyAes.KeySize / 8);
            MyAes.IV = new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        }

        public static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                plainText=" ";
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an AesManaged object
            // with the specified key and IV.
            KeyGenerator();
            using (MyAes)
            {

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = MyAes.CreateEncryptor(MyAes.Key, MyAes.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.
            return Convert.ToBase64String(encrypted);
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesManaged object
            // with the specified key and IV.
            using (MyAes)
            {
                KeyGenerator();
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = MyAes.CreateDecryptor(MyAes.Key, MyAes.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }
            return plaintext;
        }
        
        public static string GetAppConfig(string stSubKey)
        {
            string valor = string.Empty;
            KeyGenerator();

            try
            {
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = @"App.config";
                Configuration config =
                   ConfigurationManager.OpenMappedExeConfiguration(fileMap,
                   ConfigurationUserLevel.None);
                using (MyAes)
                {
                    if (config.AppSettings.Settings[stSubKey] != null)
                    {
                        valor = config.AppSettings.Settings[stSubKey].Value;
                        //SecureString stValor = DecryptString(valor);

                        ////Si luego de hacer DecryptString(valor) la variable stValor queda con longitud 0 
                        ////es porque el valor no estaba encriptado por lo tanto se encripta el valor
                        //if (stValor.Length == 0)
                        //{
                        //    SetValue("", stSubKey, valor, 1);
                        //    config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                        //    valor = config.AppSettings.Settings[stSubKey].Value;
                        //    stValor = DecryptString(valor);
                        //}

                        //valor = ToInsecureString(stValor);
                        valor = DecryptStringFromBytes_Aes(Convert.FromBase64String(valor), MyAes.Key, MyAes.IV);
                    }
                    else
                    {
                        valor = "Mal leido";
                    }
                }
                return valor;
            }
            catch
            {
                SetValue("",stSubKey, valor,1);
                return valor;
            }
        }
      

		internal static string fnQueryRegistro2(string stSubKey)
		{
            //DGMORENOG - REEMPLAZO DE CONSULTA LLAVES REGISTRO WINDOWS
            return GetAppConfig(stSubKey);            
		}

		internal static void SetValue(string skeyname, string Svaluename, string VValueSetting, int lValueType)
		{
            KeyGenerator();
            using (MyAes)
            {
                //DGMORENOG - REEMPLAZO DE ESCRITURA LLAVES REGISTRO WINDOWS
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = @"App.config";
                Configuration config =
                   ConfigurationManager.OpenMappedExeConfiguration(fileMap,
                   ConfigurationUserLevel.None);
                ConfigurationSection mySection = config.GetSection("appSettings");
                mySection.CurrentConfiguration.AppSettings.Settings.Remove(Svaluename);
                config.Save(ConfigurationSaveMode.Full);
                //mySection.CurrentConfiguration.AppSettings.Settings.Add(Svaluename, EncryptString(ToSecureString(VValueSetting)));
                mySection.CurrentConfiguration.AppSettings.Settings.Add(Svaluename, EncryptStringToBytes_Aes(VValueSetting, MyAes.Key, MyAes.IV));
                config.Save(ConfigurationSaveMode.Full);
            }		
		}

		internal static string EncriptarPassword(ref string stContrase�a)
		{
			int[] arraydesp = new int[9];
			int[] entrada = new int[9];
			string[] salida = ArraysHelper.InitializeArray<string>(9);
			StringBuilder stDevolver = new StringBuilder();

			//el n�mero m�ximo de desplazamiento se debe fijar a 37
			//para que el cociente siempre sea 1 para poder
			//desencriptar partiendo del resto
			//(fijando el cociente siempre a 1).
			arraydesp[1] = 10;
			arraydesp[2] = 13;
			arraydesp[3] = 31;
			arraydesp[4] = 14;
			arraydesp[5] = 1;
			arraydesp[6] = 22;
			arraydesp[7] = 34;
			arraydesp[8] = 8;
			//Pasamos a may�sculas la contrase�a
			stContrase�a = stContrase�a.ToUpper();
			//metemos espacios a la derecha para almacenar como password siempre 8 caracteres
			if (stContrase�a.Length < 8)
			{
				stContrase�a = stContrase�a + new String(' ', 8 - stContrase�a.Length);
			}
			//vamos recortando la contrase�a palabra por palabra
			for (int I = 1; I <= 8; I++)
			{
				entrada[I] = Strings.Asc(stContrase�a.Substring(I - 1, Math.Min(1, stContrase�a.Length - (I - 1)))[0]);
				//se transforman los rangos de caracteres v�lidos en un rango de 1..38
				if (entrada[I] == 32)
				{
					entrada[I] = 1;
				}
				else
				{
					if (entrada[I] >= 48 && entrada[I] <= 57)
					{
						entrada[I] -= 46;
					}
					else
					{
						if (entrada[I] >= 65 && entrada[I] <= 90)
						{
							entrada[I] -= 53;
						}
						else
						{
							entrada[I] = 38;
						}
					}
				}
				//se le suma el desplazamiento y se obtiene el m�dulo 38
				entrada[I] += arraydesp[I];
				entrada[I] = entrada[I] % 38;
				//se transforma el ascii en su caracter
				if (entrada[I] == 1)
				{
					entrada[I] = 32;
				}
				else
				{
					if (entrada[I] >= 2 && entrada[I] <= 11)
					{
						entrada[I] += 46;
					}
					else
					{
						if (entrada[I] >= 12 && entrada[I] <= 37)
						{
							entrada[I] += 53;
						}
						else
						{
							entrada[I] = 209;
						}
					}
				}
				salida[I] = Strings.Chr(entrada[I]).ToString();
				stDevolver.Append(salida[I]);
			}

			return stDevolver.ToString();

		}
        #endregion

        internal static int ObtenerNIntentos()
		{
			//Aqu� tenemos que hacer una llamada a SCONSGLO para determinar
			//el m�ximo n�mero de intentos al poner la contrase�a.
			//De momento...
			int result = 0;
			result = 3;
			return Convert.ToInt32(Conversion.Val(Serrores.ObternerValor_CTEGLOBAL(RcPrincipal, "NROINTEN", "NNUMERI1"))); // 3
		}

		internal static void CERRARCONEXION()
		{
			try
			{				
                RcPrincipal.Close();
				fConexion = false;
			}
			catch
			{
			}
		}

		internal static int ObtenerModo()
		{
			//Posibilidades: "1" -> Modo Trusted
			//"2" -> Modo Habitual
			//"3" -> Modo Habitual por defecto
			//Se llamar� a la tabla SCONSGLO para determinar el modo.
			//De momento...

			return 3;

		}

		//Public Function EstablecerConexion() As Boolean
		internal static bool EstablecerConexion(string PstServer, string PstDriver, string PstBasedatos, string PstUsuarioBD, string PstPasswordBD)
		{
			bool result = false;
			bool fError = false;			
			string stLetra = "P";
			Conexion.Obtener_conexion Instancia = new Conexion.Obtener_conexion();
			//Instancia.Conexion RcPrincipal, stLetra, vstUsuario_NT, fError, gstUsuario, gstPassword
			string tempRefParam = "0";
			string tempRefParam2 = "0";
			string tempRefParam3 = "PRINCIPAL";
			Instancia.Conexion(ref RcPrincipal, stLetra, vstUsuario_NT, ref fError, ref mbAcceso.gstUsuario, ref mbAcceso.gstPassword, PstServer, PstDriver, PstBasedatos, PstUsuarioBD, PstPasswordBD, tempRefParam, tempRefParam2, tempRefParam3, vstUsuario);

			if (!fError)
			{
				result = false;
			}
			else
			{
				result = true;
				Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(RcPrincipal);
				Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();
				Serrores.Obtener_TipoBD_FormatoFechaBD(ref RcPrincipal);
			}
			fConexion = result;
			return result;
		}

		internal static bool NecesitaActualizar()
		{
			bool result = false;
			string stUltimaVersion = String.Empty;
			string stNuevaVersion = String.Empty;

			try
			{
				VstError = false;
				//leer del registro la version
				stUltimaVersion = mbDatosRegistro.fnQueryRegistro("Version");
				UltimaVersion = Serrores.ToDOUBLE(stUltimaVersion);

				//leer del registro donde estan los fuentes
				if (!VstError)
				{
					Directorio = mbDatosRegistro.fnQueryRegistro("Directorio");
				}

				//leer del registro el nombre del fichero para comparar las versiones
				if (!VstError)
				{
					Fichero = mbDatosRegistro.fnQueryRegistro("Fichero");
				}

				if (!VstError)
				{
					//Servidor = fnQueryRegistro("Servidor")
					//COMPRUEBO DE LA BASE DE DATOS CUAL ES EL SERVIDOR DE APLICACIONES
					Servidor = ServAplicacion();

					//OSCAR
					//PODEMOS HACER QUE NO SE ACTUALIZE AUTOMATICAMENTE MEDIANTE LA
					//CONSTANTE GLOBAL SERAPLIC="NO ACTUALIZAR"
					//YA QUE SI NO ESTAN CONECTADOS A LA RED, FALLA.
					if (Servidor == "NO ACTUALIZAR")
					{
						return false;
					}
					//-------------------------------------------------------------

					if (Servidor == "")
					{
						Servidor = mbDatosRegistro.fnQueryRegistro("Servidoraplicacion");
					}
					else
					{
						//actualizo con el �ltimo
						SetValue("SOFTWARE\\INDRA\\GHOSP", "SERVIDORAPLICACION", Servidor, 1);
					}
				}

				//abrir el fichero y ver la version

				stNuevaVersion = LeerNuevaVersion(Fichero);
				NuevaVersion = Serrores.ToDOUBLE(stNuevaVersion);

				if (!VstError)
				{
					result = stUltimaVersion.Trim() != stNuevaVersion.Trim();
				}

				return result;
			}
			catch (System.Exception excep)
			{

				string tempRefParam = (excep.Message);
				Grabarerror(ref tempRefParam);

				return result;
			}
		}

		internal static void Grabarerror(ref string paraTexto, string mok = "")
		{
			paraTexto = "En la m�quina :" + NonMaquina() + " El d�a :" + DateTimeHelper.ToString(DateTime.Today) + " - Error : " + paraTexto;
			if (mok == "")
			{
				RadMessageBox.Show("Pongase en contacto con inform�tica", "Error en actualizaci�n",  MessageBoxButtons.OK, RadMessageIcon.Error);
			}
			if (Directorio != "")
			{
				FileSystem.FileOpen(1, Directorio + "ErrorActualizacion.log", OpenMode.Append, OpenAccess.Default, OpenShare.Default, -1);
				FileSystem.WriteLine(1, paraTexto);
				FileSystem.FileClose(1);
			}
			else
			{
				FileSystem.FileOpen(1, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErrorActualizacion.log", OpenMode.Append, OpenAccess.Default, OpenShare.Default, -1);
				FileSystem.WriteLine(1, paraTexto);
				FileSystem.FileClose(1);
			}
			Environment.Exit(0);
		}

		internal static string NonMaquina()
		{
			StringBuilder result = new StringBuilder();
			FixedLengthString BUF = new FixedLengthString(64);
			int NBUF = 0;
			int X = 0;
			try
			{
				X = 1;
				NBUF = BUF.Value.Length;
				string tempRefParam = BUF.Value;
				NBUF = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetComputerName(ref tempRefParam, ref NBUF);
				BUF.Value = tempRefParam;
				result = new StringBuilder("");
				while (Strings.Asc(BUF.Value.Substring(X - 1, Math.Min(1, BUF.Value.Length - (X - 1)))[0]) != 0)
				{
					result.Append(BUF.Value.Substring(X - 1, Math.Min(1, BUF.Value.Length - (X - 1))));
					X++;
				}
				return result.ToString();
			}
			catch (System.Exception excep)
			{
				string tempRefParam2 = (excep.Message);
				Grabarerror(ref tempRefParam2);
				return result.ToString();
			}
		}
	}
}