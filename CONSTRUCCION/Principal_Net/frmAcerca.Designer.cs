using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class frmAcerca
	{

		#region "Upgrade Support "
		private static frmAcerca m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmAcerca DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmAcerca();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cmdAceptar", "Image2", "Image1", "ImageList1", "lbPremio", "lb4", "lb3", "lb2", "lb1", "lblTexto3", "imgIcono", "lblTexto5", "lblTexto2", "lblTexto1", "lblTexto4", "_Line1_1", "_Line1_0", "Line1", "ShapeContainer1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cmdAceptar;
		public System.Windows.Forms.PictureBox Image2;
		public System.Windows.Forms.PictureBox Image1;
		public System.Windows.Forms.ImageList ImageList1;
        public Telerik.WinControls.UI.RadLabel lbPremio;
        public Telerik.WinControls.UI.RadLabel lb4;
        public Telerik.WinControls.UI.RadLabel lb3;
        public Telerik.WinControls.UI.RadLabel lb2;
        public Telerik.WinControls.UI.RadLabel lb1;
        public Telerik.WinControls.UI.RadLabel lblTexto3;
		public System.Windows.Forms.PictureBox imgIcono;
        public Telerik.WinControls.UI.RadLabel lblTexto5;
        public Telerik.WinControls.UI.RadLabel lblTexto2;
		public Telerik.WinControls.UI.RadLabel lblTexto1;
        public Telerik.WinControls.UI.RadLabel lblTexto4;
		private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_1;
		private Microsoft.VisualBasic.PowerPacks.LineShape _Line1_0;
		public Microsoft.VisualBasic.PowerPacks.LineShape[] Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[2];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAcerca));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this._Line1_1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this._Line1_0 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.cmdAceptar = new Telerik.WinControls.UI.RadButton();
            this.Image2 = new System.Windows.Forms.PictureBox();
            this.Image1 = new System.Windows.Forms.PictureBox();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lbPremio = new Telerik.WinControls.UI.RadLabel();
            this.lb4 = new Telerik.WinControls.UI.RadLabel();
            this.lb3 = new Telerik.WinControls.UI.RadLabel();
            this.lb2 = new Telerik.WinControls.UI.RadLabel();
            this.lb1 = new Telerik.WinControls.UI.RadLabel();
            this.lblTexto3 = new Telerik.WinControls.UI.RadLabel();
            this.imgIcono = new System.Windows.Forms.PictureBox();
            this.lblTexto5 = new Telerik.WinControls.UI.RadLabel();
            this.lblTexto2 = new Telerik.WinControls.UI.RadLabel();
            this.lblTexto1 = new Telerik.WinControls.UI.RadLabel();
            this.lblTexto4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPremio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this._Line1_1,
            this._Line1_0});
            this.ShapeContainer1.Size = new System.Drawing.Size(408, 249);
            this.ShapeContainer1.TabIndex = 12;
            this.ShapeContainer1.TabStop = false;
            // 
            // _Line1_1
            // 
            this._Line1_1.BorderColor = System.Drawing.Color.Gray;
            this._Line1_1.Enabled = false;
            this._Line1_1.Name = "_Line1_1";
            this._Line1_1.X1 = 4;
            this._Line1_1.X2 = 384;
            this._Line1_1.Y1 = 143;
            this._Line1_1.Y2 = 143;
            // 
            // _Line1_0
            // 
            this._Line1_0.BorderColor = System.Drawing.Color.White;
            this._Line1_0.BorderWidth = 2;
            this._Line1_0.Enabled = false;
            this._Line1_0.Name = "_Line1_0";
            this._Line1_0.X1 = 8;
            this._Line1_0.X2 = 378;
            this._Line1_0.Y1 = 100;
            this._Line1_0.Y2 = 100;
            // 
            // cmdAceptar
            // 
            this.cmdAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdAceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdAceptar.Location = new System.Drawing.Point(271, 164);
            this.cmdAceptar.Name = "cmdAceptar";
            this.cmdAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdAceptar.Size = new System.Drawing.Size(132, 23);
            this.cmdAceptar.TabIndex = 0;
            this.cmdAceptar.Text = "Aceptar";
            this.cmdAceptar.Click += new System.EventHandler(this.cmdAceptar_Click);
            // 
            // Image2
            // 
            this.Image2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image2.Image = ((System.Drawing.Image)(resources.GetObject("Image2.Image")));
            this.Image2.Location = new System.Drawing.Point(48, 8);
            this.Image2.Name = "Image2";
            this.Image2.Size = new System.Drawing.Size(32, 27);
            this.Image2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Image2.TabIndex = 1;
            this.Image2.TabStop = false;
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Location = new System.Drawing.Point(26, 267);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(19, 21);
            this.Image1.TabIndex = 2;
            this.Image1.TabStop = false;
            this.Image1.Visible = false;
            this.Image1.Click += new System.EventHandler(this.Image1_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
            this.ImageList1.Images.SetKeyName(0, "");
            // 
            // lbPremio
            // 
            this.lbPremio.AutoSize = false;
            this.lbPremio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPremio.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPremio.ForeColor = System.Drawing.Color.Red;
            this.lbPremio.Location = new System.Drawing.Point(10, 164);
            this.lbPremio.Name = "lbPremio";
            this.lbPremio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPremio.Size = new System.Drawing.Size(1, 1);
            this.lbPremio.TabIndex = 10;
            this.lbPremio.Text = "SORPRESA DESDE I-MDH. ENHORABUENA POR ENCONTRARLA";
            // 
            // lb4
            // 
            this.lb4.AutoSize = false;
            this.lb4.Cursor = System.Windows.Forms.Cursors.Default;
            this.lb4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb4.Location = new System.Drawing.Point(398, 240);
            this.lb4.Name = "lb4";
            this.lb4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb4.Size = new System.Drawing.Size(9, 10);
            this.lb4.TabIndex = 9;
            this.lb4.DoubleClick += new System.EventHandler(this.lb4_DoubleClick);
            // 
            // lb3
            // 
            this.lb3.AutoSize = false;
            this.lb3.Cursor = System.Windows.Forms.Cursors.Default;
            this.lb3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb3.Location = new System.Drawing.Point(0, 240);
            this.lb3.Name = "lb3";
            this.lb3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb3.Size = new System.Drawing.Size(9, 10);
            this.lb3.TabIndex = 8;
            this.lb3.DoubleClick += new System.EventHandler(this.lb3_DoubleClick);
            // 
            // lb2
            // 
            this.lb2.AutoSize = false;
            this.lb2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb2.Location = new System.Drawing.Point(398, 0);
            this.lb2.Name = "lb2";
            this.lb2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb2.Size = new System.Drawing.Size(9, 10);
            this.lb2.TabIndex = 7;
            this.lb2.DoubleClick += new System.EventHandler(this.lb2_DoubleClick);
            // 
            // lb1
            // 
            this.lb1.AutoSize = false;
            this.lb1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lb1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb1.Location = new System.Drawing.Point(0, 0);
            this.lb1.Name = "lb1";
            this.lb1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb1.Size = new System.Drawing.Size(9, 10);
            this.lb1.TabIndex = 6;
            this.lb1.DoubleClick += new System.EventHandler(this.lb1_DoubleClick);
            // 
            // lblTexto3
            // 
            this.lblTexto3.AutoSize = false;
            this.lblTexto3.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTexto3.Location = new System.Drawing.Point(144, 60);
            this.lblTexto3.Name = "lblTexto3";
            this.lblTexto3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTexto3.Size = new System.Drawing.Size(259, 25);
            this.lblTexto3.TabIndex = 5;
            this.lblTexto3.Text = "Copyright � 1997-2002  Indra";
            // 
            // imgIcono
            // 
            this.imgIcono.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgIcono.Image = ((System.Drawing.Image)(resources.GetObject("imgIcono.Image")));
            this.imgIcono.Location = new System.Drawing.Point(16, 40);
            this.imgIcono.Name = "imgIcono";
            this.imgIcono.Size = new System.Drawing.Size(103, 101);
            this.imgIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgIcono.TabIndex = 11;
            this.imgIcono.TabStop = false;
            this.imgIcono.Click += new System.EventHandler(this.imgIcono_Click);
            // 
            // lblTexto5
            // 
            this.lblTexto5.AutoSize = false;
            this.lblTexto5.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTexto5.Location = new System.Drawing.Point(5, 144);
            this.lblTexto5.Name = "lblTexto5";
            this.lblTexto5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTexto5.Size = new System.Drawing.Size(246, 107);
            this.lblTexto5.TabIndex = 4;
            this.lblTexto5.Text = resources.GetString("lblTexto5.Text");
            // 
            // lblTexto2
            // 
            this.lblTexto2.AutoSize = false;
            this.lblTexto2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTexto2.Location = new System.Drawing.Point(144, 26);
            this.lblTexto2.Name = "lblTexto2";
            this.lblTexto2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTexto2.Size = new System.Drawing.Size(259, 31);
            this.lblTexto2.TabIndex = 3;
            this.lblTexto2.Text = "Version 3.0.";
            // 
            // lblTexto1
            // 
            this.lblTexto1.AutoSize = false;
            this.lblTexto1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTexto1.Location = new System.Drawing.Point(144, 4);
            this.lblTexto1.Name = "lblTexto1";
            this.lblTexto1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTexto1.Size = new System.Drawing.Size(247, 16);
            this.lblTexto1.TabIndex = 2;
            // 
            // lblTexto4
            // 
            this.lblTexto4.AutoSize = false;
            this.lblTexto4.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTexto4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexto4.Location = new System.Drawing.Point(144, 94);
            this.lblTexto4.Name = "lblTexto4";
            this.lblTexto4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTexto4.Size = new System.Drawing.Size(259, 46);
            this.lblTexto4.TabIndex = 1;
            // 
            // frmAcerca
            // 
            this.AcceptButton = this.cmdAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdAceptar;
            this.ClientSize = new System.Drawing.Size(408, 249);
            this.ControlBox = false;
            this.Controls.Add(this.cmdAceptar);
            this.Controls.Add(this.Image2);
            this.Controls.Add(this.Image1);
            this.Controls.Add(this.lbPremio);
            this.Controls.Add(this.lb4);
            this.Controls.Add(this.lb3);
            this.Controls.Add(this.lb2);
            this.Controls.Add(this.lb1);
            this.Controls.Add(this.lblTexto3);
            this.Controls.Add(this.imgIcono);
            this.Controls.Add(this.lblTexto5);
            this.Controls.Add(this.lblTexto2);
            this.Controls.Add(this.lblTexto1);
            this.Controls.Add(this.lblTexto4);
            this.Controls.Add(this.ShapeContainer1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(156, 110);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAcerca";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "";
            this.Closed += new System.EventHandler(this.frmAcerca_Closed);
            this.Load += new System.EventHandler(this.frmAcerca_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPremio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTexto4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeLine1();
		}
		void InitializeLine1()
		{
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape[2];
			this.Line1[1] = _Line1_1;
			this.Line1[0] = _Line1_0;
		}
		#endregion
	}
}