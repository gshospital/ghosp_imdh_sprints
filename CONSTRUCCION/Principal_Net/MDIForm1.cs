using ElementosCompartidos;
using Mensajes;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI.Localization;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	internal partial class IMDH00F1
        : Telerik.WinControls.UI.RadForm
	{

		public IMDH00F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				m_vb6FormDefInstance = this;
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            //Localizacion para los mensajes de RadMessageBox
            RadMessageLocalizationProvider.CurrentProvider = new ClassRadMessageLocalizationProvider();
            RadGridLocalizationProvider.CurrentProvider = new ClassRadGridLocalizationProvider();
            MainMenu1.Visible = false;
            Serrores.AbrirConexionServicioFechaHoraDB();
		}


		private void IMDH00F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		
		private void IMDH00F1_Load(Object eventSender, EventArgs eventArgs)
		{
			Process[] processes = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
			if (processes.Length > 1 && Process.GetCurrentProcess().StartTime != processes[0].StartTime)
			{
                RadMessageBox.Show("2030:" + "\r" + "Ya existe una Copia Activa " + "\r" + " de la Aplicación", "Gestión Asistencial y Departamental", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
				Environment.Exit(0);
			}
			mbPrincipal.gbSalir = true;
		}

		private bool isInitializingComponent;
		private void IMDH00F1_Resize(Object eventSender, EventArgs eventArgs)
		{
            /* ralopezn_NOTODO_X_1: CF003
			if (isInitializingComponent)
			{
				return;
			}
			ConfiguracionRegional.clsConfigRegional ObjConfigurarEquipo = null;
			try
			{
				ObjConfigurarEquipo = new ConfiguracionRegional.clsConfigRegional();
			}
			catch
			{
			}
			if (WindowState == FormWindowState.Maximized)
			{
				try
				{ // SI SE MAXIMIZA ACTUALIZAR
                    
					ObjConfigurarEquipo.ConfigurarEquipo();
                   
				}
				catch
				{
				}
			}
            */
        }

        private void IMDH00F1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Serrores.CerrarConexionServicioFechaHoraDB();
        }

        private void IMDH00F1_Activated_1(object sender, EventArgs e)
        {
            MainMenu1.Enabled = true;
        }

        private void IMDH00F1_Deactivate(object sender, EventArgs e)
        {
            MainMenu1.Enabled = false;
        }
        //UPGRADE_NOTE: (7001) The following declaration (Form_Unload) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        //private void Form_Unload(int Cancel)
        //{
        //}
    }
}