using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	internal partial class S03000F1
        : Telerik.WinControls.UI.RadForm
	{
		public S03000F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void S03000F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		string Base_Actual = String.Empty;
		string Usuario_Actual = String.Empty;
		string Servidor_Actual = String.Empty;
		//Dim Tabla_BD() As String

		public void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//oscar
			mbPrincipal.bUsuarioRedireccionado = false;
			//-------

			S01000F1 fLogin = new S01000F1();
			string CONTRASENAENCRIP = String.Empty;

			//cambiar el regedit con la seleccion del combo
			//establecer una nueva conexion con la nueva B.D.
			//verificar si existe el usuario
			//GRABAR EN EL REGEDIT LA NUEVA CONEXION
			string Base_nueva = mbPrincipal.Tabla_BD[1, CmbBD.SelectedIndex + 1].Trim();
			string Servidor_nuevo = mbPrincipal.Tabla_BD[2, CmbBD.SelectedIndex + 1].Trim();

			string UsuarioBD_nuevo = mbPrincipal.Tabla_BD[3, CmbBD.SelectedIndex + 1].Trim();
			string PasswordBD_nuevo = mbPrincipal.Tabla_BD[4, CmbBD.SelectedIndex + 1].Trim();


			//Unload Me
			this.Hide();

			//If EstablecerConexion = False Then
			if (!mbPrincipal.EstablecerConexion(Servidor_nuevo, mbAcceso.gstDriver, Base_nueva, UsuarioBD_nuevo, PasswordBD_nuevo))
			{
				RadMessageBox.Show("Error al establecer la conexi�n con la Base de Datos : " + Base_nueva + "\n" + "\r" + "Se reestablecer� la conexi�n con la Base de Datos : " + Base_Actual.Trim(), Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);

				//If EstablecerConexion = False Then
				string tempRefParam = Servidor_Actual.Trim();
				string tempRefParam2 = Base_Actual.Trim();
				if (!mbPrincipal.EstablecerConexion(tempRefParam, mbAcceso.gstDriver, tempRefParam2, mbAcceso.gstUsuarioBD, mbAcceso.gstPasswordBD))
				{
					RadMessageBox.Show("Error al reestablecer la conexi�n inicial. Contacte con inform�tica.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
					Environment.Exit(0);
				}
				else
				{
					mbAcceso.gstBasedatosActual = Base_Actual.Trim();
					mbAcceso.gstServerActual = Servidor_Actual.Trim();
					mbDirectorioActivo.proCompruebaAutenticacion(mbPrincipal.RcPrincipal);
				}
			}
			else
			{
				mbAcceso.gstBasedatosActual = Base_nueva;
				mbAcceso.gstServerActual = Servidor_nuevo;
				mbAcceso.gstUsuarioBD = UsuarioBD_nuevo;
				mbAcceso.gstPasswordBD = PasswordBD_nuevo;
				mbDirectorioActivo.proCompruebaAutenticacion(mbPrincipal.RcPrincipal);
			}

			//If VstBaseInicial = fnQueryRegistro("basedatos") And VstServidorInicial = fnQueryRegistro("servidor") Then
			mbPrincipal.ObligaAactualizar = mbPrincipal.VstBaseInicial == mbAcceso.gstBasedatosActual && mbPrincipal.VstServidorInicial == mbAcceso.gstServerActual;

			//llamo al proceso de comprobacion de nombre largo o corto
			S01000F1.DefInstance.Usuario_largo();

			if (mbDirectorioActivo.vstTipoAutenticacion == "M")
			{
				mbDirectorioActivo.proAutenticacionUsuario((mbPrincipal.bLargo) ? mbPrincipal.vstUsuarioLargo : mbPrincipal.vstUsuario, mbPrincipal.RcPrincipal, mbPrincipal.bLargo);
			}
			else
			{
				mbDirectorioActivo.vstAutenticacionUsuario = mbDirectorioActivo.vstTipoAutenticacion;
			}

			if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
			{
				CONTRASENAENCRIP = mbPrincipal.EncriptarPassword(ref mbPrincipal.vstContrase�a);
			}
			else
			{
				CONTRASENAENCRIP = mbPrincipal.vstContrase�a;
			}

			//Call S01000F1.ComprobarUsuario(vstUsuario, CONTRASENAENCRIP)
			S01000F1.DefInstance.ComprobarUsuario((mbPrincipal.bLargo) ? mbPrincipal.vstUsuarioLargo : mbPrincipal.vstUsuario, CONTRASENAENCRIP); //oscar

			if (mbPrincipal.RsPrincipal.Tables[0].Rows.Count < 1)
			{
				//Oscar: Incidencias de acceso
				S01000F1.DefInstance.GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 3);
				//---------------------------
				if (mbPrincipal.PrimerAcceso)
				{
					//Usuario no autorizado.
					short tempRefParam3 = 1240;
                    string[] tempRefParam4 = new string[]{};
					mbPrincipal.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbPrincipal.RcPrincipal, tempRefParam4);
					for (int X = 1; X <= mbPrincipal.Tabla_BD.GetUpperBound(1); X++)
					{
						//Debug.Print UCase(Tabla_BD(1, x)) & " " & UCase(Tabla_BD(2, x))
						if (mbPrincipal.Tabla_BD[1, X].ToUpper() == mbPrincipal.VstBaseInicial.Trim().ToUpper() && mbPrincipal.Tabla_BD[2, X].ToUpper() == mbPrincipal.VstServidorInicial.Trim().ToUpper())
						{
							CmbBD.SelectedIndex = X - 1;
							cbAceptar_Click(cbAceptar, new EventArgs());
							break;
						}
					}
				}
				this.Close();				
				//no existe el usuario en la nueva b.d.
				mbPrincipal.AccedoLoginMedianteCambioBD = true;
				fLogin.fTarjeta = false;
				fLogin.ShowDialog();
			}
			else
			{
				//oscar
				if (mbPrincipal.PrimerAcceso)
				{
					mbPrincipal.bUsuarioRedireccionado = true;
				}
				//-------

				mbPrincipal.iContador = 0; // inicializamos el contador cada vez que entre
				// correctamente
				S01000F1.DefInstance.fLoginSucceeded = true;
				S01000F1.DefInstance.fOK = true;

				//OSCAR: Caducidad de la pasword
				if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
				{
					S01000F1.DefInstance.CaducaPassword(mbPrincipal.bLargo, (mbPrincipal.bLargo) ? mbPrincipal.vstUsuarioLargo : mbPrincipal.vstUsuario, CONTRASENAENCRIP);
				}
				//------------

				if (!mbPrincipal.fFormCargado)
				{
					//oscar
					mbPrincipal.gbSalir = false;
                    mbPrincipal.ControlPantalla.Close();
                                                            					                    
                    //Oscar: Incidencias de acceso
                    S01000F1.DefInstance.GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 0);
					S01000F1.DefInstance.GrabaAcceso(mbPrincipal.vstUsuario);
                    //---------------------------
                    
					if (mbPrincipal.PrimerAcceso)
					{
						mbPrincipal.ControlPantalla.Hide();
					}
					else
					{
                        if (mbPrincipal.ControlPantalla.Name == "IMDH00F2")
                            mbPrincipal.ControlPantalla = new IMDH00F2();
                        else if (mbPrincipal.ControlPantalla.Name == "IMDH00F3")
                            mbPrincipal.ControlPantalla = new IMDH00F3();

						mbPrincipal.ControlPantalla.Show();
					}
                    
					//-------------------------

					this.Close();
					if (S01000F1.DefInstance.fTarjeta)
					{                        
						mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
						mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;                        
					}
					//ControlPantalla.Show
					mbPrincipal.fFormCargado = true;
				}
				else
				{
					mbPrincipal.gbSalir = false;
                    mbPrincipal.ControlPantalla.Close();                  					                    
					this.Close();

					//Oscar: Incidencias de acceso
					S01000F1.DefInstance.GrabaIncidencia(mbPrincipal.vstUsuario, "PRINCIPAL", 0);
					S01000F1.DefInstance.GrabaAcceso(mbPrincipal.vstUsuario);
                    //---------------------------
                    
					if (mbPrincipal.PrimerAcceso)
					{
						mbPrincipal.ControlPantalla.Hide();
					}
					else
					{
                        if (mbPrincipal.ControlPantalla.Name == "IMDH00F2")
                            mbPrincipal.ControlPantalla = new IMDH00F2();
                        else if (mbPrincipal.ControlPantalla.Name == "IMDH00F3")
                            mbPrincipal.ControlPantalla = new IMDH00F3();
						mbPrincipal.ControlPantalla.Show();
					}
                    
					mbPrincipal.gbSalir = true;
					if (S01000F1.DefInstance.fTarjeta)
					{                        
						mbPrincipal.ControlPantalla.mnuCambiarContrase�a.Available = false;
						mbPrincipal.ControlPantalla.mnuCambiarUsuario.Available = false;                     
					}
					//ControlPantalla.Show
				}
			}
            
			if (mbPrincipal.bLargo)
			{                
			    mbPrincipal.ControlPantalla.stBar.Items[1].Text = mbPrincipal.vstUsuarioLargo;
			}
			else
			{                
				mbPrincipal.ControlPantalla.stBar.Items[1].Text = mbPrincipal.vstUsuario;
			}
            
			//ControlPantalla.stBar.Panels(4).Text = Base_nueva & " en el servidor " & Servidor_nuevo
            
			mbPrincipal.ControlPantalla.stBar.Items[3].Text = mbAcceso.gstBasedatosActual + " en el servidor " + mbAcceso.gstServerActual;
            
			if (mbPrincipal.fConexion)
			{
				mbPrincipal.CERRARCONEXION();
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			//oscar
			//SetValue "SOFTWARE\INDRA\GHOSP", "BASEDATOS", Trim(Base_Actual), 1
			mbAcceso.gstBasedatosActual = Base_Actual.Trim();
			//------
			mbPrincipal.CERRARCONEXION();
			this.Close();
		}

        private void CmbBD_SelectedIndexChanging(object sender, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs e)
        {
            if (CmbBD.SelectedIndex != -1)
            {
                cbAceptar.Enabled = true;
            }
        }        
		
		private void S03000F1_Load(Object eventSender, EventArgs eventArgs)
		{
			//leer de la tabla y cargar combo
			CARGAR_COMBO_BASEDATOS();
			cbAceptar.Enabled = false;

			Base_Actual = mbAcceso.gstBasedatosActual;
			Servidor_Actual = mbAcceso.gstServerActual;

			mbPrincipal.gstBasedatosAC = mbAcceso.gstBasedatosActual;
			mbPrincipal.gstServerAC = mbAcceso.gstServerActual;
			mbPrincipal.gstUsuarioBDAC = mbAcceso.gstUsuarioBD;
			mbPrincipal.gstpasswordBDAC = mbAcceso.gstPasswordBD;
			//-----
			Usuario_Actual = mbPrincipal.vstUsuario;
			//-----

			mbDirectorioActivo.gstTipoAutenticacion_Actual = mbDirectorioActivo.vstTipoAutenticacion;
			mbDirectorioActivo.gstDominio_Actual = mbDirectorioActivo.vstDominioAutenticacion;
			mbDirectorioActivo.gstAutenticacionUsuario_Actual = mbDirectorioActivo.vstAutenticacionUsuario;
		}

		private void CARGAR_COMBO_BASEDATOS()
		{
			bool etiqueta2 = false;
			bool etiqueta = false;
			string sql = String.Empty;
			DataSet rs = null;
			int Defecto = 0;
			bool Inicial = true;
			int X = 1;
			try
			{
				etiqueta = true;
				etiqueta2 = false;
				sql = "SELECT * FROM SCONBASE where fborrado is null ORDER BY BASEDEDATOS";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbPrincipal.RcPrincipal);
				rs = new DataSet();
				tempAdapter.Fill(rs);
				etiqueta2 = true;
				etiqueta = false;
				foreach (DataRow iteration_row in rs.Tables[0].Rows)
				{
                    mbPrincipal.Tabla_BD = ArraysHelper.RedimPreserve<string[, ]>(mbPrincipal.Tabla_BD, new int[]{5, X + 1});
					
					if (Convert.IsDBNull(iteration_row["BASEDEDATOS"]))
					{
						mbPrincipal.Tabla_BD[1, X] = "";
					}
					else
					{
						mbPrincipal.Tabla_BD[1, X] = Convert.ToString(iteration_row["BASEDEDATOS"]).Trim();
					}					
					if (Convert.IsDBNull(iteration_row["SERVIDOR"]))
					{
						mbPrincipal.Tabla_BD[2, X] = "";
					}
					else
					{
						mbPrincipal.Tabla_BD[2, X] = Convert.ToString(iteration_row["SERVIDOR"]).Trim();
					}					
					if (Convert.IsDBNull(iteration_row["GUSUBASE"]))
					{
						mbPrincipal.Tabla_BD[3, X] = "";
					}
					else
					{
						mbPrincipal.Tabla_BD[3, X] = Convert.ToString(iteration_row["GUSUBASE"]).Trim();
					}					
					if (Convert.IsDBNull(iteration_row["GCONTRAS"]))
					{
						mbPrincipal.Tabla_BD[4, X] = "";
					}
					else
					{
						mbPrincipal.Tabla_BD[4, X] = mbPrincipal.DesencriptarPassword(Convert.ToString(iteration_row["GCONTRAS"]).Trim()).Trim();
					}

					if (mbPrincipal.Tabla_BD[1, X].ToUpper() == mbPrincipal.VstBaseInicial.ToUpper() && mbPrincipal.Tabla_BD[2, X].ToUpper() == mbPrincipal.VstServidorInicial.ToUpper())
					{
						Defecto = X;
						Inicial = false;
					}
					X++;
				}
				
				rs.Close();
				if (Inicial)
				{
                    mbPrincipal.Tabla_BD = ArraysHelper.RedimPreserve<string[, ]>(mbPrincipal.Tabla_BD, new int[]{5, X + 1});
					mbPrincipal.Tabla_BD[1, X] = mbPrincipal.VstBaseInicial.Trim();
					mbPrincipal.Tabla_BD[2, X] = mbPrincipal.VstServidorInicial.Trim();
					mbPrincipal.Tabla_BD[3, X] = mbPrincipal.gstUsuarioBDInicial;
					mbPrincipal.Tabla_BD[4, X] = mbPrincipal.gstpasswordBDInicial;
					Defecto = X;
				}
				CmbBD.Items.Clear();
				for (X = 1; X <= mbPrincipal.Tabla_BD.GetUpperBound(1); X++)
				{                    
                    CmbBD.Items.Add(mbPrincipal.Tabla_BD[1, X] + " en el servidor " + mbPrincipal.Tabla_BD[2, X]);
				}
				if (Defecto != 0)
				{
					CmbBD.SelectedIndex = Defecto - 1;
				}
			}
			catch (Exception excep)
			{
				if (!etiqueta2 && !etiqueta)
				{
					throw excep;
				}
				if (etiqueta)
				{
					//UPGRADE_ISSUE: (2064) RDO.rdoErrors property rdoEngine.rdoErrors.Count was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//UPGRADE_ISSUE: (2064) RDO.rdoErrors property rdoEngine.rdoErrors.Item was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//if (UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.Item(UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.getCount() - 1).Number == 208)
					//{
     //                   mbPrincipal.Tabla_BD = ArraysHelper.RedimPreserve<string[, ]>(mbPrincipal.Tabla_BD, new int[]{5, X + 1});
					//	mbPrincipal.Tabla_BD[1, X] = mbPrincipal.VstBaseInicial.Trim();
					//	mbPrincipal.Tabla_BD[2, X] = mbPrincipal.VstServidorInicial.Trim();
					//	mbPrincipal.Tabla_BD[3, X] = mbPrincipal.gstUsuarioBDInicial;
					//	mbPrincipal.Tabla_BD[4, X] = mbPrincipal.gstpasswordBDInicial;
					//	Defecto = X;
					//	CmbBD.Items.Clear();
					//	for (X = 1; X <= mbPrincipal.Tabla_BD.GetUpperBound(1); X++)
					//	{
					//		CmbBD.AddItem(mbPrincipal.Tabla_BD[1, X] + " en el servidor " + mbPrincipal.Tabla_BD[2, X]);
					//	}
					//	if (Defecto != 0)
					//	{
					//		CmbBD.SelectedIndex = Defecto - 1;
					//	}
					//}
					//else
					//{
					//	MessageBox.Show(excep.Message, Application.ProductName);
					//}
					return;
				}
				if (etiqueta2 || etiqueta)
				{
					RadMessageBox.Show(excep.Message, Application.ProductName);
				}
			}
		}

		private void S03000F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            S03000F1.DefInstance.Dispose();
		}
	}
}
 
 