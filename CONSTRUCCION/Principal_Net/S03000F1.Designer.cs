using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class S03000F1
	{

		#region "Upgrade Support "
		private static S03000F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static S03000F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new S03000F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbAceptar", "cbCancelar", "CmbBD", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadDropDownList CmbBD;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.CmbBD = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbBD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.Location = new System.Drawing.Point(261, 86);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 31);
            this.cbAceptar.TabIndex = 3;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(347, 86);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 31);
            this.cbCancelar.TabIndex = 2;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.CmbBD);
            this.Frame1.HeaderText = "Conectar a otra Base de Datos";
            this.Frame1.Location = new System.Drawing.Point(4, 4);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(424, 78);
            this.Frame1.TabIndex = 0;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Conectar a otra Base de Datos";
            // 
            // CmbBD
            // 
            this.CmbBD.Cursor = System.Windows.Forms.Cursors.Default;
            this.CmbBD.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CmbBD.Location = new System.Drawing.Point(15, 23);
            this.CmbBD.Name = "CmbBD";
            this.CmbBD.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CmbBD.Size = new System.Drawing.Size(398, 20);
            this.CmbBD.TabIndex = 1;
            this.CmbBD.SelectedIndexChanging += new Telerik.WinControls.UI.Data.PositionChangingEventHandler(this.CmbBD_SelectedIndexChanging);
            // 
            // S03000F1
            // 
            this.AcceptButton = this.cbAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 125);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "S03000F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Cambio de conexi�n a Base de Datos";
            this.Closed += new System.EventHandler(this.S03000F1_Closed);
            this.Load += new System.EventHandler(this.S03000F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbBD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}