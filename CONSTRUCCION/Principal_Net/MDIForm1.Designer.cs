using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class IMDH00F1
	{

		#region "Upgrade Support "
		private static IMDH00F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static IMDH00F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new IMDH00F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "mnuPrueba", "MainMenu1", "Picture1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IMDH00F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.MainMenu1 = new Telerik.WinControls.UI.RadMenu();
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MainMenu1
            // 
            this.MainMenu1.Location = new System.Drawing.Point(0, 0);
            this.MainMenu1.Name = "MainMenu1";
            this.MainMenu1.Size = new System.Drawing.Size(632, 24);
            this.MainMenu1.TabIndex = 1;
            this.MainMenu1.Text = "menuStrip1";
            // 
            // IMDH00F1
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(632, 458);
            this.Controls.Add(this.MainMenu1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Location = new System.Drawing.Point(11, 18);
            this.Name = "IMDH00F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IMDH00F1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.IMDH00F1_Activated_1);
            this.Deactivate += new System.EventHandler(this.IMDH00F1_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IMDH00F1_FormClosing);
            this.Load += new System.EventHandler(this.IMDH00F1_Load);
            this.Resize += new System.EventHandler(this.IMDH00F1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        public Telerik.WinControls.UI.RadMenu MainMenu1;
	}
}