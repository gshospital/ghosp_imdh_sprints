using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class IMDH00F2
	{

		#region "Upgrade Support "
		private static IMDH00F2 m_vb6FormDefInstance;
		public static bool m_InitializingDefInstance;
		public static IMDH00F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new IMDH00F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "mnuUsuario", "mnuNuevo", "mnuAbrir", "mnuCerrar", "g11", "mnuGuardar", "mnuGuardarComo", "g12", "mnuImprimirInformes", "g13", "mnuSalirItem", "mnuArchivo", "mnuDeshacer", "g2", "mnuCortar", "mnuCopiar", "mnuPegar", "g22", "mnuEliminar", "mnuEdicion", "mnuIconosGrandes", "mnuIconosPequeños", "mnuLista", "mnuVer", "_mnuOpcion_0", "mnuOpciones", "mnuCambiarUsuario", "mnuCambiarContraseña", "g4", "mnuCambiarBD", "g5", "mnuSincronizar", "g6", "mnu_LeerCorreo", "mnu_LeerMensajesPrivados", "mnuSesion", "_mnuTemporal_0", "_mnuTemporal_1", "_mnuTemporal_2", "mnuVentana", "mnu_accesootras", "mnu_otrasopciones", "mnuContenido", "mnuIndiceItem", "mnuTemasAyuda", "g73", "mnuUsarAyuda", "g72", "mnuAcercaDeItem", "mnuAyuda", "MainMenu1", "principal4bmp", "Principalbmp", "_stBar_Panel1", "_stBar_Panel2", "_stBar_Panel3", "_stBar_Panel4", "stBar", "Timer2", "Timer1", "_LbEtiqueta_15", "Label3", "Label2", "Label1", "_LbEtiqueta_11", "_LbEtiqueta_10", "_LbEtiqueta_3", "_LbEtiqueta_8", "_LbEtiqueta_13", "_LbEtiqueta_14", "_LbEtiqueta_5", "_LbEtiqueta_4", "_LbEtiqueta_12", "_LbEtiqueta_6", "_LbEtiqueta_9", "_LbEtiqueta_7", "_LbEtiqueta_2", "_LbEtiqueta_0", "_LbEtiqueta_1", "imgIMDH", "_imgIconos_0", "LbEtiqueta", "imgIconos", "mnuOpcion", "mnuTemporal"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadMenuItem mnuUsuario;
		public Telerik.WinControls.UI.RadMenuItem mnuNuevo;
		public Telerik.WinControls.UI.RadMenuItem mnuAbrir;
		public Telerik.WinControls.UI.RadMenuItem mnuCerrar;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g11;
		public Telerik.WinControls.UI.RadMenuItem mnuGuardar;
		public Telerik.WinControls.UI.RadMenuItem mnuGuardarComo;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g12;
		public Telerik.WinControls.UI.RadMenuItem mnuImprimirInformes;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g13;
		public Telerik.WinControls.UI.RadMenuItem mnuSalirItem;
		public Telerik.WinControls.UI.RadMenuItem mnuArchivo;
		public Telerik.WinControls.UI.RadMenuItem mnuDeshacer;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g2;
		public Telerik.WinControls.UI.RadMenuItem mnuCortar;
		public Telerik.WinControls.UI.RadMenuItem mnuCopiar;
		public Telerik.WinControls.UI.RadMenuItem mnuPegar;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g22;
		public Telerik.WinControls.UI.RadMenuItem mnuEliminar;
		public Telerik.WinControls.UI.RadMenuItem mnuEdicion;
		public Telerik.WinControls.UI.RadMenuItem mnuIconosGrandes;
		public Telerik.WinControls.UI.RadMenuItem mnuIconosPequeños;
		public Telerik.WinControls.UI.RadMenuItem mnuLista;
		public Telerik.WinControls.UI.RadMenuItem mnuVer;
		private Telerik.WinControls.UI.RadMenuItem _mnuOpcion_0;
		public Telerik.WinControls.UI.RadMenuItem mnuOpciones;
		public Telerik.WinControls.UI.RadMenuItem mnuCambiarUsuario;
		public Telerik.WinControls.UI.RadMenuItem mnuCambiarContraseña;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g4;
		public Telerik.WinControls.UI.RadMenuItem mnuCambiarBD;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g5;
		public Telerik.WinControls.UI.RadMenuItem mnuSincronizar;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g6;
		public Telerik.WinControls.UI.RadMenuItem mnu_LeerCorreo;
		public Telerik.WinControls.UI.RadMenuItem mnu_LeerMensajesPrivados;
		public Telerik.WinControls.UI.RadMenuItem mnuSesion;
		private Telerik.WinControls.UI.RadMenuItem _mnuTemporal_0;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _mnuTemporal_1;
		private Telerik.WinControls.UI.RadMenuItem _mnuTemporal_2;
		public Telerik.WinControls.UI.RadMenuItem mnuVentana;
		public Telerik.WinControls.UI.RadMenuItem mnu_accesootras;
		public Telerik.WinControls.UI.RadMenuItem mnu_otrasopciones;
		public Telerik.WinControls.UI.RadMenuItem mnuContenido;
		public Telerik.WinControls.UI.RadMenuItem mnuIndiceItem;
		public Telerik.WinControls.UI.RadMenuItem mnuTemasAyuda;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g73;
		public Telerik.WinControls.UI.RadMenuItem mnuUsarAyuda;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g72;
		public Telerik.WinControls.UI.RadMenuItem mnuAcercaDeItem;
		public Telerik.WinControls.UI.RadMenuItem mnuAyuda;
		public System.Windows.Forms.PictureBox principal4bmp;
		public System.Windows.Forms.PictureBox Principalbmp;

        private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel1;
		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel2;
		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel3;
		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel4;

        //private System.Windows.Forms.ToolStripStatusLabel _stBar_Panel1;
        //private System.Windows.Forms.ToolStripStatusLabel _stBar_Panel2;
        //private System.Windows.Forms.ToolStripStatusLabel _stBar_Panel3;
        //private System.Windows.Forms.ToolStripStatusLabel _stBar_Panel4;

        public Telerik.WinControls.UI.RadStatusStrip stBar;
       // public System.Windows.Forms.StatusStrip stBar;
		public System.Windows.Forms.Timer Timer2;
		public System.Windows.Forms.Timer Timer1;
		public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_15;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_11;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_10;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_3;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_8;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_13;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_14;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_5;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_4;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_12;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_6;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_9;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_7;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_2;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_0;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_1;
		public System.Windows.Forms.PictureBox imgIMDH;
		private System.Windows.Forms.PictureBox _imgIconos_0;
		public Telerik.WinControls.UI.RadLabel[] LbEtiqueta = new Telerik.WinControls.UI.RadLabel[16];
		public System.Windows.Forms.PictureBox[] imgIconos = new System.Windows.Forms.PictureBox[1];
		public Telerik.WinControls.RadItem[] mnuOpcion = new Telerik.WinControls.RadItem[1];
		public Telerik.WinControls.RadItem[] mnuTemporal = new Telerik.WinControls.RadItem[3];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		public void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IMDH00F2));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.mnuArchivo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuSalirItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuUsuario = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuAbrir = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCerrar = new Telerik.WinControls.UI.RadMenuItem();
            this.g11 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuGuardar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuGuardarComo = new Telerik.WinControls.UI.RadMenuItem();
            this.g12 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuImprimirInformes = new Telerik.WinControls.UI.RadMenuItem();
            this.g13 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuEdicion = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuDeshacer = new Telerik.WinControls.UI.RadMenuItem();
            this.g2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuCortar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuPegar = new Telerik.WinControls.UI.RadMenuItem();
            this.g22 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuEliminar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuVer = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuIconosGrandes = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuIconosPequeños = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuLista = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuOpciones = new Telerik.WinControls.UI.RadMenuItem();
            this._mnuOpcion_0 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuSesion = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCambiarUsuario = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCambiarContraseña = new Telerik.WinControls.UI.RadMenuItem();
            this.g4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuCambiarBD = new Telerik.WinControls.UI.RadMenuItem();
            this.g5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuSincronizar = new Telerik.WinControls.UI.RadMenuItem();
            this.g6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnu_LeerCorreo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_LeerMensajesPrivados = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuVentana = new Telerik.WinControls.UI.RadMenuItem();
            this._mnuTemporal_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnuTemporal_1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._mnuTemporal_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_otrasopciones = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_accesootras = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuAyuda = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuIndiceItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuTemasAyuda = new Telerik.WinControls.UI.RadMenuItem();
            this.g72 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuAcercaDeItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuContenido = new Telerik.WinControls.UI.RadMenuItem();
            this.g73 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuUsarAyuda = new Telerik.WinControls.UI.RadMenuItem();
            this.principal4bmp = new System.Windows.Forms.PictureBox();
            this.Principalbmp = new System.Windows.Forms.PictureBox();
            this.stBar = new Telerik.WinControls.UI.RadStatusStrip();
            this._stBar_Panel1 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this._stBar_Panel2 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this._stBar_Panel3 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this._stBar_Panel4 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.Timer2 = new System.Windows.Forms.Timer(this.components);
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
            this.CDAyudaFont = new System.Windows.Forms.FontDialog();
            this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
            this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
            this._LbEtiqueta_15 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_11 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_10 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_3 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_8 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_13 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_14 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_5 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_4 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_12 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_6 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_9 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_7 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_2 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_0 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_1 = new Telerik.WinControls.UI.RadLabel();
            this.imgIMDH = new System.Windows.Forms.PictureBox();
            this._imgIconos_0 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.principal4bmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Principalbmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIMDH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._imgIconos_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuArchivo
            // 
            this.mnuArchivo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuSalirItem});
            this.mnuArchivo.Name = "mnuArchivo";
            this.mnuArchivo.Text = "&Archivo";
            // 
            // mnuSalirItem
            // 
            this.mnuSalirItem.Name = "mnuSalirItem";
            this.mnuSalirItem.Text = "&Salir";
            this.mnuSalirItem.Click += new System.EventHandler(this.mnuSalirItem_Click);
            // 
            // mnuUsuario
            // 
            this.mnuUsuario.Enabled = false;
            this.mnuUsuario.Name = "mnuUsuario";
            this.mnuUsuario.Text = "&Usuario";
            // 
            // mnuNuevo
            // 
            this.mnuNuevo.Name = "mnuNuevo";
            this.mnuNuevo.Text = "&Nuevo";
            // 
            // mnuAbrir
            // 
            this.mnuAbrir.Name = "mnuAbrir";
            this.mnuAbrir.Text = "A&brir";
            // 
            // mnuCerrar
            // 
            this.mnuCerrar.Name = "mnuCerrar";
            this.mnuCerrar.Text = "&Cerrar";
            // 
            // g11
            // 
            this.g11.Name = "g11";
            this.g11.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuGuardar
            // 
            this.mnuGuardar.Name = "mnuGuardar";
            this.mnuGuardar.Text = "&Guardar";
            // 
            // mnuGuardarComo
            // 
            this.mnuGuardarComo.Name = "mnuGuardarComo";
            this.mnuGuardarComo.Text = "G&uardar como ...";
            // 
            // g12
            // 
            this.g12.Name = "g12";
            this.g12.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuImprimirInformes
            // 
            this.mnuImprimirInformes.Name = "mnuImprimirInformes";
            this.mnuImprimirInformes.Text = "&Imprimir Informes";
            // 
            // g13
            // 
            this.g13.Name = "g13";
            this.g13.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuEdicion
            // 
            this.mnuEdicion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuDeshacer,
            this.g2,
            this.mnuCortar,
            this.mnuCopiar,
            this.mnuPegar,
            this.g22,
            this.mnuEliminar});
            this.mnuEdicion.Name = "mnuEdicion";
            this.mnuEdicion.Text = "&Edición";
            // 
            // mnuDeshacer
            // 
            this.mnuDeshacer.Enabled = false;
            this.mnuDeshacer.Name = "mnuDeshacer";
            this.mnuDeshacer.Text = "&Deshacer";
            // 
            // g2
            // 
            this.g2.Name = "g2";
            this.g2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuCortar
            // 
            this.mnuCortar.Enabled = false;
            this.mnuCortar.Name = "mnuCortar";
            this.mnuCortar.Text = "Cor&tar";
            // 
            // mnuCopiar
            // 
            this.mnuCopiar.Enabled = false;
            this.mnuCopiar.Name = "mnuCopiar";
            this.mnuCopiar.Text = "&Copiar";
            // 
            // mnuPegar
            // 
            this.mnuPegar.Enabled = false;
            this.mnuPegar.Name = "mnuPegar";
            this.mnuPegar.Text = "&Pegar";
            // 
            // g22
            // 
            this.g22.Name = "g22";
            this.g22.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuEliminar
            // 
            this.mnuEliminar.Enabled = false;
            this.mnuEliminar.Name = "mnuEliminar";
            this.mnuEliminar.Text = "E&liminar";
            // 
            // mnuVer
            // 
            this.mnuVer.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuIconosGrandes,
            this.mnuIconosPequeños,
            this.mnuLista});
            this.mnuVer.Name = "mnuVer";
            this.mnuVer.Text = "&Ver";
            // 
            // mnuIconosGrandes
            // 
            this.mnuIconosGrandes.IsChecked = true;
            this.mnuIconosGrandes.Name = "mnuIconosGrandes";
            this.mnuIconosGrandes.Text = "Iconos &Grandes";
            this.mnuIconosGrandes.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // mnuIconosPequeños
            // 
            this.mnuIconosPequeños.Enabled = false;
            this.mnuIconosPequeños.Name = "mnuIconosPequeños";
            this.mnuIconosPequeños.Text = "Iconos &Pequeños";
            // 
            // mnuLista
            // 
            this.mnuLista.Enabled = false;
            this.mnuLista.Name = "mnuLista";
            this.mnuLista.Text = "&Lista";
            // 
            // mnuOpciones
            // 
            this.mnuOpciones.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnuOpcion_0});
            this.mnuOpciones.Name = "mnuOpciones";
            this.mnuOpciones.Text = "&Opciones";
            // 
            // _mnuOpcion_0
            // 
            this._mnuOpcion_0.Name = "_mnuOpcion_0";
            this._mnuOpcion_0.Text = "";
            this._mnuOpcion_0.Click += new System.EventHandler(this.mnuOpcion_Click);
            // 
            // mnuSesion
            // 
            this.mnuSesion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuCambiarUsuario,
            this.mnuCambiarContraseña,
            this.g4,
            this.mnuCambiarBD,
            this.g5,
            this.mnuSincronizar,
            this.g6,
            this.mnu_LeerCorreo,
            this.mnu_LeerMensajesPrivados});
            this.mnuSesion.Name = "mnuSesion";
            this.mnuSesion.Text = "&Sesión";
            // 
            // mnuCambiarUsuario
            // 
            this.mnuCambiarUsuario.Name = "mnuCambiarUsuario";
            this.mnuCambiarUsuario.Text = "Cambiar &Usuario";
            this.mnuCambiarUsuario.Click += new System.EventHandler(this.mnuCambiarUsuario_Click);
            // 
            // mnuCambiarContraseña
            // 
            this.mnuCambiarContraseña.Name = "mnuCambiarContraseña";
            this.mnuCambiarContraseña.Text = "Cambiar &Contraseña";
            this.mnuCambiarContraseña.Click += new System.EventHandler(this.mnuCambiarContraseña_Click);
            // 
            // g4
            // 
            this.g4.Name = "g4";
            this.g4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuCambiarBD
            // 
            this.mnuCambiarBD.Name = "mnuCambiarBD";
            this.mnuCambiarBD.Text = "Cambiar Conexion a Base de Datos";
            this.mnuCambiarBD.Click += new System.EventHandler(this.mnuCambiarBD_Click);
            // 
            // g5
            // 
            this.g5.Name = "g5";
            this.g5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuSincronizar
            // 
            this.mnuSincronizar.Name = "mnuSincronizar";
            this.mnuSincronizar.Text = "&Sincronizar Fecha/Hora";
            this.mnuSincronizar.Click += new System.EventHandler(this.mnuSincronizar_Click);
            // 
            // g6
            // 
            this.g6.Name = "g6";
            this.g6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnu_LeerCorreo
            // 
            this.mnu_LeerCorreo.Name = "mnu_LeerCorreo";
            this.mnu_LeerCorreo.Text = "&Leer correo";
            this.mnu_LeerCorreo.Click += new System.EventHandler(this.mnu_LeerCorreo_Click);
            // 
            // mnu_LeerMensajesPrivados
            // 
            this.mnu_LeerMensajesPrivados.Name = "mnu_LeerMensajesPrivados";
            this.mnu_LeerMensajesPrivados.Text = "Mensajería Privada";
            this.mnu_LeerMensajesPrivados.Click += new System.EventHandler(this.mnu_LeerMensajesPrivados_Click);
            // 
            // mnuVentana
            // 
            this.mnuVentana.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnuTemporal_0,
            this._mnuTemporal_1,
            this._mnuTemporal_2});
            this.mnuVentana.Name = "mnuVentana";
            this.mnuVentana.Text = "Ven&tana";
            // 
            // _mnuTemporal_0
            // 
            this._mnuTemporal_0.Name = "_mnuTemporal_0";
            this._mnuTemporal_0.Text = "&Organizar Iconos";
            this._mnuTemporal_0.Click += new System.EventHandler(this.mnuTemporal_Click);
            // 
            // _mnuTemporal_1
            // 
            this._mnuTemporal_1.Name = "_mnuTemporal_1";
            this._mnuTemporal_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._mnuTemporal_1.Click += new System.EventHandler(this.mnuTemporal_Click);
            // 
            // _mnuTemporal_2
            // 
            this._mnuTemporal_2.Name = "_mnuTemporal_2";
            this._mnuTemporal_2.Text = "";
            this._mnuTemporal_2.Click += new System.EventHandler(this.mnuTemporal_Click);
            // 
            // mnu_otrasopciones
            // 
            this.mnu_otrasopciones.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnu_accesootras});
            this.mnu_otrasopciones.Name = "mnu_otrasopciones";
            this.mnu_otrasopciones.Text = "O&tras opciones";
            // 
            // mnu_accesootras
            // 
            this.mnu_accesootras.Name = "mnu_accesootras";
            this.mnu_accesootras.Text = "Acceso a otras opciones";
            this.mnu_accesootras.Click += new System.EventHandler(this.mnu_accesootras_Click);
            // 
            // mnuAyuda
            // 
            this.mnuAyuda.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuIndiceItem,
            this.mnuTemasAyuda,
            this.g72,
            this.mnuAcercaDeItem});
            this.mnuAyuda.Name = "mnuAyuda";
            this.mnuAyuda.Text = "A&yuda";
            // 
            // mnuIndiceItem
            // 
            this.mnuIndiceItem.Name = "mnuIndiceItem";
            this.mnuIndiceItem.Text = "&Indice";
            this.mnuIndiceItem.Click += new System.EventHandler(this.mnuIndiceItem_Click);
            // 
            // mnuTemasAyuda
            // 
            this.mnuTemasAyuda.Name = "mnuTemasAyuda";
            this.mnuTemasAyuda.Text = "&Temas de Ayuda";
            this.mnuTemasAyuda.Click += new System.EventHandler(this.mnuTemasAyuda_Click);
            // 
            // g72
            // 
            this.g72.Name = "g72";
            this.g72.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuAcercaDeItem
            // 
            this.mnuAcercaDeItem.Name = "mnuAcercaDeItem";
            this.mnuAcercaDeItem.Text = "A&cerca de ";
            this.mnuAcercaDeItem.Click += new System.EventHandler(this.mnuAcercaDeItem_Click);
            // 
            // mnuContenido
            // 
            this.mnuContenido.Name = "mnuContenido";
            this.mnuContenido.Text = "&Contenido";
            this.mnuContenido.Click += new System.EventHandler(this.mnuContenido_Click);
            // 
            // g73
            // 
            this.g73.Name = "g73";
            this.g73.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuUsarAyuda
            // 
            this.mnuUsarAyuda.Name = "mnuUsarAyuda";
            this.mnuUsarAyuda.Text = "&C&omo usar la Ayuda";
            this.mnuUsarAyuda.Click += new System.EventHandler(this.mnuUsarAyuda_Click);
            // 
            // principal4bmp
            // 
            this.principal4bmp.BackColor = System.Drawing.SystemColors.Control;
            this.principal4bmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.principal4bmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.principal4bmp.Image = ((System.Drawing.Image)(resources.GetObject("principal4bmp.Image")));
            this.principal4bmp.Location = new System.Drawing.Point(24, 296);
            this.principal4bmp.Name = "principal4bmp";
            this.principal4bmp.Size = new System.Drawing.Size(57, 41);
            this.principal4bmp.TabIndex = 21;
            this.principal4bmp.TabStop = false;
            this.principal4bmp.Visible = false;
            // 
            // Principalbmp
            // 
            this.Principalbmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Principalbmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.Principalbmp.Image = ((System.Drawing.Image)(resources.GetObject("Principalbmp.Image")));
            this.Principalbmp.Location = new System.Drawing.Point(24, 128);
            this.Principalbmp.Name = "Principalbmp";
            this.Principalbmp.Size = new System.Drawing.Size(65, 41);
            this.Principalbmp.TabIndex = 20;
            this.Principalbmp.TabStop = false;
            this.Principalbmp.Visible = false;
            // 
            // stBar
            // 
            this.stBar.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stBar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._stBar_Panel1,
            this._stBar_Panel2,
            this._stBar_Panel3,
            this._stBar_Panel4});
            this.stBar.Location = new System.Drawing.Point(0, 625);
            this.stBar.Name = "stBar";
            this.stBar.Size = new System.Drawing.Size(811, 22);
            this.stBar.TabIndex = 18;
            // 
            // _stBar_Panel1
            // 
            this._stBar_Panel1.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel1.AutoSize = false;
            this._stBar_Panel1.Bounds = new System.Drawing.Rectangle(0, 0, 129, 22);
            this._stBar_Panel1.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel1.Name = "_stBar_Panel1";
            this.stBar.SetSpring(this._stBar_Panel1, false);
            this._stBar_Panel1.Tag = "";
            this._stBar_Panel1.Text = "Usuario";
            // 
            // _stBar_Panel2
            // 
            this._stBar_Panel2.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel2.AutoSize = false;
            this._stBar_Panel2.Bounds = new System.Drawing.Rectangle(0, 0, 258, 22);
            this._stBar_Panel2.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel2.Name = "_stBar_Panel2";
            this.stBar.SetSpring(this._stBar_Panel2, false);
            this._stBar_Panel2.Tag = "";
            // 
            // _stBar_Panel3
            // 
            this._stBar_Panel3.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel3.AutoSize = false;
            this._stBar_Panel3.Bounds = new System.Drawing.Rectangle(0, 0, 138, 22);
            this._stBar_Panel3.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel3.Name = "_stBar_Panel3";
            this.stBar.SetSpring(this._stBar_Panel3, false);
            this._stBar_Panel3.Tag = "";
            this._stBar_Panel3.Text = "Base de Datos";
            // 
            // _stBar_Panel4
            // 
            this._stBar_Panel4.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel4.AutoSize = false;
            this._stBar_Panel4.Bounds = new System.Drawing.Rectangle(0, 0, 291, 22);
            this._stBar_Panel4.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel4.Name = "_stBar_Panel4";
            this.stBar.SetSpring(this._stBar_Panel4, false);
            this._stBar_Panel4.Tag = "";
            // 
            // Timer2
            // 
            this.Timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // Timer1
            // 
            this.Timer1.Interval = 10000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // _LbEtiqueta_15
            // 
            this._LbEtiqueta_15.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_15.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_15.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_15.Location = new System.Drawing.Point(34, 255);
            this._LbEtiqueta_15.Name = "_LbEtiqueta_15";
            this._LbEtiqueta_15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_15.Size = new System.Drawing.Size(323, 30);
            this._LbEtiqueta_15.TabIndex = 19;
            this._LbEtiqueta_15.Text = "OTros Profesionales Asistenciales";
            this._LbEtiqueta_15.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_15.Visible = false;
            this._LbEtiqueta_15.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_15.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(15, 36);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(39, 18);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "Label3";
            this.Label3.Visible = false;
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Comic Sans MS", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Silver;
            this.Label2.Location = new System.Drawing.Point(24, 144);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(256, 99);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "I-MDH";
            this.Label2.Visible = false;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Comic Sans MS", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Label1.Location = new System.Drawing.Point(27, 145);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(256, 99);
            this.Label1.TabIndex = 15;
            this.Label1.Text = "I-MDH";
            this.Label1.Visible = false;
            // 
            // _LbEtiqueta_11
            // 
            this._LbEtiqueta_11.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_11.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_11.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_11.Location = new System.Drawing.Point(42, 378);
            this._LbEtiqueta_11.Name = "_LbEtiqueta_11";
            this._LbEtiqueta_11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_11.Size = new System.Drawing.Size(189, 30);
            this._LbEtiqueta_11.TabIndex = 14;
            this._LbEtiqueta_11.Text = "Sistema de Gestión";
            this._LbEtiqueta_11.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_11.Visible = false;
            this._LbEtiqueta_11.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_11.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_10
            // 
            this._LbEtiqueta_10.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_10.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_10.Location = new System.Drawing.Point(604, 465);
            this._LbEtiqueta_10.Name = "_LbEtiqueta_10";
            this._LbEtiqueta_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_10.Size = new System.Drawing.Size(94, 30);
            this._LbEtiqueta_10.TabIndex = 13;
            this._LbEtiqueta_10.Text = "Dietética";
            this._LbEtiqueta_10.Visible = false;
            this._LbEtiqueta_10.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_10.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_3
            // 
            this._LbEtiqueta_3.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_3.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_3.Location = new System.Drawing.Point(550, 421);
            this._LbEtiqueta_3.Name = "_LbEtiqueta_3";
            this._LbEtiqueta_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_3.Size = new System.Drawing.Size(112, 30);
            this._LbEtiqueta_3.TabIndex = 12;
            this._LbEtiqueta_3.Text = "Enfermería";
            this._LbEtiqueta_3.Visible = false;
            this._LbEtiqueta_3.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_8
            // 
            this._LbEtiqueta_8.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_8.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_8.Location = new System.Drawing.Point(516, 382);
            this._LbEtiqueta_8.Name = "_LbEtiqueta_8";
            this._LbEtiqueta_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_8.Size = new System.Drawing.Size(174, 30);
            this._LbEtiqueta_8.TabIndex = 11;
            this._LbEtiqueta_8.Text = "Servicios Médicos";
            this._LbEtiqueta_8.Visible = false;
            this._LbEtiqueta_8.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_8.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_13
            // 
            this._LbEtiqueta_13.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_13.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_13.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_13.Location = new System.Drawing.Point(34, 454);
            this._LbEtiqueta_13.Name = "_LbEtiqueta_13";
            this._LbEtiqueta_13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_13.Size = new System.Drawing.Size(213, 30);
            this._LbEtiqueta_13.TabIndex = 10;
            this._LbEtiqueta_13.Text = "Información Gerencial";
            this._LbEtiqueta_13.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_13.Visible = false;
            this._LbEtiqueta_13.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_13.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_14
            // 
            this._LbEtiqueta_14.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_14.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_14.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_14.Location = new System.Drawing.Point(669, 510);
            this._LbEtiqueta_14.Name = "_LbEtiqueta_14";
            this._LbEtiqueta_14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_14.Size = new System.Drawing.Size(51, 30);
            this._LbEtiqueta_14.TabIndex = 9;
            this._LbEtiqueta_14.Text = "Nido";
            this._LbEtiqueta_14.Visible = false;
            this._LbEtiqueta_14.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_14.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_5
            // 
            this._LbEtiqueta_5.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_5.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_5.Location = new System.Drawing.Point(38, 416);
            this._LbEtiqueta_5.Name = "_LbEtiqueta_5";
            this._LbEtiqueta_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_5.Size = new System.Drawing.Size(240, 30);
            this._LbEtiqueta_5.TabIndex = 8;
            this._LbEtiqueta_5.Text = "Información del Paciente";
            this._LbEtiqueta_5.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_5.Visible = false;
            this._LbEtiqueta_5.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_4
            // 
            this._LbEtiqueta_4.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_4.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_4.Location = new System.Drawing.Point(128, 336);
            this._LbEtiqueta_4.Name = "_LbEtiqueta_4";
            this._LbEtiqueta_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_4.Size = new System.Drawing.Size(78, 30);
            this._LbEtiqueta_4.TabIndex = 7;
            this._LbEtiqueta_4.Text = "Archivo";
            this._LbEtiqueta_4.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_4.Visible = false;
            this._LbEtiqueta_4.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_12
            // 
            this._LbEtiqueta_12.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_12.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_12.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_12.Location = new System.Drawing.Point(24, 486);
            this._LbEtiqueta_12.Name = "_LbEtiqueta_12";
            this._LbEtiqueta_12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_12.Size = new System.Drawing.Size(159, 30);
            this._LbEtiqueta_12.TabIndex = 6;
            this._LbEtiqueta_12.Text = "Admon. Sistema";
            this._LbEtiqueta_12.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_12.Visible = false;
            this._LbEtiqueta_12.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_12.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_6
            // 
            this._LbEtiqueta_6.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_6.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_6.Location = new System.Drawing.Point(493, 336);
            this._LbEtiqueta_6.Name = "_LbEtiqueta_6";
            this._LbEtiqueta_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_6.Size = new System.Drawing.Size(110, 30);
            this._LbEtiqueta_6.TabIndex = 5;
            this._LbEtiqueta_6.Text = "Quirófanos";
            this._LbEtiqueta_6.Visible = false;
            this._LbEtiqueta_6.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_9
            // 
            this._LbEtiqueta_9.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_9.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_9.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_9.Location = new System.Drawing.Point(519, 265);
            this._LbEtiqueta_9.Name = "_LbEtiqueta_9";
            this._LbEtiqueta_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_9.Size = new System.Drawing.Size(187, 30);
            this._LbEtiqueta_9.TabIndex = 4;
            this._LbEtiqueta_9.Text = "Servicios Centrales";
            this._LbEtiqueta_9.Visible = false;
            this._LbEtiqueta_9.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_9.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_7
            // 
            this._LbEtiqueta_7.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_7.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_7.Location = new System.Drawing.Point(559, 221);
            this._LbEtiqueta_7.Name = "_LbEtiqueta_7";
            this._LbEtiqueta_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_7.Size = new System.Drawing.Size(160, 30);
            this._LbEtiqueta_7.TabIndex = 3;
            this._LbEtiqueta_7.Text = "Radiodiagnóstico";
            this._LbEtiqueta_7.Visible = false;
            this._LbEtiqueta_7.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_7.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_2
            // 
            this._LbEtiqueta_2.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_2.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_2.Location = new System.Drawing.Point(589, 174);
            this._LbEtiqueta_2.Name = "_LbEtiqueta_2";
            this._LbEtiqueta_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_2.Size = new System.Drawing.Size(95, 30);
            this._LbEtiqueta_2.TabIndex = 2;
            this._LbEtiqueta_2.Text = "Consultas";
            this._LbEtiqueta_2.Visible = false;
            this._LbEtiqueta_2.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_0
            // 
            this._LbEtiqueta_0.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_0.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_0.Location = new System.Drawing.Point(609, 122);
            this._LbEtiqueta_0.Name = "_LbEtiqueta_0";
            this._LbEtiqueta_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_0.Size = new System.Drawing.Size(98, 30);
            this._LbEtiqueta_0.TabIndex = 1;
            this._LbEtiqueta_0.Text = "Urgencias";
            this._LbEtiqueta_0.Visible = false;
            this._LbEtiqueta_0.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_0.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_1
            // 
            this._LbEtiqueta_1.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_1.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_1.Location = new System.Drawing.Point(614, 70);
            this._LbEtiqueta_1.Name = "_LbEtiqueta_1";
            this._LbEtiqueta_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_1.Size = new System.Drawing.Size(89, 30);
            this._LbEtiqueta_1.TabIndex = 0;
            this._LbEtiqueta_1.Text = "Admisión";
            this._LbEtiqueta_1.Visible = false;
            this._LbEtiqueta_1.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // imgIMDH
            // 
            this.imgIMDH.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgIMDH.Image = ((System.Drawing.Image)(resources.GetObject("imgIMDH.Image")));
            this.imgIMDH.Location = new System.Drawing.Point(309, 46);
            this.imgIMDH.Name = "imgIMDH";
            this.imgIMDH.Size = new System.Drawing.Size(64, 64);
            this.imgIMDH.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgIMDH.TabIndex = 22;
            this.imgIMDH.TabStop = false;
            this.imgIMDH.Visible = false;
            // 
            // _imgIconos_0
            // 
            this._imgIconos_0.BackColor = System.Drawing.Color.Transparent;
            this._imgIconos_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._imgIconos_0.Image = ((System.Drawing.Image)(resources.GetObject("_imgIconos_0.Image")));
            this._imgIconos_0.Location = new System.Drawing.Point(177, 42);
            this._imgIconos_0.Name = "_imgIconos_0";
            this._imgIconos_0.Size = new System.Drawing.Size(64, 64);
            this._imgIconos_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._imgIconos_0.TabIndex = 23;
            this._imgIconos_0.TabStop = false;
            this._imgIconos_0.Visible = false;
            this._imgIconos_0.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.imgIconos_GiveFeedback);
            this._imgIconos_0.DoubleClick += new System.EventHandler(this.imgIconos_DoubleClick);
            this._imgIconos_0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imgIconos_MouseDown);
            this._imgIconos_0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imgIconos_MouseUp);
            // 
            // IMDH00F2
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(781, 664);
            this.ControlBox = false;
            this.Controls.Add(this.principal4bmp);
            this.Controls.Add(this.Principalbmp);
            this.Controls.Add(this._LbEtiqueta_15);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this._LbEtiqueta_11);
            this.Controls.Add(this._LbEtiqueta_10);
            this.Controls.Add(this._LbEtiqueta_3);
            this.Controls.Add(this._LbEtiqueta_8);
            this.Controls.Add(this._LbEtiqueta_13);
            this.Controls.Add(this._LbEtiqueta_14);
            this.Controls.Add(this._LbEtiqueta_5);
            this.Controls.Add(this._LbEtiqueta_4);
            this.Controls.Add(this._LbEtiqueta_12);
            this.Controls.Add(this._LbEtiqueta_6);
            this.Controls.Add(this._LbEtiqueta_9);
            this.Controls.Add(this._LbEtiqueta_7);
            this.Controls.Add(this._LbEtiqueta_2);
            this.Controls.Add(this._LbEtiqueta_0);
            this.Controls.Add(this._LbEtiqueta_1);
            this.Controls.Add(this.imgIMDH);
            this.Controls.Add(this._imgIconos_0);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(8, 24);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IMDH00F2";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Activated += new System.EventHandler(this.IMDH00F2_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.IMDH00F2_Closed);
            this.Load += new System.EventHandler(this.IMDH00F2_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.IMDH00F2_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.IMDH00F2_DragEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IMDH00F2_MouseMove);
            this.Resize += new System.EventHandler(this.IMDH00F2_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.principal4bmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Principalbmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIMDH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._imgIconos_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        public void ReLoadForm(bool addEvents)
		{
			InitializemnuTemporal();
			InitializemnuOpcion();
			InitializeimgIconos();
			InitializeLbEtiqueta();
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = Principal.IMDH00F1.DefInstance;
			Principal.IMDH00F1.DefInstance.Show();
		}
		void InitializemnuTemporal()
		{
			this.mnuTemporal = new Telerik.WinControls.RadItem[3];
			this.mnuTemporal[0] = _mnuTemporal_0;
			this.mnuTemporal[1] = _mnuTemporal_1;
			this.mnuTemporal[2] = _mnuTemporal_2;
		}
		void InitializemnuOpcion()
		{
			this.mnuOpcion = new Telerik.WinControls.RadItem[1];
			this.mnuOpcion[0] = _mnuOpcion_0;
		}
		void InitializeimgIconos()
		{
			this.imgIconos = new System.Windows.Forms.PictureBox[1];
			this.imgIconos[0] = _imgIconos_0;
		}
		void InitializeLbEtiqueta()
		{
			this.LbEtiqueta = new Telerik.WinControls.UI.RadLabel[16];
			this.LbEtiqueta[15] = _LbEtiqueta_15;
			this.LbEtiqueta[11] = _LbEtiqueta_11;
			this.LbEtiqueta[10] = _LbEtiqueta_10;
			this.LbEtiqueta[3] = _LbEtiqueta_3;
			this.LbEtiqueta[8] = _LbEtiqueta_8;
			this.LbEtiqueta[13] = _LbEtiqueta_13;
			this.LbEtiqueta[14] = _LbEtiqueta_14;
			this.LbEtiqueta[5] = _LbEtiqueta_5;
			this.LbEtiqueta[4] = _LbEtiqueta_4;
			this.LbEtiqueta[12] = _LbEtiqueta_12;
			this.LbEtiqueta[6] = _LbEtiqueta_6;
			this.LbEtiqueta[9] = _LbEtiqueta_9;
			this.LbEtiqueta[7] = _LbEtiqueta_7;
			this.LbEtiqueta[2] = _LbEtiqueta_2;
			this.LbEtiqueta[0] = _LbEtiqueta_0;
			this.LbEtiqueta[1] = _LbEtiqueta_1;
		}
		#endregion
	}
}