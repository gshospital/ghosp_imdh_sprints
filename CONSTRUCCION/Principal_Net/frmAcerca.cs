using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	internal partial class frmAcerca
        : Telerik.WinControls.UI.RadForm
	{
        private int iCuenta = 0;
		private int posTop = 0;
		private int posLeft = 0;
		public frmAcerca()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}



		private void cmdAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

        private void frmAcerca_Load(Object eventSender, EventArgs eventArgs)
		{
			frmAcerca.DefInstance.Left = (int) (((Convert.ToInt32((float) (mbPrincipal.FormularioMDI.Width * 15)) / 2) - (Convert.ToInt32((float) (frmAcerca.DefInstance.Width * 15)) / 2)) / 15);
			frmAcerca.DefInstance.Top = (int) (((Convert.ToInt32((float) (mbPrincipal.FormularioMDI.Height * 15)) / 2) - (Convert.ToInt32((float) (frmAcerca.DefInstance.Height * 15)) / 2)) / 15);
			lblTexto2.Text = lblTexto2.Text.Trim() + mbDatosRegistro.fnQueryRegistro("Version").Trim();
			if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico", FileAttribute.Normal) != "")
			{
				this.imgIcono.Image = Image.FromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico");
				this.Icon = new Icon(Path.GetDirectoryName(Application.ExecutablePath) + "\\Principal.ico");
			}
			lblTexto1.Text = Serrores.vNomAplicacion;
			lblTexto3.Text = "Copyright � 1997-" + DateTime.Now.Year.ToString() + "  Indra";
			iCuenta = 0;
			Image1.Image = ImageList1.Images[0];
			Image1.Visible = true;
            this.Width = 408;
            this.Height = 249;
		}



		private void Image1_Click(Object eventSender, EventArgs eventArgs)
		{

			// Volvemos al tama�o anterior

			Image1.Visible = false;
			this.Width = 408;
			this.Height = 249;
			this.Top = (int) (posTop / 15);
			this.Left = (int) (posLeft / 15);

			// Mostramos el bot�n

			cmdAceptar.Visible = true;

		}

		private void imgIcono_Click(Object eventSender, EventArgs eventArgs)
		{

			if (iCuenta != 4)
			{
				return;
			}

			iCuenta = 0;

			posTop = Convert.ToInt32((float) (this.Top * 15));
			posLeft = Convert.ToInt32((float) (this.Left * 15));

			// La colocamos

			this.Top = (int) (Screen.PrimaryScreen.Bounds.Height / 5);
			this.Left = (int) (Screen.PrimaryScreen.Bounds.Width / 5);

			this.Width = (int) (Screen.PrimaryScreen.Bounds.Width / 1.5d);
			this.Height = (int) (Screen.PrimaryScreen.Bounds.Height / 1.5d);

			Image1.Width = (int) VB6.ToPixelsUserWidth((float) (this.Width * 15), 5746, 408);
			Image1.Height = (int) VB6.ToPixelsUserHeight((float) (this.Height * 15), 2577, 249);

			Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			Image1.Top = (int) 0;
			Image1.Left = (int) 0;
			Image1.Visible = true;

			// Escondemos el bot�n

			cmdAceptar.Visible = false;

		}

		private void lb1_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			iCuenta = 1;
		}

		private void lb2_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			if (iCuenta == 1)
			{
				iCuenta = 2;
			}
			else
			{
				iCuenta = 0;
			}
		}

		private void lb3_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			if (iCuenta == 2)
			{
				iCuenta = 3;
			}
			else
			{
				iCuenta = 0;
			}
		}

		private void lb4_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			if (iCuenta == 3)
			{
				iCuenta = 4;
			}
			else
			{
				iCuenta = 0;
			}
		}
		private void frmAcerca_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}