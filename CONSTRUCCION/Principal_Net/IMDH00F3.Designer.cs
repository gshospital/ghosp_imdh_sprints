using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Principal
{
	partial class IMDH00F3
	{

		#region "Upgrade Support "
		private static IMDH00F3 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static IMDH00F3 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new IMDH00F3();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "mnuUsuario", "mnuNuevo", "mnuAbrir", "mnuCerrar", "g11", "mnuGuardar", "mnuGuardarComo", "g12", "mnuImprimirInformes", "g13", "mnuSalirItem", "mnuArchivo", "mnuDeshacer", "g2", "mnuCortar", "mnuCopiar", "mnuPegar", "g22", "mnuEliminar", "mnuEdicion", "mnuIconosGrandes", "mnuIconosPequeños", "mnuLista", "mnuVer", "_mnuOpcion_0", "mnuOpciones", "mnuCambiarUsuario", "mnuCambiarContraseña", "g4", "mnuCambiarBD", "g5", "mnuSincronizar", "g6", "mnu_LeerCorreo", "mnu_LeerMensajesPrivados", "mnuSesion", "_mnuTemporal_0", "_mnuTemporal_1", "_mnuTemporal_2", "mnuVentana", "mnu_accesootras", "mnu_otrasopciones", "mnuContenido", "mnuIndiceItem", "mnuTemasAyuda", "g73", "mnuUsarAyuda", "g72", "mnuAcercaDeItem", "mnuAyuda", "MainMenu1", "Principalbmp5", "Principalbmp", "Timer2", "Timer1", "_stBar_Panel1", "_stBar_Panel2", "_stBar_Panel3", "_stBar_Panel4", "stBar", "Line21", "Shape22", "Label5", "Label4", "Label1", "Line20", "Line19", "Line18", "Line17", "Line16", "Line15", "Line14", "Line13", "Line12", "Line11", "Line10", "Line9", "Line8", "Line7", "Line6", "Line5", "Line4", "Line3", "Line2", "Line1", "Shape21", "Shape20", "Shape19", "Shape18", "Shape17", "Shape16", "Shape15", "Shape14", "Shape13", "Shape12", "Shape11", "Shape10", "Shape9", "Shape8", "Shape7", "Shape6", "Shape5", "Shape4", "Shape3", "Shape2", "Shape1", "_LbEtiqueta_15", "Label3", "_LbEtiqueta_11", "_LbEtiqueta_10", "_LbEtiqueta_3", "_LbEtiqueta_8", "_LbEtiqueta_13", "_LbEtiqueta_14", "_LbEtiqueta_5", "_LbEtiqueta_4", "_LbEtiqueta_12", "_LbEtiqueta_6", "_LbEtiqueta_9", "_LbEtiqueta_7", "_LbEtiqueta_2", "_LbEtiqueta_0", "_LbEtiqueta_1", "imgIMDH", "_imgIconos_0", "Image1", "LbEtiqueta", "imgIconos", "mnuOpcion", "mnuTemporal", "ShapeContainer2"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadMenuItem mnuUsuario;
		public Telerik.WinControls.UI.RadMenuItem mnuNuevo;
		public Telerik.WinControls.UI.RadMenuItem mnuAbrir;
		public Telerik.WinControls.UI.RadMenuItem mnuCerrar;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g11;
		public Telerik.WinControls.UI.RadMenuItem mnuGuardar;
		public Telerik.WinControls.UI.RadMenuItem mnuGuardarComo;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g12;
		public Telerik.WinControls.UI.RadMenuItem mnuImprimirInformes;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g13;
		public Telerik.WinControls.UI.RadMenuItem mnuSalirItem;
		public Telerik.WinControls.UI.RadMenuItem mnuArchivo;
		public Telerik.WinControls.UI.RadMenuItem mnuDeshacer;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g2;
		public Telerik.WinControls.UI.RadMenuItem mnuCortar;
		public Telerik.WinControls.UI.RadMenuItem mnuCopiar;
		public Telerik.WinControls.UI.RadMenuItem mnuPegar;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g22;
		public Telerik.WinControls.UI.RadMenuItem mnuEliminar;
		public Telerik.WinControls.UI.RadMenuItem mnuEdicion;
		public Telerik.WinControls.UI.RadMenuItem mnuIconosGrandes;
		public Telerik.WinControls.UI.RadMenuItem mnuIconosPequeños;
		public Telerik.WinControls.UI.RadMenuItem mnuLista;
		public Telerik.WinControls.UI.RadMenuItem mnuVer;
		private Telerik.WinControls.UI.RadMenuItem _mnuOpcion_0;
		public Telerik.WinControls.UI.RadMenuItem mnuOpciones;
		public Telerik.WinControls.UI.RadMenuItem mnuCambiarUsuario;
		public Telerik.WinControls.UI.RadMenuItem mnuCambiarContraseña;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g4;
		public Telerik.WinControls.UI.RadMenuItem mnuCambiarBD;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g5;
		public Telerik.WinControls.UI.RadMenuItem mnuSincronizar;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g6;
		public Telerik.WinControls.UI.RadMenuItem mnu_LeerCorreo;
		public Telerik.WinControls.UI.RadMenuItem mnu_LeerMensajesPrivados;
		public Telerik.WinControls.UI.RadMenuItem mnuSesion;
		private Telerik.WinControls.UI.RadMenuItem _mnuTemporal_0;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _mnuTemporal_1;
		private Telerik.WinControls.UI.RadMenuItem _mnuTemporal_2;
		public Telerik.WinControls.UI.RadMenuItem mnuVentana;
		public Telerik.WinControls.UI.RadMenuItem mnu_accesootras;
		public Telerik.WinControls.UI.RadMenuItem mnu_otrasopciones;
		public Telerik.WinControls.UI.RadMenuItem mnuContenido;
		public Telerik.WinControls.UI.RadMenuItem mnuIndiceItem;
		public Telerik.WinControls.UI.RadMenuItem mnuTemasAyuda;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g73;
		public Telerik.WinControls.UI.RadMenuItem mnuUsarAyuda;
		public Telerik.WinControls.UI.RadMenuSeparatorItem g72;
		public Telerik.WinControls.UI.RadMenuItem mnuAcercaDeItem;
		public Telerik.WinControls.UI.RadMenuItem mnuAyuda;
		public System.Windows.Forms.PictureBox Principalbmp5;
		public System.Windows.Forms.PictureBox Principalbmp;
		public System.Windows.Forms.Timer Timer2;
		public System.Windows.Forms.Timer Timer1;
        public System.Windows.Forms.Panel panelShapes;


		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel1;
		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel2;
		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel3;        
		private Telerik.WinControls.UI.RadStatusBarPanelElement _stBar_Panel4;
		public Telerik.WinControls.UI.RadStatusStrip stBar;



		public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line21;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape22;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line20;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line19;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line18;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line17;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line16;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line15;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line14;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line13;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line12;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line11;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line10;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line9;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line8;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line7;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line6;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line5;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line4;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line3;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line2;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape21;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape20;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape19;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape18;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape17;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape16;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape15;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape14;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape13;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape12;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape11;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape10;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape9;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape8;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape7;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape6;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape5;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape4;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape3;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape2;
		public Microsoft.VisualBasic.PowerPacks.OvalShape Shape1;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_15;
		public Telerik.WinControls.UI.RadLabel Label3;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_11;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_10;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_3;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_8;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_13;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_14;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_5;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_4;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_12;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_6;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_9;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_7;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_2;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_0;
		private Telerik.WinControls.UI.RadLabel _LbEtiqueta_1;
		public System.Windows.Forms.PictureBox imgIMDH;
		private System.Windows.Forms.PictureBox _imgIconos_0;
		public System.Windows.Forms.PictureBox Image1;
		public Telerik.WinControls.UI.RadLabel[] LbEtiqueta = new Telerik.WinControls.UI.RadLabel[16];
		public System.Windows.Forms.PictureBox[] imgIconos = new System.Windows.Forms.PictureBox[1];
        public Telerik.WinControls.RadItem[]  mnuOpcion = new Telerik.WinControls.RadItem[1];
		public Telerik.WinControls.RadItem[]  mnuTemporal = new Telerik.WinControls.RadItem[3];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IMDH00F3));
            this.panelShapes = new System.Windows.Forms.Panel();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Line21 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Shape22 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Line20 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line19 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line18 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line17 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line16 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line15 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line14 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line13 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line12 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line11 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line10 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Shape21 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape20 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape19 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape18 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape17 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape16 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape15 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape14 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape13 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape12 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape11 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape10 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape9 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape8 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape7 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape6 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape5 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape4 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape3 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape2 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Shape1 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.mnuArchivo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuSalirItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuUsuario = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuAbrir = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCerrar = new Telerik.WinControls.UI.RadMenuItem();
            this.g11 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuGuardar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuGuardarComo = new Telerik.WinControls.UI.RadMenuItem();
            this.g12 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuImprimirInformes = new Telerik.WinControls.UI.RadMenuItem();
            this.g13 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuEdicion = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuDeshacer = new Telerik.WinControls.UI.RadMenuItem();
            this.g2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuCortar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuPegar = new Telerik.WinControls.UI.RadMenuItem();
            this.g22 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuEliminar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuVer = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuIconosGrandes = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuIconosPequeños = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuLista = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuOpciones = new Telerik.WinControls.UI.RadMenuItem();
            this._mnuOpcion_0 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuSesion = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCambiarUsuario = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCambiarContraseña = new Telerik.WinControls.UI.RadMenuItem();
            this.g4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuCambiarBD = new Telerik.WinControls.UI.RadMenuItem();
            this.g5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuSincronizar = new Telerik.WinControls.UI.RadMenuItem();
            this.g6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnu_LeerCorreo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_LeerMensajesPrivados = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuVentana = new Telerik.WinControls.UI.RadMenuItem();
            this._mnuTemporal_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._mnuTemporal_1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._mnuTemporal_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_otrasopciones = new Telerik.WinControls.UI.RadMenuItem();
            this.mnu_accesootras = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuAyuda = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuIndiceItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuTemasAyuda = new Telerik.WinControls.UI.RadMenuItem();
            this.g72 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuAcercaDeItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuContenido = new Telerik.WinControls.UI.RadMenuItem();
            this.g73 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuUsarAyuda = new Telerik.WinControls.UI.RadMenuItem();
            this.Principalbmp5 = new System.Windows.Forms.PictureBox();
            this.Principalbmp = new System.Windows.Forms.PictureBox();
            this.Timer2 = new System.Windows.Forms.Timer(this.components);
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.stBar = new Telerik.WinControls.UI.RadStatusStrip();
            this._stBar_Panel1 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this._stBar_Panel2 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this._stBar_Panel3 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this._stBar_Panel4 = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
            this.CDAyudaFont = new System.Windows.Forms.FontDialog();
            this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
            this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_15 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_11 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_10 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_3 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_8 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_13 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_14 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_5 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_4 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_12 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_6 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_9 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_7 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_2 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_0 = new Telerik.WinControls.UI.RadLabel();
            this._LbEtiqueta_1 = new Telerik.WinControls.UI.RadLabel();
            this.imgIMDH = new System.Windows.Forms.PictureBox();
            this._imgIconos_0 = new System.Windows.Forms.PictureBox();
            this.Image1 = new System.Windows.Forms.PictureBox();
            this.panelShapes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Principalbmp5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Principalbmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIMDH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._imgIconos_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panelShapes
            // 
            this.panelShapes.BackColor = System.Drawing.Color.Transparent;
            this.panelShapes.Controls.Add(this.Label5);
            this.panelShapes.Controls.Add(this.ShapeContainer2);
            this.panelShapes.Location = new System.Drawing.Point(0, 0);
            this.panelShapes.Name = "panelShapes";
            this.panelShapes.Size = new System.Drawing.Size(906, 636);
            this.panelShapes.TabIndex = 26;
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.Line21,
            this.Shape22,
            this.Line20,
            this.Line19,
            this.Line18,
            this.Line17,
            this.Line16,
            this.Line15,
            this.Line14,
            this.Line13,
            this.Line12,
            this.Line11,
            this.Line10,
            this.Line9,
            this.Line8,
            this.Line7,
            this.Line6,
            this.Line5,
            this.Line4,
            this.Line3,
            this.Line2,
            this.Line1,
            this.Shape21,
            this.Shape20,
            this.Shape19,
            this.Shape18,
            this.Shape17,
            this.Shape16,
            this.Shape15,
            this.Shape14,
            this.Shape13,
            this.Shape12,
            this.Shape11,
            this.Shape10,
            this.Shape9,
            this.Shape8,
            this.Shape7,
            this.Shape6,
            this.Shape5,
            this.Shape4,
            this.Shape3,
            this.Shape2,
            this.Shape1});
            this.ShapeContainer2.Size = new System.Drawing.Size(906, 636);
            this.ShapeContainer2.TabIndex = 26;
            this.ShapeContainer2.TabStop = false;
            // 
            // Line21
            // 
            this.Line21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line21.BorderWidth = 3;
            this.Line21.Enabled = false;
            this.Line21.Name = "Line21";
            this.Line21.X1 = 619;
            this.Line21.X2 = 491;
            this.Line21.Y1 = 119;
            this.Line21.Y2 = 151;
            // 
            // Shape22
            // 
            this.Shape22.BackColor = System.Drawing.SystemColors.Window;
            this.Shape22.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape22.Enabled = false;
            this.Shape22.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape22.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape22.Location = new System.Drawing.Point(592, 100);
            this.Shape22.Name = "Shape22";
            this.Shape22.Size = new System.Drawing.Size(41, 41);
            // 
            // Line20
            // 
            this.Line20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line20.BorderWidth = 3;
            this.Line20.Enabled = false;
            this.Line20.Name = "Line20";
            this.Line20.X1 = 258;
            this.Line20.X2 = 389;
            this.Line20.Y1 = 603;
            this.Line20.Y2 = 603;
            // 
            // Line19
            // 
            this.Line19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line19.BorderWidth = 3;
            this.Line19.Enabled = false;
            this.Line19.Name = "Line19";
            this.Line19.X1 = 269;
            this.Line19.X2 = 381;
            this.Line19.Y1 = 555;
            this.Line19.Y2 = 595;
            // 
            // Line18
            // 
            this.Line18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line18.BorderWidth = 3;
            this.Line18.Enabled = false;
            this.Line18.Name = "Line18";
            this.Line18.X1 = 297;
            this.Line18.X2 = 390;
            this.Line18.Y1 = 508;
            this.Line18.Y2 = 595;
            // 
            // Line17
            // 
            this.Line17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line17.BorderWidth = 3;
            this.Line17.Enabled = false;
            this.Line17.Name = "Line17";
            this.Line17.X1 = 286;
            this.Line17.X2 = 390;
            this.Line17.Y1 = 446;
            this.Line17.Y2 = 598;
            // 
            // Line16
            // 
            this.Line16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line16.BorderWidth = 3;
            this.Line16.Enabled = false;
            this.Line16.Name = "Line16";
            this.Line16.X1 = 394;
            this.Line16.X2 = 394;
            this.Line16.Y1 = 386;
            this.Line16.Y2 = 626;
            // 
            // Line15
            // 
            this.Line15.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line15.BorderWidth = 3;
            this.Line15.Enabled = false;
            this.Line15.Name = "Line15";
            this.Line15.X1 = 331;
            this.Line15.X2 = 403;
            this.Line15.Y1 = 420;
            this.Line15.Y2 = 604;
            // 
            // Line14
            // 
            this.Line14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line14.BorderWidth = 3;
            this.Line14.Enabled = false;
            this.Line14.Name = "Line14";
            this.Line14.X1 = 270;
            this.Line14.X2 = 334;
            this.Line14.Y1 = 177;
            this.Line14.Y2 = 289;
            // 
            // Line13
            // 
            this.Line13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line13.BorderWidth = 3;
            this.Line13.Enabled = false;
            this.Line13.Name = "Line13";
            this.Line13.X1 = 275;
            this.Line13.X2 = 411;
            this.Line13.Y1 = 167;
            this.Line13.Y2 = 303;
            // 
            // Line12
            // 
            this.Line12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line12.BorderWidth = 3;
            this.Line12.Enabled = false;
            this.Line12.Name = "Line12";
            this.Line12.X1 = 562;
            this.Line12.X2 = 666;
            this.Line12.Y1 = 541;
            this.Line12.Y2 = 581;
            // 
            // Line11
            // 
            this.Line11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line11.BorderWidth = 3;
            this.Line11.Enabled = false;
            this.Line11.Name = "Line11";
            this.Line11.X1 = 575;
            this.Line11.X2 = 671;
            this.Line11.Y1 = 534;
            this.Line11.Y2 = 534;
            // 
            // Line10
            // 
            this.Line10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line10.BorderWidth = 3;
            this.Line10.Enabled = false;
            this.Line10.Name = "Line10";
            this.Line10.X1 = 651;
            this.Line10.X2 = 563;
            this.Line10.Y1 = 477;
            this.Line10.Y2 = 525;
            // 
            // Line9
            // 
            this.Line9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line9.BorderWidth = 3;
            this.Line9.Enabled = false;
            this.Line9.Name = "Line9";
            this.Line9.X1 = 448;
            this.Line9.X2 = 544;
            this.Line9.Y1 = 336;
            this.Line9.Y2 = 504;
            // 
            // Line8
            // 
            this.Line8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line8.BorderWidth = 3;
            this.Line8.Enabled = false;
            this.Line8.Name = "Line8";
            this.Line8.X1 = 652;
            this.Line8.X2 = 548;
            this.Line8.Y1 = 444;
            this.Line8.Y2 = 524;
            // 
            // Line7
            // 
            this.Line7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line7.BorderWidth = 3;
            this.Line7.Enabled = false;
            this.Line7.Name = "Line7";
            this.Line7.X1 = 608;
            this.Line7.X2 = 544;
            this.Line7.Y1 = 394;
            this.Line7.Y2 = 522;
            // 
            // Line6
            // 
            this.Line6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line6.BorderWidth = 3;
            this.Line6.Enabled = false;
            this.Line6.Name = "Line6";
            this.Line6.X1 = 489;
            this.Line6.X2 = 641;
            this.Line6.Y1 = 177;
            this.Line6.Y2 = 329;
            // 
            // Line5
            // 
            this.Line5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line5.BorderWidth = 3;
            this.Line5.Enabled = false;
            this.Line5.Name = "Line5";
            this.Line5.X1 = 486;
            this.Line5.X2 = 638;
            this.Line5.Y1 = 162;
            this.Line5.Y2 = 266;
            // 
            // Line4
            // 
            this.Line4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line4.BorderWidth = 3;
            this.Line4.Enabled = false;
            this.Line4.Name = "Line4";
            this.Line4.X1 = 617;
            this.Line4.X2 = 753;
            this.Line4.Y1 = 131;
            this.Line4.Y2 = 195;
            // 
            // Line3
            // 
            this.Line3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line3.BorderWidth = 3;
            this.Line3.Enabled = false;
            this.Line3.Name = "Line3";
            this.Line3.X1 = 624;
            this.Line3.X2 = 736;
            this.Line3.Y1 = 124;
            this.Line3.Y2 = 148;
            // 
            // Line2
            // 
            this.Line2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line2.BorderWidth = 3;
            this.Line2.Enabled = false;
            this.Line2.Name = "Line2";
            this.Line2.X1 = 617;
            this.Line2.X2 = 745;
            this.Line2.Y1 = 117;
            this.Line2.Y2 = 93;
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Line1.BorderWidth = 3;
            this.Line1.Enabled = false;
            this.Line1.Name = "Line1";
            this.Line1.X1 = 464;
            this.Line1.X2 = 464;
            this.Line1.Y1 = 144;
            this.Line1.Y2 = 272;
            // 
            // Shape21
            // 
            this.Shape21.BackColor = System.Drawing.SystemColors.Window;
            this.Shape21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape21.Enabled = false;
            this.Shape21.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape21.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape21.Location = new System.Drawing.Point(244, 592);
            this.Shape21.Name = "Shape21";
            this.Shape21.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape20
            // 
            this.Shape20.BackColor = System.Drawing.SystemColors.Window;
            this.Shape20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape20.Enabled = false;
            this.Shape20.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape20.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape20.Location = new System.Drawing.Point(260, 544);
            this.Shape20.Name = "Shape20";
            this.Shape20.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape19
            // 
            this.Shape19.BackColor = System.Drawing.SystemColors.Window;
            this.Shape19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape19.Enabled = false;
            this.Shape19.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape19.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape19.Location = new System.Drawing.Point(284, 496);
            this.Shape19.Name = "Shape19";
            this.Shape19.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape18
            // 
            this.Shape18.BackColor = System.Drawing.SystemColors.Window;
            this.Shape18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape18.Enabled = false;
            this.Shape18.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape18.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape18.Location = new System.Drawing.Point(268, 432);
            this.Shape18.Name = "Shape18";
            this.Shape18.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape17
            // 
            this.Shape17.BackColor = System.Drawing.SystemColors.Window;
            this.Shape17.BorderColor = System.Drawing.Color.Blue;
            this.Shape17.Enabled = false;
            this.Shape17.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape17.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape17.Location = new System.Drawing.Point(316, 400);
            this.Shape17.Name = "Shape17";
            this.Shape17.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape16
            // 
            this.Shape16.BackColor = System.Drawing.SystemColors.Window;
            this.Shape16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape16.Enabled = false;
            this.Shape16.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape16.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape16.Location = new System.Drawing.Point(652, 568);
            this.Shape16.Name = "Shape16";
            this.Shape16.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape15
            // 
            this.Shape15.BackColor = System.Drawing.SystemColors.Window;
            this.Shape15.BorderColor = System.Drawing.Color.Blue;
            this.Shape15.Enabled = false;
            this.Shape15.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape15.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape15.Location = new System.Drawing.Point(660, 520);
            this.Shape15.Name = "Shape15";
            this.Shape15.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape14
            // 
            this.Shape14.BackColor = System.Drawing.SystemColors.Window;
            this.Shape14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape14.Enabled = false;
            this.Shape14.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape14.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape14.Location = new System.Drawing.Point(644, 464);
            this.Shape14.Name = "Shape14";
            this.Shape14.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape13
            // 
            this.Shape13.BackColor = System.Drawing.SystemColors.Window;
            this.Shape13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape13.Enabled = false;
            this.Shape13.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape13.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape13.Location = new System.Drawing.Point(644, 432);
            this.Shape13.Name = "Shape13";
            this.Shape13.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape12
            // 
            this.Shape12.BackColor = System.Drawing.SystemColors.Window;
            this.Shape12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape12.Enabled = false;
            this.Shape12.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape12.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape12.Location = new System.Drawing.Point(596, 384);
            this.Shape12.Name = "Shape12";
            this.Shape12.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape11
            // 
            this.Shape11.BackColor = System.Drawing.SystemColors.Window;
            this.Shape11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape11.Enabled = false;
            this.Shape11.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape11.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape11.Location = new System.Drawing.Point(320, 272);
            this.Shape11.Name = "Shape11";
            this.Shape11.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape10
            // 
            this.Shape10.BackColor = System.Drawing.SystemColors.Window;
            this.Shape10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape10.Enabled = false;
            this.Shape10.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape10.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape10.Location = new System.Drawing.Point(632, 320);
            this.Shape10.Name = "Shape10";
            this.Shape10.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape9
            // 
            this.Shape9.BackColor = System.Drawing.SystemColors.Window;
            this.Shape9.BorderColor = System.Drawing.Color.Blue;
            this.Shape9.Enabled = false;
            this.Shape9.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape9.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape9.Location = new System.Drawing.Point(632, 256);
            this.Shape9.Name = "Shape9";
            this.Shape9.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape8
            // 
            this.Shape8.BackColor = System.Drawing.SystemColors.Window;
            this.Shape8.BorderColor = System.Drawing.Color.Blue;
            this.Shape8.Enabled = false;
            this.Shape8.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape8.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape8.Location = new System.Drawing.Point(744, 184);
            this.Shape8.Name = "Shape8";
            this.Shape8.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape7
            // 
            this.Shape7.BackColor = System.Drawing.SystemColors.Window;
            this.Shape7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape7.Enabled = false;
            this.Shape7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape7.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape7.Location = new System.Drawing.Point(728, 136);
            this.Shape7.Name = "Shape7";
            this.Shape7.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape6
            // 
            this.Shape6.BackColor = System.Drawing.SystemColors.Window;
            this.Shape6.BorderColor = System.Drawing.Color.Blue;
            this.Shape6.Enabled = false;
            this.Shape6.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape6.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape6.Location = new System.Drawing.Point(740, 80);
            this.Shape6.Name = "Shape6";
            this.Shape6.Size = new System.Drawing.Size(25, 25);
            // 
            // Shape5
            // 
            this.Shape5.BackColor = System.Drawing.SystemColors.Window;
            this.Shape5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape5.Enabled = false;
            this.Shape5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape5.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape5.Location = new System.Drawing.Point(520, 504);
            this.Shape5.Name = "Shape5";
            this.Shape5.Size = new System.Drawing.Size(57, 57);
            // 
            // Shape4
            // 
            this.Shape4.BackColor = System.Drawing.SystemColors.Window;
            this.Shape4.BorderColor = System.Drawing.Color.Blue;
            this.Shape4.Enabled = false;
            this.Shape4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape4.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape4.Location = new System.Drawing.Point(360, 576);
            this.Shape4.Name = "Shape4";
            this.Shape4.Size = new System.Drawing.Size(57, 57);
            // 
            // Shape3
            // 
            this.Shape3.BackColor = System.Drawing.SystemColors.Window;
            this.Shape3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape3.Enabled = false;
            this.Shape3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape3.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape3.Location = new System.Drawing.Point(240, 136);
            this.Shape3.Name = "Shape3";
            this.Shape3.Size = new System.Drawing.Size(57, 57);
            // 
            // Shape2
            // 
            this.Shape2.BackColor = System.Drawing.SystemColors.Window;
            this.Shape2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape2.Enabled = false;
            this.Shape2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape2.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape2.Location = new System.Drawing.Point(448, 136);
            this.Shape2.Name = "Shape2";
            this.Shape2.Size = new System.Drawing.Size(57, 57);
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.Teal;
            this.Shape1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape1.Enabled = false;
            this.Shape1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Shape1.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.Shape1.Location = new System.Drawing.Point(356, 256);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(153, 153);
            // 
            // Label5
            // 
            this.Label5.BackColor = System.Drawing.Color.Transparent;
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Font = new System.Drawing.Font("Comic Sans MS", 26F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.Color.Silver;
            this.Label5.Location = new System.Drawing.Point(376, 312);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(109, 55);
            this.Label5.TabIndex = 20;
            this.Label5.Text = "iMDH";
            // 
            // mnuArchivo
            // 
            this.mnuArchivo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuSalirItem});
            this.mnuArchivo.Name = "mnuArchivo";
            this.mnuArchivo.Text = "&Archivo";
            // 
            // mnuSalirItem
            // 
            this.mnuSalirItem.Name = "mnuSalirItem";
            this.mnuSalirItem.Text = "&Salir";
            this.mnuSalirItem.Click += new System.EventHandler(this.mnuSalirItem_Click);
            // 
            // mnuUsuario
            // 
            this.mnuUsuario.Enabled = false;
            this.mnuUsuario.Name = "mnuUsuario";
            this.mnuUsuario.Text = "&Usuario";
            // 
            // mnuNuevo
            // 
            this.mnuNuevo.Name = "mnuNuevo";
            this.mnuNuevo.Text = "&Nuevo";
            // 
            // mnuAbrir
            // 
            this.mnuAbrir.Name = "mnuAbrir";
            this.mnuAbrir.Text = "A&brir";
            // 
            // mnuCerrar
            // 
            this.mnuCerrar.Name = "mnuCerrar";
            this.mnuCerrar.Text = "&Cerrar";
            // 
            // g11
            // 
            this.g11.Name = "g11";
            this.g11.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuGuardar
            // 
            this.mnuGuardar.Name = "mnuGuardar";
            this.mnuGuardar.Text = "&Guardar";
            // 
            // mnuGuardarComo
            // 
            this.mnuGuardarComo.Name = "mnuGuardarComo";
            this.mnuGuardarComo.Text = "G&uardar como ...";
            // 
            // g12
            // 
            this.g12.Name = "g12";
            this.g12.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuImprimirInformes
            // 
            this.mnuImprimirInformes.Name = "mnuImprimirInformes";
            this.mnuImprimirInformes.Text = "&Imprimir Informes";
            // 
            // g13
            // 
            this.g13.Name = "g13";
            this.g13.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuEdicion
            // 
            this.mnuEdicion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuDeshacer,
            this.g2,
            this.mnuCortar,
            this.mnuCopiar,
            this.mnuPegar,
            this.g22,
            this.mnuEliminar});
            this.mnuEdicion.Name = "mnuEdicion";
            this.mnuEdicion.Text = "&Edición";
            // 
            // mnuDeshacer
            // 
            this.mnuDeshacer.Enabled = false;
            this.mnuDeshacer.Name = "mnuDeshacer";
            this.mnuDeshacer.Text = "&Deshacer";
            // 
            // g2
            // 
            this.g2.Name = "g2";
            this.g2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuCortar
            // 
            this.mnuCortar.Enabled = false;
            this.mnuCortar.Name = "mnuCortar";
            this.mnuCortar.Text = "Cor&tar";
            // 
            // mnuCopiar
            // 
            this.mnuCopiar.Enabled = false;
            this.mnuCopiar.Name = "mnuCopiar";
            this.mnuCopiar.Text = "&Copiar";
            // 
            // mnuPegar
            // 
            this.mnuPegar.Enabled = false;
            this.mnuPegar.Name = "mnuPegar";
            this.mnuPegar.Text = "&Pegar";
            // 
            // g22
            // 
            this.g22.Name = "g22";
            this.g22.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuEliminar
            // 
            this.mnuEliminar.Enabled = false;
            this.mnuEliminar.Name = "mnuEliminar";
            this.mnuEliminar.Text = "E&liminar";
            // 
            // mnuVer
            // 
            this.mnuVer.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuIconosGrandes,
            this.mnuIconosPequeños,
            this.mnuLista});
            this.mnuVer.Name = "mnuVer";
            this.mnuVer.Text = "&Ver";
            // 
            // mnuIconosGrandes
            // 
            this.mnuIconosGrandes.IsChecked = true;
            this.mnuIconosGrandes.Name = "mnuIconosGrandes";
            this.mnuIconosGrandes.Text = "Iconos &Grandes";
            this.mnuIconosGrandes.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // mnuIconosPequeños
            // 
            this.mnuIconosPequeños.Enabled = false;
            this.mnuIconosPequeños.Name = "mnuIconosPequeños";
            this.mnuIconosPequeños.Text = "Iconos &Pequeños";
            // 
            // mnuLista
            // 
            this.mnuLista.Enabled = false;
            this.mnuLista.Name = "mnuLista";
            this.mnuLista.Text = "&Lista";
            // 
            // mnuOpciones
            // 
            this.mnuOpciones.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnuOpcion_0});
            this.mnuOpciones.Name = "mnuOpciones";
            this.mnuOpciones.Text = "&Opciones";
            // 
            // _mnuOpcion_0
            // 
            this._mnuOpcion_0.Name = "_mnuOpcion_0";
            this._mnuOpcion_0.Text = "";
            this._mnuOpcion_0.Click += new System.EventHandler(this.mnuOpcion_Click);
            // 
            // mnuSesion
            // 
            this.mnuSesion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuCambiarUsuario,
            this.mnuCambiarContraseña,
            this.g4,
            this.mnuCambiarBD,
            this.g5,
            this.mnuSincronizar,
            this.g6,
            this.mnu_LeerCorreo,
            this.mnu_LeerMensajesPrivados});
            this.mnuSesion.Name = "mnuSesion";
            this.mnuSesion.Text = "&Sesión";
            // 
            // mnuCambiarUsuario
            // 
            this.mnuCambiarUsuario.Name = "mnuCambiarUsuario";
            this.mnuCambiarUsuario.Text = "Cambiar &Usuario";
            this.mnuCambiarUsuario.Click += new System.EventHandler(this.mnuCambiarUsuario_Click);
            // 
            // mnuCambiarContraseña
            // 
            this.mnuCambiarContraseña.Name = "mnuCambiarContraseña";
            this.mnuCambiarContraseña.Text = "Cambiar &Contraseña";
            this.mnuCambiarContraseña.Click += new System.EventHandler(this.mnuCambiarContraseña_Click);
            // 
            // g4
            // 
            this.g4.Name = "g4";
            this.g4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuCambiarBD
            // 
            this.mnuCambiarBD.Name = "mnuCambiarBD";
            this.mnuCambiarBD.Text = "Cambiar Conexion a Base de Datos";
            this.mnuCambiarBD.Click += new System.EventHandler(this.mnuCambiarBD_Click);
            // 
            // g5
            // 
            this.g5.Name = "g5";
            this.g5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuSincronizar
            // 
            this.mnuSincronizar.Name = "mnuSincronizar";
            this.mnuSincronizar.Text = "&Sincronizar Fecha/Hora";
            this.mnuSincronizar.Click += new System.EventHandler(this.mnuSincronizar_Click);
            // 
            // g6
            // 
            this.g6.Name = "g6";
            this.g6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnu_LeerCorreo
            // 
            this.mnu_LeerCorreo.Name = "mnu_LeerCorreo";
            this.mnu_LeerCorreo.Text = "&Leer correo";
            this.mnu_LeerCorreo.Click += new System.EventHandler(this.mnu_LeerCorreo_Click);
            // 
            // mnu_LeerMensajesPrivados
            // 
            this.mnu_LeerMensajesPrivados.Name = "mnu_LeerMensajesPrivados";
            this.mnu_LeerMensajesPrivados.Text = "Mensajería Privada";
            this.mnu_LeerMensajesPrivados.Click += new System.EventHandler(this.mnu_LeerMensajesPrivados_Click);
            // 
            // mnuVentana
            // 
            this.mnuVentana.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._mnuTemporal_0,
            this._mnuTemporal_1,
            this._mnuTemporal_2});
            this.mnuVentana.Name = "mnuVentana";
            this.mnuVentana.Text = "Ven&tana";
            // 
            // _mnuTemporal_0
            // 
            this._mnuTemporal_0.Name = "_mnuTemporal_0";
            this._mnuTemporal_0.Text = "&Organizar Iconos";
            this._mnuTemporal_0.Click += new System.EventHandler(this.mnuTemporal_Click);
            // 
            // _mnuTemporal_1
            // 
            this._mnuTemporal_1.Name = "_mnuTemporal_1";
            this._mnuTemporal_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._mnuTemporal_1.Click += new System.EventHandler(this.mnuTemporal_Click);
            // 
            // _mnuTemporal_2
            // 
            this._mnuTemporal_2.Name = "_mnuTemporal_2";
            this._mnuTemporal_2.Text = "";
            this._mnuTemporal_2.Click += new System.EventHandler(this.mnuTemporal_Click);
            // 
            // mnu_otrasopciones
            // 
            this.mnu_otrasopciones.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnu_accesootras});
            this.mnu_otrasopciones.Name = "mnu_otrasopciones";
            this.mnu_otrasopciones.Text = "O&tras opciones";
            // 
            // mnu_accesootras
            // 
            this.mnu_accesootras.Name = "mnu_accesootras";
            this.mnu_accesootras.Text = "Acceso a otras opciones";
            this.mnu_accesootras.Click += new System.EventHandler(this.mnu_accesootras_Click);
            // 
            // mnuAyuda
            // 
            this.mnuAyuda.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuIndiceItem,
            this.mnuTemasAyuda,
            this.g72,
            this.mnuAcercaDeItem});
            this.mnuAyuda.Name = "mnuAyuda";
            this.mnuAyuda.Text = "A&yuda";
            // 
            // mnuIndiceItem
            // 
            this.mnuIndiceItem.Name = "mnuIndiceItem";
            this.mnuIndiceItem.Text = "&Indice";
            this.mnuIndiceItem.Click += new System.EventHandler(this.mnuIndiceItem_Click);
            // 
            // mnuTemasAyuda
            // 
            this.mnuTemasAyuda.Name = "mnuTemasAyuda";
            this.mnuTemasAyuda.Text = "&Temas de Ayuda";
            this.mnuTemasAyuda.Click += new System.EventHandler(this.mnuTemasAyuda_Click);
            // 
            // g72
            // 
            this.g72.Name = "g72";
            this.g72.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuAcercaDeItem
            // 
            this.mnuAcercaDeItem.Name = "mnuAcercaDeItem";
            this.mnuAcercaDeItem.Text = "A&cerca de ";
            this.mnuAcercaDeItem.Click += new System.EventHandler(this.mnuAcercaDeItem_Click);
            // 
            // mnuContenido
            // 
            this.mnuContenido.Name = "mnuContenido";
            this.mnuContenido.Text = "&Contenido";
            this.mnuContenido.Click += new System.EventHandler(this.mnuContenido_Click);
            // 
            // g73
            // 
            this.g73.Name = "g73";
            this.g73.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuUsarAyuda
            // 
            this.mnuUsarAyuda.Name = "mnuUsarAyuda";
            this.mnuUsarAyuda.Text = "&C&omo usar la Ayuda";
            this.mnuUsarAyuda.Click += new System.EventHandler(this.mnuUsarAyuda_Click);
            // 
            // Principalbmp5
            // 
            this.Principalbmp5.BackColor = System.Drawing.SystemColors.Control;
            this.Principalbmp5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Principalbmp5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Principalbmp5.Image = ((System.Drawing.Image)(resources.GetObject("Principalbmp5.Image")));
            this.Principalbmp5.Location = new System.Drawing.Point(24, 208);
            this.Principalbmp5.Name = "Principalbmp5";
            this.Principalbmp5.Size = new System.Drawing.Size(65, 49);
            this.Principalbmp5.TabIndex = 22;
            this.Principalbmp5.TabStop = false;
            this.Principalbmp5.Visible = false;
            // 
            // Principalbmp
            // 
            this.Principalbmp.BackColor = System.Drawing.SystemColors.Control;
            this.Principalbmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Principalbmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.Principalbmp.Image = ((System.Drawing.Image)(resources.GetObject("Principalbmp.Image")));
            this.Principalbmp.Location = new System.Drawing.Point(16, 144);
            this.Principalbmp.Name = "Principalbmp";
            this.Principalbmp.Size = new System.Drawing.Size(65, 49);
            this.Principalbmp.TabIndex = 21;
            this.Principalbmp.TabStop = false;
            this.Principalbmp.Visible = false;
            // 
            // Timer2
            // 
            this.Timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // Timer1
            // 
            this.Timer1.Interval = 10000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // stBar
            // 
            this.stBar.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stBar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._stBar_Panel1,
            this._stBar_Panel2,
            this._stBar_Panel3,
            this._stBar_Panel4});
            this.stBar.Location = new System.Drawing.Point(0, 740);
            this.stBar.Name = "stBar";
            this.stBar.Size = new System.Drawing.Size(994, 22);
            this.stBar.TabIndex = 17;
            // 
            // _stBar_Panel1
            // 
            this._stBar_Panel1.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel1.AutoSize = false;
            this._stBar_Panel1.Bounds = new System.Drawing.Rectangle(0, 0, 129, 22);
            this._stBar_Panel1.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel1.Name = "_stBar_Panel1";
            this.stBar.SetSpring(this._stBar_Panel1, false);
            this._stBar_Panel1.Tag = "";
            this._stBar_Panel1.Text = "Usuario";
            // 
            // _stBar_Panel2
            // 
            this._stBar_Panel2.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel2.AutoSize = false;
            this._stBar_Panel2.Bounds = new System.Drawing.Rectangle(0, 0, 296, 22);
            this._stBar_Panel2.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel2.Name = "_stBar_Panel2";
            this.stBar.SetSpring(this._stBar_Panel2, false);
            this._stBar_Panel2.Tag = "";
            // 
            // _stBar_Panel3
            // 
            this._stBar_Panel3.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel3.AutoSize = false;
            this._stBar_Panel3.Bounds = new System.Drawing.Rectangle(0, 0, 176, 22);
            this._stBar_Panel3.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel3.Name = "_stBar_Panel3";
            this.stBar.SetSpring(this._stBar_Panel3, false);
            this._stBar_Panel3.Tag = "";
            this._stBar_Panel3.Text = "Base de Datos";
            // 
            // _stBar_Panel4
            // 
            this._stBar_Panel4.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._stBar_Panel4.AutoSize = false;
            this._stBar_Panel4.Bounds = new System.Drawing.Rectangle(0, 0, 330, 22);
            this._stBar_Panel4.Margin = new System.Windows.Forms.Padding(0);
            this._stBar_Panel4.Name = "_stBar_Panel4";
            this.stBar.SetSpring(this._stBar_Panel4, false);
            this._stBar_Panel4.Tag = "";
            // 
            // Label4
            // 
            this.Label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.ForeColor = System.Drawing.Color.White;
            this.Label4.Location = new System.Drawing.Point(56, 24);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(82, 18);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "Color etiquetas";
            this.Label4.Visible = false;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(113)))), ((int)(((byte)(254)))));
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(160, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(72, 18);
            this.Label1.TabIndex = 18;
            this.Label1.Text = "Color Pelotas";
            this.Label1.Visible = false;
            // 
            // _LbEtiqueta_15
            // 
            this._LbEtiqueta_15.AutoSize = false;
            this._LbEtiqueta_15.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_15.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_15.Font = new System.Drawing.Font("Comic Sans MS", 13.7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_15.Location = new System.Drawing.Point(-16, 272);
            this._LbEtiqueta_15.Name = "_LbEtiqueta_15";
            this._LbEtiqueta_15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_15.Size = new System.Drawing.Size(323, 30);
            this._LbEtiqueta_15.TabIndex = 16;
            this._LbEtiqueta_15.Text = "OTros Profesionales Asistenciales";
            this._LbEtiqueta_15.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_15.Visible = false;
            this._LbEtiqueta_15.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_15.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(15, 36);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(39, 18);
            this.Label3.TabIndex = 15;
            this.Label3.Text = "Label3";
            this.Label3.Visible = false;
            // 
            // _LbEtiqueta_11
            // 
            this._LbEtiqueta_11.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_11.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_11.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_11.Location = new System.Drawing.Point(56, 428);
            this._LbEtiqueta_11.Name = "_LbEtiqueta_11";
            this._LbEtiqueta_11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_11.Size = new System.Drawing.Size(189, 30);
            this._LbEtiqueta_11.TabIndex = 14;
            this._LbEtiqueta_11.Text = "Sistema de Gestión";
            this._LbEtiqueta_11.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_11.Visible = false;
            this._LbEtiqueta_11.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_11.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_10
            // 
            this._LbEtiqueta_10.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_10.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_10.Location = new System.Drawing.Point(696, 512);
            this._LbEtiqueta_10.Name = "_LbEtiqueta_10";
            this._LbEtiqueta_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_10.Size = new System.Drawing.Size(94, 30);
            this._LbEtiqueta_10.TabIndex = 13;
            this._LbEtiqueta_10.Text = "Dietética";
            this._LbEtiqueta_10.Visible = false;
            this._LbEtiqueta_10.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_10.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_3
            // 
            this._LbEtiqueta_3.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_3.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_3.Location = new System.Drawing.Point(680, 464);
            this._LbEtiqueta_3.Name = "_LbEtiqueta_3";
            this._LbEtiqueta_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_3.Size = new System.Drawing.Size(112, 30);
            this._LbEtiqueta_3.TabIndex = 12;
            this._LbEtiqueta_3.Text = "Enfermería";
            this._LbEtiqueta_3.Visible = false;
            this._LbEtiqueta_3.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_8
            // 
            this._LbEtiqueta_8.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_8.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_8.Location = new System.Drawing.Point(680, 416);
            this._LbEtiqueta_8.Name = "_LbEtiqueta_8";
            this._LbEtiqueta_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_8.Size = new System.Drawing.Size(174, 30);
            this._LbEtiqueta_8.TabIndex = 11;
            this._LbEtiqueta_8.Text = "Servicios Médicos";
            this._LbEtiqueta_8.Visible = false;
            this._LbEtiqueta_8.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_8.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_13
            // 
            this._LbEtiqueta_13.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_13.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_13.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_13.Location = new System.Drawing.Point(16, 544);
            this._LbEtiqueta_13.Name = "_LbEtiqueta_13";
            this._LbEtiqueta_13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_13.Size = new System.Drawing.Size(213, 30);
            this._LbEtiqueta_13.TabIndex = 10;
            this._LbEtiqueta_13.Text = "Información Gerencial";
            this._LbEtiqueta_13.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_13.Visible = false;
            this._LbEtiqueta_13.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_13.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_14
            // 
            this._LbEtiqueta_14.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_14.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_14.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_14.Location = new System.Drawing.Point(680, 568);
            this._LbEtiqueta_14.Name = "_LbEtiqueta_14";
            this._LbEtiqueta_14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_14.Size = new System.Drawing.Size(51, 30);
            this._LbEtiqueta_14.TabIndex = 9;
            this._LbEtiqueta_14.Text = "Nido";
            this._LbEtiqueta_14.Visible = false;
            this._LbEtiqueta_14.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_14.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_5
            // 
            this._LbEtiqueta_5.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_5.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_5.Location = new System.Drawing.Point(0, 488);
            this._LbEtiqueta_5.Name = "_LbEtiqueta_5";
            this._LbEtiqueta_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_5.Size = new System.Drawing.Size(240, 30);
            this._LbEtiqueta_5.TabIndex = 8;
            this._LbEtiqueta_5.Text = "Información del Paciente";
            this._LbEtiqueta_5.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_5.Visible = false;
            this._LbEtiqueta_5.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_4
            // 
            this._LbEtiqueta_4.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_4.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_4.Location = new System.Drawing.Point(56, 392);
            this._LbEtiqueta_4.Name = "_LbEtiqueta_4";
            this._LbEtiqueta_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_4.Size = new System.Drawing.Size(78, 30);
            this._LbEtiqueta_4.TabIndex = 7;
            this._LbEtiqueta_4.Text = "Archivo";
            this._LbEtiqueta_4.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_4.Visible = false;
            this._LbEtiqueta_4.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_12
            // 
            this._LbEtiqueta_12.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_12.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_12.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_12.Location = new System.Drawing.Point(48, 592);
            this._LbEtiqueta_12.Name = "_LbEtiqueta_12";
            this._LbEtiqueta_12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_12.Size = new System.Drawing.Size(159, 30);
            this._LbEtiqueta_12.TabIndex = 6;
            this._LbEtiqueta_12.Text = "Admon. Sistema";
            this._LbEtiqueta_12.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            this._LbEtiqueta_12.Visible = false;
            this._LbEtiqueta_12.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_12.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_6
            // 
            this._LbEtiqueta_6.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_6.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_6.Location = new System.Drawing.Point(624, 376);
            this._LbEtiqueta_6.Name = "_LbEtiqueta_6";
            this._LbEtiqueta_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_6.Size = new System.Drawing.Size(110, 30);
            this._LbEtiqueta_6.TabIndex = 5;
            this._LbEtiqueta_6.Text = "Quirófanos";
            this._LbEtiqueta_6.Visible = false;
            this._LbEtiqueta_6.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_9
            // 
            this._LbEtiqueta_9.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_9.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_9.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_9.Location = new System.Drawing.Point(680, 312);
            this._LbEtiqueta_9.Name = "_LbEtiqueta_9";
            this._LbEtiqueta_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_9.Size = new System.Drawing.Size(187, 30);
            this._LbEtiqueta_9.TabIndex = 4;
            this._LbEtiqueta_9.Text = "Servicios Centrales";
            this._LbEtiqueta_9.Visible = false;
            this._LbEtiqueta_9.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_9.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_7
            // 
            this._LbEtiqueta_7.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_7.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_7.Location = new System.Drawing.Point(672, 256);
            this._LbEtiqueta_7.Name = "_LbEtiqueta_7";
            this._LbEtiqueta_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_7.Size = new System.Drawing.Size(160, 30);
            this._LbEtiqueta_7.TabIndex = 3;
            this._LbEtiqueta_7.Text = "Radiodiagnóstico";
            this._LbEtiqueta_7.Visible = false;
            this._LbEtiqueta_7.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_7.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_2
            // 
            this._LbEtiqueta_2.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_2.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_2.Location = new System.Drawing.Point(792, 182);
            this._LbEtiqueta_2.Name = "_LbEtiqueta_2";
            this._LbEtiqueta_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_2.Size = new System.Drawing.Size(95, 30);
            this._LbEtiqueta_2.TabIndex = 2;
            this._LbEtiqueta_2.Text = "Consultas";
            this._LbEtiqueta_2.Visible = false;
            this._LbEtiqueta_2.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_0
            // 
            this._LbEtiqueta_0.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_0.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_0.Location = new System.Drawing.Point(769, 130);
            this._LbEtiqueta_0.Name = "_LbEtiqueta_0";
            this._LbEtiqueta_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_0.Size = new System.Drawing.Size(98, 30);
            this._LbEtiqueta_0.TabIndex = 1;
            this._LbEtiqueta_0.Text = "Urgencias";
            this._LbEtiqueta_0.Visible = false;
            this._LbEtiqueta_0.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_0.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // _LbEtiqueta_1
            // 
            this._LbEtiqueta_1.BackColor = System.Drawing.Color.Transparent;
            this._LbEtiqueta_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._LbEtiqueta_1.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LbEtiqueta_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._LbEtiqueta_1.Location = new System.Drawing.Point(774, 78);
            this._LbEtiqueta_1.Name = "_LbEtiqueta_1";
            this._LbEtiqueta_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LbEtiqueta_1.Size = new System.Drawing.Size(89, 30);
            this._LbEtiqueta_1.TabIndex = 0;
            this._LbEtiqueta_1.Text = "Admisión";
            this._LbEtiqueta_1.Visible = false;
            this._LbEtiqueta_1.Click += new System.EventHandler(this.LbEtiqueta_Click);
            this._LbEtiqueta_1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbEtiqueta_MouseMove);
            // 
            // imgIMDH
            // 
            this.imgIMDH.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgIMDH.Image = ((System.Drawing.Image)(resources.GetObject("imgIMDH.Image")));
            this.imgIMDH.Location = new System.Drawing.Point(285, 94);
            this.imgIMDH.Name = "imgIMDH";
            this.imgIMDH.Size = new System.Drawing.Size(64, 64);
            this.imgIMDH.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgIMDH.TabIndex = 23;
            this.imgIMDH.TabStop = false;
            this.imgIMDH.Visible = false;
            // 
            // _imgIconos_0
            // 
            this._imgIconos_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._imgIconos_0.Image = ((System.Drawing.Image)(resources.GetObject("_imgIconos_0.Image")));
            this._imgIconos_0.Location = new System.Drawing.Point(153, 90);
            this._imgIconos_0.Name = "_imgIconos_0";
            this._imgIconos_0.Size = new System.Drawing.Size(64, 64);
            this._imgIconos_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._imgIconos_0.TabIndex = 24;
            this._imgIconos_0.TabStop = false;
            this._imgIconos_0.Visible = false;
            this._imgIconos_0.DoubleClick += new System.EventHandler(this.imgIconos_DoubleClick);
            this._imgIconos_0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imgIconos_MouseDown);
            this._imgIconos_0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imgIconos_MouseUp);
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(0, 24);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(56, 56);
            this.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 25;
            this.Image1.TabStop = false;
            this.Image1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Image1_MouseMove);
            // 
            // IMDH00F3
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(932, 653);
            this.ControlBox = false;
            this.Controls.Add(this.Principalbmp5);
            this.Controls.Add(this.Principalbmp);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this._LbEtiqueta_15);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this._LbEtiqueta_11);
            this.Controls.Add(this._LbEtiqueta_10);
            this.Controls.Add(this._LbEtiqueta_3);
            this.Controls.Add(this._LbEtiqueta_8);
            this.Controls.Add(this._LbEtiqueta_13);
            this.Controls.Add(this._LbEtiqueta_14);
            this.Controls.Add(this._LbEtiqueta_5);
            this.Controls.Add(this._LbEtiqueta_4);
            this.Controls.Add(this._LbEtiqueta_12);
            this.Controls.Add(this._LbEtiqueta_6);
            this.Controls.Add(this._LbEtiqueta_9);
            this.Controls.Add(this._LbEtiqueta_7);
            this.Controls.Add(this._LbEtiqueta_2);
            this.Controls.Add(this._LbEtiqueta_0);
            this.Controls.Add(this._LbEtiqueta_1);
            this.Controls.Add(this.imgIMDH);
            this.Controls.Add(this._imgIconos_0);
            this.Controls.Add(this.Image1);
            this.Controls.Add(this.panelShapes);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ForeColor = System.Drawing.Color.Gray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(8, 24);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IMDH00F3";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Activated += new System.EventHandler(this.IMDH00F3_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.IMDH00F3_Closed);
            this.Load += new System.EventHandler(this.IMDH00F3_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.IMDH00F3_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.IMDH00F3_DragEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IMDH00F3_MouseMove);
            this.Resize += new System.EventHandler(this.IMDH00F3_Resize);
            this.panelShapes.ResumeLayout(false);
            this.panelShapes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Principalbmp5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Principalbmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LbEtiqueta_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIMDH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._imgIconos_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializemnuTemporal();
			InitializemnuOpcion();
			InitializeimgIconos();
			InitializeLbEtiqueta();
			//This form is an MDI child.
			//This code simulates the VB6 
			// functionality of automatically
			// loading and showing an MDI
			// child's parent.
			this.MdiParent = Principal.IMDH00F1.DefInstance;
			Principal.IMDH00F1.DefInstance.Show();
		}
		void InitializemnuTemporal()
		{
			this.mnuTemporal = new Telerik.WinControls.RadItem[3];
			this.mnuTemporal[0] = _mnuTemporal_0;
			this.mnuTemporal[1] = _mnuTemporal_1;
			this.mnuTemporal[2] = _mnuTemporal_2;
		}
		void InitializemnuOpcion()
		{
			this.mnuOpcion = new Telerik.WinControls.RadItem[1];
			this.mnuOpcion[0] = _mnuOpcion_0;
		}
		void InitializeimgIconos()
		{
			this.imgIconos = new System.Windows.Forms.PictureBox[1];
			this.imgIconos[0] = _imgIconos_0;
		}
		void InitializeLbEtiqueta()
		{
			this.LbEtiqueta = new Telerik.WinControls.UI.RadLabel[16];
			this.LbEtiqueta[15] = _LbEtiqueta_15;
			this.LbEtiqueta[11] = _LbEtiqueta_11;
			this.LbEtiqueta[10] = _LbEtiqueta_10;
			this.LbEtiqueta[3] = _LbEtiqueta_3;
			this.LbEtiqueta[8] = _LbEtiqueta_8;
			this.LbEtiqueta[13] = _LbEtiqueta_13;
			this.LbEtiqueta[14] = _LbEtiqueta_14;
			this.LbEtiqueta[5] = _LbEtiqueta_5;
			this.LbEtiqueta[4] = _LbEtiqueta_4;
			this.LbEtiqueta[12] = _LbEtiqueta_12;
			this.LbEtiqueta[6] = _LbEtiqueta_6;
			this.LbEtiqueta[9] = _LbEtiqueta_9;
			this.LbEtiqueta[7] = _LbEtiqueta_7;
			this.LbEtiqueta[2] = _LbEtiqueta_2;
			this.LbEtiqueta[0] = _LbEtiqueta_0;
			this.LbEtiqueta[1] = _LbEtiqueta_1;
		}
        #endregion
    }
}