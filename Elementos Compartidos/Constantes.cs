﻿using System;
using System.Windows.Forms;
using System.IO;
namespace ElementosCompartidos
{
    public class Constantes
    {  
        private static Constantes _DefineAyuda;
        private static bool _DefInstance;
        private string _PathAyuda = Path.GetDirectoryName(Application.ExecutablePath);
        public string _FicheroAyuda = "Ayuda_Principal.hlp";
        public static Constantes DefInstance    
  
        {
            get
            {
                if (_DefineAyuda == null)
                {
                    _DefInstance = true;
                    _DefineAyuda = new Constantes();
                    _DefInstance = false;
                }
                return _DefineAyuda;
            }
            set
            {
                _DefineAyuda = value;
            }
        }

        public string FICHEROAYUDAPRINCIPAL
        {
            get
            {
                return _FicheroAyuda;
            }
            set
            {
                _FicheroAyuda = value;
            }
        }    

        public string PATHAYUDA
        {
            get
            {
                return _PathAyuda;
            }
            set
            {
                _PathAyuda = value;
            }
        }    
        
    }

    /// <summary>
    /// indra - ralopezn - 01/04/2016
    /// Clase que permite manejar las ayudas.
    /// </summary>
    public class CDAyuda : IDisposable
    {
        public const string FICHERO_AYUDA_PRINCIPAL = "Ayuda_Principal.hlp";
        public const string FICHERO_AYUDA_ADMISION = "AYUDA_ADMISION.HLP";
        public const string FICHERO_AYUDA_OTRASDLLS = "ayuda_dlls.hlp";
        public const string FICHERO_AYUDA_FILIACION = "Ayuda_Filiacion.HLP";
        public const string FICHERO_AYUDA_DLLS_ASTERISCO = "*DLL*.HLP";

        /// <summary>
        /// https://msdn.microsoft.com/en-us/library/ff650316.aspx
        /// Tipo de singleton a utilizar. Instancia Estatica
        /// </summary>
        private static readonly CDAyuda _instance = new CDAyuda();

        /// <summary>
        /// Ruta donde se almacena la ayuda
        /// </summary>
        private string _pathAyuda;

        /// <summary>
        /// Control asociado a la ayuda
        /// </summary>
        private Control _parent;

        /// <summary>
        /// Especifica el tipo de navegación que se mostrará en la ayuda.
        /// </summary>
        public HelpNavigator HelpNavigator { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        private CDAyuda()
        {
            _pathAyuda = string.Empty;
            _parent = null;
            HelpNavigator = HelpNavigator.TableOfContents;
        }
        
        /// <summary>
        /// Instancia de tipo Singleton
        /// </summary>
        public static CDAyuda Instance { get { return _instance; } }

        /// <summary>
        /// indra - ralopezn - 01/04/2016
        /// Método que permite instanciar la ayuda al control
        /// </summary>
        /// <param name="parent">Control donde se asociará la ayuda</param>
        /// <param name="nameHelpFile">Nombre del archivo de ayuda</param>
        public void setHelpFile(Control parent, string nameHelpFile)
        {
            _parent = parent;
            _pathAyuda = Path.GetDirectoryName(Application.ExecutablePath) + @"\" + nameHelpFile;
        }

        public string getHelpFile()
        {
            return "";
        }

        public void setHelpContext(object obj)
        {

        }

        /// <summary>
        /// indra - ralopezn - 01/04/2016
        /// Método que permite mostrar la ayuda, en caso de no encontrar la ruta, se puede 
        /// especificar desde un DialogFile.
        /// </summary>
        public void ShowHelp()
        {
            try
            {
                Help.ShowHelp(_parent, _pathAyuda, this.HelpNavigator);
            }
            catch (Exception ex)
            {
                DialogResult Result = Telerik.WinControls
                                             .RadMessageBox
                                             .Show("No se puede hallar el archivo " + _pathAyuda + "." + " ¿Desea Intentar hallarlo usted mismo?", 
                                                   "Ayuda de Windows", 
                                                   MessageBoxButtons.YesNo, 
                                                   Telerik.WinControls.RadMessageIcon.Question);
                if (Result == System.Windows.Forms.DialogResult.Yes)
                {
                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        _pathAyuda = openFileDialog1.FileName;
                        Help.ShowHelp(_parent, _pathAyuda, this.HelpNavigator);
                    }
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~CDAyuda() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

       