using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ElementosCompartidos
{
	internal static class mbDirectorioActivo
	{


		public static string vstTipoAutenticacion = String.Empty;
		public static string vstAutenticacionUsuario = String.Empty;
		public static string vstDominioAutenticacion = String.Empty;

		public static string viTama�oPassword = String.Empty;

		public static string gstTipoAutenticacion_Actual = String.Empty;
		public static string gstDominio_Actual = String.Empty;
		public static string gstAutenticacionUsuario_Actual = String.Empty;

		private const int SEC_E_OK = 0;
		private const int HEAP_ZERO_MEMORY = 0x8;
		private const int SEC_WINNT_AUTH_IDENTITY_ANSI = 0x1;
		private const int SECBUFFER_TOKEN = 0x2;
		private const int SECURITY_NATIVE_DREP = 0x10;
		private const int SECPKG_CRED_INBOUND = 0x1;
		private const int SECPKG_CRED_OUTBOUND = 0x2;
		private const int SEC_I_CONTINUE_NEEDED = 0x90312;
		private const int SEC_I_COMPLETE_NEEDED = 0x90313;
		private const int SEC_I_COMPLETE_AND_CONTINUE = 0x90314;
		private const int VER_PLATFORM_WIN32_NT = 0x2;

		[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
		private struct SecPkgInfo
		{
			public int fCapabilities;
			public short wVersion;
			public short wRPCID;
			public int cbMaxToken;
			public int Name;
			public int Comment;
		}

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[StructLayout(LayoutKind.Sequential)]
		//public struct SecHandle
		//{
			//public int dwLower;
			//public int dwUpper;
		//};

		[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
		private struct AUTH_SEQ
		{
			public bool fInitialized;
			public bool fHaveCredHandle;
			public bool fHaveCtxtHandle;
			public UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle hcred;
			public UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle hctxt;
			public static AUTH_SEQ CreateInstance()
			{
					AUTH_SEQ result = new AUTH_SEQ();
					return result;
			}
		}

		[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
		private struct SEC_WINNT_AUTH_IDENTITY
		{
			public string User;
			public int UserLength;
			public string Domain;
			public int DomainLength;
			public string Password;
			public int PasswordLength;
			public int Flags;
			public static SEC_WINNT_AUTH_IDENTITY CreateInstance()
			{
					SEC_WINNT_AUTH_IDENTITY result = new SEC_WINNT_AUTH_IDENTITY();
					result.User = String.Empty;
					result.Domain = String.Empty;
					result.Password = String.Empty;
					return result;
			}
		}

		[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
		private struct SEC_WINNT_AUTH_IDENTITYL
		{
			public int User;
			public int UserLength;
			public int Domain;
			public int DomainLength;
			public int Password;
			public int PasswordLength;
			public int Flags;
		}

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[StructLayout(LayoutKind.Sequential)]
		//public struct TimeStamp
		//{
			//public int LowPart;
			//public int HighPart;
		//};

		[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
		private struct SecBuffer
		{
			public int cbBuffer;
			public int BufferType;
			public int pvBuffer;
		}

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[StructLayout(LayoutKind.Sequential)]
		//public struct SecBufferDesc
		//{
			//public int ulVersion;
			//public int cBuffers;
			//public int pBuffers;
		//};

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
		//public struct OSVERSIONINFO
		//{
			//public int OSVSize;
			//public int dwVerMajor;
			//public int dwVerMinor;
			//public int dwBuildNumber;
			//public int PlatformID;
			//[System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst=128)]
			//private char[] _szCSDVersion;
			//public string szCSDVersion
			//{
				//get
				//{
					//return new string(_szCSDVersion);
				//}
				//set
				//{
					//MemoryHelper.CopyValueToArray(_szCSDVersion, value);
				//}
			//}
			//
			//private static void InitStruct(ref OSVERSIONINFO result, bool init)
			//{
					//result._szCSDVersion = new char[128];
			//}
			//public static OSVERSIONINFO CreateInstance()
			//{
					//OSVERSIONINFO result = new OSVERSIONINFO();
					//InitStruct(ref result, true);
					//return result;
			//}
			//public OSVERSIONINFO Clone()
			//{
					//OSVERSIONINFO result = this;
					//InitStruct(ref result, false);
					//Array.Copy(this._szCSDVersion, result._szCSDVersion, 128);
					//return result;
			//}
		//};

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void CopyMemory(System.IntPtr Destination, System.IntPtr Source, int Length);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure SecBufferDesc may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int CompleteAuthToken(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pToken);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int DeleteSecurityContext(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int FreeCredentialsHandle(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int FreeContextBuffer(int pvContextBuffer);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetProcessHeap();

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int HeapAlloc(int hHeap, int dwFlags, int dwBytes);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int HeapFree(int hHeap, int dwFlags, int lpMem);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure OSVERSIONINFO may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetVersionExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetVersionEx(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO lpVersionInformation);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("secur32.dll", EntryPoint = "QuerySecurityPackageInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int QuerySecurityPackageInfo([MarshalAs(UnmanagedType.VBByRefStr)] ref string PackageName, ref int pPackageInfo);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure TimeStamp may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecBufferDesc may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("secur32.dll", EntryPoint = "InitializeSecurityContextA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int InitializeSecurityContext(System.IntPtr phCredential, System.IntPtr phContext, int pszTargetName, int fContextReq, int Reserved1, int TargetDataRep, System.IntPtr pInput, int Reserved2, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure TimeStamp may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("secur32.dll", EntryPoint = "AcquireCredentialsHandleA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int AcquireCredentialsHandle(int pszPrincipal, [MarshalAs(UnmanagedType.VBByRefStr)] ref string pszPackage, int fCredentialUse, int pvLogonId, System.IntPtr pAuthData, int pGetKeyFn, int pvGetKeyArgument, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure TimeStamp may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecBufferDesc may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecBufferDesc may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		////UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int AcceptSecurityContext(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, System.IntPtr phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pInput, int fContextReq, int TargetDataRep, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry);

		private static bool GetClientContext(ref mbDirectorioActivo.AUTH_SEQ AuthSeq, mbDirectorioActivo.SEC_WINNT_AUTH_IDENTITY AuthIdentity, int pIn, int cbIn, int pOut, ref int cbOut, ref bool fDone)
		{

			bool result = false;
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc sbdOut = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc();
			mbDirectorioActivo.SecBuffer sbOut = new mbDirectorioActivo.SecBuffer();
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc sbdIn = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc();
			mbDirectorioActivo.SecBuffer sbIn = new mbDirectorioActivo.SecBuffer();
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp tsExpiry = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp();
			int fContextAttr = 0;
			int success = 0;

			if (!AuthSeq.fInitialized)
			{

				string tempRefParam = "NTLM";
				if (UpgradeSupportHelper.PInvoke.SafeNative.secur32.AcquireCredentialsHandle(0, ref tempRefParam, SECPKG_CRED_OUTBOUND, 0, ref AuthIdentity, 0, 0, ref AuthSeq.hcred, ref tsExpiry) != 0)
				{

					//failed to get credentials, so bail
					return false;

				}
				else
				{

					AuthSeq.fHaveCredHandle = true;

				} //If AcquireCredentialsHandle
			} //If Not AuthSeq.fInitialized

			//Prepare the output buffer
			sbdOut.ulVersion = 0;
			sbdOut.cBuffers = 1;
			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			sbdOut.pBuffers = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapAlloc(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), HEAP_ZERO_MEMORY, Marshal.SizeOf(sbOut));

			sbOut.cbBuffer = cbOut;
			sbOut.BufferType = SECBUFFER_TOKEN;
			sbOut.pvBuffer = pOut;

            //UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecBuffer, int>(sbdOut.pBuffers, ref sbOut, Marshal.SizeOf(sbOut));

			//attempt to establish a security context
			//between the server and a remote client.
			if (AuthSeq.fInitialized)
			{

				sbdIn.ulVersion = 0;
				sbdIn.cBuffers = 1;
				//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				sbdIn.pBuffers = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapAlloc(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), HEAP_ZERO_MEMORY, Marshal.SizeOf(sbIn));

				sbIn.cbBuffer = cbIn;
				sbIn.BufferType = SECBUFFER_TOKEN;
				sbIn.pvBuffer = pIn;

                //UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
                UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecBuffer, int>(sbdIn.pBuffers, ref sbIn, Marshal.SizeOf(sbIn));

				success = UpgradeSupportHelper.PInvoke.SafeNative.secur32.InitializeSecurityContext(ref AuthSeq.hcred, ref AuthSeq.hctxt, 0, 0, 0, SECURITY_NATIVE_DREP, ref sbdIn, 0, ref AuthSeq.hctxt, ref sbdOut, ref fContextAttr, ref tsExpiry);


			}
			else
			{

				int tempRefParam2 = 0;
				int tempRefParam3 = 0;
				success = UpgradeSupportHelper.PInvoke.SafeNative.secur32.InitializeSecurityContext(ref AuthSeq.hcred, ref tempRefParam2, 0, 0, 0, SECURITY_NATIVE_DREP, ref tempRefParam3, 0, ref AuthSeq.hctxt, ref sbdOut, ref fContextAttr, ref tsExpiry);
			} //If AuthSeq.fInitialized

			if (success >= SEC_E_OK)
			{

				//the security context received from
				//the client was accepted. If an output
				//token was generated by the function,
				//it must be sent to the client process.
				AuthSeq.fHaveCtxtHandle = true;

				//if a protocol (such as DCE) needs to
				//revise the security information after
				//the transport application has updated
				//some message parameters, pass it to
				//CompleteAuthToken
				if (success == SEC_I_COMPLETE_NEEDED || success == SEC_I_COMPLETE_AND_CONTINUE)
				{

					if (UpgradeSupportHelper.PInvoke.SafeNative.secur32.CompleteAuthToken(ref AuthSeq.hctxt, ref sbdOut) < SEC_E_OK)
					{

						//couldn't complete, so return false
						FreeMemory(sbdOut.pBuffers);
						FreeMemory(sbdIn.pBuffers);
						return false;

					} // If CompleteAuthToken

				} //If success

				//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				int tempRefParam4 = sbdOut.pBuffers;
                UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecBuffer, int>(ref sbOut, tempRefParam4, Marshal.SizeOf(sbOut));
				cbOut = sbOut.cbBuffer;

				AuthSeq.fInitialized = true;

				fDone = !(success == SEC_I_CONTINUE_NEEDED || success == SEC_I_COMPLETE_AND_CONTINUE);

				result = true;

			} //If success >= SEC_E_OK

			FreeMemory(sbdOut.pBuffers);
			FreeMemory(sbdIn.pBuffers);

			return result;
		}


		private static bool GetServerContext(ref mbDirectorioActivo.AUTH_SEQ AuthSeq, int pIn, int cbIn, int pOut, ref int cbOut, ref bool fDone)
		{

			bool result = false;
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc sbdOut = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc();
			mbDirectorioActivo.SecBuffer sbOut = new mbDirectorioActivo.SecBuffer();
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc sbdIn = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc();
			mbDirectorioActivo.SecBuffer sbIn = new mbDirectorioActivo.SecBuffer();
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp tsExpiry = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp();
			int fContextAttr = 0;
			int success = 0;

			if (!AuthSeq.fInitialized)
			{

				string tempRefParam = "NTLM";
				int tempRefParam2 = 0;
				if (UpgradeSupportHelper.PInvoke.SafeNative.secur32.AcquireCredentialsHandle(0, ref tempRefParam, SECPKG_CRED_INBOUND, 0, ref tempRefParam2, 0, 0, ref AuthSeq.hcred, ref tsExpiry) != 0)
				{

					//failed to get credentials, so bail
					return false;

				}
				else
				{

					AuthSeq.fHaveCredHandle = true;

				} //If AcquireCredentialsHandle

			} //If Not AuthSeq.fInitialized


			//Prepare the output and input buffers
			sbdOut.ulVersion = 0;
			sbdOut.cBuffers = 1;
			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			sbdOut.pBuffers = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapAlloc(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), HEAP_ZERO_MEMORY, Marshal.SizeOf(sbOut));

			sbOut.cbBuffer = cbOut;
			sbOut.BufferType = SECBUFFER_TOKEN;
			sbOut.pvBuffer = pOut;

			sbdIn.ulVersion = 0;
			sbdIn.cBuffers = 1;
			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			sbdIn.pBuffers = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapAlloc(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), HEAP_ZERO_MEMORY, Marshal.SizeOf(sbIn));

			sbIn.cbBuffer = cbIn;
			sbIn.BufferType = SECBUFFER_TOKEN;
			sbIn.pvBuffer = pIn;

			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			int tempRefParam3 = sbdOut.pBuffers;
            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecBuffer, int>(tempRefParam3, ref sbOut, Marshal.SizeOf(sbOut));
			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			int tempRefParam4 = sbdIn.pBuffers;
            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecBuffer, int>(tempRefParam4, ref sbIn, Marshal.SizeOf(sbIn));

			//attempt to establish a security context
			if (AuthSeq.fInitialized)
			{


				success = UpgradeSupportHelper.PInvoke.SafeNative.secur32.AcceptSecurityContext(ref AuthSeq.hcred, ref AuthSeq.hctxt, ref sbdIn, 0, SECURITY_NATIVE_DREP, ref AuthSeq.hctxt, ref sbdOut, ref fContextAttr, ref tsExpiry);
			}
			else
			{

				int tempRefParam5 = 0;
				success = UpgradeSupportHelper.PInvoke.SafeNative.secur32.AcceptSecurityContext(ref AuthSeq.hcred, ref tempRefParam5, ref sbdIn, 0, SECURITY_NATIVE_DREP, ref AuthSeq.hctxt, ref sbdOut, ref fContextAttr, ref tsExpiry);

			} //If AuthSeq.fInitialized

			if (success >= SEC_E_OK)
			{

				//the security context received from
				//the client was accepted. If an output
				//token was generated by the function,
				//it must be sent to the client process.
				AuthSeq.fHaveCtxtHandle = true;

				//if a protocol (such as DCE) needs to
				//revise the security information after
				//the transport application has updated
				//some message parameters, pass it to
				//CompleteAuthToken
				if (success == SEC_I_COMPLETE_NEEDED || success == SEC_I_COMPLETE_AND_CONTINUE)
				{

					if (UpgradeSupportHelper.PInvoke.SafeNative.secur32.CompleteAuthToken(ref AuthSeq.hctxt, ref sbdOut) < SEC_E_OK)
					{

						//couldn't complete, so return false
						FreeMemory(sbdOut.pBuffers);
						FreeMemory(sbdIn.pBuffers);
						return false;

					} // If CompleteAuthToken

				} //If success

				//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				int tempRefParam6 = sbdOut.pBuffers;
                UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecBuffer, int>(ref sbOut, tempRefParam6, Marshal.SizeOf(sbOut));
				cbOut = sbOut.cbBuffer;

				AuthSeq.fInitialized = true;

				fDone = !(success == SEC_I_CONTINUE_NEEDED || success == SEC_I_COMPLETE_AND_CONTINUE);

				result = true;

			} //If success >= SEC_E_OK

			FreeMemory(sbdOut.pBuffers);
			FreeMemory(sbdIn.pBuffers);

			return result;
		}


		internal static bool AuthenticateUser(string sDomain, string sUser, string sPassword)
		{

			//UPGRADE_WARNING: (1063) Arrays in structure osinfo may need to be initialized before they can be used. More Information: http://www.vbtonet.com/ewis/ewi1063.aspx
			bool result = false;
			UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO osinfo = UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO.CreateInstance();
			mbDirectorioActivo.AUTH_SEQ authClient = mbDirectorioActivo.AUTH_SEQ.CreateInstance();
			mbDirectorioActivo.AUTH_SEQ authServer = mbDirectorioActivo.AUTH_SEQ.CreateInstance();
			mbDirectorioActivo.SEC_WINNT_AUTH_IDENTITY swai = mbDirectorioActivo.SEC_WINNT_AUTH_IDENTITY.CreateInstance();
			mbDirectorioActivo.SecPkgInfo spi = new mbDirectorioActivo.SecPkgInfo();
			int ptrSpi = 0;
			int cbMaxToken = 0;
			int pClientBuf = 0;
			int pServerBuf = 0;
			int cbIn = 0;
			int cbOut = 0;
			bool fDone = false;


			//Determine if user's OS version
			//is Windows NT 5.0 or later
			if (IsWinNT2000Plus())
			{

				//Get max token size by passing to
				//QuerySecurityPackageInfo the name
				//of the security package to obtain
				//a pointer to a SECPKGINFO structure
				//containing security package information.
				//"NTLM" refers to "NT LAN Manager"
				//authentication, referred to as an
				//"NT Challenge"
				string tempRefParam = "NTLM";
				if (UpgradeSupportHelper.PInvoke.SafeNative.secur32.QuerySecurityPackageInfo(ref tempRefParam, ref ptrSpi) == SEC_E_OK)
				{

					//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					int tempRefParam2 = ptrSpi;
					UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory<mbDirectorioActivo.SecPkgInfo, int>(ref spi, tempRefParam2, Marshal.SizeOf(spi));
					cbMaxToken = spi.cbMaxToken;
					UpgradeSupportHelper.PInvoke.SafeNative.secur32.FreeContextBuffer(ptrSpi);

					//Allocate buffers for client
					//and server messages
					pClientBuf = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapAlloc(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), HEAP_ZERO_MEMORY, cbMaxToken);

					if (pClientBuf != 0)
					{

						pServerBuf = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapAlloc(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), HEAP_ZERO_MEMORY, cbMaxToken);

						if (pServerBuf != 0)
						{

							//Initialize authentication
							//identity structure
							swai.Domain = sDomain;
							swai.DomainLength = sDomain.Length;
							swai.User = sUser;
							swai.UserLength = sUser.Length;
							swai.Password = sPassword;
							swai.PasswordLength = sPassword.Length;

							//credentials passed are in ANSI
							swai.Flags = SEC_WINNT_AUTH_IDENTITY_ANSI;

							//Prepare the client message (negotiate)
							cbOut = cbMaxToken;
							if (GetClientContext(ref authClient, swai, 0, 0, pClientBuf, ref cbOut, ref fDone))
							{

								//Prepare the server message (challenge).
								//Most likely failure: AcceptServerContext
								//fails with SEC_E_LOGON_DENIED in the case
								//of bad szUser or szPassword.
								//
								//Note that there can be an unexpected result:
								//Validation will succeed if you
								//pass in a bad username to the call
								//when the guest account is enabled
								//in the specified domain.
								cbIn = cbOut;
								cbOut = cbMaxToken;

								if (GetServerContext(ref authServer, pClientBuf, cbIn, pServerBuf, ref cbOut, ref fDone))
								{

									//Prepare client message (authenticate)
									cbIn = cbOut;
									cbOut = cbMaxToken;

									if (GetClientContext(ref authClient, swai, pServerBuf, cbIn, pClientBuf, ref cbOut, ref fDone))
									{

										//Prepare server message (authenticate)
										cbIn = cbOut;
										cbOut = cbMaxToken;
										if (GetServerContext(ref authServer, pClientBuf, cbIn, pServerBuf, ref cbOut, ref fDone))
										{

											result = true;

										} //If GetServerContext(authServer
									} //If GetClientContext(authClient
								} //If GetServerContext(authServer
							} //If GetClientContext(authClient
						} //If pServerBuf <> 0
					} // If pClientBuf <> 0
				} //If QuerySecurityPackageInfo <> 0

				//Clean up resources
				if (authClient.fHaveCtxtHandle)
				{
					UpgradeSupportHelper.PInvoke.SafeNative.secur32.DeleteSecurityContext(ref authClient.hctxt);
				}
				if (authServer.fHaveCtxtHandle)
				{
					UpgradeSupportHelper.PInvoke.SafeNative.secur32.DeleteSecurityContext(ref authServer.hctxt);
				}
				if (authClient.fHaveCredHandle)
				{
					UpgradeSupportHelper.PInvoke.SafeNative.secur32.FreeCredentialsHandle(ref authClient.hcred);
				}
				if (authServer.fHaveCtxtHandle)
				{
					UpgradeSupportHelper.PInvoke.SafeNative.secur32.FreeCredentialsHandle(ref authServer.hcred);
				}
				FreeMemory(pClientBuf);
				FreeMemory(pServerBuf);

			}

			return result;
		}


		private static bool IsWinNT2000Plus()
		{

			//returns True if running Win2000 or WinXP
			bool result = false;
#if Win32


			//UPGRADE_WARNING: (1063) Arrays in structure OSV may need to be initialized before they can be used. More Information: http://www.vbtonet.com/ewis/ewi1063.aspx
			UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO OSV = UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO.CreateInstance();

			//UPGRADE_WARNING: (2081) Len has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			OSV.OSVSize = Marshal.SizeOf(OSV);

			if (UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetVersionEx(ref OSV) == 1)
			{

				//PlatformId contains a value representing the OS.
				result = (OSV.PlatformID == VER_PLATFORM_WIN32_NT) && (OSV.dwVerMajor >= 5);
			}

#endif

			return result;
		}


		private static void FreeMemory(int memblock)
		{

			if (memblock != 0)
			{
				UpgradeSupportHelper.PInvoke.SafeNative.kernel32.HeapFree(UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetProcessHeap(), 0, memblock);
			}

		}

		internal static void proCompruebaAutenticacion(SqlConnection cnAutenticacion)
		{


			string StSql = "select isNull(valfanu1,'I') Tipo, isNull(valfanu2,'') Dominio, isNull(nnumeri1, 8) Password from SCONSGLO where gconsglo = 'IAUTENTI'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, cnAutenticacion);
			DataSet RrAutenticacion = new DataSet();
			tempAdapter.Fill(RrAutenticacion);

			if (RrAutenticacion.Tables[0].Rows.Count == 0)
			{
				vstTipoAutenticacion = "I";
				vstDominioAutenticacion = "";
				viTama�oPassword = "8";
			}
			else
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstTipoAutenticacion = Convert.ToString(RrAutenticacion.Tables[0].Rows[0]["Tipo"]).Trim().ToUpper();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstDominioAutenticacion = Convert.ToString(RrAutenticacion.Tables[0].Rows[0]["Dominio"]).Trim();

				if (vstTipoAutenticacion == "I")
				{
					viTama�oPassword = "8";
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToDouble(RrAutenticacion.Tables[0].Rows[0]["Password"]) <= 8)
					{
						viTama�oPassword = "8";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						viTama�oPassword = Convert.ToString(RrAutenticacion.Tables[0].Rows[0]["Password"]);
					}
				}

				// Si no hay dominio de validaci�n, solo podemos validar por I-MDH

				if (vstDominioAutenticacion.Trim() == "")
				{
					vstTipoAutenticacion = "I";
				}

			}

			// Este valor lo reiniciamos

			vstAutenticacionUsuario = "I";

			
			RrAutenticacion.Close();

		}

		internal static void proAutenticacionUsuario(string ElUsuario, SqlConnection cnAutenticacion, bool fbLargo)
		{


			string StSql = "select isNull(itipaute,'I') itipaute from SUSUARIO where " + 
			               ((fbLargo) ? "gusularg" : "gusuario") + " = '" + ElUsuario.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, cnAutenticacion);
			DataSet RrAutenticacion = new DataSet();
			tempAdapter.Fill(RrAutenticacion);

			if (RrAutenticacion.Tables[0].Rows.Count == 0)
			{
				vstAutenticacionUsuario = "I";
			}
			else
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstAutenticacionUsuario = Convert.ToString(RrAutenticacion.Tables[0].Rows[0]["itipaute"]).Trim().ToUpper();
			}

			
			RrAutenticacion.Close();

		}
	}
}