﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Localization;
//using Telerik.WinControls.UI.Localization;

namespace ElementosCompartidos
{
    /// <summary>
    ///Localización para el texto de los botones de los RadMessageBox
    /// </summary>
    public class ClassRadMessageLocalizationProvider : RadMessageLocalizationProvider
    {
        public override string GetLocalizedString(string id)
        {
            switch (id)
            {
                case RadMessageStringID.AbortButton: return "Abortar";
                case RadMessageStringID.CancelButton: return "Cancelar";
                case RadMessageStringID.IgnoreButton: return "Ignorar";
                case RadMessageStringID.NoButton: return "No";
                case RadMessageStringID.OKButton: return "Aceptar";
                case RadMessageStringID.RetryButton: return "Reintentar";
                case RadMessageStringID.YesButton: return "Si";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }

    /// <summary>
    ///Localización para el control RadGridView
    /// </summary>
    public class ClassRadGridLocalizationProvider : RadGridLocalizationProvider
        {
            public override string GetLocalizedString(string id)
            {
                switch (id)
                {
                    case RadGridStringId.ConditionalFormattingPleaseSelectValidCellValue: return "Por favor seleccione un valor de celda válido";
                    case RadGridStringId.ConditionalFormattingPleaseSetValidCellValue: return "Por favor entre un valor de celda válido";
                    case RadGridStringId.ConditionalFormattingPleaseSetValidCellValues: return "Por favor entre valores de celda válidos";
                    case RadGridStringId.ConditionalFormattingPleaseSetValidExpression: return "Por favor entre una expresión válida";
                    case RadGridStringId.ConditionalFormattingItem: return "Item";
                    case RadGridStringId.ConditionalFormattingInvalidParameters: return "Parametros inválidos";
                    case RadGridStringId.FilterFunctionBetween: return "Entre";
                    case RadGridStringId.FilterFunctionContains: return "Contiene";
                    case RadGridStringId.FilterFunctionDoesNotContain: return "No contiene";
                    case RadGridStringId.FilterFunctionEndsWith: return "Termina en";
                    case RadGridStringId.FilterFunctionEqualTo: return "Igual";
                    case RadGridStringId.FilterFunctionGreaterThan: return "Mayor que";
                    case RadGridStringId.FilterFunctionGreaterThanOrEqualTo: return "Mayor o igual que";
                    case RadGridStringId.FilterFunctionIsEmpty: return "Es vacio";
                    case RadGridStringId.FilterFunctionIsNull: return "Es null";
                    case RadGridStringId.FilterFunctionLessThan: return "Menor que";
                    case RadGridStringId.FilterFunctionLessThanOrEqualTo: return "Menor o igual que";
                    case RadGridStringId.FilterFunctionNoFilter: return "Sin filtro";
                    case RadGridStringId.FilterFunctionNotBetween: return "No entre";
                    case RadGridStringId.FilterFunctionNotEqualTo: return "Diferente de";
                    case RadGridStringId.FilterFunctionNotIsEmpty: return "No es vacio";
                    case RadGridStringId.FilterFunctionNotIsNull: return "No es null";
                    case RadGridStringId.FilterFunctionStartsWith: return "Inicia con";
                    case RadGridStringId.FilterFunctionCustom: return "Personalizado";
                    case RadGridStringId.FilterOperatorBetween: return "Entre";
                    case RadGridStringId.FilterOperatorContains: return "Contiene";
                    case RadGridStringId.FilterOperatorDoesNotContain: return "No contiene";
                    case RadGridStringId.FilterOperatorEndsWith: return "Termina con";
                    case RadGridStringId.FilterOperatorEqualTo: return "Igual";
                    case RadGridStringId.FilterOperatorGreaterThan: return "Mayor que";
                    case RadGridStringId.FilterOperatorGreaterThanOrEqualTo: return "Mayor o igual que";
                    case RadGridStringId.FilterOperatorIsEmpty: return "Es Vacio";
                    case RadGridStringId.FilterOperatorIsNull: return "Es null";
                    case RadGridStringId.FilterOperatorLessThan: return "Menor que";
                    case RadGridStringId.FilterOperatorLessThanOrEqualTo: return "Menor o igual que";
                    case RadGridStringId.FilterOperatorNoFilter: return "Sin filtro";
                    case RadGridStringId.FilterOperatorNotBetween: return "No esta entre";
                    case RadGridStringId.FilterOperatorNotEqualTo: return "No es igual";
                    case RadGridStringId.FilterOperatorNotIsEmpty: return "No es vacio";
                    case RadGridStringId.FilterOperatorNotIsNull: return "No es null";
                    case RadGridStringId.FilterOperatorStartsWith: return "Inicia con";
                    case RadGridStringId.FilterOperatorIsLike: return "Like";
                    case RadGridStringId.FilterOperatorNotIsLike: return "NotLike";
                    case RadGridStringId.FilterOperatorIsContainedIn: return "ContainedIn";
                    case RadGridStringId.FilterOperatorNotIsContainedIn: return "NotContainedIn";
                    case RadGridStringId.FilterOperatorCustom: return "Custom";
                    case RadGridStringId.CustomFilterMenuItem: return "Custom";
                    case RadGridStringId.CustomFilterDialogCaption: return "RadGridView Filter Dialog [{0}]";
                    case RadGridStringId.CustomFilterDialogLabel: return "Show rows where:";
                    case RadGridStringId.CustomFilterDialogRbAnd: return "And";
                    case RadGridStringId.CustomFilterDialogRbOr: return "Or";
                    case RadGridStringId.CustomFilterDialogBtnOk: return "Aceptar";
                    case RadGridStringId.CustomFilterDialogBtnCancel: return "Cancelar";
                    case RadGridStringId.CustomFilterDialogCheckBoxNot: return "Not";
                    case RadGridStringId.CustomFilterDialogTrue: return "True";
                    case RadGridStringId.CustomFilterDialogFalse: return "False";
                    case RadGridStringId.FilterMenuBlanks: return "Empty";
                    case RadGridStringId.FilterMenuAvailableFilters: return "Available Filters";
                    case RadGridStringId.FilterMenuSearchBoxText: return "Buscar...";
                    case RadGridStringId.FilterMenuClearFilters: return "Limpiar Filtro";
                    case RadGridStringId.FilterMenuButtonOK: return "Aceptar";
                    case RadGridStringId.FilterMenuButtonCancel: return "Cancelar";
                    case RadGridStringId.FilterMenuSelectionAll: return "Todo";
                    case RadGridStringId.FilterMenuSelectionAllSearched: return "All Search Result";
                    case RadGridStringId.FilterMenuSelectionNull: return "Null";
                    case RadGridStringId.FilterMenuSelectionNotNull: return "Not Null";
                    case RadGridStringId.FilterFunctionSelectedDates: return "Filter by specific dates:";
                    case RadGridStringId.FilterFunctionToday: return "Hoy";
                    case RadGridStringId.FilterFunctionYesterday: return "Ayer";
                    case RadGridStringId.FilterFunctionDuringLast7days: return "During last 7 days";
                    case RadGridStringId.FilterLogicalOperatorAnd: return "AND";
                    case RadGridStringId.FilterLogicalOperatorOr: return "OR";
                    case RadGridStringId.FilterCompositeNotOperator: return "NOT";
                    case RadGridStringId.DeleteRowMenuItem: return "Eliminar fila";
                    case RadGridStringId.SortAscendingMenuItem: return "Ordenar Ascendentemente";
                    case RadGridStringId.SortDescendingMenuItem: return "Ordenar Descendentemente";
                    case RadGridStringId.ClearSortingMenuItem: return "Limpiar Ordenamiento";
                    case RadGridStringId.ConditionalFormattingMenuItem: return "Conditional Formatting";
                    case RadGridStringId.GroupByThisColumnMenuItem: return "Group by this column";
                    case RadGridStringId.UngroupThisColumn: return "Ungroup this column";
                    case RadGridStringId.ColumnChooserMenuItem: return "Column Chooser";
                    case RadGridStringId.HideMenuItem: return "Hide Column";
                    case RadGridStringId.HideGroupMenuItem: return "Hide Group";
                    case RadGridStringId.UnpinMenuItem: return "Unpin Column";
                    case RadGridStringId.UnpinRowMenuItem: return "Unpin Row";
                    case RadGridStringId.PinMenuItem: return "Pinned state";
                    case RadGridStringId.PinAtLeftMenuItem: return "Pin at left";
                    case RadGridStringId.PinAtRightMenuItem: return "Pin at right";
                    case RadGridStringId.PinAtBottomMenuItem: return "Pin at bottom";
                    case RadGridStringId.PinAtTopMenuItem: return "Pin at top";
                    case RadGridStringId.BestFitMenuItem: return "Best Fit";
                    case RadGridStringId.PasteMenuItem: return "Paste";
                    case RadGridStringId.EditMenuItem: return "Edit";
                    case RadGridStringId.ClearValueMenuItem: return "Clear Value";
                    case RadGridStringId.CopyMenuItem: return "Copiar";
                    case RadGridStringId.CutMenuItem: return "Cortar";
                    case RadGridStringId.AddNewRowString: return "Click aquí para adicionar una nueva fila";
                    case RadGridStringId.SearchRowResultsOfLabel: return "of";
                    case RadGridStringId.SearchRowMatchCase: return "Match case";
                    case RadGridStringId.ConditionalFormattingSortAlphabetically: return "Sort columns alphabetically";
                    case RadGridStringId.ConditionalFormattingCaption: return "Conditional Formatting Rules Manager";
                    case RadGridStringId.ConditionalFormattingLblColumn: return "Format only cells with";
                    case RadGridStringId.ConditionalFormattingLblName: return "Rule name";
                    case RadGridStringId.ConditionalFormattingLblType: return "Cell value";
                    case RadGridStringId.ConditionalFormattingLblValue1: return "Value 1";
                    case RadGridStringId.ConditionalFormattingLblValue2: return "Value 2";
                    case RadGridStringId.ConditionalFormattingGrpConditions: return "Rules";
                    case RadGridStringId.ConditionalFormattingGrpProperties: return "Rule Properties";
                    case RadGridStringId.ConditionalFormattingChkApplyToRow: return "Apply this formatting to entire row";
                    case RadGridStringId.ConditionalFormattingChkApplyOnSelectedRows: return "Apply this formatting if the row is selected";
                    case RadGridStringId.ConditionalFormattingBtnAdd: return "Add new rule";
                    case RadGridStringId.ConditionalFormattingBtnRemove: return "Remove";
                    case RadGridStringId.ConditionalFormattingBtnOK: return "OK";
                    case RadGridStringId.ConditionalFormattingBtnCancel: return "Cancel";
                    case RadGridStringId.ConditionalFormattingBtnApply: return "Apply";
                    case RadGridStringId.ConditionalFormattingRuleAppliesOn: return "Rule applies to";
                    case RadGridStringId.ConditionalFormattingCondition: return "Condition";
                    case RadGridStringId.ConditionalFormattingExpression: return "Expression";
                    case RadGridStringId.ConditionalFormattingChooseOne: return "[Choose one]";
                    case RadGridStringId.ConditionalFormattingEqualsTo: return "equals to [Value1]";
                    case RadGridStringId.ConditionalFormattingIsNotEqualTo: return "is not equal to [Value1]";
                    case RadGridStringId.ConditionalFormattingStartsWith: return "starts with [Value1]";
                    case RadGridStringId.ConditionalFormattingEndsWith: return "ends with [Value1]";
                    case RadGridStringId.ConditionalFormattingContains: return "contains [Value1]";
                    case RadGridStringId.ConditionalFormattingDoesNotContain: return "does not contain [Value1]";
                    case RadGridStringId.ConditionalFormattingIsGreaterThan: return "is greater than [Value1]";
                    case RadGridStringId.ConditionalFormattingIsGreaterThanOrEqual: return "is greater than or equal [Value1]";
                    case RadGridStringId.ConditionalFormattingIsLessThan: return "is less than [Value1]";
                    case RadGridStringId.ConditionalFormattingIsLessThanOrEqual: return "is less than or equal to [Value1]";
                    case RadGridStringId.ConditionalFormattingIsBetween: return "is between [Value1] and [Value2]";
                    case RadGridStringId.ConditionalFormattingIsNotBetween: return "is not between [Value1] and [Value1]";
                    case RadGridStringId.ConditionalFormattingLblFormat: return "Format";
                    case RadGridStringId.ConditionalFormattingBtnExpression: return "Expression editor";
                    case RadGridStringId.ConditionalFormattingTextBoxExpression: return "Expression";
                    case RadGridStringId.ConditionalFormattingPropertyGridCaseSensitive: return "CaseSensitive";
                    case RadGridStringId.ConditionalFormattingPropertyGridCellBackColor: return "CellBackColor";
                    case RadGridStringId.ConditionalFormattingPropertyGridCellForeColor: return "CellForeColor";
                    case RadGridStringId.ConditionalFormattingPropertyGridEnabled: return "Enabled";
                    case RadGridStringId.ConditionalFormattingPropertyGridRowBackColor: return "RowBackColor";
                    case RadGridStringId.ConditionalFormattingPropertyGridRowForeColor: return "RowForeColor";
                    case RadGridStringId.ConditionalFormattingPropertyGridRowTextAlignment: return "RowTextAlignment";
                    case RadGridStringId.ConditionalFormattingPropertyGridTextAlignment: return "TextAlignment";
                    case RadGridStringId.ConditionalFormattingPropertyGridCaseSensitiveDescription: return "Determines whether case-sensitive comparisons will be made when evaluating string values.";
                    case RadGridStringId.ConditionalFormattingPropertyGridCellBackColorDescription: return "Enter the background color to be used for the cell.";
                    case RadGridStringId.ConditionalFormattingPropertyGridCellForeColorDescription: return "Enter the foreground color to be used for the cell.";
                    case RadGridStringId.ConditionalFormattingPropertyGridEnabledDescription: return "Determines whether the condition is enabled (can be evaluated and applied).";
                    case RadGridStringId.ConditionalFormattingPropertyGridRowBackColorDescription: return "Enter the background color to be used for the entire row.";
                    case RadGridStringId.ConditionalFormattingPropertyGridRowForeColorDescription: return "Enter the foreground color to be used for the entire row.";
                    case RadGridStringId.ConditionalFormattingPropertyGridRowTextAlignmentDescription: return "Enter the alignment to be used for the cell values, when ApplyToRow is true.";
                    case RadGridStringId.ConditionalFormattingPropertyGridTextAlignmentDescription: return "Enter the alignment to be used for the cell values.";
                    case RadGridStringId.ColumnChooserFormCaption: return "Column Chooser";
                    case RadGridStringId.ColumnChooserFormMessage: return "Drag a column header from the\ngrid here to remove it from\nthe current view.";
                    case RadGridStringId.GroupingPanelDefaultMessage: return "Drag a column here to group by this column.";
                    case RadGridStringId.GroupingPanelHeader: return "Group by:";
                    case RadGridStringId.PagingPanelPagesLabel: return "Page";
                    case RadGridStringId.PagingPanelOfPagesLabel: return "of";
                    case RadGridStringId.NoDataText: return "No hay datos para mostrar";
                    case RadGridStringId.CompositeFilterFormErrorCaption: return "Filter Error";
                    case RadGridStringId.CompositeFilterFormInvalidFilter: return "The composite filter descriptor is not valid.";
                    case RadGridStringId.ExpressionMenuItem: return "Expression";
                    case RadGridStringId.ExpressionFormTitle: return "Expression Builder";
                    case RadGridStringId.ExpressionFormFunctions: return "Functions";
                    case RadGridStringId.ExpressionFormFunctionsText: return "Text";
                    case RadGridStringId.ExpressionFormFunctionsAggregate: return "Aggregate";
                    case RadGridStringId.ExpressionFormFunctionsDateTime: return "Date-Time";
                    case RadGridStringId.ExpressionFormFunctionsLogical: return "Logical";
                    case RadGridStringId.ExpressionFormFunctionsMath: return "Math";
                    case RadGridStringId.ExpressionFormFunctionsOther: return "Other";
                    case RadGridStringId.ExpressionFormOperators: return "Operators";
                    case RadGridStringId.ExpressionFormConstants: return "Constants";
                    case RadGridStringId.ExpressionFormFields: return "Fields";
                    case RadGridStringId.ExpressionFormDescription: return "Description";
                    case RadGridStringId.ExpressionFormResultPreview: return "Result preview";
                    case RadGridStringId.ExpressionFormTooltipPlus: return "Plus";
                    case RadGridStringId.ExpressionFormTooltipMinus: return "Minus";
                    case RadGridStringId.ExpressionFormTooltipMultiply: return "Multiply";
                    case RadGridStringId.ExpressionFormTooltipDivide: return "Divide";
                    case RadGridStringId.ExpressionFormTooltipModulo: return "Modulo";
                    case RadGridStringId.ExpressionFormTooltipEqual: return "Equal";
                    case RadGridStringId.ExpressionFormTooltipNotEqual: return "Not Equal";
                    case RadGridStringId.ExpressionFormTooltipLess: return "Less";
                    case RadGridStringId.ExpressionFormTooltipLessOrEqual: return "Less Or Equal";
                    case RadGridStringId.ExpressionFormTooltipGreaterOrEqual: return "Greater Or Equal";
                    case RadGridStringId.ExpressionFormTooltipGreater: return "Greater";
                    case RadGridStringId.ExpressionFormTooltipAnd: return "Logical \"AND\"";
                    case RadGridStringId.ExpressionFormTooltipOr: return "Logical \"OR\"";
                    case RadGridStringId.ExpressionFormTooltipNot: return "Logical \"NOT\"";
                    case RadGridStringId.ExpressionFormAndButton: return string.Empty; //if empty, default button image is used
                    case RadGridStringId.ExpressionFormOrButton: return string.Empty; //if empty, default button image is used
                    case RadGridStringId.ExpressionFormNotButton: return string.Empty; //if empty, default button image is used
                    case RadGridStringId.ExpressionFormOKButton: return "OK";
                    case RadGridStringId.ExpressionFormCancelButton: return "Cancel";
                }
                return string.Empty;
            }
        }
   
}
