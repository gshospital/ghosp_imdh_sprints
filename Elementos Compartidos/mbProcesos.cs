using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ElementosCompartidos
{
	internal static class mbProcesos
	{
		public const int PROCESS_TERMINATE = 0x1;

        public struct PROCESSENTRY32
        {
            public int dwSize;
            public int cntUsage;
            public int th32ProcessID;
            public int th32DefaultHeapID;
            public int th32ModuleID;
            public int cntThreads;
            public int th32ParentProcessID;
            public int pcPriClassBase;
            public int dwFlags;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 260)]
            private char[] _szExeFile;

            public string szExeFile
            {
                get
                {
                    return new string(_szExeFile);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szExeFile, value);
                }
            }

            private static void InitStruct(ref PROCESSENTRY32 result, bool init)
            {
                result._szExeFile = new char[260];
            }

            public static PROCESSENTRY32 CreateInstance()
            {
                PROCESSENTRY32 result = new PROCESSENTRY32();
                InitStruct(ref result, true);
                return result;
            }

            public PROCESSENTRY32 Clone()
            {
                PROCESSENTRY32 result = this;
                InitStruct(ref result, false);
                Array.Copy(this._szExeFile, result._szExeFile, 260);
                return result;
            }
        };

		internal static void proCerrarModulos(SqlConnection RcCierre, string ultimo = "")
		{
			string StSql = "select * from SSIAEXES order by norden";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcCierre);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
			{

				if (Convert.ToString(iteration_row["oejecuta"]).Trim().ToUpper() != ultimo.Trim().ToUpper())
				{
					while (proCerrarModuloPorNombre(Convert.ToString(iteration_row["oejecuta"]).Trim()))
					{
						UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(500);
					}
				}
			}			
			RrDatos.Close();
			// Si hemos cerrado todo menos el �ltimo, tenemos que cerrarlo tambi�n

			if (ultimo.Trim() != "")
			{
				while (proCerrarModuloPorNombre(ultimo.Trim()))
				{
					UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(500);
				}
			}
		}

		internal static bool proCerrarModuloPorPID(int lProceso)
		{
			bool result = false;
			int hProceso = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.OpenProcess(PROCESS_TERMINATE, -1, lProceso);
			int Resultado = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.TerminateProcess(hProceso, 99);
			UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CloseHandle(hProceso);
			// Si se cerr� correctamente...
			if (Resultado != 0)
			{
				result = true;
			}
			return result;
		}

        /// <summary>
        /// indra - ralopezn - 01/04/2016
        /// Se reemplaza implementaci�n utilizando m�todos nativos para trabajar con procesos.
        /// Aqu� se utiliza el m�todo para realizar el kill a procesos abiertos.
        /// </summary>
        /// <param name="stNombre">Nombre de la aplicaci�n</param>
        /// <returns>Verdadero si se pudo cerrar el modulo</returns>
		private static bool proCerrarModuloPorNombre(string stNombre)
		{
			bool result = false;
            System.Diagnostics.Process procesoDelModulo = FindProcessByName(stNombre);
            // Si el proceso est� abierto...

            if (procesoDelModulo != null)
            {
                try
                {
                    procesoDelModulo.Kill();
                    result = true;
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }
			return result;
		}

        /// <summary>
        /// indra - ralopezn - 01/04/2016
        /// Se crea este metodo para buscar procesos que se encuentran activos dentro del
        /// sistema para luego retorna el correspondiente. En caso de no encontrar ninguno,
        /// retornar� nulo.
        /// </summary>
        /// <param name="processName">Nombre del proceso a buscar</param>
        /// <returns>Objeto de tipo Process a retornar</returns>
       public static System.Diagnostics.Process FindProcessByName(string processName,  string prefijo ="NET_")
        {
            System.Diagnostics.Process procesoDelModulo = null;
            string nombreProcesoPrincipal = System.IO.Path.GetFileNameWithoutExtension(processName);
            System.Diagnostics.Process[] listadoDeProcesosActivos = System.Diagnostics.Process.GetProcesses();
            System.Collections.IEnumerator lista = listadoDeProcesosActivos.GetEnumerator();
           

            while (lista.MoveNext())
            {
                System.Diagnostics.Process proceso = (System.Diagnostics.Process)lista.Current;
                if (proceso.ProcessName.ToLower().Contains((prefijo + nombreProcesoPrincipal).ToLower().Trim()))
                {
                    procesoDelModulo = proceso;
                    break;
                }
            }

            return procesoDelModulo;
        }

        /// <summary>
        /// indra - ralopezn - 01/04/2016
        /// Se ajusta m�todo y se reemplaza su implementaci�n para utilizar los m�todos nativos
        /// de .Net para la consulta de procesos en el sistema.
        /// </summary>
        /// <returns>Verdadero si el proceso de la aplicaci�n Principal se encuentra activo</returns>
		internal static bool estaPrincipalActivo()
		{
            string nombreProcesoPrincipal = System.IO.Path.GetFileNameWithoutExtension("Principal");
            System.Diagnostics.Process procesoActivo = FindProcessByName("Principal");

            // Si el proceso est� abierto...
            return procesoActivo != null;
		}

		internal static void proActivaTimer(SqlConnection rcTimer, Timer elTimer)
		{
			string StSql = "select isNull(valfanu1,'N') valfanu1, isNull(nnumeri2, 0) nnumeri2 from " + 
			               "SCONSGLO where gconsglo = 'IBLOQUEO'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcTimer);
			DataSet RrTimer = new DataSet();
			tempAdapter.Fill(RrTimer);

			elTimer.Enabled = false;

			if (RrTimer.Tables[0].Rows.Count != 0)
			{				
				if (Convert.ToString(RrTimer.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{					
					if (Convert.ToDouble(RrTimer.Tables[0].Rows[0]["nnumeri2"]) > 0)
					{						
						if (Convert.ToInt32(Convert.ToDouble(RrTimer.Tables[0].Rows[0]["nnumeri2"]) * 1000) == 0)
						{
							elTimer.Enabled = false;
						}
						else
						{
							elTimer.Interval = Convert.ToInt32(Convert.ToDouble(RrTimer.Tables[0].Rows[0]["nnumeri2"]) * 1000);
							elTimer.Enabled = true;
						}
						elTimer.Enabled = true;
					}
				}
			}			
			RrTimer.Close();
		}
	}
}